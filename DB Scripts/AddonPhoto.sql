USE [DB_A0683E_Booking]
GO

/****** Object:  Table [dbo].[ServicePhoto]    Script Date: 09/26/2016 18:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO



CREATE TABLE [dbo].[AddonPhoto](
	[rowID] [int] IDENTITY(1,1) NOT NULL,
	[AddonID] [bigint] NOT NULL,
	[photo1] [varbinary](max) NULL,
	[photo2] [varbinary](max) NULL,
	[photo3] [varbinary](max) NULL,
 CONSTRAINT [PK_AddonPhot] PRIMARY KEY CLUSTERED 
(
	[rowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AddonPhoto]  WITH CHECK ADD  CONSTRAINT [FK_AddonPhoto_Addon] FOREIGN KEY([AddonID])
REFERENCES dbo.Addon ([addonID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AddonPhoto] CHECK CONSTRAINT [FK_AddonPhoto_Addon]
GO


