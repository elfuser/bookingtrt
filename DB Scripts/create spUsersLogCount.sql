CREATE PROC [dbo].[spUsersLogCount]
 ( @user_id int,  
  @from  smalldatetime ,  
  @to   smalldatetime  
  )

AS
BEGIN


     SELECT COUNT(LogID) 
     FROM UsersLog 
    WHERE user_id  = @user_id and  event_date between @from and @to  

	 
 
END