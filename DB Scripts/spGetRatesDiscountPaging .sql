CREATE  proc [dbo].[spGetRatesDiscountPaging]    
  @company_code char(10) = '',    
  @rate_code  char(10) = '',   
  @PageNumber INT,      
  @PageSize INT = NULL,      
  @OrderBy VARCHAR(100) = NULL,      
  @IsAccending BIT = 1    
    
AS    
BEGIN    
    
  DECLARE @SQLString NVARCHAR(MAX),      
     @FilterString VARCHAR(MAX),       
     @ParmDefinition NVARCHAR(500)     
         
  DECLARE @cultureID int      
       
   IF (@PageSize IS NULL OR @PageSize = 0)      
   SET @PageSize = 20      
          
   IF (@OrderBy IS NULL OR @OrderBy = '')      
   SET @OrderBy = '  RowID '      
         
   IF @IsAccending = 0      
   SET @OrderBy = @OrderBy + ' DESC'      
   ELSE      
   SET @OrderBy = @OrderBy + ' ASC'      
         
   SET  @FilterString = ''     
       
   SET @ParmDefinition = N'     
      @company_code char(10),     
      @rate_code char(10),    
   @PageNumber INT,      
   @PageSize INT = NULL        
   '     
        
      
 Set @SQLString=' WITH SQLPaging AS            
         
 (SELECT TOP ( @PageSize* @PageNumber )            
         
   ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),          
     [RowID]  
    ,[company_code]  
    ,[rate_code]  
    ,[type]  
    ,[discount]  
    ,[disc1]  
    ,[disc2]  
    ,[disc3]  
    ,[disc4]  
    ,[disc5]  
    ,[disc6]  
    ,[disc7]  
    ,[disc8]  
    ,[dateFrom]  
    ,[dateTo]  
  FROM [dbo].[RateDiscount]  
  WHERE company_code = @company_code    
      AND  rate_code=@rate_code    
  ORDER BY ' + @OrderBy  + '           
         
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'            
         
         
         
    EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@rate_code,@PageNumber,@PageSize        
      
END    
       
             