USE [DB_A0683E_BookingEngine]
GO
/****** Object:  StoredProcedure [dbo].[spCompanies]    Script Date: 04/04/2017 10:54:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spCompanies]    
 (     
 @mode    char(3),    
 @company_code  char(10),    
 @company_name  varchar(50) = '',    
 @legal_name   varchar(50) = '',    
 @legail_id   varchar(50) = '',    
 @address   text = '',    
 @telephone_nr1  varchar(15) = '',    
 @telephone_nr2  varchar(15) = '',    
 @telephone_nr3  varchar(15) = '',    
 @fax_nr    varchar(15) = '',    
 @contact_name  varchar(50) = '',    
 @contact_email  varchar(50) = '',    
 @web    varchar(50) = '',    
 @email    varchar(50) = '',    
 @call_toll_free  varchar(15) = '',    
 @bank_account  varchar(50) = '',    
 @bank_name   varchar(20) = '',    
 @cfo_name   varchar(50) = '',    
 @cfo_email   varchar(50) = '',    
 @res_name   varchar(50) = '',    
 @res_email   varchar(50) = '',    
 @res_email_alt  varchar(50) = '',    
 @facebookURL  varchar(100) = '',    
 @twitterText  varchar(140) = '',    
 @culture   char(5) = '',    
 @company_type  char(1) = '',    
 @hotel    bit = 0,    
 @services   bit = 0,    
 @googleID   varchar(20) = '',    
 @googleConvID  varchar(20) = '',    
 @googleLabel  varchar(30) = '',    
 @active    bit = 0,    
 @userID    int = 0,    
 @migrar    bit = 0 ,
  @provincia   varchar(50) = '',  
  @canton  varchar(50) = '', 
  @distrito   varchar(50) = '',
  @TechContact varchar(200) = '',
  @TechEmail varchar(200) = ''
 )    
AS    
    
 Set nocount on    
 DECLARE @cultureID int    
 SET @cultureID = 1   
   
 If @mode = 'INS'    
 BEGIN  
 IF EXISTS( SELECT 1  
    FROM Company  
    WHERE company_code = @company_code)  
 BEGIN  
  SET @mode = 'UPD'    
 END   
 END   
    
     
 If @mode = 'INS'    
     
  Begin    
      
   Insert into Company    
   (company_code, company_name, legal_name, legal_id, creation_date, address, telephone_nr1, telephone_nr2, telephone_nr3, fax_nr, contact_name,    
    web, email, call_toll_free, guest_nr, hotel, [services],    
    active, contact_email, bank_account, bank_name, cfo_name, cfo_email, res_name, res_email, res_email_alt,    
    facebookURL, twitterText, google_id, google_conv_id, google_conv_label, city, state, 
    country_iso3, descriptor, migrar,Provincia,Canton,Distrito,TechContact,TechEmail)    
   Values    
   (@company_code, @company_name, @legal_name, @legail_id, GETDATE(), @address, @telephone_nr1, @telephone_nr2, @telephone_nr3, @fax_nr, @contact_name,    
    @web, @email,@call_toll_free,  0, @hotel, @services,    
    1, @contact_email, @bank_account, @bank_name, @cfo_name, @cfo_email, @res_name, @res_email, @res_email_alt,    
    @facebookURL, @twitterText, @googleID, @googleConvID, @googleLabel, '', '', 'CRI','', 
    @migrar,@provincia,@canton,@distrito,@TechContact,@TechEmail)    
    
    Insert into UserCompany (company_code, user_id)    
    VALUES (@company_code, @userID)    
  End    
      
 If @mode = 'UPD'    
     
  Begin    
      
   Update Company    
   Set company_name = @company_name, legal_name = @legal_name, legal_id = @legail_id, address = @address,     
    telephone_nr1 = @telephone_nr1, telephone_nr2 = @telephone_nr2, telephone_nr3 = @telephone_nr3, fax_nr = @fax_nr, contact_name = @contact_name,    
    web = @web, email = @email, call_toll_free = @call_toll_free,     
    contact_email = @contact_email, res_email_alt = @res_email_alt,    
    bank_account = @bank_account, bank_name = @bank_name, cfo_name = @cfo_name, cfo_email = @cfo_email, res_name = @res_name, res_email = @res_email,    
    facebookURL= @facebookURL, twitterText = @twitterText, hotel = @hotel, [services] = @services, migrar = @migrar,    
    google_id = @googleID, google_conv_id = @googleConvID, 
    google_conv_label = @googleLabel, active = @active ,
    provincia= @provincia,canton=@canton,distrito =@distrito ,
   TechContact=@TechContact,TechEmail=@TechEmail
   Where    
    company_code = @company_code    
      
  End    
     
 If @mode = 'DEL'    
     
  Begin    
   BEGIN TRANSACTION    
    Delete From CompanyHotel    
    Where company_code = @company_code    
    
    Delete From CompanyService    
    Where company_code = @company_code    
    
    delete From CompanyProcessor    
    Where company_code = @company_code    
    
    delete from UserCompany    
    Where company_code = @company_code    
    
    Delete From Company    
    Where company_code = @company_code    
    
   IF @@ERROR = 0    
    COMMIT TRANSACTION    
   ELSE    
    ROLLBACK TRANSACTION    
  End    
    
 If @mode = 'SEL'    
     
  Begin    
      
   If @company_code = ''       
    Begin    
      SELECT * from Company     
      ORDER BY company_name    
    End    
    
   Else    
       
    Begin    
     IF @company_type = 'G'    
      Select c.*,  p.credomatic, p.bancoNacional, p.PayPal, p.credomaticProcID,    
       (Select count(*) From dbo.Addon Where company_code=@company_code) as Addons     
      From Company c INNER JOIN CompanyProcessor p ON c.company_code = p.company_code    
      Where c.company_code = @company_code    
    
     IF @company_type = 'H'    
      Select c.*, ch.*,  p.credomatic, p.bancoNacional, p.PayPal, p.credomaticProcID,    
       (Select count(*) From dbo.Addon Where company_code=@company_code) as Addons     
      From Company c INNER JOIN CompanyProcessor p ON c.company_code = p.company_code    
       INNER JOIN CompanyHotel ch ON c.company_code = ch.company_code    
      Where c.company_code = @company_code    
    
     IF @company_type = 'S'    
     BEGIN    
      IF @culture=''    
       SET @cultureID = 1    
      ELSE    
       SELECT @cultureID = ISNULL(CultureID,0) FROM dbo.Culture WHERE CultureName = @culture    
    
      Select c.*, ch.*,  p.credomatic, p.bancoNacional, p.PayPal, p.credomaticProcID, ctl.terms, ctl.waiver    
      From Company c INNER JOIN CompanyProcessor p ON c.company_code = p.company_code    
       INNER JOIN CompanyService ch ON c.company_code = ch.company_code    
       INNER JOIN CompanyTerm ct ON c.company_code = ct.company_code    
       INNER JOIN CompanyTerm_Locale ctl ON ct.term_id = ctl.term_id and ctl.CultureID = @cultureID    
      Where c.company_code = @company_code    
     END    
    
     IF @company_type = 'T' --TripAdvisor    
     BEGIN    
      INSERT INTO dbo.QueryTA (qryType) VALUES (1)    
    
      SELECT c.company_name, c.company_code, c.address, c.city, c.state, co.country_name,    
       c.latitude, c.longitude, c.web, c.email, c.telephone_nr1    
      FROM dbo.Company c INNER JOIN dbo.Country co ON c.country_iso3 = co.country_iso3    
      WHERE c.active = 1 AND c.tripAdvisor = 1    
     END    
    End    
  End    
    
 IF @mode = 'SE1'    
 BEGIN    
  SELECT @cultureID = ISNULL(CultureID,0) FROM dbo.Culture WHERE CultureName = @culture    
  IF @cultureID = 0    
   SET @cultureID = 1    
    
  SELECT c.res_email, c.res_email_alt, ctl.terms, ctl.waiver, c.company_name    
  FROM dbo.Company c INNER JOIN dbo.CompanyTerm ct ON c.company_code = ct.company_code INNER JOIN dbo.CompanyTerm_Locale ctl ON ct.term_id = ctl.term_id and ctl.CultureID = @cultureID    
  WHERE c.company_code = @company_code    
 END    
    
 If @mode = 'COM'    
  Select company_code, company_name    
  From dbo.Company    
  ORDER BY company_name    
    
 If @mode = 'QUO'    
  Select ch.max_adults, ch.max_children, ch.tax_perc, ch.children_free, c.res_email, c.res_email_alt    
  From dbo.Company c INNER JOIN dbo.CompanyHotel ch ON c.company_code = ch.company_code    
  Where c.company_code = @company_code    
  ORDER BY company_name    
    
 If @mode = 'EMA'    
  Select res_email, res_email_alt    
  From dbo.Company    
  Where company_code = @company_code    
    
 If @mode = 'GOG'    
  Select google_conv_id, google_conv_label    
  From dbo.Company    
  Where company_code = @company_code    
    
 If @mode = 'DAT'    
  Select company_name, res_email, telephone_nr1    
  From dbo.Company    
  Where company_code = @company_code    