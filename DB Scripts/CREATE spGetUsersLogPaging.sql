CREATE  proc [dbo].[spGetUsersLogPaging]      
  @user_id int,  
  @from  smalldatetime ,  
  @to   smalldatetime  ,    
  @PageNumber INT,        
  @PageSize INT = NULL,        
  @OrderBy VARCHAR(100) = NULL,        
  @IsAccending BIT = 1      
      
AS      
BEGIN      
      
  DECLARE @SQLString NVARCHAR(MAX),        
     @FilterString VARCHAR(MAX),         
     @ParmDefinition NVARCHAR(500)       
           
  DECLARE @cultureID int        
         
   IF (@PageSize IS NULL OR @PageSize = 0)        
   SET @PageSize = 20        
            
   IF (@OrderBy IS NULL OR @OrderBy = '')        
   SET @OrderBy = ' LogID  '        
           
   IF @IsAccending = 0        
   SET @OrderBy = @OrderBy + ' DESC'        
   ELSE        
   SET @OrderBy = @OrderBy + ' ASC'        
           
   SET  @FilterString = ''       
         
   SET @ParmDefinition = N'       
     @user_id int,   
     @from  smalldatetime ,  
     @to   smalldatetime  ,       
	 @PageNumber INT,        
     @PageSize INT = NULL          
   '       
          
        
 Set @SQLString=' WITH SQLPaging AS              
           
 (SELECT TOP ( @PageSize* @PageNumber )              
           
   ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),            
	  [LogID]
      ,[user_id]
      ,[ref_id]
      ,[event]
      ,[event_date]
  FROM  dbo.UsersLog
  WHERE   uc.company_code = @company_code and super_user = 0      
  ORDER BY ' + @OrderBy  + '             
           
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'              
           
           
           
    EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@PageNumber,@PageSize          
        
END      
         