CREATE Proc [dbo].[spGetAvailabilityPrices]  
 @company_code char(10), 
 @RoomType char(10),
 @CurrentDate   date
 
AS  
 
 BEGIN
 
 
	DECLARE @firstdate DATETIME
	DECLARE @lastdate DATETIME

	SET @firstdate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@CurrentDate)-1),@CurrentDate),101) 

	SET @lastdate= CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@CurrentDate))),DATEADD(mm,2,@CurrentDate)),101)
 
 
 
 SELECT [date],[adult1]
 FROM Availability av
 WHERE company_code =@company_code
	AND roomtype_code = @RoomType
	AND [date] BETWEEN @firstdate AND @lastdate


    
 END
  