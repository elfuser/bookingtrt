    
ALTER proc [dbo].[spAgency]        
 @mode  char(3),     
 @agency_code int,      
 @company_code char(10) = NULL,       
 @name varchar(100) = NULL,    
 @agency_type_code smallint = NULL,     
 @country varchar (100) = NULL,    
 @province varchar (100) = NULL,    
 @canton varchar(100) = NULL,    
 @district varchar(100) = NULL,    
 @address varchar(250) = NULL,    
 @phone varchar(50) = NULL,    
 @contact_name varchar(100) = NULL,    
 @contact_occupation varchar(100) = NULL,    
 @contact_email varchar(50) = NULL,    
 @contact_office_phone varchar(50) = NULL,    
 @contact_mobile_phone varchar(50) = NULL,    
 @notification_emails_reservations varchar(250) = NULL,    
 @active bit = NULL,    
 @available_credit bit = NULL ,
 @NumTarjeta varchar(50) = NULL, 
 @FecVencimiento varchar(50) = NULL, 
 @CodSeguridad varchar(50) = NULL, 
 @TipoTarjeta   varchar(50) = NULL 
       
AS        
        
 DECLARE @returnid int        
      
BEGIN TRY        
 IF @mode = 'INS'        
  BEGIN        
        
   INSERT INTO dbo.Agency (company_code, name, agency_type_code, country, province, canton, district, address, phone, contact_name, contact_occupation,    
        contact_email, contact_office_phone, contact_mobile_phone, notification_emails_reservations, active, 
        available_credit, created,NumTarjeta,FecVencimiento,CodSeguridad,TipoTarjeta)        
   VALUES (@company_code, @name, @agency_type_code, @country, @province, @canton, @district, @address, @phone, @contact_name, @contact_occupation,    
     @contact_email, @contact_office_phone, @contact_mobile_phone, @notification_emails_reservations, @active, 
     @available_credit, GETDATE(),@NumTarjeta,@FecVencimiento,@CodSeguridad,@TipoTarjeta)        
           
   SET @returnid = @@IDENTITY          
         
   SELECT @returnid           
  END        
          
 IF @mode = 'UPD'        
  BEGIN        
   UPDATE dbo.[Agency]     
   SET name = coalesce(@name,name), agency_type_code = coalesce(@agency_type_code,agency_type_code),    
   country = coalesce(@country,country), province = coalesce(@province,province), canton = coalesce(@canton,canton),        
   district = coalesce(@district,district), address = coalesce(@address,address),     
   phone = coalesce(@phone,phone), contact_name = coalesce(@contact_name,contact_name),     
   contact_occupation = coalesce(@contact_occupation,contact_occupation), contact_email = coalesce(@contact_email,contact_email),     
   contact_office_phone = coalesce(@contact_office_phone,contact_office_phone), contact_mobile_phone = coalesce(@contact_mobile_phone,contact_mobile_phone),     
   notification_emails_reservations = coalesce(@notification_emails_reservations,notification_emails_reservations), active = coalesce(@active,active),     
   available_credit = coalesce(@available_credit,available_credit),    
   modified = GETDATE() ,
   NumTarjeta=@NumTarjeta,
   FecVencimiento=@FecVencimiento,
   CodSeguridad=@CodSeguridad,
   TipoTarjeta=@TipoTarjeta      
   WHERE agency_code = @agency_code           
         
   SELECT 0        
  END        
          
 IF @mode = 'DEL'        
  BEGIN        
            
    DELETE FROM dbo.[Agency] WHERE agency_code = @agency_code         
          
    SELECT 0      
              
  END        
         
 IF @mode = 'SEL'        
  BEGIN         
        
   IF @agency_code = 0        
    SELECT *      
    FROM dbo.[Agency]    
 WHERE company_code = @company_code          
    ORDER BY created      
   ELSE        
    SELECT *        
    FROM dbo.[Agency]      
    WHERE agency_code = @agency_code           
  END   
    
  IF @mode = 'CNT'          
  BEGIN           
          
   IF @agency_code = 0          
    SELECT COUNT(*)        
    FROM dbo.[Agency]   
    WHERE company_code = @company_code                   
   ELSE          
    SELECT COUNT(*)          
    FROM dbo.[Agency]        
    WHERE agency_code = @agency_code                  
  END         
             
END TRY      
BEGIN CATCH         
  SELECT -1      
  print error_message()      
END CATCH 