ALTER  proc [dbo].[spGetServicesPaging]      
  @CompanyCode char(10)= '',    
  @PageNumber INT,        
  @PageSize INT = NULL,        
  @OrderBy VARCHAR(100) = NULL,        
  @IsAccending BIT = 1      
      
AS      
BEGIN      
      
  DECLARE @SQLString NVARCHAR(MAX),        
     @FilterString VARCHAR(MAX),         
     @ParmDefinition NVARCHAR(500)       
           
  DECLARE @cultureID int        
         
   IF (@PageSize IS NULL OR @PageSize = 0)        
   SET @PageSize = 20        
            
   IF (@OrderBy IS NULL OR @OrderBy = '')        
   SET @OrderBy = ' s.ServiceID  '        
           
   IF @IsAccending = 0        
   SET @OrderBy = @OrderBy + ' DESC'        
   ELSE        
   SET @OrderBy = @OrderBy + ' ASC'        
           
   SET  @FilterString = ''       
         
   SET @ParmDefinition = N'        
     @CompanyCode char(10),          
	 @PageNumber INT,        
     @PageSize INT = NULL          
   '       
          
        
 Set @SQLString=' WITH SQLPaging AS              
           
 (SELECT TOP ( @PageSize* @PageNumber )              
           
   ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),            
    s.Code, 
    s.Active, 
    s.ListOrder, 
    s.ServiceID, 
    s.VideoURL,
    s.CategoryID,
    s.Difficulty,
    S.ControlInventory,
    sl.Description ,
    sl.Details,
    sl.Conditions,
    sl.BringInfo
   FROM dbo.Service s 
		INNER JOIN dbo.Service_Locale sl
			ON s.ServiceID = sl.ServiceID and sl.CultureID = 1  
   WHERE s.CompanyCode = @CompanyCode      
  ORDER BY ' + @OrderBy  + '             
           
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'              
           
           
           
    EXECUTE sp_executesql @SQLString,@ParmDefinition,@CompanyCode,@PageNumber,@PageSize          
        
END      
         
         
  SELECT s.*, sl.Description, sl.Details 
  FROM dbo.Service s INNER JOIN dbo.Service_Locale sl
   ON s.ServiceID = sl.ServiceID and sl.CultureID = @cultureID  
  WHERE s.CompanyCode = @CompanyCode  
  ORDER by sl.Description  

  