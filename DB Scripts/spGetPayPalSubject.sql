USE [Booking]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateStatus]    Script Date: 9/5/2016 12:40:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spGetPayPalSubject]
	@company_Code	char(10)
as
BEGIN
	SELECT ISNULL(PayPalSubject,'') as PayPalSubject 
	FROM dbo.CompanyProcessor 
	WHERE company_code =  @company_Code
END

