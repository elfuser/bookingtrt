CREATE PROCEDURE [dbo].[spGetRoomTypes_V1]      
 (      
 @roomtype_code char(10) = '',      
 @company_code char(10) = '',      
 @culture  char(5)      
 )      
As      
       
BEGIN    
    
    
 Set nocount on      
 DECLARE @cultureID INT    
 DECLARE @today VARCHAR(10)      
     
 SET @today =  CONVERT(char(10), GetDate(),126)    
    
     
 IF LTRIM(RTRIM(@roomtype_code))=''    
 SET @roomtype_code=NULL    
      
 SELECT @cultureID = ISNULL(CultureID,0)     
 FROM dbo.Culture     
 WHERE CultureName = @culture      
     
 IF @cultureID = 0      
 SET @cultureID = 1      
      
 SELECT   
      rt.roomtype_id,  
   rt.roomtype_code,    
   rl.description,     
   rl.room_detail ,    
   rt.max_adults,    
   rt.max_children,    
   rt.max_occupance,    
   [dbo].[GetRoomPrice] (rt.company_code,rt.roomtype_code,@today) AS Price    
 From RoomType rt      
  INNER JOIN RoomType_Locale rl     
  ON (rt.roomtype_id = rl.roomtype_id AND rl.CultureID = @cultureID)     
 WHERE rt.roomtype_code = ISNULL(@roomtype_code,rt.roomtype_code)    
  AND rt.company_code = @company_code      
      
      
END    
      