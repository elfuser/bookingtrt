create proc [dbo].[spGetAPIUsersPaging] 
	 @PageNumber INT,  
	 @PageSize INT = NULL,  
	 @OrderBy VARCHAR(100) = NULL,  
	 @IsAccending BIT = 1
AS
BEGIN


	DECLARE @SQLString NVARCHAR(MAX),  
		   @FilterString VARCHAR(MAX),   
		   @ParmDefinition NVARCHAR(500) 
		   
				 
		 IF (@PageSize IS NULL OR @PageSize = 0)  
			SET @PageSize = 20  
				  
		 IF (@OrderBy IS NULL OR @OrderBy = '')  
			SET @OrderBy = '  a.[user_id]'  
		   
		 IF @IsAccending = 0  
			SET @OrderBy = @OrderBy + ' DESC'  
		 ELSE  
			SET @OrderBy = @OrderBy + ' ASC'  
		   
		 SET  @FilterString = '' 
		 
		 SET @ParmDefinition = N' 
		    @PageNumber INT,  
			@PageSize INT = NULL    
		 ' 
			 
		
	
Set @SQLString=' WITH SQLPaging AS        
    
(SELECT TOP ( @PageSize* @PageNumber )        
    
		ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),      
		a.user_id,a.company_code,a.password,a.active,a.last_activity,
		a.user_register,a.date_register,a.user_modified,a.date_modified
		FROM dbo.UserAPI a 				
 ORDER BY ' + @OrderBy  + '       
    
  )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'        
    
    
    
   EXECUTE sp_executesql @SQLString,@ParmDefinition,@PageNumber,@PageSize  

END		
			
