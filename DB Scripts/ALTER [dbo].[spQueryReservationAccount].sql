ALTER proc [dbo].[spQueryReservationAccount]  
 @DateFrom smalldatetime,  
 @DateTo  smalldatetime,  
 @Company_code char(10),  
 @Range  tinyint  
AS  
  
If @Range = 1  
 SELECT *,  
   CASE [status]   
   WHEN 1 THEN 'Not Processed'  
   WHEN 2 THEN 'Approved'  
   WHEN 3 THEN 'Rejected'  
   WHEN 4 THEN 'Card Error'  
   WHEN 5 THEN 'Qouted'  
  End AS [StatusText]  
 FROM dbo.vwReservationConfirmationAccount  
 WHERE company_code = @Company_code AND creation_date BETWEEN @DateFrom AND @DateTo  
 ORDER BY creation_date  
Else  
 SELECT *,  
   CASE [status]   
   WHEN 1 THEN 'Not Processed'  
   WHEN 2 THEN 'Approved'  
   WHEN 3 THEN 'Rejected'  
   WHEN 4 THEN 'Card Error'  
   WHEN 5 THEN 'Qouted'  
  End AS [StatusText]  
 FROM dbo.vwReservationConfirmationAccount  
 WHERE company_code = @Company_code AND arrival BETWEEN @DateFrom AND @DateTo  
 ORDER BY arrival  
  
  
  