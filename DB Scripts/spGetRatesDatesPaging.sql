ALTER  proc [dbo].[spGetRatesDatesPaging]    
  @rate_id  int,   
  @PageNumber INT,      
  @PageSize INT = NULL,      
  @OrderBy VARCHAR(100) = NULL,      
  @IsAccending BIT = 1    
    
AS    
BEGIN    
    
  DECLARE @SQLString NVARCHAR(MAX),      
     @FilterString VARCHAR(MAX),       
     @ParmDefinition NVARCHAR(500)     
         
  
   IF (@PageSize IS NULL OR @PageSize = 0)      
   SET @PageSize = 20      
          
   IF (@OrderBy IS NULL OR @OrderBy = '')      
   SET @OrderBy = '  ratedet_id '      
         
   IF @IsAccending = 0      
   SET @OrderBy = @OrderBy + ' DESC'      
   ELSE      
   SET @OrderBy = @OrderBy + ' ASC'      
         
   SET  @FilterString = ''     
       
   SET @ParmDefinition = N'     
      @rate_id char(10),      
   @PageNumber INT,      
   @PageSize INT = NULL        
   '     
        
      
 Set @SQLString=' WITH SQLPaging AS            
         
 (SELECT TOP ( @PageSize* @PageNumber )            
     ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + ')          
          ,[ratedet_id]  
    ,[rate_id]  
    ,[start_date]  
    ,[end_date]  
    ,[adult1]  
    ,[adult2]  
    ,[adult3]  
    ,[adult4]  
    ,[adult5]  
    ,[adult6]  
    ,[adult7]  
    ,[adult8]  
    ,[adult9]  
    ,[adult10]  
    ,[adult11]  
    ,[adult12]  
    ,[child]  
    ,[wadult1]  
    ,[wadult2]  
    ,[wadult3]  
    ,[wadult4]  
    ,[wadult5]  
    ,[wadult6]  
    ,[wadult7]  
    ,[wadult8]  
    ,[wadult9]  
    ,[wadult10]  
    ,[wadult11]  
    ,[wadult12]  
    ,[wchild]  
    ,[created]  
    ,[modified]  
    , isnull((Select description From RoomType_Locale a Inner Join RateRoomType b on a.roomtype_id = b.roomtype_id Where b.ratedet_id = c.ratedet_id and a.CultureID = 1),''** Not assigned **'') as room_name    
    ,isnull((Select roomtype_code From RoomType a Inner Join RateRoomType b on a.roomtype_id = b.roomtype_id Where b.ratedet_id = c.ratedet_id),null) as roomtype_code     
  FROM [dbo].[RateDetail]  as c
  Where rate_id = @rate_id    
  ORDER BY ' + @OrderBy  + '           
         
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'            
         
         

         
    EXECUTE sp_executesql @SQLString,@ParmDefinition,@rate_id,@PageNumber,@PageSize        
      
END    
       
             