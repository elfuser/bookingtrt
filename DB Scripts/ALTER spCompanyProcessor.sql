ALTER PROCEDURE [dbo].[spCompanyProcessor]  
 (   
 @mode    char(3),  
 @company_code  char(10),  
 @commission   money = 0,  
 @processorID  varchar(50) = '',  
 @keyID    varchar(10) = '',  
 @keyHash   varchar(50) = '',  
 @type    varchar(5) = '',  
 @URL    varchar(200) = '',  
 @userName   varchar(30) = ''  
 )  
AS  
  
 Set nocount on  
 
  If @mode = 'INS'  
 BEGIN
	IF EXISTS( SELECT 1
				FROM CompanyProcessor
				WHERE company_code = @company_code)
	BEGIN
		SET @mode = 'UPD'  
	END 
 END 
   
 If @mode = 'INS'  
   
  Begin  
    
   Insert into CompanyProcessor  
   (company_code, credomaticPorcCom, processorID, KeyID, KeyHash, type, URL, UserName,  
   credomatic, credomaticProcID, processorIDAmex, bancoNacional, BNProcID, IDACQUIRER, IDCOMMERCE, PayPalID)   
   Values  
   (@company_code, @commission, @processorID, @keyID, @keyHash, @type, @URL, @userName,  
   1, 3, '', 0, 0, 0, 0, 0)  
  
  End  
    
 If @mode = 'UPD'  
   
  Begin   
    Update CompanyProcessor  
    Set credomaticPorcCom = @commission, processorID = @processorID, KeyID = @keyID, KeyHash = @keyHash,  
     type = @type, URL = @URL, UserName = @userName  
    Where  
     company_code = @company_code  
  End  
  
  
 If @mode = 'SEL'   
  Select *  
  From CompanyProcessor  
  Where company_code = @company_code  
  
  
  
  
  