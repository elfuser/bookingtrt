CREATE PROC [dbo].[spUsersCount]
	 @company_code	char(10) = ''

AS
BEGIN


     SELECT COUNT(U.user_id) 
     FROM [User] u 
		INNER JOIN Profile c 
			ON  (u.profile_id = c.profile_id )
		INNER JOIN UserCompany uc
			ON  (u.user_id = uc.user_id)  
    WHERE uc.company_code = @company_code

	 
 
END