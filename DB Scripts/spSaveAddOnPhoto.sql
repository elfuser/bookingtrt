CREATE proc [dbo].[spSaveAddOnPhoto]    
 @AddOn_id BIGINT ,
 @numPhoto tinyint,
 @Image as image
AS    
    
BEGIN   
   
   IF @numPhoto=1
   BEGIN
		IF EXISTS (SELECT AddonID    
					FROM dbo.AddonPhoto  
					WHERE AddonID = @AddOn_id ) 
		BEGIN
			UPDATE dbo.AddonPhoto  SET photo1=@Image
			WHERE AddonID = @AddOn_id
		END
			ELSE
				BEGIN
						INSERT INTO [dbo].[AddonPhoto]
						   ([AddonID]
						   ,[photo1]
						   ,[photo2]
						   ,[photo3])
						VALUES
						   (@AddOn_id
						   ,@Image
						   ,NULL
						   ,NULL)
				END 
   END 
   
   
   IF @numPhoto=2
   BEGIN
		IF EXISTS (SELECT [AddonID]    
					FROM dbo.AddonPhoto  
					WHERE AddonID = @AddOn_id ) 
		BEGIN
			UPDATE dbo.AddonPhoto  SET photo2=@Image
			WHERE AddonID = @AddOn_id
		END
			ELSE
				BEGIN
						INSERT INTO [dbo].[AddonPhoto]
						   ([AddonID]
						   ,[photo1]
						   ,[photo2]
						   ,[photo3])
						VALUES
						   (@AddOn_id
						   ,NULL
						   ,@Image
						   ,NULL)
				END 
   END 
    
END 