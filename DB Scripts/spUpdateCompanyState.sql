USE [Booking]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateStatus]    Script Date: 9/5/2016 12:40:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[spUpdateCompanyState]
	@company_Code	char(10),
	@active tinyint
as
BEGIN
	UPDATE dbo.Company SET active = @active
	WHERE company_code = @company_Code
END

