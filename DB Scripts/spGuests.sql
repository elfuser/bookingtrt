-- =============================================    
-- Author:  Cesar M. Varela V.    
-- Create date: 05/12/2009    
-- Description: Insert, Update, Delete And Select records from the table tblGuests    
-- =============================================    
    
ALTER PROCEDURE [dbo].[spGuests]    
 (    
 @mode char(3),     
 @guest_id   bigint = 0,    
 @name    varchar(30) = '',    
 @lastname   varchar(30) = '',    
 @company_code  char(10),    
 @creation_date  date = '',    
 @telephone_nr  varchar(15) = '',    
 @country_iso3  char(3) = '',    
 @email    varchar(50) = ''    
 )    
As    
     
 Set nocount on    
 Declare @Cant int  = 0  
     
 --comment by William Fallas, not neccesary .. guest_id =  identity  
  SELECT @Cant = count(*) FROM Guest WHERE guest_id = @guest_id    
    
 If @mode = 'INS'    
  IF @Cant = 0    
   Begin    
       
    /*Set @guest_id = (Select guest_nr From tblCompanies Where company_code = @company_code) + 1    
    Update tblCompanies Set guest_nr = @guest_id*/    
         
    Insert into Guest    
     (name, lastname, company_code, creation_date,      
      telephone_nr, country_iso3, email)    
    Values    
    (@name, @lastname, @company_code, @creation_date,      
     @telephone_nr, @country_iso3, @email )    
        
    Select @@IDENTITY    
        
   End    
  ELSE    
   Begin    
       
    Update Guest    
    Set name = @name, lastname = @lastname, company_code = @company_code,    
     creation_date = @creation_date,     
     telephone_nr = @telephone_nr, country_iso3 = @country_iso3,    
     email = @email    
    Where    
     guest_id = @guest_id    
       
    Select @guest_id    
    
   End     
    
 If @mode = 'DEL'    
     
  Begin    
      
   Delete From Guest    
   Where guest_id = @guest_id    
      
  End    
    
 If @mode = 'SEL'   -- No se aplica el concepto de seleccionar todos los registros porque no tiene uso    
     
  Begin    
      
   Select * From Guest    
   Where company_code = @company_code    
      
  End    
    