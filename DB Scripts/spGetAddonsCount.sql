USE [DB_A0683E_BookingEngine]
GO
/****** Object:  StoredProcedure [dbo].[spAddons]    Script Date: 10/27/2016 16:34:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[spGetAddonsPagingCount]
	 @company_code	char(10) = '',
	 @culture		char(5) = ''

AS
BEGIN
       
        DECLARE @cultureID int
 
	 	IF @culture = '' or @culture is null
			SET @cultureID = 1
		ELSE
			SELECT @cultureID = ISNULL(CultureID,0) 
			FROM dbo.Culture
		    WHERE CultureName = @culture

		IF @cultureID = 0
			SET @cultureID = 1


		SELECT COUNT(a.addonID)
		FROM dbo.Addon a 
				INNER JOIN dbo.Addon_Locale al 
					ON a.addonID = al.addonID 
   		 WHERE a.company_code = @company_code
   			AND  al.CultureID=@cultureID
		   
	 
 
END

		
   						
	
