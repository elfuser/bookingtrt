
CREATE PROC [dbo].[GetFreeNights]
(
		@company_code	char(10),
		@rate_code	char(10)
)
AS

BEGIN

SELECT free_nights 
FROM dbo.RateHeader 
WHERE company_code = @company_code
 AND rate_code = @rate_code 
 
 
END
GO
