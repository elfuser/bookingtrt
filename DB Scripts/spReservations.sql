ALTER PROCEDURE [dbo].[spReservations]  
 (  
 @mode    char(3),  
 @reservation_id  char(10),  
 @guest_id   bigint = 0,  
 @company_code  char(10) = '',  
 @arrival   date = '',  
 @departure   date = '',  
 @creation_date  date = '',  
 @cancellation_date date = '',  
 @rate_code   char(10) = '',  
 @rate_amount  money = 0,  
 @tax_amount   money = 0,  
 @ccard_transid  int = 0,  
 @remarks   text = '',  
 @visit_reason  char(100) = '',  
 @promo_id   smallint = 0,  
 @addon_amount  money = 0,  
 @promo_amount  money = 0,  
 @disc_amount  money = 0,  
 @processor   char(2) = '',  
 @rooms    tinyint = 0,  
 @origin    integer = 0,  
 @resellerID   varchar(15) = '',  
 @sessionID   uniqueidentifier = null  
 )  
As  
   
 Set nocount on  
 DECLARE @processorID smallint  
 DECLARE @commissionOL money, @commissionBank money, @credomaticFund money  
  
 If @mode = 'INS'  
  --IF @Cant = 0  
   Begin  
    Declare @reservation_nr as bigint  
  
    --Generar Numero de Reservacion  
    Set @reservation_nr = (Select reservation_nr From CompanyHotel Where company_code = @company_code) + 1  
    Update CompanyHotel Set reservation_nr = @reservation_nr Where company_code = @company_code  
    Set @reservation_id = (Select rtrim(ltrim(confirm_prefix)) From CompanyHotel Where company_code = @company_code) + CAST(@reservation_nr as char(7))  
      
    --Comisiones  
    IF @processor = 'CR' --Credomatic  
      SELECT @processorID = credomaticProcID,   
      @commissionOL = @rate_amount * credomaticOLPorcCom / 100,   
      @commissionBank = @rate_amount * credomaticPorcCom / 100,  
      @credomaticFund = @rate_amount * credomaticFund / 100  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE IF @processor = 'BN'  
      SELECT @processorID = BNProcID,   
      @commissionOL = @rate_amount * BNOLPorcCom / 100, @commissionBank = 0, @credomaticFund = 0   
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE IF @processor = 'PP'  
      SELECT @processorID = PayPalID,   
      @commissionOL = @rate_amount * PayPalPorcCom / 100, @commissionBank = 0, @credomaticFund = 0  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE  
      SELECT @processorID = credomaticProcID,   
      @commissionOL = @rate_amount * credomaticOLPorcCom / 100,   
      @commissionBank = 0,  
      @credomaticFund = @rate_amount * credomaticFund / 100  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    --Guardar Reserva  
    Insert into Reservation  
     (reservation_id, guest_id, company_code, arrival, departure, creation_date, cancellation_date, rate_code,   
      ccard_transid, remarks, visit_reason, promo_id, rate_amount, tax_amount, deposit_number, transaction_bank,  
      addon_amount, promo_amount, commissionOL, commissionBank, credomaticFund, processorID, rooms,  
      origin, resellerID, disc_amount)  
    Values  
     (@reservation_id, @guest_id, @company_code, @arrival, @departure, @creation_date, @cancellation_date, @rate_code,   
      @ccard_transid, @remarks, @visit_reason, @promo_id, @rate_amount, @tax_amount, '', '',   
      @addon_amount, @promo_amount, @commissionOL, @commissionBank, @credomaticFund, @processorID, @rooms,  
      @origin, @resellerID, @disc_amount)  
        
    Update dbo.SessionData SET reservationID = @reservation_id  
    Where SessionID = @SessionID  
  
    Select @reservation_id  

   End  
  
    
 If @mode = 'DEL'  
   
  Begin  
    
   Delete From Reservation  
   Where  reservation_id = @reservation_id  
    
  End  
  
 If @mode = 'SEL'  
   
  Begin  
    
   If @company_code = 0  
     
    Begin    
     Select * From Reservation  
     Where reservation_id = @reservation_id  
    End  
     
   Else  
     
    Begin    
     Select * From Reservation  
     Where company_code = @company_code  
    End  
        
  End  
  
 If @mode = 'RES'  
  Select * FROM dbo.vwReservation  
  Where reservation_id = @reservation_id  
  
  
  
  
  