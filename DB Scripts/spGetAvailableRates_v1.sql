CREATE PROCEDURE [dbo].[spGetAvailableRates_v1]   
 (   
 @company_code char(10),  
 @room_type char(10), 
 @arrival  datetime = '',  
 @departure  datetime = '',  
 @adults   int = 1,  
 @children  int = 0,  
 @culture  char(5)    
 )   
AS  
  
Set dateformat 'dmy'  
Set nocount on  
  
Declare @stay as int  
Declare @tax_perc as money  
Declare @tax_included as bit  
Declare @min_value as money  
Declare @cultureID int  
  
Set @stay = datediff(DAY,@arrival,@departure)  
  
-- Obtencion del porcentaje de impuestos  
Set @tax_perc = (Coalesce((Select tax_perc From CompanyHotel Where company_code = @company_code), 0.00) / 100) + 1  
--Set @tax_included = (Select tax_included From Company Where company_code = @company_code)  
  
SELECT @cultureID = ISNULL(CultureID,0) FROM dbo.Culture WHERE CultureName = @culture  
  
IF @cultureID = 0  
 SET @cultureID = 1  
  
-- Selecccion de tarifas (normales y paquete) disponibles en el rango de fechas  
  
If @tax_perc = 0  -- Se incluye el impuesto de venta hotel en el total  
  
 Begin  
   
 Select distinct av.rate_code, rl.description,   
 (Case @adults  
  When 1 Then (MIN(av.adult1) + MIN(av.child * @children)) * @tax_perc  
  When 2 Then (MIN(av.adult2) + MIN(av.child * @children)) * @tax_perc  
  When 3 Then (MIN(av.adult3) + MIN(av.child * @children)) * @tax_perc  
  When 4 Then (MIN(av.adult4) + MIN(av.child * @children)) * @tax_perc  
  When 5 Then (MIN(av.adult5) + MIN(av.child * @children)) * @tax_perc  
  When 6 Then (MIN(av.adult6) + MIN(av.child * @children)) * @tax_perc  
  When 7 Then (MIN(av.adult7) + MIN(av.child * @children)) * @tax_perc  
  When 8 Then (MIN(av.adult8) + MIN(av.child * @children)) * @tax_perc  
  When 9 Then (MIN(av.adult9) + MIN(av.child * @children)) * @tax_perc  
  When 10 Then (MIN(av.adult10) + MIN(av.child * @children)) * @tax_perc  
  When 11 Then (MIN(av.adult11) + MIN(av.child * @children)) * @tax_perc  
  When 12 Then (MIN(av.adult12) + MIN(av.child * @children)) * @tax_perc  
  Else 0.00  
 End) as min_value,   
 rh.type, rl.rate_detail, rh.start_date, rh.end_date, rh.min_los, rh.max_los, rh.roworder, dbo.GetRateDiscount(@company_code, rh.rate_code, @arrival, @departure, @stay) as discount  
 From Availability av Inner Join RateHeader rh on av.rate_code = rh.rate_code and av.company_code = rh.company_code  
 Inner Join dbo.RateHeader_Locale rl on rh.rate_id = rl.rate_id and rl.CultureID = @cultureID  
 Inner Join dbo.RoomType rt ON rt.company_code = av.company_code and rt.roomtype_code = av.roomtype_code  
 Inner Join dbo.AvailabilityRoom ar on av.roomtype_code = ar.roomtype_code and av.company_code = ar.company_code and av.date = ar.date  
 Where av.company_code =  @company_code  and av.roomtype_code = @room_type
 And av.date Between @arrival and @departure - 1  and ar.rooms_avail > 0 
 and rh.enabled = 1 and rt.max_occupance >= (@adults+@children) and  
 (Case @adults  
  When 1 Then av.adult1  
  When 2 Then av.adult2  
  When 3 Then av.adult3  
  When 4 Then av.adult4   
  When 5 Then av.adult5  
  When 6 Then av.adult6  
  When 7 Then av.adult7  
  When 8 Then av.adult8  
  When 9 Then av.adult9   
  When 10 Then av.adult10   
  When 11 Then av.adult11  
  When 12 Then av.adult12  
  Else 0.00  
 End) > 0   
 Group By av.rate_code, rl.description,  rh.type, rl.rate_detail, rh.start_date, rh.end_date, rh.min_los, rh.max_los, rh.roworder, dbo.GetRateDiscount(@company_code, rh.rate_code, @arrival, @departure, @stay)  
 Having  
 (Case @adults  
  When 1 Then (MIN(av.adult1) + MIN(av.child * @children)) * @tax_perc  
  When 2 Then (MIN(av.adult2) + MIN(av.child * @children)) * @tax_perc  
  When 3 Then (MIN(av.adult3) + MIN(av.child * @children)) * @tax_perc  
  When 4 Then (MIN(av.adult4) + MIN(av.child * @children)) * @tax_perc  
  When 5 Then (MIN(av.adult5) + MIN(av.child * @children)) * @tax_perc  
  When 6 Then (MIN(av.adult6) + MIN(av.child * @children)) * @tax_perc  
  When 7 Then (MIN(av.adult7) + MIN(av.child * @children)) * @tax_perc  
  When 8 Then (MIN(av.adult8) + MIN(av.child * @children)) * @tax_perc  
  When 9 Then (MIN(av.adult9) + MIN(av.child * @children)) * @tax_perc  
  When 10 Then (MIN(av.adult10) + MIN(av.child * @children)) * @tax_perc  
  When 11 Then (MIN(av.adult11) + MIN(av.child * @children)) * @tax_perc  
  When 12 Then (MIN(av.adult12) + MIN(av.child * @children)) * @tax_perc  
  Else 0.00  
 End) > 0   
 Order by rh.roworder  
   
 End  
  
Else     -- No se incluye el impuesto de venta hotel en el total  
  
 Begin  
   
 Select distinct av.rate_code, rl.description,   
 (Case @adults  
  When 1 Then (MIN(av.adult1) + MIN(av.child * @children))   
  When 2 Then (MIN(av.adult2) + MIN(av.child * @children))  
  When 3 Then (MIN(av.adult3) + MIN(av.child * @children))   
  When 4 Then (MIN(av.adult4) + MIN(av.child * @children))  
  When 5 Then (MIN(av.adult5) + MIN(av.child * @children))   
  When 6 Then (MIN(av.adult6) + MIN(av.child * @children))  
  When 7 Then (MIN(av.adult7) + MIN(av.child * @children))  
  When 8 Then (MIN(av.adult8) + MIN(av.child * @children))   
  When 9 Then (MIN(av.adult9) + MIN(av.child * @children))   
  When 10 Then (MIN(av.adult10) + MIN(av.child * @children))   
  When 11 Then (MIN(av.adult11) + MIN(av.child * @children))   
  When 12 Then (MIN(av.adult12) + MIN(av.child * @children))   
  Else 0.00  
 End) as min_value,   
 rh.type, rl.rate_detail, rh.start_date, rh.end_date, rh.min_los, rh.max_los, rh.roworder, dbo.GetRateDiscount(@company_code, rh.rate_code, @arrival, @departure, @stay) as discount  
 From Availability av Inner Join RateHeader rh on av.rate_code = rh.rate_code and av.company_code = rh.company_code  
 Inner Join dbo.RateHeader_Locale rl on rh.rate_id = rl.rate_id and rl.CultureID = @cultureID  
 Inner Join dbo.RoomType rt ON rt.company_code = av.company_code and rt.roomtype_code = av.roomtype_code  
 Inner Join dbo.AvailabilityRoom ar on av.roomtype_code = ar.roomtype_code and av.company_code = ar.company_code and av.date = ar.date  
 Where av.company_code =  @company_code   and av.roomtype_code = @room_type
	And av.date Between @arrival and @departure - 1 and ar.rooms_avail > 0 
	and rh.enabled = 1 and rt.max_occupance >= (@adults+@children) and  
 (Case @adults  
  When 1 Then av.adult1  
  When 2 Then av.adult2  
  When 3 Then av.adult3  
  When 4 Then av.adult4   
  When 5 Then av.adult5  
  When 6 Then av.adult6  
  When 7 Then av.adult7  
  When 8 Then av.adult8  
  When 9 Then av.adult9   
  When 10 Then av.adult10   
  When 11 Then av.adult11  
  When 12 Then av.adult12  
  Else 0.00  
 End) > 0  
 Group By av.rate_code, rl.description, rh.type, rl.rate_detail, rh.start_date, rh.end_date, rh.min_los, rh.max_los, rh.roworder, dbo.GetRateDiscount(@company_code, rh.rate_code, @arrival, @departure, @stay)  
 Having  
 (Case @adults  
  When 1 Then (MIN(av.adult1) + MIN(av.child * @children))   
  When 2 Then (MIN(av.adult2) + MIN(av.child * @children))  
  When 3 Then (MIN(av.adult3) + MIN(av.child * @children))   
  When 4 Then (MIN(av.adult4) + MIN(av.child * @children))  
  When 5 Then (MIN(av.adult5) + MIN(av.child * @children))   
  When 6 Then (MIN(av.adult6) + MIN(av.child * @children))  
  When 7 Then (MIN(av.adult7) + MIN(av.child * @children))  
  When 8 Then (MIN(av.adult8) + MIN(av.child * @children))   
  When 9 Then (MIN(av.adult9) + MIN(av.child * @children))   
  When 10 Then (MIN(av.adult10) + MIN(av.child * @children))   
  When 11 Then (MIN(av.adult11) + MIN(av.child * @children))   
  When 12 Then (MIN(av.adult12) + MIN(av.child * @children))   
  Else 0.00  
 End) > 0   
 Order by rh.roworder  
    
 End  
  
  
  
  
  
  
  