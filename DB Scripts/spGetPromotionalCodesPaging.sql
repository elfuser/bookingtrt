create proc [dbo].[spGetPromotionalCodesPaging]   
  @company_code char(10) = '',  
  @PageNumber INT,    
  @PageSize INT = NULL,    
  @OrderBy VARCHAR(100) = NULL,    
  @IsAccending BIT = 1  
AS  
BEGIN  
  
  
 DECLARE @SQLString NVARCHAR(MAX),    
     @FilterString VARCHAR(MAX),     
     @ParmDefinition NVARCHAR(500)   
       
       
   IF (@PageSize IS NULL OR @PageSize = 0)    
   SET @PageSize = 20    
        
   IF (@OrderBy IS NULL OR @OrderBy = '')    
   SET @OrderBy = '  a.company_code '    
       
   IF @IsAccending = 0    
   SET @OrderBy = @OrderBy + ' DESC'    
   ELSE    
   SET @OrderBy = @OrderBy + ' ASC'    
       
   SET  @FilterString = ''   
     
   SET @ParmDefinition = N'   
   @company_code char(10),   
      @PageNumber INT,    
   @PageSize INT = NULL      
   '   
      
    
   
Set @SQLString=' WITH SQLPaging AS          
      
(SELECT TOP ( @PageSize* @PageNumber )          
      
  ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),        
  a.promo_id,a.promo_code,a.description,a.type,a.company_code,a.start_date,a.end_date,a.active  
  FROM [dbo].[PromotionalCode] a    
  WHERE a.company_code = @company_code     
  ORDER BY ' + @OrderBy  + '         
      
  )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'          
      
     

   EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@PageNumber,@PageSize    
  
END    
     