   
 CREATE PROC GetFeaturesByRoom
 (
	@company_code char(10),
	@roomtype_id int
 )
 AS
 
 BEGIN
 
    CREATE   TABLE #tmpFeatures (feature_id smallint,description varchar(50),checked bit)
   
    INSERT INTO #tmpFeatures (feature_id,description,checked)
    Select f.feature_id, fl.description, 0 as checked
    From Feature f 
		INNER JOIN Feature_Locale fl 
			ON(f.feature_id = fl.feature_id and fl.CultureID=1) 
    Where company_code = @company_code 
    Order by fl.description  
    
    
    UPDATE #tmpFeatures SET checked = 1
    From FeaturesRoomType f  
		Inner join #tmpFeatures r 
			on ( f.feature_id = r.feature_id) 
   Where F.roomtype_id = @roomtype_id  
   
       
    SELECT * FROM #tmpFeatures
    
END
    
    
