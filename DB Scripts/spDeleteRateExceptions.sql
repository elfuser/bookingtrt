CREATE PROC [dbo].[spDeleteRateExceptions]  
 @RowId  int,  
 @company_code char(10),  
 @rate_code  char(10),  
 @roomtype_code char(10) = '',  
 @start_date  date = '',  
 @end_date  date = '' 


AS  
  
 DECLARE @proc_date smalldatetime  
 DECLARE @roomtype_id int  
 DECLARE @ratedet_id int  
 DECLARE @adult1   money = 0,  
		 @adult2   money = 0,  
		 @adult3   money = 0,  
		 @adult4   money = 0,  
		 @adult5   money = 0,  
		 @adult6   money = 0,  
		 @adult7   money = 0,  
		 @adult8   money = 0,  
		 @adult9   money = 0,  
		 @adult10  money = 0,  
		 @adult11  money = 0,  
		 @adult12  money = 0,  
		 @child   money = 0  
   
   DELETE FROM dbo.RateException  
   Where RowId = @RowId 
  
   SELECT @roomtype_id = roomtype_id FROM dbo.RoomType WHERE company_code = @company_code AND roomtype_code = @roomtype_code  
   Set @proc_date = @start_date  
  
   While @proc_date < @end_date  
   Begin  
    SELECT @ratedet_id = a.ratedet_id FROM dbo.RateRoomType a INNER JOIN dbo.RateDetail b ON a.ratedet_id = b.ratedet_id  
    WHERE @proc_date BETWEEN b.start_date and b.end_date AND a.roomtype_id = @roomtype_id    
     
    SELECT @adult1 = adult1, @adult2 = adult2, @adult3 = adult3, @adult4 = adult4, @adult5 = adult5,  
    @adult6 = adult6, @adult7 = adult7, @adult8 = adult8, @adult9 = adult9, @adult10 = adult10,  
    @adult11 = adult11, @adult12 = adult12, @child = child  FROM dbo.RateDetail   
    WHERE ratedet_id = @ratedet_id  
     
    Update Availability Set adult1 = @adult1, adult2 = @adult2, adult3 = @adult3, adult4 = @adult4, adult5 = @adult5, adult6 = @adult6,  
     adult7 = @adult7, adult8 = @adult8, adult9 = @adult9, adult10 = @adult10, adult11 = @adult11, adult12 = @adult12, child = @child  
    Where company_code = @company_code and rate_code = @rate_code and roomtype_code = @roomtype_code and date = @proc_date  
      
    Set @proc_date = @proc_date + 1  
    Continue  
   End   
     


  