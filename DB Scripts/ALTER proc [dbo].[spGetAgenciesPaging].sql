ALTER proc [dbo].[spGetAgenciesPaging]        
  @company_code char(10) = '',        
  @culture  char(5) = '',        
  @PageNumber INT,          
  @PageSize INT = NULL,          
  @OrderBy VARCHAR(100) = NULL,          
  @IsAccending BIT = 1        
        
AS        
BEGIN        
        
  DECLARE @SQLString NVARCHAR(MAX),          
     @FilterString VARCHAR(MAX),           
     @ParmDefinition NVARCHAR(500)         
             
  DECLARE @cultureID int          
           
   IF (@PageSize IS NULL OR @PageSize = 0)          
   SET @PageSize = 20          
              
   IF (@OrderBy IS NULL OR @OrderBy = '')          
   SET @OrderBy = ' agency_code '          
             
   IF @IsAccending = 0          
   SET @OrderBy = @OrderBy + ' DESC'          
   ELSE          
   SET @OrderBy = @OrderBy + ' ASC'          
             
   SET  @FilterString = ''         
           
   SET @ParmDefinition = N'         
      @company_code char(10),         
      @cultureID int,        
   @PageNumber INT,          
   @PageSize INT = NULL            
   '         
           
           
    IF @culture = '' or @culture is null        
    SET @cultureID = 1        
   ELSE        
    SELECT @cultureID = ISNULL(CultureID,0)         
    FROM dbo.Culture        
       WHERE CultureName = @culture        
        
   IF @cultureID = 0        
    SET @cultureID = 1        
        
         
Set @SQLString=' WITH SQLPaging AS                
            
(SELECT TOP ( @PageSize* @PageNumber )                
            
  ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),              
  [agency_code]  
      ,[company_code]  
      ,[name]  
      ,[agency_type_code]  
      ,[country]  
      ,[province]  
      ,[canton]  
      ,[district]  
      ,[address]  
      ,[phone]  
      ,[contact_name]  
      ,[contact_occupation]  
      ,[contact_email]  
      ,[contact_office_phone]  
      ,[contact_mobile_phone]  
      ,[notification_emails_reservations]  
      ,isnull(active,0) as [active]  
      ,isnull(available_credit,0) as [available_credit]  
      ,[created]  
      ,[modified]   
      ,NumTarjeta
      ,FecVencimiento
      ,CodSeguridad
      ,TipoTarjeta
  FROM Agency   
  WHERE company_code = @company_code         
 ORDER BY ' + @OrderBy  + '               
            
  )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'                
            
            
            
   EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@cultureID,@PageNumber,@PageSize            
         
END        