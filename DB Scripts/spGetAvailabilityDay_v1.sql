CREATE Proc [dbo].[spGetAvailabilityDay_v1]  
 @company_code char(10),  
 @CurrentDate   date
 
AS  
 
 BEGIN
 
 
	DECLARE @firstdate DATETIME
	DECLARE @lastdate DATETIME

	SET @firstdate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@CurrentDate)-1),@CurrentDate),101) 

	SET @lastdate= CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@CurrentDate))),DATEADD(mm,2,@CurrentDate)),101)
 
	 SELECT [date] as [Day],SUM([rooms_avail]) as Qty  
	 FROM AvailabilityRoom  
	 where company_code=@company_code AND date between @firstdate and @lastdate   
	   Group by [date]  
		Having sum(rooms_avail) <= 0  
		Order by [date]  
    
 END
  