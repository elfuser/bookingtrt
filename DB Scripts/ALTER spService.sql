ALTER proc [dbo].[spService]  
 @Mode char(3),  
 @CompanyCode char(10)= '',  
 @ServiceID  int = 0,  
 @CategoryID  int = 0,  
 @Code   varchar(10) = '',  
 @Description varchar(100) = '',  
 @Details  varchar(500) = '',  
 @ListOrder  tinyint = 0,  
 @VideoURL  varchar(200) = '',  
 @Difficulty  tinyint = 0,  
 @Active   bit = 0,  
 @Culture  char(5) = '',  
 @Conditions  text = '',  
 @BringInfo  text = '',  
 @Culture_id  int = 0,  
 @PictureFile varchar(50) = '',  
 @PictureType tinyint = 0,  
 @Inventory  bit = 0  
AS  
  
 Declare @cultureID as int  
 SELECT @cultureID = ISNULL(CultureID,0) FROM dbo.Culture WHERE CultureName = @Culture  
 IF @cultureID = 0  
  SET @cultureID = 1  
  
IF @Mode = 'INS'  
 BEGIN  
  DECLARE @id int  
  INSERT INTO [dbo].[Service]  
      ([CompanyCode]  
      ,[CategoryID]  
      ,[Code]  
      ,[ListOrder]  
      ,[VideoURL]  
      ,[Difficulty]  
      ,[Active]  
      ,[Picture1]  
      ,[Picture2]  
      ,[Picture3]  
      ,[ControlInventory]  
      )  
   VALUES  
      (@CompanyCode, @CategoryID, @Code, @ListOrder, @VideoURL, @Difficulty, @Active,'','','',@Inventory)  
  
  SELECT @id = @@IDENTITY  
  
   INSERT INTO dbo.Service_Locale (ServiceID, CultureID, Description, Details, Conditions, BringInfo)  
   VALUES (@@IDENTITY, @cultureID, @Description, @Details, '', '')  
     
   SELECT @id  
 END  
  
IF @Mode = 'UPD'  
 BEGIN  
  UPDATE dbo.Service SET CategoryID = @CategoryID, Code = @Code, ListOrder = @ListOrder, VideoURL = @VideoURL, Difficulty = @Difficulty, Active = @Active, ControlInventory = @Inventory  
  WHERE ServiceID = @ServiceID  
  
  UPDATE dbo.Service_Locale SET Description = @Description, Details = @Details  
  WHERE ServiceID = @ServiceID and CultureID = @cultureID  
 END  
  
IF @Mode = 'CON'  
begin
 if exists (select ServiceID from dbo.Service_Locale WHERE ServiceID = @ServiceID and CultureID = @culture_id  )
 begin
	 UPDATE dbo.Service_Locale SET Conditions = @Conditions  
	 WHERE ServiceID = @ServiceID and CultureID = @culture_id  
 end
	else
		begin
			insert into dbo.Service_Locale(ServiceID,CultureID,Description,Details,Conditions,BringInfo)
			values (@ServiceID,@culture_id,'','',@Conditions,'')
		end
end
  
IF @Mode = 'PIC'  
 IF @PictureType = 1 --Small Picture  
  UPDATE dbo.Service SET Picture1 = @PictureFile  
  WHERE ServiceID = @ServiceID   
 ELSE IF @PictureType = 2  
  UPDATE dbo.Service SET Picture2 = @PictureFile  
  WHERE ServiceID = @ServiceID   
 ELSE  
  UPDATE dbo.Service SET Picture3 = @PictureFile  
  WHERE ServiceID = @ServiceID   
  
IF @Mode = 'BI'  
BEGIN
	begin
 if exists (select ServiceID from dbo.Service_Locale WHERE ServiceID = @ServiceID and CultureID = @culture_id  )
 begin
	 UPDATE dbo.Service_Locale SET BringInfo = @BringInfo  
	 WHERE ServiceID = @ServiceID and CultureID = @culture_id  
 end
	else
		begin
			insert into dbo.Service_Locale(ServiceID,CultureID,Description,Details,Conditions,BringInfo)
			values (@ServiceID,@culture_id,'','','',@BringInfo)
		end
end
END

  
IF @Mode = 'SEL'  
 IF @ServiceID = 0  
  SELECT s.*, sl.Description, sl.Details FROM dbo.Service s INNER JOIN dbo.Service_Locale sl ON s.ServiceID = sl.ServiceID and sl.CultureID = @cultureID  
  WHERE s.CompanyCode = @CompanyCode  
  ORDER by sl.Description  
 ELSE  
  SELECT s.*, sl.Description, sl.Details FROM dbo.Service s INNER JOIN dbo.Service_Locale sl ON s.ServiceID = sl.ServiceID and sl.CultureID = @cultureID  
  WHERE s.ServiceID = @ServiceID  
  
IF @Mode = 'SE2'  
  SELECT s.Code, s.Active, s.ListOrder, s.ServiceID, sl.Description FROM dbo.Service s INNER JOIN dbo.Service_Locale sl ON s.ServiceID = sl.ServiceID and sl.CultureID = 1  
  WHERE s.CompanyCode = @CompanyCode  
  ORDER by sl.Description  
  
IF @Mode = 'SE1'  
 SELECT isnull(Conditions,'') as Conditions, isnull(BringInfo,'') as BringInfo FROM dbo.Service_Locale  
 WHERE ServiceID = @ServiceID and CultureID = @CultureID  
  
IF @Mode = 'SE3'  
 SELECT isnull(Conditions,'') as Conditions, isnull(BringInfo,'') as BringInfo FROM dbo.Service_Locale  
 WHERE ServiceID = @ServiceID and CultureID = @Culture_id  
  
IF @Mode = 'CBO'  
 SELECT s.ServiceID, sl.Description   
 FROM dbo.Service s INNER JOIN dbo.Service_Locale sl ON s.ServiceID = sl.ServiceID and sl.CultureID=1 and s.ControlInventory = 1  
 WHERE s.CompanyCode = @CompanyCode  
   
IF @Mode = 'DEL'  
 BEGIN  
  DECLARE @cant int  
  SELECT @cant = COUNT(*) FROM dbo.SaleItem where ServiceID = @ServiceID  
  
  IF @cant = 0  
  BEGIN  
   BEGIN TRAN  
    DELETE FROM dbo.ServiceAvailability WHERE ServiceID = @ServiceID  
  
    DELETE FROM dbo.ServiceSchedule WHERE ServiceID = @ServiceID  
  
    DELETE FROM dbo.Service_Locale WHERE ServiceID = @ServiceID  
  
    DELETE FROM dbo.Service WHERE ServiceID = @ServiceID  
   IF @@ERROR=0  
    COMMIT TRAN  
   ELSE  
    ROLLBACK TRAN  
  END  
  
  SELECT @cant  
 END  
  
   
  
  
  