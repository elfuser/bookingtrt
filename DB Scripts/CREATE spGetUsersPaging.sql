CREATE  proc [dbo].[spGetUsersPaging]      
  @company_code char(10) = '',        
  @PageNumber INT,        
  @PageSize INT = NULL,        
  @OrderBy VARCHAR(100) = NULL,        
  @IsAccending BIT = 1      
      
AS      
BEGIN      
      
  DECLARE @SQLString NVARCHAR(MAX),        
     @FilterString VARCHAR(MAX),         
     @ParmDefinition NVARCHAR(500)       
           
  DECLARE @cultureID int        
         
   IF (@PageSize IS NULL OR @PageSize = 0)        
   SET @PageSize = 20        
            
   IF (@OrderBy IS NULL OR @OrderBy = '')        
   SET @OrderBy = ' user_name  '        
           
   IF @IsAccending = 0        
   SET @OrderBy = @OrderBy + ' DESC'        
   ELSE        
   SET @OrderBy = @OrderBy + ' ASC'        
           
   SET  @FilterString = ''       
         
   SET @ParmDefinition = N'       
        @company_code char(10),         
  @PageNumber INT,        
     @PageSize INT = NULL          
   '       
          
        
 Set @SQLString=' WITH SQLPaging AS              
           
 (SELECT TOP ( @PageSize* @PageNumber )              
           
   ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),            
    U.[user_name]  
      ,U.[user_id]  
      ,U.[first_name]  
      ,U.[last_name]  
      ,U.[profile_id]  
      ,U.[super_user]  
      ,U.[active]  
      ,U.[last_login]  
      ,U.[SecondSurname]   
      ,U.password   
      ,c.profile_name as profile  
  FROM [dbo].[User] as U  
 INNER JOIN Profile c ON  
  (u.profile_id = c.profile_id)  
 INNER JOIN UserCompany uc  
   ON  (u.user_id = uc.user_id)    
  WHERE   uc.company_code = @company_code and super_user = 0      
  ORDER BY ' + @OrderBy  + '             
           
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'              
           
           
           
    EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@PageNumber,@PageSize          
        
END      
         