CREATE proc [dbo].[spAddOnPhoto]  
 @AddOn_id BIGINT 
AS  
  
BEGIN 
 
    SELECT photo1 as photo 
    FROM dbo.AddonPhoto
    WHERE AddonID = @AddOn_id  
  
END  