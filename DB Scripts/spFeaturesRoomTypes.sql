-- =============================================  
-- Author:  Cesar M. Varela V.  
-- Create date: 05/12/2009  
-- Description: Insert, Update, Delete And Select records from the table tblFeaturesRoomTypes  
-- =============================================  
  
ALTER PROCEDURE [dbo].[spFeaturesRoomTypes]  
 (  
 @mode   char(3),  
 @roomtype_id int = 0,  
 @feature_id  smallint = 0  
 )  
As  
   
 Set nocount on  
  
 If @mode = 'INS'  
   
  Begin  
      
   Insert into FeaturesRoomType  
    (roomtype_id, feature_id)  
   Values  
    (@roomtype_id, @feature_id)  
         
  End  
   
 If @mode = 'DEL'  
   
  Begin  
    
   Delete From FeaturesRoomType  
   Where roomtype_id = @roomtype_id and feature_id = @feature_id  
    
  End  
  
  
    
 If @mode = 'DRP'  
   
  Begin  
    
   Delete From FeaturesRoomType  
   Where roomtype_id = @roomtype_id 
    
  End  
  
 If @mode = 'SEL'   -- No se aplica el concepto de seleccionar todos los registros porque no tiene uso  
   
  Begin  
    
   Select f.*, r.description From FeaturesRoomType f  
   Inner join Feature_Locale r on f.feature_id = r.feature_id and r.CultureID = 1  
   Where roomtype_id = @roomtype_id  
    
  End  
  
  