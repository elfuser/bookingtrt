ALTER PROCEDURE [dbo].[spCompanyHotel]  
 (   
 @mode    char(3),  
 @company_code  char(10),  
 @tax_perc   money = 0,  
 @max_adults   tinyint = 0,  
 @max_children  tinyint = 0,  
 @msg_adults   varchar(20) = '',  
 @msg_children  varchar(20) = '',  
 @children_free  bit = 0,  
 @msg_children_free varchar(20) = '',  
 @children_note  varchar(100) = '',  
 @confirm_prefix  char(4) = ''  
 )  
AS  
  
 Set nocount on  
 
  If @mode = 'INS'  
  BEGIN
	 IF EXISTS (SELECT confirm_prefix FROM CompanyHotel WHERE company_code = @company_code  ) 
		SET @mode = 'UPD' 
  END
   
 If @mode = 'INS'  
   
  Begin  
    
   Insert into CompanyHotel  
   (company_code, tax_perc, max_adults, max_children,  
    msg_adults, msg_children, children_free, msg_children_free, children_note, confirm_prefix, reservation_nr, tripAdRate)  
   Values  
   (@company_code, @tax_perc, @max_adults,   
    @max_children,   
    @msg_adults, @msg_children, @children_free, @msg_children_free, @children_note, @confirm_prefix, 0, '')  
  
  End  
    
 If @mode = 'UPD'  
   
  Begin   
    Update CompanyHotel  
    Set tax_perc = @tax_perc, max_adults = @max_adults, max_children = @max_children, confirm_prefix = @confirm_prefix,      
     msg_adults = @msg_adults, msg_children = @msg_children, children_free = @children_free, msg_children_free = @msg_children_free, children_note = @children_note  
    Where  
     company_code = @company_code  
  End  
  
  
 If @mode = 'SEL'   
  Select *  
  From CompanyHotel   
  Where company_code = @company_code  
  
  
  