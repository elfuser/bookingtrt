CREATE proc [dbo].[spFeatureLocale]    
 @mode   char(3),    
 @feature_id  smallint,    
 @CultureID  int,    
 @description varchar(50) = '',  
 @company_code char(10) = ''   
as    
    
 IF @mode = 'INS'    
 BEGIN  
  --SELECT @feature_id= ISNULL(MAX(@feature_id),0) + 1  
  --FROM dbo.Feature_Locale   
    
  INSERT INTO dbo.Feature(company_code)  
  VALUES(@company_code)  
  
  SELECT @feature_id=@@Identity
  
  INSERT INTO dbo.Feature_Locale (feature_id, CultureID, description)    
  VALUES (@feature_id, @CultureID, @Description)    
    

 END  
    
 IF @mode = 'UPD'    
  UPDATE dbo.Feature_Locale SET description = @description    
  WHERE feature_id = @feature_id and CultureID = @CultureID    
    
 IF @mode = 'DEL'    
  DELETE FROM dbo.Feature_Locale     
  WHERE feature_id = @feature_id and CultureID = @CultureID    
    
 IF @mode = 'SEL'    
  SELECT * FROM dbo.Feature_Locale    
  WHERE feature_id = @feature_id and CultureID = @CultureID    
    