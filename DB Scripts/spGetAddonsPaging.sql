ALTER proc [dbo].[spGetAddonsPaging]  
  @company_code char(10) = '',  
  @culture  char(5) = '',  
  @PageNumber INT,    
  @PageSize INT = NULL,    
  @OrderBy VARCHAR(100) = NULL,    
  @IsAccending BIT = 1  
  
AS  
BEGIN  
  
  DECLARE @SQLString NVARCHAR(MAX),    
     @FilterString VARCHAR(MAX),     
     @ParmDefinition NVARCHAR(500)   
       
  DECLARE @cultureID int    
     
   IF (@PageSize IS NULL OR @PageSize = 0)    
   SET @PageSize = 20    
        
   IF (@OrderBy IS NULL OR @OrderBy = '')    
   SET @OrderBy = '  a.addonID '    
       
   IF @IsAccending = 0    
   SET @OrderBy = @OrderBy + ' DESC'    
   ELSE    
   SET @OrderBy = @OrderBy + ' ASC'    
       
   SET  @FilterString = ''   
     
   SET @ParmDefinition = N'   
      @company_code char(10),   
      @cultureID int,  
   @PageNumber INT,    
   @PageSize INT = NULL      
   '   
     
     
    IF @culture = '' or @culture is null  
    SET @cultureID = 1  
   ELSE  
    SELECT @cultureID = ISNULL(CultureID,0)   
    FROM dbo.Culture  
       WHERE CultureName = @culture  
  
   IF @cultureID = 0  
    SET @cultureID = 1  
  
   
Set @SQLString=' WITH SQLPaging AS          
      
(SELECT TOP ( @PageSize* @PageNumber )          
      
  ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),        
  a.addonID,  
  a.addonType,  
  a.amount,  
  a.enabled,  
  a.roworder,  
  a.tax,  
  a.valid_from,  
  a.valid_to,  
  a.duration,  
  a.departure,  
   al.description,   
   al.longDescription,  
   al.CultureID,  
   a.amount1,  
   a.amount2,  
   a.amount3,  
   a.amount4,  
   a.amount5,  
   a.amount6,  
   a.amount7,  
   a.amount8,  
   a.amount9,  
   a.amount10,
   a.picture1,
   a.picture2 
  FROM dbo.Addon a   
    INNER JOIN dbo.Addon_Locale al   
     ON a.addonID = al.addonID   
      WHERE a.company_code = @company_code  
      AND  al.CultureID=@cultureID  
 ORDER BY ' + @OrderBy  + '         
      
  )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'          
      
      
      
   EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@cultureID,@PageNumber,@PageSize      
   
END  
     
           
   