ALTER PROCEDURE [dbo].[spUsers]    
 (    
 @mode   char(3),    
 @user_id  int = 0,    
 @company_code char(10) = '',    
 @user_name  varchar(50) = '',    
 @password  varchar(100) = '',    
 @first_name  varchar(30) = '',    
 @last_name  varchar(50) = '',    
 @SecondSurname  varchar(50) = '',   
 @profile_id  smallint = 0,    
 @active   bit = 1 ,  
 @isAgencyUser bit =1 ,
 @AgencyCode INT = 0  
 )    
As    
     
 Set nocount on    
 DECLARE @userid int    
    
 If @mode = 'INS'    
     
  Begin    
      
   Insert into [User]    
    ([user_name], [password], first_name, last_name, profile_id, super_user, last_login, active,SecondSurname,isAgencyUser,AgencyCode)    
   Values    
    (@user_name, @password, @first_name, @last_name, @profile_id, 0, GETDATE(), @active,@SecondSurname,@isAgencyUser,@AgencyCode)    
    
   SET @user_id = @@IDENTITY    
    
   Insert into UserCompany ([user_id], company_code)    
   Values (@user_id, @company_code)    
       
   Insert into UsersLogPwd ([user_id], date_changed, next_date_change, [password])    
   Values (@user_id, getdate(), getdate(), @password)    
    
   select @user_id    
  End    
     
 If @mode = 'UPD'    
     
  Begin    
   Update [User]    
   Set first_name = @first_name, last_name = @last_name,   
   profile_id = @profile_id, [user_name] = @user_name, active = @active  ,  
   SecondSurname=@SecondSurname,isAgencyUser=@isAgencyUser ,AgencyCode=@AgencyCode 
   Where [user_id] = @user_id    
  End    
     
 If @mode = 'DEL'    
  Begin    
   BEGIN TRAN    
    Delete from UsersLogPwd    
    where [user_id] = @user_id    
    
    Delete from UserCompany    
    where [user_id] = @user_id    
    
    Delete From [User]    
    Where [user_id] = @user_id     
    
   IF @@ERROR = 0    
    COMMIT TRAN    
   ELSE    
    ROLLBACK TRAN    
  End    
    
 If @mode = 'SEL'    
  Begin    
      
   IF @user_id = 0 AND @user_name = ''    
       
    Select u.*, c.profile_name as [profile]    
    From [User] u inner join Profile c on u.profile_id = c.profile_id    
     inner join UserCompany uc on u.user_id = uc.user_id     
    Where uc.company_code = @company_code and super_user = 0    
        
   ELSE IF @user_name <> ''    
       
    Select u.*, c.profile_name as [profile] From [User] u inner join Profile c on u.profile_id = c.profile_id    
    Where [user_name] = @user_name    
   ELSE    
    Select u.*, c.profile_name as [profile] From [User] u inner join Profile c on u.profile_id = c.profile_id    
    Where [user_id] = @user_id     
       
  End    
    
 If @mode = 'CBO'    
  Select u.user_id, u.first_name, u.last_name    
  From dbo.UserCompany uc INNER JOIN dbo.[User] u ON uc.user_id = u.user_id    
  Where uc.company_code = @company_code    
      
 If @mode = 'ALL'    
      
  begin    
      
   Select u.*, c.profile_name as [profile] From [User] u inner join Profile c on u.profile_id = c.profile_id    
   order by c.profile_name    
    
  end    
    
 If @mode = 'BLK'    
  UPDATE dbo.[User] SET Active = @active    
  WHERE [user_id] = @user_id    
    
    