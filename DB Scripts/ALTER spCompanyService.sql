ALTER PROCEDURE [dbo].[spCompanyService]  
 (   
 @mode    char(3),  
 @company_code  char(10),  
 @tax_perc   money = 0,  
 @rent_perc   money = 0,  
 @max_adults   tinyint = 0,  
 @max_children  tinyint = 0,  
 @max_student  tinyint = 0,  
 @msg_adults   varchar(20) = '',  
 @msg_children  varchar(20) = '',  
 @msg_student  varchar(20) = '',  
 @children_free  bit = 0,  
 @msg_children_free varchar(20) = '',  
 @children_note  varchar(100) = '',  
 @confirm_prefix  char(3) = ''  
 )  
AS  
  
 Set nocount on  
 
  If @mode = 'INS'  
  BEGIN
  	 IF EXISTS (SELECT company_code FROM CompanyService WHERE company_code = @company_code  ) 
		SET @mode = 'UPD' 
  END
   
 If @mode = 'INS'  
   
  Begin  
    
   Insert into CompanyService  
   (company_code, tax_perc, max_adults, max_children, max_students, msg_students, rent_perc,  
    msg_adults, msg_children, children_free, msg_children_free, children_note, confirm_prefix, sale_nr)  
   Values  
   (@company_code, @tax_perc, @max_adults, @max_children, @max_student, @msg_student, @rent_perc,  
    @msg_adults, @msg_children, @children_free, @msg_children_free, @children_note, @confirm_prefix, 0)  
  
  End  
    
 If @mode = 'UPD'  
   
  Begin   
   DECLARE @existe int  
   SELECT @existe = COUNT(*) FROM dbo.CompanyService WHERE company_code = @company_code  
  
   IF @existe=0  
    Insert into CompanyService  
    (company_code, tax_perc, max_adults, max_children, max_students, msg_students, rent_perc,  
     msg_adults, msg_children, children_free, msg_children_free, children_note, confirm_prefix, sale_nr)  
    Values  
    (@company_code, @tax_perc, @max_adults, @max_children, @max_student, @msg_student, @rent_perc,  
     @msg_adults, @msg_children, @children_free, @msg_children_free, @children_note, @confirm_prefix, 0)  
   else  
    Update CompanyService  
    Set tax_perc = @tax_perc, max_adults = @max_adults, max_children = @max_children, max_students = @max_student, msg_students = @msg_student, rent_perc = @rent_perc,    
     msg_adults = @msg_adults, msg_children = @msg_children, children_free = @children_free, msg_children_free = @msg_children_free, children_note = @children_note  
    Where  
     company_code = @company_code  
  End  
  
  
 If @mode = 'SEL'   
  Select *  
  From CompanyService  
  Where company_code = @company_code  
  
  
  
  
  