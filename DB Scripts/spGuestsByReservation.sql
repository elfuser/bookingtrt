-- =============================================      
-- Author:  William Fallas     
-- Create date: 19/10/2016      
-- Description: Insert, Update, Delete And Select records from the table tblGuests      
-- =============================================      
      
CREATE PROCEDURE [dbo].[spGuestsByReservation]      
 (      
 @ReservationID char(10),            
 @name    varchar(30) = '',      
 @lastname   varchar(30) = '',      
 @company_code  char(10),      
 @creation_date  date = '',      
 @telephone_nr  varchar(15) = '',      
 @country_iso3  char(3) = '',      
 @email    varchar(50) = ''      
 )      
As      
       
 Set nocount on      
    
    DECLARE  @guest_id   bigint
    
    SELECT @guest_id=guest_id
    FROM dbo.Reservation
    WHERE reservation_id = @ReservationID
    
    if @guest_id > 0
    BEGIN
		Update Guest      
		Set name = @name, lastname = @lastname, company_code = @company_code,      
		 creation_date = @creation_date,       
		 telephone_nr = @telephone_nr, country_iso3 = @country_iso3,      
		 email = @email      
		Where      
		 guest_id = @guest_id      
    END
    Select @guest_id      
      
  
      
   
   