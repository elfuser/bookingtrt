ALTER proc [dbo].[spGetCompaniesPaging]     
  @PageNumber INT,      
  @PageSize INT = NULL,      
  @OrderBy VARCHAR(100) = NULL,      
  @IsAccending BIT = 1    
AS    
BEGIN    
    
    
 DECLARE @SQLString NVARCHAR(MAX),      
     @FilterString VARCHAR(MAX),       
     @ParmDefinition NVARCHAR(500)     
         
         
   IF (@PageSize IS NULL OR @PageSize = 0)      
   SET @PageSize = 20      
          
   IF (@OrderBy IS NULL OR @OrderBy = '')      
   SET @OrderBy = '  a.company_code '      
         
   IF @IsAccending = 0      
   SET @OrderBy = @OrderBy + ' DESC'      
   ELSE      
   SET @OrderBy = @OrderBy + ' ASC'      
         
   SET  @FilterString = ''     
       
   SET @ParmDefinition = N'     
      @PageNumber INT,      
   @PageSize INT = NULL        
   '     
        
      
     
Set @SQLString=' WITH SQLPaging AS            
        
(SELECT TOP ( @PageSize* @PageNumber )            
        
  ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),          
  a.company_code,  a.company_name,  a.legal_name,  a.legal_id,  a.creation_date,    
  a.address,  a.city,  a.state,  a.country_iso3, a.telephone_nr1,  a.telephone_nr2,    
  a.telephone_nr3,  a.fax_nr,  a.contact_name,  a.contact_email,  a.cfo_name,    
  a.cfo_email,  a.res_name,  a.res_email,  a.res_email_alt,  a.web,  a.email,    
  a.guest_nr,  a.quotation_nr,  a.call_toll_free,  a.bank_account,  a.bank_name,  a.facebookURL,    
  a.twitterText,  a.google_id,  a.google_conv_id,  a.google_conv_label,  a.descriptor,    
  a.hotel,  a.services,  a.latitude,  a.longitude,  a.tripAdvisor,  a.active,  a.migrar,a.Provincia,   
  a.Canton,a.Distrito,a.TechContact,a.TechEmail
  FROM dbo.Company a         
 ORDER BY ' + @OrderBy  + '           
        
  )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'            
        
        
        
   EXECUTE sp_executesql @SQLString,@ParmDefinition,@PageNumber,@PageSize      
    
END      