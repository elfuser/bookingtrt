CREATE  proc [dbo].[spGetRatesExceptionPaging]    
  @company_code char(10),    
  @rate_code  char(10),    
  @PageNumber INT,      
  @PageSize INT = NULL,      
  @OrderBy VARCHAR(100) = NULL,      
  @IsAccending BIT = 1    
    
AS    
BEGIN    
    
  DECLARE @SQLString NVARCHAR(MAX),      
     @FilterString VARCHAR(MAX),       
     @ParmDefinition NVARCHAR(500)     
         
  
   IF (@PageSize IS NULL OR @PageSize = 0)      
   SET @PageSize = 20      
          
   IF (@OrderBy IS NULL OR @OrderBy = '')      
   SET @OrderBy = '  a.[rate_code]'      
         
   IF @IsAccending = 0      
   SET @OrderBy = @OrderBy + ' DESC'      
   ELSE      
   SET @OrderBy = @OrderBy + ' ASC'      
         
   SET  @FilterString = ''     
       
   SET @ParmDefinition = N'     
      @company_code char(10),   
      @rate_code  char(10),      
   @PageNumber INT,      
   @PageSize INT = NULL        
   '     
        
      
 Set @SQLString=' WITH SQLPaging AS            
         
 (SELECT TOP ( @PageSize* @PageNumber )            
     ResultNum = ROW_NUMBER() OVER (ORDER BY ' + @OrderBy  + '),          
     a.[company_code]  
    ,a.[rate_code]  
    ,a.[roomtype_code]  
    ,a.[start_date]  
    ,a.[end_date]  
    ,a.[adult1]  
    ,a.[adult2]  
    ,a.[adult3]  
    ,a.[adult4]  
    ,a.[adult5]  
    ,a.[adult6]  
    ,a.[adult7]  
    ,a.[adult8]  
    ,a.[adult9]  
    ,a.[adult10]  
    ,a.[adult11]  
    ,a.[adult12]  
    ,a.[child]  
    ,a.[created]   
    ,a.RowId
    ,rl.description as room_name  
    ,b.roomtype_code as room_code     
     From RateException a   
  INNER JOIN RoomType b   
   ON a.roomtype_code = b.roomtype_code and a.company_code = b.company_code    
  INNER JOIN RoomType_Locale rl   
   ON b.roomtype_id = rl.roomtype_id and rl.CultureID=1    
     Where a.rate_code = @rate_code and a.company_code = @company_code     
  ORDER BY ' + @OrderBy  + '           
         
   )  SELECT * FROM SQLPaging WHERE ResultNum > ((@PageNumber - 1) * @PageSize )'            
         
         
         
   EXECUTE sp_executesql @SQLString,@ParmDefinition,@company_code,@rate_code,@PageNumber,@PageSize        
      
END    
       
             
             