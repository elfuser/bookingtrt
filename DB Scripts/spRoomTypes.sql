ALTER PROCEDURE [dbo].[spRoomTypes]    
 (    
 @mode   char(3),    
 @roomtype_id int = 0,    
 @roomtype_code char(10) = '',    
 @company_code char(10) = '',    
 @description varchar(50) = '',    
 @quantity  tinyint = 0,    
 @max_adults  int = 0,    
 @max_children int = 0,    
 @max_occupance  int = 0,    
 @min_occupance int = 0,    
 @room_detail varchar(1000) = ''    
 )    
As    
     
 Set nocount on    
  
 declare @id int
   
 If @mode = 'INS'    
  Begin    
   Insert into RoomType    
    (roomtype_code, company_code, quantity, max_adults, max_children, max_occupance, min_occupance)    
   Values    
    (@roomtype_code, @company_code, @quantity, @max_adults, @max_children, @max_occupance, @min_occupance)    
       
      Select @id= @@identity  
      
   Insert into RoomType_Locale    
    (roomtype_id, CultureID, description, room_detail)    
   Values    
    (@id, 1, @description, @room_detail)    
    
	Select @id
	
  End    
     
 If @mode = 'UPD'    
  Begin    
   Update RoomType    
   Set roomtype_code = @roomtype_code, company_code = @company_code,      
    quantity = @quantity, max_adults = @max_adults, max_children = @max_children,     
    max_occupance = @max_occupance, min_occupance = @min_occupance    
   Where roomtype_id = @roomtype_id    
    
   Update RoomType_Locale    
   Set description = @description, room_detail = @room_detail    
   Where roomtype_id = @roomtype_id and CultureID = 1    
     
      Select @roomtype_id    
  End    
     
 If @mode = 'DEL'    
  Begin    
   DECLARE @@roomtype_code char(10)    
   DECLARE @@company_code char(10)    
    
   SELECT @@roomtype_code = roomtype_code, @@company_code = company_code    
   FROM dbo.RoomType    
   WHERE roomtype_id = @roomtype_id    
    
   delete from Availability     
   Where company_code = @@company_code and roomtype_code = @@roomtype_code    
    
   delete from AvailabilityRoom    
   Where company_code = @@company_code and roomtype_code = @@roomtype_code    
    
   delete from FeaturesRoomType    
   Where roomtype_id = @roomtype_id    
      
   delete from RateRoomType    
   Where roomtype_id = @roomtype_id    
       
   delete from RoomType_Locale    
   Where roomtype_id = @roomtype_id    
    
   Delete From RoomType    
   Where  roomtype_id = @roomtype_id    
    
  End    
    
 If @mode = 'SEL'    
  Begin    
   If @company_code = ''    
       
    Begin       
     Select r.*, rl.description, rl.room_detail From RoomType r INNER JOIN RoomType_Locale rl ON r.roomtype_id=rl.roomtype_id    
     Where r.roomtype_id = @roomtype_id and CultureID = 1    
    End    
        
   Else    
       
    Begin       
     Select r.*, rl.description, rl.room_detail From RoomType r INNER JOIN RoomType_Locale rl ON r.roomtype_id=rl.roomtype_id    
     Where company_code = @company_code and rl.CultureID = 1    
     order by rl.description    
    End    
           
  End    
    
 If @mode = 'STA'    
  Select rl.description From RoomType r INNER JOIN RoomType_Locale rl ON r.roomtype_id=rl.roomtype_id    
  Where company_code = @company_code and rl.CultureID = 1    
  order by rl.description    
    
    
    