CREATE PROCEDURE [dbo].[spUpdateReservations]  
 (   
 @reservation_id  char(10),  
 @guest_id   bigint = 0,  
 @company_code  char(10) = '',  
 @arrival   date = '',  
 @departure   date = '',  
 @rate_code   char(10) = '',  
 @rate_amount  money = 0,  
 @tax_amount   money = 0,  
 @ccard_transid  int = 0,  
 @remarks   text = '',  
 @visit_reason  char(100) = '',  
 @promo_id   smallint = 0,  
 @addon_amount  money = 0,  
 @promo_amount  money = 0,  
 @disc_amount  money = 0,  
 @processor   char(2) = '',  
 @rooms    tinyint = 0,  
 @origin    integer = 0,  
 @resellerID   varchar(15) = '',  
 @sessionID   uniqueidentifier = null  
 )  
As  
   
BEGIN

 Set nocount on  
 DECLARE @processorID smallint  
 DECLARE @commissionOL money, @commissionBank money, @credomaticFund money  
  
 
    Declare @reservation_nr as bigint  
  
    --Generar Numero de Reservacion  
    Set @reservation_nr = (Select reservation_nr From CompanyHotel Where company_code = @company_code) + 1  
    Update CompanyHotel Set reservation_nr = @reservation_nr Where company_code = @company_code  
    Set @reservation_id = (Select rtrim(ltrim(confirm_prefix)) From CompanyHotel Where company_code = @company_code) + CAST(@reservation_nr as char(7))  
      
    --Comisiones  
    IF @processor = 'CR' --Credomatic  
      SELECT @processorID = credomaticProcID,   
      @commissionOL = @rate_amount * credomaticOLPorcCom / 100,   
      @commissionBank = @rate_amount * credomaticPorcCom / 100,  
      @credomaticFund = @rate_amount * credomaticFund / 100  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE IF @processor = 'BN'  
      SELECT @processorID = BNProcID,   
      @commissionOL = @rate_amount * BNOLPorcCom / 100, @commissionBank = 0, @credomaticFund = 0   
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE IF @processor = 'PP'  
      SELECT @processorID = PayPalID,   
      @commissionOL = @rate_amount * PayPalPorcCom / 100, @commissionBank = 0, @credomaticFund = 0  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    ELSE  
      SELECT @processorID = credomaticProcID,   
      @commissionOL = @rate_amount * credomaticOLPorcCom / 100,   
      @commissionBank = 0,  
      @credomaticFund = @rate_amount * credomaticFund / 100  
      FROM dbo.CompanyProcessor WHERE company_code = @company_code  
    --Guardar Reserva  
    UPDATE Reservation   SET
        arrival =@arrival, 
        departure =@departure,   
        rate_code =@rate_code,   
		ccard_transid=@ccard_transid,
		remarks =@remarks, 
		visit_reason = @visit_reason, 
		promo_id = @promo_id, 
		rate_amount=@rate_amount, 
		tax_amount=@tax_amount, 
		addon_amount=@addon_amount, 
		promo_amount=@promo_amount, 
		commissionOL=@commissionOL, 
		commissionBank=@commissionBank,
		credomaticFund=@credomaticFund, 
		processorID=@processorID, 
		rooms=@rooms,  
        origin=@origin, 
        resellerID=@resellerID, 
        disc_amount=@disc_amount  
      
     WHERE reservation_id= @reservation_id
   
        
    Update dbo.SessionData SET reservationID = @reservation_id  
    Where SessionID = @SessionID  
  
    Select @reservation_id  
    
    
END


  
  
  
  