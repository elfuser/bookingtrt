USE [DB_A0683E_BookingEngine]
GO
/****** Object:  StoredProcedure [dbo].[spAddons]    Script Date: 10/27/2016 16:34:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[spRateExceptionCount]
	 @company_code	char(10) = '',
	 @rate_code  char(10)

AS
BEGIN


     Select COUNT(a.company_code) 
     From RateException a 
		INNER JOIN RoomType b
			 ON a.roomtype_code = b.roomtype_code and a.company_code = b.company_code  
		INNER JOIN RoomType_Locale rl 
			ON b.roomtype_id = rl.roomtype_id and rl.CultureID=1  
     WHERE a.rate_code = @rate_code 
		and a.company_code = @company_code  

	 
 
END

		
   						
	
