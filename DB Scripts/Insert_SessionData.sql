CREATE proc [dbo].[Insert_SessionData]  
 @SessionID  uniqueidentifier = null,  
 @companyCode char(10) = '',  
 @rateCode  char(10) = '',  
 @arrivalDate date = '',  
 @departureDate date = '',  
 @rooms   tinyint = 0,  
 @room1Code  char(10) = '',  
 @room1Qty  varchar(5) = '',  
 @room2Code  char(10) = '',  
 @room2Qty  varchar(5) = '',  
 @room3Code  char(10) = '',  
 @room3Qty  varchar(5) = '',  
 @room4Code  char(10) = '',  
 @room4Qty  varchar(5) = '',  
 @promoID  int = 0,  
 @resellerID  char(10) = '',  
 @reservationID char(10) = '',  
 @PayPalToken varchar(50) = '',  
 @addons   varchar(30) = '',  
 @lang   char(5) = ''  
AS  
  
 
BEGIN 
   
   INSERT INTO dbo.SessionData (SessionID, companyCode, resellerID, room1Code, room1Qty, room2Code, room2Qty, room3Code, room3Qty, room4Code, room4Qty, reservationID, PayPalToken, Addons, [Language],arrivalDate,departureDate,rateCode)   
   VALUES (@SessionID, @companyCode, @resellerID, @room1Code, @room1Qty, @room2Code, @room2Qty, @room3Code, @room3Qty, @room4Code, @room4Qty, '', '', '', @lang,@arrivalDate,@departureDate,@rateCode)  
 
END

GO
  

  