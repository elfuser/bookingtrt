﻿
Imports System.Runtime.Serialization

Public Class TourServiceModel

    <DataMember>
    Public Property ReservationID As String

    <DataMember>
    Public Property SessionId As String

    <DataMember>
    Public Property CompanyID As String

    <DataMember>
    Public Property ServiceId As Integer

    <DataMember>
    Public Property PromCode As String

    <DataMember>
    Public Property CardType As String

    <DataMember>
    Public Property TaxAmount As Decimal

    <DataMember>
    Public Property PromoAmount As Decimal

    <DataMember>
    Public Property Discount As Decimal

    <DataMember>
    Public Property Subtotal As Decimal

    <DataMember>
    Public Property total As Decimal

    <DataMember>
    Public Property Processor As String

    '-----

    <DataMember>
    Public Property FName As String
    <DataMember>
    Public Property LName As String
    <DataMember>
    Public Property Country As String
    <DataMember>
    Public Property Phone As String
    <DataMember>
    Public Property EMail As String
    <DataMember>
    Public Property CardNumber As String
    <DataMember>
    Public Property ExpeMonth As String
    <DataMember>
    Public Property ExpeYear As String
    <DataMember>
    Public Property SecureCode As String
    <DataMember>
    Public Property NameCard As String

End Class

Public Class ServiceModelLite

    Public Property SessionId As String
    Public Property ppServiceID As Integer
    Public Property ppServiceName As String
    Public Property ppArrivalDate As Date
    Public Property ppScheduleID As Integer
    Public Property ppScheduleDesc As String
    Public Property ppHotelPickUpID As Integer
    Public Property ppHotelPickupHourID As Integer
    Public Property ppPickupPlaceDesc As String
    Public Property ppPickupScheduleDesc As String
    Public Property ppAdults As Int16
    Public Property ppChildren As Int16
    Public Property ppInfants As Int16
    Public Property ppStudents As Int16
    Public Property ppPriceAdults As Decimal
    Public Property ppPriceChildren As Decimal
    Public Property ppPriceInfants As Decimal
    Public Property ppPriceStudent As Decimal
    Public Property SubTotal As Decimal
    Public Property ppRateId As Integer
    Public Property ppPromoAmount As Decimal
    Public Property ppPromoAmountType As String

End Class