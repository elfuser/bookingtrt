﻿
Imports System.Runtime.Serialization

Public Class VirtualReceiptReservationModel

    <DataMember>
    Public Property ReservationID As String

    <DataMember>
    Public Property SessionId As String

    <DataMember>
    Public Property CompanyID As String

    <DataMember>
    Public Property ServiceId As Integer

    <DataMember>
    Public Property PromCode As String

    <DataMember>
    Public Property CardType As String

    <DataMember>
    Public Property TaxAmount As Decimal

    <DataMember>
    Public Property PromoAmount As Decimal

    <DataMember>
    Public Property Discount As Decimal

    <DataMember>
    Public Property Subtotal As Decimal

    <DataMember>
    Public Property total As Decimal

    <DataMember>
    Public Property Processor As String

    '-----
    <DataMember>
    Public Property ReceiptCode As Integer
    <DataMember>
    Public Property ClientName As String
    <DataMember>
    Public Property Description As String
    <DataMember>
    Public Property Amount As Double

    <DataMember>
    Public Property FName As String
    <DataMember>
    Public Property LName As String
    <DataMember>
    Public Property Country As String
    <DataMember>
    Public Property Phone As String
    <DataMember>
    Public Property EMail As String
    <DataMember>
    Public Property CardNumber As String
    <DataMember>
    Public Property ExpeMonth As String
    <DataMember>
    Public Property ExpeYear As String
    <DataMember>
    Public Property SecureCode As String
    <DataMember>
    Public Property NameCard As String

End Class