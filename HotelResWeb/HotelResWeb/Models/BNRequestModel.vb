﻿
Imports System.Runtime.Serialization

Public Class BNRequestModel

    <DataMember>
    Public Property mSESSIONKEY As String
    <DataMember>
    Public Property mXMLREQ As String
    <DataMember>
    Public Property mDIGITALSIGN As String
    <DataMember>
    Public Property mIDACQUIRER As String
    <DataMember>
    Public Property mIDCOMMERCE As String

End Class
