﻿
Imports System.Runtime.Serialization

Public Class CredomaticRequestModel

    <DataMember>
    Public Property hash As String
    <DataMember>
    Public Property username As String
    <DataMember>
    Public Property redirect As String
    <DataMember>
    Public Property orderid As String
    <DataMember>
    Public Property amount As String
    <DataMember>
    Public Property processor_id As String
    <DataMember>
    Public Property type As String
    <DataMember>
    Public Property key_id As String
    <DataMember>
    Public Property time As String
    <DataMember>
    Public Property ccnumber As String
    <DataMember>
    Public Property ccexp As String

    <DataMember>
    Public Property action As String

End Class
