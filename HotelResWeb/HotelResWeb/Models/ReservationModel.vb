﻿
Imports System.Runtime.Serialization

Public Class ReservationModel


    <DataMember>
    Public Property ReservationID As String


    <DataMember>
    Public Property SessionId As String


    <DataMember>
    Public Property Processor As String

    <DataMember>
    Public Property id As String
    <DataMember>
    Public Property CompanyID As String
    <DataMember>
    Public Property FName As String
    <DataMember>
    Public Property LName As String
    <DataMember>
    Public Property Country As String
    <DataMember>
    Public Property Phone As String
    <DataMember>
    Public Property EMail As String
    <DataMember>
    Public Property PromCode As String

    <DataMember>
    Public Property PromoID As Integer

    <DataMember>
    Public Property CardType As String

    <DataMember>
    Public Property TaxAmount As Decimal

    <DataMember>
    Public Property AddonAmount As Decimal

    <DataMember>
    Public Property PromoAmount As Decimal

    <DataMember>
    Public Property Discount As Decimal



    <DataMember>
    Public Property Subtotal As Decimal

    <DataMember>
    Public Property total As Decimal

    <DataMember>
    Public Property CardNumber As String

    <DataMember>
    Public Property ExpeMonth As String

    <DataMember>
    Public Property ExpeYear As String

    <DataMember>
    Public Property SecureCode As String

    <DataMember>
    Public Property NameCard As String


End Class


Public Class AddOnLiteModel

    Public Property Code As String

    Public Property Desc As String

    Public Property Price As Decimal

End Class


Public Class RoomLiteModel


    Public Property CompanyId As String

    Public Property Adults As String

    Public Property Kids As String

    Public Property Infants As String

    Public Property fromDt As String

    Public Property toDt As String

    Public Property Desc As String

    Public Property Price As Decimal

    Public Property PriceDisc As Decimal

    Public Property Code As String

    Public Property RoomType As String

    Public Property Discount As Decimal

    Public Property TaxPerc As Decimal

    Public Property RateCode As String


End Class