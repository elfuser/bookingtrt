﻿$(function () {

    setTimeout(adaptImageToHeaderNavBar, 2500);

});

function adaptImageToHeaderNavBar() {    

    var snavbarwidth = $("#navbar3").css("width");
    var sulwidth = $("ul.nav.navbar-nav").css("width");
    var inavbarwidth = parseInt(snavbarwidth.replace("px", ""))
    var isulwidth = parseInt(sulwidth.replace("px", ""))

    var simgwidth = $(".navbar-brand img").css("width");
    var iimgwidth = parseInt(simgwidth.replace("px", ""))

    var simgheight = $(".navbar-brand img").css("height");
    var iimgheight = parseInt(simgheight.replace("px", ""))

    //Check if browser is firefox
    var isFirefox = typeof InstallTrigger !== 'undefined';

    if (inavbarwidth > isulwidth) {
        var rest = inavbarwidth - isulwidth;
        $(".navbar-brand img").css("max-width", rest - 5);
        if (isFirefox) {
            $(".navbar-brand img").css("max-width", rest - 15);
        }
        var imgmarginleft;
        if (iimgwidth < rest) {
            imgmarginleft = (rest - iimgwidth) / 2;
            //$(".navbar-brand img").css("margin-left", imgmarginleft);
        }

        if (iimgwidth >= (rest + 5)) {
            if (iimgheight > iimgwidth) {
                $(".navbar-brand img").css("margin-left", "75%");
            } else if (iimgheight < iimgwidth) {
                $(".navbar-brand img").css("margin-left", "2%");
            } else {
                
                if (isFirefox) {
                    $(".navbar-brand img").css("margin-left", "30%");
                }
            }
        } else {
            
            if (isFirefox) {
                $(".navbar-brand img").css("margin-left", "30%");
            }
        }
    }

    var sulheight = $("ul.nav.navbar-nav").css("height");
    var iulheight = parseInt(sulheight.replace("px", ""));

    if (iimgheight > iulheight) {
        $(".navbar-brand img").css("max-height", iulheight - 5);
    }

    $("#idCompanyLogo").show();
}