﻿
DayAvailabilityModel = function () {
    var self = this;
    self.ppDay = ko.observable();
    self.ppQty = ko.observable();


    self.MapEntity = function (serviceEntity, DayAvailabilityDto) {
        var compareKey = {
            'ppDay': {
                key: function (data) {
                    return data.ppDay;
                }
            }
        };

        var date = new Date(parseInt(serviceEntity.ppDay.substr(6)));
        serviceEntity.ppDay = date;

        ko.mapping.fromJS(serviceEntity, compareKey, DayAvailabilityDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}