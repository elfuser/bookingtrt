﻿
RoomsModel = function () {
    var self = this;
    self.ppRoomTypeID = ko.observable();
    self.ppRoomTypeCode = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppDescription = ko.observable();
    self.ppQuantity = ko.observable();
    self.ppMax_Adults = ko.observable();
    self.ppMax_Children = ko.observable();
    self.ppImage_Count = ko.observable();
    self.ppRoom_Detail = ko.observable();
    self.ppMaxOccupance = ko.observable();
    self.ppMinOccupance = ko.observable();
    self.Price = ko.observable();
    self.Alignmentclass = ko.observable();
    self.ImageUlr = ko.observable();

    self.MapEntity = function (serviceEntity, RoomsDto) {
        var compareKey = {
            'ppRoomTypeID': {
                key: function (data) {
                    return data.ppRoomTypeID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RoomsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}