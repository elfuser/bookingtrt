﻿BNRequestModel = function () {
    var self = this;
    self.mSESSIONKEY = ko.observable();
    self.mXMLREQ = ko.observable();
    self.mDIGITALSIGN = ko.observable();
    self.mIDACQUIRER = ko.observable();
    self.mIDCOMMERCE = ko.observable();

    self.MapEntity = function (serviceEntity, BNRequestDto) {
        var compareKey = {
            'mSESSIONKEY': {
                key: function (data) {
                    return data.mSESSIONKEY;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, BNRequestDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}