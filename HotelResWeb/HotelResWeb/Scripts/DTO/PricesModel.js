﻿
PricesModel = function () {
    var self = this;
    self.date = ko.observable();
    self.ppPrices = ko.observable();
    self.strdate = ko.observable();
    self.Discount = ko.observable();
    self.PricesWithDiscount = ko.observable();
    self.ppPromoAmount = ko.observable();
    self.ppPromoType = ko.observable();

    self.MapEntity = function (serviceEntity, PricesDto) {
        var compareKey = {
            'date': {
                key: function (data) {
                    return data.date;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, PricesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}