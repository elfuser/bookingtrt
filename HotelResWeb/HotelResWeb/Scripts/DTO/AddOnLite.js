﻿
AddOnLite = function () {


    var self = this;
    self.Code = ko.observable();
    self.Price = ko.observable();
    self.Desc = ko.observable();
    self.PriceInText = ko.observable();
    self.UniqueId = ko.observable();


    self.MapEntity = function (serviceEntity, AddOnLiteDto) {
        var compareKey = {
            'Code': {
                key: function (data) {
                    return data.Code;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, AddOnLiteDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}