﻿
ReservationModel = function () {

    var self = this;

    self.ReservationID = ko.observable();
    self.SessionId = ko.observable();
    self.Processor = ko.observable();
    
    self.id = ko.observable();
    self.CompanyID = ko.observable();
    self.FName = ko.observable();
    self.LName = ko.observable();
    self.Country = ko.observable();
    self.Phone = ko.observable();
    self.EMail = ko.observable();
    self.PromCode = ko.observable();
    self.PromoID = ko.observable();

    self.Subtotal = ko.observable();
    self.TaxAmount = ko.observable();
    self.AddonAmount  = ko.observable();
    self.PromoAmount  = ko.observable();
    self.Discount = ko.observable();
    self.total = ko.observable();

    self.CardNumber = ko.observable();
    self.ExpireCard = ko.observable();

    self.ExpeMonth  = ko.observable();

    self.ExpeYear = ko.observable();

    self.SecureCode = ko.observable();

    self.NameCard = ko.observable();

    self.Processor = ko.observable();

    self.CardType = ko.observable();
    

    self.MapEntity = function (serviceEntity, ReservationDTO) {
        var compareKey = {
            'id': {
                key: function (data) {
                    return data.id;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ReservationDTO);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}