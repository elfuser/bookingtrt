﻿
TourCategoryModel = function () {
    var self = this;
    self.ppCategoryID = ko.observable();
    self.ppDescription = ko.observable();
    self.ppCategoryChecked = ko.observable();

    self.MapEntity = function (serviceEntity, CategoryDto) {
        var compareKey = {
            'CategoryID': {
                key: function (data) {
                    return data.CategoryID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CategoryDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}