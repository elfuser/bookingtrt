﻿
CardTypesModel = function () {
    var self = this;
    self.ppCode = ko.observable();
    self.ppName


    self.MapEntity = function (serviceEntity, CardTypesDto) {
        var compareKey = {
            'ppCode': {
                key: function (data) {
                    return data.ppCode;
                }
            }
        };


        ko.mapping.fromJS(serviceEntity, compareKey, CardTypesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}