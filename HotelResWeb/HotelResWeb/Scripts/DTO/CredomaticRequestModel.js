﻿CredomaticRequestModel = function () {
    var self = this;
    self.hash = ko.observable();
    self.username = ko.observable();
    self.redirect = ko.observable();
    self.orderid = ko.observable();
    self.amount = ko.observable();
    self.processor_id = ko.observable();
    self.type = ko.observable();
    self.key_id = ko.observable();
    self.time = ko.observable();
    self.ccnumber = ko.observable();
    self.ccexp = ko.observable();
    self.action = ko.observable();

    self.MapEntity = function (serviceEntity, CredomaticRequestDto) {
        var compareKey = {
            'hash': {
                key: function (data) {
                    return data.hash;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CredomaticRequestDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}