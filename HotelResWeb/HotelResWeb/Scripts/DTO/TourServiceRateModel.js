﻿
TourServiceRateModel = function () {
    var self = this;
    self.ppRateId = ko.observable();
    self.ppServiceID = ko.observable();
    self.ppDescription = ko.observable();    
    self.ppDetails = ko.observable();
    self.ppBringInfo = ko.observable();
    self.ppConditions = ko.observable();
    self.ppAdults = ko.observable();
    self.ppChildren = ko.observable();
    self.ppInfants = ko.observable();
    self.ppStudent = ko.observable();    
    self.ppPicture1 = ko.observable();    
    self.ppControlInventory = ko.observable();
    self.ppMaxadult = ko.observable();
    self.ppMaxchildren = ko.observable();
    self.ppMaxinfant = ko.observable();
    self.ppMaxstudent = ko.observable();
    self.ppRatestartdate = ko.observable();
    self.ppRateenddate = ko.observable();

    self.ImageUlr = ko.observable();
    self.PricePerAdultLabel = ko.observable();
    self.PricePerChildrenLabel = ko.observable();
    self.PricePerInfantLabel = ko.observable();
    self.PricePerStudentLabel = ko.observable();

    self.MapEntity = function (serviceEntity, ToursDto) {
        var compareKey = {
            'ppRateId': {
                key: function (data) {
                    return data.ppRateId;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ToursDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}