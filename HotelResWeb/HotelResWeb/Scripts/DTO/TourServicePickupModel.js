﻿
TourServicePickupModel = function () {
    var self = this;
    self.ppServiceID = ko.observable();
    self.ppDescription = ko.observable();    

    self.MapEntity = function (serviceEntity, Dto) {
        var compareKey = {
            'ppServiceID': {
                key: function (data) {
                    return data.ppServiceID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, Dto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}