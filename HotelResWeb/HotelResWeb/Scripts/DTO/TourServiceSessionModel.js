﻿
TourServiceSessionModel = function () {
    var self = this;
    self.SessionId = ko.observable();
    self.CompanyID = ko.observable();
    self.ServiceId = ko.observable();
    self.PromCode = ko.observable();
    self.CardType = ko.observable();
    self.TaxAmount = ko.observable();
    self.PromoAmount = ko.observable();
    self.Discount = ko.observable();
    self.Subtotal = ko.observable();
    self.total = ko.observable();
    self.Processor = ko.observable();

    self.FName = ko.observable();
    self.LName = ko.observable();
    self.Country = ko.observable();
    self.Phone = ko.observable();
    self.EMail = ko.observable();
    self.CardNumber = ko.observable();
    self.ExpireCard = ko.observable();
    self.ExpeMonth = ko.observable();
    self.ExpeYear = ko.observable();
    self.SecureCode = ko.observable();
    self.NameCard = ko.observable();

    self.MapEntity = function (serviceEntity, Dto) {
        var compareKey = {
            'CompanyID': {
                key: function (data) {
                    return data.CompanyID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, Dto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}