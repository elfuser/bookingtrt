﻿
ServiceModelLite = function () {
    var self = this;
    self.SessionId = ko.observable();   
    self.ppServiceID = ko.observable();
    self.ppServiceName = ko.observable();
    self.ppArrivalDate = ko.observable();    
    self.ppScheduleID = ko.observable();
    self.ppScheduleDesc = ko.observable();

    self.ppHotelPickUpID = ko.observable();
    self.ppHotelPickupHourID = ko.observable();
    self.ppPickupPlaceDesc = ko.observable();
    self.ppPickupScheduleDesc = ko.observable();

    self.ppAdults = ko.observable();
    self.ppChildren = ko.observable();
    self.ppInfants = ko.observable();
    self.ppStudents = ko.observable();    
    self.ppPriceAdults = ko.observable();
    self.ppPriceChildren = ko.observable();
    self.ppPriceInfants = ko.observable();
    self.ppPriceStudent = ko.observable();

    self.ppFormattedArrivalDate = ko.observable();
    self.ppTotalPerAdults = ko.observable();
    self.ppTotalPerChildren = ko.observable();
    self.ppTotalPerInfants = ko.observable();
    self.ppTotalPerStudents = ko.observable();
    self.ppShowTotalPerAdults = ko.observable();
    self.ppShowTotalPerChildren = ko.observable();
    self.ppShowTotalPerInfants = ko.observable();
    self.ppShowTotalPerStudents = ko.observable();

    self.ShowHideAdult = ko.observable();
    self.ShowHideChildren = ko.observable();
    self.ShowHideInfants = ko.observable();
    self.ShowHideStudents = ko.observable();
    self.ShowHidePickupSection = ko.observable();

    self.UniqueId = ko.observable();
    self.SubTotal = ko.observable();    

    self.ppRateId = ko.observable();
    self.ppPromoAmount = ko.observable();
    self.ppPromoAmountType = ko.observable();

    self.ppTotalPerAdultsPromo = ko.observable();
    self.ppTotalPerChildrenPromo = ko.observable();
    self.ppTotalPerInfantsPromo = ko.observable();
    self.ppTotalPerStudentsPromo = ko.observable();

    self.MapEntity = function (serviceEntity, Dto) {
        var compareKey = {
            'ppServiceID': {
                key: function (data) {
                    return data.ppServiceID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, Dto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}