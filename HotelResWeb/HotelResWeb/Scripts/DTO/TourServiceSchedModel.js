﻿
TourServiceSchedModel = function () {
    var self = this;
    self.ppScheduleID = ko.observable();
    self.ppHour = ko.observable();
    self.ppQty = ko.observable();
    self.ppAvailable = ko.observable();

    self.MapEntity = function (serviceEntity, Dto) {
        var compareKey = {
            'ppScheduleID': {
                key: function (data) {
                    return data.ppScheduleID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, Dto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}