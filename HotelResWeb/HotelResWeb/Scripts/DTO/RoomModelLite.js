﻿
RoomLiteModel = function () {

    var self = this;

    self.CompanyId = ko.observable();
    self.Adults = ko.observable();
    self.Kids = ko.observable();
    self.Infants = ko.observable();
    self.fromDt = ko.observable();
    self.toDt = ko.observable();
    self.Desc = ko.observable();
    self.Price = ko.observable();
    self.Code = ko.observable();
    self.Discount = ko.observable();
    self.PriceDisc = ko.observable();
    self.RoomType = ko.observable();
    self.TaxPerc = ko.observable();
    self.RateCode = ko.observable();
    self.Tachclass = ko.observable();
    self.PriceWithDiscLabel = ko.observable();
    self.PriceLabel = ko.observable();

    self.MapEntity = function (serviceEntity, RoomLiteDto) {
        var compareKey = {
            'Code': {
                key: function (data) {
                    return data.Code;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RoomLiteDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}