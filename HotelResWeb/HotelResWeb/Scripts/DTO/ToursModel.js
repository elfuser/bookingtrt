﻿
ToursModel = function () {
    var self = this;
    self.ppServiceID = ko.observable();
    self.ppVideoURL = ko.observable();
    self.ppDifficulty = ko.observable();
    self.ppDescription = ko.observable();
    self.ppDetails = ko.observable();
    self.ppAdults = ko.observable();
    self.ppChildren = ko.observable();
    self.ppInfants = ko.observable();
    self.ppPicture1 = ko.observable();
    self.ppQConditions = ko.observable();
    self.ppControlInventory = ko.observable();
    self.ImageUlr = ko.observable();
    self.PricePerAdultLabel = ko.observable();
    self.ppRateId = ko.observable();

    self.MapEntity = function (serviceEntity, ToursDto) {
        var compareKey = {
            'ppServiceID': {
                key: function (data) {
                    return data.ppServiceID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ToursDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}