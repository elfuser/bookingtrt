﻿

CompanyModel = function () {
    var self = this;
    self.ppURL = ko.observable();
    self.ppType = ko.observable();
    self.ppSpanish = ko.observable();
    self.ppHotel = ko.observable();
    self.ppServices = ko.observable();
    self.ppLegalName = ko.observable();
    self.ppTerms = ko.observable();
    self.ppWaiver= ko.observable();
    self.ppDescriptor= ko.observable();
    self.ppChildrenNote = ko.observable();
    self.ppTwitterText = ko.observable();
    self.ppFacebookURL = ko.observable();
    self.ppBankName = ko.observable();
    self.ppBankAccount = ko.observable();
    self.ppResEmailAlt = ko.observable();
    self.ppResEmail = ko.observable();
    self.ppResName = ko.observable();
    self.ppCfoEmail = ko.observable();
    self.ppCfoName = ko.observable();
    self.ppError= ko.observable();
    self.ppEmailHeader = ko.observable();
    self.ppActive = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppCompanyName = ko.observable();
    self.ppLegalID = ko.observable();
    self.ppCreationDate = ko.observable();
    self.ppAddress = ko.observable();
    self.ppTel1 = ko.observable();
    self.ppTel2 = ko.observable();
    self.ppTel3 = ko.observable();
    self.ppFax = ko.observable();
    self.ppContactName = ko.observable();
    self.ppContactEmail = ko.observable();
    self.ppWeb = ko.observable();
    self.ppEmail = ko.observable();
    self.ppConfirmPrefix = ko.observable();
    self.ppReservationNr = ko.observable();
    self.ppCommission = ko.observable();
    self.ppRentPorc = ko.observable();
    self.ppGuestNr = ko.observable();
    self.ppTaxPerc = ko.observable();
    self.ppCallTollFree = ko.observable();
    self.ppAddons = ko.observable();
    self.ppMaxAdults = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppMaxStudent = ko.observable();
    self.ppMsgAdults = ko.observable();
    self.ppMsgStudent = ko.observable();
    self.ppMsgChildren = ko.observable();
    self.ppMsgChildrenFree = ko.observable();
    self.ppChildrenFree = ko.observable();
    self.ppGoogleID = ko.observable();
    self.ppGoogleConvID = ko.observable();
    self.ppGoogleConvLabel = ko.observable();

    self.MapEntity = function (serviceEntity, CompanyDto) {
        var compareKey = {
            'ppCompanyCode': {
                key: function (data) {
                    return data.ppCompanyCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CompanyDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}
