﻿AgenciesHeaderModel = function () {
    var self = this;

    self.ppAgencyCode = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppAgencyName = ko.observable();
    self.ppAgencyTypeCode = ko.observable();
    self.ppCountry = ko.observable();
    self.ppProvince = ko.observable();
    self.ppCanton = ko.observable();
    self.ppDistrict = ko.observable();
    self.ppAddress = ko.observable();
    self.ppPhone = ko.observable();
    self.ppContactName = ko.observable();
    self.ppContactOccupation = ko.observable();
    self.ppContactEmail = ko.observable();
    self.ppContactOfficePhone = ko.observable();
    self.ppContactMobilePhone = ko.observable();
    self.ppNotificationEmailReservations = ko.observable();
    self.ppActive = ko.observable();
    self.ppAvailableCredit = ko.observable();
    self.ppCreated = ko.observable();
    self.ppModified = ko.observable();

    self.NumTarjeta = ko.observable();
    self.FecVencimiento = ko.observable();
    self.CodSeguridad = ko.observable();
    self.TipoTarjeta = ko.observable();

    self.FormatCreated = ko.observable();   

    self.MapEntity = function (serviceEntity, AgenciesHeaderDto) {
        var compareKey = {
            'ppAgencyCode': {
                key: function (data) {
                    return data.ppAgencyCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, AgenciesHeaderDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}