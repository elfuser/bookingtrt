﻿function ServiceCartVm(mainVm) {

    var self = this;
    self.TourlVm = ko.observable();

    self.getLogo = ko.observable();

    self.appData = mainVm;

    self.SessionId;
    self.ServiceId;

    self.TourList = ko.observableArray();
    self.TourServiceRateList = ko.observableArray();
    self.ServiceSchedAvailList = ko.observableArray();

    //-- Company variables
    self.CompanyCode = ko.observable();
    self.CompanyName = ko.observable();
    self.ppEmail = ko.observable();
    self.ppAddress = ko.observable();
    self.ppWeb = ko.observable();
    self.ppTel1 = ko.observable();
    self.Tax = ko.observable();
    self.Processor = ko.observable();
    self.RentPorc = ko.observable();
    self.ppFacebookURL = ko.observable();    
    self.ppMaxAdults = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppGoogleID = ko.observable();
    self.ppActive = ko.observable();    
    //--

    //-- Service variables
    self.ppDescription = ko.observable();
    self.ppDetails = ko.observable();
    self.ppBringInfo = ko.observable();
    self.ppConditions = ko.observable();
    self.ppPicture1 = ko.observable();
    self.ppPicture2 = ko.observable();
    self.ppPicture3 = ko.observable();
    self.ServiceImageList = ko.observableArray();
    //--

    self.TourServiceSessionInfo = ko.observable(new TourServiceSessionModel());
    self.ServiceInfo = ko.observable(new ServiceModelLite());
    self.InsertedServicesList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.CardTypeList = ko.observableArray();

    self.BNReqModel = ko.observable(new BNRequestModel());
    self.CredomaticReqModel = ko.observable(new CredomaticRequestModel());

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();
    self.PromoServiceList = ko.observableArray();

    self.CompanyTermsAndConditions = ko.observableArray();
    

    //*******************DEFAULT VALUES START*****************//
   
    self.TourlVm(mainVm);   

    //*******************DEFAULT VALUES END*****************//

    self.LoadCompany = function (data) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + self.CompanyCode() + "&sType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0))
                    {
                         self.CompanyName(response.GetCompanyResult["0"].ppCompanyName);
                         self.ppEmail(response.GetCompanyResult["0"].ppEmail);
                         self.ppAddress(response.GetCompanyResult["0"].ppAddress);
                         self.ppWeb(response.GetCompanyResult["0"].ppWeb);
                         self.CompanyCode(response.GetCompanyResult["0"].ppCompanyCode);
                         self.ppTel1(response.GetCompanyResult["0"].ppTel1);
                         self.Tax(response.GetCompanyResult["0"].ppTaxPerc);
                         self.Processor(response.GetCompanyResult["0"].Processor);
                         self.RentPorc(response.GetCompanyResult["0"].ppRentPorc);

                         self.ppFacebookURL(response.GetCompanyResult["0"].ppFacebookURL);                         
                         self.ppMaxAdults(response.GetCompanyResult["0"].ppMaxAdults);
                         self.ppMaxChildren(response.GetCompanyResult["0"].ppMaxChildren);
                         self.ppGoogleID(response.GetCompanyResult["0"].ppGoogleID);
                         self.ppActive(response.GetCompanyResult["0"].ppActive);                                                                                

                    }                    
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.SaveReservation = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.TourServiceSessionInfo));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceCart/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var ReservInfo = new TourServiceSessionModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.TourServiceSessionInfo(ReservInfo);
                        //call process card 
                        //by the processor type

                        self.ProcessCard();

                        bReturn = true;

                    }
                } else {
                    bReturn = false;
                    alertDialog("Error saving the reservation.");                    
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        //return bReturn;
    }
       

    self.PrepareServiceDtoForInsert = function (SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
        Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent) {      
        
        self.ServiceInfo = ko.observable(new ServiceModelLite());
        self.ServiceInfo().SessionId(SessionId);
        self.ServiceInfo().ppServiceID(ServiceID);
        self.ServiceInfo().ppServiceName(ServiceName);
        self.ServiceInfo().ppArrivalDate(ArrivalDate);
        self.ServiceInfo().ppScheduleID(ScheduleID);
        self.ServiceInfo().ppScheduleDesc(ScheduleDesc);
        self.ServiceInfo().ppHotelPickUpID(HotelPickUpID);        
        self.ServiceInfo().ppHotelPickupHourID(HotelPickupHourID);
        self.ServiceInfo().ppAdults(Adults);
        self.ServiceInfo().ppChildren(Children);
        self.ServiceInfo().ppInfants(Infants);
        self.ServiceInfo().ppStudents(Students);
        self.ServiceInfo().ppPriceAdults(PriceAdults);
        self.ServiceInfo().ppPriceChildren(PriceChildren);
        self.ServiceInfo().ppPriceInfants(PriceInfants);
        self.ServiceInfo().ppPriceStudent(PriceStudent);

        return self.ServiceInfo();
    }

    self.PrepareReservationInfoDtoForInsert = function () {
        //self.TourServiceSessionInfo().id(0);

        //self.TourServiceSessionInfo().CompanyID(self.CompanyCode());
        self.TourServiceSessionInfo().FName($("#firstname").val());
        self.TourServiceSessionInfo().LName($("#lastname").val());
        self.TourServiceSessionInfo().Country($('#ddl_country').find(":selected").val());
        self.TourServiceSessionInfo().Phone($("#phone").val());
        self.TourServiceSessionInfo().EMail($("#email").val());

        //self.TourServiceSessionInfo().PromCode($("#txtPromotionalCode").val());
        self.TourServiceSessionInfo().TaxAmount(parseFloat($("#taxAmount").val()));        
        //self.TourServiceSessionInfo().PromoAmount(0);
        //self.TourServiceSessionInfo().Discount(parseFloat($("#discount").val()));
        self.TourServiceSessionInfo().total(parseFloat($("#totalAmount").val()));
        self.TourServiceSessionInfo().Subtotal(parseFloat($("#Subtotal").val()));
        self.TourServiceSessionInfo().CardType($('#ddl_card_type').find(":selected").val());
        self.TourServiceSessionInfo().CardNumber($("#cardnumber").val());

        self.TourServiceSessionInfo().ExpeMonth($('#ddl_expiration_month').find(":selected").val());
        self.TourServiceSessionInfo().ExpeYear($('#ddl_expiration_year').find(":selected").val());
        self.TourServiceSessionInfo().NameCard($("#cardname").val());
    }

    //Save service in list - cookie
    self.SaveServices = function (SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
        Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent, ServiceListWithData) {

        var bReturn;
        var newAddons
        
        if (ServiceListWithData == null) {
            self.PrepareServiceDtoForInsert(SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
            Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent);

            self.InsertedServicesList.push(self.ServiceInfo());

            newServices = JSON.stringify(ko.toJS(self.InsertedServicesList()));
        } else {
            newServices = JSON.stringify(ko.toJS(ServiceListWithData));
        }
              
        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/SaveServices",
            async: false,
            crossDomain: true,
            data: newServices,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                self.appData.ProcessEnd();
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    //-- Save Reservation Info in cookie
    self.SaveReservationCookieOnly = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Load card types
    self.LoadCardTypes = function (bank) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ReservationService.svc" + "/json/GetCardTypes?Bank=" + bank,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCardTypesResult, function (key, value) {
                        var cardtypeDTO = new CardTypesModel();
                        cardtypeDTO.MapEntity(value, cardtypeDTO);
                        self.CardTypeList.push(cardtypeDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.LoadCountries = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/DireccionServices.svc" + "/json/GetCountries",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCountriesResult, function (key, value) {
                        var countryDTO = new CountriesModel();
                        countryDTO.MapEntity(value, countryDTO);
                        self.CountryList.push(countryDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Load countries in dropdown
    self.LoadCountriesInDropDownList = function () {
        var dll_countries = $("#ddl_country");

        if (self.CountryList() != null) {
            if (self.CountryList().length > 0) {
                $.each(self.CountryList(), function () {
                    dll_countries.append($("<option />").val(this.ppCountryIso3()).text(this.ppName()));
                });
            } else {
                dll_countries.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_countries.append($("<option />").val(-1).text("---"));
        }
    }

    //-- Load cardtypes in dropdown
    self.LoadCardTypesInDropDownList = function () {
        var dll_cardtypes = $("#ddl_card_type");

        if (objModelTours.CardTypeList() != null) {
            if (objModelTours.CardTypeList().length > 0) {
                $.each(objModelTours.CardTypeList(), function (i, item) {
                    dll_cardtypes.append($("<option />").val($.trim(item.ppCode())).text($.trim(item.ppName())));
                });
            } else {
                dll_cardtypes.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_cardtypes.append($("<option />").val(-1).text("---"));
        }
    }

    self.ProcessCard = function () {

        var bReturn;

        try {

            loading(true, 'Processing pay request ...');


            if (self.TourServiceSessionInfo().Processor().toLowerCase() === "bn") {
                bReturn = self.RequestBN();
            }

            if (self.TourServiceSessionInfo().Processor().toLowerCase() === "credomatic") {
                bReturn = self.ProcessPay();
            }

            loading(false);

        }
        catch (exc) {
            loading(false);
        }

        return bReturn;
    }


    self.RequestBN = function () {

        var bReturn = false;

        var NewReserv = JSON.stringify(ko.toJS(self.TourServiceSessionInfo));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceCart/GenerateBNParams",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {
                    if (response != '') {
                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {

                            var BNModel = new BNRequestModel();
                            BNModel.MapEntity(ds, BNModel);
                            self.BNReqModel(BNModel);

                            $("#dataForm").submit();

                            bReturn = true;

                        }
                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }


    self.ProcessPay = function () {

        var bReturn;

        var NewReserv = JSON.stringify(ko.toJS(self.TourServiceSessionInfo));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceCart/ProcessPay",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var CredomaticModel = new CredomaticRequestModel();
                        CredomaticModel.MapEntity(ds, CredomaticModel);
                        self.CredomaticReqModel(CredomaticModel);

                        var postPage = self.CredomaticReqModel().action();

                        $('#PostForm').attr('action', postPage);
                        $("#PostForm").submit();

                        bReturn = true;

                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    //-- Get Company terms and conditions
    self.GetCompanyTermsAndConditions = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/CompanyServices.svc" + "/json/GetCompanyTerm?sCompany_Code=" + self.CompanyCode() + '&sMode=SEL&iCulture=1',
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetCompanyTermResult != null && response.GetCompanyTermResult != '') {
                        var terms = '';
                        var valuesarray = response.GetCompanyTermResult.split("~");
                        if (valuesarray.length > 1) {
                            terms = valuesarray[1];
                        }
                        self.CompanyTermsAndConditions(terms);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //------------------------------- Begin - Get cookies info -------------------------------//

    //-- Get tour service info from the cookie
    self.GetTourServiceCookieInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetTourServiceCookieInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);               
                    self.TourServiceSessionInfo().MapEntity(ds, self.TourServiceSessionInfo());

                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedServices = function () {

        //self.SavedServicesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetSavedServices",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var Info = new ServiceModelLite();
                        Info.MapEntity(value, Info);

                        var arrivaldate = new Date(parseInt(Info.ppArrivalDate().substr(6)));
                        Info.ppFormattedArrivalDate(formatDate_v2(arrivaldate, "/", "mmddyyyy"))
                        Info.ppArrivalDate(arrivaldate)

                        Info.ppTotalPerAdults('$' + parseFloat(Info.ppPriceAdults() * Info.ppAdults()).toFixed(2));
                        Info.ppTotalPerChildren('$' + parseFloat(Info.ppPriceChildren() * Info.ppChildren()).toFixed(2));
                        Info.ppTotalPerInfants('$' + parseFloat(Info.ppPriceInfants() * Info.ppInfants()).toFixed(2));
                        Info.ppTotalPerStudents('$' + parseFloat(Info.ppPriceStudent() * Info.ppStudents()).toFixed(2));

                        if (Info.ppAdults() > 0) {
                            Info.ShowHideAdult("ShowSummaryItem");
                        } else {
                            Info.ShowHideAdult("HideSummaryItem");
                        }

                        if (Info.ppChildren() > 0) {
                            Info.ShowHideChildren("ShowSummaryItem");
                        } else {
                            Info.ShowHideChildren("HideSummaryItem");
                        }

                        if (Info.ppInfants() > 0) {
                            Info.ShowHideInfants("ShowSummaryItem");
                        } else {
                            Info.ShowHideInfants("HideSummaryItem");
                        }

                        if (Info.ppStudents() > 0) {
                            Info.ShowHideStudents("ShowSummaryItem");
                        } else {
                            Info.ShowHideStudents("HideSummaryItem");
                        }

                        if (Info.ppHotelPickUpID() > 0) {
                            Info.ShowHidePickupSection("ShowSummaryItem");
                        } else {
                            Info.ShowHidePickupSection("HideSummaryItem");
                        }

                        Info.UniqueId(s4() + s4());

                        self.InsertedServicesList.push(Info);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;

        var NewListObj = JSON.stringify(ko.toJS(objModelTours.PromoServiceList()));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/SavePromoCodeInfoService",
            async: false,
            crossDomain: true,
            data: NewListObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, iRateID) {

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmountService?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&iRateID=" + iRateID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountServiceResult != null) {
                        if (response.GetPromoCodeAmountServiceResult.length > 0) {
                            $.each(response.GetPromoCodeAmountServiceResult, function (key, value) {
                                var objDTO = new PromoCodeModel();
                                objDTO.MapEntity(value, objDTO);
                                self.PromoCodeDTO(objDTO)
                                bResult = true;
                            })
                        } else {
                            self.PromoCodeDTO(null);
                        }
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    self.GetPromoCodeInfoServices = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/GetPromoCodeInfoService",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var Info = new PromoCodeModel();
                            Info.MapEntity(value, Info);
                            self.PromoServiceList.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //------------------------------- End - Get cookies info -------------------------------//
   

    function formatDate(date, separator_char) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator_char);
    }
 

    //-- Continue Shopping Button
    ContinueShopping_Button = function (data, event) {
        try {            

            loading(true);

            var url = $("#txtTrackingURLBase").val() + '/ToursServices/ToursServices?hotelcode=' + $.trim(self.CompanyCode());
            window.location.href = url;

            loading(false);

        }
        catch (ex) {
            loading(false);
        }
    }
    //-- 


    self.InitModel = function () {                                               

        var CompanyCode = "", ServiceId = 0;

        self.GetTourServiceCookieInfo();

        //-- Read company code from cookie
        if (self.TourServiceSessionInfo() != null) {
            if (self.TourServiceSessionInfo().CompanyID() != null) {
                if (self.TourServiceSessionInfo().CompanyID() != '') {
                    CompanyCode = $.trim(self.TourServiceSessionInfo().CompanyID());
                }
            }
                       
        }
        //--

        if (CompanyCode == "") {
            CompanyCode = $("#hdf_HotelCode").val();
            if (CompanyCode == "") {
                alertDialog("No company provided");
                return false;
            }
        }        

        self.CompanyCode(CompanyCode);        

        //--Test
        //self.CompanyCode("BDIHS");
        
        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + $.trim(self.CompanyCode()) + "&photoNumber=4");
        
        self.LoadCompany();                                               

        //Get services saved in cookie
        self.GetSavedServices();
        self.LoadCountries();

        //Get PromoCode Info
        self.GetPromoCodeInfoServices();

        //Get company terms and conditions
        self.GetCompanyTermsAndConditions();
    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var objModelTours;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              
               
               try{                              
                   loading(true, 'Please wait ...');                   
                                      
                   objModelTours = new ServiceCartVm();
                   ko.applyBindings(objModelTours);

                   var urlCompanyLogo = objModelTours.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);

                   $("#idCompanyLogo").click(function () {
                       var url = "http://" + objModelTours.ppWeb();
                       window.location.href = url;
                   });

                   //Menu options
                   $("#progress_toursservices").removeClass("progress_menu_selected");
                   $("#progress_tourscart").addClass("progress_menu_selected");
                   $("#progress_toursconfirmation").removeClass("progress_menu_selected");
                   $("#cart_items").hide();
                   $("#progressmenu li#progress_tourscart").find('a').click(function () { return false });
                   $("#progressmenu li#progress_tourscart").find('a').css("cursor", "default");
                   //

                   //Load Footer Info
                   $("#lblCompany").text(objModelTours.CompanyName());
                   $("#lblAddress").text(objModelTours.ppAddress());
                   $("#lblPhoneCo").text(objModelTours.ppTel1());
                   $("#lnkEmail").text(objModelTours.ppEmail());
                   //

                   //-- Adapt Header ContentPlaceHolder
                   $("#MasterDivLeftPanelContent").removeClass("col-sm-3");
                   $("#MasterDivLeftPanelContent").hide();

                   $("#MasterDivRightPanelContent").addClass("col-sm-3");
                   $("#MasterDivRightPanelContent").show();

                   //$("#MasterDivMainContent").removeClass("col-sm-9");
                   //$("#MasterDivMainContent").addClass("col-sm-8");

                   $("#MasterDivMainContent").addClass("MasterDivMainContentStyle");                   
                   //--                 

                   //---- Validate promo code
                   if (objModelTours.PromoServiceList() != null) {
                       if (objModelTours.PromoServiceList().length > 0) {
                           $("#txtPromotionalCode").val(objModelTours.PromoServiceList()[0].ppPromoCode());
                           $('#txtPromotionalCode').prop('readonly', true);
                           $("#btnPromoCode").html('Revert');
                           $("#PromoApprovedIcon").show(300);
                       }
                   }
                   //--
                  
                   //---- Show summary section
                   if (objModelTours.InsertedServicesList() != null) {
                       if (objModelTours.InsertedServicesList().length > 0) {
                           $("#SummarySection").show();
                           updateSummaryServicesPromo();
                           LoadSummaryTotals();
                           $("#NoServicesSection").hide();
                           $("#pay_now_button").prop('disabled', false);
                       } else {
                           $("#SummarySection").hide();
                       }
                   }else {
                       $("#SummarySection").hide();
                   }

                   //-- Delete add-on from list
                   $(".RemoveServiceLink").click(function (e) {
                       e.preventDefault();
                       e.stopPropagation();
                       var ServiceUniqueIdToDelete = $(this).closest('tr').parent().closest('tr').attr("id");
                       $(this).closest('tr').parent().closest('tr')
                       .children('td')
                       .animate({ padding: 0 })
                       .wrapInner('<div />')
                       .children()
                       .slideUp(function () { $(this).closest('tr').parent().closest('tr').remove(); });

                       //-- Delete service and insert the new service list to the cookie
                       deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete);
                       //--
                   });
                   //

                   //-- Load countries in dropdownlist
                   objModelTours.LoadCountriesInDropDownList();

                   $('#ddl_country').selectpicker('refresh');

                   //-- Show card payment info
                   var ExcludeCardValidation = false;
                   if (objModelTours.TourServiceSessionInfo() != null) {
                       if (objModelTours.TourServiceSessionInfo().Processor() != null) {
                           if (objModelTours.TourServiceSessionInfo().Processor() != '') {
                               var paymenttype = objModelTours.TourServiceSessionInfo().Processor().toString().toLowerCase();
                               paymenttype = 'credomatic' //uncomment for demo

                               //Load card types
                               objModelTours.LoadCardTypes(paymenttype);

                               //-- Load countries in dropdownlist
                               objModelTours.LoadCardTypesInDropDownList();

                               switch (paymenttype) {
                                   case "bn":
                                       $(".cardinfosection").hide();
                                       ExcludeCardValidation = true;
                                       break;
                                   default:
                                       $("#cardinfosection").show();
                                       break;
                               }

                           }
                       }
                   }

                   $("#btnPromoCode").click(function () {
                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "") {
                               alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {
                               loading(true, 'Please wait ...');
                               objModelTours.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((objModelTours.PromoList().length === 0) || (objModelTours.PromoList() === null) || (objModelTours.PromoList() === undefined)) {
                                   alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppEndDate(), '/'));

                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       alertDialog('Invalid promotional code');
                                   } else if (CurrentDate > DateTo) {
                                       alertDialog('Promotional code has expired');
                                   } else {
                                       if (objModelTours.InsertedServicesList() != null) {
                                           if (objModelTours.InsertedServicesList().length > 0) {
                                               updateSummaryServicesPromo();
                                               LoadSummaryTotals();
                                           }

                                           //--Save promo code info in cookie
                                           if (objModelTours.PromoServiceList() != null) {
                                               if (objModelTours.PromoServiceList().length > 0) {
                                                   objModelTours.SavePromoCodeCookie();
                                               }
                                           }
                                           //--

                                           alertDialog('Promotional code was applied');

                                           $('#txtPromotionalCode').prop('readonly', true);
                                           $("#btnPromoCode").html('Revert');
                                           $("#PromoApprovedIcon").show(300);
                                       }
                                   }
                               }

                               loading(false);
                           }
                           catch (e) {
                               alertDialog('Error applying promotional code.');
                               loading(false);
                           }
                       } else {
                           loading(true, 'Please wait ...');

                           $("#txtPromotionalCode").val('');
                           $('#txtPromotionalCode').prop('readonly', false);
                           $("#btnPromoCode").html('Validate');
                           $("#PromoApprovedIcon").hide();
                           
                           objModelTours.PromoServiceList(null);                          

                           if (objModelTours.InsertedServicesList() != null) {
                               if (objModelTours.InsertedServicesList().length > 0) {
                                   $.each(objModelTours.InsertedServicesList(), function (i, item) {
                                       item.ppTotalPerAdultsPromo(null);
                                       item.ppTotalPerChildrenPromo(null);
                                       item.ppTotalPerInfantsPromo(null);
                                       item.ppTotalPerStudentsPromo(null);
                                   })

                                   updateSummaryServicesPromo();
                                   LoadSummaryTotals();
                               }
                           }

                           deleteCookie("PromoCodeInfoServices");

                           loading(false);
                       }
                   })

                   //-- Validate fields in form
                   FormFieldsValidationSubmit(objModelTours, ExcludeCardValidation);

                   //----

                   //-- Return to website link
                   $("#returntoSiteLink").attr("href", "http://" + objModelTours.ppWeb());
                   $("#returntoSiteLink").text("Return to Web Site");
                   //--                   

                   //-- YouTube, Facebook and Twitter links / footer                   
                   $("#facebooklink").attr("href", "http://www.facebook.com/" + objModelTours.ppFacebookURL());
                   //

                   //-- Pay Now button from terms and conditions popup
                   $("#btnPayNow").click(function () {
                       try {
                           if ($('#acceptTerms').prop('checked')) {
                               $('#lblTermsWarning').hide();
                               $('#dialogTermConditionsPopup').modal('hide'); //-- Hide/close terms and conditions modal
                               loading(true, 'Please wait ...');
                               objModelTours.SaveReservation();
                               loading(false);
                           } else {
                               $('#lblTermsWarning').show();
                           }

                       } catch (ex) {
                           loading(false);
                       }
                   })

                   $('#acceptTerms').change(function () {
                       if ($(this).prop('checked')) {
                           $('#lblTermsWarning').hide();
                       } else {
                           $('#lblTermsWarning').show();
                       }
                   });

                   $('#dialogTermConditionsPopup').on('hidden.bs.modal', function () {
                       $('#pay_now_button').prop('disabled', false);
                   })
                   //--

                   loading(false);
                  
               }
               catch (ex) {
                   loading(false);
                   alertDialog('Error loading service details. Error: ' + ex.message);
               }

               
           }

           
);
//*******************                              ******************//

function updateSummaryServicesPromo() {
    if (objModelTours.InsertedServicesList().length > 0) {
        $.each(objModelTours.InsertedServicesList(), function (i, item) {
            objModelTours.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.ppRateId())
            if (objModelTours.PromoCodeDTO() != null) {
                if (objModelTours.PromoCodeDTO().ppAmount() > 0) {

                    //--Add PromoCodeDTO object to list (save this list to cookie)
                    objModelTours.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));
                    objModelTours.PromoCodeDTO().ppServiceID(item.ppServiceID());
                    if (objModelTours.PromoServiceList() == null) {
                        objModelTours.PromoServiceList = ko.observableArray();
                    }
                    objModelTours.PromoServiceList.push(objModelTours.PromoCodeDTO())
                    //--                                                                                                                   

                    var originalprice = 0;
                    var price = 0;

                    if (item.ppAdults() > 0) {
                        originalprice = item.ppPriceAdults() * item.ppAdults();
                        price = item.ppPriceAdults() * item.ppAdults();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerAdultsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperadultspromo_" + item.UniqueId()).show();
                            $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerAdultsPromo('');
                            $("#spantotalperadultspromo_" + item.UniqueId()).hide();
                            $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }

                    if (item.ppChildren() > 0) {
                        originalprice = item.ppPriceChildren() * item.ppChildren();
                        price = item.ppPriceChildren() * item.ppChildren();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerChildrenPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperachildrenpromo_" + item.UniqueId()).show();
                            $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerChildrenPromo('');
                            $("#spantotalperachildrenpromo_" + item.UniqueId()).hide();
                            $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }

                    if (item.ppInfants() > 0) {
                        originalprice = item.ppPriceInfants() * item.ppInfants();
                        price = item.ppPriceInfants() * item.ppInfants();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerInfantsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperinfantspromo_" + item.UniqueId()).show();
                            $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerInfantsPromo('');
                            $("#spantotalperinfantspromo_" + item.UniqueId()).hide();
                            $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }


                    if (item.ppStudents() > 0) {
                        originalprice = item.ppPriceStudent() * item.ppStudents();
                        price = item.ppPriceStudent() * item.ppStudents();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerStudentsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperstudentspromo_" + item.UniqueId()).show();
                            $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerStudentsPromo('');
                            $("#spantotalperstudentspromo_" + item.UniqueId()).hide();
                            $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }


                }
            } else {
                $("#spantotalperadultspromo_" + item.UniqueId()).hide();
                $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperachildrenpromo_" + item.UniqueId()).hide();
                $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperinfantspromo_" + item.UniqueId()).hide();
                $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperstudentspromo_" + item.UniqueId()).hide();
                $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "none");
            }
        });

    }
}

function formatDate_v2(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";

    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var dayname = weekday[date.getDay()];
    var monthname = months[date.getMonth()];
    var daynumber = date.getDate();
    var ordinalday;

    if (daynumber == 1 || daynumber == 21 || daynumber == 31) {
        ordinalday = daynumber + 'st'
    } else if (daynumber == 2 || daynumber == 22) {
        ordinalday = daynumber + 'nd'
    } else if (daynumber == 3 || daynumber == 23) {
        ordinalday = daynumber + 'rd'
    } else {
        ordinalday = daynumber + 'th'
    }
   
    returnedvalue = monthname + ', ' + dayname + ' ' + ordinalday;

    return returnedvalue;
}

//-- Load cardtypes in dropdown
function LoadSchedHoursInDropDownList() {
    var dll = $("#ddl_schedhours");

    $("#ddl_schedhours option").remove();

    if (objModelTours.ServiceSchedAvailList() != null) {
        if (objModelTours.ServiceSchedAvailList().length > 0) {
            $.each(objModelTours.ServiceSchedAvailList(), function (i, item) {
                dll.append($("<option />").val(item.ppScheduleID()).text($.trim(item.ppHour())));
            });

            $(".SchedHoursSection").show();
            
        } else {
            dll.append($("<option />").val(0).text("---"));
        }
    } else {
        dll.append($("<option />").val(0).text("---"));
    }
}

function deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete) {

    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            var filteredlist;

            filteredlist = objModelTours.InsertedServicesList().filter(function (elem) {
                return elem.UniqueId() !== ServiceUniqueIdToDelete;
            });

            objModelTours.InsertedServicesList([]); //clean InsertedServicesList array
            objModelTours.InsertedServicesList(filteredlist.slice()); // fill the array with the current items, without the deleted service
            bReturn = objModelTours.SaveServices(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
                                                 , objModelTours.InsertedServicesList());

            //-- Show summary section
            //Get services saved in cookie           
            if (objModelTours.InsertedServicesList() != null) {
                if (objModelTours.InsertedServicesList().length > 0) {
                    $("#SummarySection").show();
                    LoadSummaryTotals();
                    $("#NoServicesSection").hide();
                    $("#pay_now_button").prop('disabled', false);
                } else {
                    $("#SummarySection").hide();                    
                    $("#NoServicesSection").show();
                    $("#pay_now_button").prop('disabled', true);
                }
            } else {
                $("#SummarySection").hide();
                $("#NoServicesSection").show();
                $("#pay_now_button").prop('disabled', true);
            }

            //-- Delete add-on from list
            $(".RemoveServiceLink").click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                var ServiceUniqueIdToDelete = $(this).closest('tr').parent().closest('tr').attr("id");
                $(this).closest('tr').parent().closest('tr')
                .children('td')
                .animate({ padding: 0 })
                .wrapInner('<div />')
                .children()
                .slideUp(function () { $(this).closest('tr').parent().closest('tr').remove(); });

                //-- Delete service and insert the new service list to the cookie
                deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete);
                //--
            });
            //
            //--
        }

    }

    LoadSummaryTotals();
}

function LoadSummaryTotals() {
    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            var moneysign = '$'
            var subtotal = 0, tax = 0, rent = 0, total = 0, TotalPerAdultsPromo, TotalPerChildrenPromo, TotalPerInfantsPromo, TotalPerStudentsPromo;

            $.each(objModelTours.InsertedServicesList(), function (key, item) {
                TotalPerAdultsPromo = 0
                TotalPerChildrenPromo = 0
                TotalPerInfantsPromo = 0
                TotalPerStudentsPromo = 0

                //-- If promo code is applied to service
                if (item.ppTotalPerAdultsPromo() != '' && item.ppTotalPerAdultsPromo() != null) {
                    var temptext = item.ppTotalPerAdultsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerAdultsPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerChildrenPromo() != '' && item.ppTotalPerChildrenPromo() != null) {
                    var temptext = item.ppTotalPerChildrenPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerChildrenPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerInfantsPromo() != '' && item.ppTotalPerInfantsPromo() != null) {
                    var temptext = item.ppTotalPerInfantsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerInfantsPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerStudentsPromo() != '' && item.ppTotalPerStudentsPromo() != null) {
                    var temptext = item.ppTotalPerStudentsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerStudentsPromo = parseFloat(temptext);
                    }
                }
                //--

                if (TotalPerAdultsPromo > 0 || TotalPerChildrenPromo > 0 || TotalPerInfantsPromo > 0 || TotalPerStudentsPromo > 0) {
                    //if promo code is applied to service
                    subtotal += TotalPerAdultsPromo + TotalPerChildrenPromo + TotalPerInfantsPromo + TotalPerStudentsPromo;
                } else {
                    subtotal += item.SubTotal();
                }

            })           

            $("#lblSubTotal").text(subtotal.toFixed(2));
            if (objModelTours.Tax() == 0) {
                $("#lblTax").text("0.00")
            } else {
                tax = subtotal * 0.13;
                $("#lblTax").text(tax.toFixed(2));
            }

            rent = (subtotal * objModelTours.RentPorc()) / 100;
            $("#lblRent").text(rent.toFixed(2));

            total = subtotal + tax + rent;
            $("#lblTOTAL").text(total.toFixed(2));

            $("#Subtotal").val(parseFloat(subtotal));
            $("#totalAmount").val(parseFloat(total));
            //$("#discount").val(parseFloat(Discount));            
            $("#taxAmount").val(parseFloat(tax));
        }
    }
}

function FormFieldsValidationSubmit(coObj, ExcludeCardValidation) {
    //-- Form fields validations
    $('#checkout_form').bootstrapValidator({       
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            lastname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    },
                    identical: {
                        field: 'confirmemail',
                        message: 'The email and its confirm are not the same'
                    }
                }
            },
            confirmemail: {
                validators: {
                    notEmpty: {
                        message: 'Please confirm your email address'
                    },
                    emailAddress: {
                        message: 'Please confirm a valid email address'
                    },
                    identical: {
                        field: 'email',
                        message: 'The email and its confirm are not the same'
                    }
                }
            },
            cardname: {
                excluded: ExcludeCardValidation,
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Please supply your card name'
                    }
                }
            },
            ddl_card_type: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'The type is required'
                    }
                }
            },
            cardnumber: {
                excluded: ExcludeCardValidation,
                validators: {
                    creditCard: {
                        message: 'The card number is not valid'
                    },
                    notEmpty: {
                        message: 'Please supply your card number'
                    }
                }
            },
            expiration_month_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    }
                }
            },
            expiration_year_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration year'
                    }
                }
            },
            txtCVC: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    },
                    cvv: {
                        creditCardField: 'cardnumber',
                        message: 'The CVV number is not valid'
                    }
                }
            }
            //,
            //acceptTerms: {
            //    validators: {
            //        notEmpty: {
            //            message: 'You have to accept General Terms and Conditions'
            //        }
            //    }
            //}
        }
    })
    .on('success.form.bv', function (e) {
        //$('#success_message').slideDown({ opacity: "show" }, "slow") //

        // Prevent form submission
        e.preventDefault();       

        try {

            TermAndConditionsDialog(objModelTours.CompanyTermsAndConditions());

            //loading(true, 'Please wait ...');            
            //objModelTours.SaveReservation();
            //loading(false);

            //var url = $("#txtTrackingURLBase").val() + '/ServiceConfirmation/ServiceConfirmation?hotelcode=' + $.trim(coObj.CompanyCode());
            //window.location.href = url; //uncomment for demo

        } catch (ex) {
            loading(false);
        }

        // Use Ajax to submit form data
        //$.post($form.attr('action'), $form.serialize(), function (result) {
        //    console.log(result);
        //}, 'json');
    });

}

function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);    
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');    
}

function TermAndConditionsDialog(Content) {
    $('#lblTermsWarning').hide();
    $("#termconditions").html(Content);
    $("#dialogTermConditionsPopup").modal({ backdrop: "static" });
    $('#dialogTermConditionsPopup').modal('show');
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--

