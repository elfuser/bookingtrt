﻿function ServiceDetailVm(mainVm) {

    var self = this;
    self.TourlVm = ko.observable();

    self.getLogo = ko.observable();

    self.appData = mainVm;

    self.SessionId;
    self.ServiceId;

    self.TourList = ko.observableArray();
    self.TourServiceRateList = ko.observableArray();
    self.ServiceSchedAvailList = ko.observableArray();
    self.PickupPlaces = ko.observableArray();
    self.PickupHours = ko.observableArray();
    
    //-- Company variables
    self.CompanyCode = ko.observable();
    self.CompanyName = ko.observable();
    self.ppEmail = ko.observable();
    self.ppAddress = ko.observable();
    self.ppWeb = ko.observable();
    self.ppTel1 = ko.observable();
    self.Tax = ko.observable();
    self.Processor = ko.observable();
    self.RentPorc = ko.observable();
    self.ppFacebookURL = ko.observable();    
    self.ppMaxAdults = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppGoogleID = ko.observable();
    self.ppActive = ko.observable();    
    //--

    //-- Service variables
    self.ppDescription = ko.observable();
    self.ppDetails = ko.observable();
    self.ppBringInfo = ko.observable();
    self.ppConditions = ko.observable();
    self.ppPicture1 = ko.observable();
    self.ppPicture2 = ko.observable();
    self.ppPicture3 = ko.observable();
    self.ServiceImageList = ko.observableArray();
    self.ppServiceSelectedRateId = ko.observable();
    //--

    self.TourServiceSessionInfo = ko.observable(new TourServiceSessionModel());
    self.ServiceInfo = ko.observable(new ServiceModelLite());
    self.InsertedServicesList = ko.observableArray();
    self.SavedServicesList = ko.observableArray();

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();
    self.PromoServiceList = ko.observableArray();
    

    //*******************DEFAULT VALUES START*****************//
   
    self.TourlVm(mainVm);   

    //*******************DEFAULT VALUES END*****************//

    self.LoadCompany = function (data) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + self.CompanyCode() + "&sType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0))
                    {
                         self.CompanyName(response.GetCompanyResult["0"].ppCompanyName);
                         self.ppEmail(response.GetCompanyResult["0"].ppEmail);
                         self.ppAddress(response.GetCompanyResult["0"].ppAddress);
                         self.ppWeb(response.GetCompanyResult["0"].ppWeb);
                         self.CompanyCode(response.GetCompanyResult["0"].ppCompanyCode);
                         self.ppTel1(response.GetCompanyResult["0"].ppTel1);
                         self.Tax(response.GetCompanyResult["0"].ppTaxPerc);
                         self.Processor(response.GetCompanyResult["0"].Processor);
                         self.RentPorc(response.GetCompanyResult["0"].ppRentPorc);
                         

                         self.ppFacebookURL(response.GetCompanyResult["0"].ppFacebookURL);                         
                         self.ppMaxAdults(response.GetCompanyResult["0"].ppMaxAdults);
                         self.ppMaxChildren(response.GetCompanyResult["0"].ppMaxChildren);
                         self.ppGoogleID(response.GetCompanyResult["0"].ppGoogleID);
                         self.ppActive(response.GetCompanyResult["0"].ppActive);                                                                                

                    }                    
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }
    
    self.GetServiceByID = function (serviceId) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetServiceByID?ServiceID=" + serviceId + "&Culture=en-US",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if ((response.GetServiceByIDResult != null) && (response.GetServiceByIDResult.length > 0)) {                                             
                        self.ppDescription(response.GetServiceByIDResult["0"].ppDescription);
                        self.ppDetails(response.GetServiceByIDResult["0"].ppDetails);
                        self.ppBringInfo(response.GetServiceByIDResult["0"].ppBringInfo);
                        self.ppConditions(response.GetServiceByIDResult["0"].ppConditions);
                        //self.ServiceImageList.push(response.GetServiceByIDResult["0"].ppPicture1);
                        //self.ServiceImageList.push(response.GetServiceByIDResult["0"].ppPicture2);
                        //self.ServiceImageList.push(response.GetServiceByIDResult["0"].ppPicture3);                                                                                                               

                        $.each(response.GetServiceByIDResult, function (key, value) {
                            var Info = new TourServiceRateModel();
                            Info.MapEntity(value, Info);

                            Info.PricePerAdultLabel('$' + Info.ppAdults())
                            Info.PricePerChildrenLabel('$' + Info.ppChildren)
                            Info.PricePerInfantLabel('$' + Info.ppInfants())
                            Info.PricePerStudentLabel('$' + Info.ppStudent())

                            self.TourServiceRateList.push(Info);
                        })
                    }
                    
                    self.ServiceImageList.push($("#txtTrackingURLBase").val() + "/Handlers/getPhotoSvc.ashx?ServiceID=" + serviceId + "&photoNumber=1");
                    self.ServiceImageList.push($("#txtTrackingURLBase").val() + "/Handlers/getPhotoSvc.ashx?ServiceID=" + serviceId + "&photoNumber=2");
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetServiceSchedAvailability = function (serviceId, arrivaldate, qtypeople, pickuphour) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetServiceSchedAvailability?ServiceID=" + serviceId + "&ArrivalDate=" + arrivaldate + "&QtyPeople=" + qtypeople + "&PickupHour=" + pickuphour,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if ((response.GetServiceSchedAvailabilityResult != null) && (response.GetServiceSchedAvailabilityResult.length > 0)) {
                        $.each(response.GetServiceSchedAvailabilityResult, function (key, value) {
                            var Info = new TourServiceSchedModel();
                            Info.MapEntity(value, Info);                           
                            self.ServiceSchedAvailList.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);                
            }
        });
    }

    self.GetServicePickupPlaces = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetServicePickup?CompanyCode=" + self.CompanyCode(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if ((response.GetServicePickupResult != null) && (response.GetServicePickupResult.length > 0)) {
                        $.each(response.GetServicePickupResult, function (key, value) {
                            var Info = new TourServicePickupModel();
                            Info.MapEntity(value, Info);
                            self.PickupPlaces.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
            }
        });
    }

    self.GetServicePickupHours = function (PlaceID) {

        objModelTours.PickupHours([]); //clean pickup hours list

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetServicePickupHours?PlaceID=" + PlaceID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if ((response.GetServicePickupHoursResult != null) && (response.GetServicePickupHoursResult.length > 0)) {
                        $.each(response.GetServicePickupHoursResult, function (key, value) {
                            var Info = new TourServicePickupModel();
                            Info.MapEntity(value, Info);
                            self.PickupHours.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
            }
        });
    }
 
    self.GetCurrentPage = function () {

        var currentpage = "";

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetCurrentPage",
            async: false,
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                currentpage = response;                
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }

        });

        return currentpage;
    }

    self.SaveNextPage = function (Page) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveCurrentPage?CurrentPage=" + Page,
            async: false,
            crossDomain: true,
            success: function (response) {
                return true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                return false;
            }
        });

        return true;
    }

    self.PrepareServiceDtoForInsert = function (SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
        Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent, PickupPlaceDesc, PickUpHourDesc) {      
        
        self.ServiceInfo = ko.observable(new ServiceModelLite());
        self.ServiceInfo().SessionId(SessionId);
        self.ServiceInfo().ppServiceID(ServiceID);
        self.ServiceInfo().ppServiceName(ServiceName);
        self.ServiceInfo().ppArrivalDate(ArrivalDate);
        self.ServiceInfo().ppScheduleID(ScheduleID);
        self.ServiceInfo().ppScheduleDesc(ScheduleDesc);
        self.ServiceInfo().ppHotelPickUpID(HotelPickUpID);        
        self.ServiceInfo().ppHotelPickupHourID(HotelPickupHourID);
        self.ServiceInfo().ppAdults(Adults);
        self.ServiceInfo().ppChildren(Children);
        self.ServiceInfo().ppInfants(Infants);
        self.ServiceInfo().ppStudents(Students);
        self.ServiceInfo().ppPriceAdults(PriceAdults);
        self.ServiceInfo().ppPriceChildren(PriceChildren);
        self.ServiceInfo().ppPriceInfants(PriceInfants);
        self.ServiceInfo().ppPriceStudent(PriceStudent);
        self.ServiceInfo().ppPickupPlaceDesc(PickupPlaceDesc);
        self.ServiceInfo().ppPickupScheduleDesc(PickUpHourDesc);
        self.ServiceInfo().ppRateId(self.ppServiceSelectedRateId());
        self.ServiceInfo().SubTotal((Adults * PriceAdults) + (Children * PriceChildren) + (Infants * PriceInfants) + (Students * PriceStudent));

        return self.ServiceInfo();
    }

    self.PrepareDtoForInsert = function () {
        self.ReservationDTO().id(0);

        self.ReservationDTO().CompanyID(self.CompanyCode());
        self.ReservationDTO().FName($("#firstname").val());
        self.ReservationDTO().LName($("#lastname").val());
        self.ReservationDTO().Country($('#ddl_country').find(":selected").val());
        self.ReservationDTO().Phone($("#phone").val());
        self.ReservationDTO().EMail($("#email").val());

        self.ReservationDTO().PromCode($("#txtPromotionalCode").val());
        self.ReservationDTO().TaxAmount(parseFloat($("#taxAmount").val()));
        self.ReservationDTO().AddonAmount(parseFloat($("#addonAmount").val()));
        self.ReservationDTO().PromoAmount(0);
        self.ReservationDTO().Discount(parseFloat($("#discount").val()));
        self.ReservationDTO().total(parseFloat($("#totalAmount").val()));
        self.ReservationDTO().Subtotal(parseFloat($("#Subtotal").val()));

        self.ReservationDTO().CardType($('#ddl_card_type').find(":selected").val());

    }



    //Save service in list - cookie
    self.SaveServices = function (SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
        Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent, PickupPlaceDesc, PickUpHourDesc, ServiceListWithData) {

        var bReturn;
        var newAddons
        
        if (ServiceListWithData == null) {
            self.PrepareServiceDtoForInsert(SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
            Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent, PickupPlaceDesc, PickUpHourDesc);

            self.InsertedServicesList.push(self.ServiceInfo());

            newServices = JSON.stringify(ko.toJS(self.InsertedServicesList()));
        } else {
            newServices = JSON.stringify(ko.toJS(ServiceListWithData));
        }
              
        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/SaveServices",
            async: false,
            crossDomain: true,
            data: newServices,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                self.appData.ProcessEnd();
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    //-- Save Reservation Info in cookie
    self.SaveReservationCookieOnly = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;

        var NewListObj = JSON.stringify(ko.toJS(objModelTours.PromoServiceList()));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/SavePromoCodeInfoService",
            async: false,
            crossDomain: true,
            data: NewListObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, iRateID) {        

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmountService?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&iRateID=" + iRateID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountServiceResult != null) {
                        if (response.GetPromoCodeAmountServiceResult.length > 0) {
                            $.each(response.GetPromoCodeAmountServiceResult, function (key, value) {
                                var objDTO = new PromoCodeModel();
                                objDTO.MapEntity(value, objDTO);
                                self.PromoCodeDTO(objDTO)
                                bResult = true;                                
                            })
                        } else {
                            self.PromoCodeDTO(null);
                        }
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    //------------------------------- Begin - Get cookies info -------------------------------//

    //-- Get tour service info from the cookie
    self.GetTourServiceCookieInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetTourServiceCookieInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);               
                    self.TourServiceSessionInfo().MapEntity(ds, self.TourServiceSessionInfo());

                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedServices = function () {        

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetSavedServices",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var Info = new ServiceModelLite();
                        Info.MapEntity(value, Info);

                        var arrivaldate = new Date(parseInt(Info.ppArrivalDate().substr(6)));
                        Info.ppFormattedArrivalDate(formatDate_v2(arrivaldate, "/", "mmddyyyy"))
                        Info.ppArrivalDate(arrivaldate)

                        Info.ppTotalPerAdults('$' + parseFloat(Info.ppPriceAdults() * Info.ppAdults()).toFixed(2));
                        Info.ppTotalPerChildren('$' + parseFloat(Info.ppPriceChildren() * Info.ppChildren()).toFixed(2));
                        Info.ppTotalPerInfants('$' + parseFloat(Info.ppPriceInfants() * Info.ppInfants()).toFixed(2));
                        Info.ppTotalPerStudents('$' + parseFloat(Info.ppPriceStudent() * Info.ppStudents()).toFixed(2));                        

                        if (Info.ppAdults() > 0) {
                            Info.ShowHideAdult("ShowSummaryItem");
                        } else {
                            Info.ShowHideAdult("HideSummaryItem");
                        }

                        if (Info.ppChildren() > 0) {
                            Info.ShowHideChildren("ShowSummaryItem");
                        } else {
                            Info.ShowHideChildren("HideSummaryItem");
                        }

                        if (Info.ppInfants() > 0) {
                            Info.ShowHideInfants("ShowSummaryItem");
                        } else {
                            Info.ShowHideInfants("HideSummaryItem");
                        }

                        if (Info.ppStudents() > 0) {
                            Info.ShowHideStudents("ShowSummaryItem");
                        } else {
                            Info.ShowHideStudents("HideSummaryItem");
                        }

                        if (Info.ppHotelPickUpID() > 0) {
                            Info.ShowHidePickupSection("ShowSummaryItem");
                        } else {
                            Info.ShowHidePickupSection("HideSummaryItem");
                        }                        

                        Info.UniqueId(s4() + s4());

                        self.InsertedServicesList.push(Info);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetReservationInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetReservationInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var ReservInfo = new ReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.ReservationDTO(ReservInfo);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetPromoCodeInfoServices = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/GetPromoCodeInfoService",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var Info = new PromoCodeModel();
                            Info.MapEntity(value, Info);                                                       
                            self.PromoServiceList.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }
    //------------------------------- End - Get cookies info -------------------------------//
   

    function formatDate(date, separator_char) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator_char);
    }


    //-- Add to cart Button
    AddToCart_Button = function (data, event) {
        var BtnTourId = $(event.target).attr('id');
        
        var adults= 0, children = 0, infants = 0, students = 0, totalofpersons;
        
        try {

            if ($('.AdultsSpinner').val() != null) {
                if ($.isNumeric($('.AdultsSpinner').val())) {
                    adults = parseInt($('.AdultsSpinner').val());
                }
            }

            if ($('.ChildrenSpinner').val() != null) {
                if ($.isNumeric($('.ChildrenSpinner').val())) {
                    children =  parseInt($('.ChildrenSpinner').val());
                }
            }

            if ($('.InfantsSpinner').val() != null) {
                if ($.isNumeric($('.InfantsSpinner').val())) {
                    infants =  parseInt($('.InfantsSpinner').val());
                }
            }

            if ($('.StudentsSpinner').val() != null) {
                if ($.isNumeric($('.StudentsSpinner').val())) {
                    students =  parseInt($('.StudentsSpinner').val());
                }
            }

            totalofpersons = adults + children + infants + students

            if ($('#ddl_schedhours').val() == 0) {
                alertDialog("Choose a specific hour.");
                return false;
            }

            if (totalofpersons == 0) {
                alertDialog("You have to choose at least one person.");
                return false;
            }            

            if ($('.SchedHoursSection').is(':visible')) {
                if ($('#ddl_schedhours').val() != 0) {                    
                    var InvQty = 0
                    if ((objModelTours.ServiceSchedAvailList() != null) && (objModelTours.ServiceSchedAvailList().length > 0)) {
                        InvQty = objModelTours.ServiceSchedAvailList()[0].ppQty();
                    }
                    if (totalofpersons > InvQty) {
                        $('#lblNoServiceAvailable').text("People quantity exceeds inventory. Max " + InvQty + " people");
                        $('.NoServiceAvailable').show();
                        return false;
                    }
                }
            }

            loading(true);

            var dates = $("#datepicker").datepick('getDate');
            var arrivaldate;
            if (dates.length > 0) {
                arrivaldate = formatDate_v2(dates[0], '-', 'yyyymmdd');                
            }

            var bReturn;
            // Save service in list - cookie
            var priceadults = 0, pricechildren = 0, priceinfants = 0, pricestudents = 0;
            var pickupplacedesc = '---', pickuphourdesc = '---';
            priceadults = parseFloat($('#lblPriceAdults').text());
            pricechildren = parseFloat($('#lblPriceChildren').text());
            priceinfants = parseFloat($('#lblPriceInfants').text());
            pricestudents = parseFloat($('#lblPriceStundents').text());

            if ($('#ddl_shuttleplace').val() > 0) {
                pickupplacedesc = $('#ddl_shuttleplace :selected').text();
            }

            if ($('#ddl_shuttlepickuphour').val() > 0) {
                pickuphourdesc = $('#ddl_shuttlepickuphour :selected').text();
            }

            bReturn = objModelTours.SaveServices(self.TourServiceSessionInfo().SessionId(), self.TourServiceSessionInfo().ServiceId(), self.ppDescription(), arrivaldate,
                $('#ddl_schedhours').val(), $('#ddl_schedhours :selected').text(), $('#ddl_shuttleplace').val(), $('#ddl_shuttlepickuphour').val(), adults, children, infants, students, priceadults, pricechildren,
                priceinfants, pricestudents, pickupplacedesc, pickuphourdesc, null);

            if (bReturn) {

                //-- Show summary section
                //Get services saved in cookie
                objModelTours.InsertedServicesList([]);
                self.GetSavedServices();
                if (objModelTours.InsertedServicesList() != null) {
                    if (objModelTours.InsertedServicesList().length > 0) {
                        $("#SummarySection").show();
                        updateSummaryServicesPromo();
                        LoadSummaryTotals();                        
                    } else {
                        $("#SummarySection").hide();
                    }
                } else {
                    $("#SummarySection").hide();
                }

                validatecartmenuoption();

                //-- Delete add-on from list
                $(".RemoveServiceLink").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var ServiceUniqueIdToDelete = $(this).closest('tr').parent().closest('tr').attr("id");
                    $(this).closest('tr').parent().closest('tr')
                    .children('td')
                    .animate({ padding: 0 })
                    .wrapInner('<div />')
                    .children()
                    .slideUp(function () { $(this).closest('tr').parent().closest('tr').remove(); });

                    //-- Delete service and insert the new service list to the cookie
                    deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete);
                    //--
                });
                //
                //--

                $('#ServiceDateAndQtySection').hide();
                $('#ServiceAddedSection').show();

                //Hide arrows from slider
                //$('.bx-controls-direction').find(".bx-prev").hide();
                //$('.bx-controls-direction').find(".bx-next").hide();
                //

                alertDialog("Service added.");
                $('#dialog_msg').delay(4000).fadeOut(1000);

                setTimeout(function () {
                    $('#dialog_msg').modal("hide");

                    //Show arrows from slider
                    //$('.bx-controls-direction').find(".bx-prev").show();
                    //$('.bx-controls-direction').find(".bx-next").show();
                    //

                }, 2000);
                
            } else {                
                alertDialog("Failed adding the service to cart.");
            }

            loading(false);
        }
        catch (ex) {
            loading(false);
            alertDialog("Failed adding the service to cart.");
        }
    }
    //-- 

    //-- Continue Shopping Button
    ContinueShopping_Button = function (data, event) {
        try {            

            loading(true);

            var url = $("#txtTrackingURLBase").val() + '/ToursServices/ToursServices?hotelcode=' + $.trim(self.CompanyCode());
            window.location.href = url;

            loading(false);

        }
        catch (ex) {
            loading(false);
        }
    }
    //-- 

    //-- Continue Shopping Button
    ProceedCheckout_Button = function (data, event) {
        try {

            loading(true);

            var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(self.CompanyCode());
            window.location.href = url;

            loading(false);

        }
        catch (ex) {
            loading(false);
        }
    }
    //--


    self.InitModel = function () {                                               

        var CompanyCode = "", ServiceId = 0;

        self.GetTourServiceCookieInfo();

        //-- Read company code from cookie
        if (self.TourServiceSessionInfo() != null) {
            if (self.TourServiceSessionInfo().CompanyID() != null) {
                if (self.TourServiceSessionInfo().CompanyID() != '') {
                    CompanyCode = $.trim(self.TourServiceSessionInfo().CompanyID());
                }
            }
            

            if (self.TourServiceSessionInfo().ServiceId() != '') {
                if (self.TourServiceSessionInfo().ServiceId() != null) {
                    ServiceId = self.TourServiceSessionInfo().ServiceId();
                }                
            }        
        }
        //--

        if (CompanyCode == "") {
            CompanyCode = $("#hdf_HotelCode").val();
            if (CompanyCode == "") {
                alertDialog("No company provided");
                return false;
            }
        }

        if (ServiceId == "") {
            ServiceId = $("#hdf_ServiceId").val();
            if (ServiceId == "") {
                alertDialog("No Service Id provided");
                return false;
            }
        }

        self.CompanyCode(CompanyCode);        

        //--Test
        //self.CompanyCode("BDIHS");
        
        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + $.trim(self.CompanyCode()) + "&photoNumber=4");
        
        self.LoadCompany();        

        var currentdate = new Date();
        var strdate = formatDate(currentdate, '-');        
                
        self.GetServiceByID(ServiceId);

        //Get services saved in cookie
        self.GetSavedServices();

        //Get pickup places
        self.GetServicePickupPlaces();

        //Get PromoCode Info
        self.GetPromoCodeInfoServices();
    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var objModelTours;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              
               
               try{                              
                   loading(true, 'Please wait ...');                   
                                      
                   objModelTours = new ServiceDetailVm();
                   ko.applyBindings(objModelTours);

                   var urlCompanyLogo = objModelTours.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);

                   $("#idCompanyLogo").click(function () {
                       var url = "http://" + objModelTours.ppWeb();
                       window.location.href = url;
                   });

                   //Menu options
                   $("#progress_toursservices").removeClass("progress_menu_selected");
                   $("#progress_tourscart").addClass("progress_menu_selected");
                   $("#progress_toursconfirmation").removeClass("progress_menu_selected");

                   validatecartmenuoption();                   
                   //

                   //Load Footer Info
                   $("#lblCompany").text(objModelTours.CompanyName());
                   $("#lblAddress").text(objModelTours.ppAddress());
                   $("#lblPhoneCo").text(objModelTours.ppTel1());
                   $("#lnkEmail").text(objModelTours.ppEmail());
                   //

                   //-- Adapt Header ContentPlaceHolder
                   $("#MasterDivLeftPanelContent").removeClass("col-sm-3");
                   $("#MasterDivLeftPanelContent").hide();

                   $("#MasterDivRightPanelContent").addClass("col-sm-4");
                   $("#MasterDivRightPanelContent").show();

                   $("#MasterDivMainContent").removeClass("col-sm-9");
                   $("#MasterDivMainContent").addClass("col-sm-8");

                   $("#MasterDivMainContent").addClass("MasterDivMainContentStyle");                   
                   //--                 

                   $('.bxslider').bxSlider({
                       adaptiveHeight: true,
                       auto: false,
                       pause: 6000,
                       mode: 'fade'                       
                   });
                   //--

                   $("#datepicker").datepick({                       
                       //rangeSelect: true,
                       //showOtherMonths: true,
                       changeMonth: false,
                       prevText: 'Previous Month',
                       nextText: 'Next Month',
                       onDate: showDayCellData,
                       //onChangeMonthYear: loadDataOnMonthYearChange,
                       onSelect: dateSelected,
                       showTrigger: '#calImg',
                       pickerClass: 'PickerStyle',
                       minDate: 0, maxDate: +330
                   });

                   //Spinners
                   //-- Init spinners             
                   $(".AdultsSpinner").spinner({ min: 0 });
                   $(".AdultsSpinner").spinner("value", 0);
                   $(".ChildrenSpinner").spinner({ min: 0 });
                   $(".ChildrenSpinner").spinner("value", 0);
                   $(".InfantsSpinner").spinner({ min: 0});
                   $(".InfantsSpinner").spinner("value", 0);
                   $(".StudentsSpinner").spinner({ min: 0 });
                   $(".StudentsSpinner").spinner("value", 0);                   

                   FixSpinnerIcons();

                   $(".AdultsSpinner").spinner("disable");
                   $(".ChildrenSpinner").spinner("disable");
                   $(".InfantsSpinner").spinner("disable");
                   $(".StudentsSpinner").spinner("disable");

                   showHideSpinners();
                   //--

                   //-- Shuttle/Pickup                   
                   if (objModelTours.PickupPlaces() != null) {
                       if (objModelTours.PickupPlaces().length > 0) {
                           $(".ShuttleSection").show();
                           $("#ddl_needshuttle").prop('disabled', 'disabled');                           

                           $("#ddl_needshuttle").change(function () {
                               if ($(this).val() == '0') {
                                   $(".PickupOptionsSection").hide(500);
                               } else {
                                   LoadPickupPlacesInDropDownList();
                                   $("#ddl_shuttlepickuphour").prop('disabled', 'disabled');
                                   $(".PickupOptionsSection").show(500);                                   
                               }
                           });

                           $("#ddl_shuttleplace").change(function () {
                               objModelTours.GetServicePickupHours($(this).val());
                               LoadPickupHoursInDropDownList();                               
                           });
                           
                       } else {
                           $(".ShuttleSection").hide();
                       }
                   } else {
                       $(".ShuttleSection").hide();
                   }
                   //--


                   //---- Validate promo code
                   if (objModelTours.PromoServiceList() != null) {
                       if (objModelTours.PromoServiceList().length > 0) {
                           $("#txtPromotionalCode").val(objModelTours.PromoServiceList()[0].ppPromoCode());
                           $('#txtPromotionalCode').prop('readonly', true);
                           $("#btnPromoCode").html('Revert');
                           $("#PromoApprovedIcon").show(300);                                                     
                       } 
                   } 
                   //--

                   //---- Show summary section
                   if (objModelTours.InsertedServicesList() != null) {
                       if (objModelTours.InsertedServicesList().length > 0) {
                           $("#SummarySection").show();
                           updateSummaryServicesPromo();                                                              
                           LoadSummaryTotals();
                       } else {
                           $("#SummarySection").hide();
                       }
                   }else {
                       $("#SummarySection").hide();
                   }

                   //-- Delete add-on from list
                   $(".RemoveServiceLink").click(function (e) {
                       e.preventDefault();
                       e.stopPropagation();
                       var ServiceUniqueIdToDelete = $(this).closest('tr').parent().closest('tr').attr("id");
                       $(this).closest('tr').parent().closest('tr')
                       .children('td')
                       .animate({ padding: 0 })
                       .wrapInner('<div />')
                       .children()
                       .slideUp(function () { $(this).closest('tr').parent().closest('tr').remove(); });

                       //-- Delete service and insert the new service list to the cookie
                       deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete);
                       //--
                   });
                   //

                   //----
                   $("#btnPromoCode").click(function () {
                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "") {
                               alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {
                               loading(true, 'Please wait ...');
                               objModelTours.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((objModelTours.PromoList().length === 0) || (objModelTours.PromoList() === null) || (objModelTours.PromoList() === undefined)) {
                                   alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppEndDate(), '/'));

                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       alertDialog('Invalid promotional code');
                                   } else if (CurrentDate > DateTo) {
                                       alertDialog('Promotional code has expired');
                                   } else {
                                       if (objModelTours.InsertedServicesList() != null) {
                                           if (objModelTours.InsertedServicesList().length > 0) {
                                               updateSummaryServicesPromo();
                                               LoadSummaryTotals();
                                           } else {
                                               if (objModelTours.TourServiceRateList() != null) {
                                                   if (objModelTours.TourServiceRateList().length > 0) {
                                                       $.each(objModelTours.TourServiceRateList(), function (i, item) {
                                                           objModelTours.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.ppRateId())
                                                           if (objModelTours.PromoCodeDTO() != null) {
                                                               if (objModelTours.PromoCodeDTO().ppAmount() > 0) {
                                                                   //--Add PromoCodeDTO object to list (save this list to cookie)
                                                                   objModelTours.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));
                                                                   objModelTours.PromoCodeDTO().ppServiceID(item.ppServiceID());
                                                                   if (objModelTours.PromoServiceList() == null) {
                                                                       objModelTours.PromoServiceList = ko.observableArray();
                                                                   }
                                                                   objModelTours.PromoServiceList.push(objModelTours.PromoCodeDTO())
                                                                   //--
                                                               }
                                                           }
                                                       });
                                                   }
                                               }

                                               if ($("#datepicker").datepick('getDate').length > 0) {
                                                   var dates = $("#datepicker").datepick('getDate');
                                                   var arrivaldate;
                                                   if (dates.length > 0) {
                                                       dateSelected(dates)                                                       
                                                   }                                                   
                                               }
                                           }

                                           //--Save promo code info in cookie
                                           if (objModelTours.PromoServiceList() != null) {
                                               if (objModelTours.PromoServiceList().length > 0) {
                                                   objModelTours.SavePromoCodeCookie();
                                               } else {                                                 
                                                   alertDialog('Promotional code cannot be applied because it is not related to the service and rates.');                                                                                                      
                                                   loading(false);
                                                   return false;
                                               }
                                           } else {
                                               alertDialog('Promotional code cannot be applied because it is not related to the service and rates.');
                                               loading(false);
                                               return false;
                                           }
                                           
                                           //--

                                           alertDialog('Promotional code was applied');

                                           $('#txtPromotionalCode').prop('readonly', true);
                                           $("#btnPromoCode").html('Revert');
                                           $("#PromoApprovedIcon").show(300);
                                       }
                                   }
                               }

                               loading(false);
                           }
                           catch (e) {
                               alertDialog('Error applying promotional code.');
                               loading(false);
                           }
                       } else {                                                      
                           loading(true, 'Please wait ...');

                           $("#txtPromotionalCode").val('');
                           $('#txtPromotionalCode').prop('readonly', false);
                           $("#btnPromoCode").html('Validate');
                           $("#PromoApprovedIcon").hide();                                                  

                           //$(".SummaryPromoCode").hide();
                           objModelTours.PromoServiceList(null);

                           if ($("#datepicker").datepick('getDate').length > 0) {
                               var dates = $("#datepicker").datepick('getDate');
                               var arrivaldate;
                               if (dates.length > 0) {
                                   dateSelected(dates)                                   
                               }
                           }

                           if (objModelTours.InsertedServicesList() != null) {
                               if (objModelTours.InsertedServicesList().length > 0) {
                                   $.each(objModelTours.InsertedServicesList(), function (i, item) {
                                       item.ppTotalPerAdultsPromo(null);
                                       item.ppTotalPerChildrenPromo(null);
                                       item.ppTotalPerInfantsPromo(null);
                                       item.ppTotalPerStudentsPromo(null);
                                   })

                                   updateSummaryServicesPromo();
                                   LoadSummaryTotals();
                               }
                           }                         

                           deleteCookie("PromoCodeInfoServices");

                           loading(false);
                       }
                   })

                   //-- Return to website link
                   $("#returntoSiteLink").attr("href", "http://" + objModelTours.ppWeb());
                   $("#returntoSiteLink").text("Return to Web Site");
                   //--                   

                   //-- YouTube, Facebook and Twitter links / footer                   
                   $("#facebooklink").attr("href", "http://www.facebook.com/" + objModelTours.ppFacebookURL());
                   //

                   loading(false);
                  
               }
               catch (ex) {
                   loading(false);
                   alertDialog('Error loading service details. Error: ' + ex.message);
               }

               
           }

           
);
//*******************                              ******************//

function updateSummaryServicesPromo() {
    if (objModelTours.InsertedServicesList().length > 0) {
        $.each(objModelTours.InsertedServicesList(), function (i, item) {
            objModelTours.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.ppRateId())
            if (objModelTours.PromoCodeDTO() != null) {
                if (objModelTours.PromoCodeDTO().ppAmount() > 0) {

                    //--Add PromoCodeDTO object to list (save this list to cookie)
                    objModelTours.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));
                    objModelTours.PromoCodeDTO().ppServiceID(item.ppServiceID());
                    if (objModelTours.PromoServiceList() == null) {
                        objModelTours.PromoServiceList = ko.observableArray();
                    }
                    objModelTours.PromoServiceList.push(objModelTours.PromoCodeDTO())
                    //--                                                                                                                   

                    var originalprice = 0;
                    var price = 0;

                    if (item.ppAdults() > 0) {
                        originalprice = item.ppPriceAdults() * item.ppAdults();
                        price = item.ppPriceAdults() * item.ppAdults();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerAdultsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperadultspromo_" + item.UniqueId()).show();
                            $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerAdultsPromo('');
                            $("#spantotalperadultspromo_" + item.UniqueId()).hide();
                            $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }

                    if (item.ppChildren() > 0) {
                        originalprice = item.ppPriceChildren() * item.ppChildren();
                        price = item.ppPriceChildren() * item.ppChildren();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerChildrenPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperachildrenpromo_" + item.UniqueId()).show();
                            $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerChildrenPromo('');
                            $("#spantotalperachildrenpromo_" + item.UniqueId()).hide();
                            $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }

                    if (item.ppInfants() > 0) {
                        originalprice = item.ppPriceInfants() * item.ppInfants();
                        price = item.ppPriceInfants() * item.ppInfants();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerInfantsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperinfantspromo_" + item.UniqueId()).show();
                            $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerInfantsPromo('');
                            $("#spantotalperinfantspromo_" + item.UniqueId()).hide();
                            $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }


                    if (item.ppStudents() > 0) {
                        originalprice = item.ppPriceStudent() * item.ppStudents();
                        price = item.ppPriceStudent() * item.ppStudents();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }
                            }
                        }

                        if (originalprice > price) {
                            item.ppTotalPerStudentsPromo('$' + parseFloat(price).toFixed(2));
                            $("#spantotalperstudentspromo_" + item.UniqueId()).show();
                            $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "line-through");

                        } else {
                            item.ppTotalPerStudentsPromo('');
                            $("#spantotalperstudentspromo_" + item.UniqueId()).hide();
                            $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "none"); //remove line (style) over the price
                        }
                    }


                }
            } else {
                $("#spantotalperadultspromo_" + item.UniqueId()).hide();
                $("#spantotalperadults_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperachildrenpromo_" + item.UniqueId()).hide();
                $("#spantotalperchildren_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperinfantspromo_" + item.UniqueId()).hide();
                $("#spantotalperinfants_" + item.UniqueId()).css("text-decoration", "none");
                $("#spantotalperstudentspromo_" + item.UniqueId()).hide();
                $("#spantotalperstudents_" + item.UniqueId()).css("text-decoration", "none");
            }
        });        

    }
}

function validatecartmenuoption() {
    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            $("#cart_items").text(objModelTours.InsertedServicesList().length);
            $("#cart_items").show();
            $("#progress_tourscart").css("cursor", "pointer");

            $("#cart_items").click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });

            $("#progressmenu li#progress_tourscart").find('a').css("cursor", "pointer");

            $("#progressmenu li#progress_tourscart").find('a').click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });

            $("#progressmenu li#progress_tourscart").click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });
        } else {
            $("#cart_items").hide();
            $("#progressmenu li#progress_tourscart").find('a').click(function () { return false });
            $("#progressmenu li#progress_tourscart").find('a').css("cursor", "default");
        }
    } else {
        $("#cart_items").hide();
        $("#progressmenu li#progress_tourscart").find('a').click(function () { return false });
        $("#progressmenu li#progress_tourscart").find('a').css("cursor", "default");
    }
}

function formatDate_v2(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";

    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var dayname = weekday[date.getDay()];
    var monthname = months[date.getMonth()];
    var daynumber = date.getDate();
    var ordinalday;

    if (daynumber == 1 || daynumber == 21 || daynumber == 31) {
        ordinalday = daynumber + 'st'
    } else if (daynumber == 2 || daynumber == 22) {
        ordinalday = daynumber + 'nd'
    } else if (daynumber == 3 || daynumber == 23) {
        ordinalday = daynumber + 'rd'
    } else {
        ordinalday = daynumber + 'th'
    }
   
    returnedvalue = monthname + ', ' + dayname + ' ' + ordinalday;

    return returnedvalue;
}

function showDayCellData(date) {

    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var dayname = weekday[date.getDay()];
    var priceperday = '';
    var dateformatted = formatDate_v2(date, '/', 'ddmmyyyy');    

    var CellClassStyle = 'DateCellStyle';
    var bselectable = true;
    var tooltip = $.datepick.formatDate('Select DD, M d, yyyy', date);

    var currentdate = new Date();
    var dateinCalendar = new Date(date);

    dateinCalendar.setHours(0, 0, 0, 0)
    currentdate.setHours(0, 0, 0, 0)



    if (dateinCalendar >= currentdate) {
        if (objModelTours.TourServiceRateList() != null) {
            $.each(objModelTours.TourServiceRateList(), function (key, obj) {
                //var Info = new TourServiceRateModel();
                //Info.MapEntity(value, Info);

                var startdate, enddate;
                var ratestartdate, rateendate;
                startdate = obj.ppRatestartdate();
                enddate = obj.ppRateenddate();

                if (startdate != null) {
                    if (startdate != '') {
                        var ratestartdate = new Date(parseInt(startdate.substr(6)));                       
                    } else {
                        return false;
                    }                    
                } else {
                    return false;
                }

                if (enddate != null) {
                    if (enddate != '') {
                        var rateendate = new Date(parseInt(enddate.substr(6)));
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }                

                if ((dateinCalendar >= ratestartdate) && (dateinCalendar <= rateendate)) {
                    bselectable = true
                    CellClassStyle = 'DateCellStyle';
                    tooltip = $.datepick.formatDate('Select DD, M d, yyyy', date);
                    return false;
                } else {
                    CellClassStyle = 'UnavailableDateCellStyle';
                    bselectable = false;
                    tooltip = 'No service available';
                }
                             
            })
           
        }
    }

    //var textcontent = '<div class="datenumberstyle">' + date.getDate() + '</div>' + '<div>' + priceperday + '</div>';

    //return { content: textcontent, dateClass: CellClassStyle, selectable: bselectable, title: tooltip };
    return { dateClass: CellClassStyle, selectable: bselectable, title: tooltip };
}

function dateSelected(dates) {
    var bselectable = false;
    var maxadults = 0, maxchildren = 0, maxinfants = 0, maxstudents = 0;
    var priceadults = 0, pricechildren = 0, priceinfants = 0, pricestudents = 0;
    var serviceid, controlinventory = false;

    try{
        loading(true, 'Please wait ...');

        $(".PickupOptionsSection").hide();        

        //disable add to cart button by default
        $("#btnAddToCart").prop('disabled', true);

        //-- Init spinners                     
        $(".AdultsSpinner").spinner("value", 0);        
        $(".ChildrenSpinner").spinner("value", 0);        
        $(".InfantsSpinner").spinner("value", 0);        
        $(".StudentsSpinner").spinner("value", 0);

        $(".AdultsSpinner").spinner("disable");
        $(".ChildrenSpinner").spinner("disable");
        $(".InfantsSpinner").spinner("disable");
        $(".StudentsSpinner").spinner("disable");
        //--

        if (dates != null) {
            if (objModelTours.TourServiceRateList() != null) {
                $.each(objModelTours.TourServiceRateList(), function (key, obj) {
                    var startdate, enddate;
                    var ratestartdate, rateendate;
                    startdate = obj.ppRatestartdate();
                    enddate = obj.ppRateenddate();

                    if (startdate != null) {
                        if (startdate != '') {
                            var ratestartdate = new Date(parseInt(startdate.substr(6)));
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }

                    if (enddate != null) {
                        if (enddate != '') {
                            var rateendate = new Date(parseInt(enddate.substr(6)));
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }

                    if ((dates[0] >= ratestartdate) && (dates[0] <= rateendate)) {
                        bselectable = true;
                        maxadults = obj.ppMaxadult();
                        maxchildren = obj.ppMaxchildren();
                        maxinfants = obj.ppMaxinfant();
                        maxstudents = obj.ppMaxstudent();

                        priceadults = obj.ppAdults();
                        pricechildren = obj.ppChildren();
                        priceinfants = obj.ppInfants();
                        pricestudents = obj.ppStudent();

                        serviceid = obj.ppServiceID();
                        controlinventory = obj.ppControlInventory();

                        //Set selected rate id for the service
                        objModelTours.ppServiceSelectedRateId(obj.ppRateId());

                        return false;
                    }

                })

                if (bselectable) {                    
                    SetPricesLabels(priceadults, pricechildren, priceinfants, pricestudents, serviceid);

                    if (controlinventory) {
                        objModelTours.ServiceSchedAvailList([]); //Clean pricelist                    
                        var dateformatted = formatDate_v2(dates[0], '-', 'yyyymmdd');

                        objModelTours.ServiceSchedAvailList([]); //clean ServiceSchedAvailList
                        objModelTours.GetServiceSchedAvailability(serviceid, dateformatted, 1, '');

                        if ((objModelTours.ServiceSchedAvailList() != null) && (objModelTours.ServiceSchedAvailList().length > 0)) {
                            LoadSchedHoursInDropDownList();
                            SetMaxValueForSpinners(maxadults, maxchildren, maxinfants, maxstudents);
                            $(".NoServiceAvailable").hide();
                            $("#btnAddToCart").prop('disabled', false);
                            $("#btnAddToCart").prop('title', 'Add service to cart');
                            $("#ddl_needshuttle").prop('disabled', false);
                        } else {
                            $(".SchedHoursSection").hide();
                            $("#lblNoServiceAvailable").text("Service not available");
                            $(".NoServiceAvailable").show();
                            $("#btnAddToCart").prop('disabled', true);
                            $("#btnAddToCart").prop('title', 'Service not available');
                        }
                    } else {
                        $(".SchedHoursSection").hide();
                        $(".NoServiceAvailable").hide();
                        SetMaxValueForSpinners(maxadults, maxchildren, maxinfants, maxstudents);
                        $("#btnAddToCart").prop('disabled', false);
                        $("#btnAddToCart").prop('title', 'Add service to cart');
                        $("#ddl_needshuttle").prop('disabled', false);
                    }                    

                } else {
                    $("#btnAddToCart").prop('disabled', true);
                    $("#btnAddToCart").prop('title', 'Service not available');
                    alertDialog('Service is not available.');                    
                }

            }
        }

        loading(false);
    }
    catch (e) {
        loading(false);
        alertDialog('Error loading service info for specific date.');
    }
    
}

function SetMaxValueForSpinners(maxadults, maxchildren, maxinfants, maxstudents) {
    if (maxadults >= 0) {
        $('.AdultsSpinner').spinner('option', 'max', maxadults);
        $(".AdultsSpinner").spinner("enable");
    } else {
        $(".AdultsSpinner").spinner("disable");
    }

    if (maxchildren >= 0) {
        $('.ChildrenSpinner').spinner('option', 'max', maxchildren);
        $(".ChildrenSpinner").spinner("enable");
    } else {
        $(".ChildrenSpinner").spinner("disable");
    }

    if (maxinfants >= 0) {
        $('.InfantsSpinner').spinner('option', 'max', maxinfants);
        $(".InfantsSpinner").spinner("enable");
    } else {
        $(".InfantsSpinner").spinner("disable");
    }

    if (maxstudents >= 0) {
        $('.StudentsSpinner').spinner('option', 'max', maxstudents);
        $(".StudentsSpinner").spinner("enable");
    } else {
        $(".StudentsSpinner").spinner("disable");
    }
}

function SetPricesLabels(priceadults, pricechildren, priceinfants, pricestudents, serviceid) {
    var promoamount = 0;
    var promotype = 'P';
    if (objModelTours.PromoServiceList() != null) {
        if (objModelTours.PromoServiceList().length > 0) {
            $.each(objModelTours.PromoServiceList(), function (i, item) {
                if (item.ppServiceID() == serviceid) {
                    promoamount = item.ppAmount();
                    promotype = item.ppType();
                    return false;
                }
                
            })
        }
    }            

    if (priceadults >= 0) {
        var originalprice = priceadults;        
        if (promoamount > 0) {
            var price = priceadults;
            if (promotype.toUpperCase() == 'P') {
                price = price - (price * (promoamount / 100));
            } else {
                if (price > promoamount) {
                    price = price - promoamount;
                } else {
                    price = 0;
                }
            }

            if (originalprice > price) {
                $("#lblPromoPriceAdults").text(price.toFixed(2));
                $("#divPromoPriceAdults").show();
                $("#lblPriceAdults").css("text-decoration", "line-through");
            } else {
                $("#lblPromoPriceAdults").text(''); //clean 'price with discount' label
                $("#divPromoPriceAdults").hide();
                $("#lblPriceAdults").css("text-decoration", "none"); //remove line (style) over the price
            }         
        } else {
            $("#lblPromoPriceAdults").text(''); //clean 'price with discount' label
            $("#divPromoPriceAdults").hide();
            $("#lblPriceAdults").css("text-decoration", "none"); //remove line (style) over the price
        }

        $('#lblPriceAdults').text(originalprice);
        
    } 

    if (pricechildren >= 0) {
        var originalprice = pricechildren;
        if (promoamount > 0) {
            var price = pricechildren;
            if (promotype.toUpperCase() == 'P') {
                price = price - (price * (promoamount / 100));
            } else {
                if (price > promoamount) {
                    price = price - promoamount;
                } else {
                    price = 0;
                }
            }

            if (originalprice > price) {
                $("#lblPromoPriceChildren").text(price.toFixed(2));
                $("#divPromoPriceChildren").show();
                $("#lblPriceChildren").css("text-decoration", "line-through");
            } else {
                $("#lblPromoPriceChildren").text(''); //clean 'price with discount' label
                $("#divPromoPriceChildren").hide();
                $("#lblPriceChildren").css("text-decoration", "none"); //remove line (style) over the price
            }          
        } else {
            $("#lblPromoPriceChildren").text(''); //clean 'price with discount' label
            $("#divPromoPriceChildren").hide();
            $("#lblPriceChildren").css("text-decoration", "none"); //remove line (style) over the price
        }

        $('#lblPriceChildren').text(originalprice);
    } 

    if (priceinfants >= 0) {
        var originalprice = priceinfants;
        if (promoamount > 0) {
            var price = priceinfants;
            if (promotype.toUpperCase() == 'P') {
                price = price - (price * (promoamount / 100));
            } else {
                if (price > promoamount) {
                    price = price - promoamount;
                } else {
                    price = 0;
                }
            }

            if (originalprice > price) {
                $("#lblPromoPriceInfants").text(price.toFixed(2));
                $("#divPromoPriceInfants").show();
                $("#lblPriceInfants").css("text-decoration", "line-through");
            } else {
                $("#lblPromoPriceInfants").text(''); //clean 'price with discount' label
                $("#divPromoPriceInfants").hide();
                $("#lblPriceInfants").css("text-decoration", "none"); //remove line (style) over the price
            }
        } else {
            $("#lblPromoPriceInfants").text(''); //clean 'price with discount' label
            $("#divPromoPriceInfants").hide();
            $("#lblPriceInfants").css("text-decoration", "none"); //remove line (style) over the price
        }

        $('#lblPriceInfants').text(originalprice);
    }

    if (pricestudents >= 0) {
        var originalprice = pricestudents;
        if (promoamount > 0) {
            var price = pricestudents;
            if (promotype.toUpperCase() == 'P') {
                price = price - (price * (promoamount / 100));
            } else {
                if (price > promoamount) {
                    price = price - promoamount;
                } else {
                    price = 0;
                }
            }

            if (originalprice > price) {
                $("#lblPromoPriceStundents").text(price.toFixed(2));
                $("#divPromoPriceStundents").show();
                $("#lblPriceStundents").css("text-decoration", "line-through");
            } else {
                $("#lblPromoPriceStundents").text(''); //clean 'price with discount' label
                $("#divPromoPriceStundents").hide();
                $("#lblPriceStundents").css("text-decoration", "none"); //remove line (style) over the price
            }
        } else {
            $("#lblPromoPriceStundents").text(''); //clean 'price with discount' label
            $("#divPromoPriceStundents").hide();
            $("#lblPriceStundents").css("text-decoration", "none"); //remove line (style) over the price
        }

        $('#lblPriceStundents').text(originalprice);
    } 
}

//-- Load schedule hours in dropdown
function LoadSchedHoursInDropDownList() {
    var dll = $("#ddl_schedhours");

    $("#ddl_schedhours option").remove();

    if (objModelTours.ServiceSchedAvailList() != null) {
        if (objModelTours.ServiceSchedAvailList().length > 0) {
            $.each(objModelTours.ServiceSchedAvailList(), function (i, item) {
                dll.append($("<option />").val(item.ppScheduleID()).text($.trim(item.ppHour())));
            });

            $(".SchedHoursSection").show();
            
        } else {
            dll.append($("<option />").val(0).text("---"));
        }
    } else {
        dll.append($("<option />").val(0).text("---"));
    }
}

//-- Load pickup places in dropdown
function LoadPickupPlacesInDropDownList() {
    var dll = $("#ddl_shuttleplace");

    $("#ddl_shuttleplace option").remove();

    if (objModelTours.PickupPlaces() != null) {
        if (objModelTours.PickupPlaces().length > 0) {

            dll.append($("<option />").val(0).text("Select Place:"));

            $.each(objModelTours.PickupPlaces(), function (i, item) {
                dll.append($("<option />").val(item.ppServiceID()).text($.trim(item.ppDescription())));
            });

            //$(".SchedHoursSection").show();

        } else {
            dll.append($("<option />").val(0).text("---"));
        }
    } else {
        dll.append($("<option />").val(0).text("---"));
    }
}

//-- Load pickup hours in dropdown
function LoadPickupHoursInDropDownList() {
    var dll = $("#ddl_shuttlepickuphour");

    $("#ddl_shuttlepickuphour option").remove();

    if (objModelTours.PickupHours() != null) {
        if (objModelTours.PickupHours().length > 0) {

            dll.append($("<option />").val(0).text("Select Hour:"));

            $.each(objModelTours.PickupHours(), function (i, item) {
                dll.append($("<option />").val(item.ppServiceID()).text($.trim(item.ppDescription())));
            });

            $("#ddl_shuttlepickuphour").prop('disabled', false);            

        } else {
            dll.append($("<option />").val(0).text("- No hours available -"));
            $("#ddl_shuttlepickuphour").prop('disabled', 'disabled');
        }
    } else {
        dll.append($("<option />").val(0).text("- No hours available -"));
        $("#ddl_shuttlepickuphour").prop('disabled', 'disabled');
    }
}

//-- Fix Spinner Icons issue with bootstrap
function FixSpinnerIcons() {
    $('.ui-spinner-button.ui-spinner-up').each(function (i, item) {
        $(this).addClass('ui-button');
        $(this).addClass('ui-widget');
        $(this).addClass('ui-button-icon-only');
        var htmlbuttonup = '<span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span><span class="ui-button-icon-space"></span>'
        $(this).html(htmlbuttonup);
    });

    $('.ui-spinner-button.ui-spinner-down').each(function (i, item) {
        $(this).addClass('ui-button');
        $(this).addClass('ui-widget');
        $(this).addClass('ui-button-icon-only');
        var htmlbuttonup = '<span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span><span class="ui-button-icon-space"></span>'
        $(this).html(htmlbuttonup);
    });
}

function deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete) {  

    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            var filteredlist;                      

            filteredlist = objModelTours.InsertedServicesList().filter(function (elem) {
                return elem.UniqueId() !== ServiceUniqueIdToDelete;
            });
           
            objModelTours.InsertedServicesList([]); //clean InsertedServicesList array
            objModelTours.InsertedServicesList(filteredlist.slice()); // fill the array with the current items, without the deleted service
            bReturn = objModelTours.SaveServices(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                                                 null, null, objModelTours.InsertedServicesList());           
            
            //-- Show summary section
            //Get services saved in cookie           
            if (objModelTours.InsertedServicesList() != null) {
                if (objModelTours.InsertedServicesList().length > 0) {
                    $("#SummarySection").show();
                    $("#btnProceedCheckout").prop('disabled', false);
                } else {
                    $("#SummarySection").hide();
                    $("#btnProceedCheckout").prop('disabled', true);
                }
            } else {
                $("#SummarySection").hide();
                $("#btnProceedCheckout").prop('disabled', true);
            }

            validatecartmenuoption();

            //-- Delete add-on from list
            $(".RemoveServiceLink").click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                var ServiceUniqueIdToDelete = $(this).closest('tr').parent().closest('tr').attr("id");
                $(this).closest('tr').parent().closest('tr')
                .children('td')
                .animate({ padding: 0 })
                .wrapInner('<div />')
                .children()
                .slideUp(function () { $(this).closest('tr').parent().closest('tr').remove(); });

                //-- Delete service and insert the new service list to the cookie
                deleteServiceAndUpdateServiceListCookie(ServiceUniqueIdToDelete);
                //--
            });
            //
            //--
        }

    }    
   
    LoadSummaryTotals();
}

function LoadSummaryTotals() {
    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            var moneysign = '$'
            var subtotal = 0, tax = 0, rent = 0, total = 0, TotalPerAdultsPromo, TotalPerChildrenPromo, TotalPerInfantsPromo, TotalPerStudentsPromo;

            $.each(objModelTours.InsertedServicesList(), function (key, item) {
                TotalPerAdultsPromo = 0
                TotalPerChildrenPromo = 0
                TotalPerInfantsPromo = 0
                TotalPerStudentsPromo = 0

                //-- If promo code is applied to service
                if (item.ppTotalPerAdultsPromo() != '' && item.ppTotalPerAdultsPromo() != null) {
                    var temptext = item.ppTotalPerAdultsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerAdultsPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerChildrenPromo() != '' && item.ppTotalPerChildrenPromo() != null) {
                    var temptext = item.ppTotalPerChildrenPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerChildrenPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerInfantsPromo() != '' && item.ppTotalPerInfantsPromo() != null) {
                    var temptext = item.ppTotalPerInfantsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerInfantsPromo = parseFloat(temptext);
                    }
                }

                if (item.ppTotalPerStudentsPromo() != '' && item.ppTotalPerStudentsPromo() != null) {
                    var temptext = item.ppTotalPerStudentsPromo().replace(moneysign, '');
                    if ($.isNumeric(temptext)) {
                        TotalPerStudentsPromo = parseFloat(temptext);
                    }
                }
                //--

                if (TotalPerAdultsPromo > 0 || TotalPerChildrenPromo > 0 || TotalPerInfantsPromo > 0 || TotalPerStudentsPromo > 0) {
                    //if promo code is applied to service
                    subtotal += TotalPerAdultsPromo + TotalPerChildrenPromo + TotalPerInfantsPromo + TotalPerStudentsPromo;
                } else {
                    subtotal += item.SubTotal();
                }
                
            })

            $("#lblSubTotal").text(subtotal.toFixed(2));
            if (objModelTours.Tax() == 0) {
                $("#lblTax").text("0.00")
            } else {
                tax = subtotal * 0.13;
                $("#lblTax").text(tax.toFixed(2));
            }

            rent = (subtotal * objModelTours.RentPorc()) / 100;
            $("#lblRent").text(rent.toFixed(2));

            total = subtotal + tax + rent;
            $("#lblTOTAL").text(total.toFixed(2));
        }
    }    
}

function showHideSpinners() {    
    if (objModelTours.TourServiceRateList() != null) {
        var maxadults = 0, maxchildren = 0, maxinfants = 0, maxstudents = 0;        

        $.each(objModelTours.TourServiceRateList(), function (key, obj) {
            maxadults = obj.ppMaxadult();
            maxchildren = obj.ppMaxchildren();
            maxinfants = obj.ppMaxinfant();
            maxstudents = obj.ppMaxstudent();

            if (maxadults == 0) {
                $('#AdultsRow').hide();
            } else {
                $('#AdultsRow').show();
            }

            if (maxchildren == 0) {
                $('#ChildrenRow').hide();
            } else {
                $('#ChildrenRow').show();
            }

            if (maxinfants == 0) {
                $('#InfantsRow').hide();
            } else {
                $('#InfantsRow').show();
            }

            if (maxstudents == 0) {
                $('#StudentsRow').hide();
            } else {
                $('#StudentsRow').show();
            }
        })
     }
}

function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);    
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
    $('#dialog_msg').css("z-index", "10000");
        
    //$('.CloseDialogButton').click(function (e) {
    //    //e.preventDefault();
    //    //e.stopPropagation();
    //    //Show arrows from slider
    //    $('.bx-controls-direction').find(".bx-prev").show();
    //    $('.bx-controls-direction').find(".bx-next").show();
    //    //
    //});
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--

