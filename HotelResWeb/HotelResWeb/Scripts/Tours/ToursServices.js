﻿function ToursServicesVm(mainVm) {

    var self = this;
    self.TourlVm = ko.observable();

    self.getLogo = ko.observable();

    self.appData = mainVm;

    self.TourList = ko.observableArray();
    self.TourCategoryList = ko.observableArray();

    //-- Company variables
    self.CompanyCode = ko.observable();
    self.CompanyName = ko.observable();
    self.ppEmail = ko.observable();
    self.ppAddress = ko.observable();
    self.ppWeb = ko.observable();
    self.ppTel1 = ko.observable();
    self.Tax = ko.observable();
    self.Processor = ko.observable();
    self.ppFacebookURL = ko.observable();    
    self.ppMaxAdults = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppGoogleID = ko.observable();
    self.ppActive = ko.observable();

    self.TourServiceDTO = ko.observable(new TourServiceSessionModel());
    self.TourServiceSessionInfo = ko.observable(new TourServiceSessionModel());
    self.InsertedServicesList = ko.observableArray();

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();
    self.PromoServiceList = ko.observableArray();

    //--

    //*******************DEFAULT VALUES START*****************//
   
    self.TourlVm(mainVm);   

    //*******************DEFAULT VALUES END*****************//

    self.LoadCompany = function (data) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + self.CompanyCode() + "&sType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0))
                    {
                         self.CompanyName(response.GetCompanyResult["0"].ppCompanyName);
                         self.ppEmail(response.GetCompanyResult["0"].ppEmail);
                         self.ppAddress(response.GetCompanyResult["0"].ppAddress);
                         self.ppWeb(response.GetCompanyResult["0"].ppWeb);
                         self.CompanyCode(response.GetCompanyResult["0"].ppCompanyCode);
                         self.ppTel1(response.GetCompanyResult["0"].ppTel1);
                         self.Tax(response.GetCompanyResult["0"].ppTaxPerc);
                         self.Processor(response.GetCompanyResult["0"].Processor);

                         self.ppFacebookURL(response.GetCompanyResult["0"].ppFacebookURL);                         
                         self.ppMaxAdults(response.GetCompanyResult["0"].ppMaxAdults);
                         self.ppMaxChildren(response.GetCompanyResult["0"].ppMaxChildren);
                         self.ppGoogleID(response.GetCompanyResult["0"].ppGoogleID);
                         self.ppActive(response.GetCompanyResult["0"].ppActive);                                                                                

                    }                    
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.LoadTourCategories = function () {

        $.ajax({
            type: "GET",            
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetCategories?Culture=en-US",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var alignclass = '';

                    if (response.GetCategoriesResult.length > 0)
                    {
                        var Info = new TourCategoryModel();
                        Info.ppCategoryID(0);
                        Info.ppDescription("All");
                        Info.ppCategoryChecked(true)
                        self.TourCategoryList.push(Info);                        
                    }

                    $.each(response.GetCategoriesResult, function (key, value) {
                        var Info = new TourCategoryModel();
                        Info.MapEntity(value, Info);
                        Info.ppCategoryChecked(false)
                        self.TourCategoryList.push(Info);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.LoadTours = function (category, currentdate) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ServiceServices.svc" + "/json/GetServiceAvailability?sCompanyCode=" + self.CompanyCode() + "&CategoryID=" + category + "&Culture=en-US&ArrivalDate=" + currentdate,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var alignclass = '';

                    $.each(response.GetServiceAvailabilityResult, function (key, value) {
                        var Info = new ToursModel();
                        Info.MapEntity(value, Info);

                        Info.ImageUlr($("#txtTrackingURLBase").val() + "/Handlers/getPhotoSvc.ashx?serviceID=" + Info.ppServiceID() + "&photoNumber=1");
                        Info.PricePerAdultLabel('$' + Info.ppAdults())

                        self.TourList.push(Info);
                    })

                    //Show/Hide tour list
                    if (self.TourList() != null) {
                        if (self.TourList().length > 0) {
                            $("#divNoTours").hide();
                            $("#tours_list_div").show();
                        } else {
                            $("#divNoTours").show();
                            $("#tours_list_div").hide();
                        }
                    } else {
                        $("#divNoTours").show();
                        $("#tours_list_div").hide();
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedServices = function () {

        //self.SavedServicesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetSavedServices",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var Info = new ServiceModelLite();
                        Info.MapEntity(value, Info);

                        var arrivaldate = new Date(parseInt(Info.ppArrivalDate().substr(6)));
                        Info.ppFormattedArrivalDate(formatDate_v2(arrivaldate, "/", "mmddyyyy"))
                        Info.ppArrivalDate(arrivaldate)

                        Info.ppTotalPerAdults('$' + parseFloat(Info.ppPriceAdults() * Info.ppAdults()).toFixed(2));
                        Info.ppTotalPerChildren('$' + parseFloat(Info.ppPriceChildren() * Info.ppChildren()).toFixed(2));
                        Info.ppTotalPerInfants('$' + parseFloat(Info.ppPriceInfants() * Info.ppInfants()).toFixed(2));
                        Info.ppTotalPerStudents('$' + parseFloat(Info.ppPriceStudent() * Info.ppStudents()).toFixed(2));

                        if (Info.ppAdults() > 0) {
                            Info.ShowHideAdult("ShowSummaryItem");
                        } else {
                            Info.ShowHideAdult("HideSummaryItem");
                        }

                        if (Info.ppChildren() > 0) {
                            Info.ShowHideChildren("ShowSummaryItem");
                        } else {
                            Info.ShowHideChildren("HideSummaryItem");
                        }

                        if (Info.ppInfants() > 0) {
                            Info.ShowHideInfants("ShowSummaryItem");
                        } else {
                            Info.ShowHideInfants("HideSummaryItem");
                        }

                        if (Info.ppStudents() > 0) {
                            Info.ShowHideStudents("ShowSummaryItem");
                        } else {
                            Info.ShowHideStudents("HideSummaryItem");
                        }

                        Info.UniqueId(s4() + s4());

                        self.InsertedServicesList.push(Info);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }
 
    self.GetCurrentPage = function () {

        var currentpage = "";

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetCurrentPage",
            async: false,
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                currentpage = response;                
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }

        });

        return currentpage;
    }

    self.SaveNextPage = function (Page) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveCurrentPage?CurrentPage=" + Page,
            async: false,
            crossDomain: true,
            success: function (response) {
                return true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                return false;
            }
        });

        return true;
    }

    self.PrepareDtoForInsert = function (RoomId,RoomType, DateFrom, DateTo, Adults, Kids, Infants) {
        
        self.Room = ko.observable(new RoomLiteModel());
        self.Room().Code(RoomId);
        self.Room().Price(parseFloat($("#roomPrice_" + RoomId).html()));
        self.Room().Desc($("#roomTypeDesc_" + RoomId).html());
        self.Room().fromDt(DateFrom);
        self.Room().toDt(DateTo);
        //self.Room().Infants($("#InfantsSpinner_" + RoomId).val());
        //self.Room().Kids($("#KidsSpinner_" + RoomId).val());
        //self.Room().Adults($("#AdultsSpinner_" + RoomId).val());
        self.Room().Adults(Adults);
        self.Room().Kids(Kids);
        self.Room().Infants(Infants);
        self.Room().CompanyId(self.CompanyCode());
        self.Room().RoomType(RoomType);
        self.Room().TaxPerc(self.Tax());

        return self.Room();
    }

    self.PrepareTourServiceInfoDtoForInsert = function (ServiceId) {
        self.TourServiceDTO().SessionId('');
        self.TourServiceDTO().CompanyID(self.CompanyCode());
        self.TourServiceDTO().ServiceId(ServiceId);
        self.TourServiceDTO().PromCode('');
        self.TourServiceDTO().TaxAmount(0);        
        self.TourServiceDTO().PromoAmount(0);
        self.TourServiceDTO().Discount(0);
        self.TourServiceDTO().total(0);

        //self.TourServiceDTO().Processor(self.Processor());
        self.TourServiceDTO().Processor("credomatic"); // uncomment for demo
        self.TourServiceDTO().CardType("MC");
    }

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, iRateID) {
        //self.AddOnTypeList.removeAll();

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmountService?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&iRateID=" + iRateID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountServiceResult != null) {
                        if (response.GetPromoCodeAmountServiceResult.length > 0) {
                            $.each(response.GetPromoCodeAmountServiceResult, function (key, value) {
                                var objDTO = new PromoCodeModel();
                                objDTO.MapEntity(value, objDTO);

                                //PriceDisc = PricesDTO.ppPrices();
                                //Discount = PricesDTO.Discount();

                                self.PromoCodeDTO(objDTO)
                                bResult = true;
                                //self.PricesList.push(PricesDTO);
                            })
                        } else {
                            self.PromoCodeDTO(null);
                        }
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }       

    //------------------------------- Begin - Get cookies info -------------------------------//    

    //-- Save Tour Service Info in cookie
    self.SaveTourServiceCookieOnly = function (TourId) {

        var bReturn;
        self.PrepareTourServiceInfoDtoForInsert(TourId);

        var NewReserv = JSON.stringify(ko.toJS(self.TourServiceDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/SaveTourServiceInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;

        var NewListObj = JSON.stringify(ko.toJS(objModelTours.PromoServiceList()));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/SavePromoCodeInfoService",
            async: false,
            crossDomain: true,
            data: NewListObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Get tour service info from the cookie
    self.GetTourServiceCookieInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetTourServiceCookieInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);
                    self.TourServiceSessionInfo().MapEntity(ds, self.TourServiceSessionInfo());

                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoServices = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ToursServices/GetPromoCodeInfoService",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var Info = new PromoCodeModel();
                            Info.MapEntity(value, Info);
                            self.PromoServiceList.push(Info);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }
    //------------------------------- End - Get cookies info -------------------------------//
   

    function formatDate(date, separator_char) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator_char);
    }

    //-- Select Tour Button
    Select_Tour_Button = function (data, event) {
        var BtnTourId = $(event.target).attr('id');
        var valuesarray = BtnTourId.split("_");
        var TourId;
        if (valuesarray.length > 3) {
            TourId = valuesarray[3];
        }        
       
        try{
            loading(true);

            //-- Save TourServiceInfo in cookie
            self.SaveTourServiceCookieOnly(TourId);
            //   

            var url = $("#txtTrackingURLBase").val() + '/ServiceDetail/ServiceDetail?hotelcode=' + $.trim(self.CompanyCode()) + '&serviceid=' + TourId;
            window.location.href = url;

            loading(false);

        }
        catch (ex) {
            loading(false);
        }                                            
    }
    //-- 

    self.InitModel = function () {                                        

        //-- Verify cookie info
        self.GetTourServiceCookieInfo();

        var CompanyCode = "";
        //-- Read company code from cookie
        if (self.TourServiceSessionInfo() != null) {
            if (self.TourServiceSessionInfo().CompanyID() != null) {
                if (self.TourServiceSessionInfo().CompanyID() != '') {
                    CompanyCode = $.trim(self.TourServiceSessionInfo().CompanyID());
                }
            }           
        }
        //--

        if (CompanyCode == "") {
            CompanyCode = $("#hdf_HotelCode").val();
            if (CompanyCode == "") {
                alertDialog("No company provided");
                return false;
            }
        }

        self.CompanyCode(CompanyCode);

        //--Test
        //self.CompanyCode("BDIHS");
        
        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + $.trim(self.CompanyCode()) + "&photoNumber=4");
        
        self.LoadCompany();
        self.LoadTourCategories();

        var currentdate = new Date();
        var strdate = formatDate(currentdate, '-');

        self.LoadTours(0, strdate);

        self.GetSavedServices();

        //Get PromoCode Info
        self.GetPromoCodeInfoServices();
        
    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var objModelTours;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              
               
               try{                              
                   loading(true, 'Please wait ...');                   
                                      
                   objModelTours = new ToursServicesVm();
                   ko.applyBindings(objModelTours);

                   var urlCompanyLogo = objModelTours.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);

                   $("#idCompanyLogo").click(function () {
                       var url = "http://" + objModelTours.ppWeb();
                       window.location.href = url;
                   });

                   //Menu options
                   $("#progress_toursservices").addClass("progress_menu_selected");
                   $("#progress_tourscart").removeClass("progress_menu_selected");
                   $("#progress_toursconfirmation").removeClass("progress_menu_selected");

                   validatecartmenuoption();

                   //

                   //Load Footer Info
                   $("#lblCompany").text(objModelTours.CompanyName());
                   $("#lblAddress").text(objModelTours.ppAddress());
                   $("#lblPhoneCo").text(objModelTours.ppTel1());
                   $("#lnkEmail").text(objModelTours.ppEmail());
                   //

                   //-- Categories list
                   var state = false, answer = $(".CategoryBox").next('.CategoryList').hide().css('height', 'auto').slideUp();
                 
                   $(".CategoryBox").click(function () {
                           state = !state;
                           answer.slideToggle(state);
                           $(".CategoryBox").toggleClass('active', state);
                   });
                   //--

                   //-- Return to website link
                   $("#returntoSiteLink").attr("href", "http://" + objModelTours.ppWeb());
                   $("#returntoSiteLink").text("Return to Web Site");
                   //--

                   //-- On change event for categories
                   $('input:radio[name="Cat"]').change(
                        function () {
                            if ($(this).prop("checked")) {
                                var id = $(this).attr("id");
                                var valuesarray = id.split("_");
                                var Category = 0;
                                if (valuesarray.length > 1) {
                                    Category = valuesarray[1];
                                }

                                var currentdate = new Date();
                                var strdate = formatDate_v2(currentdate, '-', 'yyyymmdd');

                                objModelTours.TourList([]); //Clean tour list
                                try {
                                    loading(true);
                                    objModelTours.LoadTours(Category, strdate);
                                    loading(false);
                                }
                                catch (ex) {
                                    loading(false);                                    
                                    throw ex
                                }

                            }                                                                                   
                        }
                   );
                   //
                   
                   //---- Validate promo code
                   if (objModelTours.PromoServiceList() != null) {
                       if (objModelTours.PromoServiceList().length > 0) {
                           $("#txtPromotionalCode").val(objModelTours.PromoServiceList()[0].ppPromoCode());
                           $('#txtPromotionalCode').prop('readonly', true);
                           $("#btnPromoCode").html('Revert');
                           $("#PromoApprovedIcon").show(300);

                           updateServicesPromo(false);
                       }
                   }
                   //--                                    

                   $("#btnPromoCode").click(function (){
                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "") {                               
                               alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {
                               loading(true, 'Please wait ...');
                               objModelTours.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((objModelTours.PromoList().length === 0) || (objModelTours.PromoList() === null) || (objModelTours.PromoList() === undefined)) {
                                   alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(objModelTours.PromoList()[0].ppEndDate(), '/'));
                                   
                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       alertDialog('Invalid promotional code');                                       
                                   } else if (CurrentDate > DateTo) {
                                       alertDialog('Promotional code has expired');
                                   } else {
                                       updateServicesPromo(true);
                                   }
                               }

                               loading(false);
                           }
                           catch(e){
                               alertDialog('Error applying promotional code.');
                               loading(false);
                           }
                       } else {                          
                           deleteCookie("PromoCodeInfoServices");
                           location.reload();
                       }
                   })

                   //-- YouTube, Facebook and Twitter links / footer                   
                   $("#facebooklink").attr("href", "http://www.facebook.com/" + objModelTours.ppFacebookURL());
                   //

                   loading(false);
                  
               }
               catch (ex) {
                   loading(false);
                   alertDialog('Error loading tours data. Error: ' + ex.message);
               }

               
           }

           
);
//*******************                              ******************//

function updateServicesPromo(promocodebutton) {
    var promocodeapplied = false;
    if (objModelTours.TourList() != null) {
        if (objModelTours.TourList().length > 0) {

            $.each(objModelTours.TourList(), function (i, item) {
                objModelTours.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.ppRateId())
                if (objModelTours.PromoCodeDTO() != null) {
                    if (objModelTours.PromoCodeDTO().ppAmount() > 0) {

                        //--Add PromoCodeDTO object to list (save this list to cookie)
                        objModelTours.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));
                        objModelTours.PromoCodeDTO().ppServiceID(item.ppServiceID());
                        objModelTours.PromoServiceList.push(objModelTours.PromoCodeDTO())
                        //--

                        var originalprice = item.ppAdults();
                        var price = item.ppAdults();
                        if (price > 0) {
                            if (objModelTours.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                price = price - (price * (objModelTours.PromoCodeDTO().ppAmount() / 100));
                                //item.Price(price.toFixed(2));
                            } else {
                                if (price > objModelTours.PromoCodeDTO().ppAmount()) {
                                    price = price - objModelTours.PromoCodeDTO().ppAmount();
                                } else {
                                    price = 0;
                                }

                                //item.Price(price);
                            }
                        }

                        if (originalprice > price) {
                            $("#ServicePriceWithDiscount_" + item.ppServiceID()).text('$' + price.toFixed(2));
                            $("#ServicePriceWithDiscount_" + item.ppServiceID()).show();
                            $("#servicePrice_" + item.ppServiceID()).css("text-decoration", "line-through");
                        } else {
                            $("#ServicePriceWithDiscount_" + item.ppServiceID()).text(''); //clean 'price with discount' label
                            $("#servicePrice_" + item.ppServiceID()).css("text-decoration", "none"); //remove line (style) over the price
                        }

                        promocodeapplied = true;
                    }
                }
            });

            if (promocodebutton && promocodeapplied) {
                //--Save promo code info in cookie
                objModelTours.SavePromoCodeCookie();
                //--

                alertDialog('Promotional code was applied');
            }

            $('#txtPromotionalCode').prop('readonly', true);
            $("#btnPromoCode").html('Revert');
            $("#PromoApprovedIcon").show(300);

        }
    }
}

function validatecartmenuoption() {
    if (objModelTours.InsertedServicesList() != null) {
        if (objModelTours.InsertedServicesList().length > 0) {
            $("#cart_items").text(objModelTours.InsertedServicesList().length);
            $("#cart_items").show();
            $("#progress_tourscart").css("cursor", "pointer");

            $("#cart_items").click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });

            $("#progressmenu li#progress_tourscart").find('a').css("cursor", "pointer");

            $("#progressmenu li#progress_tourscart").find('a').click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });

            $("#progressmenu li#progress_tourscart").click(function () {
                loading(true);

                var url = $("#txtTrackingURLBase").val() + '/ServiceCart/ServiceCart?hotelcode=' + $.trim(objModelTours.CompanyCode());
                window.location.href = url;

                loading(false);
            });
        } else {
            $("#cart_items").hide();
            $("#progressmenu li#progress_tourscart").find('a').click(function () { return false });
            $("#progressmenu li#progress_tourscart").find('a').css("cursor", "default");
        }
    } else {
        $("#cart_items").hide();
        $("#progressmenu li#progress_tourscart").find('a').click(function () { return false });
        $("#progressmenu li#progress_tourscart").find('a').css("cursor", "default");
    }
}

function formatDate_v2(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";

    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var dayname = weekday[date.getDay()];
    var monthname = months[date.getMonth()];
    var daynumber = date.getDate();
    var ordinalday;

    if (daynumber == 1 || daynumber == 21 || daynumber == 31) {
        ordinalday = daynumber + 'st'
    } else if (daynumber == 2 || daynumber == 22) {
        ordinalday = daynumber + 'nd'
    } else if (daynumber == 3 || daynumber == 23) {
        ordinalday = daynumber + 'rd'
    } else {
        ordinalday = daynumber + 'th'
    }
   
    returnedvalue = monthname + ', ' + dayname + ' ' + ordinalday;

    return returnedvalue;
}

function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);    
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
    
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--