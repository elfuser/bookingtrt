﻿

//Convert Date to UTC Formate
Date.prototype.ConvertDateToUTC = function () {

    var sdateTemp;
    var UTCdate;

    if (this != null) {

        if (this.toString().indexOf("/Date(") == -1) {

            sdateTemp = new Date(this.toString());

        }
        else {

            sdateTemp = new Date(parseInt(this.substring(6)))
        }


    }

    var dt = new Date(sdateTemp);
    UTCdate = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), 0, 0, 0, 0));

    return UTCdate;

}

//Convert Date to Json Formate
Date.prototype.ConvertDateToJsonFormate = function () {

    var time = this.getTime();
    var timezone = this.getTimezoneOffset();

    var totaltime = parseInt(timezone) * 60000 * (-1) + parseInt(time);

    var Jsondate = "\/Date(" + totaltime.toString() + ")\/";
    //var Jsondate = "\/Date(" + this.getTime().toString() + ")\/";
    return Jsondate;
};

String.prototype.ConverteJsonDateToLocalTimeZone = function () {
    var d = new Date();
    var ShipDate;
    var offset = d.getTimezoneOffset();

    if (this.toString().indexOf("-") != -1) {

        var timeoffsetfromservicedate = this.toString().substring(this.toString().indexOf("-") + 1, this.toString().indexOf(")/"));

        var offsethour = parseInt(timeoffsetfromservicedate.toString().substring(0, 2) * 60) + parseInt(timeoffsetfromservicedate.toString().substring(2, 4));

        var totalmillsecond = parseInt(offset) * 60000 + parseInt(parseInt(this.toString().substring(6))) - parseInt(offsethour) * 60000;

        ShipDate = new Date(totalmillsecond);
    }
    else {
        var totalmillsecond;
        if (this.toString().indexOf("+") > 0) {

            var timeoffsetfromservicedate = this.toString().substring(this.toString().indexOf("+") + 1, this.toString().indexOf(")/"));

            var offsethour = parseInt(timeoffsetfromservicedate.toString().substring(0, 2) * 60) + parseInt(timeoffsetfromservicedate.toString().substring(2, 4));

            totalmillsecond = parseInt(offset) * 60000 + parseInt(parseInt(this.toString().substring(6))) + parseInt(offsethour) * 60000;
        }
        else {
            totalmillsecond = parseInt(parseInt(this.toString().substring(6)));
        }
        ShipDate = new Date(totalmillsecond);
    }


    var getMonth = ShipDate.getMonth() + 1;
    var getDay = ShipDate.getDate();
    var getYear = ShipDate.getFullYear()

    var formatedShipDate = getMonth + '/' + getDay + '/' + getYear;
    return formatedShipDate;
}

function decode_base64(s) {
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [43, 44], [47, 48]];

    for (z in n) { for (i = n[z][0]; i < n[z][1]; i++) { v.push(w(i)); } }
    for (i = 0; i < 64; i++) { e[v[i]] = i; }

    for (i = 0; i < s.length; i += 72) {
        var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)]; b = (b << 6) + c; l += 6;
            while (l >= 8) { r += w((b >>> (l -= 8)) % 256); }
        }
    }
    return r;
}

function LoadClasses() {
    var self = this;

    self.ClassList = new Array();
    self.AllClassesList = ko.observableArray();

    self.AllClassesList = [{ classes: "50", Effectiveclass: "50" },
                            { classes: "55", Effectiveclass: "55" },
                            { classes: "60", Effectiveclass: "60" },
                            { classes: "65", Effectiveclass: "65" },
                            { classes: "70", Effectiveclass: "70" },
                            { classes: "77.5", Effectiveclass: "77" },
                            { classes: "85", Effectiveclass: "85" },
                            { classes: "92.5", Effectiveclass: "92" },
                            { classes: "100", Effectiveclass: "100" },
                            { classes: "110", Effectiveclass: "110" },
                            { classes: "125", Effectiveclass: "125" },
                            { classes: "150", Effectiveclass: "150" },
                            { classes: "175", Effectiveclass: "175" },
                            { classes: "200", Effectiveclass: "200" },
                            { classes: "250", Effectiveclass: "250" },
                            { classes: "300", Effectiveclass: "300" },
                            { classes: "400", Effectiveclass: "400" },
                            { classes: "500", Effectiveclass: "500"}];

    for (var i = 0; i < self.AllClassesList.length; i++) {

        var classdto = new Saas4all.BolClassListModel();

        classdto.Classes = self.AllClassesList[i].classes;
        classdto.EffectiveClasses = self.AllClassesList[i].Effectiveclass;

        self.ClassList.push(classdto);
    }
    return self.ClassList;
}


function OnlyPositiveNumeric(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode

    if (((charCode >= 48 && charCode <= 57) ||
    // comma, period and minus, . on keypad
                        charCode == 190 || charCode == 188 || charCode == 110 ||
    // Backspace and Tab and Enter
                        charCode == 8 || charCode == 9 || charCode == 13 ||
    // Home and End
                        charCode == 35 || charCode == 36 ||
    // left and right arrows
                        charCode == 37 || charCode == 39 ||
    // Del and Ins
                        charCode == 46) && !evt.shiftKey)

        return true;
    else
        return false;

    /*    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;*/

    return true;
}

function OnlyPostiveIntegers(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode

    if (((charCode >= 48 && charCode <= 57) ||
        // comma, period and minus, . on keypad
                        charCode == 190 || charCode == 188 || charCode == 110 ||
        // Backspace and Tab and Enter
                        charCode == 8 || charCode == 9 || charCode == 13 ||
        // Home and End
                        charCode == 35 || charCode == 36 ||
        // left and right arrows
                        charCode == 37 || charCode == 39 
        // Del and Ins
                        ) && !evt.shiftKey)

        return true;
    else
        return false;

    /*    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;*/

    return true;
}


function OnlyNumeric(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode

    if (((charCode >= 48 && charCode <= 57) ||
        // comma, period and minus, . on keypad
                        charCode == 190 || charCode == 188 || charCode == 110 ||
        // Backspace and Tab and Enter
                        charCode == 8 || charCode == 9 || charCode == 13 ||
        // Home and End
                        charCode == 35 || charCode == 36 ||
        // left and right arrows
                        charCode == 37 || charCode == 39 ||
        // Del and Ins
                        charCode == 46 || charCode==45) && !evt.shiftKey)
        //. and minus 
        return true;
    else
        return false;

    /*    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;*/

    return true;
}
/** Text Select***/
function TextSelect(data) {

    $("input").focus(function () { $(this).select(); });
    $("textarea").focus(function () { $(this).select(); });
}




function CalculatePCF(Pallets, Lenght, Width, Height, Weight) {

    var length;
    var width;
    var height;
    var volum;
    var density;
    var perpallet;
    var PCF;
    if (Lenght != null && $.trim(Lenght) != "" && Width != null && $.trim(Width) != "" && Height != null && $.trim(Height) != "" &&
     Weight != null && $.trim(Weight) != "" && Lenght != 0 && Width != 0 && Height != 0 && Weight != 0) {
        length = parseInt(Lenght);
        width = parseInt(Width);
        height = parseInt(Height);
        if (length != null && $.trim(length) != "" && width != null && $.trim(width) != "" && height != null && $.trim(height) != null) {
            volum = (length * width * height) / 1728;
        }

        if (volum != null && $.trim(volum) != "" && volum != 0) {
            if (Pallets != null && $.trim(Pallets) != "" && Pallets != 0) {
                perpallet = parseFloat(Weight) / parseFloat(Pallets).toFixed(4);
                if (perpallet != null && $.trim(perpallet) != "" && perpallet != 0) {
                    density = parseFloat(perpallet) / parseFloat(volum).toFixed(4);
                }
            }
            else {
                density = parseFloat(Weight) / parseFloat(volum).toFixed(4);
            }
        }


        if (density != null && $.trim(density) != "") {
            PCF = parseFloat(density).toFixed(2);
        }
    }
    return PCF;
}


function GetMailEventForBolStatus(statusId) {
    var MailEventID; 
    if (statusId == 0) {
        MailEventID = 3;
    }
    else if (statusId == 1) {
        MailEventID = 4;
    }
    else if (statusId == 2) {
        MailEventID = 1;
    }
    else if (statusId == 4) {
        MailEventID = 2;
    }
    else if (statusId == 5) {
        MailEventID = 6;
    }
    else if (statusId == 6) {
        MailEventID = 5;
    }

    else if (statusId == 8) {
        MailEventID = 40;
    }
    else if (statusId == 9) {
        MailEventID = 15;
    }

    return MailEventID;

}

function FixDecimal (data) {

    var fixedValue;

    fixedValue = parseFloat(data).toFixed(2);

    return fixedValue;
}

Date.prototype.ConvertDateToMMDDYYFormat = function () {


    var date = this;
    var getMonth = date.getMonth() + 1;
    var getDay = date.getDate();
    var getYear = date.getFullYear()
    var mdyDate = getMonth + '/' + getDay + '/' + getYear;
    return mdyDate;
}

function validatedate(inputText) {
    var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;

    // Match the date format through regular expression  
    if (inputText.match(dateformat)) {

        //Test which seperator is used '/' or '-'  
        var opera1 = inputText.split('/');
        var opera2 = inputText.split('-');
        lopera1 = opera1.length;
        lopera2 = opera2.length;
        // Extract the string into month, date and year  
        if (lopera1 > 1) {
            var pdate = inputText.split('/');
        }
        else if (lopera2 > 1) {
            var pdate = inputText.split('-');
        }
        var mm = parseInt(pdate[0]);
        var dd = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]  
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {

                return false;
            }
        }
        if (mm == 2) {
            var lyear = false;
            //if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {

                return false;
            }
            if ((lyear == true) && (dd > 29)) {

                return false;
            }
        }
    }
    else {

        return false;
    }

}

    function EstimateClassFromPCF(pCF) {

        var PCF = pCF;
        var newClass = 0;

        if (PCF < 1)
            newClass = 400;
        if (PCF >= 1 && PCF < 2)
            newClass = 300;
        if (PCF >= 2 && PCF < 4)
            newClass = 250;
        if (PCF >= 4 && PCF < 6)
            newClass = 150;
        if (PCF >= 6 && PCF < 8)
            newClass = 125;
        if (PCF >= 8 && PCF < 10)
            newClass = 100;
        if (PCF >= 10 && PCF < 12)
            newClass = 92.5;
        if (PCF >= 12 && PCF < 15)
            newClass = 85;
        if (PCF >= 15 && PCF < 22.5)
            newClass = 70;
        if (PCF >= 22.5 && PCF < 30)
            newClass = 65;
        if (PCF >= 30 && PCF < 35)
            newClass = 60;
        if (PCF >= 35 && PCF < 50)
            newClass = 55;
        if (PCF >= 50)
            newClass = 50;

        return newClass;
    }

    function GetUniversalDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function formatDateJson(date, separator_char) {


        var dateString = date;
        var dx = new Date(parseInt(dateString.substr(6)));

        var dd = dx.getDate();
        var mm = dx.getMonth() + 1;
        var yy = dx.getFullYear();

        if (dd <= 9) {
            dd = "0" + dd;
        }

        if (mm <= 9) {
            mm = "0" + mm;
        }

        var displayDate = mm + separator_char + dd + separator_char + yy;

        return displayDate;
    }