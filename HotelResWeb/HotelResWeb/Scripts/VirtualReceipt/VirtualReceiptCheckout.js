﻿/// <reference path="../DTO/ReservationModel.js" />
function CheckoutVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();

    
    
    
    self.PromoList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.CardTypeList = ko.observableArray();
    

    self.getLogo = ko.observable();

    self.TempAddedAddonList = ko.observableArray();


    self.ReservationDTO = ko.observable(new ReservationModel());
    self.VirtuaReceiptDTO = ko.observable(new VirtualReceiptsModel());
    self.VirtualReceiptsList = ko.observableArray();
    self.selectedCountry = ko.observable(new CountriesModel());

    self.BNReqModel = ko.observable(new BNRequestModel());
    self.CredomaticReqModel = ko.observable(new CredomaticRequestModel());

    self.VRReservationDTO = ko.observable(new VRReservationModel());

    self.GetVirtualReceipt = function (ReceiptCode) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/GetVirtualReceipt?ReceiptCode=" + ReceiptCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        $.each(ds, function (key, value) {
                            var VirtualReceiptDTO = new VirtualReceiptsModel();
                            VirtualReceiptDTO.MapEntity(value, VirtualReceiptDTO);
                            self.VirtualReceiptsList.push(VirtualReceiptDTO);
                        })
                      
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.PrepareDtoForInsert = function () {
        //self.VRReservationDTO().id(0);
        self.VRReservationDTO().ReceiptCode($("#hdf_ReceiptCode").val());

        self.VRReservationDTO().CompanyID(self.CompanyCode());
        self.VRReservationDTO().FName($("#firstname").val());
        self.VRReservationDTO().LName($("#lastname").val());
        self.VRReservationDTO().Country($('#ddl_country').find(":selected").val());       
        self.VRReservationDTO().Phone($("#phone").val());
        self.VRReservationDTO().EMail($("#email").val());

        //self.VRReservationDTO().PromCode($("#txtPromotionalCode").val());
        //self.VRReservationDTO().TaxAmount(parseFloat($("#taxAmount").val()));
        //self.VRReservationDTO().AddonAmount(parseFloat($("#addonAmount").val()));
        //self.VRReservationDTO().PromoAmount(0);
        //self.VRReservationDTO().Discount(parseFloat($("#discount").val()));
        self.VRReservationDTO().total(parseFloat($("#totalAmount").val()));
        //self.VRReservationDTO().Subtotal(parseFloat($("#Subtotal").val()));
        
        self.VRReservationDTO().CardType($('#ddl_card_type').find(":selected").val());

        if (checkoutObj.VirtualReceiptsList() != null) {
            if (checkoutObj.VirtualReceiptsList().length > 0) {
                self.VRReservationDTO().ClientName(self.VirtualReceiptsList()[0].ClientName());
                self.VRReservationDTO().Description(self.VirtualReceiptsList()[0].Description());
                self.VRReservationDTO().Amount(self.VirtualReceiptsList()[0].Amount());                
            }
        }

        self.VRReservationDTO().CardNumber($("#cardnumber").val());
        self.VRReservationDTO().ExpeMonth($('#ddl_expiration_month').find(":selected").val());
        self.VRReservationDTO().ExpeYear($('#ddl_expiration_year').find(":selected").val());
        self.VRReservationDTO().NameCard($("#cardname").val());
        
                
    }    

    self.SaveReservation = function () {

        var bReturn;
        self.PrepareDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.VRReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var ReservInfo = new VRReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.VRReservationDTO(ReservInfo);
                        //call process card 
                        //by the processor type

                        self.ProcessCard();
                        
                        bReturn = true;

                    }
                }
               
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    //-- Save Tour Service Info in cookie
    self.SaveVirtualReceiptCookieOnly = function () {

        var bReturn;
        self.PrepareDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.VRReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/SaveVirtualReceiptInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--


    self.ProcessCard = function () {

        var bReturn;

        try {

            loading(true, 'Processing pay request ...');


            if (self.VRReservationDTO().Processor().toLowerCase() === "bn")
            {
                bReturn = self.RequestBN();
            }

            if (self.VRReservationDTO().Processor().toLowerCase() === "credomatic") {
                bReturn = self.ProcessPay();
            }

            loading(false);

        }
        catch (exc)
        {
            loading(false);
        }

        return bReturn;
    }


    self.RequestBN = function () {

        var bReturn = false;
   
        var NewReserv = JSON.stringify(ko.toJS(self.VRReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/GenerateBNParams",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {
                    if (response != '') {
                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {

                            var BNModel = new BNRequestModel();
                            BNModel.MapEntity(ds, BNModel);
                            self.BNReqModel(BNModel);

                            $("#dataForm").submit();

                            bReturn = true;

                        }
                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }


    self.ProcessPay = function () {

        var bReturn;

        var NewReserv = JSON.stringify(ko.toJS(self.VRReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/ProcessPay",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var CredomaticModel = new CredomaticRequestModel();
                        CredomaticModel.MapEntity(ds, CredomaticModel);
                        self.CredomaticReqModel(CredomaticModel);

                        var postPage = self.CredomaticReqModel().action();

                        $('#PostForm').attr('action', postPage);
                        $("#PostForm").submit();

                        bReturn = true;

                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }   
   
    self.LoadCountries = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/DireccionServices.svc" + "/json/GetCountries" ,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCountriesResult, function (key, value) {
                        var countryDTO = new CountriesModel();
                        countryDTO.MapEntity(value, countryDTO);
                        self.CountryList.push(countryDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Load card types
    self.LoadCardTypes = function (bank) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ReservationService.svc" + "/json/GetCardTypes?Bank=" + bank,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCardTypesResult, function (key, value) {
                        var cardtypeDTO = new CardTypesModel();
                        cardtypeDTO.MapEntity(value, cardtypeDTO);
                        self.CardTypeList.push(cardtypeDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }           
    
    //-- Load countries in dropdown
    self.LoadCountriesInDropDownList = function () {
        var dll_countries = $("#ddl_country");

        if (self.CountryList() != null) {
            if (self.CountryList().length > 0) {
                $.each(self.CountryList(), function () {
                    dll_countries.append($("<option />").val(this.ppCountryIso3()).text(this.ppName()));
                });
            } else {
                dll_countries.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_countries.append($("<option />").val(-1).text("---"));
        }        
    }

    //-- Load cardtypes in dropdown
    self.LoadCardTypesInDropDownList = function () {
        var dll_cardtypes = $("#ddl_card_type");

        if (checkoutObj.CardTypeList() != null) {
            if (checkoutObj.CardTypeList().length > 0) {
                $.each(checkoutObj.CardTypeList(), function (i, item) {
                    dll_cardtypes.append($("<option />").val($.trim(item.ppCode())).text($.trim(item.ppName())));
                });
            } else {
                dll_cardtypes.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_cardtypes.append($("<option />").val(-1).text("---"));
        }
    }


    self.InitModel = function () {

        self.GetVirtualReceipt($("#hdf_ReceiptCode").val());

        self.CompanyCode($("#hdf_CompanyCode").val());
        //self.CompanyCode("VC0213");

        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=4");       
        
        self.LoadCountries();

        self.VRReservationDTO().Processor("credomatic") //uncomment for demo

    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//

}


var checkoutObj;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {             

               try{
                   loading(true, 'Please wait ...');

                   checkoutObj = new CheckoutVm();
                   ko.applyBindings(checkoutObj);                  

                   var urlCompanyLogo = checkoutObj.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);

                   

                   var showPaymentMessageSection = false;                  
                   var exit = false;
                   if (checkoutObj.VirtualReceiptsList() != null) {
                       if (checkoutObj.VirtualReceiptsList().length > 0) {                           
                           var PaymentMessage = '';

                           showPaymentMessageSection = checkoutObj.VirtualReceiptsList()[0].ReceiptPaid();
                           if (showPaymentMessageSection) {
                               $("#LinkPaymentMessageSection").show();
                               $("#lblLinkPaymentMessage").text("This receipt has been paid.");
                               $("#checkout_form").hide();
                               exit = true;
                               loading(false);                               
                           }

                           if (!exit) {
                               var JsonExpiryDate = checkoutObj.VirtualReceiptsList()[0].Expiry();
                               if (JsonExpiryDate != null) {
                                   var ExpiryDate = new Date(parseInt(JsonExpiryDate.substr(6)))
                                   if (ExpiryDate != null) {
                                       var today = new Date();

                                       if (ExpiryDate <= today) {
                                           $("#LinkPaymentMessageSection").show();
                                           $("#lblLinkPaymentMessage").text("This receipt has expired.");
                                           $("#checkout_form").hide();
                                           loading(false);

                                           showPaymentMessageSection = true;
                                       }
                                   }
                               }
                           }                                                     
                           
                       }
                   }

                   //-- Load Summary info
                   LoadSummaryInfo();

                   if (showPaymentMessageSection) {
                       return false;
                   }

                   //-- Load countries in dropdownlist
                   checkoutObj.LoadCountriesInDropDownList();

                   $('#ddl_country').selectpicker('refresh');

                   //-- Show card payment info
                   var ExcludeCardValidation = false;
                   if (checkoutObj.VRReservationDTO() != null) {
                       if (checkoutObj.VRReservationDTO().Processor() != null) {
                           if (checkoutObj.VRReservationDTO().Processor() != '') {
                               var paymenttype = checkoutObj.VRReservationDTO().Processor().toString().toLowerCase();
                               paymenttype = 'credomatic' //uncomment for demo

                               //Load card types
                               checkoutObj.LoadCardTypes(paymenttype);

                               //-- Load countries in dropdownlist
                               checkoutObj.LoadCardTypesInDropDownList();
                               
                               switch(paymenttype){                                   
                                   case "bn":
                                       $("#cardinfosection").hide();
                                       ExcludeCardValidation = true;
                                       break;
                                   default:
                                       $("#cardinfosection").show();
                                       break;
                               }
                               
                           }
                       }
                   }                   

                   //-- Validate fields in form
                   FormFieldsValidationSubmit(checkoutObj, ExcludeCardValidation);

                                               

                   loading(false);

               }
               catch (ex) {
                   alertDialog("Error loading summary information.");
                   loading(false);
               }                              

              
           }
);
//*************************************//


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    //$("#dialog").show();
    //$("#dialog").dialog("open");
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function formatDateToShowLabel(date) {
    var returnedvalue;    
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var day = '' + date.getDate();
    var year = date.getFullYear();
    
    if (day.length < 2) day = '0' + day;

    var monthname = months[date.getMonth()];    

    returnedvalue = day + '-' + monthname + '-' + year;

    return returnedvalue;
}

function FormFieldsValidationSubmit(coObj, ExcludeCardValidation)
{
    //-- Form fields validations
    $('#checkout_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            lastname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },            
            cardname: {
                excluded: ExcludeCardValidation,
                validators: {
                    stringLength: {
                            min: 8,
                            },
                    notEmpty: {
                            message: 'Please supply your card name'
                    }
                }
            },
            ddl_card_type: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'The type is required'
                    }
                }
            },
            cardnumber: {
                excluded: ExcludeCardValidation,
                validators: {
                        creditCard: {
                                message: 'The card number is not valid'
                        },
                        notEmpty: {
                                message: 'Please supply your card number'
                        }
                }
            },
            expiration_month_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    }
                }
            },
            expiration_year_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration year'
                    }
                }
            },
            txtCVC: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    },
                    cvv: {
                        creditCardField: 'cardnumber',
                        message: 'The CVV number is not valid'
                    }
                }
            },
            acceptTerms: {
                validators: {
                    notEmpty: {
                        message: 'You have to accept General Terms and Conditions'
                    }
                }
            }
        }
    })    
    .on('success.form.bv', function (e) {
        //$('#success_message').slideDown({ opacity: "show" }, "slow") //

        // Prevent form submission
        e.preventDefault();       

        // Get the form instance
        //var $form = $(e.target);

        // Get the BootstrapValidator instance
        //var bv = $form.data('bootstrapValidator');

        try
        {
            loading(true, 'Please wait ...');
            coObj.SaveVirtualReceiptCookieOnly();
            coObj.SaveReservation();
            var url = $("#txtTrackingURLBase").val() + '/Confirmation/Confirmation?hotelcode=' + $.trim(coObj.CompanyCode());
            //window.location.href = url; //uncomment for demo
            loading(false);

        } catch (ex)
        {
            loading(false);
        }

        // Use Ajax to submit form data
        //$.post($form.attr('action'), $form.serialize(), function (result) {
        //    console.log(result);
        //}, 'json');
    });

}

function LoadSummaryInfo() {
    if (checkoutObj.VirtualReceiptsList() != null) {
        if (checkoutObj.VirtualReceiptsList().length > 0) {
            var TOTAL = 0;
            var clientname, description;

            TOTAL = checkoutObj.VirtualReceiptsList()[0].Amount().toFixed(2);
            clientname = checkoutObj.VirtualReceiptsList()[0].ClientName();
            description = checkoutObj.VirtualReceiptsList()[0].Description();

            $("#lbClientName").html(clientname);
            $("#lbDescription").html(description);
            $("#lbAmount").html(TOTAL);                       
        }
    }
}

//Delete Cookie
function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--