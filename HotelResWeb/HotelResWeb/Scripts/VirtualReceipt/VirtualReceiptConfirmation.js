﻿function VRConfirmationVm(mainVm) {

    var self = this;
    self.TourlVm = ko.observable();

    self.getLogo = ko.observable();

    self.appData = mainVm;

    self.SessionId;
    self.ServiceId;

    self.TourList = ko.observableArray();
    self.TourServiceRateList = ko.observableArray();
    self.ServiceSchedAvailList = ko.observableArray();

    //-- Company variables
    self.CompanyCode = ko.observable();
    self.CompanyName = ko.observable();
    self.ppEmail = ko.observable();
    self.ppAddress = ko.observable();
    self.ppWeb = ko.observable();
    self.ppTel1 = ko.observable();
    self.Tax = ko.observable();
    self.Processor = ko.observable();
    self.RentPorc = ko.observable();
    self.ppFacebookURL = ko.observable();    
    self.ppMaxAdults = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppGoogleID = ko.observable();
    self.ppActive = ko.observable();    
    //--

    //-- Service variables
    self.ppDescription = ko.observable();
    self.ppDetails = ko.observable();
    self.ppBringInfo = ko.observable();
    self.ppConditions = ko.observable();
    self.ppPicture1 = ko.observable();
    self.ppPicture2 = ko.observable();
    self.ppPicture3 = ko.observable();
    self.ServiceImageList = ko.observableArray();
    //--

    //self.TourServiceSessionInfo = ko.observable(new TourServiceSessionModel());
    //self.ServiceInfo = ko.observable(new ServiceModelLite());
    self.InsertedServicesList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.CardTypeList = ko.observableArray();
    self.VRReservationDTO = ko.observable(new VRReservationModel());
    self.VirtuaReceiptDTO = ko.observable(new VirtualReceiptsModel());
    self.VirtualReceiptsList = ko.observableArray();
    

    //*******************DEFAULT VALUES START*****************//
   
    self.TourlVm(mainVm);   

    //*******************DEFAULT VALUES END*****************//

    self.LoadCompany = function (data) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + self.CompanyCode() + "&sType=S",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0))
                    {
                         self.CompanyName(response.GetCompanyResult["0"].ppCompanyName);
                         self.ppEmail(response.GetCompanyResult["0"].ppEmail);
                         self.ppAddress(response.GetCompanyResult["0"].ppAddress);
                         self.ppWeb(response.GetCompanyResult["0"].ppWeb);
                         self.CompanyCode(response.GetCompanyResult["0"].ppCompanyCode);
                         self.ppTel1(response.GetCompanyResult["0"].ppTel1);
                         self.Tax(response.GetCompanyResult["0"].ppTaxPerc);
                         self.Processor(response.GetCompanyResult["0"].Processor);
                         self.RentPorc(response.GetCompanyResult["0"].ppRentPorc);

                         self.ppFacebookURL(response.GetCompanyResult["0"].ppFacebookURL);                         
                         self.ppMaxAdults(response.GetCompanyResult["0"].ppMaxAdults);
                         self.ppMaxChildren(response.GetCompanyResult["0"].ppMaxChildren);
                         self.ppGoogleID(response.GetCompanyResult["0"].ppGoogleID);
                         self.ppActive(response.GetCompanyResult["0"].ppActive);                                                                                

                    }                    
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    

    self.GetVirtualReceipt = function (ReceiptCode) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceiptCheckout/GetVirtualReceipt?ReceiptCode=" + ReceiptCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        $.each(ds, function (key, value) {
                            var VirtualReceiptDTO = new VirtualReceiptsModel();
                            VirtualReceiptDTO.MapEntity(value, VirtualReceiptDTO);
                            self.VirtualReceiptsList.push(VirtualReceiptDTO);
                        })

                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.SaveReservation = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.TourServiceSessionInfo));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/ServiceCart/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        //var ReservInfo = new ReservationModel();
                        //ReservInfo.MapEntity(ds, ReservInfo);
                        //self.ReservationDTO(ReservInfo);
                        ////call process card 
                        ////by the processor type

                        //self.ProcessCard();

                        bReturn = true;

                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        //return bReturn;
    }
       

    self.PrepareServiceDtoForInsert = function (SessionId, ServiceID, ServiceName, ArrivalDate, ScheduleID, ScheduleDesc, HotelPickUpID, HotelPickupHourID, Adults, Children, Infants,
        Students, PriceAdults, PriceChildren, PriceInfants, PriceStudent) {      
        
        self.ServiceInfo = ko.observable(new ServiceModelLite());
        self.ServiceInfo().SessionId(SessionId);
        self.ServiceInfo().ppServiceID(ServiceID);
        self.ServiceInfo().ppServiceName(ServiceName);
        self.ServiceInfo().ppArrivalDate(ArrivalDate);
        self.ServiceInfo().ppScheduleID(ScheduleID);
        self.ServiceInfo().ppScheduleDesc(ScheduleDesc);
        self.ServiceInfo().ppHotelPickUpID(HotelPickUpID);        
        self.ServiceInfo().ppHotelPickupHourID(HotelPickupHourID);
        self.ServiceInfo().ppAdults(Adults);
        self.ServiceInfo().ppChildren(Children);
        self.ServiceInfo().ppInfants(Infants);
        self.ServiceInfo().ppStudents(Students);
        self.ServiceInfo().ppPriceAdults(PriceAdults);
        self.ServiceInfo().ppPriceChildren(PriceChildren);
        self.ServiceInfo().ppPriceInfants(PriceInfants);
        self.ServiceInfo().ppPriceStudent(PriceStudent);

        return self.ServiceInfo();
    }

    self.PrepareReservationInfoDtoForInsert = function () {
        self.TourServiceSessionInfo().id(0);

        self.TourServiceSessionInfo().CompanyID(self.CompanyCode());
        self.TourServiceSessionInfo().FName($("#firstname").val());
        self.TourServiceSessionInfo().LName($("#lastname").val());
        self.TourServiceSessionInfo().Country($('#ddl_country').find(":selected").val());
        self.TourServiceSessionInfo().Phone($("#phone").val());
        self.TourServiceSessionInfo().EMail($("#email").val());

        self.TourServiceSessionInfo().PromCode($("#txtPromotionalCode").val());
        self.TourServiceSessionInfo().TaxAmount(parseFloat($("#taxAmount").val()));
        self.TourServiceSessionInfo().AddonAmount(parseFloat($("#addonAmount").val()));
        self.TourServiceSessionInfo().PromoAmount(0);
        //self.TourServiceSessionInfo().Discount(parseFloat($("#discount").val()));
        self.TourServiceSessionInfo().total(parseFloat($("#totalAmount").val()));
        self.TourServiceSessionInfo().Subtotal(parseFloat($("#Subtotal").val()));

        self.TourServiceSessionInfo().CardType($('#ddl_card_type').find(":selected").val());
    }   

    //-- Save Reservation Info in cookie
    self.SaveReservationCookieOnly = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //------------------------------- Begin - Get cookies info -------------------------------//

    //-- Get tour service info from the cookie
    self.GetTourServiceCookieInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetTourServiceCookieInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);               
                    self.TourServiceSessionInfo().MapEntity(ds, self.TourServiceSessionInfo());

                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                //self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedServices = function () {

        //self.SavedServicesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/ServiceDetail/GetSavedServices",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var Info = new ServiceModelLite();
                        Info.MapEntity(value, Info);

                        var arrivaldate = new Date(parseInt(Info.ppArrivalDate().substr(6)));
                        Info.ppFormattedArrivalDate(formatDate_v2(arrivaldate, "/", "mmddyyyy"))
                        Info.ppArrivalDate(arrivaldate)

                        Info.ppTotalPerAdults('$' + parseFloat(Info.ppPriceAdults() * Info.ppAdults()).toFixed(2));
                        Info.ppTotalPerChildren('$' + parseFloat(Info.ppPriceChildren() * Info.ppChildren()).toFixed(2));
                        Info.ppTotalPerInfants('$' + parseFloat(Info.ppPriceInfants() * Info.ppInfants()).toFixed(2));
                        Info.ppTotalPerStudents('$' + parseFloat(Info.ppPriceStudent() * Info.ppStudents()).toFixed(2));

                        if (Info.ppAdults() > 0) {
                            Info.ShowHideAdult("ShowSummaryItem");
                        } else {
                            Info.ShowHideAdult("HideSummaryItem");
                        }

                        if (Info.ppChildren() > 0) {
                            Info.ShowHideChildren("ShowSummaryItem");
                        } else {
                            Info.ShowHideChildren("HideSummaryItem");
                        }

                        if (Info.ppInfants() > 0) {
                            Info.ShowHideInfants("ShowSummaryItem");
                        } else {
                            Info.ShowHideInfants("HideSummaryItem");
                        }

                        if (Info.ppStudents() > 0) {
                            Info.ShowHideStudents("ShowSummaryItem");
                        } else {
                            Info.ShowHideStudents("HideSummaryItem");
                        }

                        if (Info.ppHotelPickUpID() > 0) {
                            Info.ShowHidePickupSection("ShowSummaryItem");
                        } else {
                            Info.ShowHidePickupSection("HideSummaryItem");
                        }

                        Info.UniqueId(s4() + s4());

                        self.InsertedServicesList.push(Info);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //------------------------------- End - Get cookies info -------------------------------//
   

    function formatDate(date, separator_char) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator_char);
    }
 

    //-- Continue Shopping Button
    ContinueShopping_Button = function (data, event) {
        try {            

            loading(true);

            var url = $("#txtTrackingURLBase").val() + '/ToursServices/ToursServices?hotelcode=' + $.trim(self.CompanyCode());
            window.location.href = url;

            loading(false);

        }
        catch (ex) {
            loading(false);
        }
    }
    //--     

    self.InitModel = function () {                                                      

        self.GetVirtualReceipt($("#hdf_ReceiptCode").val());

        self.CompanyCode($("#hdf_CompanyCode").val());
        //self.CompanyCode("VC0213");

        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=4");

    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var objModelTours;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              
               
               try{                              
                   loading(true, 'Please wait ...');                   
                                      
                   objModelTours = new VRConfirmationVm();
                   ko.applyBindings(objModelTours);

                   var urlCompanyLogo = objModelTours.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);

                   $("#idCompanyLogo").click(function () {
                       var url = "http://" + objModelTours.ppWeb();
                       window.location.href = url;
                   });
                   

                   ////Load Footer Info
                   //$("#lblCompany").text(objModelTours.CompanyName());
                   //$("#lblAddress").text(objModelTours.ppAddress());
                   //$("#lblPhoneCo").text(objModelTours.ppTel1());
                   //$("#lnkEmail").text(objModelTours.ppEmail());
                   ////

                   //-- Adapt Header ContentPlaceHolder
                   $("#MasterDivLeftPanelContent").removeClass("col-sm-3");
                   $("#MasterDivLeftPanelContent").hide();

                   $("#MasterDivRightPanelContent").addClass("col-sm-3");
                   $("#MasterDivRightPanelContent").show();

                   //$("#MasterDivMainContent").removeClass("col-sm-9");
                   //$("#MasterDivMainContent").addClass("col-sm-8");

                   $("#MasterDivMainContent").addClass("MasterDivMainContentStyle");                   
                   //--                 
                  
                   ////---- Show summary section
                   //if (objModelTours.InsertedServicesList() != null) {
                   //    if (objModelTours.InsertedServicesList().length > 0) {
                   //        $("#SummarySection").show();
                   //        LoadSummaryTotals();
                   //        $("#NoServicesSection").hide();                           
                   //    } else {
                   //        $("#SummarySection").hide();
                   //    }
                   //}else {
                   //    $("#SummarySection").hide();
                   //}

                   //-- Load Summary info
                   LoadSummaryInfo();
                   
                   //-- Return to website link
                   //$("#returntoSiteLink").attr("href", "http://" + objModelTours.ppWeb());
                   //$("#returntoSiteLink").text("Return to Web Site");
                   //--                   

                   //-- YouTube, Facebook and Twitter links / footer                   
                   //$("#facebooklink").attr("href", "http://www.facebook.com/" + objModelTours.ppFacebookURL());
                   //

                   //TESTRS
                   deleteCookie("VirtualReceiptInfo");

                   loading(false);
                  
               }
               catch (ex) {
                   loading(false);
                   alertDialog('Error loading virtual receipt details. Error: ' + ex.message);
               }

               
           }

           
);
//*******************                              ******************//

function formatDate_v2(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";

    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var dayname = weekday[date.getDay()];
    var monthname = months[date.getMonth()];
    var daynumber = date.getDate();
    var ordinalday;

    if (daynumber == 1 || daynumber == 21 || daynumber == 31) {
        ordinalday = daynumber + 'st'
    } else if (daynumber == 2 || daynumber == 22) {
        ordinalday = daynumber + 'nd'
    } else if (daynumber == 3 || daynumber == 23) {
        ordinalday = daynumber + 'rd'
    } else {
        ordinalday = daynumber + 'th'
    }
   
    returnedvalue = monthname + ', ' + dayname + ' ' + ordinalday;

    return returnedvalue;
}

function LoadSummaryInfo() {
    if (objModelTours.VirtualReceiptsList() != null) {
        if (objModelTours.VirtualReceiptsList().length > 0) {
            var TOTAL = 0;
            var clientname, description;

            TOTAL = objModelTours.VirtualReceiptsList()[0].Amount().toFixed(2);
            clientname = objModelTours.VirtualReceiptsList()[0].ClientName();
            description = objModelTours.VirtualReceiptsList()[0].Description();

            $("#lbClientName").html(clientname);
            $("#lbDescription").html(description);
            $("#lbAmount").html(TOTAL);
        }
    }
}

function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);    
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
    
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--

