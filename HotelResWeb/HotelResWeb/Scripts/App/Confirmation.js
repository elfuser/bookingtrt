﻿function ConfimationVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();

    self.InsertedAddonList = ko.observableArray();
    self.AddedRoomList = ko.observableArray();
    self.AddedAddonList = ko.observableArray();
    self.PromoList = ko.observableArray();
   
    self.getLogo = ko.observable();    

    self.ReservationDTO = ko.observable(new ReservationModel());

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();

    self.GetSavedAddons = function () {

        self.AddedAddonList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedAddons",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var AddonInfo = new AddOnLite();
                        AddonInfo.MapEntity(value, AddonInfo);
                        AddonInfo.PriceInText('$' + AddonInfo.Price());                        
                        self.AddedAddonList.push(AddonInfo);                        
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedRooms = function () {

        self.AddedRoomList.removeAll();


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedRooms",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var RommsInfo = new RoomLiteModel();
                        RommsInfo.MapEntity(value, RommsInfo);

                        if (RommsInfo.Discount() > 0) {
                            RommsInfo.PriceWithDiscLabel('$' + parseFloat(RommsInfo.PriceDisc()).toFixed(2));
                            RommsInfo.Tachclass("Discount_item_summary");
                        }

                        RommsInfo.PriceLabel('$' + parseFloat(RommsInfo.Price()).toFixed(2));

                        self.AddedRoomList.push(RommsInfo);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetReservationInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetReservationInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var ReservInfo = new ReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.ReservationDTO(ReservInfo);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetPromoCodeInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var Info = new PromoCodeModel();
                        Info.MapEntity(ds, Info);
                        self.PromoCodeDTO(Info);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, RateCode) {
        //self.AddOnTypeList.removeAll();

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmount?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&sRateCode=" + RateCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountResult != null) {
                        $.each(response.GetPromoCodeAmountResult, function (key, value) {
                            var objDTO = new PromoCodeModel();
                            objDTO.MapEntity(value, objDTO);
                            objDTO.ppPromoCode(PromoCode);
                            self.PromoCodeDTO(objDTO)
                            bResult = true;
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    //-- Start Over button
    self.StartOver = function () {
        deleteCookie("RoomInfo");
        deleteCookie("CurrentProgress");
        deleteCookie("AddonInfo");
        deleteCookie("ReservationInfo");
        deleteCookie("PromoCodeInfo");

        var url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim(self.CompanyCode())

        // uncomment for demo
        //url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim('BDIHS')

        window.location.href = url;
    }

    self.InitModel = function () {

        self.CompanyCode($("#hdf_HotelCode").val());
        //self.CompanyCode("VC0213");

        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=4");


        self.GetSavedAddons();
        self.GetSavedRooms();
        self.GetReservationInfo();
        self.GetPromoCodeInfo();
    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//

}


var confirmObj;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {             

               $("#dialog").dialog({
                   autoOpen: false,
                   modal: true,
                   buttons: {
                       Ok: function () {
                           $(this).dialog("close");
                       }
                   }
               });

               try {
                   loading(true, 'Please wait ...');

                   confirmObj = new ConfimationVm();
                   ko.applyBindings(confirmObj);

                   var urlCompanyLogo = confirmObj.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);                   

                   //-- Load Summary info
                   LoadSummaryInfo();

                   //Hide Promo Code button
                   $("#btnPromoCode").hide();
                   $('#txtPromotionalCode').prop('readonly', true);

                   loading(false);

               }
               catch (ex) {
                   alertDialog("Error loading summary information.");
                   loading(false);
               }             
           }
);

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function LoadSummaryInfo() {
    if (confirmObj.AddedRoomList() != null) {
        if (confirmObj.AddedRoomList().length > 0) {
            var SubTotalRoomNights = 0;
            var SubTotalRoomNightsWithDiscount = 0;
            var tax = 0;
            var TOTAL = 0;
            var Totaltax = 0;
            var oneDay = 24 * 60 * 60 * 1000;
            var applyPromoCode = false;

            //-- Number of nights
            var fromDate = new Date(confirmObj.AddedRoomList()[0].fromDt().replace(/-/g, "/"));
            var toDate = new Date(confirmObj.AddedRoomList()[0].toDt().replace(/-/g, "/"));
            var diffDays = Math.round(Math.abs((fromDate.getTime() - toDate.getTime()) / (oneDay)));
            //--          

            //-- Iterate rooms
            var guestsQty = 0;
            $.each(confirmObj.AddedRoomList(), function (i, item) {
                //-- Quantity of guests
                guestsQty += (parseInt(confirmObj.AddedRoomList()[i].Adults()) +
                           parseInt(confirmObj.AddedRoomList()[i].Kids()) +
                           parseInt(confirmObj.AddedRoomList()[i].Infants()));


                //-- Sum room price
                SubTotalRoomNights += (parseFloat(confirmObj.AddedRoomList()[i].Price()) * diffDays);
                SubTotalRoomNightsWithDiscount += (parseFloat(confirmObj.AddedRoomList()[i].PriceDisc()) * diffDays);
            })
            //--

            //-- Put info in summary labels
            var fromDateLabel = formatDateToShowLabel(fromDate);
            var toDateLabel = formatDateToShowLabel(toDate);

            $("#lbArrivalDate").html(fromDateLabel);
            $("#lbDepartureDate").html(toDateLabel);
            $("#lbRoomNights").html(diffDays);
            $("#lbRoomsQty").html(confirmObj.AddedRoomList().length);

            //$("#lbGuestsQty").html(guestsQty);
            //$("#lbRateSelected").html('$' + confirmObj.AddedRoomList()[0].Price());
            //SubTotalRoomNights = '$' + (parseFloat(confirmObj.AddedRoomList()[0].Price()) * diffDays);
            //$("#lbRoomTypeDesc").html(confirmObj.AddedRoomList()[0].Desc());

            $("#lbSubTotalRoomNights").html('$' + parseFloat(SubTotalRoomNights).toFixed(2));
            //--

            //Price with discount            
            //if (confirmObj.AddedRoomList()[0].Discount() > 0) {
            //    $("#lbRateSelectedDisc").html('$' + parseFloat(confirmObj.AddedRoomList()[0].PriceDisc()).toFixed(2));
            //    $("#lbRateSelected").addClass("Discount_item_summary");
            //}
            //else {
            //    $("#divDiscount").hide();
            //}

            $("#lbDiscount").html(confirmObj.AddedRoomList()[0].Discount() + ' %');
            Discount = SubTotalRoomNights * (confirmObj.AddedRoomList()[0].Discount() / 100);
            //

            //-- Take the tax of the first room, because it is the same tax for all the rooms
            tax = confirmObj.AddedRoomList()[0].TaxPerc();

            //-- Addons
            var SubTotalAddOns = 0;
            if (confirmObj.AddedAddonList != null) {
                if (confirmObj.AddedAddonList().length > 0) {
                    $.each(confirmObj.AddedAddonList(), function (i, item) {
                        SubTotalAddOns += parseFloat(item.Price());
                    })
                }
            }

            $("#lbSubTotalAddOns").html('$' + SubTotalAddOns);
            //--

            //-- Totals
            //if (confirmObj.AddedRoomList()[0].Discount() > 0) {
            //    TOTAL = ((parseFloat(confirmObj.AddedRoomList()[0].PriceDisc()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2);
            //}
            //else { TOTAL = ((parseFloat(confirmObj.AddedRoomList()[0].Price()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2); }

            //-- Verify Promo code info
            if (confirmObj.PromoCodeDTO() != null) {
                if (confirmObj.PromoCodeDTO().ppPromoCode() != null && confirmObj.PromoCodeDTO().ppPromoCode() != '') {
                    applyPromoCode = true;

                    $("#txtPromotionalCode").val(confirmObj.PromoCodeDTO().ppPromoCode());
                    $('#txtPromotionalCode').prop('readonly', true);
                    $("#btnPromoCode").html('Revert');
                    $("#PromoApprovedIcon").show(300);

                    $("#lblPromoCodeInHeader").text(confirmObj.PromoCodeDTO().ppPromoCode());
                    $("#PromoCodeInHeaderSection").show(300);

                    $("#lbPromoCodeSummary").text(confirmObj.PromoCodeDTO().ppPromoCode());

                    if (confirmObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        $("#lbPromoAmountSummary").text("- " + confirmObj.PromoCodeDTO().ppAmount() + '%');
                    } else {
                        $("#lbPromoAmountSummary").text('- $' + confirmObj.PromoCodeDTO().ppAmount());
                    }

                    $(".SummaryPromoCode").show();

                } else {
                    $(".SummaryPromoCode").hide();
                }
            } else {
                $(".SummaryPromoCode").hide();
            }
            //--

            //-- Totals
            if (confirmObj.AddedRoomList()[0].Discount() > 0) {
                TOTAL = (SubTotalRoomNightsWithDiscount + parseFloat(SubTotalAddOns)).toFixed(2);
            }
            else { TOTAL = (SubTotalRoomNights + parseFloat(SubTotalAddOns)).toFixed(2); }

            if (tax > 0) {
                Totaltax = parseFloat(TOTAL * (tax / 100));

                TOTAL = parseFloat(TOTAL) + Totaltax;
            }

            //$("#totalAmount").val(parseFloat(TOTAL));
            //$("#discount").val(parseFloat(Discount));
            //$("#addonAmount").val(parseFloat(SubTotalAddOns));
            //$("#taxAmount").val(parseFloat(Totaltax));

            //-- Check if we need to apply promo code discount
            if (applyPromoCode) {
                if (confirmObj.PromoCodeDTO().ppAmount() > 0) {
                    if (confirmObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        TOTAL = TOTAL - (TOTAL * (confirmObj.PromoCodeDTO().ppAmount() / 100));
                    } else {
                        if (TOTAL > confirmObj.PromoCodeDTO().ppAmount()) {
                            TOTAL = TOTAL - confirmObj.PromoCodeDTO().ppAmount();
                        } else {
                            TOTAL = 0;
                        }
                    }
                }
            }
            //--

            $("#lbTOTAL").html('$' + parseFloat(TOTAL).toFixed(2)); //TOTAL label
            $("#lbTaxes").html('$' + parseFloat(Totaltax).toFixed(2)); //Tax label
            //--
        }
    }
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var day = '' + date.getDate();
    var year = date.getFullYear();

    if (day.length < 2) day = '0' + day;

    var monthname = months[date.getMonth()];

    returnedvalue = day + '-' + monthname + '-' + year;

    return returnedvalue;
}

//Delete Cookie
function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}