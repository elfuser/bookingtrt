﻿
function MainPageModelVm() {


    //Variable
    var self = this;
    self.ClientID = ko.observable();


    //**********************PROGRESSBAR METHODS START*********************//
    var ProgressbarCount = 0;

    self.ProgressbarShow = function () {
        if (ProgressbarCount == 0) {
            $("#progressbar").show();
            $("#ProgressbarContainer").show();

            ProgressbarCount++;
        }
        else {
            ProgressbarCount++;
        }
    }

    self.ProgressbarHide = function () {

        ProgressbarCount--;

        if (ProgressbarCount == 0) {

            $("#progressbar").hide();
            $("#ProgressbarContainer").hide();
        }
        else {

            ProgressbarCount--;
        }
    }
    //**********************PROGRESSBAR METHODS END*********************//

    self.CancelErrorPopupDialog = function () {
        $("#ErrorDialogBox").dialog("close");
    }

    self.MainPageVmConstructor = function () {


    };


    self.MainPageVmConstructor();


}