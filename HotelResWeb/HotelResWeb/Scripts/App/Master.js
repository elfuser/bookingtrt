﻿function MasterVm() {
   
}

$(document).ready(function () {
        var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        //var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        if (!isChrome) {
            adaptHeaderNavBar();
        }
        
        getlang();
        CheckUserLogged();
        
        if ($.cookie("userNameBookingcookie") != null && $.cookie("userNameBookingcookie") != '') {
            $("#UserName").val($.cookie("userNameBookingcookie"));
            $("#Password").val($.cookie("passwordBookingcookie"));
        } else {
            $("#UserName").val('');
            $("#Password").val('');
        }

        $("#PromoCodeMenuLink").click(function () {
            $("#dialogPromoCodePopup").modal({ backdrop: "static" });
            $('#dialogPromoCodePopup').modal('show');
            $("#PromoCodeMessageSection").hide();
            if (!$("#txtPromotionalCode").prop('readonly')) {
                $("#txtPromotionalCode").val('');
            }
        });
        LoadCompany();
        $("#login-nav").submit(function (event) {

            if ($("#UserName").val()) {
                $("#UserName").val(($("#UserName").val().replace("&", "%26")));
            }

            /* stop form from submitting normally */
            event.preventDefault();

                loading(true, 'Please wait ...');

                //---
                var params = {
                    UserName: $("#UserName").val(),
                    Password: $("#Password").val()
                };

                var obj = JSON.stringify(params);

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/DoLogin",
                    async: false,
                    crossDomain: true,
                    data: obj,
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        loading(false);
                        if ($("#chkRememberUser").prop("checked")) {
                            $.cookie("userNameBookingcookie", $("#UserName").val(), { expires: 10000 });
                            $.cookie("passwordBookingcookie", $("#Password").val(), { expires: 10000 });
                        }
                        if (response != null) {
                            //var ResponseDTO = new ResponseModel();
                            //ResponseDTO.MapEntity(response, ResponseDTO);

                            var ResponseDTO = JSON.parse(response);
                        }


                        if (ResponseDTO.Status === "-1") {
                            $("#lblLoginValidateMsg").text(ResponseDTO.Message);
                        }


                        if (ResponseDTO.Status === "0") {
                            $("#lblLoginValidateMsg").text(ResponseDTO.Message);
                                                                                   
                            $("#change_pass_form").hide();
                            
                            if (ResponseDTO.UserName != null && ResponseDTO.UserName != '') {
                                $("#UserNameDivSection").show();
                                $("#lblUserName").text(ResponseDTO.UserName);
                                $("#lblUserNameInLogin").text('User: ' + ResponseDTO.UserName);
                                if (ResponseDTO.UserName_Email != null && ResponseDTO.UserName_Email != '') {
                                    $("#lblUserIdInLogin").text('(' + ResponseDTO.UserName_Email + ')');
                                }
                                
                            } else {
                                $("#UserNameDivSection").hide();
                            }
                            $("#lblLoginButtonText").text('Logout')
                            $("#SignOutSection").show();
                            $("#login-nav").hide();                            
                            $("#login-dp").dropdown('toggle');

                        }

                        if (ResponseDTO.Status === "1") {
                            $("#lblLoginValidateMsg").text(ResponseDTO.Message);
                            //go to change password........
                            $("#Change_CurrentPass").val('');
                            $("#Change_NewPass").val('');
                            $("#Change_ConfirmPass").val('');
                            $("#login-nav").hide();
                            $("#change_pass_form").show(300);
                        }

                        //MasterObj.isLogin(false);
                    },
                    error: function (errorResponse) {
                        //MasterObj.appData.ProcessEnd();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        //MasterObj.ShowErrorList(errorMsg);
                        bReturn = false;
                    }
                });
            
        });

        $("#change_pass_form").submit(function (event) {

            if ($("#Change_NewPass").val() !== $("#Change_ConfirmPass").val()) {
                $("#lblChangePassValidateMsg").text("Your new and confirmation passwords do not match, please check");
                return;
            }

            /* stop form from submitting normally */
            event.preventDefault();

            loading(true, 'Please wait ...');

            //---
            var params = {
                UserName: $("#UserName").val(),
                Password: $("#Change_CurrentPass").val(),
                NewPassword: $("#Change_NewPass").val()
            };

            var obj = JSON.stringify(params);

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/ChangePassword",
                async: false,
                crossDomain: true,
                data: obj,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    loading(false);                    
                    if (response != null) {                       
                        var ResponseDTO = JSON.parse(response);
                    }


                    if (ResponseDTO.Status === "-1") {
                        $("#lblLoginValidateMsg").text(ResponseDTO.Message);
                    }


                    if (ResponseDTO.Status === "0") {
                        $("#lblLoginValidateMsg").text(ResponseDTO.Message);
                        $("#Password").val($("#Change_NewPass").val());
                        $("#login-nav").trigger("submit");

                    }                   
                },
                error: function (errorResponse) {
                    //MasterObj.appData.ProcessEnd();
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    //MasterObj.ShowErrorList(errorMsg);
                    bReturn = false;
                }
            });
           
        });

        $("#btnCancelChangePass").click(function () {
            $("#login-nav").show(300);
            $("#change_pass_form").hide();
            $("#lblLoginValidateMsg").text('');            
        });

        $("#btnSignOut").click(function () {
           
            loading(true, 'Please wait ...');

            //---
            //var params = {
            //    UserName: $("#UserName").val(),
            //    Password: $("#Password").val()
            //};

            //var obj = JSON.stringify(params);

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SignOut",
                async: false,
                crossDomain: true,
                //data: obj,
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    loading(false);                    
                    if (response != null) {                        
                        var ResponseDTO = JSON.parse(response);
                    }

                    if (ResponseDTO.Status === "0") {
                        //$("#lblLoginValidateMsg").text('');                      
                        //$("#change_pass_form").hide();
                        //$("#UserNameDivSection").hide();
                        //$("#lblUserName").text('');
                        //$("#lblUserNameInLogin").text('');
                        //$("#lblUserIdInLogin").text('');

                        //if ($.cookie("userNameBookingcookie") != null && $.cookie("userNameBookingcookie") != '') {
                        //    $("#UserName").val($.cookie("userNameBookingcookie"));
                        //    $("#Password").val($.cookie("passwordBookingcookie"));
                        //} else {
                        //    $("#UserName").val('');
                        //    $("#Password").val('');
                        //}
                        
                        //$("#SignOutSection").hide();
                        //$("#login-nav").show(300);
                        //$("#login-dp").dropdown('toggle');
                        location.reload();
                    }
                  
                },
                error: function (errorResponse) {
                    //MasterObj.appData.ProcessEnd();
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    //MasterObj.ShowErrorList(errorMsg);
                    bReturn = false;
                }
            });

        });      
   
    }           
);

function CheckUserLogged() {
    //-- User Name
    if ($("#hdf_UserName").val() != null && $("#hdf_UserName").val() != '') {
        $("#UserNameDivSection").show();
        $("#lblUserName").text($("#hdf_UserName").val());

        //-- show signout section
        $("#change_pass_form").hide();
        if ($("#hdf_UserName").val() != null && $("#hdf_UserName").val() != '') {
            $("#UserNameDivSection").show();
            $("#lblUserName").text($("#hdf_UserName").val());
            $("#lblUserNameInLogin").text('User: ' + $("#hdf_UserName").val());
            if ($("#hdf_UserName_Email").val() != null && $("#hdf_UserName_Email").val() != '') {
                $("#lblUserIdInLogin").text('(' + $("#hdf_UserName_Email").val() + ')');
            }

        } else {
            $("#UserNameDivSection").hide();
        }

        $("#lblLoginButtonText").text('Logout')
        $("#SignOutSection").show();
        $("#login-nav").hide();
        //--
    } else {
        $("#UserNameDivSection").hide();
        $("#lblUserName").text('');
    }

    //--
}

function adaptHeaderNavBar() {
    var max = -1;
    $("ul.nav.navbar-nav li").each(function () {
        if (!$(this).hasClass("lastitemmenu")) {
            var h = $(this).height();
            max = h > max ? h : max;
        }        
    });  

    $("ul.nav.navbar-nav li a").each(function () {     
        $(this).css("height", max + 5);
    });    
}

//********************Languages Change **************//
function changelang(lang) {
    var cookieValue = $.cookie("be-lang");
    var cookieValueC = cookieValue;
    if ((cookieValue == null) || (cookieValue == '') || (typeof cookieValue == 'undefined')) {
        createCookie("be-lang", lang, 365)
        
    } else {
        createCookie("be-lang", lang, 365)
    }
    cookieValue = $.cookie("be-lang");
    if (cookieValue == "es-CR") {
        $("#lang-ENUS").text("Ingles");
        $("#lang-ESCR").text("Español");
        $("#lang-ENUS").css("font-weight", "normal");
        $("#lang-ESCR").css("font-weight", "bold");
    } else {
        $("#lang-ENUS").text("English");
        $("#lang-ESCR").text("Spanish");
        $("#lang-ENUS").css("font-weight", "bold");
        $("#lang-ESCR").css("font-weight", "normal");
    }
    if (typeof lang != 'undefined') {        
        if (cookieValueC != lang)
            location.reload();
    }
}

function getlang(lang) {
    var cookieValue = $.cookie("be-lang");
    if ((cookieValue == null) || (cookieValue == '') || (cookieValue== 'undefined')) {
        createCookie("be-lang", lang, 365)
        cookieValue = $.cookie("be-lang");
    } 
    if (cookieValue == "es-CR") {
        $("#lang-ENUS").text("Ingles");
        $("#lang-ESCR").text("Español");
        $("#lang-ENUS").css("font-weight", "normal");
        $("#lang-ESCR").css("font-weight", "bold");
        
    } else {
        $("#lang-ENUS").text("English");
        $("#lang-ESCR").text("Spanish");
        $("#lang-ENUS").css("font-weight", "bold");
        $("#lang-ESCR").css("font-weight", "normal");
    }
    
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function LoadCompany() {
    var hotelcode = getParameterByName("hotelcode");
    $.ajax({
        type: "GET",
        url: $("#txtTrackingURLBase").val() + "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + hotelcode + "&sType=H",
        async: false,
        crossDomain: true,
        contentType: 'application/json',
        success: function (response) {
            if (response != null) {

                if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0)) {
                  
                    var ppPinterest = response.GetCompanyResult["0"].ppPinterest
                    var ppFacebook = response.GetCompanyResult["0"].ppFacebookURL
                    var pptwitterText = response.GetCompanyResult["0"].ppTwitterText
                    var ppGooglePlus = response.GetCompanyResult["0"].ppGooglePlus
                    var ppBlog = response.GetCompanyResult["0"].ppBlog
                    var ppInstagram = response.GetCompanyResult["0"].ppInstagram


                    $("#pint-link").attr("href", ppPinterest);
                    $("#face-link").attr("href", ppFacebook);
                    $("#twi-link").attr("href",pptwitterText);
                    $("#gp-link").attr("href", ppGooglePlus);
                    $("#blog-link").attr("href", ppBlog);
                    $("#insta-link").attr("href", ppInstagram);

                  
                }
                //$.each(response, function (key, value) {
                //var CompanyInfo = new CompanyModel();
                //CompanyInfo.MapEntity(response, CompanyInfo);
                //self.CompanyInfoList.push(CompanyInfo);
                //})
            }
        }
    })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}