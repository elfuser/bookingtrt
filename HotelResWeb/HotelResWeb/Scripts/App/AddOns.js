﻿function AddOnsVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();
    self.getLogoAddOn = ko.observable();
    self.getLogo = ko.observable();
    
    self.DateFrom = ko.observable();
    self.DateTo = ko.observable();

    self.AddOnTypeList = ko.observableArray();

    self.AddOnsList = ko.observableArray();

    self.AddOnsListOther = ko.observableArray();


    self.AddonLite = ko.observable(new AddOnLite());

    self.AddedAddonList = ko.observableArray();

    self.TempAddedAddonList = ko.observableArray();

    self.InsertedAddonList = ko.observableArray();

    self.AddedRoomList = ko.observableArray();

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();


    //**********************PROGRESSBAR METHODS START*********************//
    var ProgressbarCount = 0;

    self.ProgressbarShow = function () {
        if (ProgressbarCount == 0) {
            $("#progressbar").show();
            $("#ProgressbarContainer").show();

            ProgressbarCount++;
        }
        else {
            ProgressbarCount++;
        }
    }

    self.ProgressbarHide = function () {

        ProgressbarCount--;
        if (ProgressbarCount < 0) {
            ProgressbarCount = 0;
        }
        if (ProgressbarCount == 0) {

            $("#progressbar").hide();
            $("#ProgressbarContainer").hide();
        }
        else {

            ProgressbarCount--;
        }
    }

    //**********************PROGRESSBAR METHODS END*********************//

    //*******************DEFAULT VALUES START*****************//
    tempurl = $("#urlhidden").val();
    self.RootlVm(mainVm);
    serviceUrl = tempurl;

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        $("#DivErrorList").show();
    }

    self.GetAddOnTypes = function (DateFrom, DateTo) {

        self.AddOnTypeList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/AddonServices.svc" + "/json/GetAddonQtys?companyCode=" + self.CompanyCode() + "&DateFrom=" + DateFrom + '&DateTo=' + DateTo,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetAddonQtysResult, function (key, value) {
                        var AddOnDTO = new AddOnsModel();
                        AddOnDTO.MapEntity(value, AddOnDTO);

                        if (AddOnDTO.ppAddonType() !== "O")
                        {
                            AddOnDTO.ImageUlr($("#txtTrackingURLBase").val() + '/Images/addon/' + AddOnDTO.ppAddonType() + '.png');

                            AddOnDTO.ImageUrlON($("#txtTrackingURLBase").val() + '/Images/addon/' + AddOnDTO.ppAddonType() + '_ON.png' );
                            self.AddOnTypeList.push(AddOnDTO);                            
                        }
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetAddOns = function (Category) {

        self.AddOnsList.removeAll();


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/AddonServices.svc" + "/json/GetAddonsByCategory?companyCode=" +
                self.CompanyCode() + "&DateFrom=" +  self.DateFrom() + '&DateTo=' +  self.DateTo() + '&culture=en-US&Category=' + Category,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetAddonsByCategoryResult, function (key, value) {

                        var Amount = '';

                        var AddOnDTO = new AddOnsModel();
                        AddOnDTO.MapEntity(value, AddOnDTO);

                        if (AddOnDTO.ppAmount() !== null)
                        {
                            Amount = AddOnDTO.ppAmount();
                            AddOnDTO.DescPrice("Price $ " + Amount);
                        }

                        AddOnDTO.ImageUlr($("#txtTrackingURLBase").val() + AddOnDTO.ImageUlr());

                        self.AddOnsList.push(AddOnDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetAddOnsOthers = function () {

        self.AddOnsListOther.removeAll();


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/AddonServices.svc" + "/json/GetAddonsByCategory?companyCode=" +
                self.CompanyCode() + "&DateFrom=" + self.DateFrom() + '&DateTo=' + self.DateTo() + '&culture=en-US&Category=O' ,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetAddonsByCategoryResult, function (key, value) {

                        var Amount = '';

                        var AddOnDTO = new AddOnsModel();
                        AddOnDTO.MapEntity(value, AddOnDTO);

                        if (AddOnDTO.ppAmount() !== null) {
                            Amount = AddOnDTO.ppAmount();
                            AddOnDTO.DescPrice("Price $ " + Amount);
                        }

                        AddOnDTO.ImageUlr($("#txtTrackingURLBase").val() + AddOnDTO.ImageUlr());

                        self.AddOnsListOther.push(AddOnDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.BtnAddOnType_click = function (data) {

        var filteredType;

        filteredType = Enumerable.From(self.AddOnTypeList()).Where(function (x) { return x.ppDescription().toString().toUpperCase().match("^" + data.toUpperCase()) }).ToArray();

        if ((filteredType !== null) && (filteredType !== undefined) && (filteredType.length > 0))
        {
            try{
                loading(true);

                var img = filteredType[0].ImageUrlON();

                var id = "#" + data;

                $(id).attr('src', img);

                $("#AddOnDetail").show(150);
                self.GetAddOns(filteredType[0].ppAddonType());

                loading(false);
            }
            catch (ex) {
                loading(false);
            }
        }

    }   

    self.PrepareDtoForInsert = function (Code, Price, Desc) {

        self.AddonLite = ko.observable(new AddOnLite());

        self.AddonLite().Code(Code);
        self.AddonLite().Price(Price);
        self.AddonLite().Desc(Desc);

    }

    self.BookAddon = function (data) {

        var Code = data.ppAddonID();
        var Price = data.ppAmount();
        var Desc = data.ppDescription();

        self.SaveAddon(Code, Price, Desc, null);

        //-- Alert add-on added
        alertDialog("Add-on '" + Desc + "' added to your booking.");

        //-- Add add-on to table
        var imageAddOnStatusURL = $("#txtTrackingURLBase").val() + "/Images/success.png";
        var newId = s4() + s4();

        var newAddOnRow = $("<tr id='" + newId + "' class='addon_table_row' data-target=" + Code + "><td class='addon_summary_item_desc'>" + Desc + "</td><td class='addon_summary_item_price'>$" + Price +
                            "</td><td class='addon_button'><img class='addon_button_image' src='" + imageAddOnStatusURL + "'</td></tr>");
        
        $("#AddOnsTableInSummary").find('tbody').append(newAddOnRow);        
        $('#AddOnsTableInSummary tr:last').hide();
        $('#AddOnsTableInSummary tr:last').show(750);
        //--

        //-- MouseOver and MouseOut of add-on recently added 
        $(".addon_table_row").mouseover(function () {            
            var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
            currentURL = currentURL.replace("success.png", "delete.png")
            $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
        });

        $(".addon_table_row").mouseout(function () {            
            var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
            currentURL = currentURL.replace("delete.png", "success.png")
            $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
        });
        //--

        //-- Delete add-on from list
        $(".addon_button_image").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var UniqueIdAddOnToDelete = $(this).closest('tr').attr("id")
            $(this).closest('tr')
            .children('td')
            .animate({ padding: 0 })
            .wrapInner('<div />')
            .children()
            .slideUp(function () { $(this).closest('tr').remove(); });


            //-- Delete add-on and insert the new add-on list to the cookie
            deleteAddonAndUpdateAddOnsCookie(UniqueIdAddOnToDelete);
            //--
        });
        //--

        self.PrepareDtoForInsert(Code, Price, Desc);
        self.TempAddedAddonList.push(self.AddonLite());

        //-- Reload Summary Info with new addon
        LoadSummaryInfo(false);                   
    }


    self.SaveAddon = function (Code, Price, Desc, AddOnListWithData) {

        var bReturn;
        var newAddons

        if (AddOnListWithData == null) {
            self.PrepareDtoForInsert(Code, Price, Desc);
            //var newAddon = JSON.stringify(ko.toJS(self.AddonLite()));
            self.InsertedAddonList.push(self.AddonLite());
            newAddons = JSON.stringify(ko.toJS(self.InsertedAddonList()));
        } else {
            newAddons = JSON.stringify(ko.toJS(AddOnListWithData));
        }        

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationAddons",
            async: false,
            crossDomain: true,
            data: newAddons,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                self.appData.ProcessEnd();
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    self.GetSavedAddons = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/RoomsAvailability/GetSavedAddons",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var AddonInfo = new AddOnLite();
                        AddonInfo.MapEntity(value, AddonInfo);

                        AddonInfo.PriceInText('$' + AddonInfo.Price());
                        AddonInfo.UniqueId(s4() + s4());

                        self.AddedAddonList.push(AddonInfo);
                        self.TempAddedAddonList.push(AddonInfo); //Temp added addon list
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedRooms = function () {

        self.AddedRoomList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedRooms",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var RommsInfo = new RoomLiteModel();
                        RommsInfo.MapEntity(value, RommsInfo);

                        if (RommsInfo.Discount() > 0) {
                            RommsInfo.PriceWithDiscLabel('$' + parseFloat(RommsInfo.PriceDisc()).toFixed(2));
                            RommsInfo.Tachclass("Discount_item_summary");
                        }

                        RommsInfo.PriceLabel('$' + parseFloat(RommsInfo.Price()).toFixed(2));

                        self.AddedRoomList.push(RommsInfo);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetPromoCodeInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var Info = new PromoCodeModel();
                        Info.MapEntity(ds, Info);
                        self.PromoCodeDTO(Info);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, RateCode) {
        //self.AddOnTypeList.removeAll();

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmount?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&sRateCode=" + RateCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountResult != null) {
                        $.each(response.GetPromoCodeAmountResult, function (key, value) {
                            var objDTO = new PromoCodeModel();
                            objDTO.MapEntity(value, objDTO);

                            //PriceDisc = PricesDTO.ppPrices();
                            //Discount = PricesDTO.Discount();
                            objDTO.ppPromoCode(PromoCode);
                            self.PromoCodeDTO(objDTO)
                            bResult = true;
                            //self.PricesList.push(PricesDTO);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    self.SaveNextPage = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveCurrentPage?CurrentPage=Checkout",
            async: false,
            crossDomain: true,
            success: function (response) {
                return true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                return false;
            }
        });

        return true;
    }

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=H",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;

        self.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));

        var NewObj = JSON.stringify(ko.toJS(self.PromoCodeDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SavePromoCodeInfo",
            async: false,
            crossDomain: true,
            data: NewObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    PayNow_Button = function (data, event) {       

        try{
            loading(true, 'Please wait ...');
       
            self.SaveNextPage();
            var url = $("#txtTrackingURLBase").val() + '/Checkout/Checkout?hotelcode=' + $.trim(self.CompanyCode());
            window.location.href = url;       

            loading(false);
        }
        catch (ex) {
            loading(false);
        }
    }

    //-- Start Over button
    self.StartOver = function () {
        deleteCookie("RoomInfo");
        deleteCookie("CurrentProgress");
        deleteCookie("AddonInfo");
        deleteCookie("ReservationInfo");

        var url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim(self.CompanyCode())

        // uncomment for demo
        //url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim('BDIHS')
        window.location.href = url;        
    }

    self.InitModel = function () {

        //-- Verify rooms cookie info
        self.GetSavedRooms();
        
        var CompanyCode = "";
        var bLoadRoomsCookie = false;
        var roomcode, dtFrom, dtTo;

        if (self.AddedRoomList() != null) {
            if (self.AddedRoomList().length > 0) {
                $.each(self.AddedRoomList(), function (i, item) {
                    if (item.CompanyId() != null) {
                        if (item.CompanyId() != "") {
                            CompanyCode = item.CompanyId();
                            roomcode = item.Code();
                            dtFrom = item.fromDt();
                            dtTo = item.toDt();                                                       

                            return false;
                        }
                    }
                });
            }
        }
        //--      


        if (CompanyCode == "") {
            CompanyCode = $("#hdf_HotelCodeInAddOns").val();
            if (CompanyCode == "") {
                alertDialog("No company provided");
                return false;
            } else {
                if (dtFrom == null) {
                    dtFrom = $("#hdf_DateFromInAddOns").val();
                } else {
                    if (dtFrom == "") {
                        dtFrom = $("#hdf_DateFromInAddOns").val();
                    }
                }
                
                if (dtTo == null) {
                    dtTo = $("#hdf_DateToInAddOns").val();
                } else {
                    if (dtTo == "") {
                        dtTo = $("#hdf_DateToInAddOns").val();
                    }
                }               
            }
        }

        self.CompanyCode(CompanyCode);
        self.DateFrom(dtFrom);
        self.DateTo(dtTo);

        //--commented for demo
        //self.CompanyCode("VC0213");
        //self.DateFrom("2013-02-16");
        //self.DateTo("2014-12-18");
        //--

        self.getLogoAddOn($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=3");

        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=4");
        
        self.GetAddOnTypes(self.DateTo(), self.DateFrom());
        self.GetAddOnsOthers();

        // Check if the hotel has other promotions to show section or not
        if (self.AddOnsListOther() != null){
            if (self.AddOnsListOther().length > 0) {
                $("#PromotionsDiv").show();
            } else {
                $("#PromotionsDiv").hide();
            }
        } else {
            $("#PromotionsDiv").hide();
        }

        self.GetSavedAddons();        
        self.GetPromoCodeInfo();
    }



    self.MouseOver = function (data) {

        var filteredType;

        filteredType = Enumerable.From(self.AddOnTypeList()).Where(function (x) { return x.ppDescription().toString().toUpperCase().match("^" + data.toUpperCase()) }).ToArray();

        if ((filteredType !== null) && (filteredType !== undefined) && (filteredType.length > 0)) {
     
            var img = filteredType[0].ImageUrlON();

            var id = "#" + data;

            $(id).attr('src', img);

        }



    }

    self.MouseOut = function (data) {

        var filteredType;

        filteredType = Enumerable.From(self.AddOnTypeList()).Where(function (x) { return x.ppDescription().toString().toUpperCase().match("^" + data.toUpperCase()) }).ToArray();

        if ((filteredType !== null) && (filteredType !== undefined) && (filteredType.length > 0)) {

            var img = filteredType[0].ImageUlr();

            var id = "#" + data;

            $(id).attr('src', img);

        }

    }

    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var addonObj;
var slider;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               //$("#dialog").dialog({
               //    autoOpen: false,
               //    modal: true,
               //    buttons: {
               //        Ok: function () {
               //            $(this).dialog("close");
               //        }
               //    }
               //});

               try{
                   loading(true, 'Please wait ...');

                   addonObj = new AddOnsVm();
                   ko.applyBindings(addonObj);

                   var urlCompanyLogo = addonObj.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);                   


                   $(".Type_Button").click(function () {                    
                       addonObj.BtnAddOnType_click(this.title);
                   });

                   slider =  $('.slider2').bxSlider({
                       slideWidth: 300,
                       minSlides: 2,
                       maxSlides: 2
                       //slideMargin: 50                   
                   });


                   $("#btnPromoCode").click(function () {

                       var bReturn;
                       $("#PromoCodeMessageSection").hide();

                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "") {
                               $("#lblPromoCodeMessage").text("Enter a valid promotional code");
                               $("#PromoCodeMessageSection").show(300);
                               //alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {

                               loading(true, 'Please wait ...');

                               addonObj.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((addonObj.PromoList().length === 0) || (addonObj.PromoList() === null) || (addonObj.PromoList() === undefined)) {
                                   $("#lblPromoCodeMessage").text("Invalid promotional code");
                                   $("#PromoCodeMessageSection").show(300);
                                   //alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(addonObj.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(addonObj.PromoList()[0].ppEndDate(), '/'));                                  
                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       //alertDialog('Invalid promotional code');
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else if (CurrentDate > DateTo) {
                                       //alertDialog('Promotional code has expired');
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else {
                                       if (addonObj.AddedRoomList() != null) {
                                           if (addonObj.AddedRoomList().length > 0) {
                                               $.each(addonObj.AddedRoomList(), function (i, item) {
                                                   addonObj.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.RateCode())
                                                   if (addonObj.PromoCodeDTO() != null) {
                                                       if (addonObj.PromoCodeDTO().ppAmount() > 0) {                                                          
                                                           //-- Load Summary info
                                                           LoadSummaryInfo(false);                                                          
                                                       }
                                                   }
                                               });

                                               //--Save promo code info in cookie
                                               addonObj.SavePromoCodeCookie();
                                               //--

                                               //alertDialog('Promotional code was applied');
                                               $("#lblPromoCodeMessage").text("Promotional code was applied");
                                               $("#lblPromoCodeMessage").css('color', '#65ca65');
                                               $("#PromoCodeMessageSection").show(300);

                                               $("#lblPromoCodeInHeader").text($("#txtPromotionalCode").val());
                                               $("#PromoCodeInHeaderSection").show(300);

                                               setTimeout(function () {
                                                   $('#dialogPromoCodePopup').modal("hide");
                                               }, 2500);
                                               
                                           }
                                       }
                                   }
                               }

                               loading(false);
                           }

                           catch (ex) {
                               alertDialog(ex);
                               loading(false);
                           }
                       } else {
                           try {

                               loading(true, 'Please wait ...');

                               $("#txtPromotionalCode").val('');
                               $('#txtPromotionalCode').prop('readonly', false);
                               $("#btnPromoCode").html('Validate');
                               $("#PromoApprovedIcon").hide();
                               $("#PromoCodeInHeaderSection").hide();
                               $('#dialogPromoCodePopup').modal("hide");

                               $(".SummaryPromoCode").hide();
                               addonObj.PromoCodeDTO(null);

                               //-- Load Summary info
                               LoadSummaryInfo(false);

                               deleteCookie("PromoCodeInfo");

                               loading(false);
                           }                          

                           catch (ex) {
                               alertDialog(ex);
                               loading(false);
                           }
                       }

                   });

                   //-- Match media - responsive                                 
                   if (matchMedia) {
                       var mq = window.matchMedia("(min-width: 560px)");
                       mq.addListener(ChangeSliderSettings);
                       ChangeSliderSettings(mq, false);
                   }
                   //--                   

                   //-- Load Summary info
                   LoadSummaryInfo(true);                                    

                   loading(false);
               }
               catch (ex) {
                   alertDialog("Error loading add-ons and summary information.");
                   loading(false);
               }

               $(".Type_Button").mouseover(function () {
                   addonObj.MouseOver(this.title);
               });

               $(".Type_Button").mouseout(function () {
                   addonObj.MouseOut(this.title);
               });

               //-- MouseOver and MouseOut of add-ons added
               $(".addon_table_row").mouseover(function () {                   
                   var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
                   currentURL = currentURL.replace("success.png", "delete.png")
                   $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
               });

               $(".addon_table_row").mouseout(function () {                   
                   var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
                   currentURL = currentURL.replace("delete.png", "success.png")
                   $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
               });
               //--

               //-- Delete add-on from list
               $(".addon_button_image").click(function (e) {
                   e.preventDefault();
                   e.stopPropagation();
                   var UniqueIdAddOnToDelete = $(this).closest('tr').attr("id")
                   $(this).closest('tr')
                   .children('td')
                   .animate({ padding: 0 })
                   .wrapInner('<div />')
                   .children()
                   .slideUp(function () { $(this).closest('tr').remove(); });


                   //-- Delete add-on and insert the new add-on list to the cookie
                   deleteAddonAndUpdateAddOnsCookie(UniqueIdAddOnToDelete);
                   //--

               });
               //

                   
                   
           }
);
//*************************************//

// media query change
function ChangeSliderSettings(mq, reloadSlider) {
    if (mq.matches) {
        //more
        if ((reloadSlider) || (reloadSlider == null)) {
            slider.reloadSlider({
                slideWidth: 300,
                minSlides: 2,
                maxSlides: 2
            });
        }
    }
    else {
        //less             
        slider.reloadSlider({
            slideWidth: 300,
            minSlides: 1,
            maxSlides: 1
        });
   }    
}

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    //$("#dialog").show();
    //$("#dialog").dialog("open");

    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

//-- Calculate info of summary section
function LoadSummaryInfo(InsertCurrentAddOnsInCookie) {

    if (addonObj.AddedRoomList() != null) //If room list is not empty
    {
        if (addonObj.AddedRoomList().length > 0) {
            var SubTotalRoomNights = 0;
            var SubTotalRoomNightsWithDiscount = 0;
            var tax = 0;
            var TOTAL = 0;
            var Totaltax = 0;
            var oneDay = 24 * 60 * 60 * 1000;
            var applyPromoCode = false;

            //-- Number of nights
            var fromDate = new Date(addonObj.AddedRoomList()[0].fromDt().replace(/-/g, "/"));
            var toDate = new Date(addonObj.AddedRoomList()[0].toDt().replace(/-/g, "/"));
            var diffDays = Math.round(Math.abs((fromDate.getTime() - toDate.getTime()) / (oneDay)));
            //--
            
            var guestsQty = 0;
            $.each(addonObj.AddedRoomList(), function (i, item) {
                //-- Quantity of guests
                guestsQty += (parseInt(addonObj.AddedRoomList()[i].Adults()) +
                           parseInt(addonObj.AddedRoomList()[i].Kids()) +
                           parseInt(addonObj.AddedRoomList()[i].Infants()));

                //-- Sum room price
                SubTotalRoomNights += (parseFloat(addonObj.AddedRoomList()[i].Price()) * diffDays);
                SubTotalRoomNightsWithDiscount += (parseFloat(addonObj.AddedRoomList()[i].PriceDisc()) * diffDays);
            })            
            //--

            //-- Put info in summary labels
            var fromDateLabel = formatDateToShowLabel(fromDate);
            var toDateLabel = formatDateToShowLabel(toDate);

            $("#lbArrivalDate").html(fromDateLabel);
            $("#lbDepartureDate").html(toDateLabel);
            $("#lbRoomNights").html(diffDays);
            $("#lbRoomsQty").html(addonObj.AddedRoomList().length);

            //$("#lbGuestsQty").html(guestsQty);
            //$("#lbRateSelected").html('$' + addonObj.AddedRoomList()[0].Price());
            //SubTotalRoomNights = '$' + (parseFloat(addonObj.AddedRoomList()[0].Price()) * diffDays);
            //$("#lbRoomTypeDesc").html(addonObj.AddedRoomList()[0].Desc());

            $("#lbSubTotalRoomNights").html('$' + parseFloat(SubTotalRoomNights).toFixed(2));
            //--

            //Price with discount            
            //if (addonObj.AddedRoomList()[0].Discount() > 0) 
            //{
            //    $("#lbRateSelectedDisc").html('$' + parseFloat(addonObj.AddedRoomList()[0].PriceDisc()).toFixed(2));
            //    $("#lbRateSelected").addClass("Discount_item_summary");
            //}
            //else {
            //    $("#divDiscount").hide();
            //}

            //-- Take the discount of the first room, because it is the same discount for all the rooms (same room type)
            $("#lbDiscount").html(addonObj.AddedRoomList()[0].Discount() + ' %');
            Discount = SubTotalRoomNights * (addonObj.AddedRoomList()[0].Discount() / 100);
            //

            //-- Take the tax of the first room, because it is the same tax for all the rooms
            tax = addonObj.AddedRoomList()[0].TaxPerc(); //Tax                       

            //-- Addons
            var SubTotalAddOns = 0;
            if (addonObj.TempAddedAddonList != null) {
                if (addonObj.TempAddedAddonList().length > 0) {
                    $.each(addonObj.TempAddedAddonList(), function (i, item) {
                        SubTotalAddOns += parseFloat(item.Price());
                        if ((InsertCurrentAddOnsInCookie != null) && (InsertCurrentAddOnsInCookie)){
                            addonObj.SaveAddon(item.Code(), item.Price(), item.Desc(), null);
                        }
                    })

                }
            }

            $("#lbSubTotalAddOns").html('$' + SubTotalAddOns);
            //--
            
            //-- Totals
            //if (addonObj.AddedRoomList()[0].Discount() > 0) {
            //    TOTAL = ((parseFloat(addonObj.AddedRoomList()[0].PriceDisc()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2);
            //}
            //else { TOTAL = ((parseFloat(addonObj.AddedRoomList()[0].Price()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2); }


            //-- Verify Promo code info
            if (addonObj.PromoCodeDTO() != null) {
                if (addonObj.PromoCodeDTO().ppPromoCode() != null && addonObj.PromoCodeDTO().ppPromoCode() != '') {
                    applyPromoCode = true;

                    $("#txtPromotionalCode").val(addonObj.PromoCodeDTO().ppPromoCode());
                    $('#txtPromotionalCode').prop('readonly', true);
                    $("#btnPromoCode").html('Revert');
                    $("#PromoApprovedIcon").show(300);

                    $("#lblPromoCodeInHeader").text(addonObj.PromoCodeDTO().ppPromoCode());
                    $("#PromoCodeInHeaderSection").show(300);

                    $("#lbPromoCodeSummary").text(addonObj.PromoCodeDTO().ppPromoCode());                    

                    if (addonObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        $("#lbPromoAmountSummary").text("- " + addonObj.PromoCodeDTO().ppAmount() + '%');
                    } else {
                        $("#lbPromoAmountSummary").text('- $' + addonObj.PromoCodeDTO().ppAmount());
                    }

                    $(".SummaryPromoCode").show();

                } else {
                    $(".SummaryPromoCode").hide();
                }
            } else {
                $(".SummaryPromoCode").hide();
            }
            //--

            if (addonObj.AddedRoomList()[0].Discount() > 0) {
                TOTAL = (SubTotalRoomNightsWithDiscount + parseFloat(SubTotalAddOns)).toFixed(2);
            }
            else { TOTAL = (SubTotalRoomNights + parseFloat(SubTotalAddOns)).toFixed(2); }

            if (tax > 0) {                
                Totaltax = parseFloat(TOTAL * (tax / 100));
                TOTAL = parseFloat(TOTAL) + Totaltax;
            }

            //-- Check if we need to apply promo code discount
            if (applyPromoCode) {
                if (addonObj.PromoCodeDTO().ppAmount() > 0) {
                    if (addonObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        TOTAL = TOTAL - (TOTAL * (addonObj.PromoCodeDTO().ppAmount() / 100));
                    } else {
                        if (TOTAL > addonObj.PromoCodeDTO().ppAmount()) {
                            TOTAL = TOTAL - addonObj.PromoCodeDTO().ppAmount();
                        } else {
                            TOTAL = 0;
                        }
                    }
                }
            }
            //--

            //$("#totalAmount").val(parseFloat(TOTAL));
            //$("#discount").val(parseFloat(Discount));
            //$("#addonAmount").val(parseFloat(SubTotalAddOns));
            //$("#taxAmount").val(parseFloat(Totaltax));

            $("#lbTOTAL").html('$' + parseFloat(TOTAL).toFixed(2)); //TOTAL label
            $("#lbTaxes").html('$' + parseFloat(Totaltax).toFixed(2)); //Tax label
            //--
        }
    }

}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var day = '' + date.getDate();
    var year = date.getFullYear();

    if (day.length < 2) day = '0' + day;

    var monthname = months[date.getMonth()];

    returnedvalue = day + '-' + monthname + '-' + year;

    return returnedvalue;
}

function deleteAddonAndUpdateAddOnsCookie(UniqueIdAddOnToDelete) {
    var AddOnListInTable = [];
    addonObj.TempAddedAddonList([]);
    $("#AddOnsTableInSummary").find('tr').each(function (i, el) {
        var Id = $(this).attr("id");

        if (UniqueIdAddOnToDelete != Id) {
            var Code = $(this).attr("data-target");
            var Desc = $(this).find(".addon_summary_item_desc").html();
            var Price = $(this).find(".addon_summary_item_price").html();
            var PriceNumber;

            Price = $.trim(Price.replace("$", ""));
            if ($.isNumeric(Price)) {
                PriceNumber = parseFloat(Price);
            }
            addonObj.PrepareDtoForInsert(Code, PriceNumber, Desc);
            AddOnListInTable.push(addonObj.AddonLite());

            addonObj.TempAddedAddonList.push(addonObj.AddonLite());
        }
    });

    if (AddOnListInTable != null) {        
        addonObj.SaveAddon(null, null, null, AddOnListInTable); //Store add-on list updated in cookie       
    }

    LoadSummaryInfo(false);
}

//Delete Cookie
function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--