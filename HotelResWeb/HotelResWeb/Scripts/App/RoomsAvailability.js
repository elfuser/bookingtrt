﻿function RoomsAvailabilityVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();

    self.getLogo = ko.observable();

    self.appData = mainVm;

    self.RoomList = ko.observableArray();
    self.CompanyCode = ko.observable();
    self.CompanyName = ko.observable();

    self.ppEmail = ko.observable();
    self.ppAddress = ko.observable();
    self.ppWeb = ko.observable();
    self.ppTel1 = ko.observable();
    self.Tax = ko.observable();

    self.Processor = ko.observable()

    self.test = ko.observable("test");


    self.AvailableList = ko.observableArray();
    self.PricesList = ko.observableArray();


    self.Room = ko.observable(new RoomLiteModel());
    self.AddedRoomList = ko.observableArray();

    self.InsertedRoomList = ko.observableArray();

    self.ReservationDTO = ko.observable(new ReservationModel());

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();
    


    //**********************PROGRESSBAR METHODS START*********************//
    var ProgressbarCount = 0;

    self.ProgressbarShow = function () {
        if (ProgressbarCount == 0) {
            $("#progressbar").show();
            $("#ProgressbarContainer").show();

            ProgressbarCount++;
        }
        else {
            ProgressbarCount++;
        }
    }

    self.ProgressbarHide = function () {

        ProgressbarCount--;
        if (ProgressbarCount < 0) {
            ProgressbarCount = 0;
        }
        if (ProgressbarCount == 0) {

            $("#progressbar").hide();
            $("#ProgressbarContainer").hide();
        }
        else {

            ProgressbarCount--;
        }
    }

    //**********************PROGRESSBAR METHODS END*********************//

    //*******************DEFAULT VALUES START*****************//
    tempurl = $("#urlhidden").val();
    self.RootlVm(mainVm);
    serviceUrl = tempurl;

    //*******************DEFAULT VALUES END*****************//

    self.LoadCompany = function (data) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Services/CompanyServices.svc" + "/json/GetCompany?sCompany_Code=" + self.CompanyCode() + "&sType=H",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    if ((response.GetCompanyResult != null) && (response.GetCompanyResult.length > 0))
                    {
                         self.CompanyName(response.GetCompanyResult["0"].ppCompanyName);
                         self.ppEmail(response.GetCompanyResult["0"].ppEmail);
                         self.ppAddress(response.GetCompanyResult["0"].ppAddress);
                         self.ppWeb(response.GetCompanyResult["0"].ppWeb);
                         self.CompanyCode(response.GetCompanyResult["0"].ppCompanyCode);
                         self.ppTel1(response.GetCompanyResult["0"].ppTel1);
                         self.Tax(response.GetCompanyResult["0"].ppTaxPerc);

                         self.Processor(response.GetCompanyResult["0"].Processor);

                    }
                    //$.each(response, function (key, value) {
                        //var CompanyInfo = new CompanyModel();
                        //CompanyInfo.MapEntity(response, CompanyInfo);
                        //self.CompanyInfoList.push(CompanyInfo);
                    //})
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.LoadRooms = function () {

        var cookieValue = $.cookie("be-lang");

        $.ajax({
            type: "GET",            
            url: $("#txtTrackingURLBase").val() + "/Services/RoomServices.svc" + "/json/GetRoomTypes_V1?roomtype_code=&company_code=" + self.CompanyCode() + "&culture="+cookieValue,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var alignclass = '';

                    $.each(response.GetRoomTypes_V1Result, function (key, value) {
                        var RommsInfo = new RoomsModel();
                        RommsInfo.MapEntity(value, RommsInfo);                        
                        RommsInfo.ImageUlr($("#txtTrackingURLBase").val() + RommsInfo.ImageUlr());

                        if (alignclass == 'li_room_left' || alignclass == '') {
                            alignclass = 'li_room_right';
                        } else {
                            alignclass = 'li_room_left';
                        }
                        RommsInfo.Alignmentclass(alignclass);
                        self.RoomList.push(RommsInfo);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //self.GetSavedRooms = function () {

    //    $.ajax({
    //        type: "GET",
    //        url: $("#txtTrackingURLBase").val()  + "/RoomsAvailability/GetSavedRooms",
    //        async: false,
    //        crossDomain: true,
    //        contentType: 'application/json',
    //        success: function (response) {
    //            if (response != null) {

    //                var ds = jQuery.parseJSON(response);

    //                $.each(ds, function (key, value) {
    //                    var RommsInfo = new RoomLiteModel();
    //                    RommsInfo.MapEntity(value, RommsInfo);
    //                    self.AddedRoomList.push(RommsInfo);
    //                })
    //            }
    //        },
    //        error: function (errorResponse) {
    //            var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
    //            self.ShowErrorList(errorMsg);
    //        }
    //    });
    //}

    self.LoadAvaibility = function (mydate) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/QueryServices.svc" + "/json/QueryAvailabilityDay?CompanyCode=" + self.CompanyCode() + "&CurrentDate=" + mydate,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.QueryAvailabilityDayResult, function (key, value) {
                        var DayAvailableDTO = new DayAvailabilityModel();
                        DayAvailableDTO.MapEntity(value, DayAvailableDTO);

                        //-- Check if the date is already in the availablelist
                        if (self.AvailableList() != null) {
                            if (self.AvailableList().length > 0) {
                                var bItemExist = false;
                                var dateformatted = formatDate(DayAvailableDTO.ppDay(), '/', 'ddmmyyyy');
                                var filteredAvailableDates;
                                filteredAvailableDates = Enumerable.From(self.AvailableList()).Where(function (x) { return formatDate(x.ppDay(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
                                if (filteredAvailableDates != null) {
                                    if (filteredAvailableDates.length > 0) {
                                        bItemExist = true;
                                    }
                                }
                            }
                        }

                        if (!bItemExist) {
                            //-- Insert date in availablelist
                            self.AvailableList.push(DayAvailableDTO);
                        }
                        
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.LoadPrices = function (mydate,RoomType,PromoCode) {

        var PriceDisc = 0;
        var Discount = 0;

        if (PromoCode != null) {
            if (PromoCode != '') {
                if ($("#btnPromoCode").html() == 'Validate') {
                    PromoCode = '';
                }
            }
        }

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/QueryServices.svc" + "/json/QueryPrices?CompanyCode=" + self.CompanyCode() + "&CurrentDate=" + mydate + "&RoomType=" + RoomType + "&PromoCode=" + PromoCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.QueryPricesResult, function (key, value) {
                        var PricesDTO = new PricesModel();
                        PricesDTO.MapEntity(value, PricesDTO);

                        PriceDisc = PricesDTO.ppPrices();
                        Discount = PricesDTO.Discount();

                        if (Discount > 0)
                        {
                            PriceDisc = PriceDisc - (PriceDisc * (Discount / 100));
                            PricesDTO.PricesWithDiscount(PriceDisc);                                     
                        }

                        //-- Check if Promotional Code has to be applied to the prices
                        if (PromoCode != null) {
                            if (PromoCode != '') {
                                if (PricesDTO.ppPromoAmount() > 0) {
                                    var price = PriceDisc;
                                    if (price > 0) {
                                        if (PricesDTO.ppPromoType().toUpperCase() == 'P') {
                                            price = price - (price * (PricesDTO.ppPromoAmount() / 100));
                                            PriceDisc = price;
                                        } else {
                                            if (price > PricesDTO.ppPromoAmount()) {
                                                price = price - PricesDTO.ppPromoAmount();
                                            } else {
                                                price = 0;
                                            }

                                            PriceDisc = price;
                                        }
                                    }
                                }
                            }
                        }
                        //--

                        PricesDTO.PricesWithDiscount(PriceDisc);

                        self.PricesList.push(PricesDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetAddOnTypes = function (DateFrom, DateTo) {
        //self.AddOnTypeList.removeAll();

        var bShowAddOns = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/AddonServices.svc" + "/json/GetAddonQtys?companyCode=" + self.CompanyCode() + "&DateFrom=" + DateFrom + '&DateTo=' + DateTo,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetAddonQtysResult != null) {
                        if (response.GetAddonQtysResult.length > 0) {
                            bShowAddOns = true;
                        }
                    }                                       
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bShowAddOns;
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, RateCode) {
        //self.AddOnTypeList.removeAll();

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmount?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&sRateCode=" + RateCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountResult != null) {
                        $.each(response.GetPromoCodeAmountResult, function (key, value) {
                            var objDTO = new PromoCodeModel();
                            objDTO.MapEntity(value, objDTO);

                            //PriceDisc = PricesDTO.ppPrices();
                            //Discount = PricesDTO.Discount();

                            self.PromoCodeDTO(objDTO)
                            bResult = true;
                            //self.PricesList.push(PricesDTO);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    self.GetCurrentPage = function () {

        var currentpage = "";

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetCurrentPage",
            async: false,
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                currentpage = response;                
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }

        });

        return currentpage;
    }

    self.SaveNextPage = function (Page) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveCurrentPage?CurrentPage=" + Page,
            async: false,
            crossDomain: true,
            success: function (response) {
                return true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                return false;
            }
        });

        return true;
    }

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=H",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.PrepareDtoForInsert = function (RoomId,RoomType, DateFrom, DateTo, Adults, Kids, Infants, RateCode) {
        
        self.Room = ko.observable(new RoomLiteModel());
        self.Room().Code(RoomId);
        self.Room().Price(parseFloat($("#roomPrice_" + RoomId).html()));
        self.Room().Desc($("#roomTypeDesc_" + RoomId).html());
        self.Room().fromDt(DateFrom);
        self.Room().toDt(DateTo);
        //self.Room().Infants($("#InfantsSpinner_" + RoomId).val());
        //self.Room().Kids($("#KidsSpinner_" + RoomId).val());
        //self.Room().Adults($("#AdultsSpinner_" + RoomId).val());
        self.Room().Adults(Adults);
        self.Room().Kids(Kids);
        self.Room().Infants(Infants);
        self.Room().CompanyId(self.CompanyCode());
        self.Room().RoomType(RoomType);
        self.Room().TaxPerc(self.Tax());
        self.Room().RateCode(RateCode);

        return self.Room();
    }

    self.PrepareReservationInfoDtoForInsert = function () {
        self.ReservationDTO().id(0);

        self.ReservationDTO().CompanyID(self.CompanyCode());
        self.ReservationDTO().FName('');
        self.ReservationDTO().LName('');
        self.ReservationDTO().Country('');
        self.ReservationDTO().Phone('');
        self.ReservationDTO().EMail('');

        self.ReservationDTO().PromCode('');
        self.ReservationDTO().TaxAmount(0);
        self.ReservationDTO().AddonAmount(0);
        self.ReservationDTO().PromoAmount(0);
        self.ReservationDTO().Discount(0);
        self.ReservationDTO().total(0);

        //self.ReservationDTO().Processor(self.Processor());
        self.ReservationDTO().Processor("credomatic"); // uncomment for demo
        self.ReservationDTO().CardType("MC");
    }

    //-- Save rooms info in cookie
    self.SaveReservationRooms = function (RoomId, RoomType, DateFrom, DateTo, RateCode) {

        var bReturn;

        var objroom = ko.observable(new RoomLiteModel());

        $('#room_section_' + RoomId + ' div.roomdiv').each(function (i, item) {
            objroom = self.PrepareDtoForInsert(RoomId, RoomType, DateFrom, DateTo, 
                                               $(this).find(".AdultsSpinner").val(), $(this).find(".KidsSpinner").val(), $(this).find(".InfantsSpinner").val(), RateCode);
            self.InsertedRoomList.push(objroom);
        });            

        var newRooms = JSON.stringify(ko.toJS(self.InsertedRoomList()));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationRooms",
            async: false,
            crossDomain: true,
            data: newRooms,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;                
            },
            error: function (errorResponse) {
                self.appData.ProcessEnd();
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;                
            }
        });

        return bReturn;
    }

    //-- Save Reservation Info in cookie
    self.SaveReservationCookieOnly = function () {

        var bReturn;
        self.PrepareReservationInfoDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;       

        self.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));        

        var NewObj = JSON.stringify(ko.toJS(self.PromoCodeDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SavePromoCodeInfo",
            async: false,
            crossDomain: true,
            data: NewObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //------------------------------- Begin - Get cookies info -------------------------------//

    //-- Get saved rooms in the cookie for the company
    self.GetSavedRooms = function () {

        self.AddedRoomList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedRooms",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var RommsInfo = new RoomLiteModel();
                        RommsInfo.MapEntity(value, RommsInfo);
                        self.AddedRoomList.push(RommsInfo);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetReservationInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetReservationInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var ReservInfo = new ReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.ReservationDTO(ReservInfo);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }
    //------------------------------- End - Get cookies info -------------------------------//



    self.ShowErrorList = function (data) {
        $("#DivErrorList").show();        
    }

    function formatDate(date, separator_char) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join(separator_char);
    }

    //-- Book Now Button in room type
    Book_Now_Button = function (data, event) {        
        var li_roomId = $(event.target).closest('li').attr('id');
        var valuesarray = li_roomId.split("_");
        var roomId, roomCode;
        if (valuesarray.length > 2) {
            roomId = valuesarray[1];
            roomCode = valuesarray[2];
        }        
        
        if ($("#book_section_" + roomId).is(':visible')) {
            $("#book_section_" + roomId).hide(300);
        } else {

            try{
                loading(true);

                $(".book_section_style").hide();
                $("#book_section_" + roomId).show(150);            

                var currentdate = new Date();
                var strdate = formatDate(currentdate, '-');
                self.PricesList([]); //Clean pricelist
                self.LoadPrices(strdate, roomCode, $.trim($("#txtPromotionalCode").val()));
                self.AvailableList([]); //Clean list of unavailable days
                self.LoadAvaibility(strdate);

                if ($("#datepicker_" + roomId).hasClass("is-datepick")){
                    $("#datepicker_" + roomId).datepick('destroy');
                }                   

                $("#datepicker_" + roomId).datepick({
                    monthsToShow: 2,
                    rangeSelect: true,
                    showOtherMonths: true,
                    changeMonth: false,
                    prevText: 'Previous Month',
                    nextText: 'Next Month',
                    onDate: showDayCellData,
                    onChangeMonthYear: loadDataOnMonthYearChange,
                    onSelect: dateRangeSelected,
                    showTrigger: '#calImg',
                    pickerClass: 'PickerStyle',
                    minDate: 0, maxDate: +330                
                });

                //-- Check for the max value for the spinners
                SetMaxValueForSpinners(roomId);
                //--

                loading(false);
            }
            catch (ex) {
                loading(false);
            }
         }                       
    }
    //-- 

    //-- Book button in calendar
    Book_Button = function (data, event) {
        var li_roomId = $(event.target).closest('li').attr('id');
        var valuesarray = li_roomId.split("_");
        var roomId, roomCode;
        if (valuesarray.length > 2) {
            roomId = valuesarray[1];
            roomCode = valuesarray[2];
        }        

        var dates = $("#datepicker_" + roomId).datepick('getDate');

        //$("#dialog").dialog({
        //    autoOpen: false,
        //    modal: true,
        //    buttons: {
        //        Ok: function () {
        //            $(this).dialog("close");
        //        }
        //    }
        //});       

        //-- Validate dates in calendar
        if (dates == null) {
            alertDialog('Select dates to book your room.');
            return false;
        } else if (dates.length == 0) {
            alertDialog('Select dates to book your room.');
            return false;
        } else if (dates.length > 1) {
            if (dates[0] >= dates[1]) {
                alertDialog('Select a departure date to book your room.');
                return false;
            }            
        }
        //--

        //-- Validate guests chosen

        var alertmessage = '';
        //iterate rooms
        $('#room_section_' + roomId + ' div.roomdiv').each(function (i, item) {
            var adults = parseInt($(this).find(".AdultsSpinner").val());
            var kids = parseInt($(this).find(".KidsSpinner").val());
            var infants = parseInt($(this).find(".InfantsSpinner").val());

            var roomlabel = $(this).find(".roomnumberlabel").html();

            if ((adults === 0) && (kids === 0) && (infants === 0)) {
                alertmessage = 'Select at least a guest to book your room - ' + roomlabel
                return false;
            } else if ((adults === 0) && (kids === 0) && (infants > 0)) {
                alertmessage = 'Select at least an adult guest to book your room - ' + roomlabel          
                return false;
            } else if ((adults === 0) && (kids > 0) && (infants > 0)) {
                alertmessage = 'Select at least an adult guest to book your room. - ' + roomlabel
                return false;
            } else {

                if ($.isNumeric($("#roomMaxCapacity_" + roomId).html())) {
                    var maxRoomCapacity = parseInt($("#roomMaxCapacity_" + roomId).html());
                    var totalguests = adults + kids + infants;
                    if (totalguests > maxRoomCapacity) {
                        alertmessage = 'The total of guests selected has exceeded the room max capacity. Max capacity: ' + maxRoomCapacity + ' - ' + roomlabel
                        return false;
                    }
                }
            }
           
        });

        if (alertmessage != '') {
            alertDialog(alertmessage);
            return false;
        }        
        //--

        var bselectable = true;

        //-- Check if the date range selected contains a date with no available rooms
        if (objModelRooms.AvailableList() != null) {
            if (objModelRooms.AvailableList().length > 0) {                
                var start = dates[0],
                end = dates[1],
                currentDate = new Date(start);
                //between = []
                while ((currentDate <= end) && bselectable) {
                    //between.push(new Date(currentDate));

                    var dateformatted = formatDate(currentDate, '/', 'ddmmyyyy');

                    var filteredPrices;
                    filteredPrices = Enumerable.From(objModelRooms.AvailableList()).Where(function (x) { return formatDate(x.ppDay(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
                    if (filteredPrices != null) {
                        if (filteredPrices.length > 0) {
                            bselectable = false;
                        }
                    }

                    currentDate.setDate(currentDate.getDate() + 1);
                }
            }
        } 

        if (!bselectable) {
            alertDialog('There is no room(s) available for the selected dates, please change your selection.');
            return false;
        }
        //--

        var value = '';
        var DateFrom, DateTo;
        if (dates.length > 1) {
            DateFrom = formatDate(dates[0], '-', 'yyyymmdd');
            DateTo = formatDate(dates[1], '-', 'yyyymmdd');
        }

        //Get RateCode for the selected room
        var RateCode;
        if (objModelRooms.RoomList() != null) {
            if (objModelRooms.RoomList().length > 0) {
                var filtered;
                filtered = Enumerable.From(objModelRooms.RoomList()).Where(function (x) { return x.ppRoomTypeID() == parseInt(roomId) }).ToArray();
                if (filtered != null) {
                    if (filtered.length > 0) {
                        RateCode = filtered[0].RateCode();
                    }
                }
            }
        }
        
        try{ 
            loading(true, 'Please wait ...');

            if ($.trim($("#txtPromotionalCode").val()) == '') {
                deleteCookie("PromoCodeInfo");
            }

            var Saved = self.SaveReservationRooms(roomId, roomCode, DateFrom, DateTo, RateCode);

            if (Saved === true) {

                //-- Save ReservationInfo in cookie
                self.SaveReservationCookieOnly();
                //

                var bShowAddOns = self.GetAddOnTypes(DateFrom, DateTo);

                //bShowAddOns = true; //commented for demo
                if (bShowAddOns) {
                    self.SaveNextPage("AddOns");
                    var url = $("#txtTrackingURLBase").val() + '/AddOns/AddOns?hotelcode=' + $.trim(self.CompanyCode()) + '&DateFrom=' + DateFrom + '&DateTo=' + DateTo;
                    window.location.href = url;
                } else {
                    self.SaveNextPage("Checkout");
                    var url = $("#txtTrackingURLBase").val() + '/Checkout/Checkout?hotelcode=' + $.trim(self.CompanyCode());
                    window.location.href = url;
                }
            }

            loading(false);
        }
        catch (ex) {
            loading(false);
        }
    }

    //-- Enhance visit button
    enhance_visit_button = function (data, event) {
        var url = $("#txtTrackingURLBase").val() + '/AddOns/AddOns?hotelcode=' + $.trim(self.CompanyCode())
        window.location.href = url;       
    }

    //-- Checkout button
    checkout_button = function (data, event) {
        var url = $("#txtTrackingURLBase").val() + '/Checkout/Checkout?hotelcode=' + $.trim(self.CompanyCode())
        window.location.href = url;
    }

    //-- Close button
    btn_close_popup = function (data, event) {
        //$("#dialogPaymentPopup").dialog("close");
        $("#dialogPaymentPopup").modal('hide');
    }

    //-- Add Room button in room type
    Add_Room_Button = function (data, event) {
        var li_roomId = $(event.target).closest('li').attr('id');
        var valuesarray = li_roomId.split("_");
        var roomId, roomCode;
        if (valuesarray.length > 2) {
            roomId = valuesarray[1];
            roomCode = valuesarray[2];
        }        

        var currentnumberofrooms = $('#room_section_' + roomId + ' div.roomdiv').length; //get current number of rooms

        $(".AdultsSpinner").spinner("destroy");
        $(".KidsSpinner").spinner("destroy");
        $(".InfantsSpinner").spinner("destroy");

        $('#room_section_' + roomId + ' div.roomdiv:last').clone().appendTo($("#room_section_" + roomId)); //clone room 

        $('#room_section_' + roomId + ' div.roomdiv:last').find(".roomnumberlabel").html("Room " + (currentnumberofrooms + 1)); // put the room number in label

        $('#room_section_' + roomId + ' div.roomdiv:last').find(".removelabel span").attr("id", "removeroomlink_" + roomId + "_" + (currentnumberofrooms + 1)) //Change the Id attribute for the remove link

        $('#room_section_' + roomId + ' div.roomdiv').find(".removelabel").show(); //show "remove" label in first room

        $("#removeroomlink_" + roomId + "_1").unbind().bind("click", removeRoomFromList); //bind click event to remove first room

        $("#removeroomlink_" + roomId + "_" + (currentnumberofrooms + 1)).unbind().bind("click", removeRoomFromList); //bind click event to remove the room cloned

        currentnumberofrooms = $('#room_section_' + roomId + ' div.roomdiv').length; //get current number of rooms
        showAddRoomSection(currentnumberofrooms);

        ////-- Init spinners and check for the max value             
        $(".AdultsSpinner").spinner({ min: 0 });        
        $(".KidsSpinner").spinner({ min: 0 });             
        $(".InfantsSpinner").spinner({ min: 0 });

        var AdultsSpinner = $('#room_section_' + roomId + ' div.roomdiv:last').find(".AdultsSpinner").spinner();
        AdultsSpinner.spinner("value", 1);
        var KidsSpinner = $('#room_section_' + roomId + ' div.roomdiv:last').find(".KidsSpinner").spinner();
        KidsSpinner.spinner("value", 0);
        var InfantsSpinner = $('#room_section_' + roomId + ' div.roomdiv:last').find(".InfantsSpinner").spinner();
        InfantsSpinner.spinner("value", 0);

        SetMaxValueForSpinners(roomId);

        FixSpinnerIcons();
        ////--       
    }

    self.InitModel = function () {      
       
        //-- Init message/alert dialog
        //$("#dialog").dialog({
        //    autoOpen: false,
        //    modal: true,
        //    buttons: {
        //        Ok: function () {
        //            $(this).dialog("close");
        //        }
        //    }
        //});
        //--

        //-- Init dialogPaymentPopup dialog 
        //$("#dialogPaymentPopup").dialog({
        //    autoOpen: false,
        //    modal: true,
        //    height: 455,
        //    width: 525,
        //    resizable: false            
        //});        

       // $("#dialogPaymentPopup").dialog("widget")
       //.find(".ui-dialog-titlebar").css({
       //    "float": "right",
       //    border: 0,
       //    padding: 0
       //})
       //.find(".ui-dialog-title").css({
       //    display: "none"
       //}).end()
       //.find(".ui-dialog-titlebar-close").css({
       //    display: "none"
       //});
        //--              

        //-- Verify cookie to see if we need to redirect to other view
        var CurrentPage = '';        
        CurrentPage = self.GetCurrentPage();

        if ($.trim(CurrentPage).toLowerCase() === "addons") {
            //var url = $("#txtTrackingURLBase").val() + '/AddOns/AddOns?hotelcode=' + $.trim(self.CompanyCode()) + '&DateFrom=' + DateFrom + '&DateTo=' + DateTo;
            //var url = $("#txtTrackingURLBase").val() + '/AddOns/AddOns';
            //window.location.href = url;
            //return false;
        }

        if ($.trim(CurrentPage).toLowerCase() === "checkout") {
            //-- Verify rooms cookie info
            self.GetReservationInfo();
            if (self.ReservationDTO() != null) {
                if (self.ReservationDTO().total() != null) {
                    if (self.ReservationDTO().total() != '') {
                        $("#lbTotalToPay").text('$' + parseFloat(self.ReservationDTO().total()).toFixed(2));

                        //-- Open Payment Popup
                        $("#dialogPaymentPopup").modal({ backdrop: "static" });
                        $('#dialogPaymentPopup').modal('show');
                        //$("#dialogPaymentPopup").show();
                        //$("#dialogPaymentPopup").dialog("open");
                        //--    
                    }
                }
            }                 
        }        
        

        //-- Verify rooms cookie info
        self.GetSavedRooms();

        var CompanyCode = "";
        var bLoadRoomsCookie = false;
        var roomcode, dtFrom, dtTo, adults, kids, infants;
        
        if (self.AddedRoomList() != null) {
            if (self.AddedRoomList().length > 0) {
                $.each(self.AddedRoomList(), function (i, item) {
                    if (item.CompanyId() != null) {
                        if (item.CompanyId() != "") {
                            CompanyCode = item.CompanyId();
                            roomcode = item.Code();
                            dtFrom = item.fromDt();
                            dtTo = item.toDt();
                            adults = item.Adults();
                            kids = item.Kids();
                            infants = item.Infants();

                            bLoadRoomsCookie = true;

                            return false;
                        }
                    }
                });
            }
        }
        //--      

        if (CompanyCode == "") {
            CompanyCode = $("#hdf_HotelCode").val();
            if (CompanyCode == "") {
                alertDialog("No company provided");
                return false;
            }
        }

        self.CompanyCode(CompanyCode);

        //--Test
        //self.CompanyCode("BDIHS");
        
        self.getLogo($("#txtTrackingURLBase").val() +  "/Handlers/getImage.ashx?hotelId=" + $.trim(self.CompanyCode()) + "&photoNumber=4");
        self.LoadCompany();
        self.LoadRooms();        


        //


        
    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}

var objModelRooms;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              
               
               try{                              
                   loading(true, 'Please wait ...');

                   $("#MasterDivSummaryContent").hide();
                   $("#MasterDivMainContent").removeClass("col-sm-9").addClass("col-sm-12");
                   
                   objModelRooms = new RoomsAvailabilityVm();
                   ko.applyBindings(objModelRooms);

                   var urlCompanyLogo = objModelRooms.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);                   
                   //--                           

                   var AdultsSpinner = $(".AdultsSpinner").spinner({ max: 4, min: 0, icons: { down: "ui-icon-triangle-1-s", up: "ui-icon-triangle-1-n" } });
                   AdultsSpinner.spinner("value", 1);
                   AdultsSpinner.width(20);

                   var KidsSpinner = $(".KidsSpinner").spinner({ max: 2, min: 0, icons: { down: "ui-icon-triangle-1-s", up: "ui-icon-triangle-1-n" } });
                   KidsSpinner.spinner("value", 0);
                   KidsSpinner.width(20);

                   var InfantsSpinner = $(".InfantsSpinner").spinner({ max: 2, min: 0, icons: { down: "ui-icon-triangle-1-s", up: "ui-icon-triangle-1-n" } });
                   InfantsSpinner.spinner("value", 0);
                   InfantsSpinner.width(20);

                   FixSpinnerIcons();                   
                   //--                                    
                                                                                                                         
                 
                   //-- Match media - responsive                                 
                   //if (matchMedia) {
                   //    var mq = window.matchMedia("(min-width: 1297px)");
                   //    mq.addListener(WidthChange);
                   //    WidthChange(mq);
                   //}
                   //--

                   //-- Load Room info from cookie                   
                   var CompanyCode, roomcode, dtFrom, dtTo, adults, kids, infants;

                   if (objModelRooms.AddedRoomList() != null) {
                       if (objModelRooms.AddedRoomList().length > 0) {
                           $.each(objModelRooms.AddedRoomList(), function (i, item) {
                               if (item.CompanyId() != null) {
                                   if (item.CompanyId() != "") {
                                       CompanyCode = item.CompanyId();
                                       roomcode = item.Code();
                                       dtFrom = item.fromDt();
                                       dtTo = item.toDt();
                                       adults = item.Adults();
                                       kids = item.Kids();
                                       infants = item.Infants();

                                       //Load information of room chosen
                                       LoadInfoRoomChosen(roomcode, dtFrom, dtTo, adults, kids, infants);                                                                             

                                       return false;
                                   }
                               }
                           });
                       }
                   }
                   //--

                   //$("#cancel_your_payment").attr("href", $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim(self.CompanyCode()))                   
                   
                   $("#btnPromoCode").click(function () {
                       
                       var bReturn;
                       $("#PromoCodeMessageSection").hide();

                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "")
                           {
                               $("#lblPromoCodeMessage").text("Enter a valid promotional code");
                               $("#PromoCodeMessageSection").show(300);
                               //alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {

                               loading(true, 'Please wait ...');

                               objModelRooms.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((objModelRooms.PromoList().length === 0) || (objModelRooms.PromoList() === null) || (objModelRooms.PromoList() === undefined)) {
                                   $("#lblPromoCodeMessage").text("Invalid promotional code");
                                   $("#PromoCodeMessageSection").show(300);
                                   //alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(objModelRooms.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(objModelRooms.PromoList()[0].ppEndDate(), '/'));

                                   //var DateFrom = formatDate(objModelRooms.PromoList()[0].UnFormatFrom(), '-', 'yyyymmdd');
                                   //var DateTo = formatDate(objModelRooms.PromoList()[0].UnFormatTo(), '-', 'yyyymmdd');
                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       //alertDialog('Invalid promotional code');
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else if (CurrentDate > DateTo) {
                                       //alertDialog('Promotional code has expired'); 
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else {
                                       if (objModelRooms.RoomList() != null) {
                                           if (objModelRooms.RoomList().length > 0) {
                                               $.each(objModelRooms.RoomList(), function (i, item) {
                                                   objModelRooms.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.RateCode())
                                                   if (objModelRooms.PromoCodeDTO() != null) {
                                                       if (objModelRooms.PromoCodeDTO().ppAmount() > 0) {
                                                           var originalprice = item.Price();
                                                           var price = item.Price();                                                           
                                                           if (price > 0) {
                                                               if (objModelRooms.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                                                                   price = price - (price * (objModelRooms.PromoCodeDTO().ppAmount() / 100));
                                                                   //item.Price(price.toFixed(2));
                                                               } else {
                                                                   if (price > objModelRooms.PromoCodeDTO().ppAmount()) {
                                                                       price = price - objModelRooms.PromoCodeDTO().ppAmount();
                                                                   } else {
                                                                       price = 0;
                                                                   }

                                                                   //item.Price(price);
                                                               }
                                                           }

                                                           if (originalprice > price) {
                                                               $("#RoomPriceWithDiscount_" + item.ppRoomTypeID()).text('$' + price.toFixed(2));
                                                               $("#RoomPriceWithDiscount_" + item.ppRoomTypeID()).show();
                                                               $("#roomPrice_" + item.ppRoomTypeID()).css("text-decoration", "line-through"); 
                                                           } else {
                                                               $("#RoomPriceWithDiscount_" + item.ppRoomTypeID()).text(''); //clean 'room price with discount' label
                                                               $("#roomPrice_" + item.ppRoomTypeID()).css("text-decoration", "none"); //remove line (style) over the price
                                                           }
                                                           
                                                           $("#book_section_" + item.ppRoomTypeID()).hide(); //Hide Calendar section for each room                                                           
                                                       }
                                                   }
                                               });

                                               //--Save promo code info in cookie
                                               objModelRooms.SavePromoCodeCookie();
                                               //--

                                               //alertDialog('Promotional code was applied');
                                               $("#lblPromoCodeMessage").text("Promotional code was applied");
                                               $("#lblPromoCodeMessage").css('color', '#65ca65');
                                               $("#PromoCodeMessageSection").show(300);

                                               
                                               $('#txtPromotionalCode').prop('readonly', true);
                                               $("#btnPromoCode").html('Revert');
                                               $("#PromoApprovedIcon").show(300);

                                               $("#lblPromoCodeInHeader").text($("#txtPromotionalCode").val());
                                               $("#PromoCodeInHeaderSection").show(300);
                                               $("#PromoCodeInHeaderSection").css('display', 'inline-flex');

                                               setTimeout(function () {
                                                   $('#dialogPromoCodePopup').modal("hide");
                                               }, 2500);                                               

                                           }
                                       }
                                   }
                               }

                               loading(false);
                           }

                           catch (ex) {
                               alertDialog(ex);
                               loading(false);
                           }
                       } else {
                           $("#PromoCodeInHeaderSection").hide();
                           deleteCookie("PromoCodeInfo");
                           location.reload();
                       }                     
                       
                   });

                   
                   setLanguages();
                   loading(false);
                  
               }
               catch (ex) {
                   loading(false);
                   alertDialog('Error loading rooms data. Error: ' + ex.message);
               }

               
           }

           
);
//*******************                              ******************//

//-- Begin Rooms section --
function removeRoomFromList(event) {
    var room = $(this).closest(".roomdiv");

    var li_roomId = room.closest('li').attr('id');
    var valuesarray = li_roomId.split("_");
    var roomId, roomCode;
    if (valuesarray.length > 2) {
        roomId = valuesarray[1];
        roomCode = valuesarray[2];
    }    

    room.remove();

    var currentnumberofrooms = $('#room_section_' + roomId + ' div.roomdiv').length; //get current number of rooms
    if (currentnumberofrooms == 1) {
        $('#room_section_' + roomId + ' div.roomdiv').find(".removelabel").hide(); //hide "remove" label in first room
    }
    
    $('#room_section_' + roomId + ' div.roomdiv').each(function (i, item) {
        $(this).find(".roomnumberlabel").html("Room " + (i + 1)); // put the room number in label
        $(this).find(".removelabel span").attr("id", "removeroomlink_" + roomId + "_" + (i + 1)) //Change the Id attribute for the remove link
        $("#removeroomlink_" + roomId + "_" + (i + 1)).unbind().bind("click", removeRoomFromList); //bind click event to remove the room cloned
    });

    showAddRoomSection(currentnumberofrooms);
}

function showAddRoomSection(currentnumberofrooms) {
    var MaxRoomsAllowedToBook = 3;
    if ($("#hdf_MaxRoomsAllowedToBook").val() != null) {
        if ($("#hdf_MaxRoomsAllowedToBook").val() != '') {
            MaxRoomsAllowedToBook = parseInt($("#hdf_MaxRoomsAllowedToBook").val());
        }
    }

    if (currentnumberofrooms >= MaxRoomsAllowedToBook) {
        $(".add_room_button_div").hide();
    } else {
        $(".add_room_button_div").show();
    }
}

function SetMaxValueForSpinners(roomId) {
    //-- Check for the max value for the spinners
    var maxadults = 4;
    var maxkids = 2;
    if (objModelRooms.RoomList() != null) {
        if (objModelRooms.RoomList().length > 0) {
            var filteredRoom;

            filteredRoom = Enumerable.From(objModelRooms.RoomList()).Where(function (x) { return x.ppRoomTypeID() == roomId }).ToArray();
            if (filteredRoom != null) {
                if (filteredRoom.length > 0) {
                    maxadults = filteredRoom[0].ppMax_Adults();
                    maxkids = filteredRoom[0].ppMax_Children();
                }
            }
        }
    }

    $('.AdultsSpinner').spinner('option', 'max', maxadults);
    $('.KidsSpinner').spinner('option', 'max', maxkids);
    $('.InfantsSpinner').spinner('option', 'max', 2);
    //--
}

//-- Fix Spinner Icons issue with bootstrap
function FixSpinnerIcons() {
    $('.ui-spinner-button.ui-spinner-up').each(function (i, item) {
        $(this).addClass('ui-button');
        $(this).addClass('ui-widget');
        $(this).addClass('ui-button-icon-only');
        var htmlbuttonup = '<span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span><span class="ui-button-icon-space"></span>'
        $(this).html(htmlbuttonup);
    });

    $('.ui-spinner-button.ui-spinner-down').each(function (i, item) {
        $(this).addClass('ui-button');
        $(this).addClass('ui-widget');
        $(this).addClass('ui-button-icon-only');
        var htmlbuttonup = '<span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span><span class="ui-button-icon-space"></span>'
        $(this).html(htmlbuttonup);
    });
}
//-- End Rooms section --

//******************* jQuery Spinner and Calendar *******************//
// media query change
function WidthChange(mq) {
    //var msg = (mq.matches ? "more" : "less") + " than 500 pixels";
    var roomId;
    $(".book_section_style").each(function () {
        if ($(this).is(':visible')) {
            var li_roomId = $(this).closest('li').attr('id');
            var valuesarray = li_roomId.split("_");           
            if (valuesarray.length > 2) {
                roomId = valuesarray[1];
                roomCode = $.trim(valuesarray[2]);
            }

            var p = $('#ddl_room_options_' + roomId);
            var rest = p.width() + p.css('padding-left') + p.css('padding-right');
            var position = p.offset();
            var left = position.left;
            var top = position.top;
           
            //if (mq.matches) {
            //    //more
            //} else {
            //    //less
            
            //}
        }
    });
 
}

function showDayCellData(date) {

    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var dayname = weekday[date.getDay()];
    var priceperday = '';       
    var dateformatted = formatDate(date, '/', 'ddmmyyyy');    
       
    if (objModelRooms.PricesList() != null) {
        var filteredPrices;        
        //linq to find the price for the specific date
        filteredPrices = Enumerable.From(objModelRooms.PricesList()).Where(function (x) { return formatDateJsonLocal(x.ppDate(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
        if (filteredPrices != null) {
            if (filteredPrices.length > 0){
                priceperday = '$' + (filteredPrices[0].PricesWithDiscount().toFixed(2));
                var today = new Date();                
                var todayformatted = formatDate(today, '/', 'ddmmyyyy');
                if (dateformatted === todayformatted) {
                    var li_roomId = $(this).closest('li').attr('id');
                    var valuesarray = li_roomId.split("_");
                    var roomId;
                    if (valuesarray.length > 2) {
                        roomId = valuesarray[1];                        
                    }

                    $("#RoomPriceWithDiscount_" + roomId).text(priceperday);
                    //$("#roomPrice_" + roomId).css("text-decoration", "line-through");

                    if ($.isNumeric($("#roomPrice_" + roomId).text().replace("$", "")) && $.isNumeric($("#RoomPriceWithDiscount_" + roomId).text().replace("$", ""))) {
                        var normalprice = parseFloat($("#roomPrice_" + roomId).text().replace("$", ""));
                        var pricewithdisc = parseFloat($("#RoomPriceWithDiscount_" + roomId).text().replace("$", ""));
                        if (normalprice != pricewithdisc) {
                            $("#roomPrice_" + roomId).css("text-decoration", "line-through");
                            $("#RoomPriceWithDiscount_" + roomId).show();
                        } else {
                            $("#roomPrice_" + roomId).css("text-decoration", "none");
                            $("#RoomPriceWithDiscount_" + roomId).hide();
                        }
                    }                    
                    
                }
            }
        }              
    }

    var CellClassStyle = 'DateCellStyle';
    var bselectable = true;
    var tooltip = $.datepick.formatDate('Select DD, M d, yyyy', date);

    var currentdate = new Date();    
    var dateinCalendar = new Date(date);

    dateinCalendar.setHours(0, 0, 0, 0)
    currentdate.setHours(0, 0, 0, 0)

    if (dateinCalendar >= currentdate)
    {
        if (objModelRooms.AvailableList() != null) {
            var filteredPrices;
            //linq to find the price for the specific date
            filteredPrices = Enumerable.From(objModelRooms.AvailableList()).Where(function (x) { return formatDate(x.ppDay(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
            if (filteredPrices != null) {
                if (filteredPrices.length > 0) {
                    CellClassStyle = 'UnavailableDateCellStyle';
                    bselectable = false;
                    tooltip = 'No rooms available';
                }
            }
        }
    }   

    var textcontent = '' + dayname + '' + '<div class="datenumberstyle">' + date.getDate() + '</div>' + '<div>' + priceperday + '</div>';

    return { content: textcontent, dateClass: CellClassStyle, selectable: bselectable, title: tooltip };
}

function loadDataOnMonthYearChange(year, month) {

    var strdate = [year, month, '01'].join('-');
    var roomCode = '';

    loading(true);

    $(".book_section_style").each(function () {
        if ($(this).is(':visible')) {
            var li_roomId = $(this).closest('li').attr('id');
            var valuesarray = li_roomId.split("_");
            var roomId;
            if (valuesarray.length > 2) {
                roomId = valuesarray[1];
                roomCode = $.trim(valuesarray[2]);
            }
        }
    });

    objModelRooms.PricesList([]); //Clean pricelist
    objModelRooms.LoadPrices(strdate, roomCode, $.trim($("#txtPromotionalCode").val()));

    //objModelRooms.AvailableList([]); //Clean list of unavailable days
    objModelRooms.LoadAvaibility(strdate);

    loading(false);
}

function dateRangeSelected(dates) {
    if (dates != null) {

        var roomCode = '';
        var roomId;

        $(".book_section_style").each(function () {
            if ($(this).is(':visible')) {
                var li_roomId = $(this).closest('li').attr('id');
                var valuesarray = li_roomId.split("_");               
                if (valuesarray.length > 2) {
                    roomId = valuesarray[1];
                    roomCode = $.trim(valuesarray[2]);
                }
            }
        });

        //-- Mantain calendar if first date selected is in the current month
       var d = new Date(),
       currentmonth = (d.getMonth() + 1);

        var monthoffirstdate = dates[0].getMonth() + 1
        var monthof2nddate = dates[1].getMonth() + 1

        if ((monthoffirstdate == currentmonth) && (monthof2nddate == (currentmonth + 1))) {
            $("#datepicker_" + roomId).datepick('option', { monthsOffset: 0 });
        }
        //--

        $(this).closest('li').find('label').each(function () {
            var labelid = $(this).attr('id');
            if (labelid.indexOf('lblArrivalDate') != -1) {
                var datevaluelabel = formatDateToShowLabel(dates[0]);
                $(this).html(datevaluelabel);
            }

            if (labelid.indexOf('lblDepartureDate') != -1) {
                var datevaluelabel = formatDateToShowLabel(dates[1]);
                $(this).html(datevaluelabel);
            }
        });        

        var strdate = formatDate(dates[1], '-', 'yyyymmdd')
        
        objModelRooms.PricesList([]); //Clean pricelist
        objModelRooms.LoadPrices(strdate, roomCode, $.trim($("#txtPromotionalCode").val()));

        //objModelRooms.AvailableList([]); //Clean list of unavailable days
        objModelRooms.LoadAvaibility(strdate);
    }   
}

//Load information of room chosen
function LoadInfoRoomChosen(roomcode, dtFrom, dtTo, adults, kids, infants) {
    $("#roomTypeDesc_" + roomcode).closest("li").find(".Book_Now_Button").trigger("click");

    $("#AdultsSpinner_" + roomcode).val(adults);
    $("#KidsSpinner_" + roomcode).val(kids);
    $("#InfantsSpinner_" + roomcode).val(infants);

    var dtFromArray = dtFrom.split("-");
    var dtFromFormatted;
    if (dtFromArray.length > 2) {
        dtFromFormatted = dtFromArray[1] + "/" + dtFromArray[2] + "/" + dtFromArray[0]
    }

    var dtToArray = dtTo.split("-");
    var dtToFormatted;
    if (dtToArray.length > 2) {
        dtToFormatted = dtToArray[1] + "/" + dtToArray[2] + "/" + dtToArray[0]
    }

    var dates = [];
    dates.push(new Date(dtFromFormatted));
    dates.push(new Date(dtToFormatted));

    $("#datepicker_" + roomcode).datepick('setDate', dates);
}

function formatDate(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateJsonLocal(date, separator_char, returned_format) {
    var dateString = date;
    var dx = new Date(parseInt(dateString.substr(6)));

    var day = dx.getDate();
    var month = dx.getMonth() + 1;
    var year = dx.getFullYear();

    if (day <= 9) {
        day = "0" + day;
    }

    if (month <= 9) {
        month = "0" + month;
    }

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDateToShowLabel(date) {
    var returnedvalue;
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";

    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var dayname = weekday[date.getDay()];
    var monthname = months[date.getMonth()];
    var daynumber = date.getDate();
    var ordinalday;

    if (daynumber == 1 || daynumber == 21 || daynumber == 31) {
        ordinalday = daynumber + 'st'
    } else if (daynumber == 2 || daynumber == 22) {
        ordinalday = daynumber + 'nd'
    } else if (daynumber == 3 || daynumber == 23) {
        ordinalday = daynumber + 'rd'
    } else {
        ordinalday = daynumber + 'th'
    }
   
    returnedvalue = monthname + ', ' + dayname + ' ' + ordinalday;

    return returnedvalue;
}

function cancel_your_payment() {
    deleteCookie("RoomInfo");
    deleteCookie("CurrentProgress");
    deleteCookie("AddonInfo");
    deleteCookie("ReservationInfo");
    
    var url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim(objModelRooms.CompanyCode())
    window.location.href = url;

    return true;
}


function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}


//******************* jQuery Spinner and Calendar END ****************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    //$("#dialog").show();    
    //$("#dialog").dialog("open");

    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
    
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function setLanguages() {
    var cookieValue = $.cookie("be-lang");
    if (cookieValue == "es-CR") {
        $("#FeaturedContent_book_p").text('RESERVER SU TIPO DE HABITACION');
        $("#progress_availability a").text('Disponibilidad')
        
    } else {
        $("#FeaturedContent_book_p").text('BOOK YOUR ROOM TYPE');
        $("#progress_availability a").text('Rooms Availability')
    }
}

