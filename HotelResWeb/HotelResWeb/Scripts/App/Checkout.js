﻿/// <reference path="../DTO/ReservationModel.js" />
function CheckoutVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();

    self.InsertedAddonList = ko.observableArray();
    self.AddedRoomList = ko.observableArray();
    self.AddedAddonList = ko.observableArray();
    self.PromoList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.CardTypeList = ko.observableArray();
    

    self.getLogo = ko.observable();

    self.TempAddedAddonList = ko.observableArray();


    self.ReservationDTO = ko.observable(new ReservationModel());
    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.selectedCountry = ko.observable(new CountriesModel());

    self.BNReqModel = ko.observable(new BNRequestModel());
    self.CredomaticReqModel = ko.observable(new CredomaticRequestModel());

    self.PromoCodeDTO = ko.observable(new PromoCodeModel());
    self.PromoList = ko.observableArray();
    self.AgencyDTO = ko.observable(new AgenciesHeaderModel());

    self.CompanyTermsAndConditions = ko.observableArray();

    self.ValidatePromoCode = function (PromoCode) {

        self.PromoList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCode?sCompanyCode=" + self.CompanyCode() + "&sPromoCode=" + PromoCode + "&sPromoType=H",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    $.each(response.GetPromoCodeResult, function (key, value) {
                        var PromoDTO = new PromoCodeModel();
                        PromoDTO.MapEntity(value, PromoDTO);
                        self.PromoList.push(PromoDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedAddons = function () {

        self.AddedAddonList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedAddons",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var AddonInfo = new AddOnLite();
                        AddonInfo.MapEntity(value, AddonInfo);
                        AddonInfo.PriceInText('$' + AddonInfo.Price());
                        AddonInfo.UniqueId(s4() + s4());
                        self.AddedAddonList.push(AddonInfo);
                        self.TempAddedAddonList.push(AddonInfo); //Temp added addon list
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetSavedRooms = function () {

        self.AddedRoomList.removeAll();


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetSavedRooms",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var RommsInfo = new RoomLiteModel();
                        RommsInfo.MapEntity(value, RommsInfo);

                        if (RommsInfo.Discount() > 0) {
                            RommsInfo.PriceWithDiscLabel('$' + parseFloat(RommsInfo.PriceDisc()).toFixed(2));
                            RommsInfo.Tachclass("Discount_item_summary");
                        }

                        RommsInfo.PriceLabel('$' + parseFloat(RommsInfo.Price()).toFixed(2));

                        self.AddedRoomList.push(RommsInfo);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetReservationInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetReservationInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                   if (ds !== null) {
                        var ReservInfo = new ReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.ReservationDTO(ReservInfo);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfo = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/GetPromoCodeInfo",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        var Info = new PromoCodeModel();
                        Info.MapEntity(ds, Info);
                        self.PromoCodeDTO(Info);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodeInfoXRate = function (PromoCode, RateCode) {
        //self.AddOnTypeList.removeAll();

        var bResult = false;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/PromoCodesService.svc" + "/json/GetPromoCodeAmount?iPromoID=0&sPromoCode=" + PromoCode + "&sCompanyCode=" + self.CompanyCode() + "&sRateCode=" + RateCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetPromoCodeAmountResult != null) {
                        $.each(response.GetPromoCodeAmountResult, function (key, value) {
                            var objDTO = new PromoCodeModel();
                            objDTO.MapEntity(value, objDTO);
                            objDTO.ppPromoCode(PromoCode);
                            self.PromoCodeDTO(objDTO)
                            bResult = true;                            
                        })
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

        return bResult;
    }

    self.PrepareDtoForInsert = function () {
        var promoid = 0, promoamount = 0;

        self.ReservationDTO().id(0);

        self.ReservationDTO().CompanyID(self.CompanyCode());
        self.ReservationDTO().FName($("#firstname").val());
        self.ReservationDTO().LName($("#lastname").val());
        self.ReservationDTO().Country($('#ddl_country').find(":selected").val());       
        self.ReservationDTO().Phone($("#phone").val());
        self.ReservationDTO().EMail($("#email").val());

        self.ReservationDTO().PromCode($("#txtPromotionalCode").val());
        if (self.PromoCodeDTO() != null) {
            if (self.PromoCodeDTO().ppPromoID() && self.PromoCodeDTO().ppAmount()) {
                promoid = self.PromoCodeDTO().ppPromoID();
                promoamount = self.PromoCodeDTO().ppAmount()
            }            
        }
        self.ReservationDTO().PromoID(promoid);
        self.ReservationDTO().TaxAmount(parseFloat($("#taxAmount").val()));
        self.ReservationDTO().AddonAmount(parseFloat($("#addonAmount").val()));
        self.ReservationDTO().PromoAmount(promoamount);
        self.ReservationDTO().Discount(parseFloat($("#discount").val()));
        self.ReservationDTO().total(parseFloat($("#totalAmount").val()));
        self.ReservationDTO().Subtotal(parseFloat($("#Subtotal").val()));
        
        self.ReservationDTO().CardType($('#ddl_card_type').find(":selected").val());
                
    }

    self.PrepareAddOnDtoForInsert = function (Code, Price, Desc) {

        self.AddonLite = ko.observable(new AddOnLite());

        self.AddonLite().Code(Code);
        self.AddonLite().Price(Price);
        self.AddonLite().Desc(Desc);

    }

    self.SaveReservation = function () {

        var bReturn;
        self.PrepareDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/Checkout/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var ReservInfo = new ReservationModel();
                        ReservInfo.MapEntity(ds, ReservInfo);
                        self.ReservationDTO(ReservInfo);
                        //call process card 
                        //by the processor type

                        self.ProcessCard();
                        
                        bReturn = true;

                    }
                }
               
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    //-- Save Reservation Info in cookie
    self.SaveReservationCookieOnly = function () {

        var bReturn;
        self.PrepareDtoForInsert();

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));        

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationInfo",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--

    //-- Save Promo Code Info in cookie
    self.SavePromoCodeCookie = function () {

        var bReturn;

        self.PromoCodeDTO().ppPromoCode($.trim($("#txtPromotionalCode").val()));

        var NewObj = JSON.stringify(ko.toJS(self.PromoCodeDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SavePromoCodeInfo",
            async: false,
            crossDomain: true,
            data: NewObj,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }
    //--


    self.ProcessCard = function () {

        var bReturn;

        try {

            loading(true, 'Processing pay request ...');


            if (self.ReservationDTO().Processor().toLowerCase() === "bn")
            {
                bReturn = self.RequestBN();
            }

            if (self.ReservationDTO().Processor().toLowerCase() === "credomatic") {
                bReturn = self.ProcessPay();
            }

            loading(false);

        }
        catch (exc)
        {
            loading(false);
        }

        return bReturn;
    }


    self.RequestBN = function () {

        var bReturn = false;
   
        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/Checkout/GenerateBNParams",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {
                    if (response != '') {
                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {

                            var BNModel = new BNRequestModel();
                            BNModel.MapEntity(ds, BNModel);
                            self.BNReqModel(BNModel);

                            $("#dataForm").submit();

                            bReturn = true;

                        }
                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }


    self.ProcessPay = function () {

        var bReturn;

        var NewReserv = JSON.stringify(ko.toJS(self.ReservationDTO));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/Checkout/ProcessPay",
            async: false,
            crossDomain: true,
            data: NewReserv,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {

                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var CredomaticModel = new CredomaticRequestModel();
                        CredomaticModel.MapEntity(ds, CredomaticModel);
                        self.CredomaticReqModel(CredomaticModel);

                        var postPage = self.CredomaticReqModel().action();

                        $('#PostForm').attr('action', postPage);
                        $("#PostForm").submit();

                        bReturn = true;

                    }
                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }


    self.SavedRoomsWithPromo = function (PromoCode) {

        var bReturn;
        self.AddedRoomList.removeAll();


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveRoomsPromotionalCode?PromotionalCode=" + PromoCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var RommsInfo = new RoomLiteModel();
                        RommsInfo.MapEntity(value, RommsInfo);
                        self.AddedRoomList.push(RommsInfo);
                        bReturn = true;
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

    self.SaveAddon = function (AddOnListWithData) {

        var bReturn;
        var newAddons
        
        newAddons = JSON.stringify(ko.toJS(AddOnListWithData));        

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/RoomsAvailability/SaveReservationAddons",
            async: false,
            crossDomain: true,
            data: newAddons,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                bReturn = true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                bReturn = false;
            }
        });

        return bReturn;
    }

   
    self.LoadCountries = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/DireccionServices.svc" + "/json/GetCountries" ,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCountriesResult, function (key, value) {
                        var countryDTO = new CountriesModel();
                        countryDTO.MapEntity(value, countryDTO);
                        self.CountryList.push(countryDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Load card types
    self.LoadCardTypes = function (bank) {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/ReservationService.svc" + "/json/GetCardTypes?Bank=" + bank,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    $.each(response.GetCardTypesResult, function (key, value) {
                        var cardtypeDTO = new CardTypesModel();
                        cardtypeDTO.MapEntity(value, cardtypeDTO);
                        self.CardTypeList.push(cardtypeDTO);
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Get Company terms and conditions
    self.GetCompanyTermsAndConditions = function () {

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/CompanyServices.svc" + "/json/GetCompanyTerm?sCompany_Code=" + self.CompanyCode() + '&sMode=SEL&iCulture=1',
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    if (response.GetCompanyTermResult != null && response.GetCompanyTermResult != '') {
                        var terms = '';
                        var valuesarray = response.GetCompanyTermResult.split("~");
                        if (valuesarray.length > 1) {                            
                            terms = valuesarray[1];
                        }                        
                        self.CompanyTermsAndConditions(terms);
                    }
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }
    
    //-- Load countries in dropdown
    self.LoadCountriesInDropDownList = function () {
        var dll_countries = $("#ddl_country");

        if (self.CountryList() != null) {
            if (self.CountryList().length > 0) {
                $.each(self.CountryList(), function () {
                    dll_countries.append($("<option />").val(this.ppCountryIso3()).text(this.ppName()));
                });
            } else {
                dll_countries.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_countries.append($("<option />").val(-1).text("---"));
        }        
    }

    //-- Load cardtypes in dropdown
    self.LoadCardTypesInDropDownList = function () {
        var dll_cardtypes = $("#ddl_card_type");

        if (checkoutObj.CardTypeList() != null) {
            if (checkoutObj.CardTypeList().length > 0) {
                $.each(checkoutObj.CardTypeList(), function (i, item) {
                    dll_cardtypes.append($("<option />").val($.trim(item.ppCode())).text($.trim(item.ppName())));
                });
            } else {
                dll_cardtypes.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_cardtypes.append($("<option />").val(-1).text("---"));
        }
    }

    self.GetAgencyInfo = function (AgencyCode, CompanyCode) {        

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Checkout/GetAgencyInfo?AgencyCode=" + AgencyCode + '&CompanyCode=' + CompanyCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var ds = jQuery.parseJSON(response);

                    $.each(ds, function (key, value) {
                        var Info = new AgenciesHeaderModel();
                        Info.MapEntity(value, Info);
                        self.AgencyDTO(Info);                        
                    })
                }
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    //-- Start Over button
    self.StartOver = function () {
        deleteCookie("RoomInfo");
        deleteCookie("CurrentProgress");
        deleteCookie("AddonInfo");
        deleteCookie("ReservationInfo");
        deleteCookie("PromoCodeInfo");

        var url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim(self.CompanyCode())

        // uncomment for demo
        //url = $("#txtTrackingURLBase").val() + '/RoomsAvailability/RoomsAvailability?hotelcode=' + $.trim('BDIHS')

        window.location.href = url;
    }


    self.InitModel = function () {

        self.CompanyCode($("#hdf_HotelCodeInCheckout").val());
        //self.CompanyCode("VC0213");

        self.getLogo($("#txtTrackingURLBase").val() + "/Handlers/getImage.ashx?hotelId=" + self.CompanyCode() + "&photoNumber=4");


        self.GetSavedAddons();
        self.GetSavedRooms ();
        self.GetReservationInfo();
        self.GetPromoCodeInfo();
        self.LoadCountries();
        self.GetCompanyTermsAndConditions();

    }


    //***************** CALL CONSTRUCTER**********************//

    self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//

}


var checkoutObj;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               //$("#dialog").dialog({
               //    autoOpen: false,
               //    modal: true,
               //    buttons: {
               //        Ok: function () {
               //            $(this).dialog("close");
               //        }
               //    }
               //});

               try{
                   loading(true, 'Please wait ...');

                   checkoutObj = new CheckoutVm();
                   ko.applyBindings(checkoutObj);                  

                   var urlCompanyLogo = checkoutObj.getLogo();
                   $("#idCompanyLogo").attr('src', urlCompanyLogo);                  

                   //-- Load countries in dropdownlist
                   checkoutObj.LoadCountriesInDropDownList();

                   $('#ddl_country').selectpicker('refresh');

                   //-- Show card payment info
                   var ExcludeCardValidation = false;
                   if (checkoutObj.ReservationDTO() != null) {
                       if (checkoutObj.ReservationDTO().Processor() != null) {
                           if (checkoutObj.ReservationDTO().Processor() != '') {
                               var paymenttype = checkoutObj.ReservationDTO().Processor().toString().toLowerCase();
                               paymenttype = 'credomatic' //uncomment for demo

                               //Load card types
                               checkoutObj.LoadCardTypes(paymenttype);

                               //-- Load countries in dropdownlist
                               checkoutObj.LoadCardTypesInDropDownList();
                               
                               switch(paymenttype){                                   
                                   case "bn":
                                       $("#cardinfosection").hide();
                                       ExcludeCardValidation = true;
                                       break;
                                   default:
                                       $("#cardinfosection").show();
                                       break;
                               }
                               
                           }
                       }
                   }

                   $("#btnPromoCode").click(function () {

                       var bReturn;
                       $("#PromoCodeMessageSection").hide();

                       if ($("#btnPromoCode").html() == 'Validate') {
                           if ($("#txtPromotionalCode").val() === "") {
                               $("#lblPromoCodeMessage").text("Enter a valid promotional code");
                               $("#PromoCodeMessageSection").show(300);
                               //alertDialog("Enter a valid promotional code");
                               return false;
                           }

                           try {

                               loading(true, 'Please wait ...');

                               checkoutObj.ValidatePromoCode($("#txtPromotionalCode").val());

                               if ((checkoutObj.PromoList().length === 0) || (checkoutObj.PromoList() === null) || (checkoutObj.PromoList() === undefined)) {
                                   $("#lblPromoCodeMessage").text("Invalid promotional code");
                                   $("#PromoCodeMessageSection").show(300);
                                   //alertDialog('Invalid promotional code');
                               }
                               else {
                                   var today = new Date();

                                   var DateFrom = GetUniversalDate(formatDateJson(checkoutObj.PromoList()[0].ppStartDate(), '/'));
                                   var DateTo = GetUniversalDate(formatDateJson(checkoutObj.PromoList()[0].ppEndDate(), '/'));
                                   var CurrentDate = GetUniversalDate(today);

                                   if (CurrentDate < DateFrom) {
                                       //alertDialog('Invalid promotional code');
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else if (CurrentDate > DateTo) {
                                       //alertDialog('Promotional code has expired');
                                       $("#lblPromoCodeMessage").text("Invalid promotional code");
                                       $("#PromoCodeMessageSection").show(300);
                                   } else {
                                       if (checkoutObj.AddedRoomList() != null) {
                                           if (checkoutObj.AddedRoomList().length > 0) {
                                               $.each(checkoutObj.AddedRoomList(), function (i, item) {
                                                   checkoutObj.GetPromoCodeInfoXRate($("#txtPromotionalCode").val(), item.RateCode())
                                                   if (checkoutObj.PromoCodeDTO() != null) {
                                                       if (checkoutObj.PromoCodeDTO().ppAmount() > 0) {
                                                           //-- Load Summary info
                                                           LoadSummaryInfo();
                                                       }
                                                   }
                                               });

                                               //--Save promo code info in cookie
                                               checkoutObj.SavePromoCodeCookie();
                                               //--

                                               //alertDialog('Promotional code was applied');
                                               $("#lblPromoCodeMessage").text("Promotional code was applied");
                                               $("#lblPromoCodeMessage").css('color', '#65ca65');
                                               $("#PromoCodeMessageSection").show(300);

                                               $("#lblPromoCodeInHeader").text($("#txtPromotionalCode").val());
                                               $("#PromoCodeInHeaderSection").show(300);

                                               setTimeout(function () {
                                                   $('#dialogPromoCodePopup').modal("hide");
                                               }, 2500);
                                               
                                           }
                                       }
                                   }
                               }

                               loading(false);
                           }

                           catch (ex) {
                               alertDialog(ex);
                               loading(false);
                           }
                       } else {
                           try {

                               loading(true, 'Please wait ...');

                               $("#txtPromotionalCode").val('');
                               $('#txtPromotionalCode').prop('readonly', false);
                               $("#btnPromoCode").html('Validate');
                               $("#PromoApprovedIcon").hide();
                               $("#PromoCodeInHeaderSection").hide();
                               $('#dialogPromoCodePopup').modal("hide");

                               $(".SummaryPromoCode").hide();
                               checkoutObj.PromoCodeDTO(null);

                               //-- Load Summary info
                               LoadSummaryInfo();

                               deleteCookie("PromoCodeInfo");

                               loading(false);
                           }

                           catch (ex) {
                               alertDialog(ex);
                               loading(false);
                           }
                       }

                   });                   

                   //-- Validate fields in form
                   FormFieldsValidationSubmit(checkoutObj, ExcludeCardValidation);

                   //-- Load Summary info
                   LoadSummaryInfo();

                   //-- Save current reservation info in cookie
                   checkoutObj.SaveReservationCookieOnly();                  

                   //-- Check if user has an agency assigned
                   if ($("#hdf_UserAgencyCode").val() != '' && $("#hdf_UserAgencyCode").val() != '0') {
                       checkoutObj.GetAgencyInfo($("#hdf_UserAgencyCode").val(), checkoutObj.CompanyCode());
                       if (checkoutObj.AgencyDTO() != null) {
                           if (checkoutObj.AgencyDTO().ppAgencyCode() != null) {
                               if ($("#hdf_UserName").val() != null && $("#hdf_UserName").val() != '') {
                                   var userfirstname = '', userlastname = '';
                                   var valuesarray = $("#hdf_UserName").val().split(" ");
                                   if (valuesarray.length > 1) {
                                       userfirstname = $.trim(valuesarray[0]);
                                       userlastname = $.trim(valuesarray[1]);
                                   }
                               }

                               $("#firstname").val(userfirstname);
                               $("#lastname").val(userlastname);
                               $('#ddl_country').val(checkoutObj.AgencyDTO().ppCountry());
                               $("#phone").val(checkoutObj.AgencyDTO().ppPhone());
                               $("#email").val(checkoutObj.AgencyDTO().ppNotificationEmailReservations());
                               $("#cardname").val(checkoutObj.AgencyDTO().ppContactName());

                               var carttypetext = checkoutObj.AgencyDTO().TipoTarjeta();
                               $("#ddl_card_type option").filter(function () {
                                   return this.text.toLowerCase() == carttypetext.toLowerCase();
                               }).attr('selected', true);

                               //$('#ddl_card_type').val(checkoutObj.AgencyDTO().TipoTarjeta());

                               $("#cardnumber").val(checkoutObj.AgencyDTO().NumTarjeta());

                               var expmonth = '', expyear = '', expiredate;
                               expiredate = checkoutObj.AgencyDTO().FecVencimiento();
                               if (expiredate != null && expiredate != '') {
                                   expiredate = expiredate.replace("_", "/").replace("-", "/");
                                   var valuesarray = expiredate.split("/");
                                   if (valuesarray.length > 1) {
                                       expmonth = $.trim(valuesarray[0]);
                                       expyear = $.trim(valuesarray[1]);
                                   }
                               }

                               $("#ddl_expiration_month").val(expmonth);
                               $("#ddl_expiration_year").val(expyear);
                               $("#txtCVC").val(checkoutObj.AgencyDTO().CodSeguridad());
                               $("#acceptTerms").prop("checked", true);

                               if (checkoutObj.AgencyDTO().ppAvailableCredit()) {
                                   $("#checkout_form").trigger("submit");
                               }
                           }                                                                       
                       }
                   }
                   //--

                   //-- Pay Now button from terms and conditions popup
                   $("#btnPayNow").click(function () {
                       try
                       {                                                     
                           if ($('#acceptTerms').prop('checked')) {
                               $('#lblTermsWarning').hide();
                               $('#dialogTermConditionsPopup').modal('hide'); //-- Hide/close terms and conditions modal
                               loading(true, 'Please wait ...');
                               checkoutObj.SaveReservation();
                               loading(false);
                           } else {
                               $('#lblTermsWarning').show();
                           }
                                                     
                       } catch (ex)
                       {
                           loading(false);
                       }                       
                   })

                   $('#acceptTerms').change(function () {
                       if ($(this).prop('checked')) {
                           $('#lblTermsWarning').hide();
                       } else {
                           $('#lblTermsWarning').show();
                       }                       
                   });

                   $('#dialogTermConditionsPopup').on('hidden.bs.modal', function () {
                       $('#pay_now_button').prop('disabled', false);
                   })
                   //--

                   loading(false);

               }
               catch (ex) {
                   alertDialog("Error loading summary information.");
                   loading(false);
               }

               //-- MouseOver and MouseOut of add-ons added
               $(".addon_table_row").mouseover(function () {
                   var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
                   currentURL = currentURL.replace("success.png", "delete.png")
                   $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
               });

               $(".addon_table_row").mouseout(function () {
                   var currentURL = $(this).find(".addon_button").find(".addon_button_image").attr("src");
                   currentURL = currentURL.replace("delete.png", "success.png")
                   $(this).find(".addon_button").find(".addon_button_image").attr("src", currentURL);
               });
               //--

               //-- Delete add-on from list
               $(".addon_button_image").click(function () {
                   var UniqueIdAddOnToDelete = $(this).closest('tr').attr("id")
                   $(this).closest('tr')
                   .children('td')
                   .animate({ padding: 0 })
                   .wrapInner('<div />')
                   .children()
                   .slideUp(function () { $(this).closest('tr').remove(); });

                   //-- Delete add-on and insert the new add-on list to the cookie
                   deleteAddonAndUpdateAddOnsCookie(UniqueIdAddOnToDelete);
                   //--
               });
               //

              
           }
);
//*************************************//


function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    //$("#dialog").show();
    //$("#dialog").dialog("open");
    $("#dialog_msg").modal({ backdrop: "static" });
    $('#dialog_msg').modal('show');
}

function TermAndConditionsDialog(Content) {
    $('#lblTermsWarning').hide();
    $("#termconditions").html(Content);   
    $("#dialogTermConditionsPopup").modal({ backdrop: "static" });
    $('#dialogTermConditionsPopup').modal('show');
}

//******************* Loading dialog ****************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function formatDateToShowLabel(date) {
    var returnedvalue;    
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    var day = '' + date.getDate();
    var year = date.getFullYear();
    
    if (day.length < 2) day = '0' + day;

    var monthname = months[date.getMonth()];    

    returnedvalue = day + '-' + monthname + '-' + year;

    return returnedvalue;
}

function FormFieldsValidationSubmit(coObj, ExcludeCardValidation)
{
    //-- Form fields validations
    $('#checkout_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            lastname: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },            
            cardname: {
                excluded: ExcludeCardValidation,
                validators: {
                    stringLength: {
                            min: 8,
                            },
                    notEmpty: {
                            message: 'Please supply your card name'
                    }
                }
            },
            ddl_card_type: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'The type is required'
                    }
                }
            },
            cardnumber: {
                excluded: ExcludeCardValidation,
                validators: {
                        creditCard: {
                                message: 'The card number is not valid'
                        },
                        notEmpty: {
                                message: 'Please supply your card number'
                        }
                }
            },
            expiration_month_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    }
                }
            },
            expiration_year_select: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration year'
                    }
                }
            },
            txtCVC: {
                excluded: ExcludeCardValidation,
                validators: {
                    notEmpty: {
                        message: 'Please select the expiration month'
                    },
                    cvv: {
                        creditCardField: 'cardnumber',
                        message: 'The CVV number is not valid'
                    }
                }
            }
            //,
            //acceptTerms: {
            //    validators: {
            //        notEmpty: {
            //            message: 'You have to accept General Terms and Conditions'
            //        }
            //    }
            //}
        }
    })    
    .on('success.form.bv', function (e) {
        //$('#success_message').slideDown({ opacity: "show" }, "slow") //

        // Prevent form submission
        e.preventDefault();       

        // Get the form instance
        //var $form = $(e.target);

        // Get the BootstrapValidator instance
        //var bv = $form.data('bootstrapValidator');       

        TermAndConditionsDialog(coObj.CompanyTermsAndConditions());        

        // Use Ajax to submit form data
        //$.post($form.attr('action'), $form.serialize(), function (result) {
        //    console.log(result);
        //}, 'json');
    });

}

function LoadSummaryInfo() {
    if (checkoutObj.AddedRoomList() != null) {
        if (checkoutObj.AddedRoomList().length > 0) {
            var SubTotalRoomNights = 0;
            var SubTotalRoomNightsWithDiscount = 0;
            var tax = 0;
            var TOTAL = 0;
            var Totaltax = 0;
            var oneDay = 24 * 60 * 60 * 1000;
            var applyPromoCode = false;

            //-- Number of nights
            var fromDate = new Date(checkoutObj.AddedRoomList()[0].fromDt().replace(/-/g, "/"));
            var toDate = new Date(checkoutObj.AddedRoomList()[0].toDt().replace(/-/g, "/"));
            var diffDays = Math.round(Math.abs((fromDate.getTime() - toDate.getTime()) / (oneDay)));
            //--

            //-- Iterate rooms
            var guestsQty = 0;
            $.each(checkoutObj.AddedRoomList(), function (i, item) {
                //-- Quantity of guests
                guestsQty += (parseInt(checkoutObj.AddedRoomList()[i].Adults()) +
                           parseInt(checkoutObj.AddedRoomList()[i].Kids()) +
                           parseInt(checkoutObj.AddedRoomList()[i].Infants()));                            

                
                //-- Sum room price
                SubTotalRoomNights += (parseFloat(checkoutObj.AddedRoomList()[i].Price()) * diffDays);                
                SubTotalRoomNightsWithDiscount += (parseFloat(checkoutObj.AddedRoomList()[i].PriceDisc()) * diffDays);
            })           
            //--

            //-- Put info in summary labels
            var fromDateLabel = formatDateToShowLabel(fromDate);
            var toDateLabel = formatDateToShowLabel(toDate);

            $("#lbArrivalDate").html(fromDateLabel);
            $("#lbDepartureDate").html(toDateLabel);
            $("#lbRoomNights").html(diffDays);
            $("#lbRoomsQty").html(checkoutObj.AddedRoomList().length);

            //$("#lbGuestsQty").html(guestsQty);
            //$("#lbRateSelected").html('$' + checkoutObj.AddedRoomList()[0].Price());
            //SubTotalRoomNights = (parseFloat(checkoutObj.AddedRoomList()[0].Price()) * diffDays);
            //$("#lbRoomTypeDesc").html(checkoutObj.AddedRoomList()[0].Desc());

            $("#lbSubTotalRoomNights").html('$' + parseFloat(SubTotalRoomNights).toFixed(2));
            //--

            //Price with discount            
            //if (checkoutObj.AddedRoomList()[0].Discount() > 0) {
            //    $("#lbRateSelectedDisc").html('$' + parseFloat(checkoutObj.AddedRoomList()[0].PriceDisc()).toFixed(2));
            //    $("#lbRateSelected").addClass("Discount_item_summary");
            //}
            //else {
            //    $("#divDiscount").hide();
            //}

            //-- Take the discount of the first room, because it is the same discount for all the rooms (same room type)
            $("#lbDiscount").html(checkoutObj.AddedRoomList()[0].Discount() + ' %');
            Discount = SubTotalRoomNights * (checkoutObj.AddedRoomList()[0].Discount() / 100);
            //

            //-- Take the tax of the first room, because it is the same tax for all the rooms
            tax = checkoutObj.AddedRoomList()[0].TaxPerc();
            //

            //-- Addons
            var SubTotalAddOns = 0;           
            if (checkoutObj.TempAddedAddonList != null) {
                if (checkoutObj.TempAddedAddonList().length > 0) {
                    $.each(checkoutObj.TempAddedAddonList(), function (i, item) {
                        SubTotalAddOns += parseFloat(item.Price());                        
                    })
                }
            }

            $("#lbSubTotalAddOns").html('$' + SubTotalAddOns);
            //--

            //-- Totals
            //if (checkoutObj.AddedRoomList()[0].Discount() > 0) {
            //    TOTAL =  ((parseFloat(checkoutObj.AddedRoomList()[0].PriceDisc()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2);
            //}
            //else { TOTAL =  ((parseFloat(checkoutObj.AddedRoomList()[0].Price()) * diffDays) + parseFloat(SubTotalAddOns)).toFixed(2); }


            //-- Verify Promo code info
            if (checkoutObj.PromoCodeDTO() != null) {
                if (checkoutObj.PromoCodeDTO().ppPromoCode() != null && checkoutObj.PromoCodeDTO().ppPromoCode() != '') {
                    applyPromoCode = true;

                    $("#txtPromotionalCode").val(checkoutObj.PromoCodeDTO().ppPromoCode());
                    $('#txtPromotionalCode').prop('readonly', true);
                    $("#btnPromoCode").html('Revert');
                    $("#PromoApprovedIcon").show(300);

                    $("#lblPromoCodeInHeader").text(checkoutObj.PromoCodeDTO().ppPromoCode());
                    $("#PromoCodeInHeaderSection").show(300);

                    $("#lbPromoCodeSummary").text(checkoutObj.PromoCodeDTO().ppPromoCode());

                    if (checkoutObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        $("#lbPromoAmountSummary").text("- " + checkoutObj.PromoCodeDTO().ppAmount() + '%');
                    } else {
                        $("#lbPromoAmountSummary").text('- $' + checkoutObj.PromoCodeDTO().ppAmount());
                    }

                    $(".SummaryPromoCode").show();

                } else {
                    $(".SummaryPromoCode").hide();
                }
            } else {
                $(".SummaryPromoCode").hide();
            }

            //--
            if (checkoutObj.AddedRoomList()[0].Discount() > 0) {
                TOTAL = (SubTotalRoomNightsWithDiscount + parseFloat(SubTotalAddOns)).toFixed(2);
            }
            else { TOTAL = (SubTotalRoomNights  + parseFloat(SubTotalAddOns)).toFixed(2); }

            if (tax> 0)
            {
                Totaltax = parseFloat(TOTAL * (tax / 100));

                TOTAL = parseFloat(TOTAL) + Totaltax;
            }

            //-- Check if we need to apply promo code discount
            if (applyPromoCode) {
                if (checkoutObj.PromoCodeDTO().ppAmount() > 0) {
                    if (checkoutObj.PromoCodeDTO().ppType().toUpperCase() == 'P') {
                        TOTAL = TOTAL - (TOTAL * (checkoutObj.PromoCodeDTO().ppAmount() / 100));
                    } else {
                        if (TOTAL > checkoutObj.PromoCodeDTO().ppAmount()) {
                            TOTAL = TOTAL - checkoutObj.PromoCodeDTO().ppAmount();
                        } else {
                            TOTAL = 0;
                        }
                    }
                }
            }
            //--
            
            $("#Subtotal").val(parseFloat(SubTotalRoomNights));
            $("#totalAmount").val(parseFloat(TOTAL));
            $("#discount").val(parseFloat(Discount));
            $("#addonAmount").val(parseFloat(SubTotalAddOns));
            $("#taxAmount").val(parseFloat(Totaltax));

            $("#lbTOTAL").html('$' + parseFloat(TOTAL).toFixed(2)); //TOTAL label
            $("#lbTaxes").html('$' + parseFloat(Totaltax).toFixed(2)); //Tax label
            //--
        }
    }
}

function deleteAddonAndUpdateAddOnsCookie(UniqueIdAddOnToDelete) {
    var AddOnListInTable = [];
    checkoutObj.TempAddedAddonList([]);
    $("#AddOnsTableInSummary").find('tr').each(function (i, el) {
        var Id = $(this).attr("id");

        if (UniqueIdAddOnToDelete != Id) {
            var Code = $(this).attr("data-target");
            var Desc = $(this).find(".addon_summary_item_desc").html();
            var Price = $(this).find(".addon_summary_item_price").html();
            var PriceNumber;

            Price = $.trim(Price.replace("$", ""));
            if ($.isNumeric(Price)) {
                PriceNumber = parseFloat(Price);
            }
            checkoutObj.PrepareAddOnDtoForInsert(Code, PriceNumber, Desc);
            AddOnListInTable.push(checkoutObj.AddonLite());

            checkoutObj.TempAddedAddonList.push(checkoutObj.AddonLite());
        }
    });

    if (AddOnListInTable != null) {
        checkoutObj.SaveAddon(AddOnListInTable); //Store add-on list updated in cookie       
    }

    LoadSummaryInfo();
}

//Delete Cookie
function deleteCookie(name) {
    document.cookie = name + '=;-1; path=/';
}

//-- Generates a guid
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}
//--