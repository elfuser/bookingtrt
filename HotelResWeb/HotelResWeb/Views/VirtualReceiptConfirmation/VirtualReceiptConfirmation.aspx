﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/VirtualReceipt.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="CheckoutTitle" ContentPlaceHolderID="TitleContent" runat="server">
Confirmation
</asp:Content>

<asp:Content ID="CheckoutFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">        
          <p>CONFIRMATION</p>
    </section>
</asp:Content>   

<asp:Content ID="RoomsSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server">
   <div id="summarysection" class="summarysectionstyle">
      <div class="summarybox"> <!-- left position -->
            <!-- Summary content -->    
            <div class="summarytitle">
                SUMMARY
            </div>
            <ul id="summarysection-menu"> 
                <li>
                    <div class="left_item_summary">
                        Client:
                    </div>    
                    <div class="right_item_summary">                        
                        <label id="lbClientName"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Description:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbDescription"> -- </label>
                    </div>
                </li>
                <li>
                    <div>
                      <hr style="border-top: 1px solid #f5a623;"/>
                    </div>
                </li> 
                <li>
                   <div class="left_item_summary item">
                        <label class="SummaryTotalLabel"> TOTAL </label>
                    </div>  
                    <div class="right_item_summary item">
                        <label class="SummaryTotalLabel" id="lbAmount"> -- </label>
                    </div> 
                </li>                                                                                     
            </ul>                                                                   
      </div>
  </div>              
</asp:Content>  

<asp:Content ID="CheckoutContent" ContentPlaceHolderID="MainContent" runat="server">
<input id="hdf_ReceiptCode"  value="<%: ViewBag.ReceiptCode%>" type="hidden"/>
<input id="hdf_CompanyCode"  value="<%: ViewBag.CompanyCode%>" type="hidden"/>
<input id="txtTrackingURLBase"  value="<%: Url.Action("VirtualReceiptConfirmation").ToLower().Replace("/virtualreceiptconfirmation/virtualreceiptconfirmation", "").Replace("/virtualreceiptconfirmation/hotelcode", "")%>" type="hidden"/>

<div id="panels">    
  <div id="checkoutcontent">
      <div class="contenttitle">


          <%: ViewBag.Message%>

               
      </div>
       <div class="confirmationmessage">
             <%: ViewBag.subtitle%>
    </div>     
  </div>    
</div>

<div id="dialog" title="Alert" style="display:none;">  
     <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
</div>
    <table   >

        <tr>
            <td >
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/VirtualReceiptConfirmation") %>         
    <%: Styles.Render("~/Content/VirtualReceiptConfirmationCss") %>        
</asp:Content>
