﻿<%@ Page Language="VB" Inherits="System.Web.Mvc.ViewPage" %>

<!-- Modal -->
<div id="dialogPaymentPopup" class="modal fade col-sm-12" style="display:none;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="col-sm-4 paymentpopup_leftsection">
              <div class="row leftsection_rowup">
                <div class="col-sm-12 to_pay_label">
                    To pay 
                    <br />
                    <label id="lbTotalToPay"></label>
                </div>            
            </div>
            <div class="row leftsection_rowdown">
                <div class="col-sm-12">               
                   <a class="cancel_your_payment" onclick="return cancel_your_payment();"> Cancel your payment </a>               
                </div>            
            </div>  
            </div>
            <div class="col-sm-8">        
                <%--<div class="row rightsection_row">
                    <div class="col-sm-12 Log_In">
                        <a href="<%=ResolveUrl("~/LogIn/LogIn") %>"> Log In </a>
                        <br />
                    </div>            
                </div>--%>
                <div class="row rightsection_row">
                    <div class="col-sm-12">
                        <hr />
                    </div>            
                </div>          
                <div class="row rightsection_row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-default payment_popup_button" id="enhance_visit_button" data-bind="click: enhance_visit_button">ENHANCE YOUR VISIT</button> 
                    </div>            
                </div>
                <div class="row rightsection_row">
                    <div class="col-sm-12">
                        or
                    </div>            
                </div> 
                <div class="row rightsection_row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-default payment_popup_button" id="checkout_button" data-bind="click: checkout_button">CHECK OUT</button> 
                    </div>            
                </div> 
                <div class="row rightsection_row">
                    <div class="col-sm-12 close_button_div">
                        <button id="btn_close_popup" class="btn" data-bind="click: btn_close_popup">Close</button> 
                    </div>            
                </div>       
            </div>
        </div>       
    </div>    
</div>
