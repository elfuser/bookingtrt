﻿<%@ Page Language="VB" Inherits="System.Web.Mvc.ViewPage" %>

<!-- Modal -->
<div class="modal fade" id="dialogTermConditionsPopup" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">        
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="modal-title">Terms And Conditions</h4>
                </div>                
            </div>
            <br />
            <div class="row">
                <div class="col-md-12">
                    <div id="termconditions">
                    </div>
                </div>                
            </div>            
            <hr />     
            <div class="row">                    
                <div class="col-sm-12 AcceptConditions">
                   <input type="checkbox" name="acceptTerms" id="acceptTerms" /> I accept the General Terms and Conditions 
                   <label id="lblTermsWarning" style="display:none;">You have to accept General Terms and Conditions</label>            
                </div>                                          
            </div>
            <br />            
            <div class="row">                
                 <div class="paynowmessage col-md-12">
                    <p>Your card will be billed for the total amount when press PAY NOW, please wait a few seconds until receive the response of the bank, do NOT press back or stop</p>
                 </div>                                             
            </div>
            <br />                      
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-default pay_now" id="btnPayNow">PAY NOW</button>                       
                </div>               
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
