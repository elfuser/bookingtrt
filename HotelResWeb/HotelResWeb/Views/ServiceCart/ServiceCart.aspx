﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Tours.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="ServiceDetailTitle" ContentPlaceHolderID="TitleContent" runat="server">
Service Detail
</asp:Content>

<asp:Content ID="ToursServicesLeftPanelContent" ContentPlaceHolderID="LeftPanelContent" runat="server" Visible="false">
   
</asp:Content>

<asp:Content ID="ToursServicesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>    
    <input id="txtTrackingURLBase"  value="<%: Url.Action("ServiceCart").ToLower().Replace("/servicecart/servicecart", "").Replace("/servicecart/hotelcode", "")%>" type="hidden"/>
    
     <div id="checkoutcontent"   <%--data-bind="with: ReservationDTO()"--%>>
      <form id="checkout_form"  method="post" class="form-horizontal">
          <div class="row contenttitle">
                Personal Information
          </div>
          <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="firstname">First Name:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter first name" size="100"  <%--data-bind="value: FName"--%>>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="lastname">Last Name:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter last name" size="100" <%--data-bind="value: LName"--%>>
                    </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email Address:</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" size="50"  <%--data-bind="value: EMail"--%>>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Confirm Address:</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="confirmemail" name="confirmemail" placeholder="Confirm email" size="50"  <%--data-bind="value: ConfirmEMail"--%>>
                    </div>
                  </div>
              </div>              
          </div>
          <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="phone">Phone:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" size="50"  <%--data-bind="value: Phone"--%>>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="ddl_country">Country:</label>
                    <div class="col-sm-8">
                        <select name="ddl_country" class="selectpicker form-control" id="ddl_country" data-live-search="true">                                               
                         </select>         
                    </div>
                  </div>
              </div>
          </div>
          <div class="row contenttitle cardinfosection">
                Payment Information
          </div>
          <div class="row cardinfosection">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield" for="ddl_card_type">Card type:</label>            
                    <div class="col-sm-8">                
                      <select name="ddl_card_type" class="form-control" id="ddl_card_type" >
                        <option value="">Select a type</option>                    
                      </select>
                    </div>            
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield" for="cardnumber">Credit card number</label>            
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="cardnumber" name="cardnumber"  <%--data-bind="value: CardNumber"--%>  placeholder="Enter credit card number" size="50">            
                    </div>            
                  </div>
              </div>
          </div>
          <div class="row cardinfosection">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield" for="cardname">Name on the card:</label>            
                    <div class="col-sm-8">                
                      <input type="text" class="form-control" id="cardname" name="cardname" <%--data-bind="value: NameCard"--%>  placeholder="Enter name on the card" size="50">            
                    </div>            
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield" for="txtCVC">CVC/CVV</label>            
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="txtCVC" name="txtCVC" placeholder="" size="10"  >            
                    </div>            
                  </div>
              </div>
          </div>
          <div class="row cardinfosection">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield" >Expiration Month</label>             
                    <div class="col-sm-8">                
                      <select name="expiration_month_select" class="form-control" id="ddl_expiration_month"  <%--data-bind="value: ExpeMonth"--%> >
                                <option value ="01">January</option>
                                <option  value ="02">February</option>
                                <option  value ="03">March</option>
                                <option  value ="04">April</option>
                                <option  value ="05">May</option>
                                <option  value ="06">June</option>
                                <option  value ="07">July</option>
                                <option  value ="08">August</option>
                                <option  value ="09">September</option>
                                <option  value ="10">October</option>
                                <option  value ="11">November</option>
                                <option  value ="12">December</option>
                       </select> 
                    </div>            
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label col-sm-4 paymentinfofield">Year</label>             
                    <div class="col-sm-8">                
                      <select name="expiration_year_select" class="form-control" id="ddl_expiration_year"  <%--data-bind="value: ExpeYear"--%> >                                                        
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>     
                                <option>2023</option>     
                                 <option>2024</option>       
                                 <option>2025</option>             
                       </select>  
                    </div>            
                  </div>
              </div>             
          </div>
          <%--<div class="row contenttitle">              
                  Terms and Conditions              
          </div>
          <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">            
                    <div class="col-sm-12 AcceptConditions">
                       <input type="checkbox" name="acceptTerms" /> Yes, I have read and I agree to the booking conditions and waiver.            
                    </div>            
                  </div>
              </div>              
          </div>--%>
          <br />
          <div>
               <button type="submit" <%--data-bind="click: $parent.SubmitClick" --%>class="btn btn-default" id="pay_now_button">PAY NOW</button>                       
          </div>
          <input type="hidden" id="taxAmount" />          
          <input type="hidden" id="discount" />
          <input type="hidden" id="totalAmount" />
          <input type="hidden" id="Subtotal" />                    
      </form>
      
  </div>


        <form id="dataForm" method="post"  action="https://vpayment.verifika.com/VPOS/MM/transactionStart20.do">
            <div id=""  data-bind="with: BNReqModel()">
            <input name="IDACQUIRER" id="IDACQUIRER" type="hidden"  data-bind="value: mIDACQUIRER" />
            <input name="IDCOMMERCE" id="IDCOMMERCE" type="hidden"  data-bind="value: mIDCOMMERCE" />
            <input name="XMLREQ" id="XMLREQ" type="hidden"  data-bind="value: mXMLREQ" />
            <input name="DIGITALSIGN" id="DIGITALSIGN" type="hidden"  data-bind="value: mDIGITALSIGN" />
            <input name="SESSIONKEY" id="SESSIONKEY" type="hidden"  data-bind="value: mSESSIONKEY" />
            </div>
        </form>


    <form id="PostForm" name="PostForm" action="" method="POST">
             <div id=""   data-bind="with: CredomaticReqModel()">
                <input type="hidden" name="username"  data-bind="value: username"  >
                <input type="hidden" name="time"  data-bind="value: time" >
                <input type="hidden" name="processor_id"  data-bind="value: processor_id" >
                <input type="hidden" name="hash"  data-bind="value: hash" >
                <input type="hidden" name="key_id"  data-bind="value: key_id" >
                <input type="hidden" name="ccexp"  data-bind="value: ccexp" >
                <input type="hidden" name="orderid"  data-bind="value: orderid" >
                <input type="hidden" name="amount"  data-bind="value: amount" >
                <input type="hidden" name="type"  data-bind="value: type" >
                <input type="hidden" name="ccnumber"  data-bind="value: ccnumber" >
                <input type="hidden" name="redirect"  data-bind="value: redirect" >
          </div>
    </form>


 <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</asp:Content>

<asp:Content ID="ServiceDetailRightPanelContent" ContentPlaceHolderID="RightPanelContent" runat="server">         
   <div id="SummarySection" class="row">
            <hr class="division_line_style" />
           <div class="CartSummarySectionTitle">
               <label id="lblCartSummary">CART SUMMARY</label>
           </div>          
           <div id="ServicesTableInSummary">
               <table style="width: 100%;">
                   <tbody data-bind="foreach: $root.InsertedServicesList()">                   
                   <tr class="service_table_row" data-bind="attr: { 'id': UniqueId() }">
                       <td>
                           <table style="width: 100%;" >
                               <tbody >
                                   <tr>
                                        <td colspan="2">
                                            <hr class="division_line_style_v2" />
                                        </td>
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceTitle" data-bind="text: ppServiceName">
                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Date: <span data-bind="text: ppFormattedArrivalDate"></span>, Hour: <span data-bind="text: ppScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHidePickupSection">
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Pickup Place: <span data-bind="text: ppPickupPlaceDesc"></span>, Hour: <span data-bind="    text: ppPickupScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>                     
                                   <tr data-bind="css: ShowHideAdult">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppAdults"></span> Adult(s) = <span id="spantotalperadults" data-bind="    attr: { 'id': 'spantotalperadults_' + UniqueId() }, text: ppTotalPerAdults"></span> <span id="spantotalperadultspromo" data-bind="    attr: { 'id': 'spantotalperadultspromo_' + UniqueId() }, text: ppTotalPerAdultsPromo"></span>                                      
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideChildren">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppChildren"></span> Children = <span id="spantotalperchildren" data-bind="    attr: { 'id': 'spantotalperchildren_' + UniqueId() }, text: ppTotalPerChildren"></span> <span id="spantotalperachildrenpromo" data-bind="    attr: { 'id': 'spantotalperachildrenpromo_' + UniqueId() }, text: ppTotalPerChildrenPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideInfants">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppInfants"></span> Infant(s) = <span id="spantotalperinfants" data-bind="    attr: { 'id': 'spantotalperinfants_' + UniqueId() }, text: ppTotalPerInfants"></span> <span id="spantotalperinfantspromo" data-bind="    attr: { 'id': 'spantotalperinfantspromo_' + UniqueId() }, text: ppTotalPerInfantsPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideStudents">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppStudents"></span> Student(s) = <span id="spantotalperstudents" data-bind="    attr: { 'id': 'spantotalperstudents_' + UniqueId() }, text: ppTotalPerStudents"></span> <span id="spantotalperstudentspromo" data-bind="    attr: { 'id': 'spantotalperstudentspromo_' + UniqueId() }, text: ppTotalPerStudentsPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceDetailsRowRight">                                              
                                             <label class="RemoveServiceLink">Remove</label>                                  
                                        </td>                                                                                                             
                                   </tr>
                               </tbody>   
                           </table>
                       </td>
                   </tr>
                   </tbody>  
               </table>               
           </div>
           <div>
               <table style="width: 100%;">
                   <tr>
                       <td>
                           <hr class="division_line_style" />
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Subtotal: </span><label id="lblSubTotal" ></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Tax: </span><label id="lblTax"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Rent: </span><label id="lblRent"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRightTotal">
                           <span>Total US$: </span><label id="lblTOTAL"></label> 
                       </td>
                   </tr>
               </table>
           </div>           
   </div>
   <div id="NoServicesSection">
       <label id="lblNoServicesInCart">There are no services in cart.</label>

       <hr class="division_line_style" />
       <div class="row ServiceAddedButtonSectionStyle">
           <button class="btn btn-default ServiceAddedButton" id="btnContinueShopping" data-bind="click: ContinueShopping_Button">Continue shopping</button>    
       </div>
   </div>
   

    <% Html.RenderPartial("TermConditionsPopup")%> 
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">                  
    <%: Scripts.Render("~/bundles/ServiceCart") %>         
    <%: Styles.Render("~/Content/ServiceCartCss") %>        
</asp:Content>