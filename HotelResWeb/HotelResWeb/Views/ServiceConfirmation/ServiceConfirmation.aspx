﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Tours.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="ServiceDetailTitle" ContentPlaceHolderID="TitleContent" runat="server">
Service Detail
</asp:Content>

<asp:Content ID="ToursServicesLeftPanelContent" ContentPlaceHolderID="LeftPanelContent" runat="server" Visible="false">
   
</asp:Content>

<asp:Content ID="ToursServicesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>    
    <input id="txtTrackingURLBase"  value="<%: Url.Action("ServiceConfirmation").ToLower().Replace("/serviceconfirmation/serviceconfirmation", "").Replace("/serviceconfirmation/hotelcode", "")%>" type="hidden"/>      

    <div id="checkoutcontent">
      <div class="contenttitle">


          <%: ViewBag.Message%>

               
      </div>
       <div class="confirmationmessage">
             <%: ViewBag.subtitle%>
        </div>          
  </div> 
 <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</asp:Content>

<asp:Content ID="ServiceDetailRightPanelContent" ContentPlaceHolderID="RightPanelContent" runat="server">         
   <div id="SummarySection" class="row">
            <hr class="division_line_style" />
           <div class="CartSummarySectionTitle">
               <label id="lblCartSummary">CART SUMMARY</label>
           </div>          
           <div id="ServicesTableInSummary">
               <table style="width: 100%;">
                   <tbody data-bind="foreach: $root.InsertedServicesList()">                   
                   <tr class="service_table_row" data-bind="attr: { 'id': UniqueId() }">
                       <td>
                           <table style="width: 100%;" >
                               <tbody >
                                   <tr>
                                        <td colspan="2">
                                            <hr class="division_line_style_v2" />
                                        </td>
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceTitle" data-bind="text: ppServiceName">
                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Date: <span data-bind="text: ppFormattedArrivalDate"></span>, Hour: <span data-bind="text: ppScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHidePickupSection">
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Pickup Place: <span data-bind="text: ppPickupPlaceDesc"></span>, Hour: <span data-bind="    text: ppPickupScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>                      
                                   <tr data-bind="css: ShowHideAdult">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppAdults"></span> Adult(s) = <span id="spantotalperadults" data-bind="    attr: { 'id': 'spantotalperadults_' + UniqueId() }, text: ppTotalPerAdults"></span> <span id="spantotalperadultspromo" data-bind="    attr: { 'id': 'spantotalperadultspromo_' + UniqueId() }, text: ppTotalPerAdultsPromo"></span>                                      
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideChildren">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppChildren"></span> Children = <span id="spantotalperchildren" data-bind="    attr: { 'id': 'spantotalperchildren_' + UniqueId() }, text: ppTotalPerChildren"></span> <span id="spantotalperachildrenpromo" data-bind="    attr: { 'id': 'spantotalperachildrenpromo_' + UniqueId() }, text: ppTotalPerChildrenPromo"></span>
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideInfants">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppInfants"></span> Infant(s) = <span id="spantotalperinfants" data-bind="    attr: { 'id': 'spantotalperinfants_' + UniqueId() }, text: ppTotalPerInfants"></span> <span id="spantotalperinfantspromo" data-bind="    attr: { 'id': 'spantotalperinfantspromo_' + UniqueId() }, text: ppTotalPerInfantsPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideStudents">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppStudents"></span> Student(s) = <span id="spantotalperstudents" data-bind="    attr: { 'id': 'spantotalperstudents_' + UniqueId() }, text: ppTotalPerStudents"></span> <span id="spantotalperstudentspromo" data-bind="    attr: { 'id': 'spantotalperstudentspromo_' + UniqueId() }, text: ppTotalPerStudentsPromo"></span> 
                                        </td>                                                                                                             
                                   </tr>                                   
                               </tbody>   
                           </table>
                       </td>
                   </tr>
                   </tbody>  
               </table>               
           </div>
           <div>
               <table style="width: 100%;">
                   <tr>
                       <td>
                           <hr class="division_line_style" />
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Subtotal: </span><label id="lblSubTotal" ></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Tax: </span><label id="lblTax"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Rent: </span><label id="lblRent"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRightTotal">
                           <span>Total US$: </span><label id="lblTOTAL"></label> 
                       </td>
                   </tr>
               </table>
           </div>           
   </div>
   <div id="NoServicesSection">
       <label id="lblNoServicesInCart">There are no services in cart.</label>

       <hr class="division_line_style" />
       <div class="row ServiceAddedButtonSectionStyle">
           <button class="btn btn-default ServiceAddedButton" id="btnContinueShopping" data-bind="click: ContinueShopping_Button">Continue shopping</button>    
       </div>
   </div>
   
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">                  
    <%: Scripts.Render("~/bundles/ServiceConfirmation") %>         
    <%: Styles.Render("~/Content/ServiceConfirmationCss") %>        
</asp:Content>