﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Tours.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="ServiceDetailTitle" ContentPlaceHolderID="TitleContent" runat="server">
Service Detail
</asp:Content>

<asp:Content ID="ToursServicesLeftPanelContent" ContentPlaceHolderID="LeftPanelContent" runat="server" Visible="false">
   
</asp:Content>

<asp:Content ID="ToursServicesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>
    <input id="hdf_ServiceId"  value="<%: ViewBag.ServiceId%>" type="hidden"/>
    <input id="txtTrackingURLBase"  value="<%: Url.Action("ServiceDetail").ToLower().Replace("/servicedetail/servicedetail", "").Replace("/servicedetail/hotelcode", "")%>" type="hidden"/>
    
    <div class="row SliderRow">
        <%--<img class="TourImageStyle img-responsive" src="" data-bind="attr: { src: ImageUlr }" /> --%>
        <ul class="bxslider" data-bind="foreach: $root.ServiceImageList">
          <li><img src ="" data-bind="attr: { src: $data }" class="img-responsive ServiceImageStyle"/></li>        
        </ul>

    </div>
    <div class="row ServiceDescTitle">
        <label id="lblServiceDescTitle">DESCRIPTION</label>
    </div>
    <div class="row divServiceDetails">
        <div class="col-sm-4" style="width:100%;">
            <div class="row ServiceTitle" data-bind="text: ppDescription">   
                             
            </div>
            <div class="row ServiceDetailsRow" data-bind="text: ppDetails">
                
            </div>
            <div class="row subtitle">
                <label id="lblConditionsTitle">Conditions</label>
            </div>
            <div class="row ServiceDetailsRow" data-bind="text: ppConditions">
                
            </div>
            <div class="row subtitle">
                <label id="lblWhatToBringTitle">What To Bring</label>
            </div>
            <div class="row ServiceDetailsRow" data-bind="text: ppBringInfo">
                
            </div>
        </div>
    </div>  


 <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default CloseDialogButton" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</asp:Content>

<asp:Content ID="ServiceDetailRightPanelContent" ContentPlaceHolderID="RightPanelContent" runat="server">   
   <div id="ServiceDateAndQtySection">       
       <div class="row CalendarSection">       
            <div id="datepicker"></div>          
       </div>
       <div class="row NoServiceAvailable" style="display:none;">
           <label id="lblNoServiceAvailable">Service not available</label>
       </div>
       <div class="row form-group SchedHoursSection" style="display:none;">
           <label class="control-label col-sm-2" for="ddl_schedhours" id="lblHour">Hour:</label>            
           <div class="col-sm-10">                
            <select name="ddl_schedhours" class="form-control" id="ddl_schedhours" >
                <option value="0">Select Hour:</option>                    
            </select>
           </div>
       </div>
       <div class="PriceQtySectionRow">
            <table style="width:100%;">
                <tr style="vertical-align: middle;">
                    <th>
                        <label id="lblGuestsQty">Qty</label>
                    </th>
                     <th>
                        <label id="lblGuestsHeader">Guests</label>
                    </th>
                     <th>
                        <label id="lblPriceHeader">Price $</label>
                    </th>
                </tr>
                <tr style="vertical-align: middle;" id="AdultsRow">
                    <td>
                        <input class="AdultsSpinner"  name="value">
                    </td>
                    <td>
                        <label id="lblAdults">Adults</label>
                    </td>
                    <td>
                        <label id="lblPriceAdults">0.00</label>
                        <div id="divPromoPriceAdults" style="display:none;">
                            &nbsp<label id="lblPromoPriceAdults"></label>
                        </div>                        
                    </td>
                </tr>
                <tr style="vertical-align: middle;" id="ChildrenRow">
                    <td>
                        <input class="ChildrenSpinner"  name="value">
                    </td>
                    <td>
                        <label id="lblChildren">Children</label>
                    </td>
                    <td>
                        <label id="lblPriceChildren">0.00</label>
                        <div id="divPromoPriceChildren" style="display:none;">
                            &nbsp<label id="lblPromoPriceChildren"></label>
                        </div> 
                    </td>
                </tr>
                <tr style="vertical-align: middle;" id="InfantsRow">
                    <td>
                        <input class="InfantsSpinner"  name="value">
                    </td>
                    <td>
                        <label id="lblInfants">Infant(s)</label>
                    </td>
                    <td>
                        <label id="lblPriceInfants">0.00</label>
                        <div id="divPromoPriceInfants" style="display:none;">
                            &nbsp<label id="lblPromoPriceInfants"></label>
                        </div> 
                    </td>
                </tr>
                <tr style="vertical-align: middle;" id="StudentsRow">
                    <td>
                        <input class="StudentsSpinner"  name="value">
                    </td>
                    <td>
                        <label id="lblStundents">Stundent(s)</label>
                    </td>
                    <td>
                        <label id="lblPriceStundents">0.00</label>
                        <div id="divPromoPriceStundents" style="display:none;">
                            &nbsp<label id="lblPromoPriceStundents"></label>
                        </div>
                    </td>
                </tr>
            </table>           
       </div>
       <div class="row ShuttleSection" style="display:none;">
           <hr class="division_line_style" />
       </div>
       <div class="row form-group ShuttleSection" style="display:none;">
           <label class="control-label col-sm-7" for="ddl_needshuttle" id="lblNeedShuttle">Need Shuttle Service ?</label>            
           <div class="col-sm-5">                
            <select name="ddl_needshuttle" class="form-control" id="ddl_needshuttle" >
                <option value="0">No</option>                    
                <option value="1">Yes</option>
            </select>
           </div>
       </div>
       <div class="row form-group PickupOptionsSection" style="display:none;">
           <label class="control-label col-sm-2" for="ddl_shuttleplace" id="lblShuttlePlace">Place: </label>            
           <div class="col-sm-10">                
            <select name="ddl_shuttleplace" class="form-control" id="ddl_shuttleplace" >
                <option value="0">Select Place:</option>                                    
            </select>
           </div>
       </div>
       <div class="row form-group PickupOptionsSection" style="display:none;">
           <label class="control-label col-sm-2" for="ddl_shuttlepickuphour" id="lblShuttleHour">Hour: </label>            
           <div class="col-sm-10">                
            <select name="ddl_shuttlepickuphour" class="form-control" id="ddl_shuttlepickuphour" >
                <option value="0">Select Hour:</option>                                    
            </select>
           </div>
       </div>
       <div class="row SelectTourButtonStyle">
           <button class="btn btn-default" id="btnAddToCart" disabled title="Choose a date first." data-bind="click: AddToCart_Button">Add to cart</button>           
       </div>
   </div>
   <div id="ServiceAddedSection" style="display:none;">
       <div class="row">
           <label id="lblServiceAdded">Service Added</label>
       </div>
       <hr class="division_line_style" />
       <div class="row ServiceAddedButtonSectionStyle">
           <button class="btn btn-default ServiceAddedButton" id="btnContinueShopping" data-bind="click: ContinueShopping_Button">Continue shopping</button>    
       </div>
       <div class="row ServiceAddedButtonSectionStyle">
           <label>OR</label>
       </div>
       <div class="row ServiceAddedButtonSectionStyle">
           <button class="btn btn-default ServiceAddedButton" id="btnProceedCheckout" data-bind="click: ProceedCheckout_Button">Proceed to Checkout</button>    
       </div>       
   </div>
   <div id="SummarySection" class="row">
           <hr class="division_line_style" />
           <div class="CartSummarySectionTitle">
               <label id="lblCartSummary">CART SUMMARY</label>
           </div>          
           <div id="ServicesTableInSummary">
               <table style="width: 100%;">
                   <tbody data-bind="foreach: $root.InsertedServicesList()">                   
                   <tr class="service_table_row" data-bind="attr: { 'id': UniqueId() }">
                       <td>
                           <table style="width: 100%;" >
                               <tbody >
                                   <tr>
                                        <td colspan="2">
                                            <hr class="division_line_style_v2" />
                                        </td>
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceTitle" data-bind="text: ppServiceName">
                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Date: <span data-bind="text: ppFormattedArrivalDate"></span>, Hour: <span data-bind="text: ppScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHidePickupSection">
                                        <td colspan="2" class="ServiceDetailsRowLeft">
                                            Pickup Place: <span data-bind="text: ppPickupPlaceDesc"></span>, Hour: <span data-bind="    text: ppPickupScheduleDesc"></span>                                       
                                        </td>                                                                                                             
                                   </tr>                     
                                   <tr data-bind="css: ShowHideAdult">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppAdults"></span> Adult(s) = <span id="spantotalperadults" data-bind="    attr: { 'id': 'spantotalperadults_' + UniqueId() }, text: ppTotalPerAdults"></span> <span id="spantotalperadultspromo" data-bind="    attr: { 'id': 'spantotalperadultspromo_' + UniqueId() }, text: ppTotalPerAdultsPromo"></span>                                      
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideChildren">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppChildren"></span> Children = <span id="spantotalperchildren" data-bind="    attr: { 'id': 'spantotalperchildren_' + UniqueId() }, text: ppTotalPerChildren"></span> <span id="spantotalperachildrenpromo" data-bind="    attr: { 'id': 'spantotalperachildrenpromo_' + UniqueId() }, text: ppTotalPerChildrenPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideInfants">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppInfants"></span> Infant(s) = <span id="spantotalperinfants" data-bind="    attr: { 'id': 'spantotalperinfants_' + UniqueId() }, text: ppTotalPerInfants"></span> <span id="spantotalperinfantspromo" data-bind="    attr: { 'id': 'spantotalperinfantspromo_' + UniqueId() }, text: ppTotalPerInfantsPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr data-bind="css: ShowHideStudents">
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                            <span data-bind="text: ppStudents"></span> Student(s) = <span id="spantotalperstudents" data-bind="    attr: { 'id': 'spantotalperstudents_' + UniqueId() }, text: ppTotalPerStudents"></span> <span id="spantotalperstudentspromo" data-bind="    attr: { 'id': 'spantotalperstudentspromo_' + UniqueId() }, text: ppTotalPerStudentsPromo"></span>                                                
                                        </td>                                                                                                             
                                   </tr>
                                   <tr>
                                        <td colspan="2" class="ServiceDetailsRowRight">
                                             <%--<a class="RemoveServiceLink" href="">Remove</a>  --%>   
                                             <label class="RemoveServiceLink">Remove</label>                                  
                                        </td>                                                                                                             
                                   </tr>
                               </tbody>   
                           </table>
                       </td>
                   </tr>
                   </tbody>  
               </table>               
           </div>
           <div>
               <table style="width: 100%;">
                   <tr>
                       <td>
                           <hr class="division_line_style" />
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Subtotal: </span><label id="lblSubTotal" ></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Tax: </span><label id="lblTax"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRight">
                           <span>Rent: </span><label id="lblRent"></label> 
                       </td>
                   </tr>
                   <tr>
                       <td class="ServiceDetailsRowRightTotal">
                           <span>Total US$: </span><label id="lblTOTAL"></label> 
                       </td>
                   </tr>
               </table>
           </div>           
   </div>

   
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">              
    <%: Scripts.Render("~/bundles/datepicker") %>
    <%: Scripts.Render("~/bundles/ServiceDetail") %>         
    <%: Styles.Render("~/Content/ServiceDetailCss") %>        
</asp:Content>