﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/VirtualReceipt.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="VirtualReceiptTitle" ContentPlaceHolderID="TitleContent" runat="server">
Virtual Receipt Payment
</asp:Content>

<asp:Content ID="VirtualReceiptFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">        
          <p>Virtual Receipt Payment</p>
    </section>
</asp:Content>   

<asp:Content ID="VirtualReceiptSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server">   
   <div id="summarysection" class="summarysectionstyle">
      <div class="summarybox"> <!-- left position -->
            <!-- Summary content -->    
            <div class="summarytitle">
                SUMMARY
            </div>
            <ul id="summarysection-menu"> 
                <li>
                    <div class="left_item_summary">
                        Client:
                    </div>    
                    <div class="right_item_summary">                        
                        <label id="lbClientName"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Description:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbDescription"> -- </label>
                    </div>
                </li>
                <li>
                    <div>
                      <hr style="border-top: 1px solid #f5a623;"/>
                    </div>
                </li> 
                <li>
                   <div class="left_item_summary item">
                        <label class="SummaryTotalLabel"> TOTAL </label>
                    </div>  
                    <div class="right_item_summary item">
                        <label class="SummaryTotalLabel" id="lbAmount"> -- </label>
                    </div> 
                </li>                                                                                     
            </ul>                                                                   
      </div>
  </div>       
</asp:Content> 

<asp:Content ID="CheckoutContent" ContentPlaceHolderID="MainContent" runat="server">
<input id="hdf_ReceiptCode"  value="<%: ViewBag.ReceiptCode%>" type="hidden"/>
<input id="hdf_CompanyCode"  value="<%: ViewBag.CompanyCode%>" type="hidden"/>
<input id="txtTrackingURLBase"  value="<%: Url.Action("VirtualReceiptCheckout").ToLower().Replace("/virtualreceiptcheckout/virtualreceiptcheckout", "")%>" type="hidden"/>

<div id="panels">

  <div id="checkoutcontent"   data-bind="with: ReservationDTO()">
      <div id="LinkPaymentMessageSection" class="contenttitle">
        <label id="lblLinkPaymentMessage" class="col-sm-12"></label>               
      </div>

      <form id="checkout_form"  method="post" class="form-horizontal">
          <div class="contenttitle">
                YOUR INFORMATION
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="firstname">First Name:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter first name" size="100"  data-bind="value: FName">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="lastname">Last Name:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter last name" size="100" data-bind="value: LName">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="ddl_country">Country:</label>
            <div class="col-sm-10">
                <select name="ddl_country" class="selectpicker form-control" id="ddl_country" data-live-search="true">                                               
                 </select>         
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="phone">Phone:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" size="50"  data-bind="value: Phone">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" size="50"  data-bind="value: EMail">
            </div>
          </div>                  
          <div id="cardinfosection">          
              <div class="contenttitle">
                    PAYMENT INFORMATION
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="cardname">Name on the card:</label>            
                <div class="col-sm-10">                
                  <input type="text" class="form-control" id="cardname" name="cardname" data-bind="value: NameCard"  placeholder="Enter name on the card" size="50">            
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="ddl_card_type">Card type:</label>            
                <div class="col-sm-10">                
                  <select name="ddl_card_type" class="form-control" id="ddl_card_type" >
                    <option value="">Select a type</option>                    
                  </select>
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="cardnumber">Credit card number</label>            
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="cardnumber" name="cardnumber"  data-bind="value: CardNumber"  placeholder="Enter credit card number" size="50">            
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" >Expiration</label>            
                <div class="col-sm-10">                
                        <div class="left_item_expiration_card">
                            <select name="expiration_month_select" class="form-control" id="ddl_expiration_month"  data-bind="value: ExpeMonth" >
                                <option value ="01">January</option>
                                <option  value ="02">February</option>
                                <option  value ="03">March</option>
                                <option  value ="04">April</option>
                                <option  value ="05">May</option>
                                <option  value ="06">June</option>
                                <option  value ="07">July</option>
                                <option  value ="08">August</option>
                                <option  value ="09">September</option>
                                <option  value ="10">October</option>
                                <option  value ="11">November</option>
                                <option  value ="12">December</option>
                            </select>   
                        </div>    
                        <div class="right_item_expiration_card">
                            <select name="expiration_year_select" class="form-control" id="ddl_expiration_year"  data-bind="value: ExpeYear" >                                                        
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>     
                                <option>2023</option>     
                                 <option>2024</option>       
                                 <option>2025</option>             
                            </select>   
                        </div>                   
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="txtCVC">CVC/CVV</label>            
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="txtCVC" name="txtCVC" placeholder="" size="10"  >            
                </div>            
              </div>
          </div>
          <hr />          
          <div class="paynowmessage">
              <p>Your card will be billed for the total amount when press PAY NOW, please wait a few seconds until receive the response of the bank, do NOT press back or stop</p>
          </div>                             
          <div class="form-group">            
            <div class="col-sm-10 AcceptConditions">
               <input type="checkbox" name="acceptTerms" /> I accept the General Terms and Conditions             
            </div>            
          </div>
                     
          <br />
          <div>
               <button type="submit" data-bind="click: $parent.SubmitClick" class="btn btn-default" id="pay_now_button">PAY NOW</button>                       
          </div>
          <input type="hidden" id="taxAmount" />
              <input type="hidden" id="addonAmount" />
              <input type="hidden" id="discount" />
              <input type="hidden" id="totalAmount" />
          <input type="hidden" id="Subtotal" />
          
      </form>
      
  </div>


        <form id="dataForm" method="post"  action="https://vpayment.verifika.com/VPOS/MM/transactionStart20.do">
            <div id=""   data-bind="with: BNReqModel()">
            <input name="IDACQUIRER" id="IDACQUIRER" type="hidden"  data-bind="value: mIDACQUIRER" />
            <input name="IDCOMMERCE" id="IDCOMMERCE" type="hidden"  data-bind="value: mIDCOMMERCE" />
            <input name="XMLREQ" id="XMLREQ" type="hidden"  data-bind="value: mXMLREQ" />
            <input name="DIGITALSIGN" id="DIGITALSIGN" type="hidden"  data-bind="value: mDIGITALSIGN" />
            <input name="SESSIONKEY" id="SESSIONKEY" type="hidden"  data-bind="value: mSESSIONKEY" />
            </div>
        </form>


    <form id="PostForm" name="PostForm" action="" method="POST">
             <div id=""   data-bind="with: CredomaticReqModel()">
                <input type="hidden" name="username"  data-bind="value: username"  >
                <input type="hidden" name="time"  data-bind="value: time" >
                <input type="hidden" name="processor_id"  data-bind="value: processor_id" >
                <input type="hidden" name="hash"  data-bind="value: hash" >
                <input type="hidden" name="key_id"  data-bind="value: key_id" >
                <input type="hidden" name="ccexp"  data-bind="value: ccexp" >
                <input type="hidden" name="orderid"  data-bind="value: orderid" >
                <input type="hidden" name="amount"  data-bind="value: amount" >
                <input type="hidden" name="type"  data-bind="value: type" >
                <input type="hidden" name="ccnumber"  data-bind="value: ccnumber" >
                <input type="hidden" name="redirect"  data-bind="value: redirect" >
          </div>
    </form>


</div>

<!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/VirtualReceiptCheckout") %>         
    <%: Styles.Render("~/Content/VirtualReceiptCheckoutCss") %>        
</asp:Content>

