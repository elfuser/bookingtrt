﻿<%@ Page Title="" Language="VB"  MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="RoomsAvailabilityTitle" ContentPlaceHolderID="TitleContent" runat="server">
Rooms Availability
</asp:Content>

<asp:Content ID="RoomsAvailabilityFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">        
        <div class="row">
            <div class="col-md-4">

            </div>                        
            <div class="col-md-4">
                <p runat="server" id="book_p">BOOK YOUR ROOM TYPE</p>
            </div>                        
            <div class="col-md-4" style="text-align:center;">
                <div id="UserNameDivSection" style="font-family: Montserrat; font-size: 14px; display:none">
                    <i class="fa fa-user" aria-hidden="true"></i> User: <label id="lblUserName" class="UserNameSection"></label>
                </div>
                
            </div>                                                
        </div>
          
    </section>
</asp:Content>    

<asp:Content ID="RoomsSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server" Visible="false">
   
</asp:Content>

<asp:Content ID="RoomsAvailabilityContent" ContentPlaceHolderID="MainContent" runat="server">
<input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>
<input id="hdf_MaxRoomsAllowedToBook"  value="<%: ViewBag.MaxRoomsAllowedToBook%>" type="hidden"/>
<input id="txtTrackingURLBase"  value="<%: Url.Action("RoomsAvailability").ToLower().Replace("/roomsavailability/roomsavailability", "").Replace("/roomsavailability/hotelcode", "")%>" type="hidden"/>
<ul id="ul_rooms" data-bind="foreach: $root.RoomList">
    <li data-bind="attr: { 'id': 'liRoom_' + ppRoomTypeID() + '_' + ppRoomTypeCode() }">        
        <div class="room_div"  data-bind="style: { backgroundImage: 'url(\'' + ImageUlr() + '\')' }" >
            <table class="li_table_room" data-bind="css: Alignmentclass">
                <tr>
                    <td>
                        <table style="width:100%;">
                            <tr>
                                <td style="vertical-align:top;">
                                    <table>
                                        <tr>
                                            <td>
                                                 <span data-bind="text: ppDescription, attr: { 'id': 'roomTypeDesc_' + ppRoomTypeID() }" class="roomtype"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Maxcapacityguest">
                                                Max capacity: <span  data-bind="text: ppMaxOccupance, attr: { 'id': 'roomMaxCapacity_' + ppRoomTypeID() }"></span>
                                            </td>
                                        </tr>
                                    </table>                                                                           
                                </td>
                                <td class="float-right roompricelabel">
                                   <table>
                                       <tr>
                                           <td style="padding-left: 5%;">
                                              $<span data-bind="text: Price, attr: { 'id': 'roomPrice_' + ppRoomTypeID() }"></span>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                              <span data-bind="attr: { 'id': 'RoomPriceWithDiscount_' + ppRoomTypeID() }" ></span>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <span class="Pernightlabel">Per Night</span>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <span class="nonrefundablelabel">Non Refundable</span>
                                           </td>
                                       </tr>
                                   </table>   
                                </td>
                            </tr>
                            <tr>
                                <td class="Room_Information_Title" colspan="2">
                                    Room Information: 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>                            
                            <tr>
                                <td class="Room_Information_Text" colspan="2">
                                     <p data-bind="text: ppRoom_Detail"></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr> 
                             <tr>
                                <td colspan="2">
                                    <input class="Book_Now_Button" data-bind="click: Book_Now_Button" type="button" value="BOOK NOW" runat="server" />
                                </td>
                            </tr>                            
                        </table>                                                                      
                    </td>
                </tr>
            </table>                                   
        </div>
        <div data-bind="attr: { 'id': 'book_section_' + ppRoomTypeID() }" class="book_section_style" style="display:none;">
            <div class="row book_section_style_row">                
                <div class="col-sm-4 book_section_table_td_content">
                    <div class="row book_section_table_td_header">
                        <div class="col-sm-12 book_section_table_td_header_label">
                            ARRIVAL:
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 book_section_table_td_header_label">
                            <label data-bind="attr: { 'id': 'lblArrivalDate_' + ppRoomTypeID() }" class="ArriveAndDepartureDatesLabel"> -- </label>
                        </div>
                    </div>                                           
                </div>
                <div class="col-sm-4 book_section_table_td_content">
                    <div class="row book_section_table_td_header">
                        <div class="col-sm-12 book_section_table_td_header_label">
                            DEPARTURE:
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 book_section_table_td_header_label">
                            <label data-bind="attr: { 'id': 'lblDepartureDate_' + ppRoomTypeID() }" class="ArriveAndDepartureDatesLabel"> -- </label>
                        </div>
                    </div>                        
                </div>
                <div class="col-sm-4 book_section_tables2">
                    <button class="btn btn-block" data-bind="click: Book_Button">BOOK</button>
                </div>
            </div>

            <div class="multiple_rooms_section">
                <div data-bind="attr: { 'id': 'room_section_' + ppRoomTypeID() }">
                    <div class="row book_section_table_td_header">
                        <div class="col-sm-12 book_section_table_td_header_label">
                            GUESTS
                        </div>
                    </div>
                    <div class="row book_section_table_td_content book_section_table_td_content_label roomdiv">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-2 roomnumberlabel">
                                        Room 1
                                    </div>
                                    <div class="col-sm-3">
                                         <div class="row">
                                             <div class="col-xs-6 adultslabelstyle">
                                                  ADULTS:
                                             </div>
                                             <div class="col-xs-6 spinnerstyle">
                                                 <input class="AdultsSpinner"  name="value">
                                                 <%--<input data-bind="attr: { 'id': 'AdultsSpinner_' + ppRoomTypeID() }" class="AdultsSpinner"  name="value">--%>
                                             </div>
                                         </div>                           
                                    </div>                        
                                    <div class="col-sm-2">
                                        <div class="row">
                                             <div class="col-xs-6 kidslabelstyle">
                                                  KIDS:
                                             </div>
                                             <div class="col-xs-6 spinnerstyle">
                                                 <input class="KidsSpinner" name="value">
                                                 <%--<input data-bind="attr: { 'id': 'KidsSpinner_' + ppRoomTypeID() }" class="KidsSpinner" name="value">--%>
                                             </div>
                                        </div>                              
                                    </div>                       
                                    <div class="col-sm-3">
                                        <div class="row">
                                             <div class="col-xs-6 infantslabelstyle">
                                                  INFANTS:
                                             </div>
                                             <div class="col-xs-6 spinnerstyle">
                                                 <input class="InfantsSpinner" name="value">
                                                 <%--<input data-bind="attr: { 'id': 'InfantsSpinner_' + ppRoomTypeID() }" class="InfantsSpinner" name="value">--%>
                                             </div>
                                        </div>                             
                                    </div>
                                    <div class="col-sm-2 removelabel" style="display:none;">
                                        <span data-bind="attr: { 'id': 'removeroomlink_' + ppRoomTypeID() + '_1' }">
                                            Remove
                                        </span>                                                                              
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                            </div> 
                        </div>
                                                              
                    </div>
                </div>
                <div class="row">                   
                    <div class="col-sm-12 add_room_button_div">
                        <button class="btn btn-default" data-bind="click: Add_Room_Button">Add Room</button>
                    </div>
                </div>
            </div>

            <div class="bookcalendartitle">
                <div>BOOK YOUR DATES</div> 
            </div>
            <div class="Calendar_TD">
                <div class="datepicker" data-bind="attr: { 'id': 'datepicker_' + ppRoomTypeID() }"></div>   
            </div>                                                                
                                                                                    
        </div>              
    </li>        
</ul>

<%--<div id="dialog" title="Alert" style="display:none;">  
     <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
</div>--%>

<!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<% Html.RenderPartial("PaymentPopup")%> 

</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">           
    <%: Scripts.Render("~/bundles/datepicker") %>
    <%: Scripts.Render("~/bundles/roomsavailability") %>         
    <%: Styles.Render("~/Content/FontAwesome/css/styles") %>     
    <%: Styles.Render("~/Content/RoomsAvailabilityCss") %>        
</asp:Content>

