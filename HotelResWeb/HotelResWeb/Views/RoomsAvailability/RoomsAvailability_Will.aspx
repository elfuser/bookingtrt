﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="RoomsAvailabilityTitle" ContentPlaceHolderID="TitleContent" runat="server">
Rooms Availability
</asp:Content>

<asp:Content ID="RoomsAvailabilityFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">        
          <p>Book your room type.</p>
    </section>
</asp:Content>    
<asp:Content ID="RoomsAvailabilityContent" ContentPlaceHolderID="MainContent" runat="server">

     <div id="DivRooms">
      
<input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>

<h2>Company Information</h2>
            
            <table  border="1px">

                <tr>    
                    <td>
                     <label>  CompanyCode:</label> 

                    </td>
                     <td data-bind="text: CompanyCode">
            

                     </td>
                </tr>


                 <tr>    
                    <td>
                       <label>  CompanyName:</label> 

                    </td>
                     <td data-bind="text: CompanyName">
                
                     </td>
                </tr>

                  <tr>    
                    <td>
                        <label>  Tel1:</label>

                    </td>
                     <td data-bind="text: ppTel1">
              
                     </td>
                </tr>

                  <tr>    
                    <td>
                      <label>   Web:</label>

                    </td>
                     <td  data-bind="text: ppWeb">
                                 
                     </td>
                </tr>

                <tr>    
                    <td>
                        <label>Email:</label>

                    </td>
                     <td  data-bind="text: ppEmail">
                
                     </td>
                </tr>

                <tr>
                    <td>
                          <label>      Children free</label>  
                    </td>
                    <td
                        >

                    </td>
                </tr>

                                <tr>    
                    <td>
                        <label> Logo:</label>

                    </td>
                     <td >
                               <img data-bind="attr: { src: getLogo }" />
                     </td>
                </tr>


            </table>
  
<h2>Room List</h2>


                            <table   border="1px" >
                                <thead >
                                    <tr>
                                        <td >
                                            
                                         <label>  Detail </label> 

                                        </td>
                                         <td>
                                             
                                             <label>Description</label>  


                                         </td>
                                        <td>
                                        <label>    Max_Adults </label>  

                                        </td>
                                         <td>
                                         <label>     Max_Children </label>  

                                         </td>
                                          <td>
                                             <label>      Price</label>  

                                          </td>

                                        

                                          <td>
                                             <label>      Logo</label>  

                                          </td>
                                    </tr>


                                </thead>

                                     <tbody data-bind="foreach: $root.RoomList">
                                         <tr>

                                             <td  data-bind="text: ppRoom_Detail" >
                                        
                                            </td>

                                             
                                             <td  data-bind="text: ppDescription" >
                                        
                                            </td>

                                             <td  data-bind="text: ppMax_Adults" >
                                             
                                            </td>

                                             <td data-bind="text: ppMax_Children">
                                               
                                            </td>

                                             <td  data-bind="text: Price">
                                                <label >
                                                </label>
                                            </td>

     
                                             
                                             <td >
                                                <img data-bind="attr: { src: ImageUlr }" width="100"  height="100" />
                                            </td>
                                         </tr>
                                    </tbody>

                            </table>

         <br />

         <h2>         Availability Days</h2>

         
                            <table   border="1px" >
                                <thead >
                                    <tr>
                                        <td >
                                            
                                         <label>  Day </label> 

                                        </td>
                                         <td>
                                             
                                             <label>Qty</label>  


                                         </td>

                                    </tr>


                                </thead>

                                     <tbody data-bind="foreach: $root.AvailableList">
                                         <tr>

                                             <td  data-bind="text: ppDay" >
                                        
                                            </td>

                                             
                                             <td  data-bind="text: ppQty" >
                                        
                                            </td>

                                         </tr>
                                    </tbody>

                            </table>

         <br />
 

         <h2>         Prices List</h2>  
         
           <table   border="1px" >
                                <thead >
                                    <tr>
                                        <td >
                                            
                                         <label>  Day </label> 

                                        </td>
                                         <td>
                                             
                                             <label>Price</label>  


                                         </td>

                                    </tr>


                                </thead>

                                     <tbody data-bind="foreach: $root.PricesList">
                                         <tr>

                                             <td  data-bind="text: strdate" >
                                        
                                            </td>

                                             
                                             <td  data-bind="text: ppPrices" >
                                        
                                            </td>

                                         </tr>
                                    </tbody>

                            </table>       
         

         
         <h2>        Validate Promo Code</h2>  

         <table>
             <tr>
                 <td>Promo Code</td>
                 <td>
                     <input type="text"  value="" data-bind="value:PromoCode" />
                 </td>
                 <td>
                      <button data-bind="click: BtnValidatePromoCode_click">Validate</button>
                 </td>
             </tr>
         </table>

                  <h2>    validar    cantidad de adultos y ninos permitidos</h2>  

          <table>
             <tr>
                 <td>+11 years</td>
                 <td>
                     <input type="text"  value="" data-bind="value:More11Years" />
                 </td>
                                  <td>6-10 years</td>
                 <td>
                     <input type="text"  value="" data-bind="value: between6_10Years" />
                 </td>

                    <td> 0-5 years</td>
                 <td>
                     <input type="text"  value="" data-bind="value: Minor5Years" />
                 </td>
                 <td>
                      <button data-bind="click: BtnValidateCombinations_click">Validate</button>
                 </td>
             </tr>
         </table>
                     
            </div>
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">    
    
    <script src="Scripts/knockout-3.4.0.js"></script>
    <script src="Scripts/knockout_mapping_latest.js"></script>
    <script src="Scripts/App/MainPageModelVm.js"></script>
    <script src="Scripts/App/RoomsAvailability.js"></script>  
    <script src="Scripts/DTO/CompanyModel.js"></script>
    <script src="Scripts/DTO/RoomsModel.js"></script> 
       <script src="Scripts/DTO/DayAvailabilityModel.js"></script> 
           <script src="Scripts/DTO/PricesModel.js"></script> 
    <script src="Scripts/DTO/PromoCodeModel.js"></script>

    <link href="Content/Rooms.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery-ui.min.css" rel="stylesheet" />


<%--        <script type="text/javascript">

        $(document).ready(
            function () {


                self.menuvm = new MenuVm();

                var loginVm = new loginvm();
                ko.applyBindings(loginVm);

                //ko.applyBindings(self, $("#DivRooms")[0]);

                //var RoomsVm = new RoomsAvailabilityVm();
                //ko.applyBindings(RoomsAvailabilityVm);

                //RoomsVm.InitModel();


            }
       );      --%> 

        <script type="text/javascript">

       // $(document).ready(
       //     function () {


       //         var mainVM = new MainPageModelVm();
       //         var RoomsVm = new RoomsAvailabilityVm(mainVM);
       //         ko.applyBindings(RoomsVm);

       //         //$("#menuPageInfo").load("MenuPageV2.shtml",
       //         //        function () {
                            
       //         //            loginVm.IsPageLoaded = true;
       //         //            loginVm.ApplyVm();
       //         //        }
       //         //    );


       //     }
       //);       
    </script>
</asp:Content>
