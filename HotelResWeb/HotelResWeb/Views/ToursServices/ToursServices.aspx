﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Tours.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="ToursServicesTitle" ContentPlaceHolderID="TitleContent" runat="server">
Tours Services
</asp:Content>

<asp:Content ID="ToursServicesLeftPanelContent" ContentPlaceHolderID="LeftPanelContent" runat="server" Visible="false">
   <div class="row">
       <div class="col-sm-12">
           <label id="lblRefineResults">REFINE RESULTS</label>           
       </div>
   </div>
   <div class="row">
       <div class="col-sm-12">            
				<!--Category type-->
				<div class="CategoryBox">                    
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <label id="lblSCategory">Category</label>
                            </td>
                            <td>
                                <i class="fa fa-chevron-down CategoryIcon" aria-hidden="true"></i>
                                <%--<span class="glyphicon glyphicon-chevron-down CategoryIcon"></span>--%>
                            </td>
                        </tr>
                    </table>
				</div>
				<div class="CategoryList" data-bind="foreach: $root.TourCategoryList">
                    <div>
                        <input type="radio" data-bind="attr: { 'id': 'Cat_' + ppCategoryID(), 'checked': ppCategoryChecked() }" name="Cat" class="CategoryRadio"/><label data-bind="    text: ppDescription()"></label>
                    </div>				    
			    </div>		    
       </div>
   </div>
   <div class="row">
       <div class="col-sm-12 CallUsPhoneIcon">
           <span class="glyphicon glyphicon-phone-alt"></span>
       </div>
   </div>
   <div class="row">
       <div class="col-sm-12">           
           <div class="CallUsLabel">              
                <div class="row">
                        <label id="lblCallUs">CALL US 2645-5929</label>
                </div>
                <div class="row">
                        <a id="returntoSiteLink" href="">Return to Web Site</a>
                </div>                          
           </div>                                
       </div>
   </div>
</asp:Content>


<asp:Content ID="ToursServicesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>
    <input id="txtTrackingURLBase"  value="<%: Url.Action("ToursServices").ToLower().Replace("/toursservices/toursservices", "").Replace("/toursservices/hotelcode", "")%>" type="hidden"/>
    <div id="tours_list_div" data-bind="foreach: $root.TourList">
        <div class="row TourRow" data-bind="attr: { 'id': 'Tour_' + ppServiceID() }">
            <div class="row-height">
                <div class="col-sm-4 col-height">
                    <img class="TourImageStyle img-responsive" src="" data-bind="attr: { src: ImageUlr }" />      
                </div>
                <div class="col-sm-5 col-height">
                    <div class="row">
                        <div class="col-sm-12 TourDescStyle" data-bind="text: ppDescription">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 TourDetailsStyle" data-bind="text: ppDetails">

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-height-last-column">                    
                    <table style="width:100%; height:100%;">
                        <tr>
                            <td>
                                <span id="lbPricePerRoom">Price per adult from </span> <b><span data-bind="attr: { 'id': 'servicePrice_' + ppServiceID() }, text: PricePerAdultLabel"></span></b>
                                <b><span data-bind="attr: { 'id': 'ServicePriceWithDiscount_' + ppServiceID() }"></span></b>
                            </td>                                
                        </tr>
                        <tr>
                            <td class="SelectTourButtonStyle">
                                <button class="btn btn-default btnSelect" data-bind="attr: { 'id': 'Select_Tour_Button_' + ppServiceID() }, click: Select_Tour_Button">Select</button>
                            </td>                                
                        </tr>
                    </table>                                     
                </div>
            </div>            
        </div>
    </div>   
    <div id="divNoTours" class="row TourDescStyle" style="display:none;">
        <label>There are no services available</label>
    </div>   


 <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">              
    <%: Scripts.Render("~/bundles/ToursServices") %>         
    <%: Styles.Render("~/Content/ToursServicesCss") %>        
</asp:Content>