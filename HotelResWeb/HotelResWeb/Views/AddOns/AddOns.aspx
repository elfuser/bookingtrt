﻿<%@ Page Language="vb"  MasterPageFile="~/Views/Shared/Site.Master"  Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="AddOnsTitle" ContentPlaceHolderID="TitleContent" runat="server">
AddOns
</asp:Content>

<asp:Content ID="AddOnsFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">

    <input id="txtTrackingURLBase"  value="<%: Url.Action("AddOns").ToLower().Replace("/addons/addons", "").Replace("/addons/hotelcode", "")%>" type="hidden"/>    

    <section class="featured clear-fix">
        <div class="row">
            <div class="col-md-4">

            </div>                        
            <div class="col-md-4">
                 <p>ENHANCE YOUR VISIT</p>  
            </div>                        
            <div class="col-md-4" style="text-align:center;">
                <div id="UserNameDivSection" style="font-family: Montserrat; font-size: 14px; display:none">
                    <i class="fa fa-user" aria-hidden="true"></i> User: <label id="lblUserName" class="UserNameSection"></label>
                </div>
                
            </div>                                                
        </div>                                           
    </section>
             
</asp:Content>   

<asp:Content ID="RoomsSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server">
   <div class="StartOverDiv">
       <button data-bind="click: StartOver" class="btn btn-startover" title="Click if you wish to begin again your booking.">Start Over</button>
   </div> 
   <div id="summarysection" class="summarysectionstyle">
      <div class="summarybox"> <!-- left position -->
            <!-- Summary content -->    
            <div class="summarytitle">
                SUMMARY
            </div>
            <ul id="summarysection-menu"> 
                <li>
                    <div class="left_item_summary">
                        Arrival:
                    </div>    
                    <div class="right_item_summary">                        
                        <label id="lbArrivalDate"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Departure:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbDepartureDate"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Nights:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomNights"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Rooms:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomsQty"> -- </label>
                    </div>
                </li>
                <li>
                    <div>
                        <table id="RoomsTableInSummary" style="width: 100%;">
                            <tbody data-bind="foreach: $root.AddedRoomList()">                                
                                <tr>
                                    <td colspan="2">
                                        <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Room Type:
                                    </td>
                                    <td data-bind="text: Desc" class="addon_summary_item_price">
                                        
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td colspan="2" class="rooms_summary_item_desc">
                                        Adult(s): <span data-bind="text: Adults"></span>, Kid(s): <span data-bind="text: Kids"></span>
                                        , Infant(s): <span data-bind="text: Infants"></span>
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Rate Selected:
                                    </td>
                                    <td data-bind="text: PriceLabel, css: Tachclass" class="addon_summary_item_price">
                                           
                                    </td>                                                                                 
                                </tr>                               
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        
                                    </td>
                                    <td data-bind="text: PriceWithDiscLabel" class="addon_summary_item_price" >
                                           
                                    </td>                                                                                 
                                </tr>                                                                
                            </tbody>
                        </table>                    
                    </div>
                 </li>                       
            </ul>           
            <ul id="summary_addons">
                <li>
                    <div>
                      <hr />
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Add-on:
                    </div>    
                    <div class="right_item_summary">
    
                            <br />
                         <table id="AddOnsTableInSummary">
                                     <tbody data-bind="foreach: $root.AddedAddonList">
                                         <tr class="addon_table_row" data-bind="attr: { 'data-target': Code(), 'id': UniqueId() }">
                                             <td  data-bind="text: Desc" class="addon_summary_item_desc">
                                        
                                             </td>                                             
                                             <td  data-bind="text: PriceInText" class="addon_summary_item_price">
                                        
                                            </td>
                                            <td class="addon_button">
                                                <img class="addon_button_image" src="<%=ResolveUrl("~/Images/success.png") %>" />
                                            </td> 
                                         </tr>
                                    </tbody>
                            </table>
                    </div>
                </li> 
            </ul>                     
            <ul id="summary_total">
                <li>
                    <div>
                      <hr style="border-top: 1px solid #f5a623;"/>
                    </div>
                </li>                  
                <li>
                    <div class="left_item_summary">
                        Total:
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Room Night(s):
                    </div>    
                    <div class="right_item_summary item">                        
                        <label id="lbSubTotalRoomNights"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Add-ons:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbSubTotalAddOns"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Discount:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbDiscount"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Taxes:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbTaxes"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        Promotional Code:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoCodeSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoAmountSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li>
                   <div class="left_item_summary item">
                        <label class="SummaryTotalLabel"> TOTAL </label>
                    </div>  
                    <div class="right_item_summary item">
                        <label class="SummaryTotalLabel" id="lbTOTAL"> -- </label>
                    </div> 
                </li>                            
            </ul>           
      </div>
  </div>       
</asp:Content>

<asp:Content ID="AddOnsContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="hdf_HotelCodeInAddOns"  value="<%: ViewBag.HotelCodeInAddOns%>" type="hidden"/>
    <input id="hdf_DateFromInAddOns"  value="<%: ViewBag.DateFromInAddOns%>" type="hidden"/>
    <input id="hdf_DateToInAddOns"  value="<%: ViewBag.DateToInAddOns%>" type="hidden"/>
        <div class="AddOn_div"  data-bind="style: { backgroundImage: 'url(\'' + getLogoAddOn() + '\')' }" >

                    <div class="AddOnsBar container">

                        <ul id="ul_AddOnsTypes" data-bind="foreach: $root.AddOnTypeList">
                            <li data-bind="attr: { 'id': 'liAddOnType_' + ppAddonID() }">    
                                <img src="" data-bind=" attr: { src: ImageUlr(), title: ppDescription(), id: ppDescription() }" class='Type_Button' />
                            </li>
                        </ul>                     

                    </div>

         </div>
        <div id="AddOnDetail" style="display:none;">
              <div class=" row-centered pos">
              
                        <table class="table" >
           
                                     <tbody data-bind="foreach: $root.AddOnsList">
                                         <tr >

                                             <td  style="width:50%" >
                                                <img src ="" data-bind="attr: { src: ImageUlr }" class="img-responsive addons_detailimage"  />
                                            
                                            </td>

                                             
                                             <td style="width:50%">

                                                 <div class="AddOnTitle" data-bind="text: ppDescription">

                                                 </div>
                                                 <br />
                                                 <br />
                                                 
                                                   <label class="AddOnDetailTitle" style="font-weight: bold;">Information</label>
                                                     <br />
                                                         <br />
                                                 <div class="AddOnDetail">
                                                   
                                                      <label data-bind="text: ppLongDescription"></label>
                                                 

                                                 </div>

                                                      <br />
                                                             <br />
                                                       <label class="Price" data-bind="text: DescPrice"></label>
                                                 
                                                    <br />
                                                          <br />  
                                                <div style="    text-align: center;">

                                               
                                                   <button data-bind="click: function (data, event) { $parent.BookAddon(data) }" class="btn  btn-book">ADD TO YOUR BOOKING</button>
                                              </div>  
                                                  <br /> 
                                                    </td>

                                         </tr>
                                    </tbody>

                            </table>       

              </div>
         </div>                
        <div id="PromotionsDiv">
            <div class="OtherPromotionsLabel">
                <p>Other Promotions For You</p>
            </div>
            <ul class="slider2" data-bind="foreach: $root.AddOnsListOther">
                <li class="slide">                               
                    <div style="text-align:center;">
                         <table style="width:100%; text-align:center;">
                             <tr>
                                 <td>
                                     <img src="" data-bind=" attr: { src: ImageUlr() }">
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <div style=" text-align: center;" class="AddOnDetailTitle" data-bind="text: ppDescription"></div>
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <div style=" text-align: center; height: 50px; overflow-y: scroll;" class="AddOnDetail" data-bind="text: ppLongDescription"></div>
                                 </td>
                             </tr>
                             <tr>
                                 <td>                                  
                                     &nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <div style=" text-align: center;">                                               
                                        <button  data-bind="click: function (data, event) { $parent.BookAddon(data) }"  class="btn  btn-book">Add to Cart</button>
                                     </div>
                                 </td>
                             </tr>
                         </table>
                    </div>
                </li>        
            </ul>
        </div>
        <div class="ReadyWithYourBookDiv" style="text-align:center;">
            <table style="width:100%">
                <tr>
                    <td class="Ready_with_your_book_label">
                        Ready with your booking?                        
                    </td>
                    <td class="pay_now">
                        <button class="btn btn-block" data-bind="click: PayNow_Button">PAY NOW</button>
                    </td>
                </tr>
            </table>
        </div>  
    
    <%--<div id="dialog" title="Alert" style="display:none;">  
     <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
    </div>--%>
    
    <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>                  
</asp:Content>

<asp:Content ID="AddOnsScripts" ContentPlaceHolderID="ScriptsSection" runat="server">     
   <%: Styles.Render("~/Content/FontAwesome/css/styles") %> 
   <%: Styles.Render("~/Content/AddonsCss") %>          
    <%: Scripts.Render("~/bundles/Addons") %>         



</asp:Content>