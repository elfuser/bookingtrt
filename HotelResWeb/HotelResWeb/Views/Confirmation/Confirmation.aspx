﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="CheckoutTitle" ContentPlaceHolderID="TitleContent" runat="server">
Confirmation
</asp:Content>

<asp:Content ID="CheckoutFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">                 
        <div class="row">
            <div class="col-md-4">

            </div>                        
            <div class="col-md-4">
                  <p>CONFIRMATION</p>
            </div>                        
            <div class="col-md-4" style="text-align:center;">
                <div id="UserNameDivSection" style="font-family: Montserrat; font-size: 14px; display:none">
                    <i class="fa fa-user" aria-hidden="true"></i> User: <label id="lblUserName" class="UserNameSection"></label>
                </div>
                
            </div>                                                
        </div>
    </section>
</asp:Content>   

<asp:Content ID="RoomsSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server">
   <div class="StartOverDiv">
        <button id="btnStartOver" data-bind="click: StartOver" class="btn btn-startover" title="Click if you wish to begin again your booking.">Start Over</button>
   </div>
   <div id="summarysection" class="summarysectionstyle">      
      <div class="summarybox"> <!-- left position -->
            <!-- Summary content -->    
            <div class="summarytitle">
                SUMMARY
            </div>
            <ul id="summarysection-menu"> 
                <li>
                    <div class="left_item_summary">
                        Arrival:
                    </div>    
                    <div class="right_item_summary">                        
                        <label id="lbArrivalDate"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Departure:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbDepartureDate"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Nights:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomNights"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Rooms:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomsQty"> -- </label>
                    </div>
                </li>
                <li>
                    <div>
                        <table id="RoomsTableInSummary" style="width: 100%;">
                            <tbody data-bind="foreach: $root.AddedRoomList()">                                
                                <tr>
                                    <td colspan="2">
                                        <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Room Type:
                                    </td>
                                    <td data-bind="text: Desc" class="addon_summary_item_price">
                                        
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td colspan="2" class="rooms_summary_item_desc">
                                        Adult(s): <span data-bind="text: Adults"></span>, Kid(s): <span data-bind="text: Kids"></span>
                                        , Infant(s): <span data-bind="text: Infants"></span>
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Rate Selected:
                                    </td>
                                    <td data-bind="text: PriceLabel, css: Tachclass" class="addon_summary_item_price">
                                           
                                    </td>                                                                                 
                                </tr>                               
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        
                                    </td>
                                    <td data-bind="text: PriceWithDiscLabel" class="addon_summary_item_price" >
                                           
                                    </td>                                                                                 
                                </tr>                                                                
                            </tbody>
                        </table>                    
                    </div>
                </li>
                <%--<li>
                    <div class="left_item_summary">
                        Guests:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbGuestsQty"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Rate Selected:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRateSelected"> -- </label>
                    </div>

                </li>
                <li>
                                       
                    <div class="right_item_summary" id="divDiscount">
                        <label id="lbRateSelectedDisc"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Room Type:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomTypeDesc"> -- </label>
                    </div>
                </li>          --%>        
            </ul>           
            <ul id="summary_addons">
                <li>
                    <div>
                      <hr />
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Add-on:
                    </div>    
                    <div class="right_item_summary">
    
                            <br />
                         <table id="AddOnsTableInSummary">
                                     <tbody data-bind="foreach: $root.AddedAddonList">
                                         <tr class="addon_table_row" data-bind="attr: { 'data-target': Code(), 'id': UniqueId() }">
                                             <td  data-bind="text: Desc" class="addon_summary_item_desc">
                                        
                                             </td>                                             
                                             <td  data-bind="text: PriceInText" class="addon_summary_item_price">
                                        
                                            </td>
                                            <td class="addon_button">
                                                <img class="addon_button_image" src="<%=ResolveUrl("~/Images/success.png") %>" />
                                            </td> 
                                         </tr>
                                    </tbody>
                            </table>
                    </div>
                </li> 
            </ul>                     
            <ul id="summary_total">
                <li>
                    <div>
                      <hr style="border-top: 1px solid #f5a623;"/>
                    </div>
                </li>                  
                <li>
                    <div class="left_item_summary">
                        Total:
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Room Night(s):
                    </div>    
                    <div class="right_item_summary item">                        
                        <label id="lbSubTotalRoomNights"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Add-ons:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbSubTotalAddOns"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Discount:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbDiscount"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Taxes:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbTaxes"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        Promotional Code:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoCodeSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoAmountSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li>
                   <div class="left_item_summary item">
                        <label class="SummaryTotalLabel"> TOTAL </label>
                    </div>  
                    <div class="right_item_summary item">
                        <label class="SummaryTotalLabel" id="lbTOTAL"> -- </label>
                    </div> 
                </li>                            
            </ul>           
      </div>
  </div>       
</asp:Content>  

<asp:Content ID="CheckoutContent" ContentPlaceHolderID="MainContent" runat="server">
<input id="hdf_HotelCode"  value="<%: ViewBag.HotelCode%>" type="hidden"/>
<input id="txtTrackingURLBase"  value="<%: Url.Action("Confirmation").ToLower().Replace("/confirmation/confirmation", "").Replace("/confirmation/hotelcode", "")%>" type="hidden"/>

<div id="panels">    
  <div id="checkoutcontent">
      <div class="contenttitle">


          <%: ViewBag.Message%>

               
      </div>
       <div class="confirmationmessage">
             <%: ViewBag.subtitle%>
    </div>
    <%--  THANKS FOR YOUR PURCHASE  <div class="confirmationmessage">
          Thanks you for your purchase, a copy of your reservation has been sent to your email. If you need any help or change please reach out to our email or phone number.
      </div>  --%>          
  </div>    
</div>

<div id="dialog" title="Alert" style="display:none;">  
     <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
</div>
    <table   >

        <tr>
            <td >
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/Confirmation") %>  
    <%: Styles.Render("~/Content/FontAwesome/css/styles") %>        
    <%: Styles.Render("~/Content/ConfirmationCss") %>        
</asp:Content>

