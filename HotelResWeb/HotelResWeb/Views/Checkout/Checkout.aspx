﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="CheckoutTitle" ContentPlaceHolderID="TitleContent" runat="server">
Checkout    
</asp:Content>

<asp:Content ID="CheckoutFeatured" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured clear-fix">                  
        <div class="row">
            <div class="col-md-4">

            </div>                        
            <div class="col-md-4">
                 <p>GUESS INFORMATION</p>  
            </div>                        
            <div class="col-md-4" style="text-align:center;">
                <div id="UserNameDivSection" style="font-family: Montserrat; font-size: 14px; display:none">
                    <i class="fa fa-user" aria-hidden="true"></i> User: <label id="lblUserName" class="UserNameSection"></label>
                </div>
                
            </div>                                                
        </div>  
    </section>
</asp:Content>   

<asp:Content ID="RoomsSummaryContent" ContentPlaceHolderID="SummaryContent" runat="server">
   <input id="hdf_UserAgencyCode"  value="<%: ViewBag.UserAgencyCode%>" type="hidden"/>
   <div class="StartOverDiv">
       <button id="btnStartOver" data-bind="click: StartOver" class="btn btn-startover" title="Click if you wish to begin again your booking.">Start Over</button>
   </div>
   <div id="summarysection" class="summarysectionstyle">
      <div class="summarybox"> <!-- left position -->
            <!-- Summary content -->    
            <div class="summarytitle">
                SUMMARY
            </div>
            <ul id="summarysection-menu"> 
                <li>
                    <div class="left_item_summary">
                        Arrival:
                    </div>    
                    <div class="right_item_summary">                        
                        <label id="lbArrivalDate"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Departure:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbDepartureDate"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Nights:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomNights"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary">
                        Rooms:
                    </div>    
                    <div class="right_item_summary">
                        <label id="lbRoomsQty"> -- </label>
                    </div>
                </li>               
                <li>
                    <div>
                        <table id="RoomsTableInSummary" style="width: 100%;">
                            <tbody data-bind="foreach: $root.AddedRoomList()">                                
                                <tr>
                                    <td colspan="2">
                                        <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Room Type:
                                    </td>
                                    <td data-bind="text: Desc" class="addon_summary_item_price">
                                        
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td colspan="2" class="rooms_summary_item_desc">
                                        Adult(s): <span data-bind="text: Adults"></span>, Kid(s): <span data-bind="text: Kids"></span>
                                        , Infant(s): <span data-bind="text: Infants"></span>
                                    </td>                                                                                 
                                </tr>
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        Rate Selected:
                                    </td>
                                    <td data-bind="text: PriceLabel, css: Tachclass" class="addon_summary_item_price">
                                           
                                    </td>                                                                                 
                                </tr>                               
                                <tr>
                                    <td class="rooms_summary_item_desc">
                                        
                                    </td>
                                    <td data-bind="text: PriceWithDiscLabel" class="addon_summary_item_price" >
                                           
                                    </td>                                                                                 
                                </tr>                                                                
                            </tbody>
                        </table>                    

                    </div>
                </li>                             
            </ul>           
            <ul id="summary_addons">
                <li>
                    <div>
                      <hr />
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary">
                        Add-on:
                    </div>    
                    <div class="right_item_summary">
    
                            <br />
                         <table id="AddOnsTableInSummary">
                                     <tbody data-bind="foreach: $root.AddedAddonList">
                                         <tr class="addon_table_row" data-bind="attr: { 'data-target': Code(), 'id': UniqueId() }">
                                             <td  data-bind="text: Desc" class="addon_summary_item_desc">
                                        
                                             </td>                                             
                                             <td  data-bind="text: PriceInText" class="addon_summary_item_price">
                                        
                                            </td>
                                            <td class="addon_button">
                                                <img class="addon_button_image" src="<%=ResolveUrl("~/Images/success.png") %>" />
                                            </td> 
                                         </tr>
                                    </tbody>
                            </table>
                    </div>
                </li> 
            </ul>                     
            <ul id="summary_total">
                <li>
                    <div>
                      <hr style="border-top: 1px solid #f5a623;"/>
                    </div>
                </li>                  
                <li>
                    <div class="left_item_summary">
                        Total:
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Room Night(s):
                    </div>    
                    <div class="right_item_summary item">                        
                        <label id="lbSubTotalRoomNights"> -- </label>
                    </div>
                </li> 
                <li>
                    <div class="left_item_summary item">
                        Add-ons:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbSubTotalAddOns"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Discount:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbDiscount"> -- </label>
                    </div>
                </li>
                <li>
                    <div class="left_item_summary item">
                        Taxes:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbTaxes"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        Promotional Code:
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoCodeSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div class="left_item_summary item">
                        
                    </div>    
                    <div class="right_item_summary item">
                        <label id="lbPromoAmountSummary"> -- </label>
                    </div>
                </li>
                <li class="SummaryPromoCode">
                    <div>
                      <hr style="border-top: 1px solid #CACACA; margin-top: 10px; margin-bottom: 10px;" />
                    </div>
                </li>
                <li>
                   <div class="left_item_summary item">
                        <label class="SummaryTotalLabel"> TOTAL </label>
                    </div>  
                    <div class="right_item_summary item">
                        <label class="SummaryTotalLabel" id="lbTOTAL"> -- </label>
                    </div> 
                </li>                            
            </ul>           
      </div>
  </div>       
</asp:Content> 

<asp:Content ID="CheckoutContent" ContentPlaceHolderID="MainContent" runat="server">
<input id="hdf_HotelCodeInCheckout"  value="<%: ViewBag.HotelCodeInCheckout%>" type="hidden"/>
<input id="txtTrackingURLBase"  value="<%: Url.Action("Checkout").ToLower().Replace("/checkout/checkout", "").Replace("/checkout/hotelcode", "")%>" type="hidden"/>

<div id="panels">

  <div id="checkoutcontent"   data-bind="with: ReservationDTO()">
      <form id="checkout_form"  method="post" class="form-horizontal">
          <div class="contenttitle">
                YOUR INFORMATION
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="firstname">First Name:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter first name" size="100"  data-bind="value: FName">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="lastname">Last Name:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter last name" size="100" data-bind="value: LName">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="ddl_country">Country:</label>
            <div class="col-sm-10">
                <select name="ddl_country" class="selectpicker form-control" id="ddl_country" data-live-search="true">                                               
                 </select>         
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="phone">Phone:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" size="50"  data-bind="value: Phone">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" size="50"  data-bind="value: EMail">
            </div>
          </div>                  
          <div id="cardinfosection">          
              <div class="contenttitle">
                    PAYMENT INFORMATION
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="cardname">Name on the card:</label>            
                <div class="col-sm-10">                
                  <input type="text" class="form-control" id="cardname" name="cardname" data-bind="value: NameCard"  placeholder="Enter name on the card" size="50">            
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="ddl_card_type">Card type:</label>            
                <div class="col-sm-10">                
                  <select name="ddl_card_type" class="form-control" id="ddl_card_type" >
                    <option value="">Select a type</option>                    
                  </select>
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="cardnumber">Credit card number</label>            
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="cardnumber" name="cardnumber"  data-bind="value: CardNumber"  placeholder="Enter credit card number" size="50">            
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" >Expiration</label>            
                <div class="col-sm-10">                
                        <div class="left_item_expiration_card">
                            <select name="expiration_month_select" class="form-control" id="ddl_expiration_month"  data-bind="value: ExpeMonth" >
                                <option value ="01">January</option>
                                <option  value ="02">February</option>
                                <option  value ="03">March</option>
                                <option  value ="04">April</option>
                                <option  value ="05">May</option>
                                <option  value ="06">June</option>
                                <option  value ="07">July</option>
                                <option  value ="08">August</option>
                                <option  value ="09">September</option>
                                <option  value ="10">October</option>
                                <option  value ="11">November</option>
                                <option  value ="12">December</option>
                            </select>   
                        </div>    
                        <div class="right_item_expiration_card">
                            <select name="expiration_year_select" class="form-control" id="ddl_expiration_year"  data-bind="value: ExpeYear" >                                                       
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>     
                                <option>2023</option>     
                                 <option>2024</option>       
                                 <option>2025</option>             
                            </select>   
                        </div>                   
                </div>            
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2 paymentinfofield" for="txtCVC">CVC/CVV</label>            
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="txtCVC" name="txtCVC" placeholder="" size="10"  >            
                </div>            
              </div>
          </div>
          <hr />
                     
          <br />
          <div>
               <button type="submit" data-bind="click: $parent.SubmitClick" class="btn btn-default" id="pay_now_button">PAY NOW</button>                       
          </div>
          <input type="hidden" id="taxAmount" />
              <input type="hidden" id="addonAmount" />
              <input type="hidden" id="discount" />
              <input type="hidden" id="totalAmount" />
          <input type="hidden" id="Subtotal" />
          
      </form>
      
  </div>


        <form id="dataForm" method="post"  action="https://vpayment.verifika.com/VPOS/MM/transactionStart20.do">
            <div id=""   data-bind="with: BNReqModel()">
            <input name="IDACQUIRER" id="IDACQUIRER" type="hidden"  data-bind="value: mIDACQUIRER" />
            <input name="IDCOMMERCE" id="IDCOMMERCE" type="hidden"  data-bind="value: mIDCOMMERCE" />
            <input name="XMLREQ" id="XMLREQ" type="hidden"  data-bind="value: mXMLREQ" />
            <input name="DIGITALSIGN" id="DIGITALSIGN" type="hidden"  data-bind="value: mDIGITALSIGN" />
            <input name="SESSIONKEY" id="SESSIONKEY" type="hidden"  data-bind="value: mSESSIONKEY" />
            </div>
        </form>


    <form id="PostForm" name="PostForm" action="" method="POST">
             <div id=""   data-bind="with: CredomaticReqModel()">
                <input type="hidden" name="username"  data-bind="value: username"  >
                <input type="hidden" name="time"  data-bind="value: time" >
                <input type="hidden" name="processor_id"  data-bind="value: processor_id" >
                <input type="hidden" name="hash"  data-bind="value: hash" >
                <input type="hidden" name="key_id"  data-bind="value: key_id" >
                <input type="hidden" name="ccexp"  data-bind="value: ccexp" >
                <input type="hidden" name="orderid"  data-bind="value: orderid" >
                <input type="hidden" name="amount"  data-bind="value: amount" >
                <input type="hidden" name="type"  data-bind="value: type" >
                <input type="hidden" name="ccnumber"  data-bind="value: ccnumber" >
                <input type="hidden" name="redirect"  data-bind="value: redirect" >
          </div>
    </form>


</div>

<!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<% Html.RenderPartial("TermConditionsPopup")%> 

</asp:Content>

<asp:Content ID="RoomsAvailabilityScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/Checkout") %>         
    <%: Styles.Render("~/Content/FontAwesome/css/styles") %> 
    <%: Styles.Render("~/Content/CheckoutCss") %>        
</asp:Content>

