﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Web

Public Class getPhoto
    Implements System.Web.IHttpHandler


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.Clear()

        If Not [String].IsNullOrEmpty(context.Request.QueryString("roomTypeID")) Then
            Dim roomID As Integer = context.Request.QueryString("roomTypeID")
            Dim id As Integer = Int32.Parse(context.Request.QueryString("photoNumber"))

            Dim image As Image = GetRoomImage(roomID, id)

            If image Is Nothing Then
                context.Response.ContentType = "image/jpeg"
                image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                Dim ms As New MemoryStream()

                image.Save(ms, ImageFormat.Jpeg)
                ms.WriteTo(context.Response.OutputStream)
            Else
                context.Response.ContentType = "image/jpeg"
                image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
            End If
        Else
            Dim Image As Image
            context.Response.ContentType = "image/jpeg"
            Image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
            Dim ms As New MemoryStream()

            Image.Save(ms, ImageFormat.Jpeg)
            ms.WriteTo(context.Response.OutputStream)
        End If
    End Sub

    Private Function GetRoomImage(roomTypeID As Integer, photoNumber As Integer) As Image
        Dim memoryStream As New MemoryStream()
        memoryStream = Nothing

        Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
        Using sqlConnection As New SqlConnection(connectionString)
            Using sqlCommand As New SqlCommand("spRoomPhoto", sqlConnection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@mode", "SEL")
                sqlCommand.Parameters.AddWithValue("@roomtype_id", roomTypeID)
                sqlCommand.Parameters.AddWithValue("@photoNumber", photoNumber)

                sqlConnection.Open()
                Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                If sqlDataReader.HasRows Then
                    sqlDataReader.Read()
                    If Not sqlDataReader("photo") Is DBNull.Value Then
                        Dim btImage As Byte() = DirectCast(sqlDataReader("photo"), Byte())
                        memoryStream = New MemoryStream(btImage, False)
                    End If
                End If
            End Using
            sqlConnection.Close()
        End Using

        If memoryStream Is Nothing Then
            Return Nothing
        Else
            Return Image.FromStream(memoryStream)
        End If
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class