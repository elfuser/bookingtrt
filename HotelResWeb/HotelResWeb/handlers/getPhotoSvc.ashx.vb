﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Web

Public Class getPhotoSvc
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.Clear()

        If Not [String].IsNullOrEmpty(context.Request.QueryString("serviceID")) Then
            Dim serviceID As Integer = context.Request.QueryString("serviceID")
            Dim id As Integer = Int32.Parse(context.Request.QueryString("photoNumber"))

            Dim image As Image = GetServiceImage(serviceID, id)

            If image Is Nothing Then
                context.Response.ContentType = "text/html"
                context.Response.Write("")
            Else
                context.Response.ContentType = "image/jpeg"
                image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
            End If
        Else
            context.Response.ContentType = "text/html"
            context.Response.Write("<p>Need a valid id</p>")
        End If
    End Sub

    Private Function GetServiceImage(serviceID As Integer, photoNumber As Integer) As Image

        Dim memoryStream As New MemoryStream()

        Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
        Using sqlConnection As New SqlConnection(connectionString)
            Using sqlCommand As New SqlCommand("spServicePhoto", sqlConnection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@mode", "SEL")
                sqlCommand.Parameters.AddWithValue("@serviceID", serviceID)
                sqlCommand.Parameters.AddWithValue("@photoNumber", photoNumber)

                sqlConnection.Open()
                Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                If sqlDataReader.HasRows Then
                    sqlDataReader.Read()
                    If sqlDataReader("photo") Is DBNull.Value Then
                        memoryStream = Nothing
                    Else
                        Dim btImage As Byte() = DirectCast(sqlDataReader("photo"), Byte())
                        memoryStream = New MemoryStream(btImage, False)
                    End If
                Else
                    memoryStream = Nothing
                End If
            End Using
            sqlConnection.Close()
        End Using

        If memoryStream Is Nothing Then
            Return Nothing
        Else
            Return Image.FromStream(memoryStream)
        End If
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class