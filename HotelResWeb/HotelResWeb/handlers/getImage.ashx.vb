﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Linq
Imports System.Web

Public Class getImage
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest


        context.Response.Clear()

        If Not [String].IsNullOrEmpty(context.Request.QueryString("hotelId")) Then
            Dim hotelId As String = context.Request.QueryString("hotelId")
            Dim id As Integer = Int32.Parse(context.Request.QueryString("photoNumber"))

            Dim image As Image = GetCompanyImage(hotelId, id)

            If Not image Is Nothing Then
                context.Response.ContentType = "image/jpeg"
                image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
            Else
                context.Response.ContentType = "text/html"
                context.Response.Write("<p>Need a valid id</p>")
            End If
        Else
            context.Response.ContentType = "text/html"
            context.Response.Write("<p>Need a valid id</p>")
        End If

    End Sub


    Private Function GetCompanyImage(companyCode As String, photoNumber As Integer) As Image

        Dim memoryStream As New MemoryStream()

        Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
        Using sqlConnection As New SqlConnection(connectionString)
            Using sqlCommand As New SqlCommand("spCompanyPhoto", sqlConnection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@mode", "SEL")
                sqlCommand.Parameters.AddWithValue("@company_code", companyCode)
                sqlCommand.Parameters.AddWithValue("@photoNumber", photoNumber)

                sqlConnection.Open()
                Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                If sqlDataReader.HasRows Then
                    sqlDataReader.Read()
                    Dim btImage As Byte() = DirectCast(sqlDataReader("photo"), Byte())

                    memoryStream = New MemoryStream(btImage, False)
                End If
            End Using
            sqlConnection.Close()
        End Using

        If memoryStream.Length > 0 Then
            Return Image.FromStream(memoryStream)
        Else
            Return Nothing
        End If
    End Function
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class