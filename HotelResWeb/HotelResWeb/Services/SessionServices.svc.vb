﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "SessionServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select SessionServices.svc or SessionServices.svc.vb at the Solution Explorer and start debugging.
Imports BRules
Imports HotelResWeb

Public Class SessionServices
    Implements ISessionServices

    Public Function CheckSessionID(SessionID As String) As Boolean Implements ISessionServices.CheckSessionID
        Dim response As Boolean
        Dim Session As New Session
        response = Session.CheckSessionID(SessionID)
        Return response
    End Function

    Public Function CheckSessionServiceID(SessionID As String) As Boolean Implements ISessionServices.CheckSessionServiceID
        Dim response As Boolean
        Dim Session As New Session
        response = Session.CheckSessionServiceID(SessionID)
        Return response
    End Function

    Public Function DeleteRowSC(RowID As Long, SessionID As String) As Response Implements ISessionServices.DeleteRowSC
        Dim response As New Response
        Dim Session As New Session
        response = Session.DeleteRowSC(RowID, SessionID)
        Return response
    End Function

    Public Function GetCompanySession(SessionID As String) As List(Of SessionInfo) Implements ISessionServices.GetCompanySession
        Dim response As List(Of SessionInfo)
        Dim Session As New Session
        response = Session.GetCompanySession(SessionID)
        Return response
    End Function

    Public Function GetCompanySessionByPPToken(PayPalToken As String) As List(Of SessionInfo) Implements ISessionServices.GetCompanySessionByPPToken
        Dim response As List(Of SessionInfo)
        Dim Session As New Session
        response = Session.GetCompanySessionByPPToken(PayPalToken)
        Return response
    End Function

    Public Function GetCompanySessionByReservationID(ReservationID As String) As List(Of SessionInfo) Implements ISessionServices.GetCompanySessionByReservationID
        Dim response As List(Of SessionInfo)
        Dim Session As New Session
        response = Session.GetCompanySessionByReservationID(ReservationID)
        Return response
    End Function

    Public Function GetCompanySessionService(Mode As String, SessionID As String, SaleID As String) As List(Of SessionServiceInfo) Implements ISessionServices.GetCompanySessionService
        Dim response As List(Of SessionServiceInfo)
        Dim Session As New Session
        response = Session.GetCompanySessionService(Mode, SessionID, SaleID)
        Return response
    End Function

    Public Function GetSessionServiceSC(SessionID As String, Culture As String) As List(Of ServiceSCInfo) Implements ISessionServices.GetSessionServiceSC
        Dim response As List(Of ServiceSCInfo)
        Dim Session As New Session
        response = Session.GetSessionServiceSC(SessionID, Culture)
        Return response
    End Function

    Public Function InsertSessionServiceSC(SessionID As String, serviceID As Integer, arrivalDate As Date, ScheduleID As Integer, hotelPickupID As Integer, hotelPickupHourID As Integer, adults As Integer, children As Integer, infants As Integer, students As Integer, priceAdults As Double, priceChildren As Double, priceInfants As Double, priceStudents As Double) As Response Implements ISessionServices.InsertSessionServiceSC
        Dim response As Response
        Dim Session As New Session
        response = Session.InsertSessionServiceSC(SessionID, serviceID, arrivalDate, ScheduleID, hotelPickupID, hotelPickupHourID, adults, children, infants, students, priceAdults, priceChildren, priceInfants, priceStudents)
        Return response
    End Function

    Public Function SaveCompanySession(SessionID As Guid, CompanyCode As String, Language As String) As Response Implements ISessionServices.SaveCompanySession
        Dim response As Response
        Dim Session As New Session
        response = Session.SaveCompanySession(SessionID, CompanyCode, Language)
        Return response
    End Function

    Public Function SaveCompanySessionService(SessionID As Guid, CompanyCode As String, Language As String) As Response Implements ISessionServices.SaveCompanySessionService
        Dim response As Response
        Dim Session As New Session
        response = Session.SaveCompanySessionService(SessionID, CompanyCode, Language)
        Return response
    End Function

    Public Function UpdateAddons(SessionID As String, Addons As String) As Response Implements ISessionServices.UpdateAddons
        Dim response As Response
        Dim Session As New Session
        response = Session.UpdateAddons(SessionID, Addons)
        Return response
    End Function

    Public Function UpdatePaypalToken(SessionID As String, PayPalToken As String) As Response Implements ISessionServices.UpdatePaypalToken
        Dim response As Response
        Dim Session As New Session
        response = Session.UpdatePaypalToken(SessionID, PayPalToken)
        Return response
    End Function

    'Public Function UpdateSessionRate(SessionID As Guid, rateCode As String, arrivalDate As Date, departureDate As Date, rooms As Integer, room1Qty As String, room2Qty As String, room3Qty As String, room4Qty As String, promoID As Integer, resellerID As String) As Response Implements ISessionServices.UpdateSessionRate
    '    Dim response As Response
    '    Dim Session As New Session
    '    response = Session.UpdateSessionRate(SessionID, rateCode, arrivalDate, departureDate, rooms, room1Qty, room2Qty, room3Qty, room4Qty, promoID, resellerID)
    '    Return response
    'End Function

    Public Function UpdateSessionRooms(SessionID As String, RateCode As String, Room1Code As String, Room2Code As String, Room3Code As String, Room4Code As String) As Response Implements ISessionServices.UpdateSessionRooms
        Dim response As Response
        Dim Session As New Session
        response = Session.UpdateSessionRooms(SessionID, RateCode, Room1Code, Room2Code, Room3Code, Room4Code)
        Return response
    End Function

    Public Function UpdateSessionService(SessionID As Guid, arrivalDate As Date, services As Integer, adults As Integer, children As Integer, infants As Integer, students As Integer, promoID As Integer, resellerID As String) As Response Implements ISessionServices.UpdateSessionService
        Dim response As Response
        Dim Session As New Session
        response = Session.UpdateSessionService(SessionID, arrivalDate, services, adults, children, infants, students, promoID, resellerID)
        Return response
    End Function

    Public Function UpdateStatus(company_Code As String, Active As Integer) As Response Implements ISessionServices.UpdateStatus
        Dim response As Response
        Dim Session As New Session
        response = Session.UpdateStatus(company_Code, Active)
        Return response
    End Function

End Class
