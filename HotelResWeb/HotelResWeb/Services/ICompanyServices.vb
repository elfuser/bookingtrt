﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface ICompanyServices

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompany(ByVal CompanyCode As String, ByVal CompanyName As String, ByVal LegalName As String, ByVal LegalID As String, ByVal Address As String, ByVal Tel1 As String, ByVal Tel2 As String,
                                ByVal Tel3 As String, ByVal Fax As String, ByVal ContactName As String, ByVal ContactEmail As String, ByVal Web As String, ByVal Email As String,
                                ByVal CallTollFree As String, ByVal sMode As String, ByVal BankName As String, ByVal BankAcct As String,
                                ByVal CfoName As String, ByVal CfoEmail As String, ByVal ResName As String, ByVal ResEmail As String, ByVal ResEmailAlt As String,
                                ByVal FacebookURL As String, ByVal TwitterText As String, ByVal Hotel As Boolean, ByVal Services As Boolean,
                                ByVal GoogleID As String, ByVal GoogleConvID As String, ByVal GoogleLabel As String, ByVal Active As Boolean, ByVal UserID As Integer, ByVal Migrar As Boolean) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyHotel", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyHotel(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer,
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyService(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal RentPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal MaxStudents As Integer,
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal MsgStudents As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyTerms", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyTerms(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sTerms As String, ByVal iCultureID As Integer) As Integer

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyWaiver", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyWaiver(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sWaiver As String, ByVal iCultureID As Integer) As Integer

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyProcessor", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyProcessor(ByVal CompanyCode As String, ByVal sMode As String, ByVal Commission As Double, ByVal ProcessorID As String,
                                         ByVal KeyID As String, ByVal KeyHash As String, ByVal Type As String, ByVal URL As String, ByVal Username As String) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompany?sCompany_Code={sCompany_Code}&sType={sType}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompany(ByVal sCompany_Code As String, ByVal sType As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyHotel?sCompany_Code={sCompany_Code}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyHotel(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyService?sCompany_Code={sCompany_Code}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyService(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyProcessor?sCompany_Code={sCompany_Code}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyProcessor(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteCompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteCompany(ByVal CompanyCode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyTerm?sCompany_Code={sCompany_Code}&sMode={sMode}&iCulture={iCulture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyTerm(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyWaiver?sCompany_Code={sCompany_Code}&sMode={sMode}&iCulture={iCulture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyWaiver(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyPrefix?sPrefix={sPrefix}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyPrefix(ByVal sPrefix As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyServicePrefix?sPrefix={sPrefix}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyServicePrefix(ByVal sPrefix As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetBankBNKeys?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetBankBNKeys(ByVal sCompanyCode As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyEmails?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyEmails(ByVal sCompanyCode As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyData?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyData(ByVal sCompanyCode As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyGoogleInfo?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyGoogleInfo(ByVal sCompanyCode As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanyTermsEmails?sCompanyCode={sCompanyCode}&sCulture={sCulture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanyTermsEmails(ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of CompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPayPalSubject?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPayPalSubject(ByVal sCompanyCode As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetBankCredomaticKeys?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetBankCredomaticKeys(ByVal sCompanyCode As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFacilities?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFacilities(ByVal sCompanyCode As String) As List(Of clsFeaturesInfo)
End Interface
