﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface IQueryServices


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryPrices?CompanyCode={CompanyCode}&CurrentDate={CurrentDate}&RoomType={RoomType}&PromoCode={PromoCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryPrices(ByVal CompanyCode As String, ByVal CurrentDate As Date, RoomType As String, ByVal PromoCode As String) As List(Of PricesDTO)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservations?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservation)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationConfirmations?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}&Range={Range}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationConfirmations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationAccount?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}&Range={Range}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationAccount(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationAccountOS?CompanyCode={CompanyCode}&DateFrom={DateFrom}&DateTo={DateTo}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationAccountOS(ByVal CompanyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryReservationConfirmation)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationRoomConfirmation?Reservation={Reservation}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationRoomConfirmation(ByVal Reservation As String) As List(Of QryRoom)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationServiceItems?SaleID={SaleID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationServiceItems(ByVal SaleID As String) As List(Of QrySaleItems)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationAddonConfirmations?Reservation={Reservation}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationAddonConfirmations(ByVal Reservation As String) As List(Of QryAddon)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryAvailability?CompanyCode={CompanyCode}&RateCode={RateCode}&DateFrom={DateFrom}&DateTo={DateTo}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryAvailability(ByVal CompanyCode As String, RateCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryAvailability)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryCalendar?CompanyCode={CompanyCode}&Month={Month}&Year={Year}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryCalendar(ByVal CompanyCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryCalendarService?ServiceID={ServiceID}&Month={Month}&Year={Year}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryCalendarService(ByVal ServiceID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryCalendarRoom?CompanyCode={CompanyCode}&RoomTypeCode={RoomTypeCode}&Month={Month}&Year={Year}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryCalendarRoom(ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryCalendarServiceSchedule?ServiceID={ServiceID}&ScheduleID={ScheduleID}&Month={Month}&Year={Year}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryCalendarServiceSchedule(ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservationBankLog?Reservation={Reservation}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservationBankLog(ByVal Reservation As String) As List(Of BankLogInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryQuotations?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryQuotations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservationConfirmation)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryAvailabilityDay?CompanyCode={CompanyCode}&CurrentDate={CurrentDate}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryAvailabilityDay(ByVal CompanyCode As String, ByVal CurrentDate As String) As List(Of DayAvailability)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryCountries", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryCountries() As List(Of CountriesInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/QuerySales?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}&Range={Range}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QuerySales(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QrySale)


End Interface
