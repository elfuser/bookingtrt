﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface IRateServices

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateDetails", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateDetails(ByVal RateDetID As Long, ByVal RateID As Integer, RoomtypeID As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Double,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal Wadult1 As Double, ByVal Wadult2 As Double, ByVal Wadult3 As Double, ByVal Wadult4 As Double, ByVal Wadult5 As Double,
                                    ByVal Wadult6 As Double, ByVal Wadult7 As Double, ByVal Wadult8 As Double, ByVal Wadult9 As Double, ByVal Wadult10 As Double, ByVal Wchild As Double,
                                    CompanyID As String, ByVal sMode As String, ByVal roomcode As Integer) As Response


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateHeader", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateHeader(ByVal RateID As Integer, ByVal RateCode As String, ByVal Description As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal MinLos As Integer, ByVal MaxLos As Integer, ByVal Enabled As Boolean, ByVal WeekEnd As Boolean,
                                   ByVal Type As String, ByVal RateDetail As String, ByVal OrderList As Integer, ByVal FreeNights As Integer, ByVal Conditions As String, ByVal sMode As String) As Response


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateLocale(ByVal RateID As Long, ByVal CultureID As Integer, ByVal Description As String,
                                   ByVal LongDescription As String, ByVal Conditions As String, ByVal sMode As String) As Response


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRateHeader", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRateHeader(ByVal RateID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateHeader?iRateID={iRateID}&sCompanyCode={sCompanyCode}&bSelectAll={bSelectAll}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateHeader(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RatesInfo)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomsAvailable?iRateID={iRateID}&sCompanyCode={sCompanyCode}&bSelectAll={bSelectAll}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomsAvailable(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RateLocaleInfo)


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateRoomType", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateRoomType(ByVal RateDetID As Long, ByVal RoomTypeID As Integer, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal sMode As String, ByVal RateID As Integer) As String

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityByVal", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityByVal(RateCode As String, ByVal RoomTypeID As Integer, ByVal Type As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal oldStartDate As Date, oldEndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, Adult1w As Double, Adult2w As Double,
                                    ByVal Adult3w As Double, ByVal Adult4w As Double, ByVal Adult5w As Double, ByVal Adult6w As Double, ByVal Adult7w As Double, ByVal Adult8w As Double,
                                    ByVal Adult9w As Double, ByVal Adult10w As Double, ByVal Child As Double, ByVal Childw As Double,
                                    ByVal CompanyID As String, ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRateDetails", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRateDetails(ByVal RateDetID As Integer, CompanyCode As String, RoomTypeCode As String, StartDate As Date, EndDate As Date) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateDetails?iRateDetID={iRateDetID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateDetails(ByVal iRateDetID As Integer) As List(Of RateDetailInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateDates?iRateDetID={iRateDetID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateDates(ByVal iRateDetID As Integer) As List(Of RateDetailInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateExcDates?sCompanyCode={sCompanyCode}&sRateCode={sRateCode}&sRoomTypeCode={sRoomTypeCode}&dStart={dStart}&dEnd={dEnd}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateExcDates(sCompanyCode As String, ByVal sRateCode As String, sRoomTypeCode As String, ByVal dStart As Date, ByVal dEnd As Date) As List(Of RateDetailInfo)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateRoomType?RateDetID={RateDetID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateRoomType(ByVal RateDetID As Integer, sCompanyCode As String) As List(Of RateRoomTypeInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRateRoomType", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRateRoomType(ByVal RateDetID As Integer, ByVal RoomTypeID As Integer) As Response


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateException", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateException(ByVal CompanyCode As String, RateCode As String, RoomtypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                      ByVal StartDateAnt As Date, ByVal EndDateAnt As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRates?sCompanyId={sCompanyId}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&iAdults={iAdults}&iChildren={iChildren}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer) As List(Of RatesInfo)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRates?sCompanyId={sCompanyId}&RoomType={RoomType}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&iAdults={iAdults}&iChildren={iChildren}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRates_v1(ByVal sCompanyId As String, ByVal RoomType As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer,
                             ByVal iChildren As Integer) As List(Of RatesInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRatesWithPromoCode?sCompanyId={sCompanyId}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&iAdults={iAdults}&iChildren={iChildren}&sPromoCode={sPromoCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRatesWithPromoCode(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer, ByVal sPromoCode As String) As List(Of RatesInfo)



    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateDiscount", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateDiscount(ByVal RowID As Integer, ByVal CompanyCode As String, RateCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Disc1 As Double, ByVal Disc2 As Double, ByVal Disc3 As Double, ByVal Disc4 As Double, ByVal Disc5 As Integer,
                                    ByVal Disc6 As Double, ByVal Disc7 As Double, ByVal Disc8 As Double, ByVal Discount As Double, ByVal Type As Integer,
                                    ByVal sMode As String) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateDiscounts?companyCode={companyCode}&rateCode={rateCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateDiscounts(ByVal companyCode As String, ByVal rateCode As String) As List(Of RateDiscountInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateDiscount?iRowID={iRowID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateDiscount(ByVal iRowID As Integer) As List(Of RateDiscountInfo)


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRateDiscount", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRateDiscount(rowID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/GetRatesFreeNights", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRatesFreeNights(ByVal CompanyCode As String, ByVal RateCode As String) As Integer


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAvailabilityRates?sCompanyId={sCompanyId}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&Adults={Adults}&Children={Children}&RoomCode={RoomCode}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAvailabilityRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date,
                                     ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer,
                                     ByVal RoomCode As String, ByVal Culture As String) As List(Of RatesInfo)
End Interface
