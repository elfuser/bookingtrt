﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules
' NOTE: You can use the "Rename" command on the context menu to change the interface name "ISalesService" in both code and config file together.
<ServiceContract()>
Public Interface ISalesService


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveSale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveSale(ByVal sMode As String, ByVal sProcessor As String, ByVal objRes As clsSaleInfo) As String

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveSaleItem", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveSaleItem(ByVal sMode As String, ByVal objRes As clsSaleItemInfo) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetSale?SaleID={SaleID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetSale(ByVal SaleID As String) As List(Of clsSaleInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetSaleItems?SaleID={SaleID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetSaleItems(ByVal SaleID As String, ByVal Culture As String) As List(Of clsSaleItemInfo)

    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailability", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailability(ByVal ResNumber As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateStatus", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16) As Response


End Interface
