﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface ISessionServices

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanySession", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanySession(ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response

    '<OperationContract>
    '<WebInvoke(Method:="POST", UriTemplate:="/UpdateSessionRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    'Function UpdateSessionRate(ByVal SessionID As System.Guid, ByVal rateCode As String,
    '                                     ByVal arrivalDate As Date, ByVal departureDate As Date, ByVal rooms As Integer,
    '                                     ByVal room1Qty As String, ByVal room2Qty As String,
    '                                     ByVal room3Qty As String, ByVal room4Qty As String,
    '                                     ByVal promoID As Integer, ByVal resellerID As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateSessionService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateSessionService(ByVal SessionID As System.Guid,
                                     ByVal arrivalDate As Date, ByVal services As Integer,
                                     ByVal adults As Integer, ByVal children As Integer,
                                     ByVal infants As Integer, ByVal students As Integer,
                                     ByVal promoID As Integer, ByVal resellerID As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateSessionRooms", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateSessionRooms(ByVal SessionID As String, ByVal RateCode As String, ByVal Room1Code As String, ByVal Room2Code As String, ByVal Room3Code As String, ByVal Room4Code As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdatePaypalToken", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdatePaypalToken(ByVal SessionID As String, ByVal PayPalToken As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateAddons", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateAddons(ByVal SessionID As String, ByVal Addons As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateStatus", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateStatus(ByVal company_Code As String, ByVal Active As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/CheckSessionID?SessionID={SessionID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function CheckSessionID(ByVal SessionID As String) As Boolean

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/CheckSessionServiceID?SessionID={SessionID", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function CheckSessionServiceID(ByVal SessionID As String) As Boolean

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanySessionByReservationID?ReservationID={ReservationID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanySessionByReservationID(ByVal ReservationID As String) As List(Of SessionInfo)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanySession?SessionID={SessionID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanySession(ByVal SessionID As String) As List(Of SessionInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanySessionService?Mode={Mode}&SessionID={SessionID}&SaleID={SaleID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanySessionService(ByVal Mode As String, ByVal SessionID As String, ByVal SaleID As String) As List(Of SessionServiceInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanySessionService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanySessionService(ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/InsertSessionServiceSC", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function InsertSessionServiceSC(ByVal SessionID As String, ByVal serviceID As Integer,
                             ByVal arrivalDate As Date, ByVal ScheduleID As Integer,
                             ByVal hotelPickupID As Integer, ByVal hotelPickupHourID As Integer,
                             ByVal adults As Integer, ByVal children As Integer,
                             ByVal infants As Integer, ByVal students As Integer,
                             ByVal priceAdults As Double, ByVal priceChildren As Double,
                             ByVal priceInfants As Double, ByVal priceStudents As Double) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetSessionServiceSC?SessionID={SessionID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetSessionServiceSC(ByVal SessionID As String, ByVal Culture As String) As List(Of ServiceSCInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRowSC", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRowSC(ByVal RowID As Long, ByVal SessionID As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCompanySessionByPPToken?PayPalToken={PayPalToken}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCompanySessionByPPToken(ByVal PayPalToken As String) As List(Of SessionInfo)


End Interface
