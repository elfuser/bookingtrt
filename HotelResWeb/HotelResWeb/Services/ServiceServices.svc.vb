﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "ServiceServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ServiceServices.svc or ServiceServices.svc.vb at the Solution Explorer and start debugging.
Imports BRules
Imports HotelResWeb

Public Class ServiceServices
    Implements IServiceServices

    Public Function DeleteService(ServiceID As Integer) As String Implements IServiceServices.DeleteService
        Dim Service As New Service
        Dim response As String
        response = Service.DeleteService(ServiceID)
        Return response
    End Function

    Public Function DeleteServiceInventory(ServiceID As Integer, ScheduleID As Integer) As String Implements IServiceServices.DeleteServiceInventory
        Dim Service As New Service
        Dim response As String
        response = Service.DeleteServiceInventory(ServiceID, ScheduleID)
        Return response
    End Function

    Public Function DeleteServiceRate(RateID As Integer, ServiceID As Integer, StartDate As Date, EndDate As Date) As String Implements IServiceServices.DeleteServiceRate
        Dim Service As New Service
        Dim response As String
        response = Service.DeleteServiceRate(RateID, ServiceID, StartDate, EndDate)
        Return response
    End Function

    Public Function GetCategories(Culture As String) As List(Of CategoryInfo) Implements IServiceServices.GetCategories
        Dim Service As New Service
        Dim response As List(Of CategoryInfo)
        response = Service.GetCategories(Culture)
        Return response
    End Function

    'Public Function GetDayAvailability(serviceID As Integer, startDate As Date, endDate As Date) As List(Of DayAvailability) Implements IServiceServices.GetDayAvailability
    '    Dim Service As New Service
    '    Dim response As List(Of DayAvailability)
    '    response = Service.GetDayAvailability(serviceID, startDate, endDate)
    '    Return response
    'End Function

    Public Function GetServiceAvailability(sCompanyCode As String, CategoryID As Integer, Culture As String, ArrivalDate As Date) As List(Of ServiceInfo) Implements IServiceServices.GetServiceAvailability
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServiceAvailability(sCompanyCode, CategoryID, Culture, ArrivalDate)
        Return response
    End Function

    Public Function GetServiceByID(ServiceID As Integer, Culture As String) As List(Of ServiceInfo) Implements IServiceServices.GetServiceByID
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServiceByID(ServiceID, Culture)
        Return response
    End Function

    Public Function GetServiceByIDArrival(ServiceID As Integer, ArrivalDate As Date, Culture As String) As List(Of ServiceInfo) Implements IServiceServices.GetServiceByIDArrival
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServiceByID(ServiceID, ArrivalDate, Culture)
        Return response
    End Function

    Public Function GetServiceCondInfo(iServiceID As Integer, iCultureID As Integer, sType As String) As String Implements IServiceServices.GetServiceCondInfo
        Dim Service As New Service
        Dim response As String
        response = Service.GetServiceCondInfo(iServiceID, iCultureID, sType)
        Return response
    End Function

    Public Function GetServiceConditions(ServiceID As Integer, Culture As String) As String Implements IServiceServices.GetServiceConditions
        Dim Service As New Service
        Dim response As String
        response = Service.GetServiceConditions(ServiceID, Culture)
        Return response
    End Function

    Public Function GetServiceInventory(iServiceID As Integer) As List(Of ServiceInventoryInfo) Implements IServiceServices.GetServiceInventory
        Dim Service As New Service
        Dim response As List(Of ServiceInventoryInfo)
        response = Service.GetServiceInventory(iServiceID)
        Return response
    End Function

    Public Function GetServiceLocale(iServiceID As Integer, iCultureID As Integer) As List(Of ServiceLocaleInfo) Implements IServiceServices.GetServiceLocale
        Dim Service As New Service
        Dim response As List(Of ServiceLocaleInfo)
        response = Service.GetServiceLocale(iServiceID, iCultureID)
        Return response
    End Function

    Public Function GetServicePickup(CompanyCode As String) As List(Of ServiceInfo) Implements IServiceServices.GetServicePickup
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServicePickup(CompanyCode)
        Return response
    End Function

    Public Function GetServicePickupHours(PlaceID As Integer) As List(Of ServiceInfo) Implements IServiceServices.GetServicePickupHours
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServicePickupHours(PlaceID)
        Return response
    End Function

    Public Function GetServiceRates(iRateID As Integer, iServiceID As Integer) As List(Of ServiceRateInfo) Implements IServiceServices.GetServiceRates
        Dim Service As New Service
        Dim response As List(Of ServiceRateInfo)
        response = Service.GetServiceRates(iRateID, iServiceID)
        Return response
    End Function

    Public Function GetServices(iServiceID As Integer, sCompanyCode As String, Culture As String, Mode As String) As List(Of ServiceInfo) Implements IServiceServices.GetServices
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServices(iServiceID, sCompanyCode, Culture, Mode)
        Return response
    End Function

    Public Function GetServiceSchedAvailability(ServiceID As Integer, ArrivalDate As Date, QtyPeople As Short, PickupHour As String) As List(Of ServiceAvailability) Implements IServiceServices.GetServiceSchedAvailability
        Dim Service As New Service
        Dim response As List(Of ServiceAvailability)
        response = Service.GetServiceSchedAvailability(ServiceID, ArrivalDate, QtyPeople, PickupHour)
        Return response
    End Function

    Public Function GetServiceSchedInventory(ScheduleID As Integer) As Integer Implements IServiceServices.GetServiceSchedInventory
        Dim Service As New Service
        Dim response As Integer
        response = Service.GetServiceSchedInventory(ScheduleID)
        Return response
    End Function

    Public Function GetServicesList(sCompanyCode As String, sCulture As String) As List(Of ServiceInfo) Implements IServiceServices.GetServicesList
        Dim Service As New Service
        Dim response As List(Of ServiceInfo)
        response = Service.GetServicesList(sCompanyCode, sCulture)
        Return response
    End Function

    Public Function GetServiceWhatToBring(ServiceID As Integer, Culture As String) As String Implements IServiceServices.GetServiceWhatToBring
        Dim Service As New Service
        Dim response As String
        response = Service.GetServiceWhatToBring(ServiceID, Culture)
        Return response
    End Function

    Public Function LoadTerms(CompanyCode As String, CultureID As String) As String Implements IServiceServices.LoadTerms
        Dim Service As New Service
        Dim response As String
        response = Service.LoadTerms(CompanyCode, CultureID)
        Return response
    End Function

    Public Function LoadWaiver(CompanyCode As String, CultureID As String) As String Implements IServiceServices.LoadWaiver
        Dim Service As New Service
        Dim response As String
        response = Service.LoadWaiver(CompanyCode, CultureID)
        Return response
    End Function

    Public Function SavePickupService(sMode As String, CompanyCode As String, PlaceID As Integer, Name As String, Active As Boolean) As String Implements IServiceServices.SavePickupService
        Dim Service As New Service
        Dim response As String
        response = Service.SavePickupService(sMode, CompanyCode, PlaceID, Name, Active)
        Return response
    End Function

    Public Function SaveService(CompanyCode As String, ServiceID As Integer, Description As String, Details As String, CategoryID As Integer, sMode As String, Code As String, ListOrder As Integer, VideoURL As String, Culture As String, Difficulty As Integer, Inventory As Boolean, Active As Boolean) As String Implements IServiceServices.SaveService
        Dim Service As New Service
        Dim response As String
        response = Service.SaveService(CompanyCode, ServiceID, Description, Details,
                                 CategoryID, sMode, Code,
                                 ListOrder, VideoURL, Culture, Difficulty,
                                 Inventory, Active)
        Return response
    End Function

    Public Function SaveServiceBringInfo(ServiceID As Integer, Culture As Integer, BringInfo As String) As Response Implements IServiceServices.SaveServiceBringInfo
        Dim Service As New Service
        Dim response As Response
        response = Service.SaveServiceBringInfo(ServiceID, Culture, BringInfo)
        Return response
    End Function

    Public Function SaveServiceConditions(ServiceID As Integer, CultureID As Integer, Conditions As String) As Response Implements IServiceServices.SaveServiceConditions
        Dim Service As New Service
        Dim response As Response
        response = Service.SaveServiceConditions(ServiceID, CultureID, Conditions)
        Return response
    End Function

    Public Function SaveServiceInventory(sMode As String, ScheduleID As Integer, ServiceID As Integer, Hour As String, Qty As Integer) As Response Implements IServiceServices.SaveServiceInventory
        Dim Service As New Service
        Dim response As Response
        response = Service.SaveServiceInventory(sMode, ScheduleID, ServiceID, Hour, Qty)
        Return response
    End Function

    Public Function SaveServiceLocale(ServiceID As Integer, CultureID As Integer, Description As String, LongDescription As String, sMode As String) As Response Implements IServiceServices.SaveServiceLocale
        Dim Service As New Service
        Dim response As Response
        response = Service.SaveServiceLocale(ServiceID, CultureID, Description, LongDescription, sMode)
        Return response
    End Function



    Public Function SaveServiceRate(sMode As String, RateID As Integer, ServiceID As Integer, Description As String,
                                    StartDate As Date, EndDate As Date, OldStartDate As Date, OldEndDate As Date,
                                    Adults As Double, Children As Double, Infant As Double, Students As Double,
                                    MaxAdult As Integer, MaxChildren As Integer, MaxInfant As Integer,
                                    MaxStudent As Integer, Active As Boolean) As Response Implements IServiceServices.SaveServiceRate
        Dim Service As New Service
        Dim response As Response
        response = Service.SaveServiceRate(sMode, RateID, ServiceID, Description,
                                     StartDate, EndDate, OldStartDate, OldEndDate, Adults, Children,
                                     Infant, Students, MaxAdult, MaxChildren, MaxInfant,
                                     MaxStudent, Active)
        Return response
    End Function

    Public Function UpdateStatus(ResNumber As String, Status As Short) As Response Implements IServiceServices.UpdateStatus
        Dim Service As New Service
        Dim response As Response
        response = Service.UpdateStatus(ResNumber, Status)
        Return response
    End Function
End Class
