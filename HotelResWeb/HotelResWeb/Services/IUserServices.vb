﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface IUserServices



    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUser?iUserID={iUserID}&sCompanyCode={sCompanyCode}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUser(ByVal iUserID As Integer, ByVal sCompanyCode As String, ByVal sMode As String) As List(Of UserInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/LoginUser", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function LoginUser(ByVal sUserName As String) As String


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/Last5Passwords", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function Last5Passwords(ByVal user_id As Integer) As String

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteUser", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteUser(ByVal iUserID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveLastPassword", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveLastPassword(ByVal iUserID As Integer, ByVal dateChanged As Date, ByVal password As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveUserLog", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveUserLog(ByVal iUserID As Integer, ByVal iRefID As Long, ByVal sEvent As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveUserCompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteUserCompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUserLog?iUserID={iUserID}&dFrom={dFrom}&dTo={dTo}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUserLog(ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserLogInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/BlockUser", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function BlockUser(ByVal iUserID As Integer, ByVal bActive As Boolean) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUserCompany?iUserID={iUserID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUserCompany(ByVal iUserID As Integer) As List(Of UserCompanyInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAPIUserCompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAPIUserCompany(ByVal sMode As String, ByVal sCompanyCode As String, ByVal iUserID As String, ByVal sPassword As String, ByVal bActive As Boolean, ByVal iUserCompany As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUserAPICompany?sUserID={sUserID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUserAPICompany(ByVal sUserID As String) As List(Of UserAPIInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUsersAPI", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUsersAPI() As List(Of UserAPIInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteUserAPICompany", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteUserAPICompany(ByVal sUserID As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/BlockAPIUser", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function BlockAPIUser(ByVal sUserID As String, ByVal bActive As Boolean) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetUserAPILog?sUserID={sUserID}&dFrom={dFrom}&dTo={dTo}&sMode={sMode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetUserAPILog(ByVal sUserID As String, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserAPILogInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetProfile?iProfileID={iProfileID}&sProfileInfoCode={sProfileInfoCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetProfile(ByVal iProfileID As Integer, ByVal sProfileInfoCode As String) As List(Of ProfileInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveProfile", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveProfile(ByVal iProfileID As Integer, ByVal sProfileName As String, ByVal sCompanyCode As String, ByVal sMode As String) As Integer

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteProfilePermissions", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteProfilePermissions(ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetProfilePermissions?iPermissionId={iPermissionId}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetProfilePermissions(ByVal iPermissionId As Integer, ByVal iProfileID As Integer) As List(Of ProfileInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveProfilePermissions", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveProfilePermissions(ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveUser", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveUser(ByVal CompanyCode As String, ByVal Password As String,
                        ByVal Blocked As Boolean) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetLog?CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetLog(ByVal CompanyCode As String) As List(Of SyncLogInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveLog", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveLog(ByVal CompanyCode As String, ByVal Message As String,
                            ByVal IpAddress As String, ByVal LogLevel As Integer) As Response

End Interface
