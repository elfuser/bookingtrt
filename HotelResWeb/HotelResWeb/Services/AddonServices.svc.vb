﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "AddonServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select AddonServices.svc or AddonServices.svc.vb at the Solution Explorer and start debugging.
Imports BRules


Public Class AddonServices
    Implements IAddonServices

    Public Function GetAddonEnabled(ByVal sCompanyCode As String) As List(Of AddonInfo) Implements IAddonServices.GetAddonEnabled
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonInfo)

        list = AddOns.GetAddonEnabled(sCompanyCode)

        Return list

    End Function


    Public Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo) Implements IAddonServices.GetCulture
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of CultureInfo)

        list = AddOns.GetCulture(iCultureID)

        Return list

    End Function


    Public Function GetAddons(ByVal iAddonID As Long, ByVal sCompanyCode As String) As List(Of AddonInfo) Implements IAddonServices.GetAddons
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonInfo)

        list = AddOns.GetAddons(iAddonID, sCompanyCode)


        Return list
    End Function


    Public Function GetAddonRates(ByVal iRateID As Integer, ByVal iAddonID As Integer) As List(Of AddonRateInfo) Implements IAddonServices.GetAddonRates
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonRateInfo)

        list = AddOns.GetAddonRates(iRateID, iAddonID)

        Return list
    End Function


    Public Function GetAddonLocale(ByVal iAddonID As Long, ByVal iCultureID As Integer) As List(Of AddonLocaleInfo) Implements IAddonServices.GetAddonLocale
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonLocaleInfo)

        list = AddOns.GetAddonLocale(iAddonID, iCultureID)

        Return list
    End Function



    Public Function SaveAddon(ByVal AddonID As Long, ByVal Description As String, ByVal CompanyCode As String, ByVal Amount As Double, Enabled As Boolean, ByVal RowOrder As Integer, ByVal sMode As String,
                              ByVal LongDes As String, ByVal Departure As String, ByVal Duration As String, ByVal AddonType As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Amount1 As Double, ByVal Amount2 As Double, ByVal Amount3 As Double, ByVal Amount4 As Double,
                              ByVal Amount5 As Double, ByVal Amount6 As Double, ByVal Amount7 As Double, ByVal Amount8 As Double, ByVal Amount9 As Double, ByVal Amount10 As Double, ByVal Tax As Boolean) As String Implements IAddonServices.SaveAddon

        Dim AddOns As New BRules.Addons
        Dim response As String

        response = AddOns.SaveAddon(AddonID, Description, CompanyCode, Amount, Enabled, RowOrder, sMode,
                              LongDes, Departure, Duration, AddonType,
                              DateFrom, DateTo, Amount1, Amount2, Amount3, Amount4,
                              Amount5, Amount6, Amount7, Amount8, Amount9, Amount10, Tax)

        Return response

    End Function


    Public Function SaveAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response Implements IAddonServices.SaveAddonLocale
        Dim AddOns As New BRules.Addons
        Dim response As New Response

        response = AddOns.SaveAddonLocale(AddonID, CultureID, Description, LongDescription, sMode)


        Return response
    End Function


    Public Function DeleteAddon(ByVal AddonID As Long) As Response Implements IAddonServices.DeleteAddon
        Dim AddOns As New BRules.Addons
        Dim response As New Response

        response = AddOns.DeleteAddon(AddonID)


        Return response
    End Function

    Public Function DeleteAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer) As Response Implements IAddonServices.DeleteAddonLocale
        Dim AddOns As New BRules.Addons
        Dim response As New Response


        response = AddOns.DeleteAddonLocale(AddonID, CultureID)

        Return response
    End Function

    Public Function DeleteAddonRate(ByVal RateID As Integer) As Response Implements IAddonServices.DeleteAddonRate
        Dim AddOns As New BRules.Addons
        Dim response As New Response

        response = AddOns.DeleteAddonRate(RateID)

        Return response
    End Function




    Public Function SaveAddonRate(ByVal sMode As String, ByVal RateID As Integer, ByVal AddonID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal Type As String, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer,
                                    ByVal MaxStudent As Integer, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer, ByVal Price As Double,
                                    ByVal Adults As Double, ByVal Children As Double, ByVal Infant As Double, ByVal Students As Double, ByVal Active As Boolean) As Response Implements IAddonServices.SaveAddonRate

        Dim AddOns As New BRules.Addons
        Dim response As New Response

        response = AddOns.SaveAddonRate(sMode, RateID, AddonID, Description,
                                    StartDate, EndDate, Type, MaxAdult, MaxChildren,
                                    MaxStudent, MaxOccupance, MinOccupance, Price,
                                    Adults, Children, Infant, Students, Active)


        Return response
    End Function


    Public Function GetAddonQtys(ByVal companyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of AddonInfo) Implements IAddonServices.GetAddonQtys
        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonInfo)

        list = AddOns.GetAddonQtys(companyCode, DateFrom, DateTo)

        Return list
    End Function


    Public Function GetAddonsByCategory(ByVal companyCode As String,
                      ByVal DateFrom As Date, ByVal DateTo As Date, culture As String, Category As String) As List(Of AddonInfo) Implements IAddonServices.GetAddonsByCategory

        Dim AddOns As New BRules.Addons
        Dim list As New List(Of AddonInfo)

        list = AddOns.GetAddons(companyCode, DateFrom, DateTo, culture, Category)

        Return list
    End Function


End Class
