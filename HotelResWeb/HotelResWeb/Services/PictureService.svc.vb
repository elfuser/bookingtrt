﻿Imports BRules
' NOTE: You can use the "Rename" command on the context menu to change the class name "PictureService" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select PictureService.svc or PictureService.svc.vb at the Solution Explorer and start debugging.
Public Class PictureService
    Implements IPictureService

    Public Function SaveRoomPhoto(roomTypeID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response Implements IPictureService.SaveRoomPhoto

        Dim response As New Response
        Dim objPicture As New Picture

        response = objPicture.SaveToDatabaseRoomPhoto(roomTypeID, photoNumber, pictureStream)

        Return response
    End Function


    Public Function SaveCompanyPhoto(companyCode As String, photoNumber As Integer, pictureStream As Byte()) As Response Implements IPictureService.SaveCompanyPhoto
        Dim response As New Response
        Dim objPicture As New Picture

        response = objPicture.SaveToDatabaseCompanyPhoto(companyCode, photoNumber, pictureStream)

        Return response
    End Function


    Public Function SaveServicePhoto(serviceID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response Implements IPictureService.SaveServicePhoto
        Dim response As New Response
        Dim objPicture As New Picture

        response = objPicture.SaveToDatabaseServicePhoto(serviceID, photoNumber, pictureStream)

        Return response
    End Function


    Public Function DownloadRoomPhoto(RoomTypeID As Integer, photoNumber As Integer) As PictureFile Implements IPictureService.DownloadRoomPhoto
        Dim pictureFile As New PictureFile
        Dim objPicture As New Picture

        pictureFile = objPicture.DownloadRoomPhoto(RoomTypeID, photoNumber)

        Return pictureFile
    End Function


    Public Function DownloadCompanyPhoto(companyCode As String, photoNumber As Integer) As PictureFile Implements IPictureService.DownloadCompanyPhoto
        Dim pictureFile As New PictureFile
        Dim objPicture As New Picture

        pictureFile = objPicture.DownloadCompanyPhoto(companyCode, photoNumber)

        Return pictureFile
    End Function


    Public Function DownloadServicePhoto(ServiceID As Integer, photoNumber As Integer) As PictureFile Implements IPictureService.DownloadServicePhoto
        Dim pictureFile As New PictureFile
        Dim objPicture As New Picture

        pictureFile = objPicture.DownloadServicePhoto(ServiceID, photoNumber)

        Return pictureFile
    End Function


End Class
