﻿Imports BRules
Imports HotelResWeb

Public Class UserServices
    Implements IUserServices


    Public Function GetUser(ByVal iUserID As Integer, ByVal sCompanyCode As String, ByVal sMode As String) As List(Of UserInfo) Implements IUserServices.GetUser

        Dim user As New Users
        Dim CompanyList As New List(Of UserInfo)()


        CompanyList = user.GetUser(iUserID, sCompanyCode, sMode)

        Return CompanyList



    End Function

    Public Function LoginUser(ByVal sUserName As String) As String Implements IUserServices.LoginUser

        Dim user As New Users
        Dim sResult As String = ""


        sResult = user.LoginUser(sUserName)

        Return sResult



    End Function
    Public Function DeleteUser(ByVal iUserID As Integer) As Response Implements IUserServices.DeleteUser

        Dim user As New Users
        Dim response As New Response

        response = user.DeleteUser(iUserID)

        Return response



    End Function

    Public Function Last5Passwords(ByVal user_id As Integer) As String Implements IUserServices.Last5Passwords

        Dim user As New Users
        Dim sResult As String = ""


        sResult = user.Last5Passwords(user_id)

        Return sResult



    End Function
    Public Function SaveLastPassword(ByVal iUserID As Integer, ByVal dateChanged As Date, ByVal password As String) As Response Implements IUserServices.SaveLastPassword

        Dim user As New Users
        Dim response As New Response


        response = user.SaveLastPassword(iUserID, dateChanged, password)



        Return response



    End Function

    Public Function SaveUserLog(ByVal iUserID As Integer, ByVal iRefID As Long, ByVal sEvent As String) As Response Implements IUserServices.SaveUserLog

        Dim user As New Users
        Dim response As New Response


        response = user.SaveUserLog(iUserID, iRefID, sEvent)



        Return response



    End Function
    Public Function SaveUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response Implements IUserServices.SaveUserCompany

        Dim user As New Users
        Dim response As New Response


        response = user.SaveUserCompany(sCompanyCode, iUserID)


        Return response



    End Function

    Public Function DeleteUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response Implements IUserServices.DeleteUserCompany

        Dim user As New Users
        Dim response As New Response


        response = user.DeleteUserCompany(sCompanyCode, iUserID)



        Return response



    End Function

    Public Function GetUserLog(ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserLogInfo) Implements IUserServices.GetUserLog

        Dim user As New Users
        Dim UserLogInfoList As New List(Of UserLogInfo)()


        UserLogInfoList = user.GetUserLog(iUserID, dFrom, dTo, sMode)

        Return UserLogInfoList



    End Function

    Public Function BlockUser(ByVal iUserID As Integer, ByVal bActive As Boolean) As Response Implements IUserServices.BlockUser

        Dim user As New Users
        Dim response As New Response


        response = user.BlockUser(iUserID, bActive)



        Return response



    End Function

    Public Function GetUserCompany(ByVal iUserID As Integer) As List(Of UserCompanyInfo) Implements IUserServices.GetUserCompany

        Dim user As New Users
        Dim UserCompanyInfoList As New List(Of UserCompanyInfo)()


        UserCompanyInfoList = user.GetUserCompany(iUserID)

        Return UserCompanyInfoList



    End Function

    Public Function SaveAPIUserCompany(ByVal sMode As String, ByVal sCompanyCode As String, ByVal iUserID As String, ByVal sPassword As String, ByVal bActive As Boolean, ByVal iUserCompany As Integer) As Response Implements IUserServices.SaveAPIUserCompany

        Dim user As New Users
        Dim response As New Response


        response = user.SaveAPIUserCompany(sMode, sCompanyCode, iUserID, sPassword, bActive, iUserCompany)



        Return response



    End Function

    Public Function GetUserAPICompany(ByVal sUserID As String) As List(Of UserAPIInfo) Implements IUserServices.GetUserAPICompany

        Dim user As New Users
        Dim UserAPIInfoList As New List(Of UserAPIInfo)()


        UserAPIInfoList = user.GetUserAPICompany(sUserID)

        Return UserAPIInfoList



    End Function

    Public Function GetUsersAPI() As List(Of UserAPIInfo) Implements IUserServices.GetUsersAPI

        Dim user As New Users
        Dim UserAPIInfoList As New List(Of UserAPIInfo)()


        UserAPIInfoList = user.GetUsersAPI()

        Return UserAPIInfoList



    End Function

    Public Function DeleteUserAPICompany(ByVal sUserID As String) As Response Implements IUserServices.DeleteUserAPICompany

        Dim user As New Users
        Dim response As New Response


        response = user.DeleteUserAPICompany(sUserID)



        Return response



    End Function

    Public Function BlockAPIUser(ByVal sUserID As String, ByVal bActive As Boolean) As Response Implements IUserServices.BlockAPIUser

        Dim user As New Users
        Dim response As New Response


        response = user.BlockAPIUser(sUserID, bActive)



        Return response



    End Function

    Public Function GetUserAPILog(ByVal sUserID As String, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserAPILogInfo) Implements IUserServices.GetUserAPILog

        Dim user As New Users
        Dim UserAPILogInfoList As New List(Of UserAPILogInfo)()


        UserAPILogInfoList = user.GetUserAPILog(sUserID, dFrom, dTo, sMode)

        Return UserAPILogInfoList



    End Function

    Public Function GetProfile(ByVal iProfileID As Integer, ByVal sProfileInfoCode As String) As List(Of ProfileInfo) Implements IUserServices.GetProfile

        Dim user As New Users
        Dim ProfileInfoList As New List(Of ProfileInfo)()


        ProfileInfoList = user.GetProfile(iProfileID, sProfileInfoCode)

        Return ProfileInfoList



    End Function

    Public Function SaveProfile(ByVal iProfileID As Integer, ByVal sProfileName As String, ByVal sCompanyCode As String, ByVal sMode As String) As Integer Implements IUserServices.SaveProfile

        Dim user As New Users
        Dim iResult As Integer


        iResult = user.SaveProfile(iProfileID, sProfileName, sCompanyCode, sMode)



        Return iResult



    End Function

    Public Function SaveProfilePermissions(ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response Implements IUserServices.SaveProfilePermissions

        Dim user As New Users
        Dim response As New Response


        response = user.SaveProfilePermissions(iPermissions, iPermissionId, iProfileID, sMode)



        Return response



    End Function
    Public Function GetProfilePermissions(ByVal iPermissionId As Integer, ByVal iProfileID As Integer) As List(Of ProfileInfo) Implements IUserServices.GetProfilePermissions
        Dim user As New Users
        Dim response As New List(Of ProfileInfo)
        response = user.GetProfilePermissions(iPermissionId, iProfileID)
        Return response
    End Function

    Public Function DeleteProfilePermissions(ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response Implements IUserServices.DeleteProfilePermissions

        Dim user As New Users
        Dim response As New Response
        response = user.DeleteProfilePermissions(iPermissionId)
        Return response

    End Function

    Public Function SaveUser(CompanyCode As String, Password As String, Blocked As Boolean) As Response Implements IUserServices.SaveUser
        Dim user As New Users
        Dim response As New Response
        response = user.SaveUser(CompanyCode, Password, Blocked)
        Return response
    End Function

    Public Function GetLog(CompanyCode As String) As List(Of SyncLogInfo) Implements IUserServices.GetLog
        Dim user As New Users
        Dim response As List(Of SyncLogInfo)
        response = user.GetLog(CompanyCode)
        Return response
    End Function

    Public Function SaveLog(CompanyCode As String, Message As String, IpAddress As String, LogLevel As Integer) As Response Implements IUserServices.SaveLog
        Dim user As New Users
        Dim response As New Response
        response = user.SaveLog(CompanyCode, Message, IpAddress, LogLevel)
        Return response
    End Function
End Class
