﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "PromoCodesService" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select PromoCodesService.svc or PromoCodesService.svc.vb at the Solution Explorer and start debugging.
Imports BRules

Public Class PromoCodesService
    Implements IPromoCodesService

    Public Function GetPromoCode(ByVal sCompanyCode As String, ByVal sPromoCode As String, ByVal sPromoType As String) As List(Of clsPromoCodeInfo) Implements IPromoCodesService.GetPromoCode
        Dim ListPromo As New List(Of clsPromoCodeInfo)
        Dim Promo As New PromoCodes

        ListPromo = Promo.GetPromoCode(sCompanyCode, sPromoCode, sPromoType)

        Return ListPromo
    End Function


    Public Function GetPromoCodes(ByVal iPromoID As Integer, ByVal sCompanyCode As String) As List(Of clsPromoCodeInfo) Implements IPromoCodesService.GetPromoCodes
        Dim ListPromo As New List(Of clsPromoCodeInfo)
        Dim Promo As New PromoCodes

        ListPromo = Promo.GetPromoCodes(iPromoID, sCompanyCode)

        Return ListPromo
    End Function


    Public Function GetPromoCodesxRate(ByVal sPromoCode As String) As List(Of clsPromoCodeRateInfo) Implements IPromoCodesService.GetPromoCodesxRate
        Dim ListPromo As New List(Of clsPromoCodeRateInfo)
        Dim Promo As New PromoCodes

        ListPromo = Promo.GetPromoCodesxRate(sPromoCode)

        Return ListPromo
    End Function


    Public Function GetPromoCodeAmount(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal sRateCode As String) As List(Of clsPromoCodeRateInfo) Implements IPromoCodesService.GetPromoCodeAmount
        Dim ListPromo As New List(Of clsPromoCodeRateInfo)
        Dim Promo As New PromoCodes


        ListPromo = Promo.GetPromoCodeAmount(iPromoID, sPromoCode, sCompanyCode, sRateCode)

        Return ListPromo
    End Function

    Public Function GetPromoCodeAmountService(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal iRateID As Integer) As List(Of clsPromoCodeRateInfo) Implements IPromoCodesService.GetPromoCodeAmountService
        Dim ListPromo As New List(Of clsPromoCodeRateInfo)
        Dim Promo As New PromoCodes


        ListPromo = Promo.GetPromoCodeAmountService(iPromoID, sPromoCode, sCompanyCode, iRateID)

        Return ListPromo
    End Function


    Public Function SavePromoCode(ByVal PromoID As Integer, ByVal PromoCode As String, ByVal Description As String, ByVal Type As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                  ByVal EndDate As Date, ByVal Active As Boolean, ByVal PromoType As String, ByVal sMode As String) As Response Implements IPromoCodesService.SavePromocode
        Dim response As New Response
        Dim Promo As New PromoCodes

        response = Promo.SavePromocode(PromoID, PromoCode, Description, Type, CompanyCode, StartDate, EndDate, Active, PromoType, "INS")

        Return response
    End Function

    Public Function DeletePromoCode(ByVal PromoID As Integer) As Response Implements IPromoCodesService.DeletePromoCode
        Dim response As New Response
        Dim Promo As New PromoCodes


        response = Promo.DeletePromoCode(PromoID)

        Return response
    End Function


    Public Function SavePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double) As Response Implements IPromoCodesService.SavePromoCodexRate
        Dim response As New Response
        Dim Promo As New PromoCodes


        response = Promo.SavePromoCodexRate(PromoID, RateID, Amount)

        Return response
    End Function

    Public Function DeletePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer) As Response Implements IPromoCodesService.DeletePromoCodexRate
        Dim response As New Response
        Dim Promo As New PromoCodes


        response = Promo.DeletePromoCodexRate(PromoID, RateID)

        Return response
    End Function

End Class
