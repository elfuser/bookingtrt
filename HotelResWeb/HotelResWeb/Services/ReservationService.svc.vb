﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "ReservationService" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ReservationService.svc or ReservationService.svc.vb at the Solution Explorer and start debugging.
Imports BRules

Public Class ReservationService
    Implements IReservationService


    Public Function GetFreeNights(ByVal sCompanyId As String, ByVal RateCode As String) As Integer Implements IReservationService.GetFreeNights
        Dim reserv As New Reservations
        Dim response As Integer

        response = reserv.GetFreeNights(sCompanyId, RateCode)

        Return response
    End Function


    Public Function GetReservationRoomsQuo(ByVal sQuotationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo) Implements IReservationService.GetReservationRoomsQuo
        Dim reserv As New Reservations
        Dim response As New List(Of clsReservationsRoomInfo)

        response = reserv.GetReservationRoomsQuo(sQuotationID, Culture)

        Return response
    End Function


    Public Function SaveReservation(ByVal sMode As String, ByVal sProcessor As String, reservation As clsReservationsInfo) As Long Implements IReservationService.SaveReservation
        Dim reserv As New Reservations
        Dim response As New Long

        response = reserv.SaveReservation(sMode, sProcessor, reservation)

        Return response
    End Function


    Public Function DeleteReservationRoom(ByVal sReservationNumber As String) As Response Implements IReservationService.DeleteReservationRoom
        Dim reserv As New Reservations
        Dim response As New Response


        response = reserv.DeleteReservationRoom(sReservationNumber)

        Return response
    End Function


    Public Function SaveReservationRoom(ByVal sMode As String, ReservationRoom As clsReservationsRoomInfo) As Long Implements IReservationService.SaveReservationRoom
        Dim reserv As New Reservations
        Dim response As Long

        response = reserv.SaveReservationRoom(sMode, ReservationRoom)


        Return response
    End Function


    Public Function SaveAvailability(ByVal ResNumber As String, ByVal KeyId As Long) As Long Implements IReservationService.SaveAvailability
        Dim reserv As New Reservations
        Dim response As Long = 0

        response = reserv.SaveAvailability(ResNumber, KeyId)

        Return response
    End Function


    Public Function SaveAvailabilityQuo(ByVal ResNumber As String) As Long Implements IReservationService.SaveAvailabilityQuo
        Dim reserv As New Reservations
        Dim response As Long = 0

        response = reserv.SaveAvailabilityQuo(ResNumber)

        Return response
    End Function


    Public Function CheckStatus(ByVal OrderId As String) As Integer Implements IReservationService.CheckStatus
        Dim reserv As New Reservations
        Dim response As Integer = 0

        response = reserv.CheckStatus(OrderId)

        Return response
    End Function



    Public Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16, ByVal KeyID As Long) As Long Implements IReservationService.UpdateStatus
        Dim reserv As New Reservations
        Dim response As Long = 0


        response = reserv.UpdateStatus(ResNumber, Status, KeyID)

        Return response
    End Function



    Public Function SaveAvailabilityQuery(ByVal CompanyCode As String, ByVal IP As String, ByVal Arrival As Date,
                                     ByVal Departure As Date, ByVal Adults As Int16, ByVal Children As Int16,
                                     ByVal Rooms As Int16, ByVal Nights As Int16,
                                     ByVal PromoCode As String, ByVal MessageCode As String, ByVal sGuid As System.Guid) As Response Implements IReservationService.SaveAvailabilityQuery
        Dim reserv As New Reservations
        Dim response As New Response


        response = reserv.SaveAvailabilityQuery(CompanyCode, IP, Arrival, Departure, Adults, Children, Rooms, Nights, PromoCode, MessageCode, sGuid)

        Return response
    End Function


    Public Function GetCardTypes(ByVal Bank As String) As List(Of clsCardTypeInfo) Implements IReservationService.GetCardTypes
        Dim reserv As New Reservations
        Dim response As New List(Of clsCardTypeInfo)

        response = reserv.GetCardTypes(Bank)


        Return response
    End Function


    Public Function SaveCardInformation(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response Implements IReservationService.SaveCardInformation
        Dim reserv As New Reservations
        Dim response As New Response

        response = reserv.SaveCardInformation(sMode, objCard)

        Return response
    End Function


    Public Function SaveCardInformationServ(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response Implements IReservationService.SaveCardInformationServ
        Dim reserv As New Reservations
        Dim response As New Response

        response = reserv.SaveCardInformation(sMode, objCard)


        Return response
    End Function


    Public Function SaveAddons(addonID As Long, reservation As String, ByVal quantity As Int16, ByVal Amount As Double) As Response Implements IReservationService.SaveAddons
        Dim reserv As New Reservations
        Dim response As New Response


        response = reserv.SaveAddons(addonID, reservation, quantity, Amount)

        Return response
    End Function



    Public Function GetAddonsQty(ByVal companyCode As String) As Integer Implements IReservationService.GetAddonsQty
        Dim reserv As New Reservations
        Dim response As Integer = 0

        response = reserv.GetAddonsQty(companyCode)


        Return response
    End Function



    Public Function GetCombinations(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Integer Implements IReservationService.GetCombinations
        Dim reserv As New Reservations
        Dim response As Integer = 0

        response = reserv.GetCombinations(companyCode, adults, children)


        Return response
    End Function


    Public Function SaveBankLog(ByVal ReservationID As String, ByVal Response As String,
                                ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                                ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response Implements IReservationService.SaveBankLog
        Dim reserv As New Reservations
        Dim resp As New Response

        resp = reserv.SaveBankLog(ReservationID, Response,
                                 AuthCode, ResponseCode, ErrorCode,
                                 ResponseText, TransactionID, Amount)


        Return resp
    End Function



    Public Function SaveBankLogQuo(ByVal QuotationID As String, ByVal Response As String,
                            ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                            ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response Implements IReservationService.SaveBankLogQuo
        Dim reserv As New Reservations
        Dim resp As New Response

        resp = reserv.SaveBankLogQuo(QuotationID, Response,
                             AuthCode, ResponseCode, ErrorCode,
                             ResponseText, TransactionID, Amount)

        Return resp
    End Function




    Public Function SaveBankPayPalLog(ByVal ReservationID As String, ByVal TransactionID As String,
                            ByVal PaymentStatus As String, ByVal ResponseText As String, ByVal PayerID As String,
                            ByVal CorrelationID As String) As Response Implements IReservationService.SaveBankPayPalLog

        Dim reserv As New Reservations
        Dim resp As New Response


        resp = reserv.SaveBankPayPalLog(ReservationID, TransactionID,
                             PaymentStatus, ResponseText, PayerID,
                             CorrelationID)

        Return resp
    End Function



    Public Function SaveBNLog(ByVal AuthorizationResult As Long, ByVal AuthorizationCode As String, ByVal ErrorCode As String,
                                ByVal ErrorMessage As String, ByVal OrderId As String) As Response Implements IReservationService.SaveBNLog
        Dim reserv As New Reservations
        Dim resp As New Response

        resp = reserv.SaveBNLog(AuthorizationResult, AuthorizationCode,
                             ErrorCode, ErrorMessage, OrderId)


        Return resp
    End Function



    Public Function GetReservation(ByVal sReservationID As String) As List(Of clsReservationInfo) Implements IReservationService.GetReservation
        Dim reserv As New Reservations
        Dim response As New List(Of clsReservationInfo)


        response = reserv.GetReservation(sReservationID)

        Return response
    End Function



    Public Function GetCredomaticOrderId(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Long Implements IReservationService.GetCredomaticOrderId
        Dim reserv As New Reservations
        Dim resp As Long

        resp = reserv.GetCredomaticOrderId(companyCode, adults, children)

        Return resp
    End Function



    Public Function GetQuotationToPay(ByVal sReservationID As String) As List(Of clsReservationPay) Implements IReservationService.GetQuotationToPay
        Dim reserv As New Reservations
        Dim response As New List(Of clsReservationPay)


        response = reserv.GetQuotationToPay(sReservationID)


        Return response
    End Function



    Public Function GetReservationRooms(ByVal sReservationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo) Implements IReservationService.GetReservationRooms
        Dim reserv As New Reservations
        Dim response As New List(Of clsReservationsRoomInfo)

        response = reserv.GetReservationRooms(sReservationID, Culture)


        Return response
    End Function



    Public Function GetReservationsNotSynced(ByVal integrationID As Int16, ByVal companyCode As String) As List(Of clsReservationInfo) Implements IReservationService.GetReservationsNotSynced
        Dim reserv As New Reservations
        Dim response As New List(Of clsReservationInfo)

        response = reserv.GetReservationsNotSynced(integrationID, companyCode)

        Return response
    End Function



    Public Function GetRateDiscount(ByVal sCompanyId As String,
                                    ByVal Rate_code As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date) As Double Implements IReservationService.GetRateDiscount
        Dim reserv As New Reservations
        Dim response As Double

        response = reserv.GetRateDiscount(sCompanyId, Rate_code, ArrivalDate, DepartureDate)


        Return response
    End Function



    Public Function GetRateConditions(ByVal company_code As String, ByVal rate_code As String, ByVal culture As String) As String Implements IReservationService.GetRateConditions
        Dim reserv As New Reservations
        Dim response As String

        response = reserv.GetRateConditions(company_code, rate_code, culture)


        Return response
    End Function



    Public Function GetDaysAvailability(ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date) As List(Of clsDayAvailability) Implements IReservationService.GetDaysAvailability
        Dim reserv As New Reservations
        Dim response As New List(Of clsDayAvailability)

        response = reserv.GetDaysAvailability(company_code, arrival, departure)


        Return response
    End Function



    Public Function SaveRateException(ByVal Reservations As String) As Response Implements IReservationService.SaveRateException
        Dim reserv As New Reservations
        Dim resp As New Response

        resp = reserv.SaveRateException(Reservations)

        Return resp
    End Function


    Public Function SaveQuotationToReservation(ByVal objRes As clsReservationsInfo) As Response Implements IReservationService.SaveQuotationToReservation
        Dim reserv As New Reservations
        Dim resp As New Response

        resp = reserv.SaveQuotationToReservation(objRes)


        Return resp
    End Function


    Public Function QueryReservations(ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of clsQryReservationConfirmation) Implements IReservationService.QueryReservations
        Dim reserv As New Reservations
        Dim response As New List(Of clsQryReservationConfirmation)

        response = reserv.QueryReservations(DateFrom, DateTo)

        Return response
    End Function



End Class
