﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "SalesService" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select SalesService.svc or SalesService.svc.vb at the Solution Explorer and start debugging.
Imports BRules

Public Class SalesService
    Implements ISalesService

    Public Function SaveSale(ByVal sMode As String, ByVal sProcessor As String,
                             ByVal objRes As clsSaleInfo) As String Implements ISalesService.SaveSale

        Dim sales As New Sales
        Dim response As String

        response = sales.SaveSale(sMode, sProcessor, objRes)

        Return response

    End Function

    Public Function SaveSaleItem(ByVal sMode As String, ByVal objRes As clsSaleItemInfo) As Response Implements ISalesService.SaveSaleItem
        Dim sales As New Sales
        Dim response As New Response

        response = sales.SaveSaleItem(sMode, objRes)

        Return response
    End Function

    Public Function GetSale(ByVal SaleID As String) As List(Of clsSaleInfo) Implements ISalesService.GetSale
        Dim sales As New Sales
        Dim response As New List(Of clsSaleInfo)

        response = sales.GetSale(SaleID)

        Return response
    End Function

    Public Function GetSaleItems(ByVal SaleID As String, ByVal Culture As String) As List(Of clsSaleItemInfo) Implements ISalesService.GetSaleItems
        Dim sales As New Sales
        Dim response As New List(Of clsSaleItemInfo)

        response = sales.GetSaleItems(SaleID, Culture)

        Return response
    End Function

    Public Function SaveAvailability(ByVal ResNumber As String) As Response Implements ISalesService.SaveAvailability
        Dim sales As New Sales
        Dim response As New Response

        response = sales.SaveAvailability(ResNumber)

        Return response
    End Function

    Public Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16) As Response Implements ISalesService.UpdateStatus
        Dim sales As New Sales
        Dim response As New Response

        response = sales.UpdateStatus(ResNumber, Status)

        Return response
    End Function

End Class
