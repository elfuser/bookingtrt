﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


' NOTE: You can use the "Rename" command on the context menu to change the interface name "IRoomServices" in both code and config file together.
<ServiceContract()>
Public Interface IRoomServices

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomsAvailable?sCompanyId={sCompanyId}&RateCode={RateCode}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&Adults={Adults}&Children={Children}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomsAvailable(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer) As List(Of RoomsAvailable)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomsAvailableDetail?sCompanyId={sCompanyId}&RateCode={RateCode}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&Adults={Adults}&Children={Children}&RoomTypeCode={RoomTypeCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomsAvailableDetail(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, ByVal RoomTypeCode As String) As List(Of RoomsAvailable)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCombinations?companyCode={companyCode}&adults1={adults1}&children1={children1}&adults2={adults2}&children2={children2}&adults3={adults3}&children3={children3}&adults4={adults4}&children4={children4}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCombinations(ByVal companyCode As String, ByVal adults1 As Integer, ByVal children1 As Integer,
                                    ByVal adults2 As Integer, ByVal children2 As Integer,
                                    ByVal adults3 As Integer, ByVal children3 As Integer,
                                    ByVal adults4 As Integer, ByVal children4 As Integer) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCombination?RowID={RowID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCombination(ByVal RowID As Long, ByVal sCompanyCode As String) As List(Of CombinationInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCombinations", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCombinations(ByVal sMode As String, ByVal RowID As Long, ByVal CompanyCode As String, ByVal Adults As Integer, ByVal Children As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRoomType", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRoomType(ByVal RoomTypeID As Integer, ByVal RoomTypeCode As String, ByVal CompanyCode As String, ByVal Description As String, ByVal Quantity As Integer,
                                 ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal RoomDetail As String, ByVal sMode As String, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRoomLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRoomLocale(ByVal RoomTypeID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomType?iRoomID={iRoomID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomType(ByVal iRoomID As Integer, ByVal sCompanyCode As String) As List(Of RoomsInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomLocale?iRoomTypeID={iRoomTypeID}&iCultureID={iCultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomLocale(ByVal iRoomTypeID As Integer, ByVal iCultureID As Integer) As List(Of RoomLocaleInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRoomType", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRoomType(ByVal RoomTypeID As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRoomBlocked", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRoomBlocked(sMode As String, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                ByVal Quantity As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomBlocked?sRoomTypeCode={sRoomTypeCode}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String) As List(Of RoomBlocked)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteRoomBlocked", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String, StartDate As Date, EndDate As Date, Qty As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityRoom", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityRoom(RoomTypeCode As String, CompanyCode As String, dDate As Date, Qty As Integer, QtyBlocked As Integer) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetReservationRooms?ReservationID={ReservationID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetReservationRooms(ByVal ReservationID As String) As List(Of ReservationsRoomInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveFeature", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveFeature(ByVal FeatureID As Integer, ByVal Description As String, ByVal CompanyCode As String, ByVal sMode As String) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFeature?iFeatureID={iFeatureID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFeature(ByVal iFeatureID As Integer, ByVal sCompanyCode As String) As List(Of FeaturesInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveFeatureToRoom", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveFeatureToRoom(ByVal iRoomTypeID As Integer, ByVal iFeatureID As Int16, ByVal sMode As String) As Response


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFeaturesFromRoom?iRoomTypeID={iRoomTypeID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFeaturesFromRoom(iRoomTypeID As Integer) As List(Of FeaturesInfo)


    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFeatureLocale?iFeatureID={iFeatureID}&iCultureID={iCultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFeatureLocale(ByVal iFeatureID As Int16, ByVal iCultureID As Integer) As List(Of FeatureLocaleInfo)

    '<OperationContract>
    '<WebInvoke(Method:="POST", UriTemplate:="/SaveFeatureLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    'Function SaveFeatureLocale(ByVal FeatureID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal sMode As String) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomsAvailableDetailSum?sCompanyId={sCompanyId}&RateCode={RateCode}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}&Adults={Adults}&Children={Children}&RoomTypeCode={RoomTypeCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomsAvailableDetailSum(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, RoomTypeCode As String) As List(Of RoomsAvailable)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetDayAvailabilityPerRoom?company_code={company_code}&arrival={arrival}&departure={departure}&roomCode={roomCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetDayAvailabilityPerRoom(ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date, ByVal roomCode As String) As List(Of clsDayAvailability)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFeatures?scompany_code={scompany_code}&roomtype_code={roomtype_code}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFeatures(ByVal scompany_code As String, ByVal roomtype_code As String,
                                ByVal Culture As String) As List(Of clsRoomFeaturesInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomTypes?roomtype_code={roomtype_code}&company_code={company_code}&culture={culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomTypes(ByVal roomtype_code As String, ByVal company_code As String, ByVal culture As String) As List(Of clsRoomsInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomPerson?company_code={company_code}&roomtype_code={roomtype_code}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomPerson(ByVal company_code As String, ByVal roomtype_code As String) As String

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/CheckRoomAvailability?company_code={company_code}&StartDate={StartDate}&EndDate={EndDate}&roomtype_code1={roomtype_code1}&roomtype_code2={roomtype_code2}&roomtype_code3={roomtype_code3}&roomtype_code4={roomtype_code4}",
               ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function CheckRoomAvailability(ByVal company_code As String, ByVal StartDate As Date,
                                          ByVal EndDate As Date, ByVal roomtype_code1 As String, ByVal roomtype_code2 As String,
                                          ByVal roomtype_code3 As String, ByVal roomtype_code4 As String) As List(Of clsCheckRoomInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRoomTypes_V1?roomtype_code={roomtype_code}&company_code={company_code}&culture={culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRoomTypes_V1(ByVal roomtype_code As String, ByVal company_code As String, ByVal culture As String) As List(Of clsRoomsInfo)
End Interface
