﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules
<ServiceContract()>
Public Interface IServiceServices

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceSchedAvailability?ServiceID={ServiceID}&ArrivalDate={ArrivalDate}&QtyPeople={QtyPeople}&PickupHour={PickupHour}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceSchedAvailability(ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal QtyPeople As Int16, ByVal PickupHour As String) As List(Of ServiceAvailability)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceSchedInventory?ScheduleID={ScheduleID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceSchedInventory(ByVal ScheduleID As Integer) As Integer

    '<WebInvoke(Method:="GET", UriTemplate:="/GetDayAvailability&serviceID={serviceID}&startDate={startDate}&endDate={endDate}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    'Function GetDayAvailability(ByVal serviceID As Integer, ByVal startDate As Date, ByVal endDate As Date) As List(Of DayAvailability)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceByIDArrival?ServiceID={ServiceID}&ArrivalDate={ArrivalDate}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceByIDArrival(ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal Culture As String) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceByID?ServiceID={ServiceID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceByID(ByVal ServiceID As Integer, ByVal Culture As String) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceConditions?ServiceID={ServiceID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceConditions(ByVal ServiceID As Integer, ByVal Culture As String) As String

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceWhatToBring?ServiceID={ServiceID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceWhatToBring(ByVal ServiceID As Integer, ByVal Culture As String) As String

    <WebInvoke(Method:="POST", UriTemplate:="/UpdateStatus", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16) As Response

    <WebInvoke(Method:="GET", UriTemplate:="/LoadWaiver?CompanyCode={CompanyCode}&CultureID={CultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function LoadWaiver(ByVal CompanyCode As String, ByVal CultureID As String) As String

    <WebInvoke(Method:="GET", UriTemplate:="/LoadTerms?CompanyCode={CompanyCode}&CultureID={CultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function LoadTerms(ByVal CompanyCode As String, ByVal CultureID As String) As String

    <WebInvoke(Method:="GET", UriTemplate:="/GetServicePickup?CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServicePickup(ByVal CompanyCode As String) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServicePickupHours?PlaceID={PlaceID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServicePickupHours(ByVal PlaceID As Integer) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceAvailability?sCompanyCode={sCompanyCode}&CategoryID={CategoryID}&Culture={Culture}&ArrivalDate={ArrivalDate}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceAvailability(ByVal sCompanyCode As String, ByVal CategoryID As Integer, ByVal Culture As String, ByVal ArrivalDate As Date) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServices?iServiceID={iServiceID}&sCompanyCode={sCompanyCode}&Culture={Culture}&Mode={Mode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServices(ByVal iServiceID As Integer, ByVal sCompanyCode As String, ByVal Culture As String, ByVal Mode As String) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServicesList?sCompanyCode={sCompanyCode}&sCulture={sCulture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServicesList(ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of ServiceInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceLocale?iServiceID={iServiceID}&iCultureID={iCultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceLocale(ByVal iServiceID As Integer, ByVal iCultureID As Integer) As List(Of ServiceLocaleInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceCondInfo?iServiceID={iServiceID}&iCultureID={iCultureID}&sType={sType}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceCondInfo(ByVal iServiceID As Integer, ByVal iCultureID As Integer, ByVal sType As String) As String

    <WebInvoke(Method:="POST", UriTemplate:="/SaveServiceLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServiceLocale(ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response

    <WebInvoke(Method:="POST", UriTemplate:="/SaveService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveService(ByVal CompanyCode As String, ByVal ServiceID As Integer, ByVal Description As String, ByVal Details As String,
                                ByVal CategoryID As Integer, ByVal sMode As String, ByVal Code As String,
                                ByVal ListOrder As Integer, ByVal VideoURL As String, ByVal Culture As String, ByVal Difficulty As Integer,
                                ByVal Inventory As Boolean, ByVal Active As Boolean) As String

    <WebInvoke(Method:="POST", UriTemplate:="/SavePickupService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SavePickupService(ByVal sMode As String,
                                  ByVal CompanyCode As String, ByVal PlaceID As Integer,
                                  ByVal Name As String, ByVal Active As Boolean) As String

    <WebInvoke(Method:="POST", UriTemplate:="/SaveServiceConditions", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServiceConditions(ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Conditions As String) As Response

    '<WebInvoke(Method:="POST", UriTemplate:="/SaveServicePicture", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    'Function SaveServicePicture(ByVal ServiceID As Integer, ByVal FileName As String, ByVal PictureType As Integer) As Response

    <WebInvoke(Method:="POST", UriTemplate:="/SaveServiceBringInfo", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServiceBringInfo(ByVal ServiceID As Integer, ByVal Culture As Integer, ByVal BringInfo As String) As Response

    <WebInvoke(Method:="POST", UriTemplate:="/DeleteService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteService(ByVal ServiceID As Integer) As String

    <WebInvoke(Method:="GET", UriTemplate:="/GetCategories?Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCategories(ByVal Culture As String) As List(Of CategoryInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceRates?iRateID={iRateID}&iServiceID={iServiceID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceRates(ByVal iRateID As Integer, ByVal iServiceID As Integer) As List(Of ServiceRateInfo)

    <WebInvoke(Method:="GET", UriTemplate:="/GetServiceInventory?iServiceID={iServiceID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetServiceInventory(ByVal iServiceID As Integer) As List(Of ServiceInventoryInfo)

    <WebInvoke(Method:="POST", UriTemplate:="/SaveServiceRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServiceRate(ByVal sMode As String, ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal OldStartDate As Date, ByVal OldEndDate As Date, ByVal Adults As Double, ByVal Children As Double,
                                    ByVal Infant As Double, ByVal Students As Double, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer, ByVal MaxInfant As Integer,
                                    ByVal MaxStudent As Integer, ByVal Active As Boolean) As Response

    <WebInvoke(Method:="POST", UriTemplate:="/SaveServiceInventory", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServiceInventory(ByVal sMode As String, ByVal ScheduleID As Integer, ByVal ServiceID As Integer, ByVal Hour As String,
                                    ByVal Qty As Integer) As Response

    <WebInvoke(Method:="POST", UriTemplate:="/DeleteServiceRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteServiceRate(ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As String

    <WebInvoke(Method:="POST", UriTemplate:="/DeleteServiceInventory", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteServiceInventory(ByVal ServiceID As Integer, ByVal ScheduleID As Integer) As String

End Interface
