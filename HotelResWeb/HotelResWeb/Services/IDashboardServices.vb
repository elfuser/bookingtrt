﻿Imports System.ServiceModel

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IDashboardServices" in both code and config file together.

Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules

<ServiceContract()>
Public Interface IDashboardServices

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesToday?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesToday(ByVal DateFrom As Date, ByVal DateTo As Date,
                                   ByVal CompanyCode As String) As List(Of clsDashSalesToday)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesByDay?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesByDay(ByVal DateFrom As Date,
                                    ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashSalesByDay)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesByMonth?iYear={iYear}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesByMonth(iYear As Integer, ByVal CompanyCode As String) As List(Of clsDashSalesByMonth)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesRoomsQty?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesRoomsQty(ByVal DateFrom As Date,
                                ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashRoomsQty)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesReservationsByDay?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesReservationsByDay(ByVal DateFrom As Date, ByVal DateTo As Date,
                                               ByVal CompanyCode As String) As List(Of clsDashSalesReservationsByDay)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/DashSalesReservationsByCountry?DateFrom={DateFrom}&DateTo={DateTo}&CompanyCode={CompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DashSalesReservationsByCountry(ByVal DateFrom As Date,
                                                   ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashCountryQty)



End Interface
