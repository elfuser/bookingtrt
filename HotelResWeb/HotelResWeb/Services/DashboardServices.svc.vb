﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "DashboardServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select DashboardServices.svc or DashboardServices.svc.vb at the Solution Explorer and start debugging.
Imports BRules

Public Class DashboardServices
    Implements IDashboardServices


    Public Function SalesToday(ByVal DateFrom As Date, ByVal DateTo As Date,
                                   ByVal CompanyCode As String) As List(Of clsDashSalesToday) Implements IDashboardServices.DashSalesToday

        Dim list As New List(Of clsDashSalesToday)
        Dim dashboard As New Dashboard


        list = dashboard.DashSalesToday(DateFrom, DateTo, CompanyCode)

        Return list
    End Function


    Public Function SalesByDay(ByVal DateFrom As Date,
                                    ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashSalesByDay) Implements IDashboardServices.DashSalesByDay

        Dim list As New List(Of clsDashSalesByDay)
        Dim dashboard As New Dashboard


        list = dashboard.DashSalesByDay(DateFrom, DateTo, CompanyCode)

        Return list

    End Function


    Public Function SalesByMonth(iYear As Integer, ByVal CompanyCode As String) As List(Of clsDashSalesByMonth) Implements IDashboardServices.DashSalesByMonth

        Dim list As New List(Of clsDashSalesByMonth)
        Dim dashboard As New Dashboard


        list = dashboard.DashSalesByMonth(iYear, CompanyCode)

        Return list

    End Function


    Public Function SalesRoomsQty(ByVal DateFrom As Date,
                                ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashRoomsQty) Implements IDashboardServices.DashSalesRoomsQty

        Dim list As New List(Of clsDashRoomsQty)
        Dim dashboard As New Dashboard

        list = dashboard.DashSalesRoomsQty(DateFrom, DateTo, CompanyCode)

        Return list
    End Function


    Public Function SalesReservationsByDay(ByVal DateFrom As Date, ByVal DateTo As Date,
                                               ByVal CompanyCode As String) As List(Of clsDashSalesReservationsByDay) Implements IDashboardServices.DashSalesReservationsByDay

        Dim list As New List(Of clsDashSalesReservationsByDay)
        Dim dashboard As New Dashboard


        list = dashboard.DashSalesReservationsByDay(DateFrom, DateTo, CompanyCode)

        Return list
    End Function


    Public Function SalesReservationsByCountry(ByVal DateFrom As Date,
                                                   ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashCountryQty) Implements IDashboardServices.DashSalesReservationsByCountry

        Dim list As New List(Of clsDashCountryQty)
        Dim dashboard As New Dashboard

        list = dashboard.DashSalesReservationsByCountry(DateFrom, DateTo, CompanyCode)

        Return list
    End Function

End Class
