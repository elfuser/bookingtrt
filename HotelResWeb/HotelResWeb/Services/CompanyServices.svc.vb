﻿Imports BRules
Imports HotelResWeb

Public Class CompanyServices
    Implements ICompanyServices

    Public Function SaveCompany(ByVal CompanyCode As String, ByVal CompanyName As String, ByVal LegalName As String, ByVal LegalID As String, ByVal Address As String, ByVal Tel1 As String, ByVal Tel2 As String,
                                ByVal Tel3 As String, ByVal Fax As String, ByVal ContactName As String, ByVal ContactEmail As String, ByVal Web As String, ByVal Email As String,
                                ByVal CallTollFree As String, ByVal sMode As String, ByVal BankName As String, ByVal BankAcct As String,
                                ByVal CfoName As String, ByVal CfoEmail As String, ByVal ResName As String, ByVal ResEmail As String, ByVal ResEmailAlt As String,
                                ByVal FacebookURL As String, ByVal TwitterText As String, ByVal Hotel As Boolean, ByVal Services As Boolean,
                                ByVal GoogleID As String, ByVal GoogleConvID As String, ByVal GoogleLabel As String, ByVal Active As Boolean, ByVal UserID As Integer, ByVal Migrar As Boolean) As Response Implements ICompanyServices.SaveCompany


        Dim response As New Response
        Dim Company As New Companies


        response = Company.SaveCompany(CompanyCode, CompanyName, LegalName, LegalID, Address, Tel1, Tel2,
                         Tel3, Fax, ContactName, ContactEmail, Web, Email,
                         CallTollFree, sMode, BankName, BankAcct,
                         CfoName, CfoEmail, ResName, ResEmail, ResEmailAlt,
                         FacebookURL, TwitterText, Hotel, Services,
                         GoogleID, GoogleConvID, GoogleLabel, Active, UserID, Migrar,
                         String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty,
                         String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
        Return response
    End Function

    Public Function SaveCompanyHotel(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer,
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response Implements ICompanyServices.SaveCompanyHotel

        Dim response As New Response
        Dim Company As New Companies
        response = Company.SaveCompanyHotel(CompanyCode, sMode, TaxPerc, MaxAdults, MaxChildren,
                                     MsgAdults, MsgChildren, ChildrenEnabled, MsgChildrenFree, ChildrenNote, ConfirmPrefix)
        Return response

    End Function

    Public Function SaveCompanyService(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal RentPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal MaxStudents As Integer,
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal MsgStudents As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response Implements ICompanyServices.SaveCompanyService

        Dim response As New Response
        Dim Company As New Companies
        response = Company.SaveCompanyService(CompanyCode, sMode, TaxPerc, RentPerc, MaxAdults, MaxChildren, MaxStudents,
                                     MsgAdults, MsgChildren, MsgStudents, ChildrenEnabled, MsgChildrenFree, ChildrenNote, ConfirmPrefix)
        Return response

    End Function

    Public Function SaveCompanyTerms(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sTerms As String, ByVal iCultureID As Integer) As Integer Implements ICompanyServices.SaveCompanyTerms
        Dim response As New Integer

        Dim Company As New Companies

        iTermID = Company.SaveCompanyTerms(CompanyCode, sMode, iTermID, sTerms, iCultureID)
        Return iTermID

    End Function

    Public Function SaveCompanyWaiver(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sWaiver As String, ByVal iCultureID As Integer) As Integer Implements ICompanyServices.SaveCompanyWaiver
        Dim Company As New Companies
        Dim iID As Integer = 0

        iID = Company.SaveCompanyWaiver(CompanyCode, sMode, iTermID, sWaiver, iCultureID)

        Return iID

    End Function

    Public Function SaveCompanyProcessor(ByVal CompanyCode As String, ByVal sMode As String, ByVal Commission As Double, ByVal ProcessorID As String,
                                         ByVal KeyID As String, ByVal KeyHash As String, ByVal Type As String, ByVal URL As String, ByVal Username As String) As Response Implements ICompanyServices.SaveCompanyProcessor
        Dim response As New Response
        Dim Company As New Companies

        response = Company.SaveCompanyProcessor(CompanyCode, sMode, Commission, ProcessorID,
                                          KeyID, KeyHash, Type, URL, Username)
        Return response

    End Function

    Public Function GetCompany(ByVal sCompany_Code As String, ByVal sType As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompany

        Dim Company As New Companies
        Dim CompanyList As New List(Of CompanyInfo)()

        Dim sMode As String = "SEL"
        CompanyList = Company.GetCompany(sCompany_Code, sMode, sType)
        Return CompanyList

    End Function

    Public Function GetCompanyHotel(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompanyHotel

        Dim Company As New Companies
        Dim CompanyList As New List(Of CompanyInfo)()

        CompanyList = Company.GetCompanyHotel(sCompany_Code, sMode)

        Return CompanyList
    End Function

    Public Function GetCompanyService(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompanyService
        Dim Company As New Companies
        Dim CompanyList As New List(Of CompanyInfo)()

        CompanyList = Company.GetCompanyService(sCompany_Code, sMode)
        Return CompanyList
    End Function

    Public Function GetCompanyProcessor(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompanyProcessor
        Dim Company As New Companies
        Dim CompanyList As New List(Of CompanyInfo)()
        CompanyList = Company.GetCompanyProcessor(sCompany_Code, sMode)
        Return CompanyList
    End Function

    Public Function DeleteCompany(ByVal CompanyCode As String) As Response Implements ICompanyServices.DeleteCompany
        Dim Company As New Companies
        Dim response As New Response
        response = Company.DeleteCompany(CompanyCode)
        Return response
    End Function

    Public Function GetCompanyTerm(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String Implements ICompanyServices.GetCompanyTerm
        Dim Company As New Companies

        Dim sresult As String

        sresult = Company.GetCompanyTerm(sCompany_Code, sMode, iCulture)
        Return sresult
    End Function

    Public Function GetCompanyWaiver(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String Implements ICompanyServices.GetCompanyWaiver
        Dim Company As New Companies
        Dim sResult As String = String.Empty

        sResult = Company.GetCompanyWaiver(sCompany_Code, sMode, iCulture)
        Return sResult

    End Function

    Public Function GetCompanyPrefix(ByVal sPrefix As String) As String Implements ICompanyServices.GetCompanyPrefix
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetCompanyPrefix(sPrefix)
        Return sResult
    End Function

    Public Function GetCompanyServicePrefix(ByVal sPrefix As String) As String Implements ICompanyServices.GetCompanyServicePrefix
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetCompanyServicePrefix(sPrefix)
        Return sResult
    End Function

    Public Function GetBankBNKeys(sCompanyCode As String) As String Implements ICompanyServices.GetBankBNKeys
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetBankBNKeys(sCompanyCode)
        Return sResult
    End Function

    Public Function GetCompanyEmails(sCompanyCode As String) As String Implements ICompanyServices.GetCompanyEmails
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetCompanyEmails(sCompanyCode)
        Return sResult
    End Function

    Public Function GetCompanyData(sCompanyCode As String) As String Implements ICompanyServices.GetCompanyData
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetCompanyData(sCompanyCode)
        Return sResult
    End Function

    Public Function GetCompanyGoogleInfo(sCompanyCode As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompanyGoogleInfo
        Dim Company As New Companies
        Dim sResult As List(Of CompanyInfo)
        sResult = Company.GetCompanyGoogleInfo(sCompanyCode)
        Return sResult
    End Function

    Public Function GetCompanyTermsEmails(sCompanyCode As String, sCulture As String) As List(Of CompanyInfo) Implements ICompanyServices.GetCompanyTermsEmails
        Dim Company As New Companies
        Dim sResult As List(Of CompanyInfo)
        sResult = Company.GetCompanyTermsEmails(sCompanyCode, sCulture)
        Return sResult
    End Function

    Public Function GetPayPalSubject(sCompanyCode As String) As String Implements ICompanyServices.GetPayPalSubject
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetPayPalSubject(sCompanyCode)
        Return sResult
    End Function

    Public Function GetBankCredomaticKeys(sCompanyCode As String) As String Implements ICompanyServices.GetBankCredomaticKeys
        Dim Company As New Companies
        Dim sResult As String = ""
        sResult = Company.GetBankCredomaticKeys(sCompanyCode)
        Return sResult
    End Function


    Public Function GetFacilities(ByVal sCompanyCode As String) As List(Of clsFeaturesInfo) Implements ICompanyServices.GetFacilities
        Dim Company As New Companies
        Dim sResult As List(Of clsFeaturesInfo)
        sResult = Company.GetFacilities(sCompanyCode)
        Return sResult
    End Function
End Class
