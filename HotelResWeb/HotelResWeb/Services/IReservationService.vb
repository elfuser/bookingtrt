﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IReservationService" in both code and config file together.
<ServiceContract()>
Public Interface IReservationService

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetFreeNights?sCompanyId={sCompanyId}&RateCode={RateCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFreeNights(ByVal sCompanyId As String, ByVal RateCode As String) As Integer

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetReservationRoomsQuo?sQuotationID={sQuotationID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetReservationRoomsQuo(ByVal sQuotationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveReservation", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveReservation(ByVal sMode As String, ByVal sProcessor As String, reservation As clsReservationsInfo) As Long

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteReservationRoom", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteReservationRoom(ByVal sReservationNumber As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveReservationRoom", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveReservationRoom(ByVal sMode As String, ReservationRoom As clsReservationsRoomInfo) As Long

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailability", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailability(ByVal ResNumber As String, ByVal KeyId As Long) As Long

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityQuo", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityQuo(ByVal ResNumber As String) As Long

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/CheckStatus", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function CheckStatus(ByVal OrderId As String) As Integer

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/UpdateStatus", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16, ByVal KeyID As Long) As Long


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityQuery", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityQuery(ByVal CompanyCode As String, ByVal IP As String, ByVal Arrival As Date,
                                     ByVal Departure As Date, ByVal Adults As Int16, ByVal Children As Int16,
                                     ByVal Rooms As Int16, ByVal Nights As Int16,
                                     ByVal PromoCode As String, ByVal MessageCode As String, ByVal sGuid As System.Guid) As Response

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCardTypes?Bank={Bank}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCardTypes(ByVal Bank As String) As List(Of clsCardTypeInfo)

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCardInformation", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCardInformation(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCardInformationServ", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCardInformationServ(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAddons", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAddons(addonID As Long, reservation As String, ByVal quantity As Int16, ByVal Amount As Double) As Response


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddonsQty?companyCode={companyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonsQty(ByVal companyCode As String) As Integer


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCombinations?companyCode={companyCode}&adults={adults}&children={children}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCombinations(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Integer


    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveBankLog", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveBankLog(ByVal ReservationID As String, ByVal Response As String,
                                ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                                ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response



    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveBankLogQuo", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveBankLogQuo(ByVal QuotationID As String, ByVal Response As String,
                            ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                            ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response




    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveBankPayPalLog", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveBankPayPalLog(ByVal ReservationID As String, ByVal TransactionID As String,
                            ByVal PaymentStatus As String, ByVal ResponseText As String, ByVal PayerID As String,
                            ByVal CorrelationID As String) As Response



    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveBNLog", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveBNLog(ByVal AuthorizationResult As Long, ByVal AuthorizationCode As String, ByVal ErrorCode As String,
                                ByVal ErrorMessage As String, ByVal OrderId As String) As Response



    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetReservation?sReservationID={sReservationID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetReservation(ByVal sReservationID As String) As List(Of clsReservationInfo)


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCredomaticOrderId?companyCode={companyCode}&adults={adults}&children={children}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCredomaticOrderId(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Long


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetQuotationToPay?sReservationID={sReservationID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetQuotationToPay(ByVal sReservationID As String) As List(Of clsReservationPay)



    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetReservationRooms?sReservationID={sReservationID}&Culture={Culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetReservationRooms(ByVal sReservationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)



    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetReservationsNotSynced?integrationID={integrationID}&companyCode={companyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetReservationsNotSynced(ByVal integrationID As Int16, ByVal companyCode As String) As List(Of clsReservationInfo)



    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateDiscount?sCompanyId={sCompanyId}&Rate_code={Rate_code}&ArrivalDate={ArrivalDate}&DepartureDate={DepartureDate}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateDiscount(ByVal sCompanyId As String,
                                    ByVal Rate_code As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date) As Double



    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetRateConditions?company_code={company_code}&rate_code={rate_code}&culture={culture}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetRateConditions(ByVal company_code As String, ByVal rate_code As String, ByVal culture As String) As String


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetDaysAvailability?company_code={company_code}&arrival={arrival}&departure={departure}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetDaysAvailability(ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date) As List(Of clsDayAvailability)



    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRateException", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRateException(ByVal Reservations As String) As Response

    <OperationContract>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveQuotationToReservation", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveQuotationToReservation(ByVal objRes As clsReservationsInfo) As Response

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/QueryReservations?DateFrom={DateFrom}&DateTo={DateTo}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function QueryReservations(ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of clsQryReservationConfirmation)



End Interface
