﻿Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports BRules

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IPictureService" in both code and config file together.
<ServiceContract()>
Public Interface IPictureService

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveRoomPhoto", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveRoomPhoto(roomTypeID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveCompanyPhoto", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveCompanyPhoto(companyCode As String, photoNumber As Integer, pictureStream As Byte()) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveServicePhoto", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveServicePhoto(serviceID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/DownloadRoomPhoto?RoomTypeID={RoomTypeID}&photoNumber={photoNumber}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DownloadRoomPhoto(RoomTypeID As Integer, photoNumber As Integer) As PictureFile

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/DownloadCompanyPhoto?companyCode={companyCode}&photoNumber={photoNumber}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DownloadCompanyPhoto(companyCode As String, photoNumber As Integer) As PictureFile

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/DownloadServicePhoto?ServiceID={ServiceID}&photoNumber={photoNumber}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DownloadServicePhoto(ServiceID As Integer, photoNumber As Integer) As PictureFile


End Interface
