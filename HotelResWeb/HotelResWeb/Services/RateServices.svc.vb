﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "RateServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select RateServices.svc or RateServices.svc.vb at the Solution Explorer and start debugging.

Imports BRules


Public Class RateServices
    Implements IRateServices


    Public Function SaveRateDetails(ByVal RateDetID As Long, ByVal RateID As Integer, RoomtypeID As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Double,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal Wadult1 As Double, ByVal Wadult2 As Double, ByVal Wadult3 As Double, ByVal Wadult4 As Double, ByVal Wadult5 As Double,
                                    ByVal Wadult6 As Double, ByVal Wadult7 As Double, ByVal Wadult8 As Double, ByVal Wadult9 As Double, ByVal Wadult10 As Double, ByVal Wchild As Double,
                                    CompanyID As String, ByVal sMode As String, ByVal roomcode As Integer) As Response Implements IRateServices.SaveRateDetails

        Dim Rates As New Rates
        Dim Response As New Response

        Response = Rates.SaveRateDetails(RateDetID, RateID, RoomtypeID, StartDate, EndDate, Adult1, Adult2, Adult3, Adult4, Adult5,
                                        Adult6, Adult7, Adult8, Adult9, Adult10, Child,
                                        Wadult1, Wadult2, Wadult3, Wadult4, Wadult5,
                                        Wadult6, Wadult7, Wadult8, Wadult9, Wadult10, Wchild,
                                        CompanyID, sMode, roomcode)

        Return Response
    End Function



    Public Function SaveRateHeader(ByVal RateID As Integer, ByVal RateCode As String, ByVal Description As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal MinLos As Integer, ByVal MaxLos As Integer, ByVal Enabled As Boolean, ByVal WeekEnd As Boolean,
                                   ByVal Type As String, ByVal RateDetail As String, ByVal OrderList As Integer, ByVal FreeNights As Integer,
                                   ByVal Conditions As String, ByVal sMode As String) As Response Implements IRateServices.SaveRateHeader
        Dim Rates As New Rates
        Dim Response As New Response

        Response = Rates.SaveRateHeader(RateID, RateCode, Description, CompanyCode, StartDate,
                                    EndDate, MinLos, MaxLos, Enabled, WeekEnd,
                                    Type, RateDetail, OrderList, FreeNights, Conditions, sMode)

        Return Response
    End Function



    Public Function SaveRateLocale(ByVal RateID As Long, ByVal CultureID As Integer, ByVal Description As String,
                                   ByVal LongDescription As String, ByVal Conditions As String, ByVal sMode As String) As Response Implements IRateServices.SaveRateLocale

        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.SaveRateLocale(RateID, CultureID, Description, LongDescription, Conditions, sMode)

        Return Response

    End Function



    Public Function DeleteRateHeader(ByVal RateID As Integer) As Response Implements IRateServices.DeleteRateHeader
        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.DeleteRateHeader(RateID)

        Return Response
    End Function


    Public Function GetRateHeader(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RatesInfo) Implements IRateServices.GetRateHeader
        Dim Rates As New Rates
        Dim list As New List(Of RatesInfo)

        list = Rates.GetRateHeader(iRateID, sCompanyCode, bSelectAll)

        Return list
    End Function



    Public Function GetRoomsAvailable(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RateLocaleInfo) Implements IRateServices.GetRoomsAvailable
        Dim Rates As New Rates
        Dim list As New List(Of RateLocaleInfo)

        list = Rates.GetRoomsAvailable(iRateID, sCompanyCode, bSelectAll)

        Return list
    End Function



    Public Function SaveRateRoomType(ByVal RateDetID As Long, ByVal RoomTypeID As Integer, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal sMode As String, ByVal RateID As Integer) As String Implements IRateServices.SaveRateRoomType

        Dim Rates As New Rates
        Dim Response As String = ""


        Response = Rates.SaveRateRoomType(RateDetID, RoomTypeID, CompanyCode, StartDate,
                                    EndDate, sMode, RateID)


        Return Response
    End Function


    Public Function SaveAvailabilityByVal(RateCode As String, ByVal RoomTypeID As Integer, ByVal Type As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal oldStartDate As Date, oldEndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, Adult1w As Double, Adult2w As Double,
                                    ByVal Adult3w As Double, ByVal Adult4w As Double, ByVal Adult5w As Double, ByVal Adult6w As Double, ByVal Adult7w As Double, ByVal Adult8w As Double,
                                    ByVal Adult9w As Double, ByVal Adult10w As Double, ByVal Child As Double, ByVal Childw As Double,
                                    ByVal CompanyID As String, ByVal sMode As String) As Response Implements IRateServices.SaveAvailabilityByVal

        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.SaveAvailability(RateCode, RoomTypeID, Type, StartDate, EndDate, oldStartDate, oldEndDate,
                                    Adult1, Adult2, Adult3, Adult4, Adult5,
                                    Adult6, Adult7, Adult8, Adult9, Adult10, Adult1w, Adult2w,
                                    Adult3w, Adult4w, Adult5w, Adult6w, Adult7w, Adult8w,
                                    Adult9w, Adult10w, Child, Childw,
                                    CompanyID, sMode)

        Return Response
    End Function


    Public Function DeleteRateDetails(ByVal RateDetID As Integer, CompanyCode As String, RoomTypeCode As String,
                                      StartDate As Date, EndDate As Date) As Response Implements IRateServices.DeleteRateDetails
        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.DeleteRateDetails(RateDetID, CompanyCode, RoomTypeCode, StartDate, EndDate)

        Return Response
    End Function


    Public Function GetRateDetails(ByVal iRateDetID As Integer) As List(Of RateDetailInfo) Implements IRateServices.GetRateDetails
        Dim Rates As New Rates
        Dim list As New List(Of RateDetailInfo)

        list = Rates.GetRateDetails(iRateDetID)


        Return list
    End Function


    Public Function GetRateDates(ByVal iRateDetID As Integer) As List(Of RateDetailInfo) Implements IRateServices.GetRateDates
        Dim Rates As New Rates
        Dim list As New List(Of RateDetailInfo)

        list = Rates.GetRateDetails(iRateDetID)

        Return list
    End Function


    Public Function GetRateExcDates(sCompanyCode As String, ByVal sRateCode As String, sRoomTypeCode As String,
                                    ByVal dStart As Date, ByVal dEnd As Date) As List(Of RateDetailInfo) Implements IRateServices.GetRateExcDates
        Dim Rates As New Rates
        Dim list As New List(Of RateDetailInfo)

        list = Rates.GetRateExcDates(sCompanyCode, sRateCode, sRoomTypeCode, dStart, dEnd)

        Return list
    End Function





    Public Function GetRateRoomType(ByVal RateDetID As Integer, sCompanyCode As String) As List(Of RateRoomTypeInfo) Implements IRateServices.GetRateRoomType
        Dim Rates As New Rates
        Dim list As New List(Of RateRoomTypeInfo)


        list = Rates.GetRateRoomType(RateDetID, sCompanyCode)

        Return list
    End Function


    Public Function DeleteRateRoomType(ByVal RateDetID As Integer, ByVal RoomTypeID As Integer) As Response Implements IRateServices.DeleteRateRoomType
        Dim Rates As New Rates
        Dim Response As New Response

        Response = Rates.DeleteRateRoomType(RateDetID, RoomTypeID)

        Return Response
    End Function


    Public Function SaveRateException(ByVal CompanyCode As String, RateCode As String, RoomtypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                      ByVal StartDateAnt As Date, ByVal EndDateAnt As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double,
                                      ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal sMode As String) As Response Implements IRateServices.SaveRateException
        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.SaveRateException(CompanyCode, RateCode, RoomtypeCode, StartDate, EndDate,
                                        StartDateAnt, EndDateAnt,
                                        Adult1, Adult2, Adult3, Adult4, Adult5,
                                        Adult6, Adult7, Adult8, Adult9, Adult10, Child, sMode)


        Return Response
    End Function


    Public Function GetRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer,
                             ByVal iChildren As Integer) As List(Of RatesInfo) Implements IRateServices.GetRates
        Dim Rates As New Rates
        Dim list As New List(Of RatesInfo)


        list = Rates.GetRates(sCompanyId, ArrivalDate, DepartureDate, iAdults, iChildren)

        Return list
    End Function


    Public Function GetRates_v1(ByVal sCompanyId As String, ByVal RoomType As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer,
                             ByVal iChildren As Integer) As List(Of RatesInfo) Implements IRateServices.GetRates_v1
        Dim Rates As New Rates
        Dim list As New List(Of RatesInfo)


        list = Rates.GetRates_v1(sCompanyId, RoomType, ArrivalDate, DepartureDate, iAdults, iChildren)

        Return list
    End Function

    Public Function GetRatesWithPromoCode(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date,
                                          ByVal iAdults As Integer, ByVal iChildren As Integer, ByVal sPromoCode As String) As List(Of RatesInfo) Implements IRateServices.GetRatesWithPromoCode
        Dim Rates As New Rates
        Dim list As New List(Of RatesInfo)

        list = Rates.GetRatesWithPromoCode(sCompanyId, ArrivalDate, DepartureDate, iAdults, iChildren, sPromoCode)

        Return list
    End Function


    Public Function SaveRateDiscount(ByVal RowID As Integer, ByVal CompanyCode As String, RateCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Disc1 As Double, ByVal Disc2 As Double, ByVal Disc3 As Double, ByVal Disc4 As Double, ByVal Disc5 As Integer,
                                    ByVal Disc6 As Double, ByVal Disc7 As Double, ByVal Disc8 As Double, ByVal Discount As Double, ByVal Type As Integer,
                                    ByVal sMode As String) As Response Implements IRateServices.SaveRateDiscount
        Dim Rates As New Rates
        Dim Response As New Response


        Response = Rates.SaveRateDiscount(RowID, CompanyCode, RateCode, StartDate, EndDate,
                                    Disc1, Disc2, Disc3, Disc4, Disc5,
                                    Disc6, Disc7, Disc8, Discount, Type, sMode)

        Return Response
    End Function



    Public Function GetRateDiscounts(ByVal companyCode As String, ByVal rateCode As String) As List(Of RateDiscountInfo) Implements IRateServices.GetRateDiscounts
        Dim Rates As New Rates
        Dim list As New List(Of RateDiscountInfo)

        list = Rates.GetRateDiscounts(companyCode, rateCode)

        Return list
    End Function


    Public Function GetRateDiscount(ByVal iRowID As Integer) As List(Of RateDiscountInfo) Implements IRateServices.GetRateDiscount
        Dim Rates As New Rates
        Dim list As New List(Of RateDiscountInfo)


        list = Rates.GetRateDiscount(iRowID)

        Return list
    End Function


    Public Function DeleteRateDiscount(rowID As Integer) As Response Implements IRateServices.DeleteRateDiscount
        Dim Rates As New Rates
        Dim Response As New Response

        Response = Rates.DeleteRateDiscount(rowID)

        Return Response
    End Function




    Public Function GetRatesFreeNights(ByVal CompanyCode As String, ByVal RateCode As String) As Integer Implements IRateServices.GetRatesFreeNights
        Dim Rates As New Rates
        Dim Response As Integer

        Response = Rates.GetRatesFreeNights(CompanyCode, RateCode)

        Return Response
    End Function


    Public Function GetAvailabilityRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date,
                                     ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer,
                                     ByVal RoomCode As String, ByVal Culture As String) As List(Of RatesInfo) Implements IRateServices.GetAvailabilityRates
        Dim Rates As New Rates
        Dim list As New List(Of RatesInfo)


        list = Rates.GetAvailabilityRates(sCompanyId, ArrivalDate,
                                          DepartureDate, Adults, Children,
                                          RoomCode, Culture)

        Return list
    End Function

End Class
