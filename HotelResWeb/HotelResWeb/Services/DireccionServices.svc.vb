﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "DashboardServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select DashboardServices.svc or DashboardServices.svc.vb at the Solution Explorer and start debugging.
Imports BRules
Imports HotelResWeb

Public Class DireccionServices
    Implements IDireccionServices

    Public Function GetCountries() As List(Of CountriesInfo) Implements IDireccionServices.GetCountries
        Dim list As New List(Of CountriesInfo)
        Dim direccion As New Direcciones
        list = direccion.GetCountries()
        Return list
    End Function

    Public Function GetStates(sCountryIso3 As String) As List(Of StatesInfo) Implements IDireccionServices.GetStates
        Dim list As New List(Of StatesInfo)
        Dim direccion As New Direcciones
        list = direccion.GetStates(sCountryIso3)
        Return list
    End Function
End Class
