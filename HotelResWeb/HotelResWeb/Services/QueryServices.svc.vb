﻿Imports BRules
Imports HotelResWeb

Public Class QueryServices
    Implements IQueryServices

    Public Function QueryAvailability(CompanyCode As String, RateCode As String, DateFrom As Date, DateTo As Date) As List(Of QryAvailability) Implements IQueryServices.QueryAvailability
        Dim query As New Queries
        Dim QryAvailabilityList As New List(Of QryAvailability)()
        QryAvailabilityList = query.QueryAvailability(CompanyCode, RateCode, DateFrom, DateTo)
        Return QryAvailabilityList
    End Function


    Public Function QueryPrices(ByVal CompanyCode As String, ByVal CurrentDate As Date, RoomType As String, ByVal PromoCode As String) As List(Of PricesDTO) Implements IQueryServices.QueryPrices
        Dim query As New Queries
        Dim list As New List(Of PricesDTO)()
        list = query.QueryPrices(CompanyCode, CurrentDate, RoomType, PromoCode)
        Return list
    End Function


    Public Function QueryAvailabilityDay(CompanyCode As String, CurrentDate As String) As List(Of DayAvailability) Implements IQueryServices.QueryAvailabilityDay
        Dim query As New Queries
        Dim DayAvailabilityList As New List(Of DayAvailability)()
        DayAvailabilityList = query.QueryAvailabilityDay(CompanyCode, CurrentDate)
        Return DayAvailabilityList
    End Function

    Public Function QueryCalendar(CompanyCode As String, Month As Integer, Year As Integer) As List(Of QryCalendar) Implements IQueryServices.QueryCalendar
        Dim query As New Queries
        Dim QryCalendarList As New List(Of QryCalendar)()
        QryCalendarList = query.QueryCalendar(CompanyCode, Month, Year)

        Return QryCalendarList
    End Function

    Public Function QueryCalendarRoom(CompanyCode As String, RoomTypeCode As String, Month As Integer, Year As Integer) As List(Of QryCalendar) Implements IQueryServices.QueryCalendarRoom
        Dim query As New Queries
        Dim QryCalendarList As New List(Of QryCalendar)()
        QryCalendarList = query.QueryCalendarRoom(CompanyCode, RoomTypeCode, Month, Year)
        Return QryCalendarList
    End Function

    Public Function QueryCalendarService(ServiceID As Integer, Month As Integer, Year As Integer) As List(Of QryCalendarService) Implements IQueryServices.QueryCalendarService
        Dim query As New Queries
        Dim QryCalendarServiceList As New List(Of QryCalendarService)()
        QryCalendarServiceList = query.QueryCalendarService(ServiceID, Month, Year)
        Return QryCalendarServiceList
    End Function

    Public Function QueryCalendarServiceSchedule(ServiceID As Integer, ScheduleID As Integer, Month As Integer, Year As Integer) As List(Of QryCalendarService) Implements IQueryServices.QueryCalendarServiceSchedule
        Dim query As New Queries
        Dim QryCalendarServiceList As New List(Of QryCalendarService)()
        QryCalendarServiceList = query.QueryCalendarServiceSchedule(ServiceID, ScheduleID, Month, Year)
        Return QryCalendarServiceList
    End Function

    Public Function QueryCountries() As List(Of CountriesInfo) Implements IQueryServices.QueryCountries
        Dim query As New Queries
        Dim CountriesInfoList As New List(Of CountriesInfo)()
        CountriesInfoList = query.QueryCountries()
        Return CountriesInfoList
    End Function

    Public Function QueryQuotations(DateFrom As Date, DateTo As Date, CompanyCode As String) As List(Of QryReservationConfirmation) Implements IQueryServices.QueryQuotations
        Dim query As New Queries
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        QryReservationConfirmationList = query.QueryQuotations(DateFrom, DateTo, CompanyCode)
        Return QryReservationConfirmationList
    End Function

    Public Function QueryReservationAccount(DateFrom As Date, DateTo As Date, CompanyCode As String, Range As Short) As List(Of QryReservationConfirmation) Implements IQueryServices.QueryReservationAccount
        Dim query As New Queries
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        QryReservationConfirmationList = query.QueryReservationAccount(DateFrom, DateTo, CompanyCode, Range)
        Return QryReservationConfirmationList
    End Function

    Public Function QueryReservationAccountOS(CompanyCode As String, DateFrom As Date, DateTo As Date) As List(Of QryReservationConfirmation) Implements IQueryServices.QueryReservationAccountOS
        Dim query As New Queries
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        QryReservationConfirmationList = query.QueryReservationAccountOS(DateFrom, DateTo, CompanyCode)
        Return QryReservationConfirmationList
    End Function

    Public Function QueryReservationAddonConfirmations(Reservation As String) As List(Of QryAddon) Implements IQueryServices.QueryReservationAddonConfirmations
        Dim query As New Queries
        Dim QryAddonList As New List(Of QryAddon)()
        QryAddonList = query.QueryReservationAddonConfirmations(Reservation)
        Return QryAddonList
    End Function

    Public Function QueryReservationBankLog(Reservation As String) As List(Of BankLogInfo) Implements IQueryServices.QueryReservationBankLog
        Dim query As New Queries
        Dim BankLogInfoList As New List(Of BankLogInfo)()
        BankLogInfoList = query.QueryReservationBankLog(Reservation)
        Return BankLogInfoList
    End Function

    Public Function QueryReservationConfirmations(DateFrom As Date, DateTo As Date, CompanyCode As String, Range As Short) As List(Of QryReservationConfirmation) Implements IQueryServices.QueryReservationConfirmations
        Dim query As New Queries
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        QryReservationConfirmationList = query.QueryReservationConfirmations(DateFrom, DateTo, CompanyCode, Range)
        Return QryReservationConfirmationList
    End Function

    Public Function QueryReservationRoomConfirmation(Reservation As String) As List(Of QryRoom) Implements IQueryServices.QueryReservationRoomConfirmation
        Dim query As New Queries
        Dim QryRoomList As New List(Of QryRoom)()
        QryRoomList = query.QueryReservationRoomConfirmation(Reservation)
        Return QryRoomList
    End Function

    Public Function QueryReservations(DateFrom As Date, DateTo As Date, CompanyCode As String) As List(Of QryReservation) Implements IQueryServices.QueryReservations
        Dim query As New Queries
        Dim QryReservationList As New List(Of QryReservation)()
        QryReservationList = query.QueryReservations(DateFrom, DateTo, CompanyCode)
        Return QryReservationList
    End Function

    Public Function QueryReservationServiceItems(SaleID As String) As List(Of QrySaleItems) Implements IQueryServices.QueryReservationServiceItems
        Dim query As New Queries
        Dim QrySaleItemsList As New List(Of QrySaleItems)()
        QrySaleItemsList = query.QueryReservationServiceItems(SaleID)
        Return QrySaleItemsList
    End Function

    Public Function QuerySales(DateFrom As Date, DateTo As Date, CompanyCode As String, Range As Short) As List(Of QrySale) Implements IQueryServices.QuerySales
        Dim query As New Queries
        Dim QrySaleList As New List(Of QrySale)()
        QrySaleList = query.QuerySales(DateFrom, DateTo, CompanyCode, Range)
        Return QrySaleList
    End Function
End Class




