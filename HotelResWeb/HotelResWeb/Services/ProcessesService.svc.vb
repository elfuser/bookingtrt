﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "ProcessesService" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ProcessesService.svc or ProcessesService.svc.vb at the Solution Explorer and start debugging.


Imports BRules

Public Class ProcessesService
    Implements IProcessesService


    Public Function BlockRoomsQuo(ByVal QuotationID As String) As Response Implements IProcessesService.BlockRoomsQuo
        Dim process As New Processes
        Dim response As New Response

        response = process.BlockRoomsQuo(QuotationID)

        Return response
    End Function


    Public Function ChangeVoidDateQuo(ByVal QuotationID As String, ByVal NewDate As Date) As Response Implements IProcessesService.ChangeVoidDateQuo
        Dim process As New Processes
        Dim response As New Response

        response = process.ChangeVoidDateQuo(QuotationID, NewDate)

        Return response
    End Function


    Public Function SaveDepositBankID(ByVal sXML As String, ByVal sDeposit As String, sTransID As String) As Response Implements IProcessesService.SaveDepositBankID
        Dim process As New Processes
        Dim response As New Response

        response = process.SaveDepositBankID(sXML, sDeposit, sTransID)

        Return response
    End Function

    Public Function SaveGuest(ByVal sMode As String, ByVal objGuest As clsGuestsInfo) As Long Implements IProcessesService.SaveGuest
        Dim process As New Processes
        Dim id As Long

        id = process.SaveGuest(sMode, objGuest)

        Return id
    End Function

    Public Function SaveReservation(ByVal sMode As String, ByVal objRes As clsReservationsInfo) As String Implements IProcessesService.SaveReservation
        Dim process As New Processes
        Dim response As String


        response = process.SaveReservation(sMode, objRes)

        Return response

    End Function


    Public Function SaveAvailabilityCalendar(ByVal CompanyCode As String, ByVal RoomTypeCode As String,
                                             ByVal objRes As List(Of clsQryCalendar), ByVal UserID As Integer) As Response Implements IProcessesService.SaveAvailabilityCalendar
        Dim process As New Processes
        Dim response As New Response

        response = process.SaveAvailabilityCalendar(CompanyCode, RoomTypeCode, objRes, UserID)

        Return response

    End Function


    Public Function SaveAvailabilityCalendarService(ByVal ServiceID As Integer, ByVal ScheduleID As Integer,
                                                    ByVal objRes As List(Of clsQryCalendarService), ByVal UserID As Integer) As Response Implements IProcessesService.SaveAvailabilityCalendarService
        Dim process As New Processes
        Dim response As New Response

        response = process.SaveAvailabilityCalendarService(ServiceID, ScheduleID, objRes, UserID)

        Return response
    End Function


    Public Function SaveReservation(ByVal objRes As List(Of clsReservationsRoomInfo)) As Response Implements IProcessesService.SaveReservation
        Dim process As New Processes
        Dim response As New Response


        response = process.SaveReservation(objRes)

        Return response
    End Function

    Public Function SaveReservationAddons(ByVal objAddons As List(Of AddonInfo),
                                          ByVal sReservation As String) As Response Implements IProcessesService.SaveReservationAddons
        Dim process As New Processes
        Dim response As New Response

        response = process.SaveReservationAddons(objAddons, sReservation)

        Return response
    End Function


    Public Function SaveApprovalCode(ByVal ReservationID As String, Code As String) As Response Implements IProcessesService.SaveApprovalCode
        Dim process As New Processes
        Dim response As New Response

        response = process.SaveApprovalCode(ReservationID, Code)

        Return response
    End Function


    Public Function BlackOutDates(ByVal sCompanyCode As String, ByVal sRoomtypeCode As String,
                                  ByVal dFrom As Date, ByVal dTo As Date) As Response Implements IProcessesService.BlackOutDates
        Dim process As New Processes
        Dim response As New Response

        response = process.BlackOutDates(sCompanyCode, sRoomtypeCode, dFrom, dTo)

        Return response
    End Function

    Public Function SendEmailPwdReminder(ByVal Email As String) As String Implements IProcessesService.SendEmailPwdReminder
        Dim process As New Processes
        Dim response As String

        response = process.SendEmailPwdReminder(Email)

        Return response
    End Function


    Public Function SendEmailCounter(ByVal HotelName As String, ByVal HotelEmail As String, ByVal HotelEmailAlt As String, ByVal HotelCode As String,
                            ByVal CustEmail As String, ByVal CustName As String, ByVal ReservationID As String,
                            ByVal CheckIn As Date, ByVal CheckOut As Date, ByVal RateCode As String,
                            ByVal Subtotal As Double, ByVal Addon As Double, ByVal Promo As Double, ByVal Tax As Double, ByVal Total As Double,
                            ByVal Nights As Integer, ByVal CantAddons As Integer) As Boolean Implements IProcessesService.SendEmailCounter

        Dim process As New Processes
        Dim response As Boolean

        response = process.SendEmailCounter(HotelName, HotelEmail, HotelEmailAlt, HotelCode,
                             CustEmail, CustName, ReservationID, CheckIn, CheckOut, RateCode,
                             Subtotal, Addon, Promo, Tax, Total, Nights, CantAddons)

        Return response

    End Function

End Class
