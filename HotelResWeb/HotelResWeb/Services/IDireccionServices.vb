﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports BRules


<ServiceContract()>
Public Interface IDireccionServices

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetStates?sCountryIso3={sCountryIso3}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetStates(ByVal sCountryIso3 As String) As List(Of StatesInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCountries", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCountries() As List(Of CountriesInfo)

End Interface
