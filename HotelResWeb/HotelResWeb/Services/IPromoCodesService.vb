﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


' NOTE: You can use the "Rename" command on the context menu to change the interface name "IPromoCodesService" in both code and config file together.
<ServiceContract()>
Public Interface IPromoCodesService

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPromoCode?sCompanyCode={sCompanyCode}&sPromoCode={sPromoCode}&sPromoType={sPromoType}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPromoCode(ByVal sCompanyCode As String, ByVal sPromoCode As String, ByVal sPromoType As String) As List(Of clsPromoCodeInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPromoCodes?iPromoID={iPromoID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPromoCodes(ByVal iPromoID As Integer, ByVal sCompanyCode As String) As List(Of clsPromoCodeInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPromoCodesxRate?sPromoCode={sPromoCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPromoCodesxRate(ByVal sPromoCode As String) As List(Of clsPromoCodeRateInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPromoCodeAmount?iPromoID={iPromoID}&sPromoCode={sPromoCode}&sCompanyCode={sCompanyCode}&sRateCode={sRateCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPromoCodeAmount(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal sRateCode As String) As List(Of clsPromoCodeRateInfo)

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetPromoCodeAmountService?iPromoID={iPromoID}&sPromoCode={sPromoCode}&sCompanyCode={sCompanyCode}&iRateID={iRateID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetPromoCodeAmountService(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal iRateID As Integer) As List(Of clsPromoCodeRateInfo)


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SavePromocode", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SavePromocode(ByVal PromoID As Integer, ByVal PromoCode As String, ByVal Description As String, ByVal Type As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                  ByVal EndDate As Date, ByVal Active As Boolean, ByVal PromoType As String, ByVal sMode As String) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/DeletePromoCode", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeletePromoCode(ByVal PromoID As Integer) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SavePromoCodexRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SavePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/DeletePromoCodexRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeletePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer) As Response

End Interface
