﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules


' NOTE: You can use the "Rename" command on the context menu to change the interface name "IAddonServices" in both code and config file together.
<ServiceContract()>
Public Interface IAddonServices

#Region " Addons "

    <OperationContract>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCombination?sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonEnabled(ByVal sCompanyCode As String) As List(Of AddonInfo)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetCulture?iCultureID={iCultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddons?iAddonID={iAddonID}&sCompanyCode={sCompanyCode}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddons(ByVal iAddonID As Long, ByVal sCompanyCode As String) As List(Of AddonInfo)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddonRates?iRateID={iRateID}&iAddonID={iAddonID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonRates(ByVal iRateID As Integer, ByVal iAddonID As Integer) As List(Of AddonRateInfo)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddonLocale?iAddonID={iAddonID}&iCultureID={iCultureID}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonLocale(ByVal iAddonID As Long, ByVal iCultureID As Integer) As List(Of AddonLocaleInfo)


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAddon", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAddon(ByVal AddonID As Long, ByVal Description As String, ByVal CompanyCode As String, ByVal Amount As Double, Enabled As Boolean, ByVal RowOrder As Integer, ByVal sMode As String,
                              ByVal LongDes As String, ByVal Departure As String, ByVal Duration As String, ByVal AddonType As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Amount1 As Double, ByVal Amount2 As Double, ByVal Amount3 As Double, ByVal Amount4 As Double,
                              ByVal Amount5 As Double, ByVal Amount6 As Double, ByVal Amount7 As Double, ByVal Amount8 As Double, ByVal Amount9 As Double, ByVal Amount10 As Double, ByVal Tax As Boolean) As String



    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAddonLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteAddon", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteAddon(ByVal AddonID As Long) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteAddonLocale", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/DeleteAddonRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function DeleteAddonRate(ByVal RateID As Integer) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAddonRate", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAddonRate(ByVal sMode As String, ByVal RateID As Integer, ByVal AddonID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal Type As String, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer,
                                    ByVal MaxStudent As Integer, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer, ByVal Price As Double,
                                    ByVal Adults As Double, ByVal Children As Double, ByVal Infant As Double, ByVal Students As Double, ByVal Active As Boolean) As Response


    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddonQtys?companyCode={companyCode}&DateFrom={DateFrom}&DateTo={DateTo}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonQtys(ByVal companyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of AddonInfo)

    <OperationContract()>
    <WebInvoke(Method:="GET", UriTemplate:="/GetAddonsByCategory?companyCode={companyCode}&DateFrom={DateFrom}&DateTo={DateTo}&culture={culture}&Category={Category}", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetAddonsByCategory(ByVal companyCode As String,
                          ByVal DateFrom As Date, ByVal DateTo As Date, culture As String, Category As String) As List(Of AddonInfo)
#End Region

End Interface
