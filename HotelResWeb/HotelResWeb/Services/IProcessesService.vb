﻿Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports System.ServiceModel.Web
Imports BRules

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IProcessesService" in both code and config file together.
<ServiceContract()>
Public Interface IProcessesService

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/BlockRoomsQuo", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function BlockRoomsQuo(ByVal QuotationID As String) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/ChangeVoidDateQuo", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function ChangeVoidDateQuo(ByVal QuotationID As String, ByVal NewDate As Date) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveDepositBankID", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveDepositBankID(ByVal sXML As String, ByVal sDeposit As String, sTransID As String) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveGuest", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveGuest(ByVal sMode As String, ByVal objGuest As clsGuestsInfo) As Long


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveReservation", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveReservation(ByVal sMode As String, ByVal objRes As clsReservationsInfo) As String


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityCalendar", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityCalendar(ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal objRes As List(Of clsQryCalendar), ByVal UserID As Integer) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveAvailabilityCalendarService", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveAvailabilityCalendarService(ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal objRes As List(Of clsQryCalendarService), ByVal UserID As Integer) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveReservation", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveReservation(ByVal objRes As List(Of clsReservationsRoomInfo)) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveReservationAddons", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveReservationAddons(ByVal objAddons As List(Of AddonInfo), ByVal sReservation As String) As Response


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SaveApprovalCode", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SaveApprovalCode(ByVal ReservationID As String, Code As String) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/BlackOutDates", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function BlackOutDates(ByVal sCompanyCode As String, ByVal sRoomtypeCode As String, ByVal dFrom As Date, ByVal dTo As Date) As Response

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SendEmailPwdReminder", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SendEmailPwdReminder(ByVal Email As String) As String


    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="/SendEmailCounter", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function SendEmailCounter(ByVal HotelName As String, ByVal HotelEmail As String, ByVal HotelEmailAlt As String, ByVal HotelCode As String,
                            ByVal CustEmail As String, ByVal CustName As String, ByVal ReservationID As String,
                            ByVal CheckIn As Date, ByVal CheckOut As Date, ByVal RateCode As String,
                            ByVal Subtotal As Double, ByVal Addon As Double, ByVal Promo As Double, ByVal Tax As Double, ByVal Total As Double,
                            ByVal Nights As Integer, ByVal CantAddons As Integer) As Boolean



End Interface
