﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "RoomServices" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select RoomServices.svc or RoomServices.svc.vb at the Solution Explorer and start debugging.

Imports BRules

Public Class RoomServices
    Implements IRoomServices


    Public Function GetRoomsAvailable(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer) As List(Of RoomsAvailable) Implements IRoomServices.GetRoomsAvailable
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomsAvailable)

        list = RoomsBL.GetRoomsAvailable(sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children)

        Return list
    End Function


    Public Function GetRoomsAvailableDetail(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, ByVal RoomTypeCode As String) As List(Of RoomsAvailable) Implements IRoomServices.GetRoomsAvailableDetail
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomsAvailable)

        list = RoomsBL.GetRoomsAvailableDetail(sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children, RoomTypeCode)

        Return list
    End Function


    Public Function GetCombinations(ByVal companyCode As String, ByVal adults1 As Integer, ByVal children1 As Integer,
                                    ByVal adults2 As Integer, ByVal children2 As Integer,
                                    ByVal adults3 As Integer, ByVal children3 As Integer,
                                    ByVal adults4 As Integer, ByVal children4 As Integer) As String Implements IRoomServices.GetCombinations

        Dim RoomsBL As New Rooms()
        Dim Result As String = ""

        Result = RoomsBL.GetCombinations(companyCode, adults1, children1, adults2, children2, adults3, children3, adults4, children4)

        Return Result

    End Function



    Public Function GetCombination(ByVal RowID As Long, ByVal sCompanyCode As String) As List(Of CombinationInfo) Implements IRoomServices.GetCombination
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of CombinationInfo)

        list = RoomsBL.GetCombination(RowID, sCompanyCode)

        Return list
    End Function


    Public Function SaveCombinations(ByVal sMode As String, ByVal RowID As Long, ByVal CompanyCode As String, ByVal Adults As Integer, ByVal Children As Integer) As Response Implements IRoomServices.SaveCombinations
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveCombinations(sMode, RowID, CompanyCode, Adults, Children)

        Return response
    End Function


    Public Function SaveRoomType(ByVal RoomTypeID As Integer, ByVal RoomTypeCode As String, ByVal CompanyCode As String, ByVal Description As String, ByVal Quantity As Integer,
                                 ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal RoomDetail As String, ByVal sMode As String, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer) As Response Implements IRoomServices.SaveRoomType
        Dim RoomsBL As New Rooms()
        Dim response As New Response()
        Dim id As Long

        id = RoomsBL.SaveRoomType(RoomTypeID, RoomTypeCode, CompanyCode, Description, Quantity, MaxAdults, MaxChildren, RoomDetail, sMode, MaxOccupance, MinOccupance)

        response.Status = id.ToString
        Return response
    End Function

    Public Function SaveRoomLocale(ByVal RoomTypeID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response Implements IRoomServices.SaveRoomLocale
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveRoomLocale(RoomTypeID, CultureID, Description, LongDescription, sMode)

        Return response
    End Function

    Public Function GetRoomType(ByVal iRoomID As Integer, ByVal sCompanyCode As String) As List(Of RoomsInfo) Implements IRoomServices.GetRoomType
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomsInfo)

        list = RoomsBL.GetRoomType(iRoomID, sCompanyCode)

        Return list
    End Function

    Public Function GetRoomLocale(ByVal iRoomTypeID As Integer, ByVal iCultureID As Integer) As List(Of RoomLocaleInfo) Implements IRoomServices.GetRoomLocale
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomLocaleInfo)

        list = RoomsBL.GetRoomLocale(iRoomTypeID, iCultureID)

        Return list
    End Function

    Public Function DeleteRoomType(ByVal RoomTypeID As Integer) As Response Implements IRoomServices.DeleteRoomType

        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.DeleteRoomType(RoomTypeID)

        Return response
    End Function

    Public Function SaveRoomBlocked(sMode As String, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                ByVal Quantity As Integer) As Response Implements IRoomServices.SaveRoomBlocked

        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveRoomBlocked(sMode, CompanyCode, RoomTypeCode, StartDate, EndDate, Quantity)

        Return response
    End Function

    Public Function GetRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String) As List(Of RoomBlocked) Implements IRoomServices.GetRoomBlocked

        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomBlocked)

        list = RoomsBL.GetRoomBlocked(sRoomTypeCode, sCompanyCode)

        Return list

    End Function

    Public Function DeleteRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String, StartDate As Date, EndDate As Date, Qty As Integer) As Response Implements IRoomServices.DeleteRoomBlocked
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.DeleteRoomBlocked(sRoomTypeCode, sCompanyCode, StartDate, EndDate, Qty)

        Return response
    End Function

    Public Function SaveAvailabilityRoom(RoomTypeCode As String, CompanyCode As String, dDate As Date, Qty As Integer, QtyBlocked As Integer) As Response Implements IRoomServices.SaveAvailabilityRoom
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveAvailabilityRoom(RoomTypeCode, CompanyCode, dDate, Qty, QtyBlocked)

        Return response
    End Function

    Public Function GetReservationRooms(ByVal ReservationID As String) As List(Of ReservationsRoomInfo) Implements IRoomServices.GetReservationRooms
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of ReservationsRoomInfo)

        list = RoomsBL.GetReservationRooms(ReservationID)

        Return list
    End Function

    Public Function SaveFeature(ByVal FeatureID As Integer, ByVal Description As String, ByVal CompanyCode As String, ByVal sMode As String) As Response Implements IRoomServices.SaveFeature
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveFeature(FeatureID, Description, CompanyCode, sMode)

        Return response
    End Function


    Public Function GetFeature(ByVal iFeatureID As Integer, ByVal sCompanyCode As String) As List(Of FeaturesInfo) Implements IRoomServices.GetFeature
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of FeaturesInfo)

        list = RoomsBL.GetFeature(iFeatureID, sCompanyCode)

        Return list
    End Function

    Public Function SaveFeatureToRoom(ByVal iRoomTypeID As Integer, ByVal iFeatureID As Int16, ByVal sMode As String) As Response Implements IRoomServices.SaveFeatureToRoom
        Dim RoomsBL As New Rooms()
        Dim response As New Response()

        response = RoomsBL.SaveFeatureToRoom(iRoomTypeID, iFeatureID, sMode)

        Return response
    End Function

    Public Function GetFeaturesFromRoom(iRoomTypeID As Integer) As List(Of FeaturesInfo) Implements IRoomServices.GetFeaturesFromRoom
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of FeaturesInfo)

        list = RoomsBL.GetFeaturesFromRoom(iRoomTypeID)

        Return list
    End Function

    Public Function GetFeatureLocale(ByVal iFeatureID As Int16, ByVal iCultureID As Integer) As List(Of FeatureLocaleInfo) Implements IRoomServices.GetFeatureLocale
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of FeatureLocaleInfo)

        list = RoomsBL.GetFeatureLocale(iFeatureID, iCultureID)

        Return list

    End Function

    'Public Function SaveFeatureLocale(ByVal FeatureID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal sMode As String) As Response Implements IRoomServices.SaveFeatureLocale
    '    Dim RoomsBL As New Rooms()
    '    Dim response As New Response()

    '    response = RoomsBL.SaveFeatureLocale(FeatureID, CultureID, Description, sMode)

    '    Return response

    'End Function


    Public Function GetRoomsAvailableDetailSum(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer,
                                               ByVal Children As Integer, RoomTypeCode As String) As List(Of RoomsAvailable) Implements IRoomServices.GetRoomsAvailableDetailSum
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of RoomsAvailable)

        list = RoomsBL.GetRoomsAvailableDetailSum(sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children, RoomTypeCode)

        Return list
    End Function

    Public Function GetDayAvailabilityPerRoom(ByVal company_code As String, ByVal arrival As Date,
                                              ByVal departure As Date, ByVal roomCode As String) As List(Of clsDayAvailability) Implements IRoomServices.GetDayAvailabilityPerRoom
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of clsDayAvailability)

        list = RoomsBL.GetDayAvailabilityPerRoom(company_code, arrival, departure, roomCode)

        Return list
    End Function

    Public Function GetFeatures(ByVal scompany_code As String, ByVal roomtype_code As String,
                                ByVal Culture As String) As List(Of clsRoomFeaturesInfo) Implements IRoomServices.GetFeatures
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of clsRoomFeaturesInfo)

        list = RoomsBL.GetFeatures(scompany_code, roomtype_code, Culture)

        Return list
    End Function


    Public Function GetRoomTypes(ByVal roomtype_code As String, ByVal company_code As String,
                                 ByVal culture As String) As List(Of clsRoomsInfo) Implements IRoomServices.GetRoomTypes

        Dim RoomsBL As New Rooms()
        Dim list As New List(Of clsRoomsInfo)

        list = RoomsBL.GetRoomTypes(roomtype_code, company_code, culture)

        Return list
    End Function


    Public Function GetRoomPerson(ByVal company_code As String, ByVal roomtype_code As String) As String Implements IRoomServices.GetRoomPerson
        Dim RoomsBL As New Rooms()
        Dim resp As String

        resp = RoomsBL.GetRoomPerson(company_code, roomtype_code)

        Return resp
    End Function


    Public Function CheckRoomAvailability(ByVal company_code As String, ByVal StartDate As Date,
                                          ByVal EndDate As Date, ByVal roomtype_code1 As String, ByVal roomtype_code2 As String,
                                          ByVal roomtype_code3 As String, ByVal roomtype_code4 As String) As List(Of clsCheckRoomInfo) Implements IRoomServices.CheckRoomAvailability
        Dim RoomsBL As New Rooms()
        Dim list As New List(Of clsCheckRoomInfo)

        list = RoomsBL.CheckRoomAvailability(company_code, StartDate, EndDate, roomtype_code1, roomtype_code2, roomtype_code3, roomtype_code4)

        Return list
    End Function


    Public Function GetRoomTypes_V1(ByVal roomtype_code As String, ByVal company_code As String,
                             ByVal culture As String) As List(Of clsRoomsInfo) Implements IRoomServices.GetRoomTypes_V1

        Dim RoomsBL As New Rooms()
        Dim list As New List(Of clsRoomsInfo)

        list = RoomsBL.GetRoomTypes_V1(roomtype_code, company_code, culture)

        Return list
    End Function
End Class
