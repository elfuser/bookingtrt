﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports System.Web.Routing

Public Class RouteConfig
    Public Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")



        routes.MapRoute(
            name:="ConfirmationController",
            url:="Confirmation/Confirmation",
            defaults:=New With {.controller = "Confirmation", .action = "Confirmation", .hotelcode = UrlParameter.Optional}
        )

        routes.MapRoute(
            name:="AddOnsController",
            url:="AddOns/hotelcode",
            defaults:=New With {.controller = "AddOns", .action = "AddOns", .hotelcode = UrlParameter.Optional}
        )

        routes.MapRoute( _
            name:="RoomsAvailabilityController", _
            url:="RoomsAvailability/hotelcode", _
            defaults:=New With {.controller = "RoomsAvailability", .action = "RoomsAvailability", .hotelcode = UrlParameter.Optional} _
        )

        routes.MapRoute(
            name:="Default",
            url:="{controller}/{action}/{id}",
            defaults:=New With {.controller = "RoomsAvailability", .action = "RoomsAvailability", .id = UrlParameter.Optional}
        )

    End Sub
End Class