﻿
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Security.Cryptography
Imports BRules

Public Class Utils

    'Public Shared Property UserID As String
    '    Get
    '        If HttpContext.Current.Session("Session_UserID_Booking") = "" Then
    '            Return ""
    '        End If
    '        Return HttpContext.Current.Session("Session_UserID_Booking")
    '    End Get
    '    Set(ByVal value As String)
    '        HttpContext.Current.Session("Session_UserID_Booking") = value
    '    End Set
    'End Property

    Public Shared Property UserObject As ResponseUser
        Get
            Return HttpContext.Current.Session("Session_User_Booking")
        End Get
        Set(ByVal value As ResponseUser)
            HttpContext.Current.Session("Session_User_Booking") = value
        End Set
    End Property

    Public Shared Function GetMD5(Str As String) As String
        Dim md5 As MD5 = MD5CryptoServiceProvider.Create
        Dim encoding As New ASCIIEncoding()
        Dim stream As Byte() = Nothing
        Dim sb As New StringBuilder()
        stream = md5.ComputeHash(encoding.GetBytes(Str))
        For i As Integer = 0 To stream.Length - 1
            sb.AppendFormat("{0:x2}", stream(i))
        Next
        Return sb.ToString()
    End Function

    Public Shared Function SaveReservationInfo(reservation As ReservationModel) As Boolean


        Dim EncryptJson As String
        Dim ReservationJson As String

        ReservationJson = Utils.GetJsonFromObject(reservation)

        EncryptJson = Utils.encrypt(ReservationJson)

        Dim userCookie = New HttpCookie("ReservationInfo", EncryptJson)
        userCookie.Expires.AddDays(365)
        HttpContext.Current.Response.Cookies.Add(userCookie)

        Return True

    End Function

    Public Shared Function SavePromoCodeInfo(obj As clsPromoCodeRateInfo) As Boolean


        Dim EncryptJson As String
        Dim objJson As String

        objJson = Utils.GetJsonFromObject(obj)

        EncryptJson = Utils.encrypt(objJson)

        Dim userCookie = New HttpCookie("PromoCodeInfo", EncryptJson)
        userCookie.Expires.AddDays(365)
        HttpContext.Current.Response.Cookies.Add(userCookie)

        Return True

    End Function

    Public Shared Function SavePromoCodeInfoService(obj As List(Of clsPromoCodeRateInfo)) As Boolean


        Dim EncryptJson As String
        Dim objJson As String

        objJson = Utils.GetJsonFromObject(obj)

        EncryptJson = Utils.encrypt(objJson)

        Dim userCookie = New HttpCookie("PromoCodeInfoServices", EncryptJson)
        userCookie.Expires.AddDays(365)
        HttpContext.Current.Response.Cookies.Add(userCookie)

        Return True

    End Function


    Public Shared Function DeserializeAddons() As List(Of AddOnLiteModel)
        Dim addons As New List(Of AddOnLiteModel)
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("AddonInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("AddonInfo").Value)
                If HttpContext.Current.Request.Cookies("AddonInfo").Value <> "" Then
                    addons = Utils.GetAddonsFromJSON(js)
                End If
            End If

        Catch ex As Exception

        End Try

        Return addons
    End Function

    Public Shared Function DeserializeReservationInfo() As ReservationModel
        Dim reserv As New ReservationModel
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("ReservationInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("ReservationInfo").Value)
                If HttpContext.Current.Request.Cookies("ReservationInfo").Value <> "" Then
                    reserv = Utils.GetReservationFromJSON(js)
                End If
            End If

        Catch ex As Exception

        End Try

        Return reserv
    End Function

    Public Shared Function DeserializePromoCodeInfo() As clsPromoCodeRateInfo
        Dim obj As New clsPromoCodeRateInfo
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("PromoCodeInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("PromoCodeInfo").Value)
                If HttpContext.Current.Request.Cookies("PromoCodeInfo").Value <> "" Then
                    obj = Utils.GetPromoCodeFromJSON(js)
                End If
            End If

        Catch ex As Exception

        End Try

        Return obj
    End Function


    Public Shared Function DeserializeRooms() As List(Of RoomLiteModel)
        Dim rooms As New List(Of RoomLiteModel)
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("RoomInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("RoomInfo").Value)
                If HttpContext.Current.Request.Cookies("RoomInfo").Value <> "" Then
                    rooms = Utils.GetRoomsFromJSON(js)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return rooms
    End Function

    ''' <summary>
    '''Encrypts a string
    ''' </summary>
    ''' <param name="strValues">String that wants to code</param>
    ''' <returns>String encripted</returns>
    ''' <remarks></remarks>
    Public Shared Function encrypt(ByVal strValues As String) As String
        Dim objEncrypt As New CryptoUtil()
        Return objEncrypt.Encrypt(strValues)
    End Function

    ''' <summary>
    '''Decrypts a string
    ''' </summary>
    ''' <param name="strValues">string that wants to be decoded</param>
    ''' <returns>String decrypted</returns>
    ''' <remarks></remarks>
    Public Shared Function decrypt(ByVal strValues As String) As String
        If strValues Is Nothing Then
            Return Nothing
        Else
            Dim objEncrypt As New CryptoUtil()
            Return objEncrypt.Decrypt(strValues)
        End If
    End Function

    ''' <summary>
    '''Decrypts and returns the value of an encrypted querystring field
    ''' </summary>
    ''' <param name="encrypted">String with the information encrypted</param>
    ''' <param name="field">Name of the field that wants to return</param>
    ''' <returns>Value of the field dencrypted</returns>
    ''' <remarks></remarks>
    Public Shared Function getHTTPField(ByVal encrypted As String, ByVal field As String) As String
        If encrypted = "" Then
            Return ""
        End If

        encrypted = encrypted.Replace(" ", "+")
        Dim decrypted As String = decrypt(encrypted)
        Dim s() As String = decrypted.Split("&"c), k() As String, ret As String
        Dim i, j As Integer
        field = field.ToLower

        For i = 0 To s.Length - 1
            k = s(i).Split("="c)
            If k(0).ToLower = field Then
                ret = k(1)
                If k.Length > 2 Then
                    For j = 2 To k.Length - 1
                        ret += "=" & k(j)
                    Next
                End If
                Return ret
            End If
        Next
        Return ""
    End Function

    Public Shared Function GetJsonFromObject(ByVal obj As Object) As String
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Serialize(obj)
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Shared Function GetRoomsFromJSON(ByVal jsonText As String) As List(Of RoomLiteModel)
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of List(Of RoomLiteModel))(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function GetAddonsFromJSON(ByVal jsonText As String) As List(Of AddOnLiteModel)
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of List(Of AddOnLiteModel))(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Public Shared Function GetReservationFromJSON(ByVal jsonText As String) As ReservationModel
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of ReservationModel)(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetPromoCodeFromJSON(ByVal jsonText As String) As clsPromoCodeRateInfo
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of clsPromoCodeRateInfo)(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#Region "Tour/Service"
    Public Shared Function SaveTourServiceInfo(ByVal data As TourServiceModel) As Boolean

        Dim EncryptJson As String
        Dim objJson As String

        Try
            objJson = Utils.GetJsonFromObject(data)

            EncryptJson = Utils.encrypt(objJson)

            Dim userCookie = New HttpCookie("TourServiceInfo", EncryptJson)
            userCookie.Expires.AddDays(365)
            HttpContext.Current.Response.Cookies.Add(userCookie)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function DeserializeTourService() As TourServiceModel
        Dim obj As New TourServiceModel
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("TourServiceInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("TourServiceInfo").Value)
                If HttpContext.Current.Request.Cookies("TourServiceInfo").Value <> "" Then
                    obj = Utils.GetTourServiceInfoFromJSON(js)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Shared Function GetTourServiceInfoFromJSON(ByVal jsonText As String) As TourServiceModel
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of TourServiceModel)(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function DeserializeServices() As List(Of ServiceModelLite)
        Dim list As New List(Of ServiceModelLite)
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("ServicesInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("ServicesInfo").Value)
                If HttpContext.Current.Request.Cookies("ServicesInfo").Value <> "" Then
                    list = Utils.GetServicesFromJSON(js)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return list
    End Function

    Public Shared Function GetServicesFromJSON(ByVal jsonText As String) As List(Of ServiceModelLite)
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of List(Of ServiceModelLite))(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function DeserializePromoCodeServiceInfo() As List(Of clsPromoCodeRateInfo)
        Dim obj As New List(Of clsPromoCodeRateInfo)
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("PromoCodeInfoServices") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("PromoCodeInfoServices").Value)
                If HttpContext.Current.Request.Cookies("PromoCodeInfoServices").Value <> "" Then
                    obj = Utils.GetPromoCodeServiceFromJSON(js)
                End If
            End If

        Catch ex As Exception

        End Try

        Return obj
    End Function

    Public Shared Function GetPromoCodeServiceFromJSON(ByVal jsonText As String) As List(Of clsPromoCodeRateInfo)
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of List(Of clsPromoCodeRateInfo))(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

#Region "Virtual Receipt"
    Public Shared Function SaveVirtualReceiptInfo(ByVal data As VirtualReceiptReservationModel) As Boolean

        Dim EncryptJson As String
        Dim objJson As String

        Try
            objJson = Utils.GetJsonFromObject(data)

            EncryptJson = Utils.encrypt(objJson)

            Dim userCookie = New HttpCookie("VirtualReceiptInfo", EncryptJson)
            userCookie.Expires.AddDays(365)
            HttpContext.Current.Response.Cookies.Add(userCookie)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function DeserializeVirtualReceipt() As VirtualReceiptReservationModel
        Dim obj As New VirtualReceiptReservationModel
        Dim js As String = ""

        Try

            If Not HttpContext.Current.Request.Cookies("VirtualReceiptInfo") Is Nothing Then
                js = Utils.decrypt(HttpContext.Current.Request.Cookies("VirtualReceiptInfo").Value)
                If HttpContext.Current.Request.Cookies("VirtualReceiptInfo").Value <> "" Then
                    obj = Utils.GetVirtualReceiptInfoFromJSON(js)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Shared Function GetVirtualReceiptInfoFromJSON(ByVal jsonText As String) As VirtualReceiptReservationModel
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Deserialize(Of VirtualReceiptReservationModel)(jsonText)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    'Public Shared Function DeserializeServices() As List(Of ServiceModelLite)
    '    Dim list As New List(Of ServiceModelLite)
    '    Dim js As String = ""

    '    Try

    '        If Not HttpContext.Current.Request.Cookies("ServicesInfo") Is Nothing Then
    '            js = Utils.decrypt(HttpContext.Current.Request.Cookies("ServicesInfo").Value)
    '            If HttpContext.Current.Request.Cookies("ServicesInfo").Value <> "" Then
    '                list = Utils.GetServicesFromJSON(js)
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    '    Return list
    'End Function

    'Public Shared Function GetServicesFromJSON(ByVal jsonText As String) As List(Of ServiceModelLite)
    '    Try
    '        Dim serializer As New JavaScriptSerializer()
    '        Return serializer.Deserialize(Of List(Of ServiceModelLite))(jsonText)
    '    Catch ex As Exception
    '        Return Nothing
    '    End Try
    'End Function
#End Region
End Class
