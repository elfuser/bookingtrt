﻿Imports System.Web
Imports System.Web.Optimization

Public Class BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    Public Shared Sub RegisterBundles(ByVal bundles As BundleCollection)
        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                   "~/Scripts/jquery-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryui").Include(
                    "~/Scripts/jquery-ui-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.unobtrusive*",
                    "~/Scripts/jquery.validate*"))

        '-- knockout js
        bundles.Add(New ScriptBundle("~/bundles/knockout").Include(
                   "~/Scripts/knockout-{version}.js",
                   "~/Scripts/knockout_mapping_latest.js"))

        '-- bootstrap js
        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
                   "~/Scripts/bootstrap.min.js"))

        '-- datepicker js
        bundles.Add(New ScriptBundle("~/bundles/datepicker").Include(
                   "~/Scripts/jquery.plugin.min.js",
                   "~/Scripts/jquery.datepick.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/App/master").Include(
                    "~/Scripts/jquery.cookie.js",
                    "~/Scripts/App/master.js"))

        '-- rooms availability js
        bundles.Add(New ScriptBundle("~/bundles/roomsavailability").Include(
                   "~/Scripts/App/RoomsAvailability.js",
                   "~/Scripts/DTO/CompanyModel.js",
                   "~/Scripts/DTO/RoomsModel.js",
                   "~/Scripts/DTO/DayAvailabilityModel.js",
                   "~/Scripts/DTO/RoomModelLite.js",
                   "~/Scripts/DTO/AddOnLite.js",
                   "~/Scripts/DTO/ReservationModel.js",
                   "~/Scripts/DTO/PricesModel.js",
                   "~/Scripts/DTO/PromoCodeModel.js",
                   "~/Scripts/linq.min.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/bootstrap.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/UtilImage.js"))

        '-- addons js
        bundles.Add(New ScriptBundle("~/bundles/Addons").Include(
                   "~/Scripts/App/AddOns.js",
                   "~/Scripts/DTO/AddOnsModel.js",
                   "~/Scripts/DTO/RoomModelLite.js",
                   "~/Scripts/DTO/AddOnLite.js",
                   "~/Scripts/DTO/ReservationModel.js",
                   "~/Scripts/DTO/PromoCodeModel.js",
                   "~/Scripts/linq.min.js",
                   "~/Scripts/jquery.bxslider.min.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/bootstrap.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/UtilImage.js"))

        '-- checkout js
        bundles.Add(New ScriptBundle("~/bundles/Checkout").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/App/Checkout.js",
                   "~/Scripts/DTO/RoomModelLite.js",
                   "~/Scripts/DTO/AddOnLite.js",
                    "~/Scripts/DTO/ReservationModel.js",
                   "~/Scripts/DTO/CountriesModel.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/DTO/ReservationModel.js",
                     "~/Scripts/DTO/BNRequestModel.js",
                 "~/Scripts/DTO/CredomaticRequestModel.js",
                 "~/Scripts/DTO/CardTypesModel.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/DTO/AgenciesHeaderModel.js",
                 "~/Scripts/bootstrapValidator.min.js",
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/bootstrap-select.min.js",
                   "~/Scripts/Utility.js",
                 "~/Scripts/UtilImage.js"))

        '-- confirmation js
        bundles.Add(New ScriptBundle("~/bundles/Confirmation").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/App/Confirmation.js",
                   "~/Scripts/DTO/RoomModelLite.js",
                   "~/Scripts/DTO/AddOnLite.js",
                    "~/Scripts/DTO/ReservationModel.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/DTO/ReservationModel.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/Utility.js",
                 "~/Scripts/UtilImage.js"))

        '-- tour services js
        bundles.Add(New ScriptBundle("~/bundles/ToursServices").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/DTO/ToursModel.js",
                 "~/Scripts/DTO/TourCategoryModel.js",
                 "~/Scripts/Tours/ToursServices.js",
                 "~/Scripts/jquery.blockui.js",
                 "~/Scripts/DTO/TourServiceSessionModel.js",
                 "~/Scripts/DTO/ServiceModelLite.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/Utility.js"))

        '-- service detail js
        bundles.Add(New ScriptBundle("~/bundles/ServiceDetail").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/DTO/ToursModel.js",
                 "~/Scripts/DTO/TourServiceRateModel.js",
                 "~/Scripts/DTO/TourServiceSchedModel.js",
                 "~/Scripts/Tours/ServiceDetail.js",
                 "~/Scripts/jquery.blockui.js",
                 "~/Scripts/jquery.bxslider.min.js",
                 "~/Scripts/linq.min.js",
                 "~/Scripts/DTO/TourServiceSessionModel.js",
                 "~/Scripts/DTO/ServiceModelLite.js",
                 "~/Scripts/DTO/TourServicePickupModel.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/Utility.js"))

        '-- service cart js
        bundles.Add(New ScriptBundle("~/bundles/ServiceCart").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/Tours/ServiceCart.js",
                 "~/Scripts/jquery.blockui.js",
                 "~/Scripts/bootstrapValidator.min.js",
                 "~/Scripts/bootstrap-select.min.js",
                 "~/Scripts/DTO/TourServiceSessionModel.js",
                 "~/Scripts/DTO/ServiceModelLite.js",
                 "~/Scripts/DTO/CountriesModel.js",
                 "~/Scripts/DTO/CardTypesModel.js",
                 "~/Scripts/DTO/BNRequestModel.js",
                 "~/Scripts/DTO/CredomaticRequestModel.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/Utility.js"))

        '-- service confirmation js
        bundles.Add(New ScriptBundle("~/bundles/ServiceConfirmation").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/Tours/ServiceConfirmation.js",
                 "~/Scripts/jquery.blockui.js",
                 "~/Scripts/DTO/TourServiceSessionModel.js",
                 "~/Scripts/DTO/ServiceModelLite.js",
                 "~/Scripts/DTO/PromoCodeModel.js",
                 "~/Scripts/Utility.js"))

        '-- virtual receipt checkout js
        bundles.Add(New ScriptBundle("~/bundles/VirtualReceiptCheckout").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/VirtualReceipt/VirtualReceiptCheckout.js",
                    "~/Scripts/DTO/ReservationModel.js",
                   "~/Scripts/DTO/CountriesModel.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/DTO/VirtualReceiptsModel.js",
                 "~/Scripts/DTO/VRReservationModel.js",
                     "~/Scripts/DTO/BNRequestModel.js",
                 "~/Scripts/DTO/CredomaticRequestModel.js",
                 "~/Scripts/DTO/CardTypesModel.js",
                 "~/Scripts/bootstrapValidator.min.js",
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/bootstrap-select.min.js"))

        '-- virtual receipt confirmation js
        bundles.Add(New ScriptBundle("~/bundles/VirtualReceiptConfirmation").Include(
                 "~/Scripts/bootstrap.min.js",
                 "~/Scripts/VirtualReceipt/VirtualReceiptConfirmation.js",
                 "~/Scripts/jquery.blockui.js",
                 "~/Scripts/DTO/VirtualReceiptsModel.js",
                 "~/Scripts/DTO/ServiceModelLite.js",
                  "~/Scripts/DTO/VirtualReceiptsModel.js",
                 "~/Scripts/DTO/VRReservationModel.js"))


        '-- scroller js
        bundles.Add(New ScriptBundle("~/bundles/Scroller").Include(
                   "~/Scripts/jquery.scroller.js",
                   "~/Scripts/jquery.event.move.js",
                   "~/Scripts/jquery.event.swipe.js"))

        bundles.Add(New StyleBundle("~/Content/Scroller").Include("~/Content/scroller.css"))

        ' Use the development version of Modernizr to develop with and learn from. Then, when you're
        ' ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"))

        '-- site styles
        bundles.Add(New StyleBundle("~/Content/css").Include("~/Content/site.css", "~/Content/bootstrap.min.css"))

        '-- jquery-ui styles
        bundles.Add(New StyleBundle("~/Content/themes/base/styles").Include("~/Content/themes/base/jquery-ui.min.css"))

        '-- Booking redesign styles
        bundles.Add(New StyleBundle("~/Content/BookingRedesign").Include("~/Content/BookingRedesign.css"))

        '-- font awesome styles
        bundles.Add(New StyleBundle("~/Content/FontAwesome/css/styles").Include("~/Content/FontAwesome/css/font-awesome.min.css"))

        '-- rooms availability styles
        bundles.Add(New StyleBundle("~/Content/RoomsAvailabilityCss").Include("~/Content/jquery.datepick.css",
                                                                              "~/Content/Rooms.css",
                                                                              "~/Content/PaymentPopup.css"))

        '-- add ons styles
        bundles.Add(New StyleBundle("~/Content/AddonsCss").Include("~/Content/AddOns.css",
                                                                   "~/Content/jquery.bxslider.css"))

        '-- checkout styles
        bundles.Add(New StyleBundle("~/Content/CheckoutCss").Include("~/Content/Checkout.css",
                                                                     "~/Content/bootstrapValidator.min.css",
                                                                     "~/Content/bootstrap-select.min.css"))

        '-- confirmation styles
        bundles.Add(New StyleBundle("~/Content/ConfirmationCss").Include("~/Content/Confirmation.css"))


        '-- tour site styles
        bundles.Add(New StyleBundle("~/Content/css/Tours").Include("~/Content/Tours/ToursSite.css", "~/Content/bootstrap.min.css"))

        '-- tour redesign styles 
        bundles.Add(New StyleBundle("~/Content/Tours/ToursRedesign").Include("~/Content/Tours/ToursRedesign.css"))

        '-- tour services styles
        bundles.Add(New StyleBundle("~/Content/ToursServicesCss").Include("~/Content/Tours/ToursServices.css"))

        '-- service detail styles
        bundles.Add(New StyleBundle("~/Content/ServiceDetailCss").Include("~/Content/jquery.datepick.css",
                                                                          "~/Content/Tours/ServiceDetail.css",
                                                                          "~/Content/jquery.bxslider.css"))

        '-- service cart styles
        bundles.Add(New StyleBundle("~/Content/ServiceCartCss").Include("~/Content/Tours/ServiceCart.css",
                                                                          "~/Content/bootstrapValidator.min.css",
                                                                          "~/Content/bootstrap-select.min.css"))

        '-- service confirmation styles
        bundles.Add(New StyleBundle("~/Content/ServiceConfirmationCss").Include("~/Content/Tours/ServiceConfirmation.css"))

        '-- virtual receipt checkout styles
        bundles.Add(New StyleBundle("~/Content/VirtualReceiptCheckoutCss").Include("~/Content/VirtualReceipt/VirtualReceiptCheckout.css",
                                                                     "~/Content/bootstrapValidator.min.css",
                                                                     "~/Content/bootstrap-select.min.css"))

        '-- virtual receipt confirmation styles
        bundles.Add(New StyleBundle("~/Content/VirtualReceiptConfirmationCss").Include("~/Content/VirtualReceipt/VirtualReceiptConfirmation.css"))

        bundles.Add(New StyleBundle("~/Content/themes/base/css").Include(
                    "~/Content/themes/base/jquery.ui.core.css",
                    "~/Content/themes/base/jquery.ui.resizable.css",
                    "~/Content/themes/base/jquery.ui.selectable.css",
                    "~/Content/themes/base/jquery.ui.accordion.css",
                    "~/Content/themes/base/jquery.ui.autocomplete.css",
                    "~/Content/themes/base/jquery.ui.button.css",
                    "~/Content/themes/base/jquery.ui.dialog.css",
                    "~/Content/themes/base/jquery.ui.slider.css",
                    "~/Content/themes/base/jquery.ui.tabs.css",
                    "~/Content/themes/base/jquery.ui.datepicker.css",
                    "~/Content/themes/base/jquery.ui.progressbar.css",
                    "~/Content/themes/base/jquery.ui.theme.css"))


    End Sub
End Class