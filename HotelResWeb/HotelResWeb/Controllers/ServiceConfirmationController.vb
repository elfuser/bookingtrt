﻿Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Web.Mvc
Imports BRules
Imports VPOS20_PLUGIN

Namespace Controllers
    Public Class ServiceConfirmationController
        Inherits Controller

        Private gHotelCode As String
        Private culture As String = "en-US"

        ' GET: ToursServices
        Function ServiceConfirmation(ByVal hotelcode As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCode = hotelcode.Trim.ToUpper

                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            Else
                Dim reserv As New TourServiceModel
                reserv = Utils.DeserializeTourService()
                If Not reserv Is Nothing Then
                    gHotelCode = reserv.CompanyID
                    ViewBag.HotelCode = reserv.CompanyID

                End If

            End If

            If Not Request.QueryString("hash") Is Nothing Then
                Call ProcessedCredomaticRedirect()
            End If

            'If Not Request.QueryString("BN") Is Nothing Then
            '    Call ProcessedBN()
            'End If

            'ViewBag.Message = "Thank you for staying with us !"
            'ViewBag.subtitle = "Your reservation has been processed and successfully registered, a confirmation email with all the details will arrive to your account soon, please do not make the reservation again if the email does not arrive, contact our reservation's department. Your confirmation number is: ......."

            Return View()
        End Function

        Private Sub ProcessedCredomaticRedirect()
            Dim mResponse As String = Request.QueryString("response")
            Dim mResNumber As String = Request.QueryString("orderid")
            Dim mHashReturn As String
            Dim objCompany As New Companies
            Dim mKeys() As String
            Dim mReturnKeys As String
            Dim mKeyId As String

            Dim reserv As New TourServiceModel
            reserv = Utils.DeserializeTourService()

            If reserv.CompanyID <> "DEMO2000" Then

                mReturnKeys = objCompany.GetBankCredomaticKeys(reserv.CompanyID)
                If mReturnKeys.Trim.Length > 0 Then
                    mKeys = mReturnKeys.Split("|")
                End If
                mKeyId = mKeys(0)
                Dim Key As String = mKeys(1)
                Key = "v6c4Zz52wDG5bSgFqrS6DKV42Yry2Uhc" 'uncomment for demo
                'mKeyId = "6430339"

                With Request
                    mHashReturn = Utils.GetMD5(Trim(.QueryString("orderid")) + "|" + .QueryString("amount") + "|" + .QueryString("response") + "|" + .QueryString("transactionid") + "|" + .QueryString("avsresponse") + "|" + .QueryString("cvvresponse") + "|" + .QueryString("time") + "|" + Key)
                    If .QueryString("hash") <> mHashReturn Then
                        UpdateStatus(mResNumber, 4)
                        If culture = "es-CR" Then
                            CardDenied(reserv.ReservationID, "Hay problemas con la respuesta del banco", "Los datos enviados/recibidos por el banco son incorrectos, contacte al departamento de reservaciones, disculpas por el inconveniente", .QueryString("responsetext"))
                        Else
                            CardDenied(reserv.ReservationID, "There is a problem with the bank's response", "Some of the information returned by the bank is not valid, please contact the reservation's department", .QueryString("responsetext"))
                        End If

                        Exit Sub
                    End If
                End With
            Else
                mResponse = "1"
            End If


            Try
                'Log the Card Transaction
                If Session("update") = 0 Then
                    With Request
                        'lblAuth.Text &= " " & Request.QueryString("authcode")
                        SaveBankTransaction(.QueryString("orderid"), .QueryString("response"), .QueryString("response_code"), .QueryString("authcode"), "", .QueryString("responsetext"), .QueryString("transactionid"), .QueryString("amount"))
                    End With
                End If

                Select Case mResponse
                    Case "1"
                        CardApproved(mResNumber, True, reserv.CompanyID)

                    Case "2"
                        UpdateStatus(mResNumber, 3)
                        If culture = "es-CR" Then
                            CardDenied(mResNumber, "Su tarjeta ha sido denegada, favor consultar con su banco", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        Else
                            CardDenied(mResNumber, "Your Card has been Denied, please contact your card's bank", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        End If
                    Case "3"
                        UpdateStatus(mResNumber, 4)
                        If culture = "es-CR" Then
                            CardDenied(mResNumber, "Hay un error con su tarjeta, favor contactar al banco", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        Else
                            CardDenied(mResNumber, "There is an error with your card, please contact your card's bank", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        End If
                End Select
            Catch ex As Exception
                EventLog.WriteEntry("Application", "Confirmation Load: " & ex.ToString, EventLogEntryType.Error)
                If culture = "es-CR" Then
                    CardDenied(reserv.ReservationID, "Hay un problema con su reserva", "Numero de confirmaci&oacute;n no es v&aacute;lido", Request.QueryString("responsetext"))
                Else
                    CardDenied(reserv.ReservationID, "There is a problem with your reservation", "Your confirmation number is not valid", Request.QueryString("responsetext"))
                End If
            End Try
        End Sub


        Private Sub ProcessedBN()

            Dim mResNumber As String = ""
            Dim col1 As New NameValueCollection
            col1 = Request.Params
            Dim sIDACQUIRER As String = col1.Get("IDACQUIRER")
            Dim sIDCOMMERCE As String = col1.Get("IDCOMMERCE")
            Dim sXMLRES As String = col1.Get("XMLRES")
            Dim sSESSIONKEY As String = col1.Get("SESSIONKEY")
            Dim sDIGITALSIGN As String = col1.Get("DIGITALSIGN")
            Dim oVPOSBean As New VPOSBean


            Dim reserv As New ReservationModel
            reserv = Utils.DeserializeReservationInfo()


            With oVPOSBean
                .cipheredXML = sXMLRES
                .cipheredSessionKey = sSESSIONKEY
                .cipheredSignature = sDIGITALSIGN
            End With


            Dim basePath As String = ConfigurationManager.AppSettings("keys")

            Dim R1 As String = basePath + "\" & reserv.CompanyID & "\ALIGNET.PRODUCCION.NOPHP.CRYPTO.PUBLIC.txt"
            Dim R2 As String = basePath + "\" & reserv.CompanyID & "\Llave.Privada.Firma.txt"


            Dim srVPOSLlaveCifradoPublica As New StreamReader(R1)
            Dim srComercioLlaveFirmaPrivada As New StreamReader(R2)

            Dim oVPOSReceive As New VPOSReceive(srVPOSLlaveCifradoPublica, srComercioLlaveFirmaPrivada, "9FFC9DE41FB95C4A")
            oVPOSReceive.execute(oVPOSBean)

            Try
                'Log the Card Transaction
                If oVPOSBean.validSign = True Then
                    With Request
                        SaveBankTransaction(oVPOSBean.purchaseOperationNumber.Trim, "", oVPOSBean.authorizationResult, oVPOSBean.authorizationCode, oVPOSBean.errorCode, oVPOSBean.errorMessage, "", CDbl(oVPOSBean.purchaseAmount) / 100)
                    End With

                    If oVPOSBean.errorCode = "00" Then
                        CardApproved(oVPOSBean.purchaseOperationNumber.Trim, True, reserv.CompanyID)
                    Else
                        UpdateStatus(oVPOSBean.purchaseOperationNumber.Trim, 3)
                        CardDenied(oVPOSBean.purchaseOperationNumber.Trim, "Your reservation has not been paid", "The card has been denied or the payment has been cancelled", oVPOSBean.errorMessage)
                    End If

                    ViewBag.lblCode = oVPOSBean.authorizationCode
                    ViewBag.lblResult = oVPOSBean.authorizationResult
                    ViewBag.lblErrorCode = oVPOSBean.errorCode
                    ViewBag.lblErrorMsg = oVPOSBean.errorMessage

                End If
            Catch ex As Exception
                EventLog.WriteEntry("Application", "Processed BN: " & ex.ToString, EventLogEntryType.Error)
                CardDenied(reserv.ReservationID, "There is a problem with your transaction", "The response from the bank can not be validated, please contact our reservations department", "")

            End Try

        End Sub


        Private Sub CardApproved(ByVal sResNumber As String, ByVal Availability As Boolean, ByVal CompanyId As String)

            MsgSuccess(sResNumber)
            'Update Availability and change reservation status to 2 (Reserved)
            If Availability Then UpdateAvailability(sResNumber)

            PrepareEmail(sResNumber, CompanyId)
        End Sub

        Private Function UpdateAvailability(ReservNumber As String) As Boolean

            Dim objRes As New Sales

            Try

                objRes.SaveAvailability(ReservNumber)

            Catch ex As Exception
                Throw ex
            Finally
                objRes = Nothing
            End Try

            Return True

        End Function

        Private Function UpdateStatus(ReservNumber As String, Status As String) As Boolean

            Dim objRes As New Sales

            Try

                objRes.UpdateStatus(ReservNumber, Status)

            Catch ex As Exception
                Throw ex
            Finally
                objRes = Nothing
            End Try

            Return True

        End Function

        Private Sub SaveBankTransaction(ByVal ReservationID As String, ByVal Response As String, ByVal ResponseCode As String,
                                ByVal AuthCode As String, ByVal ErrorCode As String, ByVal ResponseText As String,
                                ByVal TransactionID As String, ByVal Amount As Double)

            Dim clsCred As New Reservations

            Try
                clsCred.SaveBankLog(ReservationID, Response, AuthCode, ResponseCode, ErrorCode, ResponseText, TransactionID, Amount)

            Catch ex As Exception
                Throw ex
            Finally
                clsCred = Nothing
            End Try
        End Sub


        Private Sub PrepareEmail(ByVal Reservation As String, ByVal CompanyId As String)
            Dim companies As New List(Of CompanyInfo)
            Dim company As New CompanyInfo

            Dim Hotel, Tel1, TollFree, Website As String
            Dim objComp As New Companies
            Dim objC As List(Of CompanyInfo)

            objC = objComp.GetCompanyTermsEmails(CompanyId, "en-US")

            Hotel = objC(0).ppHotel
            Tel1 = objC(0).ppTel1
            TollFree = ""
            Website = objC(0).ppURL

            SendEmail(Tel1, "", "", TollFree, Reservation, objC(0).ppResEmail, Website, Hotel, objC(0).ppResEmailAlt, objC(0).ppTerms, objC(0).ppWaiver, CompanyId, "en-US")
        End Sub

        Private Sub SendEmail(ByVal Tel1 As String, ByVal Tel2 As String, ByVal Fax As String,
                          ByVal TollFree As String, ByVal Reservation As String, ByVal Email As String, ByVal Website As String,
                          ByVal Hotel As String, ByVal EmailAlt As String, ByVal Terms As String, ByVal Waiver As String,
                          ByVal CompanyId As String, ByVal Culture As String)
            Dim strFrom As String = Hotel & " <info@bookingtrtinteractive.com>"
            Dim strFrom2 As String = "info@bookingtrtinteractive.com"

            Dim strTo As String
            Dim mBody, mBodycc As String
            Dim mItemsTable As String = ""
            Dim mServicesTerms As String = ""
            Dim fp, fpcc As StreamReader
            Dim i As Integer = 0
            Dim objSaleItems As List(Of clsSaleItemInfo)
            Dim j As Integer
            Dim dSubtotalLine, dSubtotal, dDiscount As Double

            Try
                Dim objRes As New Sales
                Dim objR As List(Of clsSaleInfo)

                objR = objRes.GetSale(Reservation)

                If objR.Count > 0 Then
                    fp = System.IO.File.OpenText(Server.MapPath("templates\Email.html"))
                    mBody = fp.ReadToEnd()
                    fp.Close()

                    fpcc = System.IO.File.OpenText(Server.MapPath("templates\Email_cc.html"))
                    mBodycc = fpcc.ReadToEnd()
                    fpcc.Close()

                    With objR(0)
                        'Replace tags in the body
                        mBody = Replace(mBody, "<$code$>", Trim(CompanyId))
                        mBody = Replace(mBody, "<$confirmation$>", Reservation)
                        mBody = Replace(mBody, "<$date$>", Now.Date.ToShortDateString)
                        mBody = Replace(mBody, "<$remarks$>", "Requests: " & .ppRemarks.Trim)

                        mBodycc = Replace(mBodycc, "<$code$>", Trim(CompanyId))
                        mBodycc = Replace(mBodycc, "<$confirmation$>", Reservation)
                        mBodycc = Replace(mBodycc, "<$date$>", Now.Date.ToShortDateString)
                        mBodycc = Replace(mBodycc, "<$remarks$>", "Requests: " & .ppRemarks.Trim)

                        'Guest Details
                        mBody = Replace(mBody, "<$name$>", .ppName & " " & .ppLastName)
                        mBody = Replace(mBody, "<$address$>", .ppEmail)
                        mBody = Replace(mBody, "<$phone$>", .ppTelephone)
                        mBody = Replace(mBody, "<$country$>", .ppCountry)

                        mBodycc = Replace(mBodycc, "<$name$>", .ppName & " " & .ppLastName)
                        mBodycc = Replace(mBodycc, "<$address$>", .ppEmail)
                        mBodycc = Replace(mBodycc, "<$phone$>", .ppTelephone)
                        mBodycc = Replace(mBodycc, "<$country$>", .ppCountry)

                        'Footer
                        mBody = Replace(mBody, "<$company$>", Hotel)
                        mBody = Replace(mBody, "<$coaddress$>", "")
                        mBody = Replace(mBody, "<$cophone$>", Tel1)
                        mBody = Replace(mBody, "<$cocountry$>", "Costa Rica")
                        mBody = Replace(mBody, "<$coemail$>", Email)

                        strTo = .ppEmail
                    End With

                    objSaleItems = objRes.GetSaleItems(Reservation, Culture)

                    mServicesTerms = "<b>Please review the following conditions for each item:</b>"

                    For j = 0 To objSaleItems.Count - 1
                        'Calculate subtotal 
                        dSubtotalLine = objSaleItems(j).ppAdultQty * objSaleItems(j).ppAdultAmount + objSaleItems(j).ppChildrenQty * objSaleItems(j).ppChildrenAmount + objSaleItems(j).ppInfantsQty * objSaleItems(j).ppInfantsAmount + objSaleItems(j).ppStudentsQty * objSaleItems(j).ppStudentsAmount - objSaleItems(j).ppDiscount

                        mItemsTable &= "<tr>" &
                     "<td width=" & Chr(34) & "70" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objSaleItems(j).ppArrival.ToShortDateString & "</p></td>" &
                     "<td width=" & Chr(34) & "70" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objSaleItems(j).ppSchedule & "</p></td>" &
                     "<td width=" & Chr(34) & "251" & Chr(34) & " align=" & Chr(34) & "left" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objSaleItems(j).ppDescription & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objSaleItems(j).ppAdultQty & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objSaleItems(j).ppChildrenQty & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objSaleItems(j).ppInfantsQty & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objSaleItems(j).ppStudentsQty & "</p></td>" &
                     "<td width=" & Chr(34) & "70" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(dSubtotalLine, "n") & "</p></td>"
                        mItemsTable &= "</tr>"

                        mServicesTerms &= "<p><b>" & objSaleItems(j).ppDescription & "</b></p>"
                        mServicesTerms &= "<p>Conditions: " & objSaleItems(j).ppConditions & "</p>"
                        If objSaleItems(j).ppBringInfo.Trim <> "" Then mServicesTerms &= "<p>Bring Info: " & objSaleItems(j).ppBringInfo & "</p>"

                        dDiscount += objSaleItems(j).ppDiscount
                        dSubtotal += dSubtotalLine
                    Next
                    mBody = Replace(mBody, "<$itemsTable$>", mItemsTable)

                    mBody = Replace(mBody, "<$servicesTerms$>", mServicesTerms)
                    mBody = Replace(mBody, "<$terms$>", Terms)
                    mBody = Replace(mBody, "<$waiver$>", Waiver)

                    mBody = Replace(mBody, "<$subtotal$>", Format(dSubtotal, "n"))
                    mBody = Replace(mBody, "<$discount$>", Format(objR(0).ppDiscount, "n"))
                    mBody = Replace(mBody, "<$tax$>", Format(objR(0).ppTaxAmount, "n"))
                    mBody = Replace(mBody, "<$total$>", Format(objR(0).ppAmount, "n"))

                    mBodycc = Replace(mBodycc, "<$itemsTable$>", mItemsTable)
                    mBodycc = Replace(mBodycc, "<$subtotal$>", Format(dSubtotal, "n"))
                    mBodycc = Replace(mBodycc, "<$discount$>", Format(objR(0).ppDiscount, "n"))
                    mBodycc = Replace(mBodycc, "<$tax$>", Format(objR(0).ppTaxAmount, "n"))
                    mBodycc = Replace(mBodycc, "<$total$>", Format(objR(0).ppAmount, "n"))

                    Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
                    If EmailAlt.Trim <> String.Empty Then
                        MailMsg.Bcc.Add(EmailAlt)
                    End If

                    Dim MailMsg2 As New MailMessage(New MailAddress(strFrom2.Trim()), New MailAddress(Email))
                    If EmailAlt.Trim <> String.Empty Then
                        MailMsg2.Bcc.Add(EmailAlt)
                    End If

                    MailMsg.BodyEncoding = Encoding.Default
                    If Culture = "es-CR" Then
                        MailMsg.Subject = "Confirmacion de Reserva"
                    Else
                        MailMsg.Subject = "Confirmed Reservation Notification"
                    End If

                    MailMsg2.BodyEncoding = Encoding.Default
                    MailMsg2.Subject = "Confirmacion de Reserva"

                    MailMsg.Body = mBody
                    MailMsg.Priority = MailPriority.Normal
                    MailMsg.IsBodyHtml = True

                    MailMsg2.Body = mBodycc
                    MailMsg2.Priority = MailPriority.Normal
                    MailMsg2.IsBodyHtml = True

                    Dim SmtpMail As New SmtpClient
                    SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                    SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                    SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                    SmtpMail.Port = 8889
                    SmtpMail.Send(MailMsg2)
                    SmtpMail.Send(MailMsg)
                End If


            Catch ex As Exception
                EventLog.WriteEntry("Application", "SendEmail: " & ex.ToString, EventLogEntryType.Error)

            End Try
        End Sub

        Private Sub SendNotification(ByVal ResID As String, ByVal Mess As String)
            Dim objComp As New Companies
            Dim mEmails() As String

            'comment to test
            mEmails = Split(objComp.GetCompanyEmails(gHotelCode), "|")

            Dim strFrom As String = "info@bookingtrtinteractive.com"
            Dim objRes As New Sales
            Dim objR As List(Of clsSaleInfo)

            Dim strTo As String = mEmails(0)
            'Dim strTo As String = "test@gmail.com"
            Dim mBody As StringBuilder = New StringBuilder("")
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))

            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Transaccion Rechazada"

            Try
                mBody.Append("<h2>Transacci&oacute;n Rechazada</h2>")
                mBody.Append("<p>El cobro para la reserva # " & ResID & " ha sido rechazado por el banco")
                mBody.Append("<p>Motivo: " & Mess)
                mBody.Append("<p><b>Datos del cliente</b> ")

                objR = objRes.GetSale(ResID)

                If objR.Count > 0 Then
                    With objR(0)
                        mBody.Append("<p>Nombre: " & .ppName & " " & .ppLastName)
                        mBody.Append("<p>Tel: " & .ppTelephone)
                        mBody.Append("<p>Email: " & .ppEmail)
                    End With
                End If

                MailMsg.Body = mBody.ToString
                MailMsg.Priority = MailPriority.Normal
                MailMsg.IsBodyHtml = True

                Dim SmtpMail As New SmtpClient
                SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                SmtpMail.Port = 8889
                SmtpMail.Send(MailMsg)

            Catch ex As Exception
                EventLog.WriteEntry("Application", "SendNotification: " & ex.ToString, EventLogEntryType.Error)
            Finally
                objR = Nothing
                objRes = Nothing
            End Try
        End Sub


        Private Sub MsgSuccess(reserv As String)


            If culture = "es-CR" Then
                ViewBag.Message = "Gracias por seleccionar su estadia con nosotros !"
                ViewBag.subtitle = "Su reserva ha sido procesada con &eacute;xito, un correo de confirmaci&oacute;n ser&aacute; enviado pronto con los detalles, no es necesario realizar de nuevo la reserva si su correo no llega, en dado caso contacte al departamento de reservaciones por favor. Su n&uacute;mero de confirmaci&oacute;n es: " + reserv
            Else
                ViewBag.Message = "Thank you for staying with us !"
                ViewBag.subtitle = "Your reservation has been processed and successfully registered, a confirmation email with all the details will arrive to your account soon, please do not make the reservation again if the email does not arrive, contact our reservation's department. Your confirmation number is: " + reserv
            End If

        End Sub


        Private Sub CardDenied(ByVal sResNumber As String, ByVal sMessage As String, ByVal sResponse As String, ByVal ErrorMsg As String)
            Dim msj As New StringBuilder("")
            ViewBag.Message = sMessage

            If culture = "es-CR" Then
                ViewBag.subtitle = "Error: " & sResponse & ", si gusta puede volver a corregir los datos de la tarjeta haciendo click en el vinculo abajo o puede llamar a nuestro departamento de reservas, disculpas por el inconveniente."
            Else
                ViewBag.subtitle = "The reason is: " & sResponse & ", you can review your card information again (do not press Back on the browser) or call our customer service department for assistance, we are sorry for the inconvenience."
            End If
            SendNotification(sResNumber, ErrorMsg)
        End Sub

    End Class
End Namespace