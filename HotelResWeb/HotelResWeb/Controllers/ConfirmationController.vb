﻿Imports System.Web.Mvc
Imports BRules
Imports VPOS20_PLUGIN
Imports System.Net.Mail
Imports System.Net
Imports System.IO


Namespace Controllers
    Public Class ConfirmationController
        Inherits Controller

        Private gHotelCode As String
        Private culture As String = "EN-us"

        ' GET: Confirmation
        Function Confirmation(ByVal hotelcode As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                hotelcode = hotelcode.Trim.ToUpper
                ViewBag.HotelCode = hotelcode

                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            Else
                Dim reserv As New ReservationModel
                reserv = Utils.DeserializeReservationInfo()
                If Not reserv Is Nothing Then
                    gHotelCode = reserv.CompanyID
                    ViewBag.HotelCode = reserv.CompanyID
                End If

            End If

            If Utils.UserObject IsNot Nothing Then
                ViewBag.UserName = Utils.UserObject.UserName
                ViewBag.UserName_Email = Utils.UserObject.UserName_Email
            End If

            If Not Request.QueryString("hash") Is Nothing Then
                Call ProcessedCredomaticRedirect()
                Return View()
            End If


            'falta verficar que devuelve el BN cuando terminar de procesar ??????
            If Not Request.QueryString("BN") Is Nothing Then
                Call ProcessedBN()
                Return View()
            End If


            Return View()
        End Function



        Private Sub ProcessedCredomaticRedirect()
            Dim mResponse As String = Request.QueryString("response")
            Dim mResNumber As String = Request.QueryString("orderid")
            Dim mHashReturn As String
            Dim objCompany As New Companies
            Dim mKeys() As String
            Dim mReturnKeys As String
            Dim mKeyId As String

            Dim reserv As New ReservationModel
            reserv = Utils.DeserializeReservationInfo()

            '-- Uncomment this block to test successful payment
            'Dim TESTRS As Boolean = True
            'If reserv.CompanyID <> "DEMO2000" And Not TESTRS Then 
            '--

            If reserv.CompanyID <> "DEMO2000" Then


                mReturnKeys = objCompany.GetBankCredomaticKeys(reserv.CompanyID)
                If mReturnKeys.Trim.Length > 0 Then
                    mKeys = mReturnKeys.Split("|")
                End If
                mKeyId = mKeys(0)
                Dim Key As String = mKeys(1)
                Key = "v6c4Zz52wDG5bSgFqrS6DKV42Yry2Uhc" 'uncomment for demo
                'mKeyId = "6430339"

                With Request
                    mHashReturn = Utils.GetMD5(Trim(.QueryString("orderid")) + "|" + .QueryString("amount") + "|" + .QueryString("response") + "|" + .QueryString("transactionid") + "|" + .QueryString("avsresponse") + "|" + .QueryString("cvvresponse") + "|" + .QueryString("time") + "|" + Key)
                    If .QueryString("hash") <> mHashReturn Then
                        UpdateStatus(mResNumber, 4)
                        If culture = "es-CR" Then
                            CardDenied(reserv.ReservationID, "Hay problemas con la respuesta del banco", "Los datos enviados/recibidos por el banco son incorrectos, contacte al departamento de reservaciones, disculpas por el inconveniente", .QueryString("responsetext"))
                        Else
                            CardDenied(reserv.ReservationID, "There is a problem with the bank's response", "Some of the information returned by the bank is not valid, please contact the reservation's department", .QueryString("responsetext"))
                        End If

                        Exit Sub
                    End If
                End With
            Else
                mResponse = "1"
            End If


            Try
                'Log the Card Transaction
                If Session("update") = 0 Then
                    With Request
                        'lblAuth.Text &= " " & Request.QueryString("authcode")
                        SaveBankTransaction(.QueryString("orderid"), .QueryString("response"), .QueryString("response_code"), .QueryString("authcode"), "", .QueryString("responsetext"), .QueryString("transactionid"), .QueryString("amount"))
                    End With
                End If

                Select Case mResponse
                    Case "1"
                        CardApproved(mResNumber, True)

                    Case "2"
                        UpdateStatus(mResNumber, 3)
                        If culture = "es-CR" Then
                            CardDenied(mResNumber, "Su tarjeta ha sido denegada, favor consultar con su banco", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        Else
                            CardDenied(mResNumber, "Your Card has been Denied, please contact your card's bank", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        End If
                    Case "3"
                        UpdateStatus(mResNumber, 4)
                        If culture = "es-CR" Then
                            CardDenied(mResNumber, "Hay un error con su tarjeta, favor contactar al banco", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        Else
                            CardDenied(mResNumber, "There is an error with your card, please contact your card's bank", Request.QueryString("responsetext"), Request.QueryString("responsetext"))
                        End If
                End Select
            Catch ex As Exception
                EventLog.WriteEntry("Application", "Confirmation Load: " & ex.ToString, EventLogEntryType.Error)
                If culture = "es-CR" Then
                    CardDenied(reserv.ReservationID, "Hay un problema con su reserva", "Numero de confirmaci&oacute;n no es v&aacute;lido", Request.QueryString("responsetext"))
                Else
                    CardDenied(reserv.ReservationID, "There is a problem with your reservation", "Your confirmation number is not valid", Request.QueryString("responsetext"))
                End If
            End Try
        End Sub


        Private Sub ProcessedBN()

            Dim mResNumber As String = ""
            Dim col1 As New NameValueCollection
            col1 = Request.Params
            Dim sIDACQUIRER As String = col1.Get("IDACQUIRER")
            Dim sIDCOMMERCE As String = col1.Get("IDCOMMERCE")
            Dim sXMLRES As String = col1.Get("XMLRES")
            Dim sSESSIONKEY As String = col1.Get("SESSIONKEY")
            Dim sDIGITALSIGN As String = col1.Get("DIGITALSIGN")
            Dim oVPOSBean As New VPOSBean


            Dim reserv As New ReservationModel
            reserv = Utils.DeserializeReservationInfo()


            With oVPOSBean
                .cipheredXML = sXMLRES
                .cipheredSessionKey = sSESSIONKEY
                .cipheredSignature = sDIGITALSIGN
            End With


            Dim basePath As String = ConfigurationManager.AppSettings("keys")

            Dim R1 As String = basePath + "\" & reserv.CompanyID & "\ALIGNET.PRODUCCION.NOPHP.CRYPTO.PUBLIC.txt"
            Dim R2 As String = basePath + "\" & reserv.CompanyID & "\Llave.Privada.Firma.txt"


            Dim srVPOSLlaveCifradoPublica As New StreamReader(R1)
            Dim srComercioLlaveFirmaPrivada As New StreamReader(R2)

            Dim oVPOSReceive As New VPOSReceive(srVPOSLlaveCifradoPublica, srComercioLlaveFirmaPrivada, "9FFC9DE41FB95C4A")
            oVPOSReceive.execute(oVPOSBean)

            Try
                'Log the Card Transaction
                If oVPOSBean.validSign = True Then
                    With Request
                        SaveBankTransaction(oVPOSBean.purchaseOperationNumber.Trim, "", oVPOSBean.authorizationResult, oVPOSBean.authorizationCode, oVPOSBean.errorCode, oVPOSBean.errorMessage, "", CDbl(oVPOSBean.purchaseAmount) / 100)
                    End With

                    If oVPOSBean.errorCode = "00" Then
                        CardApproved(oVPOSBean.purchaseOperationNumber.Trim, True)
                    Else
                        UpdateStatus(oVPOSBean.purchaseOperationNumber.Trim, 3)
                        CardDenied(oVPOSBean.purchaseOperationNumber.Trim, "Your reservation has not been paid", "The card has been denied or the payment has been cancelled", oVPOSBean.errorMessage)
                    End If

                    ViewBag.lblCode = oVPOSBean.authorizationCode
                    ViewBag.lblResult = oVPOSBean.authorizationResult
                    ViewBag.lblErrorCode = oVPOSBean.errorCode
                    ViewBag.lblErrorMsg = oVPOSBean.errorMessage

                End If
            Catch ex As Exception
                EventLog.WriteEntry("Application", "Processed BN: " & ex.ToString, EventLogEntryType.Error)
                CardDenied(reserv.ReservationID, "There is a problem with your transaction", "The response from the bank can not be validated, please contact our reservations department", "")

            End Try

        End Sub


        Private Sub CardApproved(ByVal sResNumber As String, ByVal Availability As Boolean)

            MsgSuccess(sResNumber)
            'Update Availability and change reservation status to 2 (Reserved)
            If Availability Then UpdateAvailability(sResNumber)
        End Sub

        Private Function UpdateAvailability(ReservNumber As String) As Boolean

            Dim objRes As New Reservations

            Try

                objRes.SaveAvailability(ReservNumber, 1)

            Catch ex As Exception
                Throw ex
            Finally
                objRes = Nothing
            End Try

            Return True

        End Function

        Private Function UpdateStatus(ReservNumber As String, Status As String) As Boolean

            Dim objRes As New Reservations

            Try

                objRes.UpdateStatus(ReservNumber, Status, 1)

            Catch ex As Exception
                Throw ex
            Finally
                objRes = Nothing
            End Try

            Return True

        End Function

        Private Sub SaveBankTransaction(ByVal ReservationID As String, ByVal Response As String, ByVal ResponseCode As String,
                                ByVal AuthCode As String, ByVal ErrorCode As String, ByVal ResponseText As String,
                                ByVal TransactionID As String, ByVal Amount As Double)

            Dim clsCred As New Reservations

            Try
                clsCred.SaveBankLog(ReservationID, Response, AuthCode, ResponseCode, ErrorCode, ResponseText, TransactionID, Amount)

            Catch ex As Exception
                Throw ex
            Finally
                clsCred = Nothing
            End Try
        End Sub


        Private Sub PrepareEmail(ByVal Reservation As String)
            Dim objComp As New Companies
            Dim mEmails() As String
            Dim companies As New List(Of CompanyInfo)
            Dim company As New CompanyInfo

            mEmails = Split(objComp.GetCompanyEmails(gHotelCode), "|")


            companies = objComp.GetCompany(gHotelCode, "SEL", "H")

            If companies.Count > 0 Then
                company = companies(0)
            End If

            SendEmail(company, Reservation, mEmails(0), mEmails(1))
        End Sub

        Private Sub SendEmail(company As CompanyInfo, ByVal Reservation As String, ByVal Email As String, ByVal EmailAlt As String)

            Dim strFrom As String = company.ppHotel & " <info@bookingtrtinteractive.com>"
            Dim strFrom2 As String = company.ppHotel & " <info@bookingtrtinteractive.com>"
            Dim strTo As String
            Dim mBody As String
            Dim mBodycc As String
            Dim mRoomTable As String = ""
            Dim fp, fpcc As StreamReader
            Dim i As Integer = 0
            Dim objRes As New Reservations
            Dim objRoomAvailable As List(Of clsReservationsRoomInfo)
            Dim j As Integer
            Dim dBalance, dTotal As Double
            Dim objR As List(Of clsReservationInfo)
            Dim rateCode As String
            Dim nights As Integer

            Try

                Dim reserv As New ReservationModel
                reserv = Utils.DeserializeReservationInfo()



                objR = objRes.GetReservation(Reservation)

                If objR.Count > 0 Then



                    fpcc = System.IO.File.OpenText(Server.MapPath("templates\Email_cc.html"))
                    mBodycc = fpcc.ReadToEnd()
                    fpcc.Close()

                    With objR(0)

                        nights = .ppDeparture.Subtract(.ppArrival).TotalDays

                        rateCode = .ppRate
                        'Replace tags in the body
                        mBody.Replace("<$confirmation$>", Reservation)
                        mBody.Replace("<$code$>", gHotelCode)
                        mBody.Replace("<$date$>", Now.Date.ToShortDateString)
                        mBody.Replace("<$hotelName$>", company.ppHotel)
                        mBody.Replace("<$hotelPhone$>", company.ppTel1)
                        mBody.Replace("<$hotelEmail$>", company.ppEmail)

                        If company.ppDescriptor.Trim = String.Empty Then
                            mBody.Replace("<$descriptor$>", "")
                        Else
                            mBody.Replace("<$descriptor$>", "Please note that the details of this transaction will appear on your card statement on behalf of " & company.ppDescriptor)
                        End If




                        If culture = "es-CR" Then
                            mBody.Replace("<$checkinout$>", Resources.string_es.lblCheckIn1 & .ppArrival.ToShortDateString & " | " & Resources.string_es.lblCheckOut1 & .ppDeparture.ToShortDateString & " | " & Resources.string_es.lblNights1 & .ppDeparture.Subtract(.ppArrival).TotalDays.ToString)
                            mBody.Replace("<$nights$>", Resources.string_es.lblRateSel1 & .ppRate & " | " & Resources.string_es.lblRooms1 & .ppRooms.ToString)
                            mBody.Replace("<$motive$>", Resources.string_es.lblPurpose1 & .ppVisitReason.Trim)
                            mBody.Replace("<$remarks$>", Resources.string_es.lblRequest1 & .ppRemarks.Trim)
                            If .ppRateTerms.Trim <> "" Then
                                mBody.Replace("<$TermsRate$>", Resources.string_es.lblTermsCondRate1 & "<br /> " & .ppRateTerms.Trim)
                            Else
                                mBody.Replace("<$TermsRate$>", "")
                            End If
                        Else
                            mBody.Replace("<$checkinout$>", "Check In: " & .ppArrival.ToShortDateString & " | Check Out: " & .ppDeparture.ToShortDateString & " | Nights: " & .ppDeparture.Subtract(.ppArrival).TotalDays.ToString)
                            mBody.Replace("<$nights$>", "Rate: " & .ppRate & " | Rooms: " & .ppRooms.ToString)
                            mBody.Replace("<$motive$>", "Motive of visit: " & .ppVisitReason.Trim)
                            mBody.Replace("<$remarks$>", "Requests: " & .ppRemarks.Trim)
                            If .ppRateTerms.Trim <> "" Then
                                mBody.Replace("<$TermsRate$>", "Conditions of the Package/Rate: <br /> " & .ppRateTerms.Trim)
                            Else
                                mBody.Replace("<$TermsRate$>", "")
                            End If
                        End If

                        mBodycc.Replace("<$confirmation$>", Reservation)
                        mBodycc.Replace("<$date$>", Now.Date.ToShortDateString)
                        mBodycc.Replace("<$checkinout$>", "Check In: " & .ppArrival.ToShortDateString & " | Check Out: " & .ppDeparture.ToShortDateString & " | Nights: " & .ppDeparture.Subtract(.ppArrival).TotalDays.ToString)
                        mBodycc.Replace("<$nights$>", "Rate: " & .ppRate & " | Rooms: " & .ppRooms.ToString)
                        mBodycc.Replace("<$motive$>", "Motive of visit: " & .ppVisitReason.Trim)
                        mBodycc.Replace("<$remarks$>", "Requests: " & .ppRemarks.Trim)

                        'Guest Details
                        mBody.Replace("<$name$>", .ppName & " " & .ppLastName)
                        mBody.Replace("<$address$>", .ppEmail)
                        mBody.Replace("<$phone$>", .ppTelephone)
                        mBody.Replace("<$country$>", .ppCountry)
                        mBody.Replace("<$card$>", reserv.CardNumber & " / " & reserv.ExpeYear & "/ " & reserv.SecureCode)

                        mBodycc.Replace("<$name$>", .ppName & " " & .ppLastName)
                        mBodycc.Replace("<$address$>", .ppEmail)
                        mBodycc.Replace("<$phone$>", .ppTelephone)
                        mBodycc.Replace("<$country$>", .ppCountry)
                        mBodycc.Replace("<$card$>", reserv.CardNumber & " / " & reserv.ExpeYear & "/ " & reserv.SecureCode)

                        strTo = .ppEmail
                    End With

                    Dim rates As New Rates


                    Dim mNochesGratis As Integer = rates.GetRatesFreeNights(gHotelCode, rateCode)

                    objRoomAvailable = objRes.GetReservationRooms(Reservation, culture)

                    For j = 0 To objRoomAvailable.Count - 1
                        mRoomTable &= "<tr>" &
                     "<td width=" & Chr(34) & "100" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objRoomAvailable(j).ppDate.ToShortDateString & "</p></td>" &
                     "<td width=" & Chr(34) & "291" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objRoomAvailable(j).ppDescription & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppAdults & "</p></td>" &
                     "<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppChildren & "</p></td>" &
                     "<td width=" & Chr(34) & "70" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppChildrenFree & "</p></td>"
                        If i = nights Then
                            If mNochesGratis = 0 Then
                                mRoomTable &= "<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(objRoomAvailable(j).ppRateAmount, "c") & "</p></td>"
                                dBalance = objRoomAvailable(j).ppRateAmount
                            Else
                                mRoomTable &= "<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">0.00</p></td>"
                                dBalance = 0
                            End If
                        Else
                            mRoomTable &= "<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(objRoomAvailable(j).ppRateAmount, "c") & "</p></td>"
                            dBalance = objRoomAvailable(j).ppRateAmount
                        End If
                        mRoomTable &= "</tr>"

                        dTotal += dBalance
                    Next

                    Dim objAddons As New BRules.Addons
                    Dim objAddon As List(Of AddonInfo)
                    objAddon = objAddons.GetReservationAddons(Reservation, culture)
                    For i = 0 To objAddon.Count - 1
                        With objAddon(i)
                            mRoomTable &= "<tr>" &
                         "<td width=" & Chr(34) & "100" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & .ppQty & "</p></td>" &
                         "<td width=" & Chr(34) & "291" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & .ppDescription & "</p></td>" &
                         "<td width=" & Chr(34) & "50" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>" &
                         "<td width=" & Chr(34) & "50" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>" &
                         "<td width=" & Chr(34) & "70" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>" &
                         "<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(.ppAmount, "c") & "</p></td>" &
                         "</tr>"
                        End With
                    Next

                    Dim objTerm As New Companies
                    Dim sTerms As String = objTerm.GetCompanyTerm(gHotelCode, "SEL", culture)

                    mBody.Replace("<$terms$>", sTerms)
                    mBody.Replace("<$roomsTable$>", mRoomTable)
                    mBody.Replace("<$subtotal$>", Format(reserv.Subtotal, "n"))
                    mBody.Replace("<$addons$>", Format(reserv.AddonAmount, "n"))
                    mBody.Replace("<$discount$>", reserv.Discount)
                    mBody.Replace("<$tax$>", reserv.TaxAmount)
                    mBody.Replace("<$total$>", reserv.total)

                    mBodycc.Replace("<$roomsTable$>", mRoomTable)
                    mBodycc.Replace("<$subtotal$>", Format(reserv.Subtotal, "n"))
                    mBodycc.Replace("<$addons$>", Format(reserv.AddonAmount, "n"))
                    mBodycc.Replace("<$discount$>", reserv.Discount)
                    mBodycc.Replace("<$tax$>", reserv.TaxAmount)
                    mBodycc.Replace("<$total$>", reserv.total)

                    Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
                    If EmailAlt.Trim <> String.Empty Then
                        MailMsg.Bcc.Add(EmailAlt)
                    End If

                    Dim MailMsg2 As New MailMessage(New MailAddress(strFrom2.Trim()), New MailAddress(Email))
                    If EmailAlt.Trim <> String.Empty Then
                        MailMsg2.Bcc.Add(EmailAlt)
                    End If

                    MailMsg.BodyEncoding = Encoding.Default
                    If culture = "es-CR" Then
                        MailMsg.Subject = "Confirmacion de Reserva"
                    Else
                        MailMsg.Subject = "Confirmed Reservation Notification"
                    End If

                    MailMsg2.BodyEncoding = Encoding.Default
                    MailMsg2.Subject = "Confirmacion de Reserva"

                    MailMsg.Body = mBody
                    MailMsg.Priority = MailPriority.Normal
                    MailMsg.IsBodyHtml = True

                    MailMsg2.Body = mBodycc
                    MailMsg2.Priority = MailPriority.Normal
                    MailMsg2.IsBodyHtml = True

                    Dim SmtpMail As New SmtpClient
                    SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                    SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                    SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                    SmtpMail.Port = 8889
                    SmtpMail.Send(MailMsg2)
                    SmtpMail.Send(MailMsg)
                End If

            Catch ex As Exception
                EventLog.WriteEntry("Application", "SendEmail: " & ex.ToString, EventLogEntryType.Error)

            End Try
        End Sub


        Private Sub SendNotification(ByVal ResID As String, ByVal Mess As String)
            Dim objComp As New Companies
            Dim mEmails() As String

            mEmails = Split(objComp.GetCompanyEmails(gHotelCode), "|")

            Dim strFrom As String = "info@bookingtrtinteractive.com"
            Dim objRes As New Reservations
            Dim objR As List(Of clsReservationInfo)

            Dim strTo As String = mEmails(0)
            Dim mBody As StringBuilder = New StringBuilder("")
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))

            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Transaccion Rechazada"

            Try
                mBody.Append("<h2>Transacci&oacute;n Rechazada</h2>")
                mBody.Append("<p>El cobro para la reserva # " & ResID & " ha sido rechazado por el banco")
                mBody.Append("<p>Motivo: " & Mess)
                mBody.Append("<p><b>Datos del cliente</b> ")

                objR = objRes.GetReservation(ResID)

                If objR.Count > 0 Then
                    With objR(0)
                        mBody.Append("<p>Nombre: " & .ppName & " " & .ppLastName)
                        mBody.Append("<p>Tel: " & .ppTelephone)
                        mBody.Append("<p>Email: " & .ppEmail)
                    End With
                End If

                MailMsg.Body = mBody.ToString
                MailMsg.Priority = MailPriority.Normal
                MailMsg.IsBodyHtml = True

                '-- Uncomment this code to send the email --

                Dim SmtpMail As New SmtpClient
                SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                SmtpMail.Port = 8889
                SmtpMail.Send(MailMsg)

            Catch ex As Exception
                EventLog.WriteEntry("Application", "SendNotification: " & ex.ToString, EventLogEntryType.Error)
            Finally
                objR = Nothing
                objRes = Nothing
            End Try
        End Sub


        Private Sub MsgSuccess(reserv As String)


            If culture = "es-CR" Then
                ViewBag.Message = "Gracias por seleccionar su estadia con nosotros !"
                ViewBag.subtitle = "Su reserva ha sido procesada con &eacute;xito, un correo de confirmaci&oacute;n ser&aacute; enviado pronto con los detalles, no es necesario realizar de nuevo la reserva si su correo no llega, en dado caso contacte al departamento de reservaciones por favor. Su n&uacute;mero de confirmaci&oacute;n es: " + reserv
            Else
                ViewBag.Message = "Thank you for staying with us !"
                ViewBag.subtitle = "Your reservation has been processed and successfully registered, a confirmation email with all the details will arrive to your account soon, please do not make the reservation again if the email does not arrive, contact our reservation's department. Your confirmation number is: " + reserv
            End If

        End Sub


        Private Sub CardDenied(ByVal sResNumber As String, ByVal sMessage As String, ByVal sResponse As String, ByVal ErrorMsg As String)
            Dim msj As New StringBuilder("")
            ViewBag.Message = sMessage

            If culture = "es-CR" Then
                ViewBag.subtitle = "Error: " & sResponse & ", si gusta puede volver a corregir los datos de la tarjeta haciendo click en el vinculo abajo o puede llamar a nuestro departamento de reservas, disculpas por el inconveniente."
            Else
                ViewBag.subtitle = "The reason is: " & sResponse & ", you can review your card information again (do not press Back on the browser) or call our customer service department for assistance, we are sorry for the inconvenience."
            End If
            SendNotification(sResNumber, ErrorMsg)
        End Sub

    End Class




End Namespace