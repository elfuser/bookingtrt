﻿Imports System.Web.Mvc
Imports BRules
Namespace Controllers
    Public Class AddOnsController
        Inherits Controller

        ' GET: AddOns
        Function AddOns(ByVal hotelcode As String, ByVal DateFrom As String, ByVal DateTo As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCodeInAddOns = hotelcode.Trim.ToUpper

                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            End If

            If Not String.IsNullOrEmpty(DateFrom) Then
                ViewBag.DateFromInAddOns = DateFrom.Trim
            End If

            If Not String.IsNullOrEmpty(DateTo) Then
                ViewBag.DateToInAddOns = DateTo.Trim
            End If

            If Utils.UserObject IsNot Nothing Then
                ViewBag.UserName = Utils.UserObject.UserName
                ViewBag.UserName_Email = Utils.UserObject.UserName_Email
            End If

            Return View()
        End Function
    End Class
End Namespace