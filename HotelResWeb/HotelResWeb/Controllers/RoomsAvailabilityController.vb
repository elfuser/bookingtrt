﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Web.Mvc
Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization.Json
Imports BRules

Namespace HotelResWeb
    Public Class RoomsAvailabilityController
        Inherits System.Web.Mvc.Controller

        Private UserID As Integer

        Function RoomsAvailability(ByVal hotelcode As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCode = hotelcode.Trim.ToUpper

                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If

            End If

            If Utils.UserObject IsNot Nothing Then
                ViewBag.UserName = Utils.UserObject.UserName
                ViewBag.UserName_Email = Utils.UserObject.UserName_Email

            End If

            ViewBag.MaxRoomsAllowedToBook = ConfigurationManager.AppSettings("MaxRoomsAllowedToBook")

            Return View()
        End Function


#Region "Public Methods"

        <HttpPost()>
        Public Function SaveReservationRooms(rooms As List(Of RoomLiteModel)) As String

            Dim EncryptJson As String
            Dim ReservationJson As String

            For Each room In rooms
                CalculateRoomPrice(room)
            Next

            ReservationJson = Utils.GetJsonFromObject(rooms)

            EncryptJson = Utils.encrypt(ReservationJson)

            Dim userCookie = New HttpCookie("RoomInfo", EncryptJson)
            userCookie.Expires.AddDays(365)
            HttpContext.Response.Cookies.Add(userCookie)

            Return "1"
        End Function


        <HttpGet()>
        Public Function SaveRoomsPromotionalCode(PromotionalCode As String) As String
            Dim Rates As New Rates
            Dim rooms As New List(Of RoomLiteModel)
            Dim list As New List(Of RatesInfo)
            Dim rateInfo As New RatesInfo
            Dim json As String = ""

            rooms = Utils.DeserializeRooms()

            If PromotionalCode <> "" Then
                For Each room In rooms
                    list = Rates.GetRatesWithPromoCode(room.CompanyId, room.fromDt, room.toDt, room.Adults, room.Kids, PromotionalCode)
                    If (Not list Is Nothing) And (list.Count > 0) Then
                        rateInfo = list(0)
                        room.Price = rateInfo.ppMinValue
                        room.Discount = rateInfo.ppDiscount
                        If rateInfo.ppDiscount > 0 Then
                            room.PriceDisc = room.Price - (room.Price * (rateInfo.ppDiscount / 100))
                        End If

                        room.RateCode = rateInfo.ppRateCode

                    End If
                Next

                SaveReservationRooms(rooms)

                json = GetReservationInfo()

            End If



            Return json

        End Function

        Private Sub CalculateRoomPrice(ByRef room As RoomLiteModel)

            Dim Rates As New Rates
            Dim list As New List(Of RatesInfo)
            Dim rateInfo As New RatesInfo


            list = Rates.GetRates_v1(room.CompanyId, Trim(room.RoomType & ""), room.fromDt, room.toDt, room.Adults, room.Kids)


            If (Not list Is Nothing) And (list.Count > 0) Then
                rateInfo = list(0)
                room.Price = rateInfo.ppMinValue
                room.Discount = rateInfo.ppDiscount
                If rateInfo.ppDiscount > 0 Then
                    room.PriceDisc = room.Price - (room.Price * (rateInfo.ppDiscount / 100))
                    room.RateCode = rateInfo.ppRateCode
                End If
            End If

        End Sub

        '<HttpPost()>
        'Public Function SaveReservationRoom(room As RoomLiteModel) As String


        '    Dim EncryptJson As String
        '    Dim ReservationJson As String
        '    Dim rooms As New List(Of RoomLiteModel)


        '    rooms = DeserializeRooms()
        '    rooms.Add(room)

        '    ReservationJson = Utils.GetJsonFromObject(rooms)

        '    EncryptJson = Utils.encrypt(ReservationJson)

        '    Dim userCookie = New HttpCookie("RoomInfo", EncryptJson)
        '    userCookie.Expires.AddDays(365)
        '    HttpContext.Response.Cookies.Add(userCookie)

        '    Return "1"

        'End Function

        <HttpPost()>
        Public Function SaveReservationAddons(addOns As List(Of AddOnLiteModel)) As String


            Dim EncryptJson As String
            Dim AddOnJson As String


            AddOnJson = Utils.GetJsonFromObject(addOns)

            EncryptJson = Utils.encrypt(AddOnJson)

            Dim userCookie = New HttpCookie("AddonInfo", EncryptJson)
            userCookie.Expires.AddDays(365)
            HttpContext.Response.Cookies.Add(userCookie)

            Return "1"

        End Function

        <HttpPost()>
        Public Function SaveReservationInfo(reservation As ReservationModel) As String

            Dim succcess As Boolean = False

            succcess = Utils.SaveReservationInfo(reservation)

            If succcess Then
                Return "1"
            Else
                Return "0"
            End If

        End Function

        <HttpPost()>
        Public Function SavePromoCodeInfo(obj As clsPromoCodeRateInfo) As String

            Dim succcess As Boolean = False

            succcess = Utils.SavePromoCodeInfo(obj)

            If succcess Then
                Return "1"
            Else
                Return "0"
            End If

        End Function

        <HttpGet()>
        Public Function SaveCurrentPage(CurrentPage As String) As String

            Dim Cookie = New HttpCookie("CurrentProgress", CurrentPage)
            Cookie.Expires.AddDays(365)
            HttpContext.Response.Cookies.Add(Cookie)

            Return "1"

        End Function

        <HttpGet()>
        Public Function GetCurrentPage() As String
            Dim rooms As New List(Of AddOnLiteModel)
            Dim js As String = ""

            Try

                If Not Request.Cookies("CurrentProgress") Is Nothing Then
                    Return Request.Cookies("CurrentProgress").Value
                End If

            Catch ex As Exception

            End Try

            Return ""
        End Function

        <HttpGet()>
        Public Function GetSavedRooms() As String
            Dim rooms As New List(Of RoomLiteModel)
            rooms = Utils.DeserializeRooms()

            Dim Json As String = Utils.GetJsonFromObject(rooms)

            Return Json

        End Function

        <HttpGet()>
        Public Function GetSavedAddons() As String
            Dim addons As New List(Of AddOnLiteModel)
            addons = Utils.DeserializeAddons()

            Dim Json As String = Utils.GetJsonFromObject(addons)

            Return Json
        End Function


        <HttpGet()>
        Public Function GetReservationInfo() As String

            Dim reserv As New ReservationModel
            reserv = Utils.DeserializeReservationInfo()

            Dim Json As String = Utils.GetJsonFromObject(reserv)

            Return Json

        End Function

        <HttpGet()>
        Public Function GetPromoCodeInfo() As String

            Dim obj As New clsPromoCodeRateInfo
            obj = Utils.DeserializePromoCodeInfo()

            Dim Json As String = Utils.GetJsonFromObject(obj)

            Return Json

        End Function

#End Region

#Region "Login"
        <AllowAnonymous>
        <HttpPost()>
        Function DoLogin(UserName As String, Password As String) As String

            Dim Json As String = ""
            Dim objReturn As New ResponseUser
            'Dim ReturnUrl As String = TempData("ReturnUrl") & ""
            'Dim LoginReturn As New LoginResponseModel

            objReturn = ValidateUser(UserName, Password)

            'LoginReturn.Status = objReturn.Status
            'LoginReturn.Message = objReturn.Message

            If objReturn.Status = "0" Then

                'FormsAuthentication.SetAuthCookie(UserName, False)

                'If (Url.IsLocalUrl(ReturnUrl)) Then
                '    LoginReturn.ReturnUrl = ReturnUrl
                'End If

            End If


            Json = Utils.GetJsonFromObject(objReturn)
            Return Json

        End Function

        <HttpPost()>
        Function SignOut() As String

            Dim Json As String = ""
            Dim objReturn As New ResponseUser

            FormsAuthentication.SignOut()

            Utils.UserObject = Nothing
            objReturn.Status = "0"

            Json = Utils.GetJsonFromObject(objReturn)
            Return Json

        End Function

        Private Function ValidateUser(userName As String, Password As String) As ResponseUser

            Dim user As New BRules.Users
            Dim sResult As String = ""
            Dim dtPwdVoid As String
            Dim strResult() As String
            Dim pwd As String
            Dim SuperUser As Boolean
            Dim Active As Boolean
            Dim ProfileID As Integer
            Dim ResturnUserName As String
            Dim IsAgencyUser As Boolean = False
            Dim AgencyCode As Integer = 0
            Dim mPwdTimes As Integer
            Dim objReturn As New BRules.ResponseUser
            Dim Permissions As New List(Of ProfileInfo)
            'Dim UserID As String

            Try

                'Password = user.encriptaclave(Password)

                mPwdTimes = CInt(TempData("PwdTryCount"))

                sResult = user.LoginUser(userName)

                If sResult = String.Empty Then
                    objReturn.Message = "Not valid credentials"
                    objReturn.Status = "-1"
                    Return objReturn
                End If


                strResult = Split(sResult, "~")
                UserID = CInt(strResult(0))
                ResturnUserName = strResult(1)
                ProfileID = CInt(strResult(2))
                Active = CBool(strResult(3))
                SuperUser = CBool(strResult(4))
                pwd = strResult(5)
                dtPwdVoid = strResult(6)
                IsAgencyUser = IIf(strResult(7) = "1", True, False)
                AgencyCode = strResult(8)

                If Active = False Then
                    SaveUserLog("Login user: blocked", UserID)
                    objReturn.Message = "Your user account is Blocked, please contact support"
                    objReturn.Status = "-1"
                    Return objReturn
                End If


                'Utils.UserID = UserID
                objReturn.UserName = ResturnUserName
                objReturn.UserName_Email = userName
                objReturn.UserId = UserID
                objReturn.AgencyCode = AgencyCode
                Utils.UserObject = objReturn

                'wfallas delete this line-------------
                'Password = pwd
                Password = Password.ToLower()
                '-------------------------------------

                If Password <> pwd Then
                    If mPwdTimes = 1 Then
                        user.BlockUser(UserID, False)
                        SaveUserLog("User has been blocked", UserID)
                        objReturn.Message = "Your account has been blocked, please contact support"
                        objReturn.Status = "-1"
                        Return objReturn
                    Else
                        If mPwdTimes > 0 Then
                            mPwdTimes -= 1
                        End If

                        TempData("PwdTryCount") = mPwdTimes
                        SaveUserLog("Login user: incorrect password entered - Booking", UserID)
                        'objReturn.Message = "The password is invalid, please try again (you have " & mPwdTimes.ToString & " time(s) left before the account is blocked)"
                        objReturn.Message = "The password is invalid, please try again"
                        objReturn.Status = "-1"
                        Return objReturn
                    End If
                End If


                If dtPwdVoid = "1" Then
                    objReturn.Message = "You have to change your password"
                    objReturn.Status = "1"
                    Return objReturn
                End If


                SaveUserLog("User logged in - Booking", UserID)

                'Permissions = user.GetProfilePermissions(0, ProfileID)

            Catch ex As Exception
                objReturn.Message = "Login process failed"
                objReturn.Status = "-1"
                Return objReturn
            End Try

            objReturn.Message = "Login OK"
            objReturn.Status = "0"
            Return objReturn
        End Function


        <AllowAnonymous>
        <HttpPost()>
        Function ChangePassword(UserName As String, Password As String, NewPassword As String) As String
            Dim user As New BRules.Users
            Dim objReturn As New BRules.ResponseUser
            Dim Json As String
            Dim objUserResponse As New ResponseUser

            Try

                objUserResponse = ValidateUser(UserName, Password)

                If objUserResponse.Status = "-1" Then
                    objReturn.Message = "Not valid credentials"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                If Not ValidatePwdPattern(NewPassword) Then
                    objReturn.Message = "Password is not valid: minimum 8 characters, at least 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character (Use @#$%^&+= only). Example: Temp@2014"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                If Not ValidLast5Passwords(UserID, NewPassword) Then
                    objReturn.Message = "The new password has been used in the last 5 changes, please change again"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                'NewPassword = user.encriptaclave(NewPassword)

                user.SaveLastPassword(UserID, Now, NewPassword)

            Catch ex As Exception
                objReturn.Message = "change password process failed"
                objReturn.Status = "-1"
                Json = Utils.GetJsonFromObject(objReturn)
                Return Json
            End Try


            objReturn.Message = "Login OK"
            objReturn.Status = "0"
            Json = Utils.GetJsonFromObject(objReturn)
            Return Json

        End Function

        Private Sub SaveUserLog(ByVal sEvent As String, ByVal iRefID As Long)
            Dim cslUsers As New BRules.Users
            Dim UserId As Integer = 0
            Try
                If Utils.UserObject IsNot Nothing Then
                    UserId = Utils.UserObject.UserId
                End If
                cslUsers.SaveUserLog(UserId, iRefID, sEvent)
            Catch ex As Exception

            End Try
        End Sub

        Private Function ValidLast5Passwords(userid As Integer, NewPassword As String) As Boolean
            Dim user As New BRules.Users
            Dim sResult As String = ""
            Dim strResult() As String

            sResult = user.Last5Passwords(userid)

            strResult = Split(sResult, "~")

            For Each pwd In strResult
                If pwd = NewPassword Then
                    Return False
                End If
            Next

            Return True
        End Function

        Private Function ValidatePwdPattern(NewPassword As String) As Boolean
            Dim bReturn As Boolean = True
            Dim pattern As String = "^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"
            If Not Regex.IsMatch(NewPassword, pattern) Then
                bReturn = False
            End If
            Return bReturn
        End Function

#End Region

    End Class


End Namespace