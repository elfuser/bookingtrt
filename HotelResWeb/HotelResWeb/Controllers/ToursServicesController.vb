﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers
    Public Class ToursServicesController
        Inherits Controller

        ' GET: ToursServices
        Function ToursServices(ByVal hotelcode As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCode = hotelcode.Trim.ToUpper
                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            End If

            Return View()
        End Function

        <HttpPost()>
        Public Function SaveTourServiceInfo(ByVal data As TourServiceModel) As String

            Dim result As Boolean = False

            data.SessionId = Guid.NewGuid().ToString
            result = Utils.SaveTourServiceInfo(data)

            Return result

        End Function

        <HttpPost()>
        Public Function SavePromoCodeInfoService(obj As List(Of clsPromoCodeRateInfo)) As String

            Dim succcess As Boolean = False

            succcess = Utils.SavePromoCodeInfoService(obj)

            If succcess Then
                Return "1"
            Else
                Return "0"
            End If

        End Function

        <HttpGet()>
        Public Function GetPromoCodeInfoService() As String

            Dim obj As New List(Of clsPromoCodeRateInfo)
            obj = Utils.DeserializePromoCodeServiceInfo()

            Dim Json As String = Utils.GetJsonFromObject(obj)

            Return Json

        End Function

    End Class
End Namespace