﻿Imports System.Web.Mvc
Imports BRules
Imports System.IO
Imports VPOS20_PLUGIN
Imports System.Net.Mail
Imports System.Net

Namespace Controllers
    Public Class CheckoutController
        Inherits Controller


        Private Processor As String = ""



        ' GET: Checkout
        Function Checkout(ByVal hotelcode As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCodeInCheckout = hotelcode.Trim.ToUpper
                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            End If

            If Utils.UserObject IsNot Nothing Then
                ViewBag.UserName = Utils.UserObject.UserName
                ViewBag.UserName_Email = Utils.UserObject.UserName_Email
                ViewBag.UserAgencyCode = Utils.UserObject.AgencyCode
            End If

            Return View()
        End Function

        <HttpPost()>
        Public Function SaveReservationInfo(ReservationModel As ReservationModel) As String
            Dim result As String = ""
            Dim objResInfo As New clsReservationsInfo
            Dim company As New BRules.Companies()
            Dim CompanyList As New List(Of CompanyInfo)
            Dim RoomList As List(Of RoomLiteModel)
            Dim AddonList As List(Of AddOnLiteModel)
            Dim resp As New Response
            Dim sProcessor As String = ""
            Dim CompanyProcessor As String
            Dim ReservationSession As New ReservationSession()
            Dim Json As String = ""

            Try

                Call Utils.SaveReservationInfo(ReservationModel)
                RoomList = Utils.DeserializeRooms()
                AddonList = Utils.DeserializeAddons()

                CompanyList = company.GetCompany(ReservationModel.CompanyID, "SEL", "H")

                CompanyProcessor = CompanyList(0).Processor

                'uncomment for demo
                CompanyProcessor = "credomatic"

                If (Not ReservationModel.ReservationID Is Nothing AndAlso ReservationModel.ReservationID <> "") _
                    And (Not ReservationModel.SessionId Is Nothing AndAlso (ReservationModel.SessionId <> "")) Then
                    ReservationModel = SaveReservation(ReservationModel, RoomList, AddonList, CompanyProcessor, False)
                Else
                    ReservationModel = SaveReservation(ReservationModel, RoomList, AddonList, CompanyProcessor, True)
                End If



                Utils.SaveReservationInfo(ReservationModel)

            Catch ex As Exception
                ReservationModel.ReservationID = -1
                resp.Status = "-1"
                resp.Message = "Internal Error"
                Json = ""
                Return Json
            End Try


            Json = Utils.GetJsonFromObject(ReservationModel)


            Return Json
        End Function


        Private Function SaveReservation(ByRef ReservationModel As ReservationModel, RoomList As List(Of RoomLiteModel), AddonList As List(Of AddOnLiteModel), CompanyProcessor As String, Optional isInsert As Boolean = True) As ReservationModel

            Dim objGuestInfo As New clsGuestsInfo
            Dim objResInfo As New clsReservationsInfo
            Dim rooms As New List(Of clsReservationsRoomInfo)
            Dim addons As New List(Of AddonInfo)
            Dim addonInfo As New AddonInfo
            Dim clsReserRoomInfo As New clsReservationsRoomInfo()
            Dim FromDate As DateTime
            Dim ToDate As DateTime
            Dim rateCode As String = ""
            Dim ReservationSession As New ReservationSession()
            Dim Reservations As New Reservations()
            Dim resp As New Response
            Dim RoomCount As Integer = 0
            Dim sProcessor As String = ""

            Try

                Select Case CompanyProcessor.Trim.ToLower
                    Case "credomatic", "credomaticpaypal"
                        sProcessor = "CR"
                    Case "bn", "bnpaypal"
                        sProcessor = "BN"
                    Case "all", "credomaticbn"
                        If ReservationModel.CardType = "AMEX" Or ReservationModel.CardType = "DINN" Or ReservationModel.CardType = "DISC" Then
                            'Enviar a procesamiento con Credomatic
                            sProcessor = "CR"
                        Else
                            sProcessor = "BN"
                        End If
                    Case "noprocessor"
                        sProcessor = "NA"
                End Select

                If ReservationModel Is Nothing Then
                    Throw New Exception("Reservation not found")
                End If

                'Save the Guest and get the ID
                With objGuestInfo
                    .ppGuestID = 0
                    .ppName = ReservationModel.FName.Trim
                    .ppLastname = ReservationModel.LName.Trim
                    .ppTelephone = ReservationModel.Phone.Trim
                    .ppEmail = ReservationModel.EMail.Trim
                    .ppCountry_Iso3 = ReservationModel.Country.Trim
                    .ppCreationDate = Now
                    .ppCompany_Code = ReservationModel.CompanyID
                End With

                For Each room In RoomList
                    With clsReserRoomInfo
                        .ppAdults = room.Adults
                        .ppChildren = room.Kids
                        .ppChildrenFree = 0
                        .ppDate = Now
                        .ppDescription = room.Desc
                        .ppRateAmount = room.Price
                        .ppReservationID = 0
                        .ppRoomTypeCode = room.RoomType
                        .ppRoomTypeID = room.Code
                        .ppTaxAmount = room.TaxPerc
                        FromDate = room.fromDt
                        ToDate = room.toDt
                        rateCode = room.RateCode
                    End With
                    rooms.Add(clsReserRoomInfo)

                    RoomCount = RoomCount + 1

                    Select Case RoomCount
                        Case 1
                            objResInfo.Room1Qty = clsReserRoomInfo.ppAdults & "|" & clsReserRoomInfo.ppChildren & "|" & clsReserRoomInfo.ppChildrenFree
                        Case 2
                            objResInfo.Room2Qty = clsReserRoomInfo.ppAdults & "|" & clsReserRoomInfo.ppChildren & "|" & clsReserRoomInfo.ppChildrenFree
                        Case 3
                            objResInfo.Room3Qty = clsReserRoomInfo.ppAdults & "|" & clsReserRoomInfo.ppChildren & "|" & clsReserRoomInfo.ppChildrenFree
                        Case 4
                            objResInfo.Room4Qty = clsReserRoomInfo.ppAdults & "|" & clsReserRoomInfo.ppChildren & "|" & clsReserRoomInfo.ppChildrenFree
                    End Select

                Next

                With objResInfo
                    .ppArrival = FromDate
                    .ppCancellation_Date = CDate("01-01-1990")
                    .ppCcard_TransID = 1
                    .ppReservationID = ""
                    .ppCompany_Code = ReservationModel.CompanyID
                    .ppCreation_Date = Now
                    .ppDeparture = ToDate
                    .ppPromoID = ReservationModel.PromoID
                    .ppRate_Code = rateCode
                    .Processor = sProcessor
                    If IsNumeric(ReservationModel.total) Then
                        .ppRateAmount = CDbl(ReservationModel.total)
                    End If

                    .ppRemarks = ""

                    If IsNumeric(ReservationModel.TaxAmount) Then
                        .ppTaxAmount = ReservationModel.TaxAmount
                    End If

                    If IsNumeric(ReservationModel.AddonAmount) Then
                        .ppAddonAmount = ReservationModel.AddonAmount
                    End If

                    If IsNumeric(ReservationModel.PromoAmount) Then
                        .ppPromoAmount = ReservationModel.PromoAmount
                    End If

                    If IsNumeric(ReservationModel.Discount) Then
                        .ppDiscount = ReservationModel.Discount
                    End If

                    .ppVisit_Reason = ""
                    .ppRooms = rooms.Count
                    .ppOrigin = 1
                    .ppResellerID = ""
                    .ppSessionID = ""

                    .CardType = ReservationModel.CardType

                End With

                For Each addon In AddonList
                    With addonInfo
                        .ppQty = 1
                        .ppAmount = addon.Price
                        .ppAddonID = addon.Code
                    End With
                    addons.Add(addonInfo)
                Next


                ReservationSession.AddonList = addons
                ReservationSession.GuestsInfo = objGuestInfo
                ReservationSession.ReservationsInfo = objResInfo
                ReservationSession.RoomList = rooms


                If isInsert Then
                    resp = Reservations.SaveReservationSession(ReservationSession)
                Else
                    ReservationSession.ReservationsInfo.ppSessionID = ReservationModel.SessionId
                    ReservationSession.ReservationsInfo.ppReservationID = ReservationModel.ReservationID
                    resp = Reservations.UpdateReservationSession(ReservationSession)
                End If


                If resp.Status = "0" Then
                    ReservationModel.SessionId = ReservationSession.ReservationsInfo.ppSessionID
                    ReservationModel.ReservationID = ReservationSession.ReservationsInfo.ppReservationID
                    ReservationModel.Processor = CompanyProcessor
                    Return ReservationModel
                End If

            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing

        End Function


        Private Function SavedCardInformation(ByRef ReservationSession As ReservationSession, CompanyProcessor As String) As Boolean

            Try

                With ReservationSession

                    Select Case CompanyProcessor
                        Case "Credomatic", "CredomaticPayPal"
                            'ProcessCard()
                        Case "BN", "BNPayPal"
                            ReservationSession.GoPage = "summarybn.aspx"
                        Case "All", "CredomaticBN"
                            If .ReservationsInfo.CardType = "AMEX" Or .ReservationsInfo.CardType = "DINN" Or .ReservationsInfo.CardType = "DISC" Then
                                'Enviar a procesamiento con Credomatic
                                'ProcessCard()
                            Else
                                'Enviar a procesamiento con el BN
                                ReservationSession.GoPage = "summarybn.aspx?sessionID"
                            End If
                        Case "NoProcessor"
                            'SaveCardInfo()
                            'UpdateAvailability(ViewState("confirmationID"))
                            'UpdateStatus(ViewState("confirmationID"), 2)
                            'ReservationSession.GoPage = "confirmation.aspx"
                            ReservationSession.GoPage = "error.aspx"
                        Case Else
                            ReservationSession.GoPage = "error.aspx"
                    End Select

                End With

            Catch ex As Exception
                Throw ex
            End Try

            Return True
        End Function


        <HttpPost()>
        Public Function GenerateBNParams(ReservationModel As ReservationModel) As String
            Dim objCom As New Companies
            Dim mKeys() As String
            Dim BNReq As New BNRequestModel
            Dim Json As String = ""
            Dim amount As String

            Try
                Dim mReturnKeys As String = objCom.GetBankBNKeys(ReservationModel.CompanyID)
                If mReturnKeys.Trim.Length > 0 Then
                    mKeys = mReturnKeys.Split("|")
                End If

                BNReq.mIDACQUIRER = mKeys(0)
                BNReq.mIDCOMMERCE = mKeys(1)
                'Session("KeyProc") = mKeys(1)
                Dim oVPOSBean As New VPOSBean


                Dim basePath As String = ConfigurationManager.AppSettings("keys")

                Dim R1 As String = basePath + "\" & ReservationModel.CompanyID & "\ALIGNET.PRODUCCION.NOPHP.CRYPTO.PUBLIC.txt"
                Dim R2 As String = basePath + "\" & ReservationModel.CompanyID & "\Llave.Privada.Firma.txt"

                Dim srVPOSLlaveCifradoPublica As New StreamReader(R1)
                Dim srComercioLlaveFirmaPrivada As New StreamReader(R2)


                amount = ReservationModel.total.ToString.Replace(",", "").Replace(".", "")


                With oVPOSBean
                    .acquirerId = BNReq.mIDACQUIRER
                    .commerceId = BNReq.mIDCOMMERCE
                    .purchaseCurrencyCode = "840"
                    .terminalCode = "00000000"
                    .purchaseAmount = amount
                    .purchaseOperationNumber = Trim(ReservationModel.ReservationID)
                    .commerceMallId = "1"
                End With

                Dim oVPOSSend As New VPOSSend(srVPOSLlaveCifradoPublica, srComercioLlaveFirmaPrivada, "9FFC9DE41FB95C4A")
                oVPOSSend.execute(oVPOSBean)

                BNReq.mSESSIONKEY = oVPOSBean.cipheredSessionKey
                BNReq.mXMLREQ = oVPOSBean.cipheredXML
                BNReq.mDIGITALSIGN = oVPOSBean.cipheredSignature

                Json = Utils.GetJsonFromObject(BNReq)

            Catch ex As Exception
                EventLog.WriteEntry("Application", "GenerateParamBN: " & ex.ToString, EventLogEntryType.Error)
                Json = ""
            End Try

            Return Json

        End Function


        <HttpPost()>
        Public Function ProcessPay(ReservationModel As ReservationModel) As String
            Dim objCompany As New Companies
            Dim Reservation As New Reservations()
            Dim mKeys() As String
            Dim mReturnKeys As String
            Dim mAmount As String
            Dim mType As String
            Dim mUsername As String
            Dim mKeyId As String
            Dim mProcessorID As String
            Dim mHash As String
            Dim mTime As String
            Dim mOrderId As String
            Dim mCredomaticProcID As String
            Dim sParams As New StringBuilder("")
            Dim ts As TimeSpan = Date.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
            Dim Secs As Long = Convert.ToInt64(ts.TotalSeconds)
            Dim errorMessage As String = ""
            Dim response As New Response
            Dim CredomaticReqModel As New CredomaticRequestModel
            Dim json As String
            Dim ExpeYear As String = ""

            Try
                'mReturnKeys = objCompany.GetBankCredomaticKeys(ReservationModel.CompanyID)

                'uncomment for demo
                mReturnKeys = objCompany.GetBankCredomaticKeys("DEMO2000")

                If mReturnKeys.Trim.Length > 0 Then
                    mKeys = mReturnKeys.Split("|")
                End If
                mKeyId = mKeys(0)
                Dim Key As String = mKeys(1)
                mAmount = Replace(ReservationModel.total.ToString(), ",", "")
                mOrderId = Trim(ReservationModel.ReservationID)
                mType = mKeys(2)
                mUsername = mKeys(3)
                mCredomaticProcID = mKeys(6)



                Select Case ReservationModel.CompanyID
                    Case "VS0712"
                        If ReservationModel.CardType = "AMEX" Then
                            mProcessorID = "INET0783"
                        Else
                            mProcessorID = "INET0782"
                        End If
                    Case "ES0512"
                        If ReservationModel.CardType = "AMEX" Then
                            mProcessorID = "helsilencioamex"
                        Else
                            mProcessorID = "helsilenciovm"
                        End If
                    Case "PI1212"
                        If ReservationModel.CardType = "AMEX" Then
                            mProcessorID = "puntaislitaamex"
                        Else
                            mProcessorID = "puntaislitavm"
                        End If
                    Case "VB0712"
                        If ReservationModel.CardType = "AMEX" Then
                            mProcessorID = "villablancaamex"
                        Else
                            mProcessorID = "villablancavm"
                        End If
                    Case Else
                        mProcessorID = mKeys(4)
                End Select


                If (Not ReservationModel.ExpeYear Is Nothing) Then
                    If Len(ReservationModel.ExpeYear) > 2 Then
                        ExpeYear = Mid(ReservationModel.ExpeYear, 3, 2)
                    End If
                End If

                mTime = Secs.ToString

                '-- uncomment for demo
                Key = "v6c4Zz52wDG5bSgFqrS6DKV42Yry2Uhc"
                mKeyId = "6430339"
                mAmount = "100"

                mHash = Utils.GetMD5(mOrderId + "|" + mAmount + "|" + mTime + "|" + Key)


                If mCredomaticProcID.Trim = "1" Then 'Si el procesor es OLS Credomatic


                    sParams.Append("orderid=" & mOrderId)
                    sParams.Append("&processor_id=" & mProcessorID)
                    sParams.Append("&type=" & mType)
                    sParams.Append("&key_id=" & mKeyId)
                    sParams.Append("&username=" & mUsername)
                    sParams.Append("&ccnumber=" & Replace(ReservationModel.CardNumber, "-", ""))
                    sParams.Append("ccexp", ReservationModel.ExpeMonth & ExpeYear)
                    sParams.Append("&amount=" & mAmount)
                    sParams.Append("&time=" & mTime)
                    sParams.Append("&HASh=" & mHash)


                    'Procesar tarjeta
                    Dim mPaymentResult As String = SendPayment(sParams.ToString(), mKeys(5))
                    If mPaymentResult = "" Then
                        'Error 
                        errorMessage = "The connection To the bank Is unavailable, wait a minute And Try again please"
                    Else
                        SaveCardInfo(ReservationModel)
                        errorMessage = SaveTransResult(mPaymentResult, ReservationModel)
                    End If
                Else
                    SaveCardInfo(ReservationModel)


                    ' adding values in gash table for data post
                    'CredomaticReqModel.hash = mHash
                    'CredomaticReqModel.username = mUsername
                    'CredomaticReqModel.redirect = "http://localhost/HotelResWeb/Confirmation/Confirmation"
                    'CredomaticReqModel.orderid = mOrderId
                    'CredomaticReqModel.amount = mAmount
                    'CredomaticReqModel.processor_id = mProcessorID
                    'CredomaticReqModel.type = mType
                    'CredomaticReqModel.key_id = mKeyId
                    'CredomaticReqModel.time = mTime
                    'CredomaticReqModel.ccnumber = Replace(ReservationModel.CardNumber, "-", "")
                    'CredomaticReqModel.ccexp = ReservationModel.ExpeMonth & ExpeYear
                    'CredomaticReqModel.action = mKeys(5)
                    'Dim strForm As String = PreparePOSTForm(, data)

                    '-- uncomment for demo                
                    CredomaticReqModel.hash = mHash
                    CredomaticReqModel.username = "booknowadmin01"
                    CredomaticReqModel.redirect = Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Confirmation/Confirmation"
                    CredomaticReqModel.orderid = mOrderId
                    CredomaticReqModel.amount = mAmount
                    CredomaticReqModel.processor_id = "booknowadmin01"
                    CredomaticReqModel.type = mType
                    CredomaticReqModel.key_id = "6430339"
                    CredomaticReqModel.time = mTime
                    CredomaticReqModel.ccnumber = Replace(ReservationModel.CardNumber, "-", "")
                    CredomaticReqModel.ccexp = ReservationModel.ExpeMonth & ExpeYear

                    '-- uncomment for demo
                    'CredomaticReqModel.ccexp = "1115"

                    CredomaticReqModel.action = mKeys(5)
                    '-- 

                End If


                json = Utils.GetJsonFromObject(CredomaticReqModel)

                Return json
            Catch ex As Exception
                Return String.Empty
            End Try


        End Function


        Private Function PreparePOSTForm(url As String, data As System.Collections.Hashtable) As String
            'Set a name for the form
            Dim formID As String = "PostForm"
            'Build the form using the specified data to be posted.
            Dim strForm As New StringBuilder()
            strForm.Append("<form id=""" + formID + """ name=""" + formID + """ action=""" + url + """ method=""POST"">")

            For Each key As System.Collections.DictionaryEntry In data

                strForm.Append("<input type=""hidden"" name=""" + key.Key + """ value=""" + key.Value + """>")
            Next

            strForm.Append("</form>")
            'Build the JavaScript which will do the Posting operation.
            Dim strScript As New StringBuilder()
            strScript.Append("<script language='javascript'>")
                strScript.Append("var v" + formID + " = document." + formID + ";")
            strScript.Append("v" + formID + ".submit();")
            strScript.Append("</script>")
            'Return the form and the script concatenated.
            '(The order is important, Form then JavaScript)
            Return strForm.ToString() + strScript.ToString()
        End Function
        Private Sub SaveCardInfo(ReservationModel As ReservationModel)
            If ReservationModel.CardNumber <> String.Empty Then
                Dim objRes As New Reservations
                Dim objCardInfo As New clsCCardInfo

                With objCardInfo
                    .ppCardNumber = Utils.encrypt(Replace(ReservationModel.CardNumber, "-", ""))
                    .ppReservationID = ReservationModel.ReservationID
                    .ppCardExp = ReservationModel.ExpeMonth & ReservationModel.ExpeYear
                    .ppCardCode = ReservationModel.CardType
                    .ppAuthNr = ReservationModel.SecureCode
                    .ppApprobCode = ""
                    .ppNameCard = ReservationModel.NameCard
                End With

                objRes.SaveCardInformation("INS", objCardInfo)

            End If
        End Sub

        Private Function SendPayment(ByVal Params As String, ByVal Url As String) As String
            Dim uri As New Uri(Url)
            Dim mReturn As String
            If uri.Scheme = Uri.UriSchemeHttps Then
                Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(uri)
                request.Method = System.Net.WebRequestMethods.Http.Post
                request.ContentLength = Params.Length
                request.ContentType = "application/x-www-form-urlencoded"
                Try
                    Dim writer As New System.IO.StreamWriter(request.GetRequestStream())
                    'Open SSL Connection
                    writer.Write(Params)
                    'Send data to open connection
                    writer.Close()
                    Dim oResponse As System.Net.HttpWebResponse = request.GetResponse()
                    'Listen for response
                    Dim reader As New System.IO.StreamReader(oResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                    'Receive response
                    Dim Reply As String = reader.ReadToEnd()

                    Reply = Replace(Reply, "?", "")

                    Dim TempArray As Object = Strings.Split(Reply, "&")
                    Dim TempParam As Object
                    Dim i As Integer = 0
                    'response=3&responsetext=Processor does not allow an expired expiration date REFID:343161447&authcode=&transactionid=&avsresponse=&cvvresponse=&orderid=OS96&type=auth&response_code=300
                    While i < TempArray.length
                        TempParam = Strings.Split(TempArray(i), "=")
                        Select Case TempParam(0)
                            Case "response"
                                mReturn = TempParam(1)
                            Case "responsetext"
                                mReturn &= "&" & TempParam(1)
                            Case "authcode"
                                mReturn &= "&" & TempParam(1)
                            Case "transactionid"
                                mReturn &= "&" & TempParam(1)
                            Case "orderid"
                                mReturn &= "&" & TempParam(1)
                            Case "response_code"
                                mReturn &= "&" & TempParam(1)
                        End Select
                        i += 1
                    End While

                    oResponse.Close()
                Catch Ex As Exception
                    EventLog.WriteEntry("Application", "SendPayment: " & Ex.ToString, EventLogEntryType.Error)
                    mReturn = ""
                End Try
            Else
                mReturn = ""
            End If
            Return mReturn
        End Function


        Private Function SaveTransResult(ByVal sParams As String, ReservationModel As ReservationModel) As String
            Dim mParams() As String = sParams.Split("&")
            Dim ErrorMessage As String = ""

            SaveBankTransaction(ReservationModel.ReservationID, mParams(0), mParams(5), mParams(2), "", mParams(1), mParams(3), ReservationModel.total)

            Select Case mParams(0)
                Case "1" 'Approved
                    UpdateAvailability(ReservationModel.ReservationID)
                    UpdateStatus(ReservationModel.ReservationID, 2)
                    ErrorMessage = ""
                Case "2" 'Rejected
                    UpdateStatus(ReservationModel.ReservationID, 3)
                    ErrorMessage = "Your Card has been DENIED, you can try with another card or call your bank for assistance"
                    SendNotification(ReservationModel, mParams(1))
                Case "3" 'Card Error
                    UpdateStatus(ReservationModel.ReservationID, 4)
                    ErrorMessage = "Your Card has reported an error, you can try with another card or call your bank for assistance"
                    SendNotification(ReservationModel, mParams(1))
                Case Else
                    UpdateStatus(ReservationModel.ReservationID, 4)
                    ErrorMessage = "Your Card has reported an error, you can try with another card or call your bank for assistance"
                    SendNotification(ReservationModel, mParams(1))
            End Select

            Return ErrorMessage

        End Function


        Private Sub UpdateAvailability(ReservationNumber As String)
            Dim objRes As New Reservations

            Try
                objRes.SaveAvailability(ReservationNumber, 1)
            Catch ex As Exception
                EventLog.WriteEntry("Application", "UpdateAvailability: " & ex.ToString, EventLogEntryType.Error)

            End Try
        End Sub

        Private Sub UpdateStatus(ReservationNumber As String, Status As Int16)
            Dim objRes As New Reservations

            Try
                objRes.UpdateStatus(ReservationNumber, Status, 1)
            Catch ex As Exception
                EventLog.WriteEntry("Application", "UpdateStatus: " & ex.ToString, EventLogEntryType.Error)
            End Try
        End Sub

        Private Sub SaveBankTransaction(ByVal ReservationID As String, ByVal Response As String, ByVal ResponseCode As String,
                                ByVal AuthCode As String, ByVal ErrorCode As String, ByVal ResponseText As String,
                                ByVal TransactionID As String, ByVal Amount As Double)

            Dim clsCred As New Reservations

            Try
                clsCred.SaveBankLog(ReservationID, Response, AuthCode, ResponseCode, ErrorCode, ResponseText, TransactionID, Amount)

            Catch ex As Exception
                EventLog.WriteEntry("Application", "SaveBankTransaction: " & ex.ToString, EventLogEntryType.Error)
            End Try
        End Sub


#Region " Email "
        Private Sub SendNotification(ReservationModel As ReservationModel, ByVal Mess As String)
            Dim objComp As New Companies
            Dim mEmails() As String

            mEmails = Split(objComp.GetCompanyEmails(ReservationModel.CompanyID), "|")

            Dim strFrom As String = "info@bookingtrtinteractive.com"

            Dim strTo As String = mEmails(0)
            Dim mBody As StringBuilder = New StringBuilder("")
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            'MailMsg.Bcc.Add("vcorralesf@econsultingcr.com")
            'MailMsg.Bcc.Add("aaron.rodriguez@econsultingcr.com")

            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = "Transaccion Rechazada"

            Try
                mBody.Append("<h2>Transacci&oacute;n Rechazada</h2>")
                mBody.Append("<p>El cliente ha intentado pagar la reserva # " & ReservationModel.ReservationID & " y ha sido rechazada por el banco")
                mBody.Append("<p>Motivo: " & Mess)
                mBody.Append("<p><b>Datos del cliente</b> ")
                mBody.Append("<p>Nombre: " & ReservationModel.FName & " " & ReservationModel.LName)
                mBody.Append("<p>Tel: " & ReservationModel.Phone)
                mBody.Append("<p>Email: " & ReservationModel.EMail)

                MailMsg.Body = mBody.ToString
                MailMsg.Priority = MailPriority.Normal
                MailMsg.IsBodyHtml = True

                Dim SmtpMail As New SmtpClient
                SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                SmtpMail.Port = 8889
                SmtpMail.Send(MailMsg)

            Catch ex As Exception
                EventLog.WriteEntry("Application", "SendNotification: " & ex.ToString, EventLogEntryType.Error)

            End Try
        End Sub




#End Region

#Region "Agency"
        '<Authorize>
        <HttpGet()>
        Public Function GetAgencyInfo(ByVal AgencyCode As Integer, ByVal CompanyCode As String) As String
            Dim cls As New BRules.Agencies
            Dim objList As New List(Of AgencyInfo)
            Dim Json As String = ""

            Try

                objList = cls.GetAgencies(AgencyCode, CompanyCode)

                Json = Utils.GetJsonFromObject(objList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function
#End Region

    End Class
End Namespace