﻿Imports System.Web.Mvc
Imports BRules
Namespace Controllers
    Public Class ServiceDetailController
        Inherits Controller

        ' GET: ToursServices
        Function ServiceDetail(ByVal hotelcode As String, ByVal serviceid As String) As ActionResult
            If Not String.IsNullOrEmpty(hotelcode) Then
                ViewBag.HotelCode = hotelcode.Trim.ToUpper
                Dim company As New List(Of CompanyInfo)
                Dim companyBLL As New BRules.Companies
                company = companyBLL.GetCompanyGoogleInfo(hotelcode.Trim.ToUpper)
                Dim googleid As String
                ViewBag.GoogleID = ""
                If Not (company Is Nothing) Then
                    googleid = company(0).ppGoogleConvID
                    ViewBag.GoogleID = googleid
                End If
            End If

            If Not String.IsNullOrEmpty(serviceid) Then
                ViewBag.ServiceId = serviceid.Trim
            End If

            Return View()
        End Function

        <HttpGet()>
        Public Function GetTourServiceCookieInfo() As String
            Dim obj As New TourServiceModel
            obj = Utils.DeserializeTourService()

            Dim Json As String = Utils.GetJsonFromObject(obj)

            Return Json

        End Function

        <HttpPost()>
        Public Function SaveServices(ByVal services As List(Of ServiceModelLite)) As String

            Dim EncryptJson As String
            Dim objJson As String

            objJson = Utils.GetJsonFromObject(services)

            EncryptJson = Utils.encrypt(objJson)

            Dim userCookie = New HttpCookie("ServicesInfo", EncryptJson)
            userCookie.Expires.AddDays(365)
            HttpContext.Response.Cookies.Add(userCookie)

            Return "1"

        End Function

        <HttpGet()>
        Public Function GetSavedServices() As String
            Dim list As New List(Of ServiceModelLite)
            list = Utils.DeserializeServices()

            Dim Json As String = Utils.GetJsonFromObject(list)

            Return Json

        End Function
    End Class
End Namespace