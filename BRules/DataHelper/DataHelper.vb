﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class DataHelper
    ''' <summary>
    ''' Load sql database connection
    ''' </summary>
    ''' <returns>SqlConnection</returns>
    Public Function OpenConnection(Optional conName As [String] = "") As SqlConnection
        Try
            Dim conString As [String]

            If conName = "" Then
                conString = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
            Else
                conString = ConfigurationManager.ConnectionStrings(conName).ConnectionString
            End If


            Dim con As New SqlConnection(conString)
            con.Open()

            Return con
        Catch Ex As Exception
            Throw New Exception("connection is fail.")
        End Try
    End Function


    ''' <summary>
    ''' Execute SQL using SqlCommand against database
    ''' </summary>
    ''' <param name="cmd">SqlCommand will be used to execute SQL against database</param>
    Public Sub ExecuteCommand(cmd As SqlCommand)

        cmd.ExecuteNonQuery()

    End Sub
    ''' <summary>
    ''' Execute SQL using SqlCommand against database and fetch records in SqlDataReader
    ''' </summary>
    ''' <param name="sb">StringBuilder will be used to build a SQL</param>
    ''' <param name="con">SqlConnection will be used to connect to database.</param>
    ''' <returns>SqlDataReader</returns>
    Public Function ExecuteReader(sb As StringBuilder, con As SqlConnection) As SqlDataReader
        Dim cmd As New SqlCommand()
        cmd.CommandText = sb.ToString()
        cmd.Connection = con

        Dim reader As SqlDataReader = cmd.ExecuteReader()

        Return reader
    End Function


    Public Function ExecuteNonQuery(sb As StringBuilder, con As SqlConnection) As Integer
        Dim cmd As New SqlCommand()
        cmd.CommandText = sb.ToString()
        cmd.Connection = con

        Dim count As Integer = cmd.ExecuteNonQuery()

        Return count
    End Function

    'Public Function ExecuteNonQuery(cmd As SqlCommand, con As SqlConnection) As Integer

    '    cmd.Connection = con

    '    Dim count As Integer = cmd.ExecuteNonQuery()

    '    Return count
    'End Function


    Public Function ExecuteNonQuery(cmd As SqlCommand, con As SqlConnection) As Boolean

        cmd.Connection = con

        cmd.ExecuteNonQuery()

        Return True
    End Function

    Public Function ExecuteReader(cmd As SqlCommand, con As SqlConnection) As SqlDataReader
        cmd.Connection = con

        Dim reader As SqlDataReader = cmd.ExecuteReader()

        Return reader
    End Function

    ''' <summary>
    ''' Execute SQL using SqlCommand against database and fetch single column value.
    ''' </summary>
    ''' <param name="sb">SQL text</param>
    ''' <param name="con">database connection</param>
    ''' <returns>Single column value</returns>
    Public Function ExecuteScalar(sb As StringBuilder, con As SqlConnection) As [Object]
        Dim cmd As New SqlCommand()

        cmd.CommandText = sb.ToString()
        cmd.Connection = con

        Dim returnScaler As Object = cmd.ExecuteScalar()

        Return returnScaler
    End Function



    Public Function ExecuteScalar(cmd As SqlCommand, con As SqlConnection) As Object

        cmd.Connection = con

        Dim count As Object = cmd.ExecuteScalar()

        Return count
    End Function

End Class
