﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class VirtualReceiptInfo

#Region "Variables privadas"

    Private _receiptCode As Integer
    Private _companyCode As String
    Private _clientName As String
    Private _country As String
    Private _phone As String
    Private _email As String
    Private _description As String
    Private _amount As Double
    Private _receiptPaid As Boolean
    Private _expiry As Date
    Private _created As Date
    Private _FormatExpiry As String
    Private _FormatCreated As String
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ReceiptCode As Integer
        Get
            Return _receiptCode
        End Get
        Set(ByVal value As Integer)
            _receiptCode = value
        End Set
    End Property

    <DataMember()>
    Public Property CompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ClientName As String
        Get
            Return _clientName
        End Get
        Set(ByVal value As String)
            _clientName = value
        End Set
    End Property

    <DataMember()>
    Public Property Country As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property Phone As String
        Get
            Return _phone
        End Get
        Set(ByVal value As String)
            _phone = value
        End Set
    End Property

    <DataMember()>
    Public Property Email As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property Description As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property Amount As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ReceiptPaid As Boolean
        Get
            Return _receiptPaid
        End Get
        Set(ByVal value As Boolean)
            _receiptPaid = value
        End Set
    End Property

    <DataMember()>
    Public Property Expiry As Date
        Get
            Return _expiry
        End Get
        Set(ByVal value As Date)
            _expiry = value
        End Set
    End Property

    <DataMember()>
    Public Property Created As Date
        Get
            Return _created
        End Get
        Set(ByVal value As Date)
            _created = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatExpiry As String
        Get
            Return _FormatExpiry
        End Get
        Set(ByVal value As String)
            _FormatExpiry = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatCreated As String
        Get
            Return _FormatCreated
        End Get
        Set(ByVal value As String)
            _FormatCreated = value
        End Set
    End Property

#End Region

End Class
