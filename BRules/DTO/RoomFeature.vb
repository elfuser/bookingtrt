﻿


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class RoomFeature

#Region "Variables privadas"
    Private _RoomTypeID As Integer
    Private _FeatureID As Integer

#End Region

#Region "Propiedades"


    <DataMember()>
    Public Property RoomTypeID As Integer
        Get
            Return _RoomTypeID
        End Get
        Set(ByVal value As Integer)
            _RoomTypeID = value
        End Set
    End Property



    <DataMember()>
    Public Property FeatureID As Integer
        Get
            Return _FeatureID
        End Get
        Set(ByVal value As Integer)
            _FeatureID = value
        End Set
    End Property

#End Region


End Class
