﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class clsQryReservationConfirmation
    Dim _reservation, _status, _hotelName, _Processor, _rateName As String
    Dim _name, _lastname, _rate As String
    Dim _companyCode, _statusText As String
    Dim _startDate, _endDate, _creationDate, _void As Date
    Dim _amount, _total, _taxAmount, _addonAmount, _promoAmount, _commOL, _commBank As Double
    Dim _email, _creditCard, _expDate, _secCode, _approveCode, _cardType, _depositNumber, _transactionBank As String
    Dim _remarks, _visitReason, _telephone, _country, _reference As String
    Dim _nights As Integer
    Dim _check As Boolean

    <DataMember()>
    Public Property check As Boolean
        Get
            Return _check
        End Get
        Set(ByVal value As Boolean)
            _check = value
        End Set
    End Property

    <DataMember()>
    Public Property reference As String
        Get
            Return _reference
        End Get
        Set(ByVal value As String)
            _reference = value
        End Set
    End Property

    <DataMember()>
    Public Property rateName As String
        Get
            Return _rateName
        End Get
        Set(ByVal value As String)
            _rateName = value
        End Set
    End Property

    <DataMember()>
    Public Property processor As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

    <DataMember()>
    Public Property statusText As String
        Get
            Return _statusText
        End Get
        Set(ByVal value As String)
            _statusText = value
        End Set
    End Property

    <DataMember()>
    Public Property telephone As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property country As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property hotelName As String
        Get
            Return _hotelName
        End Get
        Set(ByVal value As String)
            _hotelName = value
        End Set
    End Property

    <DataMember()>
    Public Property transactionBank As String
        Get
            Return _transactionBank
        End Get
        Set(ByVal value As String)
            _transactionBank = value
        End Set
    End Property

    <DataMember()>
    Public Property depositNumber As String
        Get
            Return _depositNumber
        End Get
        Set(ByVal value As String)
            _depositNumber = value
        End Set
    End Property

    <DataMember()>
    Public Property status As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    <DataMember()>
    Public Property nights As String
        Get
            Return _nights
        End Get
        Set(ByVal value As String)
            _nights = value
        End Set
    End Property

    <DataMember()>
    Public Property visitReason As String
        Get
            Return _visitReason
        End Get
        Set(ByVal value As String)
            _visitReason = value
        End Set
    End Property

    <DataMember()>
    Public Property remarks As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property
    <DataMember()>
    Public Property reservation As String
        Get
            Return _reservation
        End Get
        Set(ByVal value As String)
            _reservation = value
        End Set
    End Property

    <DataMember()>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property lastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property arrival() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property departure() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property created() As Date
        Get
            Return _creationDate
        End Get
        Set(ByVal value As Date)
            _creationDate = value
        End Set
    End Property

    <DataMember()>
    Public Property void() As Date
        Get
            Return _void
        End Get
        Set(ByVal value As Date)
            _void = value
        End Set
    End Property

    <DataMember()>
    Public Property rate() As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property taxAmount() As Double
        Get
            Return _taxAmount
        End Get
        Set(ByVal value As Double)
            _taxAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property addonAmount() As Double
        Get
            Return _addonAmount
        End Get
        Set(ByVal value As Double)
            _addonAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property promoAmount() As Double
        Get
            Return _promoAmount
        End Get
        Set(ByVal value As Double)
            _promoAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property commOL() As Double
        Get
            Return _commOL
        End Get
        Set(ByVal value As Double)
            _commOL = value
        End Set
    End Property

    <DataMember()>
    Public Property commBank() As Double
        Get
            Return _commBank
        End Get
        Set(ByVal value As Double)
            _commBank = value
        End Set
    End Property

    <DataMember()>
    Public Property total() As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property

    <DataMember()>
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property creditCard() As String
        Get
            Return _creditCard
        End Get
        Set(ByVal value As String)
            _creditCard = value
        End Set
    End Property

    <DataMember()>
    Public Property expDate() As String
        Get
            Return _expDate
        End Get
        Set(ByVal value As String)
            _expDate = value
        End Set
    End Property

    <DataMember()>
    Public Property secCode() As String
        Get
            Return _secCode
        End Get
        Set(ByVal value As String)
            _secCode = value
        End Set
    End Property

    <DataMember()>
    Public Property approveCode() As String
        Get
            Return _approveCode
        End Get
        Set(ByVal value As String)
            _approveCode = value
        End Set
    End Property

    <DataMember()>
    Public Property cardType() As String
        Get
            Return _cardType
        End Get
        Set(ByVal value As String)
            _cardType = value
        End Set
    End Property
End Class
