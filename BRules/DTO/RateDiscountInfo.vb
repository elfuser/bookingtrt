﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class RateDiscountInfo

#Region "Variables privadas"

    Private _rowID, _type As Integer
    Private _start_date As Date
    Private _end_date As Date
    Private _disc1 As Double
    Private _disc2 As Double
    Private _disc3 As Double
    Private _disc4 As Double
    Private _disc5 As Double
    Private _disc6 As Double
    Private _disc7 As Double
    Private _disc8 As Double
    Private _discount As Double
    Private _error As String
    Private _rate_Code As String
    Private _companyCode As String
    Private _typeDesc As String
    Private _dateFrom, _dateTo As Date

    Private _FormatFrom As String
    Private _FormatTo As String


    Dim _UnFormatFrom As Date
    Dim _UnFormatTo As Date

#End Region

#Region "Propiedades"


    <DataMember()>
    Public Property UnFormatFrom As Date
        Get
            Return _UnFormatFrom
        End Get
        Set(ByVal value As Date)
            _UnFormatFrom = value
        End Set
    End Property


    <DataMember()>
    Public Property UnFormatTo As Date
        Get
            Return _UnFormatTo
        End Get
        Set(ByVal value As Date)
            _UnFormatTo = value
        End Set
    End Property


    <DataMember()>
    Public Property FormatFrom As String
        Get
            Return _FormatFrom
        End Get
        Set(ByVal value As String)
            _FormatFrom = value
        End Set
    End Property



    <DataMember()>
    Public Property FormatTo As String
        Get
            Return _FormatTo
        End Get
        Set(ByVal value As String)
            _FormatTo = value
        End Set
    End Property


    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTypeDesc As String
        Get
            Return _typeDesc
        End Get
        Set(ByVal value As String)
            _typeDesc = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowID As Integer
        Get
            Return _rowID
        End Get
        Set(ByVal value As Integer)
            _rowID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType As Integer
        Get
            Return _type
        End Get
        Set(ByVal value As Integer)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate As Date
        Get
            Return _start_date
        End Get
        Set(ByVal value As Date)
            _start_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate As Date
        Get
            Return _end_date
        End Get
        Set(ByVal value As Date)
            _end_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateCode As String
        Get
            Return _rate_Code
        End Get
        Set(ByVal value As String)
            _rate_Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc1 As Double
        Get
            Return _disc1
        End Get
        Set(ByVal value As Double)
            _disc1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc2 As Double
        Get
            Return _disc2
        End Get
        Set(ByVal value As Double)
            _disc2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc3 As Double
        Get
            Return _disc3
        End Get
        Set(ByVal value As Double)
            _disc3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc4 As Double
        Get
            Return _disc4
        End Get
        Set(ByVal value As Double)
            _disc4 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc5 As Double
        Get
            Return _disc5
        End Get
        Set(ByVal value As Double)
            _disc5 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc6 As Double
        Get
            Return _disc6
        End Get
        Set(ByVal value As Double)
            _disc6 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc7 As Double
        Get
            Return _disc7
        End Get
        Set(ByVal value As Double)
            _disc7 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisc8 As Double
        Get
            Return _disc8
        End Get
        Set(ByVal value As Double)
            _disc8 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

#End Region

End Class

