﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class CultureInfo
    Private _cultureID As Integer
    Private _cultureName As String
    Private _displayName As String
    Private _error As String

    <DataMember()>
    Public Property ppCultureID As Integer
        Get
            Return _cultureID
        End Get
        Set(ByVal value As Integer)
            _cultureID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCultureName As String
        Get
            Return _cultureName
        End Get
        Set(ByVal value As String)
            _cultureName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDisplayName As String
        Get
            Return _displayName
        End Get
        Set(ByVal value As String)
            _displayName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
End Class
