﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class QryReservation
#Region "Variables privadas"
    Dim _rateCode As String
    Dim _name, _lastname, _rate As String
    Dim _companyCode As String
    Dim _startDate, _endDate As Date
    Dim _amount, _tax As Double
    Dim _email, _country As String
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property reservation As String
        Get
            Return _rateCode
        End Get
        Set(ByVal value As String)
            _rateCode = value
        End Set
    End Property

    <DataMember()>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property lastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property arrival() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property departure() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property rate() As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property tax() As Double
        Get
            Return _tax
        End Get
        Set(ByVal value As Double)
            _tax = value
        End Set
    End Property

    <DataMember()>
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property
#End Region
End Class

<DataContract()>
Public Class QryAddon

#Region "Variables privadas"
    Dim _addon As String
    Dim _qty As Integer
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppAddon() As String
        Get
            Return _addon
        End Get
        Set(ByVal value As String)
            _addon = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property

#End Region
End Class

<DataContract()>
Public Class QryReservationConfirmation
#Region "Variables privadas"


    Dim _reservation, _status, _hotelName, _Processor, _rateName As String
    Dim _name, _lastname, _rate As String
    Dim _companyCode, _statusText As String
    Dim _startDate, _endDate As String
    Dim _creationDate, _void As String
    Dim _amount, _total, _taxAmount, _addonAmount, _promoAmount, _commOL, _commBank As Double
    Dim _email, _creditCard, _expDate, _secCode, _approveCode, _cardType, _depositNumber, _transactionBank As String
    Dim _remarks, _visitReason, _telephone, _country, _reference As String
    Dim _nights As Integer
    Dim _check As Boolean
#End Region

#Region "Propiedades"


    <DataMember()>
    Public Property check As Boolean
        Get
            Return _check
        End Get
        Set(ByVal value As Boolean)
            _check = value
        End Set
    End Property

    <DataMember()>
    Public Property reference As String
        Get
            Return _reference
        End Get
        Set(ByVal value As String)
            _reference = value
        End Set
    End Property

    <DataMember()>
    Public Property rateName As String
        Get
            Return _rateName
        End Get
        Set(ByVal value As String)
            _rateName = value
        End Set
    End Property

    <DataMember()>
    Public Property processor As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

    <DataMember()>
    Public Property statusText As String
        Get
            Return _statusText
        End Get
        Set(ByVal value As String)
            _statusText = value
        End Set
    End Property

    <DataMember()>
    Public Property telephone As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property country As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property hotelName As String
        Get
            Return _hotelName
        End Get
        Set(ByVal value As String)
            _hotelName = value
        End Set
    End Property

    <DataMember()>
    Public Property transactionBank As String
        Get
            Return _transactionBank
        End Get
        Set(ByVal value As String)
            _transactionBank = value
        End Set
    End Property

    <DataMember()>
    Public Property depositNumber As String
        Get
            Return _depositNumber
        End Get
        Set(ByVal value As String)
            _depositNumber = value
        End Set
    End Property

    <DataMember()>
    Public Property status As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    <DataMember()>
    Public Property nights As String
        Get
            Return _nights
        End Get
        Set(ByVal value As String)
            _nights = value
        End Set
    End Property

    <DataMember()>
    Public Property visitReason As String
        Get
            Return _visitReason
        End Get
        Set(ByVal value As String)
            _visitReason = value
        End Set
    End Property

    <DataMember()>
    Public Property remarks As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property
    <DataMember()>
    Public Property reservation As String
        Get
            Return _reservation
        End Get
        Set(ByVal value As String)
            _reservation = value
        End Set
    End Property

    <DataMember()>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property lastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property arrival() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property departure() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property created() As String
        Get
            Return _creationDate
        End Get
        Set(ByVal value As String)
            _creationDate = value
        End Set
    End Property

    <DataMember()>
    Public Property void() As String
        Get
            Return _void
        End Get
        Set(ByVal value As String)
            _void = value
        End Set
    End Property

    <DataMember()>
    Public Property rate() As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property taxAmount() As Double
        Get
            Return _taxAmount
        End Get
        Set(ByVal value As Double)
            _taxAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property addonAmount() As Double
        Get
            Return _addonAmount
        End Get
        Set(ByVal value As Double)
            _addonAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property promoAmount() As Double
        Get
            Return _promoAmount
        End Get
        Set(ByVal value As Double)
            _promoAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property commOL() As Double
        Get
            Return _commOL
        End Get
        Set(ByVal value As Double)
            _commOL = value
        End Set
    End Property

    <DataMember()>
    Public Property commBank() As Double
        Get
            Return _commBank
        End Get
        Set(ByVal value As Double)
            _commBank = value
        End Set
    End Property

    <DataMember()>
    Public Property total() As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property

    <DataMember()>
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property creditCard() As String
        Get
            Return _creditCard
        End Get
        Set(ByVal value As String)
            _creditCard = value
        End Set
    End Property

    <DataMember()>
    Public Property expDate() As String
        Get
            Return _expDate
        End Get
        Set(ByVal value As String)
            _expDate = value
        End Set
    End Property

    <DataMember()>
    Public Property secCode() As String
        Get
            Return _secCode
        End Get
        Set(ByVal value As String)
            _secCode = value
        End Set
    End Property

    <DataMember()>
    Public Property approveCode() As String
        Get
            Return _approveCode
        End Get
        Set(ByVal value As String)
            _approveCode = value
        End Set
    End Property

    <DataMember()>
    Public Property cardType() As String
        Get
            Return _cardType
        End Get
        Set(ByVal value As String)
            _cardType = value
        End Set
    End Property
#End Region

End Class

<DataContract()>
Public Class QryRoom

#Region "Variables privadas"

    Dim _code As String
    Dim _room As String
    Dim _adults, _children, _childrenFree As Integer
    Dim _amount As Double
    Dim _date As String
#End Region

#Region "Propiedades"


    <DataMember()>
    Public Property ppCode() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom() As String
        Get
            Return _room
        End Get
        Set(ByVal value As String)
            _room = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults() As Integer
        Get
            Return _adults
        End Get
        Set(ByVal value As Integer)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren() As Integer
        Get
            Return _children
        End Get
        Set(ByVal value As Integer)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildrenFree() As Integer
        Get
            Return _childrenFree
        End Get
        Set(ByVal value As Integer)
            _childrenFree = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDate() As String
        Get
            Return _date
        End Get
        Set(ByVal value As String)
            _date = value
        End Set
    End Property

#End Region
End Class
