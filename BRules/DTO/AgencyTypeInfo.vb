﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class AgencyTypeInfo

#Region "Variables privadas"

    Private _agency_type_code As Int16
    Private _type_name As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppAgencyTypeCode() As Int16
        Get
            Return _agency_type_code
        End Get
        Set(ByVal value As Int16)
            _agency_type_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTypeName() As String
        Get
            Return _type_name
        End Get
        Set(ByVal value As String)
            _type_name = value
        End Set
    End Property

#End Region

End Class
