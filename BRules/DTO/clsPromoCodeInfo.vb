﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsPromoCodeInfo

#Region "Variables privadas"
    Private _promoID, _AgencyCode, _ServiceId As Integer
    Private _promoCode, _description, _type, _companyCode, _error As String
    Private _startDate, _endDate As Date
    Private _FormatstartDate, _FormatendDate As String
    Private _active As Boolean
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoCode As String
        Get
            Return _promoCode
        End Get
        Set(ByVal value As String)
            _promoCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID As Integer
        Get
            Return _promoID
        End Get
        Set(ByVal value As Integer)
            _promoID = value
        End Set
    End Property


    <DataMember()>
    Public Property UnFormatTo As String
        Get
            Return _FormatendDate
        End Get
        Set(ByVal value As String)
            _FormatendDate = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatFrom As String
        Get
            Return _FormatstartDate
        End Get
        Set(ByVal value As String)
            _FormatstartDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAgencyCode As Integer
        Get
            Return _AgencyCode
        End Get
        Set(ByVal value As Integer)
            _AgencyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceId As Integer
        Get
            Return _ServiceId
        End Get
        Set(ByVal value As Integer)
            _ServiceId = value
        End Set
    End Property
#End Region

End Class



