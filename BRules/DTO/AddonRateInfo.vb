﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class AddonRateInfo

#Region "Variables privadas"
    Private _rateID, _addonID, _maxAdults, _maxChildren, _maxInfants, _maxStudents As Integer
    Private _occupancyMax, _occupangyMin As Integer
    Private _description, _discountType, _error, _type As String
    Private _startDate, _endDate As Date
    Private _adults, _children, _infants, _discount, _students, _price As Double
    Private _active As Boolean
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppOccupancyMax As Integer
        Get
            Return _occupancyMax
        End Get
        Set(ByVal value As Integer)
            _occupancyMax = value
        End Set
    End Property

    <DataMember()>
    Public Property ppOccupancyMin As Integer
        Get
            Return _occupangyMin
        End Get
        Set(ByVal value As Integer)
            _occupangyMin = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPrice As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudents As Double
        Get
            Return _students
        End Get
        Set(ByVal value As Double)
            _students = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Double
        Get
            Return _infants
        End Get
        Set(ByVal value As Double)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscountType As String
        Get
            Return _discountType
        End Get
        Set(ByVal value As String)
            _discountType = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxStudents As Integer
        Get
            Return _maxStudents
        End Get
        Set(ByVal value As Integer)
            _maxStudents = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxInfants As Integer
        Get
            Return _maxInfants
        End Get
        Set(ByVal value As Integer)
            _maxInfants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxChildren As Integer
        Get
            Return _maxChildren
        End Get
        Set(ByVal value As Integer)
            _maxChildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxAdults As Integer
        Get
            Return _maxAdults
        End Get
        Set(ByVal value As Integer)
            _maxAdults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddonID As Integer
        Get
            Return _addonID
        End Get
        Set(ByVal value As Integer)
            _addonID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateID As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property
#End Region

End Class

