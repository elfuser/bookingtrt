﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class PromotionalCodeExceptionDTO

#Region "Variables privadas"
    Private _promoExc_id As Integer
    Private _promo_id As Integer
    Private _end_date As Date
    Private _UnFormatDate As String
#End Region


#Region "Propiedades"

    <DataMember()>
    Public Property PromoExc_id As Integer
        Get
            Return _promoExc_id
        End Get
        Set(ByVal value As Integer)
            _promoExc_id = value
        End Set
    End Property

    <DataMember()>
    Public Property Promo_id As Integer
        Get
            Return _promo_id
        End Get
        Set(ByVal value As Integer)
            _promo_id = value
        End Set
    End Property

    <DataMember()>
    Public Property End_date As Date
        Get
            Return _end_date
        End Get
        Set(ByVal value As Date)
            _end_date = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatDate As String
        Get
            Return _UnFormatDate
        End Get
        Set(ByVal value As String)
            _UnFormatDate = value
        End Set
    End Property



#End Region



End Class
