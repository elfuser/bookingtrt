﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class QrySale
#Region "Variables privadas"
    Dim _reservation, _status, _companyName, _Processor, _rateName As String
    Dim _name, _lastname, _rate As String
    Dim _companyCode, _statusText As String
    Dim _arrivalDate, _creationDate, _void As Date
    Dim _amount, _total, _taxAmount, _promoAmount, _commOL, _commBank As Double
    Dim _email, _creditCard, _expDate, _approveCode, _cardType, _depositNumber, _transactionBank As String
    Dim _remarks, _telephone, _country, _reference As String
    Dim _services As Integer
    Dim _check As Boolean
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property check As Boolean
        Get
            Return _check
        End Get
        Set(ByVal value As Boolean)
            _check = value
        End Set
    End Property

    <DataMember()>
    Public Property reference As String
        Get
            Return _reference
        End Get
        Set(ByVal value As String)
            _reference = value
        End Set
    End Property

    <DataMember()>
    Public Property rateName As String
        Get
            Return _rateName
        End Get
        Set(ByVal value As String)
            _rateName = value
        End Set
    End Property

    <DataMember()>
    Public Property processor As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

    <DataMember()>
    Public Property statusText As String
        Get
            Return _statusText
        End Get
        Set(ByVal value As String)
            _statusText = value
        End Set
    End Property

    <DataMember()>
    Public Property telephone As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property country As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property companyName As String
        Get
            Return _companyName
        End Get
        Set(ByVal value As String)
            _companyName = value
        End Set
    End Property

    <DataMember()>
    Public Property transactionBank As String
        Get
            Return _transactionBank
        End Get
        Set(ByVal value As String)
            _transactionBank = value
        End Set
    End Property

    <DataMember()>
    Public Property depositNumber As String
        Get
            Return _depositNumber
        End Get
        Set(ByVal value As String)
            _depositNumber = value
        End Set
    End Property

    <DataMember()>
    Public Property status As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    <DataMember()>
    Public Property services As Integer
        Get
            Return _services
        End Get
        Set(ByVal value As Integer)
            _services = value
        End Set
    End Property

    <DataMember()>
    Public Property remarks As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property

    <DataMember()>
    Public Property reservation As String
        Get
            Return _reservation
        End Get
        Set(ByVal value As String)
            _reservation = value
        End Set
    End Property

    <DataMember()>
    Public Property name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property lastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property arrival() As Date
        Get
            Return _arrivalDate
        End Get
        Set(ByVal value As Date)
            _arrivalDate = value
        End Set
    End Property

    <DataMember()>
    Public Property created() As Date
        Get
            Return _creationDate
        End Get
        Set(ByVal value As Date)
            _creationDate = value
        End Set
    End Property

    <DataMember()>
    Public Property void() As Date
        Get
            Return _void
        End Get
        Set(ByVal value As Date)
            _void = value
        End Set
    End Property

    <DataMember()>
    Public Property rate() As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property taxAmount() As Double
        Get
            Return _taxAmount
        End Get
        Set(ByVal value As Double)
            _taxAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property promoAmount() As Double
        Get
            Return _promoAmount
        End Get
        Set(ByVal value As Double)
            _promoAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property commOL() As Double
        Get
            Return _commOL
        End Get
        Set(ByVal value As Double)
            _commOL = value
        End Set
    End Property

    <DataMember()>
    Public Property commBank() As Double
        Get
            Return _commBank
        End Get
        Set(ByVal value As Double)
            _commBank = value
        End Set
    End Property

    <DataMember()>
    Public Property total() As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property

    <DataMember()>
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property creditCard() As String
        Get
            Return _creditCard
        End Get
        Set(ByVal value As String)
            _creditCard = value
        End Set
    End Property

    <DataMember()>
    Public Property expDate() As String
        Get
            Return _expDate
        End Get
        Set(ByVal value As String)
            _expDate = value
        End Set
    End Property

    <DataMember()>
    Public Property approveCode() As String
        Get
            Return _approveCode
        End Get
        Set(ByVal value As String)
            _approveCode = value
        End Set
    End Property

    <DataMember()>
    Public Property cardType() As String
        Get
            Return _cardType
        End Get
        Set(ByVal value As String)
            _cardType = value
        End Set
    End Property
#End Region
End Class
