﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class AgencyInfo

#Region "Variables privadas"

    Private _agencyCode As Integer
    Private _companyCode As String
    Private _agencyName As String
    Private _agencyTypeCode As Int16
    Private _country As String
    Private _province As String
    Private _canton As String
    Private _district As String
    Private _address As String
    Private _phone As String
    Private _contactName As String
    Private _contactOccupation As String
    Private _contactEmail As String
    Private _contactOfficePhone As String
    Private _contactMobilePhone As String
    Private _notificationEmailReservations As String
    Private _active As Boolean
    Private _availableCredit As Boolean
    Private _created As Date
    Private _modified As Date
    Private _FormatCreated As String

    Private _NumTarjeta As String
    Private _FecVencimiento As String
    Private _CodSeguridad As String
    Private _TipoTarjeta As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppAgencyCode As Integer
        Get
            Return _agencyCode
        End Get
        Set(ByVal value As Integer)
            _agencyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAgencyName As String
        Get
            Return _agencyName
        End Get
        Set(ByVal value As String)
            _agencyName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAgencyTypeCode As Int16
        Get
            Return _agencyTypeCode
        End Get
        Set(ByVal value As Int16)
            _agencyTypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountry As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property ppProvince As String
        Get
            Return _province
        End Get
        Set(ByVal value As String)
            _province = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCanton As String
        Get
            Return _canton
        End Get
        Set(ByVal value As String)
            _canton = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDistrict As String
        Get
            Return _district
        End Get
        Set(ByVal value As String)
            _district = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddress As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPhone As String
        Get
            Return _phone
        End Get
        Set(ByVal value As String)
            _phone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContactName As String
        Get
            Return _contactName
        End Get
        Set(ByVal value As String)
            _contactName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContactOccupation As String
        Get
            Return _contactOccupation
        End Get
        Set(ByVal value As String)
            _contactOccupation = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContactEmail As String
        Get
            Return _contactEmail
        End Get
        Set(ByVal value As String)
            _contactEmail = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContactOfficePhone As String
        Get
            Return _contactOfficePhone
        End Get
        Set(ByVal value As String)
            _contactOfficePhone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContactMobilePhone As String
        Get
            Return _contactMobilePhone
        End Get
        Set(ByVal value As String)
            _contactMobilePhone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppNotificationEmailReservations As String
        Get
            Return _notificationEmailReservations
        End Get
        Set(ByVal value As String)
            _notificationEmailReservations = value
        End Set
    End Property

    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAvailableCredit As Boolean
        Get
            Return _availableCredit
        End Get
        Set(ByVal value As Boolean)
            _availableCredit = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreated As Date
        Get
            Return _created
        End Get
        Set(ByVal value As Date)
            _created = value
        End Set
    End Property

    <DataMember()>
    Public Property ppModified As Date
        Get
            Return _modified
        End Get
        Set(ByVal value As Date)
            _modified = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatCreated As String
        Get
            Return _FormatCreated
        End Get
        Set(ByVal value As String)
            _FormatCreated = value
        End Set
    End Property


    <DataMember()>
    Public Property NumTarjeta As String
        Get
            Return _NumTarjeta
        End Get
        Set(ByVal value As String)
            _NumTarjeta = value
        End Set
    End Property

    <DataMember()>
    Public Property FecVencimiento As String
        Get
            Return _FecVencimiento
        End Get
        Set(ByVal value As String)
            _FecVencimiento = value
        End Set
    End Property


    <DataMember()>
    Public Property CodSeguridad As String
        Get
            Return _CodSeguridad
        End Get
        Set(ByVal value As String)
            _CodSeguridad = value
        End Set
    End Property

    <DataMember()>
    Public Property TipoTarjeta As String
        Get
            Return _TipoTarjeta
        End Get
        Set(ByVal value As String)
            _TipoTarjeta = value
        End Set
    End Property


#End Region

End Class
