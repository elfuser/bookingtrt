﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class ServiceRateInfo
#Region "Variables privadas"
    Private _rateID, _serviceID, _maxAdults, _maxChildren, _maxInfants, _maxStudents As Integer
    Private _description, _discountType, _error As String
    Private _startDate, _endDate As Date
    Private _adults, _children, _infants, _discount, _students As Double
    Private _active As Boolean
    Private _UnFormatFrom As String
    Private _UnFormatTo As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudents As Double
        Get
            Return _students
        End Get
        Set(ByVal value As Double)
            _students = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Double
        Get
            Return _infants
        End Get
        Set(ByVal value As Double)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscountType As String
        Get
            Return _discountType
        End Get
        Set(ByVal value As String)
            _discountType = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxStudents As Integer
        Get
            Return _maxStudents
        End Get
        Set(ByVal value As Integer)
            _maxStudents = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxInfants As Integer
        Get
            Return _maxInfants
        End Get
        Set(ByVal value As Integer)
            _maxInfants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxChildren As Integer
        Get
            Return _maxChildren
        End Get
        Set(ByVal value As Integer)
            _maxChildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxAdults As Integer
        Get
            Return _maxAdults
        End Get
        Set(ByVal value As Integer)
            _maxAdults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateID As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatFrom As String
        Get
            Return _UnFormatFrom
        End Get
        Set(ByVal value As String)
            _UnFormatFrom = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatTo As String
        Get
            Return _UnFormatTo
        End Get
        Set(ByVal value As String)
            _UnFormatTo = value
        End Set
    End Property

#End Region
End Class
