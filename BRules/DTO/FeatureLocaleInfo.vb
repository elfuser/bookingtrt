﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel



Public Class FeatureLocaleInfo

#Region "Variables privadas"

    Private _featureID As Long
    Private _cultureID As Integer
    Private _description As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppFeatureID As Long
        Get
            Return _featureID
        End Get
        Set(ByVal value As Long)
            _featureID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCultureID As Integer
        Get
            Return _cultureID
        End Get
        Set(ByVal value As Integer)
            _cultureID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

#End Region

End Class
