﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsFeaturesInfo
    Private _feature_id As Integer
    Private _culture_id As Integer
    Private _feature_code As String
    Private _description As String
    Private _company_code As String
    Private _error As String

    <DataMember()>
    Public Property ppFeatureID As Integer
        Get
            Return _feature_id
        End Get
        Set(ByVal value As Integer)
            _feature_id = value
        End Set
    End Property


    <DataMember()>
    Public Property CultureId As Integer
        Get
            Return _culture_id
        End Get
        Set(ByVal value As Integer)
            _culture_id = value
        End Set
    End Property



    <DataMember()>
    Public Property ppFeatureCode As String
        Get
            Return _feature_code
        End Get
        Set(ByVal value As String)
            _feature_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
End Class
