﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class ProfileInfo
#Region "Variables privadas"
    Private _ProfileID As Integer
    Private _ProfineName, _ControlTag, _Error As String
    Private _Allow As Boolean

    Private _Dasboard As Boolean
    Private _Company As Boolean
    Private _Rates As Boolean
    Private _Rooms As Boolean
    Private _Addons As Boolean
    Private _Services As Boolean

    Private _Reservations As Boolean
    Private _Avaibility As Boolean
    Private _Comissions As Boolean

    Private _ReportsReserv As Boolean
    Private _ReportSales As Boolean
    Private _ReportGuest As Boolean
    Private _Profiles As Boolean
    Private _EventLog As Boolean

#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppProfileID As Integer
        Get
            Return _ProfileID
        End Get
        Set(ByVal value As Integer)
            _ProfileID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppProfileName As String
        Get
            Return _ProfineName
        End Get
        Set(ByVal value As String)
            _ProfineName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAllow As Boolean
        Get
            Return _Allow
        End Get
        Set(ByVal value As Boolean)
            _Allow = value
        End Set
    End Property

    <DataMember()>
    Public Property ppControlTag As String
        Get
            Return _ControlTag
        End Get
        Set(ByVal value As String)
            _ControlTag = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _Error
        End Get
        Set(ByVal value As String)
            _Error = value
        End Set
    End Property


    <DataMember()>
    Public Property Dashboard As Boolean
        Get
            Return _Dasboard
        End Get
        Set(ByVal value As Boolean)
            _Dasboard = value
        End Set
    End Property
    <DataMember()>
    Public Property Company As Boolean
        Get
            Return _Company
        End Get
        Set(ByVal value As Boolean)
            _Company = value
        End Set
    End Property
    <DataMember()>
    Public Property Rates As Boolean
        Get
            Return _Rates
        End Get
        Set(ByVal value As Boolean)
            _Rates = value
        End Set
    End Property
    <DataMember()>
    Public Property Rooms As Boolean
        Get
            Return _Rooms
        End Get
        Set(ByVal value As Boolean)
            _Rooms = value
        End Set
    End Property
    <DataMember()>
    Public Property Addons As Boolean
        Get
            Return _Addons
        End Get
        Set(ByVal value As Boolean)
            _Addons = value
        End Set
    End Property
    <DataMember()>
    Public Property Services As Boolean
        Get
            Return _Services
        End Get
        Set(ByVal value As Boolean)
            _Services = value
        End Set
    End Property
    <DataMember()>
    Public Property Reservations As Boolean
        Get
            Return _Reservations
        End Get
        Set(ByVal value As Boolean)
            _Reservations = value
        End Set
    End Property
    <DataMember()>
    Public Property Avaibility As Boolean
        Get
            Return _Avaibility
        End Get
        Set(ByVal value As Boolean)
            _Avaibility = value
        End Set
    End Property
    <DataMember()>
    Public Property Comissions As Boolean
        Get
            Return _Comissions
        End Get
        Set(ByVal value As Boolean)
            _Comissions = value
        End Set
    End Property
    <DataMember()>
    Public Property ReportsReserv As Boolean
        Get
            Return _ReportsReserv
        End Get
        Set(ByVal value As Boolean)
            _ReportsReserv = value
        End Set
    End Property
    <DataMember()>
    Public Property ReportSales As Boolean
        Get
            Return _ReportSales
        End Get
        Set(ByVal value As Boolean)
            _ReportSales = value
        End Set
    End Property
    <DataMember()>
    Public Property ReportGuest As Boolean
        Get
            Return _ReportGuest
        End Get
        Set(ByVal value As Boolean)
            _ReportGuest = value
        End Set
    End Property
    <DataMember()>
    Public Property Profiles As Boolean
        Get
            Return _Profiles
        End Get
        Set(ByVal value As Boolean)
            _Profiles = value
        End Set
    End Property
    <DataMember()>
    Public Property EventLog As Boolean
        Get
            Return _EventLog
        End Get
        Set(ByVal value As Boolean)
            _EventLog = value
        End Set
    End Property
#End Region
End Class
