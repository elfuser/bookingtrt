﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class DayAvailability
#Region "Variables privadas"
    Dim _day As Date
    Dim _qty As Integer
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppDay() As Date
        Get
            Return _day
        End Get
        Set(ByVal value As Date)
            _day = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
#End Region
End Class
