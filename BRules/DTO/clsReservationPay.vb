﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class clsReservationPay
    Private _name, _lastName, _email, _telephone, _countryName As String
    Private _guestID As Integer
    Private _rate_amount, _addon_amount As Double
    Private _arrival, _departure As Date

    <DataMember()>
    Public Property ppCountry() As String
        Get
            Return _countryName
        End Get
        Set(ByVal value As String)
            _countryName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTelephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGuestID() As Integer
        Get
            Return _guestID
        End Get
        Set(ByVal value As Integer)
            _guestID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEmail() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount() As Double
        Get
            Return _rate_amount
        End Get
        Set(ByVal value As Double)
            _rate_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddonAmount() As Double
        Get
            Return _addon_amount
        End Get
        Set(ByVal value As Double)
            _addon_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrival() As Date
        Get
            Return _arrival
        End Get
        Set(ByVal value As Date)
            _arrival = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDeparture() As Date
        Get
            Return _departure
        End Get
        Set(ByVal value As Date)
            _departure = value
        End Set
    End Property
End Class
