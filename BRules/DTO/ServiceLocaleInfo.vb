﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class ServiceLocaleInfo
#Region "Variables privadas"
    Private _serviceID As Integer
    Private _cultureID As Integer
    Private _description As String
    Private _longDescription As String
    Private _error As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCultureID As Integer
        Get
            Return _cultureID
        End Get
        Set(ByVal value As Integer)
            _cultureID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLongDescription As String
        Get
            Return _longDescription
        End Get
        Set(ByVal value As String)
            _longDescription = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region
End Class
