﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class QrySaleItems

#Region "Variables privadas"
    Dim _saleID, _description, _hour, _hotel, _hotelPickup As String
    Dim _arrivalDate As Date
    Dim _adultsQty, _childrenQty, _studentsQty, _infantsQty As Integer
    Dim _adultsAmt, _childrenAmt, _studentsAmt, _infantsAmt, _discountAmt As Double
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppDiscountAmt As Double
        Get
            Return _discountAmt
        End Get
        Set(ByVal value As Double)
            _discountAmt = value
        End Set
    End Property

    <DataMember()>
        Public Property ppInfantsAmt As Double
            Get
                Return _infantsAmt
            End Get
            Set(ByVal value As Double)
                _infantsAmt = value
            End Set
        End Property

        <DataMember()>
        Public Property ppStudentsAmt As Double
            Get
                Return _studentsAmt
            End Get
            Set(ByVal value As Double)
                _studentsAmt = value
            End Set
        End Property

        <DataMember()>
        Public Property ppChildrenAmt As Double
            Get
                Return _childrenAmt
            End Get
            Set(ByVal value As Double)
                _childrenAmt = value
            End Set
        End Property

        <DataMember()>
        Public Property ppAdultsAmt As Double
            Get
                Return _adultsAmt
            End Get
            Set(ByVal value As Double)
                _adultsAmt = value
            End Set
        End Property

        <DataMember()>
        Public Property ppInfantsQty As Integer
            Get
                Return _infantsQty
            End Get
            Set(ByVal value As Integer)
                _infantsQty = value
            End Set
        End Property

        <DataMember()>
        Public Property ppStudentsQty As Integer
            Get
                Return _studentsQty
            End Get
            Set(ByVal value As Integer)
                _studentsQty = value
            End Set
        End Property

        <DataMember()>
        Public Property ppChildrenQty As Integer
            Get
                Return _childrenQty
            End Get
            Set(ByVal value As Integer)
                _childrenQty = value
            End Set
        End Property

        <DataMember()>
        Public Property ppAdultsQty As Integer
            Get
                Return _adultsQty
            End Get
            Set(ByVal value As Integer)
                _adultsQty = value
            End Set
        End Property

        <DataMember()>
        Public Property ppArrivalDate As Date
            Get
                Return _arrivalDate
            End Get
            Set(ByVal value As Date)
                _arrivalDate = value
            End Set
        End Property

        <DataMember()>
        Public Property ppHotelPickup As String
            Get
                Return _hotelPickup
            End Get
            Set(ByVal value As String)
                _hotelPickup = value
            End Set
        End Property

        <DataMember()>
        Public Property ppHotel As String
            Get
                Return _hotel
            End Get
            Set(ByVal value As String)
                _hotel = value
            End Set
        End Property

        <DataMember()>
        Public Property ppHour As String
            Get
                Return _hour
            End Get
            Set(ByVal value As String)
                _hour = value
            End Set
        End Property

        <DataMember()>
        Public Property ppDescription As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        <DataMember()>
        Public Property ppSaleID As String
            Get
                Return _saleID
            End Get
            Set(ByVal value As String)
                _saleID = value
            End Set
        End Property
#End Region
End Class




