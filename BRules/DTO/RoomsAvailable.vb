﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel



<DataContract()>
Public Class RoomsAvailable

#Region "Variables privadas"

    Dim _roomTypeCode As String
    Dim _description, _details As String
    Dim _roomsAvailable As Integer
    Dim _roomTypeID As Integer
    Dim _maxOccupance As Integer
    Dim _minOccupance As Integer
    Dim _adults As Double
    Dim _children As Double
    Dim _unitPrice As Double
    Dim _discount As Double
    Dim _cantAdults As Integer
    Dim _cantChildren As Integer
    Dim _date As Date


#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppCantAdults As Integer
        Get
            Return _cantAdults
        End Get
        Set(ByVal value As Integer)
            _cantAdults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCantChildren As Integer
        Get
            Return _cantChildren
        End Get
        Set(ByVal value As Integer)
            _cantChildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxOccupance As Integer
        Get
            Return _maxOccupance
        End Get
        Set(ByVal value As Integer)
            _maxOccupance = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMinOccupance As Integer
        Get
            Return _minOccupance
        End Get
        Set(ByVal value As Integer)
            _minOccupance = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDate As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeID As Integer
        Get
            Return _roomTypeID
        End Get
        Set(ByVal value As Integer)
            _roomTypeID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeCode() As String
        Get
            Return _roomTypeCode
        End Get
        Set(ByVal value As String)
            _roomTypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomDetails() As String
        Get
            Return _details
        End Get
        Set(ByVal value As String)
            _details = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomsAvailable() As Integer
        Get
            Return _roomsAvailable
        End Get
        Set(ByVal value As Integer)
            _roomsAvailable = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults() As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren() As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount() As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUnitPrice() As Double
        Get
            Return _unitPrice
        End Get
        Set(ByVal value As Double)
            _unitPrice = value
        End Set
    End Property

#End Region

End Class
