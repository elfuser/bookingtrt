﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsReservationsInfo

#Region "Variables privadas"

    Private _company_code, _reservationID, _rate_code, _visit_reason, _remarks, _language, _resellerID, _sessionID As String
    Private _guestID, _ccard_transid, _promo_id, _rooms, _origin As Integer
    Private _arrival, _departure, _creation_date, _cancellation_date, _void_date As Date
    Private _rate_amount, _tax_amount, _addon_amount, _promo_amount, _discount As Double
    Private _Processor As String
    Private _room1Qty As String
    Private _room2Qty As String
    Private _room3Qty As String
    Private _room4Qty As String
    Private _CardType As String
    Private _ppSaleID As String
    Private _ppAmount As Double



#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property CardType() As String
        Get
            Return _CardType
        End Get
        Set(ByVal value As String)
            _CardType = value
        End Set
    End Property


    <DataMember()>
    Public Property Room1Qty() As String
        Get
            Return _room1Qty
        End Get
        Set(ByVal value As String)
            _room1Qty = value
        End Set
    End Property


    <DataMember()>
    Public Property Room2Qty() As String
        Get
            Return _room2Qty
        End Get
        Set(ByVal value As String)
            _room2Qty = value
        End Set
    End Property


    <DataMember()>
    Public Property Room3Qty() As String
        Get
            Return _room3Qty
        End Get
        Set(ByVal value As String)
            _room3Qty = value
        End Set
    End Property



    <DataMember()>
    Public Property Room4Qty() As String
        Get
            Return _room4Qty
        End Get
        Set(ByVal value As String)
            _room4Qty = value
        End Set
    End Property

    <DataMember()>
    Public Property Processor() As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

    <DataMember()>
    Public Property ppReservationID() As String
        Get
            Return _reservationID
        End Get
        Set(ByVal value As String)
            _reservationID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSessionID() As String
        Get
            Return _sessionID
        End Get
        Set(ByVal value As String)
            _sessionID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppResellerID() As String
        Get
            Return _resellerID
        End Get
        Set(ByVal value As String)
            _resellerID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRate_Code() As String
        Get
            Return _rate_code
        End Get
        Set(ByVal value As String)
            _rate_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppVisit_Reason() As String
        Get
            Return _visit_reason
        End Get
        Set(ByVal value As String)
            _visit_reason = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRemarks() As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLanguage() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompany_Code() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRooms() As Integer
        Get
            Return _rooms
        End Get
        Set(ByVal value As Integer)
            _rooms = value
        End Set
    End Property

    <DataMember()>
    Public Property ppOrigin() As Integer
        Get
            Return _origin
        End Get
        Set(ByVal value As Integer)
            _origin = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGuestID() As Integer
        Get
            Return _guestID
        End Get
        Set(ByVal value As Integer)
            _guestID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCcard_TransID() As Integer
        Get
            Return _ccard_transid
        End Get
        Set(ByVal value As Integer)
            _ccard_transid = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrival() As Date
        Get
            Return _arrival
        End Get
        Set(ByVal value As Date)
            _arrival = value
        End Set
    End Property

    <DataMember()>
    Public Property ppVoid() As Date
        Get
            Return _void_date
        End Get
        Set(ByVal value As Date)
            _void_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDeparture() As Date
        Get
            Return _departure
        End Get
        Set(ByVal value As Date)
            _departure = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreation_Date() As Date
        Get
            Return _creation_date
        End Get
        Set(ByVal value As Date)
            _creation_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCancellation_Date() As Date
        Get
            Return _cancellation_date
        End Get
        Set(ByVal value As Date)
            _cancellation_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateAmount() As Double
        Get
            Return _rate_amount
        End Get
        Set(ByVal value As Double)
            _rate_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTaxAmount() As Double
        Get
            Return _tax_amount
        End Get
        Set(ByVal value As Double)
            _tax_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount() As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddonAmount() As Double
        Get
            Return _addon_amount
        End Get
        Set(ByVal value As Double)
            _addon_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoAmount() As Double
        Get
            Return _promo_amount
        End Get
        Set(ByVal value As Double)
            _promo_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID() As Integer
        Get
            Return _promo_id
        End Get
        Set(ByVal value As Integer)
            _promo_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSaleID() As String
        Get
            Return _ppSaleID
        End Get
        Set(ByVal value As String)
            _ppSaleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount() As Double
        Get
            Return _ppAmount
        End Get
        Set(ByVal value As Double)
            _ppAmount = value
        End Set
    End Property

#End Region

End Class
