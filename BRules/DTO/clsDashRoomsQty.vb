﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsDashRoomsQty

#Region "Variables privadas"
    Dim _room As String
    Dim _qty As Integer
#End Region


#Region "Propiedades"
    <DataMember()>
    Public Property room() As String
        Get
            Return _room
        End Get
        Set(ByVal value As String)
            _room = value
        End Set
    End Property

    <DataMember()>
    Public Property qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
#End Region
End Class
