﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class RateDetailInfo

#Region "Variables privadas"
    Private _rateDet_id As Long
    Private _rate_id As Integer
    Private _roomtype_id As Integer
    Private _start_date As Date
    Private _end_date As Date
    Private _FormatStartDate As String
    Private _FormatEndDate As String


    Private _start_dateAnt As Date
    Private _end_dateAnt As Date

    Private _adult1 As Double
    Private _adult2 As Double
    Private _adult3 As Double
    Private _adult4 As Double
    Private _adult5 As Double
    Private _adult6 As Double
    Private _adult7 As Double
    Private _adult8 As Double
    Private _adult9 As Double
    Private _adult10 As Double
    Private _adult11 As Double
    Private _adult12 As Double
    Private _child As Double
    Private _wadult1 As Double
    Private _wadult2 As Double
    Private _wadult3 As Double
    Private _wadult4 As Double
    Private _wadult5 As Double
    Private _wadult6 As Double
    Private _wadult7 As Double
    Private _wadult8 As Double
    Private _wadult9 As Double
    Private _wadult10 As Double
    Private _wadult11 As Double
    Private _wadult12 As Double
    Private _wchild As Double
    Private _error As String
    Private _rate_Code As String
    Private _room_name As String
    Private _room_code As String
    Private _RowId As Integer

    Dim _UnFormatFrom As Date
    Dim _UnFormatTo As Date

#End Region

#Region "Propiedades"



    <DataMember()>
    Public Property ppRowID As Integer
        Get
            Return _RowId
        End Get
        Set(ByVal value As Integer)
            _RowId = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatFrom As Date
        Get
            Return _UnFormatFrom
        End Get
        Set(ByVal value As Date)
            _UnFormatFrom = value
        End Set
    End Property


    <DataMember()>
    Public Property UnFormatTo As Date
        Get
            Return _UnFormatTo
        End Get
        Set(ByVal value As Date)
            _UnFormatTo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomCode As String
        Get
            Return _room_code
        End Get
        Set(ByVal value As String)
            _room_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomName As String
        Get
            Return _room_name
        End Get
        Set(ByVal value As String)
            _room_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateCode As String
        Get
            Return _rate_Code
        End Get
        Set(ByVal value As String)
            _rate_Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateDet_id As Long
        Get
            Return _rateDet_id
        End Get
        Set(ByVal value As Long)
            _rateDet_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRate_id As Integer
        Get
            Return _rate_id
        End Get
        Set(ByVal value As Integer)
            _rate_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomtype_id As Integer
        Get
            Return _roomtype_id
        End Get
        Set(ByVal value As Integer)
            _roomtype_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStart_date As Date
        Get
            Return _start_date
        End Get
        Set(ByVal value As Date)
            _start_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEnd_date As Date
        Get
            Return _end_date
        End Get
        Set(ByVal value As Date)
            _end_date = value
        End Set
    End Property


    <DataMember()>
    Public Property ppStart_dateAnt As Date
        Get
            Return _start_dateAnt
        End Get
        Set(ByVal value As Date)
            _start_dateAnt = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEnd_dateAnt As Date
        Get
            Return _end_dateAnt
        End Get
        Set(ByVal value As Date)
            _end_dateAnt = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult1 As Double
        Get
            Return _adult1
        End Get
        Set(ByVal value As Double)
            _adult1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult2 As Double
        Get
            Return _adult2
        End Get
        Set(ByVal value As Double)
            _adult2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult3 As Double
        Get
            Return _adult3
        End Get
        Set(ByVal value As Double)
            _adult3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult4 As Double
        Get
            Return _adult4
        End Get
        Set(ByVal value As Double)
            _adult4 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult5 As Double
        Get
            Return _adult5
        End Get
        Set(ByVal value As Double)
            _adult5 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult6 As Double
        Get
            Return _adult6
        End Get
        Set(ByVal value As Double)
            _adult6 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult7 As Double
        Get
            Return _adult7
        End Get
        Set(ByVal value As Double)
            _adult7 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult8 As Double
        Get
            Return _adult8
        End Get
        Set(ByVal value As Double)
            _adult8 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult9 As Double
        Get
            Return _adult9
        End Get
        Set(ByVal value As Double)
            _adult9 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult10 As Double
        Get
            Return _adult10
        End Get
        Set(ByVal value As Double)
            _adult10 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult11 As Double
        Get
            Return _adult11
        End Get
        Set(ByVal value As Double)
            _adult11 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdult12 As Double
        Get
            Return _adult12
        End Get
        Set(ByVal value As Double)
            _adult12 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChild As Double
        Get
            Return _child
        End Get
        Set(ByVal value As Double)
            _child = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult1 As Double
        Get
            Return _wadult1
        End Get
        Set(ByVal value As Double)
            _wadult1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult2 As Double
        Get
            Return _wadult2
        End Get
        Set(ByVal value As Double)
            _wadult2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult3 As Double
        Get
            Return _wadult3
        End Get
        Set(ByVal value As Double)
            _wadult3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult4 As Double
        Get
            Return _wadult4
        End Get
        Set(ByVal value As Double)
            _wadult4 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult5 As Double
        Get
            Return _wadult5
        End Get
        Set(ByVal value As Double)
            _wadult5 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult6 As Double
        Get
            Return _wadult6
        End Get
        Set(ByVal value As Double)
            _wadult6 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult7 As Double
        Get
            Return _wadult7
        End Get
        Set(ByVal value As Double)
            _wadult7 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult8 As Double
        Get
            Return _wadult8
        End Get
        Set(ByVal value As Double)
            _wadult8 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult9 As Double
        Get
            Return _wadult9
        End Get
        Set(ByVal value As Double)
            _wadult9 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult10 As Double
        Get
            Return _wadult10
        End Get
        Set(ByVal value As Double)
            _wadult10 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult11 As Double
        Get
            Return _wadult11
        End Get
        Set(ByVal value As Double)
            _wadult11 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWAdult12 As Double
        Get
            Return _wadult12
        End Get
        Set(ByVal value As Double)
            _wadult12 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWChild As Double
        Get
            Return _wchild
        End Get
        Set(ByVal value As Double)
            _wchild = value
        End Set
    End Property


    <DataMember()>
    Public Property FormatStartDate As String
        Get
            Return _FormatStartDate
        End Get
        Set(ByVal value As String)
            _FormatStartDate = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatEndDate As String
        Get
            Return _FormatEndDate
        End Get
        Set(ByVal value As String)
            _FormatEndDate = value
        End Set
    End Property

#End Region

End Class

