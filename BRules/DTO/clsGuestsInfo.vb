﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsGuestsInfo

#Region "Variables privadas"

    Private _guestID As Long
    Private _name, _lastname As String
    Private _telephone As String
    Private _country_iso3 As String
    Private _email As String
    Private _company_code As String
    Private _creation_date As Date

#End Region


#Region "Propiedades"
    <DataMember()>
    Public Property ppCreationDate() As Date
        Get
            Return _creation_date
        End Get
        Set(ByVal value As Date)
            _creation_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGuestID() As Long
        Get
            Return _guestID
        End Get
        Set(ByVal value As Long)
            _guestID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastname() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTelephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountry_Iso3() As String
        Get
            Return _country_iso3
        End Get
        Set(ByVal value As String)
            _country_iso3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEmail() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompany_Code() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property
#End Region
End Class

