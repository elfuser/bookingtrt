﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class SessionInfo
#Region "Variables privadas"
    Private _companyCode, _sessionID, _resellerID, _reservationID, _PayPalToken, _addons, _lang As String
    Private _rateCode, _keyID As String
    Private _arrivalDate, _departureDate, _created As Date
    Private _rooms, _promoID As Integer
    Private _room1Code, _room1Qty, _room2Code, _room2Qty, _room3Code, _room3Qty, _room4Code, _room4Qty As String
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppKeyID As String
        Get
            Return _keyID
        End Get
        Set(ByVal value As String)
            _keyID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppReservationID As String
        Get
            Return _reservationID
        End Get
        Set(ByVal value As String)
            _reservationID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddons As String
        Get
            Return _addons
        End Get
        Set(ByVal value As String)
            _addons = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLanguage As String
        Get
            Return _lang
        End Get
        Set(ByVal value As String)
            _lang = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPayPalToken As String
        Get
            Return _PayPalToken
        End Get
        Set(ByVal value As String)
            _PayPalToken = value
        End Set
    End Property


    <DataMember()>
    Public Property ppSessionID As String
        Get
            Return _sessionID
        End Get
        Set(ByVal value As String)
            _sessionID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppResellerID As String
        Get
            Return _resellerID
        End Get
        Set(ByVal value As String)
            _resellerID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateCode As String
        Get
            Return _rateCode
        End Get
        Set(ByVal value As String)
            _rateCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrivalDate As Date
        Get
            Return _arrivalDate
        End Get
        Set(ByVal value As Date)
            _arrivalDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDepartureDate As Date
        Get
            Return _departureDate
        End Get
        Set(ByVal value As Date)
            _departureDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreated As Date
        Get
            Return _created
        End Get
        Set(ByVal value As Date)
            _created = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRooms As Integer
        Get
            Return _rooms
        End Get
        Set(ByVal value As Integer)
            _rooms = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID As Integer
        Get
            Return _promoID
        End Get
        Set(ByVal value As Integer)
            _promoID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom1Code As String
        Get
            Return _room1Code
        End Get
        Set(ByVal value As String)
            _room1Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom1Qty As String
        Get
            Return _room1Qty
        End Get
        Set(ByVal value As String)
            _room1Qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom2Code As String
        Get
            Return _room2Code
        End Get
        Set(ByVal value As String)
            _room2Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom2Qty As String
        Get
            Return _room2Qty
        End Get
        Set(ByVal value As String)
            _room2Qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom3Code As String
        Get
            Return _room3Code
        End Get
        Set(ByVal value As String)
            _room3Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom3Qty As String
        Get
            Return _room3Qty
        End Get
        Set(ByVal value As String)
            _room3Qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom4Code As String
        Get
            Return _room4Code
        End Get
        Set(ByVal value As String)
            _room4Code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom4Qty As String
        Get
            Return _room4Qty
        End Get
        Set(ByVal value As String)
            _room4Qty = value
        End Set
    End Property
#End Region
End Class
