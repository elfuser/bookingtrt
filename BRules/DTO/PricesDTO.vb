﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class PricesDTO

#Region "Variables privadas"
    Private _date As Date
    Private _Prices As Double
    Private _strdate As String
    Private _discount As Double
    Private _promo_amount As Double
    Private _promo_type As String
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppDate As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPrices As Double
        Get
            Return _Prices
        End Get
        Set(ByVal value As Double)
            _Prices = value
        End Set
    End Property

    <DataMember()>
    Public Property strdate As String
        Get
            Return _strdate
        End Get
        Set(ByVal value As String)
            _strdate = value
        End Set
    End Property


    <DataMember()>
    Public Property Discount As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoAmount As Double
        Get
            Return _promo_amount
        End Get
        Set(ByVal value As Double)
            _promo_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoType As String
        Get
            Return _promo_type
        End Get
        Set(ByVal value As String)
            _promo_type = value
        End Set
    End Property

#End Region

End Class
