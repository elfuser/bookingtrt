﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class QryAvailability
#Region "Variables privadas"
    Private _date As Date
    Private _room As String
    Private _rate As String
    Private _roomtypeCode As String
    Private _quantity As Integer
    Private _occupied As Integer
    Private _blocked As Integer
    Private _available As Integer
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppDate As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRate As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeCode As String
        Get
            Return _roomtypeCode
        End Get
        Set(ByVal value As String)
            _roomtypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom As String
        Get
            Return _room
        End Get
        Set(ByVal value As String)
            _room = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQuantity As Integer
        Get
            Return _quantity
        End Get
        Set(ByVal value As Integer)
            _quantity = value
        End Set
    End Property

    <DataMember()>
    Public Property ppOccupaid As Integer
        Get
            Return _occupied
        End Get
        Set(ByVal value As Integer)
            _occupied = value
        End Set
    End Property

    <DataMember()>
    Public Property ppBlocked As Integer
        Get
            Return _blocked
        End Get
        Set(ByVal value As Integer)
            _blocked = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAvailable As Integer
        Get
            Return _available
        End Get
        Set(ByVal value As Integer)
            _available = value
        End Set
    End Property
#End Region
End Class
