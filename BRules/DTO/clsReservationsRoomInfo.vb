﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

Public Class clsReservationsRoomInfo

#Region "Variables privadas"
    Private _adults, _children As Integer
    Private _children_free As Integer
    Private _roomtype_id As Integer
    Private _reservation_id As String
    Private _rateAmount, _taxAmount As Double
    Private _description, _roomTypeCode As String
    Private _date As Date
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppRoomTypeCode As String
        Get
            Return _roomTypeCode
        End Get
        Set(ByVal value As String)
            _roomTypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDate As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTaxAmount As Double
        Get
            Return _taxAmount
        End Get
        Set(ByVal value As Double)
            _taxAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateAmount As Double
        Get
            Return _rateAmount
        End Get
        Set(ByVal value As Double)
            _rateAmount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppReservationID As String
        Get
            Return _reservation_id
        End Get
        Set(ByVal value As String)
            _reservation_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeID() As Integer
        Get
            Return _roomtype_id
        End Get
        Set(ByVal value As Integer)
            _roomtype_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults() As Integer
        Get
            Return _adults
        End Get
        Set(ByVal value As Integer)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren() As Integer
        Get
            Return _children
        End Get
        Set(ByVal value As Integer)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildrenFree() As Integer
        Get
            Return _children_free
        End Get
        Set(ByVal value As Integer)
            _children_free = value
        End Set
    End Property
#End Region

End Class
