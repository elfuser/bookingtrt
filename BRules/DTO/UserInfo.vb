﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class UserInfo
#Region "Variables privadas"
    Private _user_id As Integer
    Private _company_code As String
    Private _company As String
    Private _user_name As String
    Private _password As String
    Private _PasswordConfirmation As String
    Private _first_name As String
    Private _last_name As String
    Private _profile_id As Integer
    Private _profile_name As String
    Private _SecondSurname As String
    Private _active As Boolean
    Private _last_login As DateTime
    Private _error As String
    Private _last_loginFormat As String
    Private _isAgencyUser As Boolean
    Private _AgencyCode As String
    Private _isDomainUser As Boolean

#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppProfile As String
        Get
            Return _profile_name
        End Get
        Set(ByVal value As String)
            _profile_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompany As String
        Get
            Return _company
        End Get
        Set(ByVal value As String)
            _company = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUserID As Integer
        Get
            Return _user_id
        End Get
        Set(ByVal value As Integer)
            _user_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUserName As String
        Get
            Return _user_name
        End Get
        Set(ByVal value As String)
            _user_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPassword As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property


    <DataMember()>
    Public Property ppPasswordConfirmation As String
        Get
            Return _PasswordConfirmation
        End Get
        Set(ByVal value As String)
            _PasswordConfirmation = value
        End Set
    End Property


    <DataMember()>
    Public Property ppFirstName As String
        Get
            Return _first_name
        End Get
        Set(ByVal value As String)
            _first_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastName As String
        Get
            Return _last_name
        End Get
        Set(ByVal value As String)
            _last_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppProfileID As Integer
        Get
            Return _profile_id
        End Get
        Set(ByVal value As Integer)
            _profile_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastLogin As DateTime
        Get
            Return _last_login
        End Get
        Set(ByVal value As DateTime)
            _last_login = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property


    <DataMember()>
    Public Property SecondSurname As String
        Get
            Return _SecondSurname
        End Get
        Set(ByVal value As String)
            _SecondSurname = value
        End Set
    End Property


    <DataMember()>
    Public Property LastLoginFormat As String
        Get
            Return _last_loginFormat
        End Get
        Set(ByVal value As String)
            _last_loginFormat = value
        End Set
    End Property

    <DataMember()>
    Public Property isAgencyUser As Boolean
        Get
            Return _isAgencyUser
        End Get
        Set(ByVal value As Boolean)
            _isAgencyUser = value
        End Set
    End Property

    <DataMember()>
    Public Property AgencyCode As String
        Get
            Return _AgencyCode
        End Get
        Set(ByVal value As String)
            _AgencyCode = value
        End Set
    End Property


    <DataMember()>
    Public Property ppisDomainUser As Boolean
        Get
            Return _isDomainUser

        End Get
        Set(ByVal value As Boolean)
            _isDomainUser = value
        End Set
    End Property
#End Region


End Class
