﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class RateRoomTypeInfo

#Region "Variables privadas"
    Private _rateDet_id As Long
    Private _roomtype_id As Integer
    Private _roomtype_code As String
    Private _description As String
    Private _error As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppRateDet_id As Long
        Get
            Return _rateDet_id
        End Get
        Set(ByVal value As Long)
            _rateDet_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomType_id As Integer
        Get
            Return _roomtype_id
        End Get
        Set(ByVal value As Integer)
            _roomtype_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeCode As String
        Get
            Return _roomtype_code
        End Get
        Set(ByVal value As String)
            _roomtype_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property


#End Region
End Class
