﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class ServiceInfo
#Region "Variables privadas"
    Private _serviceID, _difficulty, _listOrder, _categoryID, _QConditions, _QBringInfo As Integer
    Private _companyCode, _code As String
    Private _description, _details, _conditions, _bringInfo, _videoURL As String
    Private _error As String
    Private _adults, _children, _infants, _student, _discount As Double
    Private _active, _controlInventory As Boolean
    Private _picture1, _picture2, _picture3, _discountType As String
    Private _maxadult, _maxchildren, _maxinfant, _maxstudent, _rateId As Integer
    Private _ratestartdate, _rateenddate As Date

#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppPicture1() As String
        Get
            Return _picture1
        End Get
        Set(value As String)
            _picture1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPicture2() As String
        Get
            Return _picture2
        End Get
        Set(value As String)
            _picture2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPicture3() As String
        Get
            Return _picture3
        End Get
        Set(value As String)
            _picture3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCode() As String
        Get
            Return _code
        End Get
        Set(value As String)
            _code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQConditions As Integer
        Get
            Return _QConditions
        End Get
        Set(ByVal value As Integer)
            _QConditions = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQBringInfo As Integer
        Get
            Return _QBringInfo
        End Get
        Set(ByVal value As Integer)
            _QBringInfo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCategoryID As Integer
        Get
            Return _categoryID
        End Get
        Set(ByVal value As Integer)
            _categoryID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppListOrder As Integer
        Get
            Return _listOrder
        End Get
        Set(ByVal value As Integer)
            _listOrder = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDifficulty As Integer
        Get
            Return _difficulty
        End Get
        Set(ByVal value As Integer)
            _difficulty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDetails As String
        Get
            Return _details
        End Get
        Set(ByVal value As String)
            _details = value
        End Set
    End Property

    <DataMember()>
    Public Property ppConditions As String
        Get
            Return _conditions
        End Get
        Set(ByVal value As String)
            _conditions = value
        End Set
    End Property

    <DataMember()>
    Public Property ppBringInfo As String
        Get
            Return _bringInfo
        End Get
        Set(ByVal value As String)
            _bringInfo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppVideoURL As String
        Get
            Return _videoURL
        End Get
        Set(ByVal value As String)
            _videoURL = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Double
        Get
            Return _infants
        End Get
        Set(ByVal value As Double)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppControlInventory As Boolean
        Get
            Return _controlInventory
        End Get
        Set(ByVal value As Boolean)
            _controlInventory = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscountType() As String
        Get
            Return _discountType
        End Get
        Set(value As String)
            _discountType = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudent As Double
        Get
            Return _student
        End Get
        Set(ByVal value As Double)
            _student = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxadult As Integer
        Get
            Return _maxadult
        End Get
        Set(ByVal value As Integer)
            _maxadult = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxchildren As Integer
        Get
            Return _maxchildren
        End Get
        Set(ByVal value As Integer)
            _maxchildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxinfant As Integer
        Get
            Return _maxinfant
        End Get
        Set(ByVal value As Integer)
            _maxinfant = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxstudent As Integer
        Get
            Return _maxstudent
        End Get
        Set(ByVal value As Integer)
            _maxstudent = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRatestartdate As Date
        Get
            Return _ratestartdate
        End Get
        Set(ByVal value As Date)
            _ratestartdate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateenddate As Date
        Get
            Return _rateenddate
        End Get
        Set(ByVal value As Date)
            _rateenddate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateId As String
        Get
            Return _rateId
        End Get
        Set(ByVal value As String)
            _rateId = value
        End Set
    End Property

End Class

<DataContract()>
Public Class clsServiceSCInfo
    Private _rowID, _serviceID, _qtyAdults, _qtyChildren, _qtyInfants, _qtyStudents As Integer
    Private _description, _checkIn, _hotel, _hotelPickup, _error As String
    Private _adults, _children, _infants, _students, _total As Double
    Private _arrivalDate As Date

    <DataMember()>
    Public Property ppArrivalDate As Date
        Get
            Return _arrivalDate
        End Get
        Set(ByVal value As Date)
            _arrivalDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowID As Integer
        Get
            Return _rowID
        End Get
        Set(ByVal value As Integer)
            _rowID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyAdults As Integer
        Get
            Return _qtyAdults
        End Get
        Set(ByVal value As Integer)
            _qtyAdults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyChildren As Integer
        Get
            Return _qtyChildren
        End Get
        Set(ByVal value As Integer)
            _qtyChildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyInfants As Integer
        Get
            Return _qtyInfants
        End Get
        Set(ByVal value As Integer)
            _qtyInfants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyStudents As Integer
        Get
            Return _qtyStudents
        End Get
        Set(ByVal value As Integer)
            _qtyStudents = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCheckIn As String
        Get
            Return _checkIn
        End Get
        Set(ByVal value As String)
            _checkIn = value
        End Set
    End Property

    <DataMember()>
    Public Property ppHotel As String
        Get
            Return _hotel
        End Get
        Set(ByVal value As String)
            _hotel = value
        End Set
    End Property

    <DataMember()>
    Public Property ppHotelPickup As String
        Get
            Return _hotelPickup
        End Get
        Set(ByVal value As String)
            _hotelPickup = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Double
        Get
            Return _infants
        End Get
        Set(ByVal value As Double)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudents As Double
        Get
            Return _students
        End Get
        Set(ByVal value As Double)
            _students = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTotal As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region
End Class
