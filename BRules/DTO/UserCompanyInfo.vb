﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class UserCompanyInfo

#Region "Variables privadas"
    Private _userId As Integer
    Private _companyCode As String, _companyName As String
    Private _services, _hotel As Boolean
    Private _error As String
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppHotel As Boolean
        Get
            Return _hotel
        End Get
        Set(ByVal value As Boolean)
            _hotel = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServices As Boolean
        Get
            Return _services
        End Get
        Set(ByVal value As Boolean)
            _services = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUserId As Integer
        Get
            Return _userId
        End Get
        Set(ByVal value As Integer)
            _userId = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyName As String
        Get
            Return _companyName
        End Get
        Set(ByVal value As String)
            _companyName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region
End Class


