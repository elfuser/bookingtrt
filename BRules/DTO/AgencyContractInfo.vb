﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class AgencyContractInfo

#Region "Variables privadas"
    Private _agency_contract_code As Integer
    Private _agency_code As Integer
    Private _start_date As Date
    Private _end_date As Date
    Private _notes As String
    Private _last_date_modified As Date

    Private _FormatStartDate As String
    Private _FormatEndDate As String
    Private _FormatLastDateModified As String

    Private _UnFormatFrom As String
    Private _UnFormatTo As String

    Private _contractFileName As String
    Private _contractFileType As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppAgencyContractCode As Integer
        Get
            Return _agency_contract_code
        End Get
        Set(ByVal value As Integer)
            _agency_contract_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAgencyCode As Integer
        Get
            Return _agency_code
        End Get
        Set(ByVal value As Integer)
            _agency_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStart_date As Date
        Get
            Return _start_date
        End Get
        Set(ByVal value As Date)
            _start_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEnd_date As Date
        Get
            Return _end_date
        End Get
        Set(ByVal value As Date)
            _end_date = value
        End Set
    End Property


    <DataMember()>
    Public Property ppNotes As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastDateModified As Date
        Get
            Return _last_date_modified
        End Get
        Set(ByVal value As Date)
            _last_date_modified = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatStartDate As String
        Get
            Return _FormatStartDate
        End Get
        Set(ByVal value As String)
            _FormatStartDate = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatEndDate As String
        Get
            Return _FormatEndDate
        End Get
        Set(ByVal value As String)
            _FormatEndDate = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatLastDateModified As String
        Get
            Return _FormatLastDateModified
        End Get
        Set(ByVal value As String)
            _FormatLastDateModified = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatFrom As String
        Get
            Return _UnFormatFrom
        End Get
        Set(ByVal value As String)
            _UnFormatFrom = value
        End Set
    End Property

    <DataMember()>
    Public Property UnFormatTo As String
        Get
            Return _UnFormatTo
        End Get
        Set(ByVal value As String)
            _UnFormatTo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContractFileName As String
        Get
            Return _contractFileName
        End Get
        Set(ByVal value As String)
            _contractFileName = value
        End Set
    End Property

    <DataMember()>
    Public Property ppContractFileType As String
        Get
            Return _contractFileType
        End Get
        Set(ByVal value As String)
            _contractFileType = value
        End Set
    End Property

#End Region

End Class

