﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsDashCountryQty

#Region "Variables privadas"
    Dim _country As String
    Dim _qty As Integer
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
#End Region

End Class
