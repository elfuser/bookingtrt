﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class ServiceAvailability
#Region "Variables privadas"
    Private _scheduleID As Integer
    Private _hour As String
    Private _qty As Integer
    Private _available As Integer
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppScheduleID() As Integer
        Get
            Return _scheduleID
        End Get
        Set(value As Integer)
            _scheduleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppHour() As String
        Get
            Return _hour
        End Get
        Set(value As String)
            _hour = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQty() As Integer
        Get
            Return _qty
        End Get
        Set(value As Integer)
            _qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAvailable() As Integer
        Get
            Return _available
        End Get
        Set(value As Integer)
            _available = value
        End Set
    End Property
#End Region
End Class
