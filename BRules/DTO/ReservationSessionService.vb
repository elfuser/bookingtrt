﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class ReservationSessionService

#Region "Variables privadas"
    Private _objGuestInfo As New clsGuestsInfo
    'Private _objResInfo As New clsReservationsInfo
    Private _objSaleInfo As New clsSaleInfo
    Private _ServicesList As New List(Of ServiceSCInfo)
#End Region


#Region "Propiedades"

    <DataMember()>
    Public Property GuestsInfo As clsGuestsInfo
        Get
            Return _objGuestInfo
        End Get
        Set(ByVal value As clsGuestsInfo)
            _objGuestInfo = value
        End Set
    End Property


    <DataMember()>
    Public Property SaleInfo As clsSaleInfo
        Get
            Return _objSaleInfo
        End Get
        Set(ByVal value As clsSaleInfo)
            _objSaleInfo = value
        End Set
    End Property

    <DataMember()>
    Public Property ServicesList As List(Of ServiceSCInfo)
        Get
            Return _ServicesList
        End Get
        Set(ByVal value As List(Of ServiceSCInfo))
            _ServicesList = value
        End Set
    End Property

#End Region

End Class
