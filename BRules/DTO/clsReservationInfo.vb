﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class clsReservationInfo
    Private _integrationIP, _reservationID, _rate, _visitReason, _remarks, _countryISO, _rateTerms As String
    Private _name, _lastname, _email, _telephone, _country, _cardType, _cardNumber, _cardExp As String
    Private _guestID, _rooms, _integrationPort As Integer
    Private _arrival, _departure, _creation As Date
    Private _rate_amount, _tax_amount, _addon_amount, _promo_amount, _disc_amount As Double

    <DataMember()>
    Public Property ppReservationID() As String
        Get
            Return _reservationID
        End Get
        Set(ByVal value As String)
            _reservationID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppIntegrationIP() As String
        Get
            Return _integrationIP
        End Get
        Set(ByVal value As String)
            _integrationIP = value
        End Set
    End Property

    <DataMember()>
    Public Property ppName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateTerms() As String
        Get
            Return _rateTerms
        End Get
        Set(ByVal value As String)
            _rateTerms = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRate() As String
        Get
            Return _rate
        End Get
        Set(ByVal value As String)
            _rate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastName() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEmail() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTelephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountry() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRooms() As Integer
        Get
            Return _rooms
        End Get
        Set(ByVal value As Integer)
            _rooms = value
        End Set
    End Property

    <DataMember()>
    Public Property ppIntegrationPort() As Integer
        Get
            Return _integrationPort
        End Get
        Set(ByVal value As Integer)
            _integrationPort = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCardType() As String
        Get
            Return _cardType
        End Get
        Set(ByVal value As String)
            _cardType = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGuestID() As Integer
        Get
            Return _guestID
        End Get
        Set(ByVal value As Integer)
            _guestID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCardNumber() As String
        Get
            Return _cardNumber
        End Get
        Set(ByVal value As String)
            _cardNumber = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrival() As Date
        Get
            Return _arrival
        End Get
        Set(ByVal value As Date)
            _arrival = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreation() As Date
        Get
            Return _creation
        End Get
        Set(ByVal value As Date)
            _creation = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountryISO() As String
        Get
            Return _countryISO
        End Get
        Set(ByVal value As String)
            _countryISO = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCardExp() As String
        Get
            Return _cardExp
        End Get
        Set(ByVal value As String)
            _cardExp = value
        End Set
    End Property

    <DataMember()>
    Public Property ppVisitReason() As String
        Get
            Return _visitReason
        End Get
        Set(ByVal value As String)
            _visitReason = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRemarks() As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDeparture() As Date
        Get
            Return _departure
        End Get
        Set(ByVal value As Date)
            _departure = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateAmount() As Double
        Get
            Return _rate_amount
        End Get
        Set(ByVal value As Double)
            _rate_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTaxAmount() As Double
        Get
            Return _tax_amount
        End Get
        Set(ByVal value As Double)
            _tax_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddonAmount() As Double
        Get
            Return _addon_amount
        End Get
        Set(ByVal value As Double)
            _addon_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoAmount() As Double
        Get
            Return _promo_amount
        End Get
        Set(ByVal value As Double)
            _promo_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscAmount() As Double
        Get
            Return _disc_amount
        End Get
        Set(ByVal value As Double)
            _disc_amount = value
        End Set
    End Property
End Class

