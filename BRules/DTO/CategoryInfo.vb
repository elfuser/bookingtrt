﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class CategoryInfo

#Region "Variables privadas"
    Private _categoryID As Integer
    Private _description, _error As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppCategoryID As Integer
        Get
            Return _categoryID
        End Get
        Set(ByVal value As Integer)
            _categoryID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region

End Class
