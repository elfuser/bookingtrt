﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class ReservationSession

#Region "Variables privadas"
    Private _objGuestInfo As New clsGuestsInfo
    Private _objResInfo As New clsReservationsInfo
    Private _RoomList As New List(Of clsReservationsRoomInfo)
    Private _AddonList As New List(Of AddonInfo)
    Private _GoPage As String = ""
#End Region


#Region "Propiedades"

    <DataMember()>
    Public Property GoPage As String
        Get
            Return _GoPage
        End Get
        Set(ByVal value As String)
            _GoPage = value
        End Set
    End Property

    <DataMember()>
    Public Property GuestsInfo As clsGuestsInfo
        Get
            Return _objGuestInfo
        End Get
        Set(ByVal value As clsGuestsInfo)
            _objGuestInfo = value
        End Set
    End Property


    <DataMember()>
    Public Property ReservationsInfo As clsReservationsInfo
        Get
            Return _objResInfo
        End Get
        Set(ByVal value As clsReservationsInfo)
            _objResInfo = value
        End Set
    End Property

    <DataMember()>
    Public Property RoomList As List(Of clsReservationsRoomInfo)
        Get
            Return _RoomList
        End Get
        Set(ByVal value As List(Of clsReservationsRoomInfo))
            _RoomList = value
        End Set
    End Property

    <DataMember()>
    Public Property AddonList As List(Of AddonInfo)
        Get
            Return _AddonList
        End Get
        Set(ByVal value As List(Of AddonInfo))
            _AddonList = value
        End Set
    End Property

#End Region

End Class
