﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsDashSalesByMonth

#Region "Variables privadas"
    Dim _day As String
    Dim _amount As Double
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property dday() As String
        Get
            Return _day
        End Get
        Set(ByVal value As String)
            _day = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property
#End Region
End Class
