﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class CountriesInfo

#Region "Variables privadas"
    Private _countryIso3 As String
    Private _name As String

#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppCountryIso3() As String
        Get
            Return _countryIso3
        End Get
        Set(ByVal value As String)
            _countryIso3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property
#End Region

End Class
