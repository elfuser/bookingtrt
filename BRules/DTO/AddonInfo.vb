﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class AddonInfo

#Region "Variables privadas"

    Private _addonID As Long
    Private _qty As Integer
    Private _rowOrder As Integer
    Private _companyCode As String
    Private _description As String
    Private _longDescription As String
    Private _departure As String
    Private _duration As String
    Private _addonType As String
    Private _error As String
    Private _amount As Double
    Private _amountP As Double
    Private _amount1 As Double
    Private _amount2 As Double
    Private _amount3 As Double
    Private _amount4 As Double
    Private _amount5 As Double
    Private _amount6 As Double
    Private _amount7 As Double
    Private _amount8 As Double
    Private _amount9 As Double
    Private _amount10 As Double
    Private _enabled As Boolean
    Private _tax As Boolean
    Private _created As DateTime
    Private _modified As DateTime
    Private _from, _to As DateTime
    Private _ImageUlr As String
    Private _CultureID As String

    Private _Picture1 As String
    Private _Picture2 As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppAddonID As Long
        Get
            Return _addonID
        End Get
        Set(ByVal value As Long)
            _addonID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmountP As Double
        Get
            Return _amountP
        End Get
        Set(ByVal value As Double)
            _amountP = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount1 As Double
        Get
            Return _amount1
        End Get
        Set(ByVal value As Double)
            _amount1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount2 As Double
        Get
            Return _amount2
        End Get
        Set(ByVal value As Double)
            _amount2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount3 As Double
        Get
            Return _amount3
        End Get
        Set(ByVal value As Double)
            _amount3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount4 As Double
        Get
            Return _amount4
        End Get
        Set(ByVal value As Double)
            _amount4 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount5 As Double
        Get
            Return _amount5
        End Get
        Set(ByVal value As Double)
            _amount5 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount6 As Double
        Get
            Return _amount6
        End Get
        Set(ByVal value As Double)
            _amount6 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount7 As Double
        Get
            Return _amount7
        End Get
        Set(ByVal value As Double)
            _amount7 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount8 As Double
        Get
            Return _amount8
        End Get
        Set(ByVal value As Double)
            _amount8 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount9 As Double
        Get
            Return _amount9
        End Get
        Set(ByVal value As Double)
            _amount9 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount10 As Double
        Get
            Return _amount10
        End Get
        Set(ByVal value As Double)
            _amount10 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property
    <DataMember()>
    Public Property ppEnabled As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal value As Boolean)
            _enabled = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTax As Boolean
        Get
            Return _tax
        End Get
        Set(ByVal value As Boolean)
            _tax = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQty As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowOrder As Integer
        Get
            Return _rowOrder
        End Get
        Set(ByVal value As Integer)
            _rowOrder = value
        End Set
    End Property

    <DataMember()>
    Public Property ppFrom As DateTime
        Get
            Return _from
        End Get
        Set(ByVal value As DateTime)
            _from = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTo As DateTime
        Get
            Return _to
        End Get
        Set(ByVal value As DateTime)
            _to = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreated As DateTime
        Get
            Return _created
        End Get
        Set(ByVal value As DateTime)
            _created = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLongDescription As String
        Get
            Return _longDescription
        End Get
        Set(ByVal value As String)
            _longDescription = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDuration As String
        Get
            Return _duration
        End Get
        Set(ByVal value As String)
            _duration = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDeparture As String
        Get
            Return _departure
        End Get
        Set(ByVal value As String)
            _departure = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAddonType As String
        Get
            Return _addonType
        End Get
        Set(ByVal value As String)
            _addonType = value
        End Set
    End Property

    <DataMember()>
    Public Property ImageUlr As String
        Get
            Return _ImageUlr
        End Get
        Set(ByVal value As String)
            _ImageUlr = value
        End Set
    End Property

    <DataMember()>
    Public Property CultureID As String
        Get
            Return _CultureID
        End Get
        Set(ByVal value As String)
            _CultureID = value
        End Set
    End Property

    <DataMember()>
    Public Property Picture1 As String
        Get
            Return _Picture1
        End Get
        Set(ByVal value As String)
            _Picture1 = value
        End Set
    End Property

    <DataMember()>
    Public Property Picture2 As String
        Get
            Return _Picture2
        End Get
        Set(ByVal value As String)
            _Picture2 = value
        End Set
    End Property

#End Region

End Class
