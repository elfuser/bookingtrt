﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsSaleInfo
    Private _company_code, _saleID, _remarks, _language, _resellerID, _sessionID As String
    Private _name, _lastname, _email, _telephone, _country As String
    Private _guestID, _ccard_transid, _promo_id, _services, _origin, _rateID As Integer
    Private _arrival, _creation_date, _cancellation_date As Date
    Private _amount, _tax_amount, _promo_amount, _discount As Double
    Private _CardType As String
    Private _Processor As String

    <DataMember()>
    Public Property ppName() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastName() As String
        Get
            Return _lastname
        End Get
        Set(ByVal value As String)
            _lastname = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEmail() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTelephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountry() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSaleID() As String
        Get
            Return _saleID
        End Get
        Set(ByVal value As String)
            _saleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSessionID() As String
        Get
            Return _sessionID
        End Get
        Set(ByVal value As String)
            _sessionID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppResellerID() As String
        Get
            Return _resellerID
        End Get
        Set(ByVal value As String)
            _resellerID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRemarks() As String
        Get
            Return _remarks
        End Get
        Set(ByVal value As String)
            _remarks = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLanguage() As String
        Get
            Return _language
        End Get
        Set(ByVal value As String)
            _language = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompany_Code() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServices() As Integer
        Get
            Return _services
        End Get
        Set(ByVal value As Integer)
            _services = value
        End Set
    End Property

    <DataMember()>
    Public Property ppOrigin() As Integer
        Get
            Return _origin
        End Get
        Set(ByVal value As Integer)
            _origin = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGuestID() As Integer
        Get
            Return _guestID
        End Get
        Set(ByVal value As Integer)
            _guestID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCcard_TransID() As Integer
        Get
            Return _ccard_transid
        End Get
        Set(ByVal value As Integer)
            _ccard_transid = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrival() As Date
        Get
            Return _arrival
        End Get
        Set(ByVal value As Date)
            _arrival = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreation_Date() As Date
        Get
            Return _creation_date
        End Get
        Set(ByVal value As Date)
            _creation_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCancellation_Date() As Date
        Get
            Return _cancellation_date
        End Get
        Set(ByVal value As Date)
            _cancellation_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTaxAmount() As Double
        Get
            Return _tax_amount
        End Get
        Set(ByVal value As Double)
            _tax_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount() As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoAmount() As Double
        Get
            Return _promo_amount
        End Get
        Set(ByVal value As Double)
            _promo_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID() As Integer
        Get
            Return _promo_id
        End Get
        Set(ByVal value As Integer)
            _promo_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateID() As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property

    <DataMember()>
    Public Property CardType() As String
        Get
            Return _CardType
        End Get
        Set(ByVal value As String)
            _CardType = value
        End Set
    End Property

    <DataMember()>
    Public Property Processor() As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

End Class


