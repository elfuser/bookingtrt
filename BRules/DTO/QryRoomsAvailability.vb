﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports BRules

<DataContract()>
Public Class QryRoomsAvailability
#Region "Variables privadas"
    Private _loRoomTypes As List(Of RoomsInfo)
    Private _loRoomAvailability As List(Of QryAvailability)
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property pploRoomTypes As List(Of RoomsInfo)
        Get
            Return _loRoomTypes
        End Get
        Set(ByVal value As List(Of RoomsInfo))
            _loRoomTypes = value
        End Set
    End Property

    <DataMember()>
    Public Property pploRoomAvailability As List(Of QryAvailability)
        Get
            Return _loRoomAvailability
        End Get
        Set(ByVal value As List(Of QryAvailability))
            _loRoomAvailability = value
        End Set
    End Property

#End Region
End Class
