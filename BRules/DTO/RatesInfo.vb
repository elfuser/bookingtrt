﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class RatesInfo

#Region "Variables privadas"

    Dim _rateId As Integer
    Dim _rateCode As String
    Dim _description, _type As String
    Dim _conditions As String
    Dim _companyCode As String
    Dim _startDate, _endDate As Date
    Dim _minLos As Integer
    Dim _maxLos As Integer
    Dim _rowOrder As Integer
    Dim _freeNights As Integer
    Dim _enabled As Boolean
    Dim _weekend As Boolean
    Dim _master As Boolean
    Dim _rateDetail As String
    Dim _error As String
    Dim _minValue As Double
    Dim _discount As Double

    Dim _StrStartDate As String
    Dim _StrEndDate As String

    Dim _UnFormatFrom As Date
    Dim _UnFormatTo As Date

#End Region

#Region "Propiedades"


    <DataMember()>
    Public Property UnFormatFrom As Date
        Get
            Return _UnFormatFrom
        End Get
        Set(ByVal value As Date)
            _UnFormatFrom = value
        End Set
    End Property


    <DataMember()>
    Public Property UnFormatTo As Date
        Get
            Return _UnFormatTo
        End Get
        Set(ByVal value As Date)
            _UnFormatTo = value
        End Set
    End Property


    <DataMember()>
    Public Property ppStrStartDate As String
        Get
            Return _StrStartDate
        End Get
        Set(ByVal value As String)
            _StrStartDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStrEndDate As String
        Get
            Return _StrEndDate
        End Get
        Set(ByVal value As String)
            _StrEndDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateId() As Integer
        Get
            Return _rateId
        End Get
        Set(ByVal value As Integer)
            _rateId = value
        End Set
    End Property

    <DataMember()>
    Public Property ppFreeNights() As Integer
        Get
            Return _freeNights
        End Get
        Set(ByVal value As Integer)
            _freeNights = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowOrder() As Integer
        Get
            Return _rowOrder
        End Get
        Set(ByVal value As Integer)
            _rowOrder = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateCode() As String
        Get
            Return _rateCode
        End Get
        Set(ByVal value As String)
            _rateCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppConditions() As String
        Get
            Return _conditions
        End Get
        Set(ByVal value As String)
            _conditions = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode() As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMinLos() As Integer
        Get
            Return _minLos
        End Get
        Set(ByVal value As Integer)
            _minLos = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxLos() As Integer
        Get
            Return _maxLos
        End Get
        Set(ByVal value As Integer)
            _maxLos = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEnabled() As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal value As Boolean)
            _enabled = value
        End Set
    End Property

    <DataMember()>
    Public Property ppWeekend() As Boolean
        Get
            Return _weekend
        End Get
        Set(ByVal value As Boolean)
            _weekend = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaster() As Boolean
        Get
            Return _master
        End Get
        Set(ByVal value As Boolean)
            _master = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateDetail() As String
        Get
            Return _rateDetail
        End Get
        Set(ByVal value As String)
            _rateDetail = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMinValue() As Double
        Get
            Return _minValue
        End Get
        Set(ByVal value As Double)
            _minValue = value
        End Set
    End Property


    <DataMember()>
    Public Property ppDiscount() As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property


#End Region

End Class

