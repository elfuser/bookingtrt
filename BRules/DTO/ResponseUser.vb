﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class ResponseUser

#Region "Variables privadas"

    Private _Status As Integer
    Private _Message As String
    Private _UserName As String
    Private _UserName_Email As String
    Private _UserId As String
    Private _AgencyCode As String
    Private _ProfileId As Integer

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = value
        End Set
    End Property

    <DataMember()>
    Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal value As String)
            _Message = value
        End Set
    End Property

    <DataMember()>
    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = value
        End Set
    End Property

    <DataMember()>
    Public Property UserName_Email() As String
        Get
            Return _UserName_Email
        End Get
        Set(ByVal value As String)
            _UserName_Email = value
        End Set
    End Property

    <DataMember()>
    Public Property UserId() As String
        Get
            Return _UserId
        End Get
        Set(ByVal value As String)
            _UserId = value
        End Set
    End Property

    <DataMember()>
    Public Property AgencyCode() As Integer
        Get
            Return _AgencyCode
        End Get
        Set(ByVal value As Integer)
            _AgencyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ProfileId() As Integer
        Get
            Return _ProfileId
        End Get
        Set(ByVal value As Integer)
            _ProfileId = value
        End Set
    End Property

#End Region

End Class
