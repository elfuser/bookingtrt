﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class clsRoomsInfo
    Private _roomType_id As Integer
    Private _roomType_code As String
    Private _company_code As String
    Private _description As String
    Private _quantity As Int16
    Private _max_adults As Int16
    Private _max_children As Int16
    Private _image_count As Int16
    Private _max_occupance As Int16
    Private _min_occupance As Int16
    Private _room_detail As String
    Private _error As String
    Private _Price As Double
    Private _alignmentclass As String
    Private _ImageUlr As String
    Private _rate_code As String

    <DataMember()>
    Public Property ppRoomTypeID() As Integer
        Get
            Return _roomType_id
        End Get
        Set(ByVal value As Integer)
            _roomType_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeCode() As String
        Get
            Return _roomType_code
        End Get
        Set(ByVal value As String)
            _roomType_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQuantity() As Int16
        Get
            Return _quantity
        End Get
        Set(ByVal value As Int16)
            _quantity = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMax_Adults() As Int16
        Get
            Return _max_adults
        End Get
        Set(ByVal value As Int16)
            _max_adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMax_Children() As Int16
        Get
            Return _max_children
        End Get
        Set(ByVal value As Int16)
            _max_children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppImage_Count() As Int16
        Get
            Return _image_count
        End Get
        Set(ByVal value As Int16)
            _image_count = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom_Detail() As String
        Get
            Return _room_detail
        End Get
        Set(ByVal value As String)
            _room_detail = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMaxOccupance() As Int16
        Get
            Return _max_occupance
        End Get
        Set(ByVal value As Int16)
            _max_occupance = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMinOccupance() As Int16
        Get
            Return _min_occupance
        End Get
        Set(ByVal value As Int16)
            _min_occupance = value
        End Set
    End Property

    <DataMember()>
    Public Property Price() As Double
        Get
            Return _Price
        End Get
        Set(ByVal value As Double)
            _Price = value
        End Set
    End Property


    <DataMember()>
    Public Property Alignmentclass As String
        Get
            Return _alignmentclass
        End Get
        Set(ByVal value As String)
            _alignmentclass = value
        End Set
    End Property


    <DataMember()>
    Public Property ImageUlr As String
        Get
            Return _ImageUlr
        End Get
        Set(ByVal value As String)
            _ImageUlr = value
        End Set
    End Property

    <DataMember()>
    Public Property RateCode As String
        Get
            Return _rate_code
        End Get
        Set(ByVal value As String)
            _rate_code = value
        End Set
    End Property





End Class
