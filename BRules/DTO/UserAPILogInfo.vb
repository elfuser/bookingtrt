﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class UserAPILogInfo
#Region "Variables privadas"
    Private _user_id As String
    Private _event As String
    Private _received As String
    Private _response As String
    Private _event_date As DateTime
    Private _error As String


#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppUserID As String
        Get
            Return _user_id
        End Get
        Set(ByVal value As String)
            _user_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppReceived As String
        Get
            Return _received
        End Get
        Set(ByVal value As String)
            _received = value
        End Set
    End Property

    <DataMember()>
    Public Property ppResponse As String
        Get
            Return _response
        End Get
        Set(ByVal value As String)
            _response = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEvent As String
        Get
            Return _event
        End Get
        Set(ByVal value As String)
            _event = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEventDate As DateTime
        Get
            Return _event_date
        End Get
        Set(ByVal value As DateTime)
            _event_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region
End Class
