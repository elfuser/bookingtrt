﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsDashSalesByDay

#Region "Variables privadas"
    Dim _date As Date
    Dim _amount As Double
    Dim _qty As Integer

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ddate() As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

#End Region

End Class
