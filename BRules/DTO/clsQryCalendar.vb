﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

Public Class clsQryCalendar

#Region "Variables privadas"
    Private _room As String
    Private _roomtypeCode As String
    Private _day1, _day2, _day3, _day4, _day5, _day6, _day7, _day8, _day9, _day10, _day11, _day12, _day13 As Integer
    Private _day14, _day15, _day16, _day17, _day18, _day19, _day20, _day21, _day22, _day23, _day24 As Integer
    Private _day25, _day26, _day27, _day28, _day29, _day30, _day31 As Integer
    Private _date As Date
    Private _type As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppRoomTypeCode As String
        Get
            Return _roomtypeCode
        End Get
        Set(ByVal value As String)
            _roomtypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoom As String
        Get
            Return _room
        End Get
        Set(ByVal value As String)
            _room = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDate As Date
        Get
            Return _date
        End Get
        Set(ByVal value As Date)
            _date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay1 As Integer
        Get
            Return _day1
        End Get
        Set(ByVal value As Integer)
            _day1 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay2 As Integer
        Get
            Return _day2
        End Get
        Set(ByVal value As Integer)
            _day2 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay3 As Integer
        Get
            Return _day3
        End Get
        Set(ByVal value As Integer)
            _day3 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay4 As Integer
        Get
            Return _day4
        End Get
        Set(ByVal value As Integer)
            _day4 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay5 As Integer
        Get
            Return _day5
        End Get
        Set(ByVal value As Integer)
            _day5 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay6 As Integer
        Get
            Return _day6
        End Get
        Set(ByVal value As Integer)
            _day6 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay7 As Integer
        Get
            Return _day7
        End Get
        Set(ByVal value As Integer)
            _day7 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay8 As Integer
        Get
            Return _day8
        End Get
        Set(ByVal value As Integer)
            _day8 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay9 As Integer
        Get
            Return _day9
        End Get
        Set(ByVal value As Integer)
            _day9 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay10 As Integer
        Get
            Return _day10
        End Get
        Set(ByVal value As Integer)
            _day10 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay11 As Integer
        Get
            Return _day11
        End Get
        Set(ByVal value As Integer)
            _day11 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay12 As Integer
        Get
            Return _day12
        End Get
        Set(ByVal value As Integer)
            _day12 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay13 As Integer
        Get
            Return _day13
        End Get
        Set(ByVal value As Integer)
            _day13 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay14 As Integer
        Get
            Return _day14
        End Get
        Set(ByVal value As Integer)
            _day14 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay15 As Integer
        Get
            Return _day15
        End Get
        Set(ByVal value As Integer)
            _day15 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay16 As Integer
        Get
            Return _day16
        End Get
        Set(ByVal value As Integer)
            _day16 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay17 As Integer
        Get
            Return _day17
        End Get
        Set(ByVal value As Integer)
            _day17 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay18 As Integer
        Get
            Return _day18
        End Get
        Set(ByVal value As Integer)
            _day18 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay19 As Integer
        Get
            Return _day19
        End Get
        Set(ByVal value As Integer)
            _day19 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay20 As Integer
        Get
            Return _day20
        End Get
        Set(ByVal value As Integer)
            _day20 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay21 As Integer
        Get
            Return _day21
        End Get
        Set(ByVal value As Integer)
            _day21 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay22 As Integer
        Get
            Return _day22
        End Get
        Set(ByVal value As Integer)
            _day22 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay23 As Integer
        Get
            Return _day23
        End Get
        Set(ByVal value As Integer)
            _day23 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay24 As Integer
        Get
            Return _day24
        End Get
        Set(ByVal value As Integer)
            _day24 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay25 As Integer
        Get
            Return _day25
        End Get
        Set(ByVal value As Integer)
            _day25 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay26 As Integer
        Get
            Return _day26
        End Get
        Set(ByVal value As Integer)
            _day26 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay27 As Integer
        Get
            Return _day27
        End Get
        Set(ByVal value As Integer)
            _day27 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay28 As Integer
        Get
            Return _day28
        End Get
        Set(ByVal value As Integer)
            _day28 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay29 As Integer
        Get
            Return _day29
        End Get
        Set(ByVal value As Integer)
            _day29 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay30 As Integer
        Get
            Return _day30
        End Get
        Set(ByVal value As Integer)
            _day30 = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDay31 As Integer
        Get
            Return _day31
        End Get
        Set(ByVal value As Integer)
            _day31 = value
        End Set
    End Property
#End Region

End Class
