﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class clsPromoCodeRateInfo
#Region "Variables privadas"
    Private _promoID, _serviceID As Integer
    Private _rateID As Integer
    Private _amount As Double
    Private _description, _error, _type, _promoCode, _serviceDesc As String
#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppType As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRateID As Integer
        Get
            Return _rateID
        End Get
        Set(ByVal value As Integer)
            _rateID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID As Integer
        Get
            Return _promoID
        End Get
        Set(ByVal value As Integer)
            _promoID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoCode As String
        Get
            Return _promoCode
        End Get
        Set(ByVal value As String)
            _promoCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceDesc As String
        Get
            Return _serviceDesc
        End Get
        Set(ByVal value As String)
            _serviceDesc = value
        End Set
    End Property
#End Region

End Class

