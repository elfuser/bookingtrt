﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

Public Class RoomBlocked

#Region "Variables privadas"

    Private _companyCode As String
    Private _roomtypeCode As String
    Private _roomname As String
    Private _startDate As Date
    Private _endDate As Date
    Private _qty As Integer
    Private _error As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomTypeCode As String
        Get
            Return _roomtypeCode
        End Get
        Set(ByVal value As String)
            _roomtypeCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomName As String
        Get
            Return _roomname
        End Get
        Set(ByVal value As String)
            _roomname = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStartDate As Date
        Get
            Return _startDate
        End Get
        Set(ByVal value As Date)
            _startDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEndDate As Date
        Get
            Return _endDate
        End Get
        Set(ByVal value As Date)
            _endDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQuantity As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property

#End Region

End Class
