﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsRoomFeaturesInfo
    Private _roomtypeID As Integer
    Private _description As String
    Private _feature_code As String
    Private _company_code As String
    Private _roomType_code As String

    <DataMember()>
    Public Property ppRoomTypeCode() As String
        Get
            Return _roomType_code
        End Get
        Set(ByVal value As String)
            _roomType_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppFeatureCode() As String
        Get
            Return _feature_code
        End Get
        Set(ByVal value As String)
            _feature_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRoomtypeID() As Integer
        Get
            Return _roomtypeID
        End Get
        Set(ByVal value As Integer)
            _roomtypeID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property
End Class
