﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class ServiceInventoryInfo
#Region "Variables privadas"
    Private _scheduleID, _serviceID, _qty As Integer
    Private _hour, _error As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppHour As String
        Get
            Return _hour
        End Get
        Set(ByVal value As String)
            _hour = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppScheduleID As Integer
        Get
            Return _scheduleID
        End Get
        Set(ByVal value As Integer)
            _scheduleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQty As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            _qty = value
        End Set
    End Property
#End Region
End Class
