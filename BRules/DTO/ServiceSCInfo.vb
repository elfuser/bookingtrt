﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class ServiceSCInfo
#Region "Variables privadas"
    Private _rowID, _serviceID, _qtyAdults, _qtyChildren, _qtyInfants, _qtyStudents, _ppScheduleID As Integer
    Private _description, _checkIn, _hotel, _hotelPickup, _error As String
    Private _adults, _children, _infants, _students, _total As Double
    Private _arrivalDate As Date

    Public Property ppHotelPickUpID As Integer
    Public Property ppHotelPickupHourID As Integer
    Public Property ppPriceAdults As Decimal
    Public Property ppPriceChildren As Decimal
    Public Property ppPriceInfants As Decimal
    Public Property ppPriceStudent As Decimal
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppArrivalDate As Date
        Get
            Return _arrivalDate
        End Get
        Set(ByVal value As Date)
            _arrivalDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowID As Integer
        Get
            Return _rowID
        End Get
        Set(ByVal value As Integer)
            _rowID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyAdults As Integer
        Get
            Return _qtyAdults
        End Get
        Set(ByVal value As Integer)
            _qtyAdults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyChildren As Integer
        Get
            Return _qtyChildren
        End Get
        Set(ByVal value As Integer)
            _qtyChildren = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyInfants As Integer
        Get
            Return _qtyInfants
        End Get
        Set(ByVal value As Integer)
            _qtyInfants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppQtyStudents As Integer
        Get
            Return _qtyStudents
        End Get
        Set(ByVal value As Integer)
            _qtyStudents = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCheckIn As String
        Get
            Return _checkIn
        End Get
        Set(ByVal value As String)
            _checkIn = value
        End Set
    End Property

    <DataMember()>
    Public Property ppHotel As String
        Get
            Return _hotel
        End Get
        Set(ByVal value As String)
            _hotel = value
        End Set
    End Property

    <DataMember()>
    Public Property ppHotelPickup As String
        Get
            Return _hotelPickup
        End Get
        Set(ByVal value As String)
            _hotelPickup = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Double
        Get
            Return _adults
        End Get
        Set(ByVal value As Double)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Double
        Get
            Return _children
        End Get
        Set(ByVal value As Double)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Double
        Get
            Return _infants
        End Get
        Set(ByVal value As Double)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudents As Double
        Get
            Return _students
        End Get
        Set(ByVal value As Double)
            _students = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTotal As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property ppScheduleID As Integer
        Get
            Return _ppScheduleID
        End Get
        Set(ByVal value As Integer)
            _ppScheduleID = value
        End Set
    End Property
#End Region
End Class
