﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class UserAPIInfo
#Region "Variables privadas"
    Private _user_id As String
    Private _company_code As String
    Private _company As String
    Private _user_name As String
    Private _password As String
    Private _active As Boolean
    Private _last_login As DateTime
    Private _error As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppCompany As String
        Get
            Return _company
        End Get
        Set(ByVal value As String)
            _company = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUserID As String
        Get
            Return _user_id
        End Get
        Set(ByVal value As String)
            _user_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppUserName As String
        Get
            Return _user_name
        End Get
        Set(ByVal value As String)
            _user_name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPassword As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    <DataMember()>
    Public Property ppActive As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLastLogin As DateTime
        Get
            Return _last_login
        End Get
        Set(ByVal value As DateTime)
            _last_login = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property
#End Region
End Class
