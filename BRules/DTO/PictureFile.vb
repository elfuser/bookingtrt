﻿Imports System.Runtime.Serialization


Public Class PictureFile
    Private m_PictureStream As Byte()
    Private m_RoomTypeID As Integer
    Private m_PictureNumber As Integer
    Private m_CompanyCode As String
    Private m_ServiceID As Integer

    <DataMember>
    Public Property PictureStream() As Byte()
        Get
            Return m_PictureStream
        End Get
        Set(value As Byte())
            m_PictureStream = value
        End Set
    End Property

    <DataMember>
    Public Property RoomTypeID() As Integer
        Get
            Return m_RoomTypeID
        End Get
        Set(value As Integer)
            m_RoomTypeID = value
        End Set
    End Property

    <DataMember>
    Public Property ServiceID() As Integer
        Get
            Return m_ServiceID
        End Get
        Set(value As Integer)
            m_ServiceID = value
        End Set
    End Property

    <DataMember>
    Public Property PictureNumber() As Integer
        Get
            Return m_PictureNumber
        End Get
        Set(value As Integer)
            m_PictureNumber = value
        End Set
    End Property

    <DataMember>
    Public Property CompanyCode() As String
        Get
            Return m_CompanyCode
        End Get
        Set(value As String)
            m_CompanyCode = value
        End Set
    End Property
End Class
