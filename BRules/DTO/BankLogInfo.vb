﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class BankLogInfo
#Region "Variables privadas"
    Private _response, _transactionID As String
    Private _amount As Double
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppResponse As String
        Get
            Return _response
        End Get
        Set(ByVal value As String)
            _response = value
        End Set
    End Property

    <DataMember()>
    Public Property ppTransactionID As String
        Get
            Return _transactionID
        End Get
        Set(ByVal value As String)
            _transactionID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAmount As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property
#End Region
End Class
