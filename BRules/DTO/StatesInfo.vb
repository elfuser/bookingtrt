﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel
<DataContract()>
Public Class StatesInfo
#Region "Variables privadas"
    Private _Name As String
    Private _stateCode As String
    Private _countryIso3 As String
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property ppName() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStateCode() As String
        Get
            Return _stateCode
        End Get
        Set(ByVal value As String)
            _stateCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCountryIso3() As String
        Get
            Return _countryIso3
        End Get
        Set(ByVal value As String)
            _countryIso3 = value
        End Set
    End Property
#End Region
End Class
