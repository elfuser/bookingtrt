﻿


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsCCardInfo
    Private _cardCode As String
    Private _reservationID As String
    Private _cardNumber As String
    Private _cardExp As String
    Private _authNr As String
    Private _approbCode As String
    Private _nameCard As String

    <DataMember()>
    Public Property ppCardCode As String
        Get
            Return _cardCode
        End Get
        Set(ByVal value As String)
            _cardCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppReservationID As String
        Get
            Return _reservationID
        End Get
        Set(ByVal value As String)
            _reservationID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCardNumber As String
        Get
            Return _cardNumber
        End Get
        Set(ByVal value As String)
            _cardNumber = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCardExp As String
        Get
            Return _cardExp
        End Get
        Set(ByVal value As String)
            _cardExp = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAuthNr As String
        Get
            Return _authNr
        End Get
        Set(ByVal value As String)
            _authNr = value
        End Set
    End Property

    <DataMember()>
    Public Property ppApprobCode As String
        Get
            Return _approbCode
        End Get
        Set(ByVal value As String)
            _approbCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppNameCard As String
        Get
            Return _nameCard
        End Get
        Set(ByVal value As String)
            _nameCard = value
        End Set
    End Property
End Class
