﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsSaleItemInfo
    Private _saleID, _description, _error, _schedule, _conditions, _bringInfo As String
    Private _serviceID, _scheduleID As Integer
    Private _rowID As Long
    Private _adult_qty, _children_qty, _infants_qty, _students_qty As Integer
    Private _adult_amount, _children_amount, _infants_amount, _students_amount, _discount As Double
    Private _arrival As Date

    <DataMember()>
    Public Property ppSaleID() As String
        Get
            Return _saleID
        End Get
        Set(ByVal value As String)
            _saleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppConditions() As String
        Get
            Return _conditions
        End Get
        Set(ByVal value As String)
            _conditions = value
        End Set
    End Property

    <DataMember()>
    Public Property ppBringInfo() As String
        Get
            Return _bringInfo
        End Get
        Set(ByVal value As String)
            _bringInfo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrival() As Date
        Get
            Return _arrival
        End Get
        Set(ByVal value As Date)
            _arrival = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDescription() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServiceID() As Integer
        Get
            Return _serviceID
        End Get
        Set(ByVal value As Integer)
            _serviceID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppScheduleID() As Integer
        Get
            Return _scheduleID
        End Get
        Set(ByVal value As Integer)
            _scheduleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSchedule() As String
        Get
            Return _schedule
        End Get
        Set(ByVal value As String)
            _schedule = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowID() As Long
        Get
            Return _rowID
        End Get
        Set(ByVal value As Long)
            _rowID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdultQty() As Integer
        Get
            Return _adult_qty
        End Get
        Set(ByVal value As Integer)
            _adult_qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildrenQty() As Integer
        Get
            Return _children_qty
        End Get
        Set(ByVal value As Integer)
            _children_qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfantsQty() As Integer
        Get
            Return _infants_qty
        End Get
        Set(ByVal value As Integer)
            _infants_qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudentsQty() As Integer
        Get
            Return _students_qty
        End Get
        Set(ByVal value As Integer)
            _students_qty = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdultAmount() As Double
        Get
            Return _adult_amount
        End Get
        Set(ByVal value As Double)
            _adult_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildrenAmount() As Double
        Get
            Return _children_amount
        End Get
        Set(ByVal value As Double)
            _children_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfantsAmount() As Double
        Get
            Return _infants_amount
        End Get
        Set(ByVal value As Double)
            _infants_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudentsAmount() As Double
        Get
            Return _students_amount
        End Get
        Set(ByVal value As Double)
            _students_amount = value
        End Set
    End Property

    <DataMember()>
    Public Property ppDiscount() As Double
        Get
            Return _discount
        End Get
        Set(ByVal value As Double)
            _discount = value
        End Set
    End Property
End Class

