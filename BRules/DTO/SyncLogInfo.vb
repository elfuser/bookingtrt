﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class SyncLogInfo
#Region "Variables privadas"
    Private _company_code, _message, _ipAddress As String
    Private _logLevel As Integer
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppCompanyCode() As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppMessage() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property

    <DataMember()>
    Public Property ppIpAddress() As String
        Get
            Return _ipAddress
        End Get
        Set(ByVal value As String)
            _ipAddress = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLogLevel() As Integer
        Get
            Return _logLevel
        End Get
        Set(ByVal value As Integer)
            _logLevel = value
        End Set
    End Property
#End Region
End Class
