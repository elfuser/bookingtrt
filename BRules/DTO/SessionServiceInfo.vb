﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class SessionServiceInfo
#Region "Variables privadas"
    Private _companyCode, _sessionID, _resellerID, _saleID, _PayPalToken, _lang, _keyID As String
    Private _arrivalDate, _created As Date
    Private _services, _promoID, _adults, _children, _infants, _students As Integer
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppKeyID As String
        Get
            Return _keyID
        End Get
        Set(ByVal value As String)
            _keyID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSaleID As String
        Get
            Return _saleID
        End Get
        Set(ByVal value As String)
            _saleID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLanguage As String
        Get
            Return _lang
        End Get
        Set(ByVal value As String)
            _lang = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPayPalToken As String
        Get
            Return _PayPalToken
        End Get
        Set(ByVal value As String)
            _PayPalToken = value
        End Set
    End Property

    <DataMember()>
    Public Property ppSessionID As String
        Get
            Return _sessionID
        End Get
        Set(ByVal value As String)
            _sessionID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppResellerID As String
        Get
            Return _resellerID
        End Get
        Set(ByVal value As String)
            _resellerID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppArrivalDate As Date
        Get
            Return _arrivalDate
        End Get
        Set(ByVal value As Date)
            _arrivalDate = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCreated As Date
        Get
            Return _created
        End Get
        Set(ByVal value As Date)
            _created = value
        End Set
    End Property

    <DataMember()>
    Public Property ppServices As Integer
        Get
            Return _services
        End Get
        Set(ByVal value As Integer)
            _services = value
        End Set
    End Property

    <DataMember()>
    Public Property ppPromoID As Integer
        Get
            Return _promoID
        End Get
        Set(ByVal value As Integer)
            _promoID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults As Integer
        Get
            Return _adults
        End Get
        Set(ByVal value As Integer)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren As Integer
        Get
            Return _children
        End Get
        Set(ByVal value As Integer)
            _children = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInfants As Integer
        Get
            Return _infants
        End Get
        Set(ByVal value As Integer)
            _infants = value
        End Set
    End Property

    <DataMember()>
    Public Property ppStudents As Integer
        Get
            Return _students
        End Get
        Set(ByVal value As Integer)
            _students = value
        End Set
    End Property
#End Region
End Class
