﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class UserLogInfo
#Region "Variables privadas"
    Private _user_id As Integer
    Private _company_code As String
    Private _LogID As Long
    Private _ref_id As Long
    Private _event As String
    Private _event_date As DateTime
    Private _error As String
    Private _FormatDate As String
    Private _AgencyCode As String
#End Region
#Region "Propiedades"
    <DataMember()>
    Public Property ppUserID As Integer
        Get
            Return _user_id
        End Get
        Set(ByVal value As Integer)
            _user_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppCompanyCode As String
        Get
            Return _company_code
        End Get
        Set(ByVal value As String)
            _company_code = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLogID As Long
        Get
            Return _LogID
        End Get
        Set(ByVal value As Long)
            _LogID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRefID As Long
        Get
            Return _ref_id
        End Get
        Set(ByVal value As Long)
            _ref_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEvent As String
        Get
            Return _event
        End Get
        Set(ByVal value As String)
            _event = value
        End Set
    End Property

    <DataMember()>
    Public Property ppEventDate As DateTime
        Get
            Return _event_date
        End Get
        Set(ByVal value As DateTime)
            _event_date = value
        End Set
    End Property

    <DataMember()>
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()>
    Public Property FormatDate As String
        Get
            Return _FormatDate
        End Get
        Set(ByVal value As String)
            _FormatDate = value
        End Set
    End Property


    <DataMember()>
    Public Property AgencyCode As String
        Get
            Return _AgencyCode
        End Get
        Set(ByVal value As String)
            _AgencyCode = value
        End Set
    End Property


#End Region
End Class
