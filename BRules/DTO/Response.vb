﻿

Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel


<DataContract()>
Public Class Response

#Region "Variables privadas"

    Private _Status As Integer
    Private _Message As String

#End Region

#Region "Propiedades"

    <DataMember()>
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set
            _Status = Value
        End Set
    End Property

    <DataMember()>
        Public Property Message() As [String]
            Get
                Return _Message
            End Get
            Set
                _Message = Value
            End Set
        End Property

#End Region

End Class
