﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class clsDashSalesToday

#Region "Variables privadas"
    Dim _guest As String
    Dim _amount As Double
#End Region

#Region "Propiedades"
    <DataMember()>
    Public Property guest() As String
        Get
            Return _guest
        End Get
        Set(ByVal value As String)
            _guest = value
        End Set
    End Property

    <DataMember()>
    Public Property amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal value As Double)
            _amount = value
        End Set
    End Property
#End Region

End Class
