﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

Public Class CombinationInfo

#Region "Variables privadas"
    Private _rowID As Long
    Private _adults As Integer
    Private _children As Integer
    Private _CompanyCode As String
#End Region

#Region "Propiedades"



    <DataMember()>
    Public Property ppCompanyCode() As String
        Get
            Return _CompanyCode
        End Get
        Set(ByVal value As String)
            _CompanyCode = value
        End Set
    End Property

    <DataMember()>
    Public Property ppRowID() As Long
        Get
            Return _rowID
        End Get
        Set(ByVal value As Long)
            _rowID = value
        End Set
    End Property

    <DataMember()>
    Public Property ppAdults() As Integer
        Get
            Return _adults
        End Get
        Set(ByVal value As Integer)
            _adults = value
        End Set
    End Property

    <DataMember()>
    Public Property ppChildren() As Integer
        Get
            Return _children
        End Get
        Set(ByVal value As Integer)
            _children = value
        End Set
    End Property

#End Region

End Class
