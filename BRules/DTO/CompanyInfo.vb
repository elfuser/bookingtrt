﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.Serialization
Imports System.ServiceModel

<DataContract()>
Public Class CompanyInfo
#Region "Variables privadas"

    Private _companyCode As String
    Private _companyName, _legalName As String
    Private _legalID As String
    Private _creationDate As Date
    Private _address As String
    Private _tel1, _tel2, _tel3 As String
    Private _fax, _keyID, _keyHash, _type, _url, _userName, _processorID As String
    Private _contactName, _contactEmail, _cfoName, _cfoEmail, _resName, _resEmail As String
    Private _web, _email, _emailHeader, _email_alt As String
    Private _confirmPrefix, _bankName, _bankAccount As String
    Private _facebookURL, _twitterText, _Pinterest, _Instagram, _Blog, _GooglePlus As String
    Private _reservationNr As Long
    Private _guestNr As Long
    Private _taxPerc, _commission, _rentPerc As Double
    Private _callTollFree As String
    Private _maxAdults, _maxChildren, _maxStudent, _addons As Integer
    Private _msgAdults, _msgChildren, _msgChildrenFree, _msgStudent, _childrenNote As String
    Private _childrenFree, _credomatic, _bancoNacional, _payPal As Boolean
    Private _active As Boolean
    Private _spanish, _hotel, _services, _migrar As Boolean
    Private _error As String
    Private _googleID, _google_conv_id, _google_conv_label, _descriptor, _terms, _waiver As String
    Private _Processor As String
    Private _EnglishUrl As String
    Private _SpanishUrl As String
    Private _cultureid As String
    Private _termsid As String
    Private _provincia As String
    Private _canton As String
    Private _distrito As String
    Private _TechContact As String
    Private _TechEmail As String
    Private _ThingsToDo As String
    Private _GetThere As String
    Private _latitude As String
    Private _longitude As String

#End Region
#Region "Propiedades"

    <DataMember()>
    Public Property TechContact() As String
        Get
            Return _TechContact
        End Get
        Set(ByVal value As String)
            _TechContact = value
        End Set
    End Property


    <DataMember()>
    Public Property TechEmail() As String
        Get
            Return _TechEmail
        End Get
        Set(ByVal value As String)
            _TechEmail = value
        End Set
    End Property

    <DataMember()>
    Public Property TermsId() As String
        Get
            Return _termsid
        End Get
        Set(ByVal value As String)
            _termsid = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppUserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppURL() As String
        Get
            Return _url
        End Get
        Set(ByVal value As String)
            _url = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppType() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppKeyHash() As String
        Get
            Return _keyHash
        End Get
        Set(ByVal value As String)
            _keyHash = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppKeyID() As String
        Get
            Return _keyID
        End Get
        Set(ByVal value As String)
            _keyID = value
        End Set
    End Property

    <DataMember()>
    Public Property Processor() As String
        Get
            Return _Processor
        End Get
        Set(ByVal value As String)
            _Processor = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppProcessorID() As String
        Get
            Return _processorID
        End Get
        Set(ByVal value As String)
            _processorID = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppSpanish() As Boolean
        Get
            Return _spanish
        End Get
        Set(ByVal value As Boolean)
            _spanish = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMigrar() As Boolean
        Get
            Return _migrar
        End Get
        Set(ByVal value As Boolean)
            _migrar = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppHotel() As Boolean
        Get
            Return _hotel
        End Get
        Set(ByVal value As Boolean)
            _hotel = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppServices() As Boolean
        Get
            Return _services
        End Get
        Set(ByVal value As Boolean)
            _services = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppPayPal() As Boolean
        Get
            Return _payPal
        End Get
        Set(ByVal value As Boolean)
            _payPal = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCredomatic() As Boolean
        Get
            Return _credomatic
        End Get
        Set(ByVal value As Boolean)
            _credomatic = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppBancoNacional() As Boolean
        Get
            Return _bancoNacional
        End Get
        Set(ByVal value As Boolean)
            _bancoNacional = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppLegalName As String
        Get
            Return _legalName
        End Get
        Set(ByVal value As String)
            _legalName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTerms As String
        Get
            Return _terms
        End Get
        Set(ByVal value As String)
            _terms = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppWaiver As String
        Get
            Return _waiver
        End Get
        Set(ByVal value As String)
            _waiver = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppDescriptor As String
        Get
            Return _descriptor
        End Get
        Set(ByVal value As String)
            _descriptor = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppChildrenNote As String
        Get
            Return _childrenNote
        End Get
        Set(ByVal value As String)
            _childrenNote = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTwitterText As String
        Get
            Return _twitterText
        End Get
        Set(ByVal value As String)
            _twitterText = value
        End Set
    End Property

    <DataMember()>
    Public Property ppFacebookURL As String
        Get
            Return _facebookURL
        End Get
        Set(ByVal value As String)
            _facebookURL = value
        End Set
    End Property


    <DataMember()>
    Public Property ppPinterest As String
        Get
            Return _Pinterest
        End Get
        Set(ByVal value As String)
            _Pinterest = value
        End Set
    End Property

    <DataMember()>
    Public Property ppInstagram As String
        Get
            Return _Instagram
        End Get
        Set(ByVal value As String)
            _Instagram = value
        End Set
    End Property

    <DataMember()>
    Public Property ppBlog As String
        Get
            Return _Blog
        End Get
        Set(ByVal value As String)
            _Blog = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGooglePlus As String
        Get
            Return _GooglePlus
        End Get
        Set(ByVal value As String)
            _GooglePlus = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppBankName As String
        Get
            Return _bankName
        End Get
        Set(ByVal value As String)
            _bankName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppBankAccount As String
        Get
            Return _bankAccount
        End Get
        Set(ByVal value As String)
            _bankAccount = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppResEmailAlt As String
        Get
            Return _email_alt
        End Get
        Set(ByVal value As String)
            _email_alt = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppResEmail As String
        Get
            Return _resEmail
        End Get
        Set(ByVal value As String)
            _resEmail = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppResName As String
        Get
            Return _resName
        End Get
        Set(ByVal value As String)
            _resName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCfoEmail As String
        Get
            Return _cfoEmail
        End Get
        Set(ByVal value As String)
            _cfoEmail = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCfoName As String
        Get
            Return _cfoName
        End Get
        Set(ByVal value As String)
            _cfoName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppError As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppEmailHeader As String
        Get
            Return _emailHeader
        End Get
        Set(ByVal value As String)
            _emailHeader = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppActive() As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCompanyCode() As String
        Get
            Return _companyCode
        End Get
        Set(ByVal value As String)
            _companyCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCompanyName() As String
        Get
            Return _companyName
        End Get
        Set(ByVal value As String)
            _companyName = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppLegalID() As String
        Get
            Return _legalID
        End Get
        Set(ByVal value As String)
            _legalID = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCreationDate() As Date
        Get
            Return _creationDate
        End Get
        Set(ByVal value As Date)
            _creationDate = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppAddress() As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTel1() As String
        Get
            Return _tel1
        End Get
        Set(ByVal value As String)
            _tel1 = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTel2() As String
        Get
            Return _tel2
        End Get
        Set(ByVal value As String)
            _tel2 = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTel3() As String
        Get
            Return _tel3
        End Get
        Set(ByVal value As String)
            _tel3 = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppFax() As String
        Get
            Return _fax
        End Get
        Set(ByVal value As String)
            _fax = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppContactName() As String
        Get
            Return _contactName
        End Get
        Set(ByVal value As String)
            _contactName = value
        End Set
    End Property


    <DataMember()> _
    Public Property ppContactEmail() As String
        Get
            Return _contactEmail
        End Get
        Set(ByVal value As String)
            _contactEmail = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppWeb() As String
        Get
            Return _web
        End Get
        Set(ByVal value As String)
            _web = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppEmail() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppConfirmPrefix() As String
        Get
            Return _confirmPrefix
        End Get
        Set(ByVal value As String)
            _confirmPrefix = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppReservationNr() As Long
        Get
            Return _reservationNr
        End Get
        Set(ByVal value As Long)
            _reservationNr = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCommission() As Double
        Get
            Return _commission
        End Get
        Set(ByVal value As Double)
            _commission = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppRentPorc() As Double
        Get
            Return _rentPerc
        End Get
        Set(ByVal value As Double)
            _rentPerc = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppGuestNr() As Long
        Get
            Return _guestNr
        End Get
        Set(ByVal value As Long)
            _guestNr = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppTaxPerc() As Double
        Get
            Return _taxPerc
        End Get
        Set(ByVal value As Double)
            _taxPerc = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppCallTollFree() As String
        Get
            Return _callTollFree
        End Get
        Set(ByVal value As String)
            _callTollFree = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppAddons() As Integer
        Get
            Return _addons
        End Get
        Set(ByVal value As Integer)
            _addons = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMaxAdults() As Integer
        Get
            Return _maxAdults
        End Get
        Set(ByVal value As Integer)
            _maxAdults = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMaxChildren() As Integer
        Get
            Return _maxChildren
        End Get
        Set(ByVal value As Integer)
            _maxChildren = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMaxStudent() As Integer
        Get
            Return _maxStudent
        End Get
        Set(ByVal value As Integer)
            _maxStudent = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMsgAdults As String
        Get
            Return _msgAdults
        End Get
        Set(ByVal value As String)
            _msgAdults = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMsgStudent As String
        Get
            Return _msgStudent
        End Get
        Set(ByVal value As String)
            _msgStudent = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMsgChildren As String
        Get
            Return _msgChildren
        End Get
        Set(ByVal value As String)
            _msgChildren = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppMsgChildrenFree As String
        Get
            Return _msgChildrenFree
        End Get
        Set(ByVal value As String)
            _msgChildrenFree = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppChildrenFree As Boolean
        Get
            Return _childrenFree
        End Get
        Set(ByVal value As Boolean)
            _childrenFree = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppGoogleID As String
        Get
            Return _googleID
        End Get
        Set(ByVal value As String)
            _googleID = value
        End Set
    End Property

    <DataMember()> _
    Public Property ppGoogleConvID As String
        Get
            Return _google_conv_id
        End Get
        Set(ByVal value As String)
            _google_conv_id = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGoogleConvLabel As String
        Get
            Return _google_conv_label
        End Get
        Set(ByVal value As String)
            _google_conv_label = value
        End Set
    End Property


    <DataMember()>
    Public Property EnglishUrl As String
        Get
            Return _EnglishUrl
        End Get
        Set(ByVal value As String)
            _EnglishUrl = value
        End Set
    End Property


    <DataMember()>
    Public Property SpanishUrl As String
        Get
            Return _SpanishUrl
        End Get
        Set(ByVal value As String)
            _SpanishUrl = value
        End Set
    End Property

    <DataMember()>
    Public Property CultureId As String
        Get
            Return _cultureid
        End Get
        Set(ByVal value As String)
            _cultureid = value
        End Set
    End Property

    <DataMember()>
    Public Property provincia As String
        Get
            Return _provincia
        End Get
        Set(ByVal value As String)
            _provincia = value
        End Set
    End Property

    <DataMember()>
    Public Property canton As String
        Get
            Return _canton
        End Get
        Set(ByVal value As String)
            _canton = value
        End Set
    End Property

    <DataMember()>
    Public Property distrito As String
        Get
            Return _distrito
        End Get
        Set(ByVal value As String)
            _distrito = value
        End Set
    End Property
    <DataMember()>
    Public Property ppThingsToDo() As String
        Get
            Return _ThingsToDo
        End Get
        Set(ByVal value As String)
            _ThingsToDo = value
        End Set
    End Property

    <DataMember()>
    Public Property ppGetThere() As String
        Get
            Return _GetThere
        End Get
        Set(ByVal value As String)
            _GetThere = value
        End Set
    End Property

    <DataMember()>
    Public Property ppLatitude() As String
        Get
            Return _latitude
        End Get
        Set(ByVal value As String)
            _latitude = value
        End Set
    End Property


    <DataMember()>
    Public Property ppLongitude() As String
        Get
            Return _longitude
        End Get
        Set(ByVal value As String)
            _longitude = value
        End Set
    End Property



#End Region
End Class
