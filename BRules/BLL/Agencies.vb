﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Agencies

#Region "Public Methods"

    Public Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of CultureInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetCulture(con, iCultureID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function




    Public Function GetAgencies(ByVal AgencyCode As Integer, ByVal CompanyCode As String) As List(Of AgencyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of AgencyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetAgencies(con, AgencyCode, CompanyCode)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencies(con As SqlConnection, ByVal AgencyCode As Integer, ByVal CompanyCode As String) As List(Of AgencyInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim InfoList As New List(Of AgencyInfo)()
        Dim objInfo As New AgencyInfo
        Dim dtCreated As New Date
        Try

            With cmd
                .CommandText = "spAgency"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@agency_code", AgencyCode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objInfo = New AgencyInfo
                With objInfo
                    .ppAgencyCode = reader.Item("agency_code")
                    .ppCompanyCode = reader.Item("company_code")
                    .ppAgencyName = reader.Item("name")
                    .ppAgencyTypeCode = reader.Item("agency_type_code")
                    .ppCountry = IIf(IsDBNull(reader.Item("country")), String.Empty, reader.Item("country"))
                    .ppProvince = IIf(IsDBNull(reader.Item("province")), String.Empty, reader.Item("province"))
                    .ppCanton = IIf(IsDBNull(reader.Item("canton")), String.Empty, reader.Item("canton"))
                    .ppDistrict = IIf(IsDBNull(reader.Item("district")), String.Empty, reader.Item("district"))
                    .ppAddress = IIf(IsDBNull(reader.Item("address")), String.Empty, reader.Item("address"))
                    .ppPhone = IIf(IsDBNull(reader.Item("phone")), String.Empty, reader.Item("phone"))
                    .ppContactName = IIf(IsDBNull(reader.Item("contact_name")), String.Empty, reader.Item("contact_name"))
                    .ppContactOccupation = IIf(IsDBNull(reader.Item("contact_occupation")), String.Empty, reader.Item("contact_occupation"))
                    .ppContactEmail = IIf(IsDBNull(reader.Item("contact_email")), String.Empty, reader.Item("contact_email"))
                    .ppContactOfficePhone = IIf(IsDBNull(reader.Item("contact_office_phone")), String.Empty, reader.Item("contact_office_phone"))
                    .ppContactMobilePhone = IIf(IsDBNull(reader.Item("contact_mobile_phone")), String.Empty, reader.Item("contact_mobile_phone"))
                    .ppNotificationEmailReservations = IIf(IsDBNull(reader.Item("notification_emails_reservations")), String.Empty, reader.Item("notification_emails_reservations"))
                    .ppActive = IIf(IsDBNull(reader.Item("active")), 0, reader.Item("active"))
                    .ppAvailableCredit = IIf(IsDBNull(reader.Item("available_credit")), 0, reader.Item("available_credit"))

                    If IsDate(reader.Item("created")) Then
                        DateTime.TryParse(reader.Item("created"), dtCreated)
                        .ppCreated = dtCreated
                        .FormatCreated = dtCreated.ToShortDateString()
                    Else
                        .FormatCreated = ""
                    End If

                    .NumTarjeta = IIf(IsDBNull(reader.Item("NumTarjeta")), String.Empty, reader.Item("NumTarjeta"))
                    .FecVencimiento = IIf(IsDBNull(reader.Item("FecVencimiento")), String.Empty, reader.Item("FecVencimiento"))
                    .CodSeguridad = IIf(IsDBNull(reader.Item("CodSeguridad")), String.Empty, reader.Item("CodSeguridad"))
                    .TipoTarjeta = IIf(IsDBNull(reader.Item("TipoTarjeta")), String.Empty, reader.Item("TipoTarjeta"))

                End With
                InfoList.Add(objInfo)

            End While

            reader.Close()

            Return InfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgencyContractsPaging(ByVal AgencyCode As Integer, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AgencyContractInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DetailList As New List(Of AgencyContractInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                DetailList = GetAgencyContractsPaging(con, AgencyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return DetailList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgencyContractData(ByVal AgencyContractCode As Integer) As List(Of AgencyContractInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DetailList As New List(Of AgencyContractInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                DetailList = GetAgencyContractData(con, AgencyContractCode)
            End Using

            Return DetailList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgenciesPaging(ByVal AgencyCode As Integer, ByVal CompanyCode As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AgencyInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of AgencyInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetAgenciesPaging(con, AgencyCode, CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return List

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgencyContractsCount(ByVal AgencyCode As Integer, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetAgencyContractsCount(con, AgencyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgenciesCount(ByVal AgencyCode As Integer, ByVal CompanyCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetAgenciesCount(con, AgencyCode, CompanyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAgency(ByVal AgencyCode As Integer, ByVal CompanyCode As String, ByVal Name As String,
                                   ByVal AgencyTypeCode As Int16, ByVal Country As String, ByVal Province As String, ByVal Canton As String, ByVal District As String,
                                   ByVal Address As String, ByVal Phone As String, ByVal ContactName As String, ByVal ContactOccupation As String, ByVal ContactEmail As String,
                                   ByVal ContactOfficePhone As String, ByVal ContactMobilePhone As String, ByVal NotificationEmailsReservations As String,
                                   ByVal Active As Boolean, ByVal AvailableCredit As Boolean, NumTarjeta As String, FecVencimiento As String, CodSeguridad As String, TipoTarjeta As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAgency(con, AgencyCode, CompanyCode, Name, AgencyTypeCode, Country,
                                    Province, Canton, District, Address, Phone,
                                    ContactName, ContactOccupation, ContactEmail, ContactOfficePhone, ContactMobilePhone, NotificationEmailsReservations,
                                    Active, AvailableCredit, NumTarjeta, FecVencimiento, CodSeguridad, TipoTarjeta, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteAgency(ByVal AgencyCode As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAgency(con, AgencyCode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAgencyContract(ByVal AgencyContractCode As Integer, ByVal AgencyCode As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                       Notes As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using Scope As TransactionScope = New TransactionScope()

                Using con As SqlConnection = dataHelper.OpenConnection()

                    response = SaveAgencyContract(con, AgencyContractCode, AgencyCode, StartDate, EndDate, Notes, sMode)

                    'SaveRateRoomType(con, RateDetID, RoomtypeID, CompanyID, StartDate.Date, EndDate.Date, sMode, RateID)

                End Using

                Scope.Complete()
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteAgencyContract(ByVal AgencyContractCode As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAgencyContract(con, AgencyContractCode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAgencyContactFile(ByVal AgencyContractCode As Long, ByVal FileName As String, ByVal ContentType As String, ByVal File As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAgencyContractFile(con, AgencyContractCode, FileName, ContentType, File)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function RelatePromoCodeAgency(ByVal AgencyCode As Integer, ByVal PromoID As Int16) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = RelatePromoCodeAgency(con, AgencyCode, PromoID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgencyPromoCount(ByVal AgencyCode As Integer, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetAgencyPromoCount(con, AgencyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgencyPromosPaging(ByVal AgencyCode As Integer, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsPromoCodeInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DetailList As New List(Of clsPromoCodeInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                DetailList = GetAgencyPromosPaging(con, AgencyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return DetailList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteAgencyPromoDetail(ByVal AgencyCode As Integer, ByVal PromoId As Int16) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAgencyPromoDetail(con, AgencyCode, PromoId)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

#Region "Private Methods"
    Friend Function GetCulture(con As SqlConnection, ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CultureInfoList As New List(Of CultureInfo)()
            Dim objCultureInfo As New CultureInfo

            With cmd
                .CommandText = "spCulture"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@cultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objCultureInfo = New CultureInfo

                With objCultureInfo
                    .ppCultureID = Val(reader.Item("CultureID") & "")
                    .ppCultureName = reader.Item("CultureName") & ""
                    .ppDisplayName = reader.Item("DisplayName") & ""
                End With
                CultureInfoList.Add(objCultureInfo)

            End While

            reader.Close()

            Return CultureInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencyContractsPaging(con As SqlConnection, AgencyCode As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AgencyContractInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objList As New List(Of AgencyContractInfo)()
            Dim obj As New AgencyContractInfo

            With cmd
                .CommandText = "spGetAgencyContractsPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("agency_code", AgencyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                obj = New AgencyContractInfo
                With obj
                    .ppAgencyContractCode = reader.Item("agency_contract_code")
                    .ppAgencyCode = reader.Item("agency_code")

                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                        .FormatStartDate = .ppStart_date.ToShortDateString
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                        .FormatEndDate = .ppEnd_date.ToShortDateString
                    End If

                    .ppNotes = IIf(IsDBNull(reader.Item("notes")), String.Empty, reader.Item("notes"))

                    If IsDate(reader.Item("end_date")) Then
                        .ppLastDateModified = CDate(reader.Item("last_date_modified"))
                        .FormatLastDateModified = .ppLastDateModified.ToShortDateString
                    End If

                    .ppContractFileName = IIf(IsDBNull(reader.Item("contract_file_name")), String.Empty, reader.Item("contract_file_name"))
                    .ppContractFileType = IIf(IsDBNull(reader.Item("contract_file_type")), String.Empty, reader.Item("contract_file_type"))
                End With
                objList.Add(obj)

            End While

            reader.Close()

            Return objList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencyContractData(con As SqlConnection, AgencyContractCode As Integer) As List(Of AgencyContractInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objList As New List(Of AgencyContractInfo)()
            Dim obj As New AgencyContractInfo

            With cmd
                .CommandText = "spAgencyContract"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@agency_contract_code", AgencyContractCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                obj = New AgencyContractInfo
                With obj
                    .ppAgencyContractCode = reader.Item("agency_contract_code")
                    .ppAgencyCode = reader.Item("agency_code")

                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                        .FormatStartDate = .ppStart_date.ToShortDateString
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                        .FormatEndDate = .ppEnd_date.ToShortDateString
                    End If

                    .ppNotes = IIf(IsDBNull(reader.Item("notes")), String.Empty, reader.Item("notes"))

                    If IsDate(reader.Item("end_date")) Then
                        .ppLastDateModified = CDate(reader.Item("last_date_modified"))
                        .FormatLastDateModified = .ppLastDateModified.ToShortDateString
                    End If

                    .ppContractFileName = IIf(IsDBNull(reader.Item("contract_file_name")), String.Empty, reader.Item("contract_file_name"))
                    .ppContractFileType = IIf(IsDBNull(reader.Item("contract_file_type")), String.Empty, reader.Item("contract_file_type"))
                End With
                objList.Add(obj)

            End While

            reader.Close()

            Return objList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Friend Function GetAgenciesPaging(con As SqlConnection, AgencyCode As Integer, ByVal CompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AgencyInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim objList As New List(Of AgencyInfo)()
        Dim obj As New AgencyInfo
        Dim dtCreated As New Date
        Try
            With cmd
                .CommandText = "spGetAgenciesPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", CompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                obj = New AgencyInfo
                With obj
                    .ppAgencyCode = reader.Item("agency_code")
                    .ppCompanyCode = reader.Item("company_code")
                    .ppAgencyName = reader.Item("name")
                    .ppAgencyTypeCode = reader.Item("agency_type_code")
                    .ppCountry = IIf(IsDBNull(reader.Item("country")), String.Empty, reader.Item("country"))
                    .ppProvince = IIf(IsDBNull(reader.Item("province")), String.Empty, reader.Item("province"))
                    .ppCanton = IIf(IsDBNull(reader.Item("canton")), String.Empty, reader.Item("canton"))
                    .ppDistrict = IIf(IsDBNull(reader.Item("district")), String.Empty, reader.Item("district"))
                    .ppAddress = IIf(IsDBNull(reader.Item("address")), String.Empty, reader.Item("address"))
                    .ppPhone = IIf(IsDBNull(reader.Item("phone")), String.Empty, reader.Item("phone"))
                    .ppContactName = IIf(IsDBNull(reader.Item("contact_name")), String.Empty, reader.Item("contact_name"))
                    .ppContactOccupation = IIf(IsDBNull(reader.Item("contact_occupation")), String.Empty, reader.Item("contact_occupation"))
                    .ppContactEmail = IIf(IsDBNull(reader.Item("contact_email")), String.Empty, reader.Item("contact_email"))
                    .ppContactOfficePhone = IIf(IsDBNull(reader.Item("contact_office_phone")), String.Empty, reader.Item("contact_office_phone"))
                    .ppContactMobilePhone = IIf(IsDBNull(reader.Item("contact_mobile_phone")), String.Empty, reader.Item("contact_mobile_phone"))
                    .ppNotificationEmailReservations = IIf(IsDBNull(reader.Item("notification_emails_reservations")), String.Empty, reader.Item("notification_emails_reservations"))
                    .ppActive = IIf(IsDBNull(reader.Item("active")), 0, reader.Item("active"))
                    .ppAvailableCredit = IIf(IsDBNull(reader.Item("available_credit")), 0, reader.Item("available_credit"))

                    If IsDate(reader.Item("created")) Then
                        DateTime.TryParse(reader.Item("created"), dtCreated)
                        .ppCreated = dtCreated
                        .FormatCreated = dtCreated.ToShortDateString()
                    Else
                        .FormatCreated = ""
                    End If


                    .NumTarjeta = IIf(IsDBNull(reader.Item("NumTarjeta")), String.Empty, reader.Item("NumTarjeta"))
                    .FecVencimiento = IIf(IsDBNull(reader.Item("FecVencimiento")), String.Empty, reader.Item("FecVencimiento"))
                    .CodSeguridad = IIf(IsDBNull(reader.Item("CodSeguridad")), String.Empty, reader.Item("CodSeguridad"))
                    .TipoTarjeta = IIf(IsDBNull(reader.Item("TipoTarjeta")), String.Empty, reader.Item("TipoTarjeta"))



                End With
                objList.Add(obj)

            End While

            reader.Close()

            Return objList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencyContractsCount(con As SqlConnection, ByVal AgencyCode As Integer, PageSize As Integer) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spAgencyContract"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "CNT")
                .Parameters.AddWithValue("agency_code", AgencyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgenciesCount(con As SqlConnection, ByVal AgencyCode As Integer, ByVal CompanyCode As String, PageSize As Integer) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spAgency"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "CNT")
                .Parameters.AddWithValue("agency_code", AgencyCode)
                .Parameters.AddWithValue("company_code", CompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAgency(con As SqlConnection, ByVal AgencyCode As Integer, ByVal CompanyCode As String, ByVal Name As String,
                                   ByVal AgencyTypeCode As Int16, ByVal Country As String, ByVal Province As String, ByVal Canton As String, ByVal District As String,
                                   ByVal Address As String, ByVal Phone As String, ByVal ContactName As String, ByVal ContactOccupation As String, ByVal ContactEmail As String,
                                   ByVal ContactOfficePhone As String, ByVal ContactMobilePhone As String, ByVal NotificationEmailsReservations As String,
                                   ByVal Active As Boolean, ByVal AvailableCredit As Boolean, NumTarjeta As String, FecVencimiento As String, CodSeguridad As String, TipoTarjeta As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgency"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@agency_code", AgencyCode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@name", Name)
                .Parameters.AddWithValue("@agency_type_code", AgencyTypeCode)
                .Parameters.AddWithValue("@country", Country)
                .Parameters.AddWithValue("@province", Province)
                .Parameters.AddWithValue("@canton", Canton)
                .Parameters.AddWithValue("@district", District)
                .Parameters.AddWithValue("@address", Address)
                .Parameters.AddWithValue("@phone", Phone)
                .Parameters.AddWithValue("@contact_name", ContactName)
                .Parameters.AddWithValue("@contact_occupation", ContactOccupation)
                .Parameters.AddWithValue("@contact_email", ContactEmail)
                .Parameters.AddWithValue("@contact_office_phone", ContactOfficePhone)
                .Parameters.AddWithValue("@contact_mobile_phone", ContactMobilePhone)
                .Parameters.AddWithValue("@notification_emails_reservations", NotificationEmailsReservations)
                .Parameters.AddWithValue("@active", Active)
                .Parameters.AddWithValue("@available_credit", AvailableCredit)

                .Parameters.AddWithValue("@NumTarjeta", NumTarjeta)
                .Parameters.AddWithValue("@FecVencimiento", FecVencimiento)
                .Parameters.AddWithValue("@CodSeguridad", CodSeguridad)
                .Parameters.AddWithValue("@TipoTarjeta", TipoTarjeta)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAgency(con As SqlConnection, ByVal AgencyCode As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgency"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@agency_code", AgencyCode)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAgencyContract(con As SqlConnection, ByVal AgencyContractCode As Integer, ByVal AgencyCode As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                       Notes As String, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgencyContract"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@agency_contract_code", AgencyContractCode)
                .Parameters.AddWithValue("@agency_code", AgencyCode)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@notes", Notes)
            End With


            If AgencyContractCode = 0 Then
                AgencyContractCode = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            Else
                dataHelper.ExecuteNonQuery(cmd, con)
            End If

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response


            'Return AgencyContractCode

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAgencyContract(con As SqlConnection, ByVal AgencyContractCode As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgencyContract"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@agency_contract_code", AgencyContractCode)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAgencyContractFile(con As SqlConnection, ByVal AgencyContractCode As Long, ByVal FileName As String, ByVal ContentType As String, ByVal File As Byte()) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAgencyContract"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "UFL") 'Update Contract File data
                .Parameters.AddWithValue("@agency_contract_code", AgencyContractCode)
                .Parameters.AddWithValue("@contract_file_name", FileName)
                .Parameters.AddWithValue("@contract_file_type", ContentType)
                .Parameters.AddWithValue("@contract_file_data", File)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)


            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function RelatePromoCodeAgency(con As SqlConnection, ByVal AgencyCode As Integer, ByVal PromoID As Int16) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgencyPromotionalCode"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "INS")
                .Parameters.AddWithValue("@agency_code", AgencyCode)
                .Parameters.AddWithValue("@promo_id", PromoID)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencyPromoCount(con As SqlConnection, ByVal AgencyCode As Integer, PageSize As Integer) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spAgencyPromotionalCode"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "CNT")
                .Parameters.AddWithValue("agency_code", AgencyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgencyPromosPaging(con As SqlConnection, AgencyCode As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsPromoCodeInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objList As New List(Of clsPromoCodeInfo)()
            Dim obj As New clsPromoCodeInfo
            Dim dt As Date

            With cmd
                .CommandText = "spGetAgencyPromosPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("agency_code", AgencyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                obj = New clsPromoCodeInfo
                With obj
                    .ppDescription = reader.Item("description") & ""
                    .ppType = reader.Item("type") & ""

                    Select Case .ppType
                        Case "P"
                            .ppType = "Percentage"
                        Case Else
                            .ppType = "Amount"
                    End Select
                    If IsDate(reader.Item("start_date")) Then
                        DateTime.TryParse(reader.Item("start_date"), dt)
                        .ppStartDate = dt
                        .UnFormatFrom = dt.ToShortDateString()
                    Else
                        .UnFormatFrom = ""
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        DateTime.TryParse(reader.Item("end_date"), dt)
                        .ppEndDate = dt
                        .UnFormatTo = dt.ToShortDateString()
                    Else
                        .UnFormatTo = ""
                    End If
                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppPromoCode = IIf(IsDBNull(reader.Item("promo_code")), String.Empty, reader.Item("promo_code").ToString.Trim)
                End With
                objList.Add(obj)

            End While

            reader.Close()

            Return objList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAgencyPromoDetail(con As SqlConnection, ByVal AgencyCode As Integer, ByVal PromoID As Int16) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spAgencyPromotionalCode"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@agency_code", AgencyCode)
                .Parameters.AddWithValue("@promo_id", PromoID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
