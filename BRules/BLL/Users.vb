﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions
Public Class Users

#Region "Public Methods"

    Public Function encriptaclave(pass As String) As String
        Dim wvector As String
        Dim wivector As Integer
        Dim wipass As Integer
        Dim wpass As String
        Dim wcarpass As String
        Dim wcarvector As String
        pass = LCase(pass)
        wpass = ""
        wvector = "¡jû0$9%8q<w&er7*6.4b@n-m31!·¿to p+ñ\lk#hg?f/=ds)a:z(x>c5v,_yu2i;"
        For wipass = 1 To Len(pass)
            wcarpass = Mid(pass, wipass, 1)
            For wivector = 1 To 64
                wcarvector = Mid(wvector, wivector, 1)
                If wcarpass = wcarvector Then
                    wpass = wpass + Mid(wvector, (65 - wivector), 1)
                    Exit For
                End If
            Next
        Next
        encriptaclave = wpass
    End Function

    Public Function SaveUser(ByVal sCompanyCode As String, ByVal iUserID As Integer, ByVal sUserName As String, ByVal sPassword As String, ByVal sFirstName As String, ByVal sLastName As String,
                             ByVal iProfileID As Integer, ByVal sMode As String, ByVal bActive As Boolean, SecondSurname As String, isAgencyUser As Boolean, AgencyCode As String, ByVal bisDomainUser As Boolean
                             ) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim sresult As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                sresult = SaveUser(con, sCompanyCode, iUserID, sUserName, sPassword, sFirstName, sLastName,
                              iProfileID, sMode, bActive, SecondSurname, isAgencyUser, AgencyCode, bisDomainUser)
            End Using
            Return sresult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Public Function GetUser(ByVal iUserID As Integer, ByVal sCompanyCode As String, ByVal sMode As String) As List(Of UserInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserList As New List(Of UserInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserList = GetUser(con, iUserID, sCompanyCode, sMode)
            End Using
            Return UserList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function LoginUser(ByVal sUserName As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = LoginUser(con, sUserName)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteUser(ByVal iUserID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            'Using Scope As TransactionScope = New TransactionScope()
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteUser(con, iUserID)
            End Using
            'End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function Last5Passwords(ByVal user_id As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = Last5Passwords(con, user_id)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function SaveLastPassword(ByVal iUserID As Integer, ByVal dateChanged As Date, ByVal password As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveLastPassword(con, iUserID, dateChanged, password)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveUserLog(ByVal iUserID As Integer, ByVal iRefID As Long, ByVal sEvent As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveUserLog(con, iUserID, iRefID, sEvent)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function SaveUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveUserCompany(con, sCompanyCode, iUserID)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteUserCompany(ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteUserCompany(con, sCompanyCode, iUserID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetUserLog(ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserLogInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserLogInfoList As New List(Of UserLogInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserLogInfoList = GetUserLog(con, iUserID, dFrom, dTo, sMode)
            End Using
            Return UserLogInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function BlockUser(ByVal iUserID As Integer, ByVal bActive As Boolean) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = BlockUser(con, iUserID, bActive)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetUserCompany(ByVal iUserID As Integer) As List(Of UserCompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserCompanyInfoList As New List(Of UserCompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserCompanyInfoList = GetUserCompany(con, iUserID)
            End Using
            Return UserCompanyInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAPIUserCompany(ByVal sMode As String, ByVal sCompanyCode As String, ByVal iUserID As String, ByVal sPassword As String, ByVal bActive As Boolean, ByVal iUserCompany As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveAPIUserCompany(con, sMode, sCompanyCode, iUserID, sPassword, bActive, iUserCompany)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetUserAPICompany(ByVal sUserID As String) As List(Of UserAPIInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserAPIInfoList As New List(Of UserAPIInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserAPIInfoList = GetUserAPICompany(con, sUserID)
            End Using
            Return UserAPIInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetUsersAPI() As List(Of UserAPIInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserAPIInfoList As New List(Of UserAPIInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserAPIInfoList = GetUsersAPI(con)
            End Using
            Return UserAPIInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteUserAPICompany(ByVal sUserID As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = DeleteUserAPICompany(con, sUserID)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function BlockAPIUser(ByVal sUserID As String, ByVal bActive As Boolean) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = BlockAPIUser(con, sUserID, bActive)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetUserAPILog(ByVal sUserID As String, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserAPILogInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserAPILogInfoList As New List(Of UserAPILogInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserAPILogInfoList = GetUserAPILog(con, sUserID, dFrom, dTo, sMode)
            End Using
            Return UserAPILogInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetProfile(ByVal iProfileID As Integer, ByVal company_code As String) As List(Of ProfileInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ProfileInfoList As New List(Of ProfileInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ProfileInfoList = GetProfile(con, iProfileID, company_code)
            End Using
            Return ProfileInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveProfile(ByVal iProfileID As Integer, ByVal sProfileName As String, ByVal sCompanyCode As String, ByVal sMode As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim iResult As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                iResult = SaveProfile(con, iProfileID, sProfileName, sCompanyCode, sMode)
            End Using

            Return iResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveProfilePermissions(ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveProfilePermissions(con, iPermissions, iPermissionId, iProfileID, sMode)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function DeleteProfilePermissions(ByVal iProfileID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = DeleteProfilePermissions(con, iProfileID)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetProfilePermissions(ByVal iPermissionId As Integer, ByVal iProfileID As Integer) As List(Of ProfileInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ProfileInfoList As New List(Of ProfileInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ProfileInfoList = GetProfilePermissions(con, iPermissionId, iProfileID)
            End Using
            Return ProfileInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveUser(ByVal CompanyCode As String, ByVal Password As String,
                        ByVal Blocked As Boolean) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveUser(con, CompanyCode, Password, Blocked)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetLog(ByVal CompanyCode As String) As List(Of SyncLogInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of SyncLogInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetLog(con, CompanyCode)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveLog(ByVal CompanyCode As String, ByVal Message As String,
                            ByVal IpAddress As String, ByVal LogLevel As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveLog(con, CompanyCode, Message, IpAddress, LogLevel)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteProfile(ByVal iProfileID As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteProfile(con, iProfileID)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetUsersPaging(ByVal sCompanyCode As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of UserInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim UserList As New List(Of UserInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserList = GetUsersPaging(con, sCompanyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return UserList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetUsersPagingCount(ByVal sCompanyCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetUsersPagingCount(con, sCompanyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetUserLogPagin(ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of UserLogInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim UserLogInfoList As New List(Of UserLogInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                UserLogInfoList = GetUserLogPagin(con, iUserID, dFrom, dTo, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return UserLogInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetUsersLogPagingCount(ByVal UserId As Integer, FromDt As Date, ToDt As Date, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetUsersLogPagingCount(con, UserId, FromDt, ToDt, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function SaveUser(con As SqlConnection, ByVal sCompanyCode As String, ByVal iUserID As Integer, ByVal sUserName As String, ByVal sPassword As String, ByVal sFirstName As String, ByVal sLastName As String,
                             ByVal iProfileID As Integer, ByVal sMode As String, ByVal bActive As Boolean, SecondSurname As String, isAgencyUser As Boolean, AgencyCode As String, ByVal bisDomainUser As Boolean) As Integer
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim iResult As Integer = 0
        Dim value As Object

        Try
            With cmd
                .Connection = con
                .CommandText = "spUsers"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@user_name", sUserName)
                .Parameters.AddWithValue("@password", encriptaclave(sPassword))
                .Parameters.AddWithValue("@first_name", sFirstName)
                .Parameters.AddWithValue("@last_name", sLastName)
                .Parameters.AddWithValue("@SecondSurname", SecondSurname)
                .Parameters.AddWithValue("@profile_id", iProfileID)
                .Parameters.AddWithValue("@active", bActive)
                .Parameters.AddWithValue("@isAgencyUser", isAgencyUser)
                .Parameters.AddWithValue("@AgencyCode", AgencyCode)
                .Parameters.AddWithValue("@isDomainUser", bisDomainUser)

            End With

            value = dataHelper.ExecuteScalar(cmd, con)
            If IsNumeric(value) Then
                iResult = Convert.ToInt32(value)
            End If
            Return iResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function GetUser(con As SqlConnection, ByVal iUserID As Integer, ByVal sCompanyCode As String, ByVal sMode As String) As List(Of UserInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim UserList As New List(Of UserInfo)()
        Dim sObjUsers As New UserInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spUsers"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                Dim sObjUser As New UserInfo
                With sObjUser
                    If sMode = "CBO" Then
                        .ppUserID = reader.Item("user_id")
                        .ppFirstName = reader.Item("first_name") & " " & reader.Item("last_name")
                    Else
                        .ppUserID = reader.Item("user_id")
                        .ppFirstName = reader.Item("first_name")
                        .ppLastName = reader.Item("last_name")
                        .ppPassword = encriptaclave(reader.Item("password"))
                        .ppProfileID = reader.Item("profile_id")
                        .ppUserName = reader.Item("user_name")
                        .ppProfile = reader.Item("profile")
                        .ppLastLogin = reader.Item("last_login")
                        .ppActive = reader.Item("active")
                    End If
                End With
                UserList.Add(sObjUser)
            End While

            reader.Close()
            Return UserList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function LoginUser(con As SqlConnection, ByVal sUserName As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = ""

        Try
            With cmd
                .Connection = con
                .Parameters.AddWithValue("@user_name", sUserName)
                .CommandText = "spUserLogin"
                .CommandType = CommandType.StoredProcedure
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                sResult = reader.Item("user_id")
                sResult &= "~" & reader.Item("Name")
                sResult &= "~" & reader.Item("profile_id")
                sResult &= "~" & reader.Item("active")
                sResult &= "~" & reader.Item("super_user")
                sResult &= "~" & encriptaclave(reader.Item("password"))
                If reader.Item("next_date_change") <= Now.Date Then
                    sResult &= "~1"
                Else
                    sResult &= "~0"
                End If
                sResult &= "~" & reader.Item("IsAgencyUser")
                sResult &= "~" & reader.Item("AgencyCode")
                sResult &= "~" & reader.Item("isDomainUser")


            End While



            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteUser(con As SqlConnection, ByVal iUserID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spUsers"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@user_id", iUserID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function Last5Passwords(con As SqlConnection, ByVal user_id As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spUsersLogPwd"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@user_id", user_id)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = encriptaclave(reader.Item("password")) & "~"
            End If
            reader.Close()
            Return Mid(sResult, 1, Len(sResult) - 1)

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try


    End Function


    Friend Function SaveLastPassword(con As SqlConnection, ByVal iUserID As Integer, ByVal dateChanged As Date, ByVal password As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spUsersLogPwd"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "INS")
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@date_changed", dateChanged)
                .Parameters.AddWithValue("@password", password)
                ' .Parameters.AddWithValue("@password", encriptaclave(password))
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try


    End Function

    Friend Function SaveUserLog(con As SqlConnection, ByVal iUserID As Integer, ByVal iRefID As Long, ByVal sEvent As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spUsersLog"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "INS")
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@ref_id", iRefID)
                .Parameters.AddWithValue("@event", sEvent)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try

    End Function


    Friend Function SaveUserCompany(con As SqlConnection, ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spUserCompany"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "INS")
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try

    End Function



    Friend Function DeleteUserCompany(con As SqlConnection, ByVal sCompanyCode As String, ByVal iUserID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spUserCompany"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@company_code", sCompanyCode.Trim)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetUserCompany(con As SqlConnection, ByVal iUserID As Integer) As List(Of UserCompanyInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim UserCompanyInfoList As New List(Of UserCompanyInfo)()
        Dim sObjUserCompanyInfo As New UserCompanyInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spUserCompany"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@user_id", iUserID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjUserCompanyInfo = New UserCompanyInfo
                With sObjUserCompanyInfo
                    If iUserID > 0 Then .ppUserId = reader.Item("user_id")
                    .ppCompanyCode = IIf(IsDBNull(reader.Item("company_code")), "", reader.Item("company_code").ToString.Trim)
                    .ppCompanyName = If(IsDBNull(reader.Item("company_name")), "", reader.Item("company_name").ToString.Trim)
                    .ppServices = reader.Item("services")
                    .ppHotel = reader.Item("hotel")
                End With

                UserCompanyInfoList.Add(sObjUserCompanyInfo)
            End While

            reader.Close()

            Return UserCompanyInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function GetUserLog(con As SqlConnection, ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserLogInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim UserLogInfoList As New List(Of UserLogInfo)()
        Dim sObjUserLogInfo As New UserLogInfo

        Try
            With cmd
                .CommandText = "spUsersLog"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@from", dFrom)
                .Parameters.AddWithValue("@to", dTo)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjUserLogInfo = New UserLogInfo
                With sObjUserLogInfo
                    .ppLogID = reader.Item("LogID")
                    .ppRefID = reader.Item("ref_id")
                    .ppEvent = reader("event")
                    .ppEventDate = reader.Item("event_date")
                End With
                UserLogInfoList.Add(sObjUserLogInfo)
            End While
            reader.Close()

            Return UserLogInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetUserLogPagin(con As SqlConnection, ByVal iUserID As Integer, ByVal dFrom As DateTime, ByVal dTo As DateTime, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of UserLogInfo)

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim UserLogInfoList As New List(Of UserLogInfo)()
        Dim sObjUserLogInfo As New UserLogInfo

        Try
            With cmd
                .CommandText = "spGetUsersLogPaging"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@from", dFrom)
                .Parameters.AddWithValue("@to", dTo)

                .Parameters.AddWithValue("@PageNumber", PageNumber)
                .Parameters.AddWithValue("@PageSize", PageSize)
                .Parameters.AddWithValue("@OrderBy", OrderBy)
                .Parameters.AddWithValue("@IsAccending", IsAccending)

            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjUserLogInfo = New UserLogInfo
                With sObjUserLogInfo
                    .ppLogID = reader.Item("LogID")
                    .ppRefID = reader.Item("ref_id")
                    .ppEvent = reader("event")

                    If IsDate(reader.Item("event_date")) Then
                        .ppEventDate = CDate(reader.Item("event_date"))
                    End If

                    .FormatDate = .ppEventDate.ToShortDateString
                End With
                UserLogInfoList.Add(sObjUserLogInfo)
            End While
            reader.Close()

            Return UserLogInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function BlockUser(con As SqlConnection, ByVal iUserID As Integer, ByVal bActive As Boolean) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spUsers"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "BLK")
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@active", bActive)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function SaveAPIUserCompany(con As SqlConnection, ByVal sMode As String, ByVal sCompanyCode As String, ByVal iUserID As String, ByVal sPassword As String, ByVal bActive As Boolean, ByVal iUserCompany As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spUserAPI"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@user_id", iUserID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@password", encriptaclave(sPassword.Trim))
                .Parameters.AddWithValue("@active", bActive)
                .Parameters.AddWithValue("@user_company", iUserCompany)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try


    End Function


    Friend Function GetUserAPICompany(con As SqlConnection, ByVal sUserID As String) As List(Of UserAPIInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim UserAPIInfoList As New List(Of UserAPIInfo)()
            Dim sObjUserAPIInfo As New UserAPIInfo

            With cmd
                .Connection = con
                .CommandText = "spUserAPI"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@user_id", sUserID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)
            While reader.Read()
                sObjUserAPIInfo = New UserAPIInfo
                With sObjUserAPIInfo
                    .ppUserID = reader.Item("user_id")
                    .ppCompanyCode = reader.Item("company_code")
                    .ppPassword = encriptaclave(reader.Item("password"))
                    .ppCompany = reader.Item("company_name")
                    .ppLastLogin = reader.Item("last_activity")
                    .ppActive = reader.Item("active")
                End With
                UserAPIInfoList.Add(sObjUserAPIInfo)

            End While

            reader.Close()

            Return UserAPIInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetUsersAPI(con As SqlConnection) As List(Of UserAPIInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim UserAPIInfoList As New List(Of UserAPIInfo)()
            Dim sObjUserAPIInfo As New UserAPIInfo

            With cmd
                .Connection = con
                .CommandText = "spUserAPI"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                sObjUserAPIInfo = New UserAPIInfo

                With sObjUserAPIInfo
                    .ppUserID = reader.Item("user_id")
                    .ppCompanyCode = reader.Item("company_code")
                    .ppPassword = encriptaclave(reader.Item("password"))
                    .ppCompany = reader.Item("company_name")
                    .ppLastLogin = reader.Item("last_activity")
                    .ppActive = reader.Item("active")
                End With
                UserAPIInfoList.Add(sObjUserAPIInfo)

            End While

            reader.Close()

            Return UserAPIInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try


    End Function

    Friend Function DeleteUserAPICompany(con As SqlConnection, ByVal sUserID As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spUserAPI"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@user_id", sUserID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Friend Function BlockAPIUser(con As SqlConnection, ByVal sUserID As String, ByVal bActive As Boolean) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spUserAPI"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "BLK")
                .Parameters.AddWithValue("@user_id", sUserID)
                .Parameters.AddWithValue("@active", bActive)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetUserAPILog(con As SqlConnection, ByVal sUserID As String, ByVal dFrom As DateTime, ByVal dTo As DateTime, ByVal sMode As String) As List(Of UserAPILogInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim UserAPILogInfoList As New List(Of UserAPILogInfo)()
        Dim sObjUserAPILogInfo As New UserAPILogInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spUsersAPILog"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@user_id", sUserID)
                .Parameters.AddWithValue("@from", dFrom)
                .Parameters.AddWithValue("@to", dTo)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjUserAPILogInfo = New UserAPILogInfo

                With sObjUserAPILogInfo
                    .ppReceived = reader.Item("received")
                    .ppResponse = reader.Item("response")
                    .ppEvent = reader.Item("event")
                    .ppEventDate = reader.Item("event_date")
                End With
                UserAPILogInfoList.Add(sObjUserAPILogInfo)
            End While

            reader.Close()

            Return UserAPILogInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    'Profiles

    Friend Function GetProfile(con As SqlConnection, ByVal iProfileID As Integer, ByVal company_code As String) As List(Of ProfileInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ProfileInfoList As New List(Of ProfileInfo)()
        Dim sObjProfileInfo As New ProfileInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spProfiles"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@profile_id", iProfileID)
                .Parameters.AddWithValue("@company_code", company_code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjProfileInfo = New ProfileInfo

                With sObjProfileInfo
                    .ppProfileID = reader.Item("profile_id")
                    .ppProfileName = reader.Item("profile_name")

                End With
                ProfileInfoList.Add(sObjProfileInfo)
            End While

            reader.Close()

            Return ProfileInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function SaveProfile(con As SqlConnection, ByVal iProfileID As Integer, ByVal sProfileName As String, ByVal sCompanyCode As String, ByVal sMode As String) As Integer
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Dim iResult As Integer
        Try

            With cmd
                .Connection = con
                .CommandText = "spProfiles"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@profile_id", iProfileID)
                .Parameters.AddWithValue("@profile_name", sProfileName)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            iResult = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))

            Return iResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function DeleteProfile(con As SqlConnection, ByVal iProfileID As Integer) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = ""
        Dim iUsers As Int16 = 0

        Try

            With cmd
                .Connection = con
                .CommandText = "spProfiles"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@profile_id", iProfileID)
            End With
            iUsers = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            If iUsers > 0 Then
                sResult = "Exists"
            End If

        Catch EX1 As Exception
            sResult = "Error: " & EX1.ToString
            Throw New Exception(EX1.Message)
        End Try
        Return sResult
    End Function

    Friend Function SaveProfilePermissions(con As SqlConnection, ByVal iPermissions As List(Of ProfileInfo), ByVal iPermissionId As Integer, ByVal iProfileID As Integer, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            For i = 0 To iPermissions.Count - 1
                With cmd
                    .Connection = con
                    .Parameters.Clear()
                    .CommandText = "spProfilePermissions"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.AddWithValue("@mode", sMode)
                    .Parameters.AddWithValue("@profile_id", iProfileID)
                    .Parameters.AddWithValue("@control_tag", iPermissions(i).ppControlTag)
                    .Parameters.AddWithValue("@allow", iPermissions(i).ppAllow)
                End With
                Call dataHelper.ExecuteNonQuery(cmd, con)
            Next

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try


    End Function


    Friend Function DeleteProfilePermissions(con As SqlConnection, ByVal iProfileID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try

            With cmd
                .Connection = con
                .CommandText = "spProfilePermissions"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@profile_id", iProfileID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetProfilePermissions(con As SqlConnection, ByVal iPermissionId As Integer, ByVal iProfileID As Integer) As List(Of ProfileInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim ProfileInfoList As New List(Of ProfileInfo)()
            Dim sObjProfileInfo As New ProfileInfo

            With cmd
                .Connection = con
                .CommandText = "spProfilePermissions"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@permission_id", iPermissionId)
                .Parameters.AddWithValue("@profile_id", iProfileID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                sObjProfileInfo = New ProfileInfo

                With sObjProfileInfo
                    .ppControlTag = reader.Item("control_tag")
                    .ppAllow = reader.Item("allow")
                End With
                ProfileInfoList.Add(sObjProfileInfo)

            End While

            reader.Close()

            Return ProfileInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveLog(con As SqlConnection, ByVal CompanyCode As String, ByVal Message As String,
                            ByVal IpAddress As String, ByVal LogLevel As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try

            With cmd
                .CommandText = "spSyncLog"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "INS")
                .Parameters.AddWithValue("CompanyCode", CompanyCode)
                .Parameters.AddWithValue("Message", Message)
                .Parameters.AddWithValue("IpAddress", IpAddress)
                .Parameters.AddWithValue("LogLevel", LogLevel)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetLog(con As SqlConnection, ByVal CompanyCode As String) As List(Of SyncLogInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim SyncLogInfoList As New List(Of SyncLogInfo)()
            Dim sObjSyncLogInfo As New SyncLogInfo

            With cmd
                .CommandText = "spSyncLog"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SEL")
                .Parameters.AddWithValue("CompanyCode", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                sObjSyncLogInfo = New SyncLogInfo
                With sObjSyncLogInfo
                    .ppMessage = reader.Item("Message")
                    .ppIpAddress = reader.Item("IpAddress")
                    .ppLogLevel = reader.Item("LogLevel")
                End With
                SyncLogInfoList.Add(sObjSyncLogInfo)
            End While

            Return SyncLogInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveUser(con As SqlConnection, ByVal CompanyCode As String, ByVal Password As String,
                        ByVal Blocked As Boolean) As Response


        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spSyncUser"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "INS")
                .Parameters.AddWithValue("CompanyCode", CompanyCode)
                .Parameters.AddWithValue("Password", Password)
                .Parameters.AddWithValue("Blocked", Blocked)

            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetUsersPaging(con As SqlConnection, ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of UserInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim UserList As New List(Of UserInfo)()
            Dim sObjUsers As New UserInfo
            Dim Result As Boolean

            With cmd
                .CommandText = "spGetUsersPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                Dim sObjUser As New UserInfo
                With sObjUser
                    .ppUserID = reader.Item("user_id")
                    .SecondSurname = reader.Item("SecondSurname") & ""
                    .ppUserID = reader.Item("user_id")
                    .ppFirstName = reader.Item("first_name")
                    .ppLastName = reader.Item("last_name")
                    .ppPassword = encriptaclave(reader.Item("password"))
                    .ppPasswordConfirmation = .ppPassword
                    .ppProfileID = reader.Item("profile_id")
                    .ppUserName = reader.Item("user_name")
                    .ppProfile = reader.Item("profile")
                    If IsDate(reader.Item("last_login")) Then
                        .ppLastLogin = CDate(reader.Item("last_login"))
                        .LastLoginFormat = .ppLastLogin.ToShortDateString
                    End If


                    Boolean.TryParse(reader.Item("active") & "", Result)

                    .ppActive = Result

                    Result = False
                    Boolean.TryParse(reader.Item("isAgencyUser") & "", Result)

                    .isAgencyUser = Result

                    Result = False
                    Boolean.TryParse(reader.Item("isDomainUser") & "", Result)
                    .ppisDomainUser = Result

                    If Not IsNothing(reader.Item("AgencyCode")) Then
                        .AgencyCode = reader.Item("AgencyCode").ToString
                    End If


                End With
                UserList.Add(sObjUser)
            End While

            reader.Close()

            Return UserList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetUsersPagingCount(con As SqlConnection, ByVal sCompanyCode As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spUsersCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetUsersLogPagingCount(con As SqlConnection, ByVal UserId As Integer, FromDt As Date, ToDt As Date, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spUsersLogCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("user_id", UserId)
                .Parameters.AddWithValue("from", FromDt)
                .Parameters.AddWithValue("to", ToDt)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region



End Class
