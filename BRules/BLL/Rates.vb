﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Rates

#Region "Public Methods"

    Public Function SaveRateDetails(ByVal RateDetID As Long, ByVal RateID As Integer, RoomtypeID As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Double,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal Wadult1 As Double, ByVal Wadult2 As Double, ByVal Wadult3 As Double, ByVal Wadult4 As Double, ByVal Wadult5 As Double,
                                    ByVal Wadult6 As Double, ByVal Wadult7 As Double, ByVal Wadult8 As Double, ByVal Wadult9 As Double, ByVal Wadult10 As Double, ByVal Wchild As Double,
                                    CompanyID As String, ByVal sMode As String, ByVal roomcode As Integer
                                    ) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using Scope As TransactionScope = New TransactionScope()

                Using con As SqlConnection = dataHelper.OpenConnection()

                    RateDetID = SaveRateDetails(con, RateDetID, RateID, RoomtypeID, StartDate, EndDate, Adult1, Adult2, Adult3, Adult4, Adult5,
                                        Adult6, Adult7, Adult8, Adult9, Adult10, Child,
                                        Wadult1, Wadult2, Wadult3, Wadult4, Wadult5,
                                        Wadult6, Wadult7, Wadult8, Wadult9, Wadult10, Wchild,
                                        CompanyID, sMode, roomcode)

                    SaveRateRoomType(con, RateDetID, RoomtypeID, CompanyID, StartDate.Date, EndDate.Date, sMode, RateID)

                End Using

                Scope.Complete()
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateHeader(ByVal RateID As Integer, ByVal RateCode As String, ByVal Description As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal MinLos As Integer, ByVal MaxLos As Integer, ByVal Enabled As Boolean, ByVal WeekEnd As Boolean,
                                   ByVal Type As String, ByVal RateDetail As String, ByVal OrderList As Integer, ByVal FreeNights As Integer, ByVal Conditions As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateHeader(con, RateID, RateCode, Description, CompanyCode, StartDate,
                                    EndDate, MinLos, MaxLos, Enabled, WeekEnd,
                                    Type, RateDetail, OrderList, FreeNights, Conditions, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateLocale(ByVal RateID As Long, ByVal CultureID As Integer, ByVal Description As String,
                                   ByVal LongDescription As String, ByVal Conditions As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateLocale(con, RateID, CultureID, Description, LongDescription, Conditions, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRateHeader(ByVal RateID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRateHeader(con, RateID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateHeader(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRateHeader(con, iRateID, sCompanyCode, bSelectAll)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomsAvailable(ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RateLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RateLocaleInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRoomsAvailable(con, sCompanyCode, bSelectAll)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateRoomType(ByVal RateDetID As Long, ByVal RoomTypeID As Integer, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal sMode As String, ByVal RateID As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateRoomType(con, RateDetID, RoomTypeID, CompanyCode, StartDate,
                                    EndDate, sMode, RateID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailability(ByVal RateCode As String, ByVal RoomTypeID As Integer, ByVal Type As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal oldStartDate As Date, oldEndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, Adult1w As Double, Adult2w As Double,
                                    ByVal Adult3w As Double, ByVal Adult4w As Double, ByVal Adult5w As Double, ByVal Adult6w As Double, ByVal Adult7w As Double, ByVal Adult8w As Double,
                                    ByVal Adult9w As Double, ByVal Adult10w As Double, ByVal Child As Double, ByVal Childw As Double,
                                    ByVal CompanyID As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailability(con, RateCode, RoomTypeID, Type, StartDate, EndDate, oldStartDate, oldEndDate,
                                    Adult1, Adult2, Adult3, Adult4, Adult5,
                                    Adult6, Adult7, Adult8, Adult9, Adult10, Adult1w, Adult2w,
                                    Adult3w, Adult4w, Adult5w, Adult6w, Adult7w, Adult8w,
                                    Adult9w, Adult10w, Child, Childw,
                                    CompanyID, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRateDetails(ByVal RateDetID As Integer, CompanyCode As String, RoomTypeCode As String, StartDate As Date, EndDate As Date) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRateDetails(con, RateDetID, CompanyCode, RoomTypeCode, StartDate, EndDate)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateDetails(ByVal iRateDetID As Integer) As List(Of RateDetailInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RateDetailInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRateDetails(con, iRateDetID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateDates(ByVal iRateDetID As Integer) As List(Of RateDetailInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RateDetailInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRateDates(con, iRateDetID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateExcDates(sCompanyCode As String, ByVal sRateCode As String, sRoomTypeCode As String, ByVal dStart As Date, ByVal dEnd As Date) As List(Of RateDetailInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RateDetailInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRateExcDates(con, sCompanyCode, sRateCode, sRoomTypeCode, dStart, dEnd)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRateExcDates(sCompanyCode As String, ByVal sRateCode As String, rowID As Integer, sRoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRateExcDates(con, sCompanyCode, rowID, sRateCode, sRoomTypeCode, StartDate, EndDate)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateRoomType(ByVal RateDetID As Integer, sCompanyCode As String) As List(Of RateRoomTypeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RateRoomTypeList As New List(Of RateRoomTypeInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RateRoomTypeList = GetRateRoomType(con, RateDetID, sCompanyCode)
            End Using
            Return RateRoomTypeList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRateRoomType(ByVal RateDetID As Integer, ByVal RoomTypeID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRateRoomType(con, RateDetID, RoomTypeID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateException(ByVal CompanyCode As String, RateCode As String, RoomtypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                      ByVal StartDateAnt As Date, ByVal EndDateAnt As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateException(con, CompanyCode, RateCode, RoomtypeCode, StartDate, EndDate,
                                        StartDateAnt, EndDateAnt,
                                        Adult1, Adult2, Adult3, Adult4, Adult5,
                                        Adult6, Adult7, Adult8, Adult9, Adult10, Child, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRates(con, sCompanyId, ArrivalDate, DepartureDate, iAdults, iChildren)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetRates_v1(ByVal sCompanyId As String, ByVal RoomType As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRates_v1(con, sCompanyId, RoomType, ArrivalDate, DepartureDate, iAdults, iChildren)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRatesWithPromoCode(ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer, ByVal sPromoCode As String) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetRatesWithPromoCode(con, sCompanyId, ArrivalDate, DepartureDate, iAdults, iChildren, sPromoCode)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateDiscount(ByVal RowID As Integer, ByVal CompanyCode As String, RateCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Disc1 As Double, ByVal Disc2 As Double, ByVal Disc3 As Double, ByVal Disc4 As Double, ByVal Disc5 As Integer,
                                    ByVal Disc6 As Double, ByVal Disc7 As Double, ByVal Disc8 As Double, ByVal Discount As Double, ByVal Type As Integer,
                                    ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateDiscount(con, RowID, CompanyCode, RateCode, StartDate, EndDate,
                                    Disc1, Disc2, Disc3, Disc4, Disc5,
                                    Disc6, Disc7, Disc8, Discount, Type, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateDiscounts(ByVal companyCode As String, ByVal rateCode As String) As List(Of RateDiscountInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RateDiscountList As New List(Of RateDiscountInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RateDiscountList = GetRateDiscounts(con, companyCode, rateCode)
            End Using
            Return RateDiscountList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateDiscount(ByVal iRowID As Integer) As List(Of RateDiscountInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RateDiscountList As New List(Of RateDiscountInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RateDiscountList = GetRateDiscount(con, iRowID)
            End Using
            Return RateDiscountList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRateDiscount(rowID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRateDiscount(con, rowID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRatesFreeNights(ByVal CompanyCode As String, ByVal RateCode As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim result As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = GetRatesFreeNights(con, CompanyCode, RateCode)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAvailabilityRates(ByVal sCompanyId As String, ByVal ArrivalDate As Date,
                                         ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer,
                                         ByVal RoomCode As String, ByVal Culture As String) As List(Of RatesInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of RatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetAvailabilityRates(con, sCompanyId, ArrivalDate,
                                          DepartureDate, Adults, Children,
                                          RoomCode, Culture)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetRatesDiscountPaging(ByVal sCompanyCode As String, rateCode As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDiscountInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DiscountList As New List(Of RateDiscountInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                DiscountList = GetRatesDiscountPaging(con, sCompanyCode, rateCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return DiscountList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRatesDiscountCount(ByVal sCompanyCode As String, rateCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetRatesDiscountCount(con, sCompanyCode, rateCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRatesDetailCount(ByVal rateId As Integer, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetRatesDetailCount(con, rateId, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRatesDatesPaging(ByVal ratingId As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDetailInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DetailList As New List(Of RateDetailInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                DetailList = GetRatesDatesPaging(con, ratingId, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return DetailList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function spRateExceptionCount(ByVal CompanyCode As String, rateCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = spRateExceptionCount(con, CompanyCode, rateCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRatesExceptionPaging(ByVal CompanyCode As String, rateCode As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDetailInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim DetailList As New List(Of RateDetailInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                DetailList = GetRatesExceptionPaging(con, CompanyCode, rateCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return DetailList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function SaveRateHeader(con As SqlConnection, ByVal RateID As Integer, ByVal RateCode As String, ByVal Description As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal MinLos As Integer, ByVal MaxLos As Integer, ByVal Enabled As Boolean, ByVal WeekEnd As Boolean,
                                   ByVal Type As String, ByVal RateDetail As String, ByVal OrderList As Integer, ByVal FreeNights As Integer, ByVal Conditions As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateHeader"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@rate_code", RateCode)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@company_code", CompanyCode)

                If StartDate > Date.MinValue Then
                    .Parameters.AddWithValue("@start_date", StartDate)
                End If


                If EndDate > Date.MinValue Then
                    .Parameters.AddWithValue("@end_date", EndDate)
                End If


                .Parameters.AddWithValue("@min_los", MinLos)
                .Parameters.AddWithValue("@max_los", MaxLos)
                .Parameters.AddWithValue("@enabled", Enabled)
                .Parameters.AddWithValue("@weekend", WeekEnd)
                .Parameters.AddWithValue("@type", Type)
                .Parameters.AddWithValue("@rate_detail", RateDetail)
                .Parameters.AddWithValue("@rate_order", OrderList)
                .Parameters.AddWithValue("@free_nights", FreeNights)
                .Parameters.AddWithValue("@rate_conditions", Conditions)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateLocale(con As SqlConnection, ByVal RateID As Long, ByVal CultureID As Integer, ByVal Description As String,
                                   ByVal LongDescription As String, ByVal Conditions As String, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateHeaderLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@CultureID", CultureID)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@rate_detail", LongDescription)
                .Parameters.AddWithValue("@rate_conditions", Conditions)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRateHeader(con As SqlConnection, ByVal RateID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateHeader"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@company_code", "")

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateHeader(con As SqlConnection, ByVal iRateID As Integer, ByVal sCompanyCode As String, ByVal bSelectAll As Boolean) As List(Of RatesInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RatesList As New List(Of RatesInfo)()
            Dim sObjRate As New RatesInfo

            With cmd
                .CommandText = "spRateHeader"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@rate_id", iRateID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@selectAll", bSelectAll)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                sObjRate = New RatesInfo
                With sObjRate
                    .ppDescription = reader.Item("description") & ""
                    .ppEnabled = reader.Item("enabled")

                    If IsDate(reader.Item("end_date")) Then
                        .ppEndDate = reader.Item("end_date")
                        .ppStrEndDate = CDate(reader.Item("end_date")).ToShortDateString
                    End If

                    .ppMaxLos = Val(reader.Item("max_los") & "")
                    .ppMinLos = Val(reader.Item("min_los") & "")
                    .ppRateCode = reader.Item("rate_code") & ""
                    .ppRateDetail = reader.Item("rate_detail") & ""
                    .ppRateId = Val(reader.Item("rate_id") & "")

                    If IsDate(reader.Item("start_date")) Then
                        .ppStartDate = reader.Item("start_date")
                        .ppStrStartDate = CDate(reader.Item("start_date")).ToShortDateString
                    End If

                    .ppType = reader.Item("type") & ""
                    .ppWeekend = reader.Item("weekend")
                    .ppRowOrder = Val(reader.Item("roworder") & "")
                    .ppFreeNights = Val(reader.Item("free_nights") & "")
                    .ppConditions = reader.Item("rate_conditions") & ""

                End With

                RatesList.Add(sObjRate)

            End While

            reader.Close()

            Return RatesList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRoomsAvailable(con As SqlConnection, ByVal iRateID As Integer, ByVal iCultureID As Integer) As List(Of RateLocaleInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateLocalesList As New List(Of RateLocaleInfo)()
            Dim objRate As New RateLocaleInfo

            With cmd
                .CommandText = "spRateHeaderLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@rate_id", iRateID)
                .Parameters.AddWithValue("@CultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objRate = New RateLocaleInfo
                With objRate
                    .ppRateID = Val(reader.Item("rate_id") & "")
                    .ppCultureID = Val(reader.Item("cultureID") & "")
                    .ppDescription = reader.Item("description") & ""
                    .ppLongDescription = reader.Item("rate_detail") & ""
                    .ppConditions = reader.Item("rate_conditions") & ""
                End With
                RateLocalesList.Add(objRate)

            End While

            reader.Close()

            Return RateLocalesList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateDetails(con As SqlConnection, ByVal RateDetID As Long, ByVal RateID As Integer, RoomtypeID As Integer, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Double,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal Wadult1 As Double, ByVal Wadult2 As Double, ByVal Wadult3 As Double, ByVal Wadult4 As Double, ByVal Wadult5 As Double,
                                    ByVal Wadult6 As Double, ByVal Wadult7 As Double, ByVal Wadult8 As Double, ByVal Wadult9 As Double, ByVal Wadult10 As Double, ByVal Wchild As Double,
                                    CompanyID As String, ByVal sMode As String, ByVal roomcode As Integer) As Integer
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@ratedet_id", RateDetID)
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@adult1", Adult1)
                .Parameters.AddWithValue("@adult2", Adult2)
                .Parameters.AddWithValue("@adult3", Adult3)
                .Parameters.AddWithValue("@adult4", Adult4)
                .Parameters.AddWithValue("@adult5", Adult5)
                .Parameters.AddWithValue("@adult6", Adult6)
                .Parameters.AddWithValue("@adult7", Adult7)
                .Parameters.AddWithValue("@adult8", Adult8)
                .Parameters.AddWithValue("@adult9", Adult9)
                .Parameters.AddWithValue("@adult10", Adult10)
                .Parameters.AddWithValue("@child", Child)
                .Parameters.AddWithValue("@wadult1", Wadult1)
                .Parameters.AddWithValue("@wadult2", Wadult2)
                .Parameters.AddWithValue("@wadult3", Wadult3)
                .Parameters.AddWithValue("@wadult4", Wadult4)
                .Parameters.AddWithValue("@wadult5", Wadult5)
                .Parameters.AddWithValue("@wadult6", Wadult6)
                .Parameters.AddWithValue("@wadult7", Wadult7)
                .Parameters.AddWithValue("@wadult8", Wadult8)
                .Parameters.AddWithValue("@wadult9", Wadult9)
                .Parameters.AddWithValue("@wadult10", Wadult10)
                .Parameters.AddWithValue("@wchild", Wchild)
                .Parameters.AddWithValue("@roomtype_id", roomcode)
            End With


            If RateDetID = 0 Then
                RateDetID = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            Else
                dataHelper.ExecuteNonQuery(cmd, con)
            End If


            Return RateDetID

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateRoomType(con As SqlConnection, ByVal RateDetID As Long, ByVal RoomTypeID As Integer, ByVal CompanyCode As String, ByVal StartDate As Date,
                                   ByVal EndDate As Date, ByVal sMode As String, ByVal RateID As Integer) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = ""

        Try

            With cmd
                .CommandText = "spRateRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@ratedet_id", RateDetID)
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@rate_id", RateID)
            End With

            sResult = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))

            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailability(con As SqlConnection, ByVal RateCode As String, ByVal RoomTypeID As Integer, ByVal Type As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal oldStartDate As Date, oldEndDate As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, Adult1w As Double, Adult2w As Double,
                                    ByVal Adult3w As Double, ByVal Adult4w As Double, ByVal Adult5w As Double, ByVal Adult6w As Double, ByVal Adult7w As Double, ByVal Adult8w As Double,
                                    ByVal Adult9w As Double, ByVal Adult10w As Double, ByVal Child As Double, ByVal Childw As Double,
                                    ByVal CompanyID As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAvailability"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyID)
                .Parameters.AddWithValue("@rate_code", RateCode)
                .Parameters.AddWithValue("@type", Type)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@oldstart_date", oldStartDate)
                .Parameters.AddWithValue("@oldend_date", oldEndDate)
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
                .Parameters.AddWithValue("@adult1", Adult1)
                .Parameters.AddWithValue("@adult2", Adult2)
                .Parameters.AddWithValue("@adult3", Adult3)
                .Parameters.AddWithValue("@adult4", Adult4)
                .Parameters.AddWithValue("@adult5", Adult5)
                .Parameters.AddWithValue("@adult6", Adult6)
                .Parameters.AddWithValue("@adult7", Adult7)
                .Parameters.AddWithValue("@adult8", Adult8)
                .Parameters.AddWithValue("@adult9", Adult9)
                .Parameters.AddWithValue("@adult10", Adult10)
                .Parameters.AddWithValue("@adult1w", Adult1w)
                .Parameters.AddWithValue("@adult2w", Adult2w)
                .Parameters.AddWithValue("@adult3w", Adult3w)
                .Parameters.AddWithValue("@adult4w", Adult4w)
                .Parameters.AddWithValue("@adult5w", Adult5w)
                .Parameters.AddWithValue("@adult6w", Adult6w)
                .Parameters.AddWithValue("@adult7w", Adult7w)
                .Parameters.AddWithValue("@adult8w", Adult8w)
                .Parameters.AddWithValue("@adult9w", Adult9w)
                .Parameters.AddWithValue("@adult10w", Adult10w)
                .Parameters.AddWithValue("@child", Child)
                .Parameters.AddWithValue("@childw", Childw)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRateDetails(con As SqlConnection, ByVal RateDetID As Integer, CompanyCode As String, RoomTypeCode As String, StartDate As Date, EndDate As Date) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@ratedet_id", RateDetID)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@roomtype_code", RoomTypeCode)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateDetails(con As SqlConnection, ByVal iRateDetID As Integer) As List(Of RateDetailInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateLocalesList As New List(Of RateDetailInfo)()
            Dim RateDetail As New RateDetailInfo

            With cmd
                .CommandText = "spRateDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@ratedet_id", iRateDetID)
                .Parameters.AddWithValue("@rate_id", 0)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDetail = New RateDetailInfo
                With RateDetail
                    .ppRate_id = Val(reader.Item("rate_id") & "")
                    .ppRateDet_id = Val(reader.Item("ratedet_id") & "")

                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                    End If
                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                    End If
                    .ppAdult1 = reader.Item("adult1")
                    .ppAdult2 = reader.Item("adult2")
                    .ppAdult3 = reader.Item("adult3")
                    .ppAdult4 = reader.Item("adult4")
                    .ppAdult5 = reader.Item("adult5")
                    .ppAdult6 = reader.Item("adult6")
                    .ppAdult7 = reader.Item("adult7")
                    .ppAdult8 = reader.Item("adult8")
                    .ppAdult9 = reader.Item("adult9")
                    .ppAdult10 = reader.Item("adult10")
                    .ppChild = reader.Item("child")
                    .ppWAdult1 = reader.Item("wadult1")
                    .ppWAdult2 = reader.Item("wadult2")
                    .ppWAdult3 = reader.Item("wadult3")
                    .ppWAdult4 = reader.Item("wadult4")
                    .ppWAdult5 = reader.Item("wadult5")
                    .ppWAdult6 = reader.Item("wadult6")
                    .ppWAdult7 = reader.Item("wadult7")
                    .ppWAdult8 = reader.Item("wadult8")
                    .ppWAdult9 = reader.Item("wadult9")
                    .ppWAdult10 = reader.Item("wadult10")
                    .ppWChild = reader.Item("wchild")
                    .ppRoomtype_id = Val(reader.Item("roomtype_id") & "")
                    .ppRoomName = reader.Item("room_name") & ""
                End With
                RateLocalesList.Add(RateDetail)

            End While

            reader.Close()

            Return RateLocalesList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateDates(con As SqlConnection, ByVal iRateID As Int16) As List(Of RateDetailInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateLocalesList As New List(Of RateDetailInfo)()
            Dim RateDetail As New RateDetailInfo

            With cmd
                .CommandText = "spRateDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@ratedet_id", 0)
                .Parameters.AddWithValue("@rate_id", iRateID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDetail = New RateDetailInfo
                With RateDetail
                    .ppRateDet_id = Val(reader.Item("ratedet_id") & "")
                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                    End If
                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                    End If
                    .ppAdult1 = reader.Item("adult1")
                    .ppAdult2 = reader.Item("adult2")
                    .ppAdult3 = reader.Item("adult3")
                    .ppAdult4 = reader.Item("adult4")
                    .ppAdult5 = reader.Item("adult5")
                    .ppAdult6 = reader.Item("adult6")
                    .ppChild = reader.Item("child")
                    .ppRoomName = reader.Item("room_name") & ""
                    .ppRoomCode = reader.Item("roomtype_code") & ""
                End With
                RateLocalesList.Add(RateDetail)

            End While

            reader.Close()

            Return RateLocalesList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateExcDates(con As SqlConnection, sCompanyCode As String, ByVal sRateCode As String, sRoomTypeCode As String, ByVal dStart As Date, ByVal dEnd As Date) As List(Of RateDetailInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateLocalesList As New List(Of RateDetailInfo)()
            Dim RateDetail As New RateDetailInfo

            With cmd
                .CommandText = "spRateExceptions"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@rate_code", sRateCode)
                .Parameters.AddWithValue("@roomtype_code", sRoomTypeCode)
                If sRoomTypeCode <> "" Then
                    .Parameters.AddWithValue("@start_date", dStart)
                    .Parameters.AddWithValue("@end_date", dEnd)
                End If
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDetail = New RateDetailInfo
                With RateDetail
                    .ppRateCode = reader.Item("rate_code") & ""
                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                    End If
                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                    End If
                    .ppAdult1 = reader.Item("adult1")
                    .ppAdult2 = reader.Item("adult2")
                    .ppAdult3 = reader.Item("adult3")
                    .ppAdult4 = reader.Item("adult4")
                    .ppAdult5 = reader.Item("adult5")
                    .ppAdult6 = reader.Item("adult6")
                    .ppChild = reader.Item("child")
                    .ppRoomName = reader.Item("room_name") & ""
                    .ppRoomCode = reader.Item("room_code") & ""
                End With
                RateLocalesList.Add(RateDetail)

            End While

            reader.Close()

            Return RateLocalesList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRateExcDates(con As SqlConnection, sCompanyCode As String, rowId As Integer, ByVal sRateCode As String, sRoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spDeleteRateExceptions"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@RowId ", rowId)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@rate_code", sRateCode)
                .Parameters.AddWithValue("@roomtype_code", sRoomTypeCode)
                .Parameters.AddWithValue("@start_date", StartDate.Date)
                .Parameters.AddWithValue("@end_date", EndDate.Date)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateRoomType(con As SqlConnection, ByVal RateDetID As Integer, sCompanyCode As String) As List(Of RateRoomTypeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RRateRoomTypeList As New List(Of RateRoomTypeInfo)()
            Dim RateRoomType As New RateRoomTypeInfo

            With cmd
                .CommandText = "spRateDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@ratedet_id", RateDetID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateRoomType = New RateRoomTypeInfo
                With RateRoomType
                    .ppDescription = reader.Item("description") & ""
                    .ppRateDet_id = Val(reader.Item("ratedet_id") & "")
                    .ppRoomType_id = Val(reader.Item("roomtype_id") & "")
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                End With
                RRateRoomTypeList.Add(RateRoomType)

            End While

            reader.Close()

            Return RRateRoomTypeList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRateRoomType(con As SqlConnection, ByVal RateDetID As Integer, ByVal RoomTypeID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@ratedet_id", RateDetID)
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateException(con As SqlConnection, ByVal CompanyCode As String, RateCode As String, RoomtypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                      ByVal StartDateAnt As Date, ByVal EndDateAnt As Date,
                                    ByVal Adult1 As Double, ByVal Adult2 As Double, ByVal Adult3 As Double, ByVal Adult4 As Double, ByVal Adult5 As Integer,
                                    ByVal Adult6 As Double, ByVal Adult7 As Double, ByVal Adult8 As Double, ByVal Adult9 As Double, ByVal Adult10 As Double, ByVal Child As Double,
                                    ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateExceptions"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@rate_code", RateCode)
                .Parameters.AddWithValue("@roomtype_code", RoomtypeCode)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                If sMode = "UPD" Then
                    .Parameters.AddWithValue("@start_dateAnt", StartDateAnt)
                    .Parameters.AddWithValue("@end_dateAnt", EndDateAnt)
                End If
                .Parameters.AddWithValue("@adult1", Adult1)
                .Parameters.AddWithValue("@adult2", Adult2)
                .Parameters.AddWithValue("@adult3", Adult3)
                .Parameters.AddWithValue("@adult4", Adult4)
                .Parameters.AddWithValue("@adult5", Adult5)
                .Parameters.AddWithValue("@adult6", Adult6)
                .Parameters.AddWithValue("@adult7", Adult7)
                .Parameters.AddWithValue("@adult8", Adult8)
                .Parameters.AddWithValue("@adult9", Adult9)
                .Parameters.AddWithValue("@adult10", Adult10)
                .Parameters.AddWithValue("@child", Child)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRates(con As SqlConnection, ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateList As New List(Of RatesInfo)()
            Dim Rate As New RatesInfo

            With cmd
                .CommandText = "spGetAvailableRates"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", iAdults)
                .Parameters.AddWithValue("children", iChildren)
                .Parameters.AddWithValue("culture", "en-US")
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Rate = New RatesInfo
                With Rate

                    .ppRateCode = reader.Item("rate_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("min_value")) Then
                        .ppMinValue = reader.Item("min_value")
                    End If
                    .ppType = reader.Item("type") & ""
                    .ppRateDetail = reader.Item("rate_detail") & ""
                    .ppMinLos = Val(reader.Item("min_los") & "")
                    .ppMaxLos = Val(reader.Item("max_los") & "")
                    If IsNumeric(reader.Item("discount")) Then
                        .ppDiscount = reader.Item("discount")
                    End If

                End With
                RateList.Add(Rate)

            End While

            reader.Close()

            Return RateList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetRates_v1(con As SqlConnection, ByVal sCompanyId As String, RoomType As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateList As New List(Of RatesInfo)()
            Dim Rate As New RatesInfo

            With cmd
                .CommandText = "spGetAvailableRates_v1"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("room_type", RoomType)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", iAdults)
                .Parameters.AddWithValue("children", iChildren)
                .Parameters.AddWithValue("culture", "en-US")
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Rate = New RatesInfo
                With Rate

                    .ppRateCode = reader.Item("rate_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("min_value")) Then
                        .ppMinValue = reader.Item("min_value")
                    End If
                    .ppType = reader.Item("type") & ""
                    .ppRateDetail = reader.Item("rate_detail") & ""
                    .ppMinLos = Val(reader.Item("min_los") & "")
                    .ppMaxLos = Val(reader.Item("max_los") & "")
                    If IsNumeric(reader.Item("discount")) Then
                        .ppDiscount = reader.Item("discount")
                    End If

                End With
                RateList.Add(Rate)

            End While

            reader.Close()

            Return RateList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function




    Friend Function GetRatesWithPromoCode(con As SqlConnection, ByVal sCompanyId As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal iAdults As Integer, ByVal iChildren As Integer, ByVal sPromoCode As String) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateList As New List(Of RatesInfo)()
            Dim Rate As New RatesInfo

            With cmd
                .CommandText = "spGetAvailableRatesPromoCode"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", iAdults)
                .Parameters.AddWithValue("children", iChildren)
                .Parameters.AddWithValue("promo_code", sPromoCode)
                .Parameters.AddWithValue("culture", "en-US")
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Rate = New RatesInfo
                With Rate

                    .ppRateCode = reader.Item("rate_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("min_value")) Then
                        .ppMinValue = reader.Item("min_value")
                    End If
                    .ppType = reader.Item("type") & ""
                    .ppRateDetail = reader.Item("rate_detail") & ""
                    .ppMinLos = Val(reader.Item("min_los") & "")
                    .ppMaxLos = Val(reader.Item("max_los") & "")

                    If IsNumeric(reader.Item("discount")) Then
                        .ppDiscount = reader.Item("discount")
                    End If

                End With
                RateList.Add(Rate)

            End While

            reader.Close()

            Return RateList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateDiscount(con As SqlConnection, ByVal RowID As Integer, ByVal CompanyCode As String, RateCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                    ByVal Disc1 As Double, ByVal Disc2 As Double, ByVal Disc3 As Double, ByVal Disc4 As Double, ByVal Disc5 As Integer,
                                    ByVal Disc6 As Double, ByVal Disc7 As Double, ByVal Disc8 As Double, ByVal Discount As Double, ByVal Type As Integer,
                                    ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spRateDiscount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@rowID", RowID)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@rate_code", RateCode)
                .Parameters.AddWithValue("@type", Type)
                .Parameters.AddWithValue("@discount", Discount)
                .Parameters.AddWithValue("@disc1", Disc1)
                .Parameters.AddWithValue("@disc2", Disc2)
                .Parameters.AddWithValue("@disc3", Disc3)
                .Parameters.AddWithValue("@disc4", Disc4)
                .Parameters.AddWithValue("@disc5", Disc5)
                .Parameters.AddWithValue("@disc6", Disc6)
                .Parameters.AddWithValue("@disc7", Disc7)
                .Parameters.AddWithValue("@disc8", Disc8)
                .Parameters.AddWithValue("@dateFrom", StartDate)
                .Parameters.AddWithValue("@dateTo", EndDate)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateDiscounts(con As SqlConnection, ByVal companyCode As String, ByVal rateCode As String) As List(Of RateDiscountInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateDiscountList As New List(Of RateDiscountInfo)()
            Dim RateDiscount As New RateDiscountInfo

            With cmd
                .CommandText = "spRateDiscount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SE1")
                .Parameters.AddWithValue("@company_code", companyCode)
                .Parameters.AddWithValue("@rate_code", rateCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDiscount = New RateDiscountInfo
                With RateDiscount
                    .ppRowID = Val(reader.Item("rowID") & "")
                    .ppType = Val(reader.Item("type") & "")

                    If IsDate(reader.Item("dateFrom")) Then
                        .ppStartDate = reader.Item("dateFrom")
                    End If

                    If IsDate(reader.Item("dateTo")) Then
                        .ppEndDate = reader.Item("dateTo")
                    End If
                    .ppDisc1 = reader.Item("disc1")
                    .ppDisc2 = reader.Item("disc2")
                    .ppDisc3 = reader.Item("disc3")
                    .ppDisc4 = reader.Item("disc4")
                    .ppDisc5 = reader.Item("disc5")
                    .ppDisc6 = reader.Item("disc6")
                    .ppDisc7 = reader.Item("disc7")
                    .ppDisc8 = reader.Item("disc8")
                    .ppDiscount = reader.Item("discount")
                    If .ppType = 1 Then
                        .ppTypeDesc = "General"
                    Else
                        .ppTypeDesc = "Per Nights"
                    End If
                End With
                RateDiscountList.Add(RateDiscount)

            End While

            reader.Close()

            Return RateDiscountList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateDiscount(con As SqlConnection, iRowID As Integer) As List(Of RateDiscountInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateDiscountList As New List(Of RateDiscountInfo)()
            Dim RateDiscount As New RateDiscountInfo

            With cmd
                .CommandText = "spRateDiscount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SE2")
                .Parameters.AddWithValue("@rowID", iRowID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDiscount = New RateDiscountInfo
                With RateDiscount
                    .ppRowID = Val(reader.Item("rowID") & "")
                    .ppType = Val(reader.Item("type") & "")

                    If IsDate(reader.Item("dateFrom")) Then
                        .ppStartDate = reader.Item("dateFrom")
                    End If

                    If IsDate(reader.Item("dateTo")) Then
                        .ppEndDate = reader.Item("dateTo")
                    End If
                    .ppDisc1 = reader.Item("disc1")
                    .ppDisc2 = reader.Item("disc2")
                    .ppDisc3 = reader.Item("disc3")
                    .ppDisc4 = reader.Item("disc4")
                    .ppDisc5 = reader.Item("disc5")
                    .ppDisc6 = reader.Item("disc6")
                    .ppDisc7 = reader.Item("disc7")
                    .ppDisc8 = reader.Item("disc8")
                    .ppDiscount = reader.Item("discount")
                    If .ppType = 1 Then
                        .ppTypeDesc = "General"
                    Else
                        .ppTypeDesc = "Per Nights"
                    End If
                End With
                RateDiscountList.Add(RateDiscount)

            End While

            reader.Close()

            Return RateDiscountList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRateDiscount(con As SqlConnection, ByVal rowID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRateDiscount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@rowID", rowID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAvailabilityRates(con As SqlConnection, ByVal sCompanyId As String, ByVal ArrivalDate As Date,
                                         ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer,
                                         ByVal RoomCode As String, ByVal Culture As String) As List(Of RatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateList As New List(Of RatesInfo)()
            Dim Rate As New RatesInfo

            With cmd
                .CommandText = "spGetAvailabilityRoomsRates"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", Adults)
                .Parameters.AddWithValue("children", Children)
                .Parameters.AddWithValue("roomtype_code", RoomCode)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Rate = New RatesInfo
                With Rate

                    .ppRateCode = reader.Item("rate_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("min_value")) Then
                        .ppMinValue = reader.Item("min_value")
                    End If
                    .ppType = reader.Item("type") & ""
                    .ppRateDetail = reader.Item("rate_detail") & ""
                    .ppMinLos = Val(reader.Item("min_los") & "")
                    .ppMaxLos = Val(reader.Item("max_los") & "")

                    If IsNumeric(reader.Item("discount")) Then
                        .ppDiscount = reader.Item("discount")
                    End If

                End With

                RateList.Add(Rate)

            End While

            reader.Close()

            Return RateList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRatesFreeNights(con As SqlConnection, ByVal CompanyCode As String, ByVal RateCode As String) As Integer

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try


            cmd.CommandText = "GetFreeNights"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("CompanyCode", CompanyCode)
            cmd.Parameters.AddWithValue("RateCode", RateCode)

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetRatesDiscountPaging(con As SqlConnection, ByVal sCompanyCode As String, rateCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDiscountInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateDiscountList As New List(Of RateDiscountInfo)()
            Dim RateDiscount As New RateDiscountInfo

            With cmd
                .CommandText = "spGetRatesDiscountPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("rate_code ", rateCode)
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDiscount = New RateDiscountInfo
                With RateDiscount
                    .ppRowID = Val(reader.Item("rowID") & "")
                    .ppType = Val(reader.Item("type") & "")

                    If IsDate(reader.Item("dateFrom")) Then
                        .ppStartDate = reader.Item("dateFrom")
                        .FormatFrom = .ppStartDate.ToShortDateString
                    End If

                    If IsDate(reader.Item("dateTo")) Then
                        .ppEndDate = reader.Item("dateTo")
                        .FormatTo = .ppEndDate.ToShortDateString
                    End If
                    .ppDisc1 = reader.Item("disc1")
                    .ppDisc2 = reader.Item("disc2")
                    .ppDisc3 = reader.Item("disc3")
                    .ppDisc4 = reader.Item("disc4")
                    .ppDisc5 = reader.Item("disc5")
                    .ppDisc6 = reader.Item("disc6")
                    .ppDisc7 = reader.Item("disc7")
                    .ppDisc8 = reader.Item("disc8")
                    .ppDiscount = reader.Item("discount")
                    If .ppType = 1 Then
                        .ppTypeDesc = "General"
                    Else
                        .ppTypeDesc = "Per Nights"
                    End If
                End With
                RateDiscountList.Add(RateDiscount)

            End While

            reader.Close()

            Return RateDiscountList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRatesDiscountCount(con As SqlConnection, ByVal sCompanyCode As String, rate_code As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetRatesDiscountCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("rate_code", rate_code)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRatesDatesPaging(con As SqlConnection, RateId As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDetailInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateDatesList As New List(Of RateDetailInfo)()
            Dim RateDetail As New RateDetailInfo

            With cmd
                .CommandText = "spGetRatesDatesPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("rate_id", RateId)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDetail = New RateDetailInfo
                With RateDetail
                    .ppRateDet_id = Val(reader.Item("ratedet_id") & "")
                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                        .FormatStartDate = .ppStart_date.ToShortDateString
                    End If
                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                        .FormatEndDate = .ppEnd_date.ToShortDateString
                    End If

                    If IsNumeric(reader.Item("adult1")) Then
                        .ppAdult1 = CDbl(reader.Item("adult1"))
                    End If

                    If IsNumeric(reader.Item("adult2")) Then
                        .ppAdult2 = CDbl(reader.Item("adult2"))
                    End If

                    If IsNumeric(reader.Item("adult3")) Then
                        .ppAdult3 = CDbl(reader.Item("adult3"))
                    End If

                    If IsNumeric(reader.Item("adult4")) Then
                        .ppAdult4 = CDbl(reader.Item("adult4"))
                    End If
                    If IsNumeric(reader.Item("adult5")) Then
                        .ppAdult5 = CDbl(reader.Item("adult5"))
                    End If

                    If IsNumeric(reader.Item("adult6")) Then
                        .ppAdult6 = CDbl(reader.Item("adult6"))
                    End If

                    If IsNumeric(reader.Item("adult7")) Then
                        .ppAdult7 = CDbl(reader.Item("adult7"))
                    End If

                    If IsNumeric(reader.Item("adult8")) Then
                        .ppAdult8 = CDbl(reader.Item("adult8"))
                    End If

                    If IsNumeric(reader.Item("adult9")) Then
                        .ppAdult9 = CDbl(reader.Item("adult9"))
                    End If

                    If IsNumeric(reader.Item("adult10")) Then
                        .ppAdult10 = CDbl(reader.Item("adult10"))
                    End If

                    If IsNumeric(reader.Item("child")) Then
                        .ppChild = CDbl(reader.Item("child"))
                    End If


                    If IsNumeric(reader.Item("child")) Then
                        .ppChild = CDbl(reader.Item("child"))
                    End If

                    If IsNumeric(reader.Item("wchild")) Then
                        .ppWChild = CDbl(reader.Item("wchild"))
                    End If



                    If IsNumeric(reader.Item("wadult1")) Then
                        .ppWAdult1 = CDbl(reader.Item("wadult1"))
                    End If

                    If IsNumeric(reader.Item("wadult2")) Then
                        .ppWAdult2 = CDbl(reader.Item("wadult2"))
                    End If

                    If IsNumeric(reader.Item("wadult3")) Then
                        .ppWAdult3 = CDbl(reader.Item("wadult3"))
                    End If

                    If IsNumeric(reader.Item("wadult4")) Then
                        .ppWAdult4 = CDbl(reader.Item("wadult4"))
                    End If
                    If IsNumeric(reader.Item("wadult5")) Then
                        .ppWAdult5 = CDbl(reader.Item("wadult5"))
                    End If

                    If IsNumeric(reader.Item("wadult6")) Then
                        .ppWAdult6 = CDbl(reader.Item("wadult6"))
                    End If

                    If IsNumeric(reader.Item("wadult7")) Then
                        .ppWAdult7 = CDbl(reader.Item("wadult7"))
                    End If

                    If IsNumeric(reader.Item("wadult8")) Then
                        .ppWAdult8 = CDbl(reader.Item("wadult8"))
                    End If

                    If IsNumeric(reader.Item("wadult9")) Then
                        .ppWAdult9 = CDbl(reader.Item("wadult9"))
                    End If

                    If IsNumeric(reader.Item("wadult10")) Then
                        .ppWAdult10 = CDbl(reader.Item("wadult10"))
                    End If

                    .ppRoomName = reader.Item("room_name") & ""
                    .ppRoomCode = reader.Item("roomtype_code") & ""
                End With
                RateDatesList.Add(RateDetail)

            End While

            reader.Close()

            Return RateDatesList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRatesDetailCount(con As SqlConnection, ByVal rateId As Integer, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetRatesDatesCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("rate_id", rateId)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function spRateExceptionCount(con As SqlConnection, ByVal company_code As String, rate_code As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spRateExceptionCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("rate_code", rate_code)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRatesExceptionPaging(con As SqlConnection, company_code As String, rate_code As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RateDetailInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RateExcList As New List(Of RateDetailInfo)()
            Dim RateDetail As New RateDetailInfo

            With cmd
                .CommandText = "spGetRatesExceptionPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("rate_code", rate_code)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                RateDetail = New RateDetailInfo
                With RateDetail
                    .ppRateCode = reader.Item("rate_code") & ""
                    If IsDate(reader.Item("start_date")) Then
                        .ppStart_date = CDate(reader.Item("start_date"))
                        .FormatStartDate = .ppStart_date.ToShortDateString
                    End If
                    If IsDate(reader.Item("end_date")) Then
                        .ppEnd_date = CDate(reader.Item("end_date"))
                        .FormatEndDate = .ppEnd_date.ToShortDateString
                    End If
                    .ppAdult1 = reader.Item("adult1")
                    .ppAdult2 = reader.Item("adult2")
                    .ppAdult3 = reader.Item("adult3")
                    .ppAdult4 = reader.Item("adult4")
                    .ppAdult5 = reader.Item("adult5")
                    .ppAdult6 = reader.Item("adult6")
                    .ppChild = reader.Item("child")
                    .ppRoomName = reader.Item("room_name") & ""
                    .ppRoomCode = reader.Item("room_code") & ""
                    .ppRowID = Val(reader.Item("RowId") & "")
                End With

                RateExcList.Add(RateDetail)


            End While

            reader.Close()

            Return RateExcList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
