﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Configuration

Public Class Processes

#Region "Public Methods"

    Public Function BlockRoomsQuo(ByVal QuotationID As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = BlockRoomsQuo(con, QuotationID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function ChangeVoidDateQuo(ByVal QuotationID As String, ByVal NewDate As Date) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = ChangeVoidDateQuo(con, QuotationID, NewDate)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveDepositBankID(ByVal sXML As String, ByVal sDeposit As String, sTransID As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveDepositBankID(con, sXML, sDeposit, sTransID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveGuest(ByVal sMode As String, ByVal objGuest As clsGuestsInfo) As Long
        Try
            Dim dataHelper As New DataHelper()
            Dim response As Long

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveGuest(con, sMode, objGuest)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function SaveReservation(ByVal sMode As String, ByVal objRes As clsReservationsInfo) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveReservation(con, objRes)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveAvailabilityCalendar(ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal objRes As List(Of clsQryCalendar), ByVal UserID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailabilityCalendar(con, CompanyCode, RoomTypeCode, objRes, UserID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailabilityCalendarService(ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal objRes As List(Of clsQryCalendarService), ByVal UserID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailabilityCalendarService(con, ServiceID, ScheduleID, objRes, UserID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveReservation(ByVal objRes As List(Of clsReservationsRoomInfo)) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveReservation(con, objRes)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveReservationAddons(ByVal objAddons As List(Of AddonInfo), ByVal sReservation As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveReservationAddons(con, objAddons, sReservation)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveApprovalCode(ByVal ReservationID As String, Code As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveApprovalCode(con, ReservationID, Code)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function BlackOutDates(ByVal sCompanyCode As String, ByVal sRoomtypeCode As String, ByVal dFrom As Date, ByVal dTo As Date) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = BlackOutDates(con, sCompanyCode, sRoomtypeCode, dFrom, dTo)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SendEmailPwdReminder(ByVal Email As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SendEmailPwdReminder(con, Email)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SendEmailCounter(ByVal HotelName As String, ByVal HotelEmail As String, ByVal HotelEmailAlt As String, ByVal HotelCode As String,
                            ByVal CustEmail As String, ByVal CustName As String, ByVal ReservationID As String,
                            ByVal CheckIn As Date, ByVal CheckOut As Date, ByVal RateCode As String,
                            ByVal Subtotal As Double, ByVal Addon As Double, ByVal Promo As Double, ByVal Tax As Double, ByVal Total As Double,
                            ByVal Nights As Integer, ByVal CantAddons As Integer) As Boolean

        Dim strFrom = HotelName & " <info@trtconsultores.com>"
        Dim strFrom2 As String = "bookingtrtinteractive.com <info@trtconsultores.com>"
        Dim strTo = CustEmail
        Dim mBody As String = ""
        Dim objReservations As New Reservations
        Dim fp As StreamReader
        Dim j As Integer
        Dim i As Integer = 0
        Dim dBalance As Double
        Dim objRoomAvailable As List(Of clsReservationsRoomInfo)
        Dim strRooms As New StringBuilder("")


        Dim mailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
        mailMsg.BodyEncoding = Encoding.Default
        mailMsg.Subject = "Reservation Request"

        Dim mailMsg2 As New MailMessage(New MailAddress(strFrom2.Trim()), New MailAddress(HotelEmail))
        If HotelEmailAlt.Trim <> String.Empty Then
            mailMsg2.Bcc.Add(HotelEmailAlt)
        End If

        mailMsg.BodyEncoding = Encoding.Default
        mailMsg2.BodyEncoding = Encoding.Default
        mailMsg2.Subject = "Quotation Information"

        Try
            fp = File.OpenText("E:\WebApps\hotelAppTest\hotel\resources\" & HotelCode & "\emailQuo.html")
            'fp = File.OpenText("C:\hotelApp\hotel\resources\templates\emailQuo.html")

            mBody = fp.ReadToEnd()
            fp.Close()

            'Replace tags in the body
            mBody.Replace("<$companyID$>", HotelCode)
            mBody.Replace("<$confirmation$>", ReservationID)
            mBody.Replace("<$date$>", Now.Date.ToShortDateString)
            mBody.Replace("<$checkinout$>", "Check In: " & CheckIn.ToShortDateString & " | Check Out: " & CheckOut.ToShortDateString)
            mBody.Replace("<$nights$>", Nights.ToString)

            'Guest Details
            mBody.Replace("<$name$>", CustName)
            mBody.Replace("<$email$>", CustEmail)


            Dim mNochesGratis As Integer = objReservations.GetFreeNights(HotelCode, RateCode)

            objRoomAvailable = objReservations.GetReservationRoomsQuo(ReservationID, "en-US")

            For j = 0 To objRoomAvailable.Count - 1
                strRooms.Append("<tr>")
                strRooms.Append("< td width=" & Chr(34) & "100" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans - serif; font-size:  12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objRoomAvailable(j).ppDate.ToShortDateString & "</p></td>")
                strRooms.Append(" <td width=" & Chr(34) & "291" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & objRoomAvailable(j).ppDescription & "</p></td>")
                strRooms.Append("<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppAdults & "</p></td>")
                strRooms.Append("<td width=" & Chr(34) & "50" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppChildren & "</p></td>")
                strRooms.Append("<td width=" & Chr(34) & "70" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & objRoomAvailable(j).ppChildrenFree & "</p></td>")
                If i = Nights Then
                    If mNochesGratis = 0 Then
                        strRooms.Append("<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(objRoomAvailable(j).ppRateAmount, "c") & "</p></td>")
                        dBalance = objRoomAvailable(j).ppRateAmount
                    Else
                        strRooms.Append("<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">0.00</p></td>")
                        dBalance = 0
                    End If
                Else
                    strRooms.Append("<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(objRoomAvailable(j).ppRateAmount, "c") & "</p></td>")
                    dBalance = objRoomAvailable(j).ppRateAmount
                End If
                strRooms.Append("</tr>")
            Next

            If CantAddons > 0 Then
                Dim objAddons As New Addons
                Dim objAddon As List(Of AddonInfo)
                objAddon = objAddons.GetReservationAddonsQuo(ReservationID, "en-US")
                For i = 0 To objAddon.Count - 1
                    With objAddon(i)
                        strRooms.Append("<tr>")
                        strRooms.Append("<td width=" & Chr(34) & "100" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & .ppQty & "</p></td>")
                        strRooms.Append("<td width=" & Chr(34) & "291" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;" & .ppDescription & "</p></td>")
                        strRooms.Append("<td width=" & Chr(34) & "50" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>")
                        strRooms.Append("<td width=" & Chr(34) & "50" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>")
                        strRooms.Append("<td width=" & Chr(34) & "70" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">&nbsp;</p></td>")
                        strRooms.Append("<td width=" & Chr(34) & "100" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & "><p style=" & Chr(34) & "font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; margin: 0; padding: 0;" & Chr(34) & ">" & Format(.ppAmount * .ppQty, "c") & "</p></td>")
                        strRooms.Append("</tr>")
                    End With
                Next
            End If

            mBody.Replace("<$roomsTable$>", strRooms.ToString)
            mBody.Replace("<$link$>", "https://www.bookingtrtinteractive.com/BSP/hotel/reservation.aspx?hotelId=" & HotelCode & "&resCode=" & ReservationID)
            mBody.Replace("<$subtotal$>", Format(Subtotal, "n"))
            mBody.Replace("<$discount$>", Format(Promo, "n"))
            mBody.Replace("<$addons$>", Format(Addon, "n"))
            If Tax > 0 Then
                mBody.Replace("<$tax$>", Format(Tax, "n"))
            Else
                mBody.Replace("<$tax$>", "Included")
            End If
            mBody.Replace("<$total$>", Format(Total, "n"))

            mailMsg.Body = mBody
            mailMsg2.Body = mBody
            mailMsg.Priority = MailPriority.High
            mailMsg.IsBodyHtml = True
            mailMsg2.Priority = MailPriority.Normal
            mailMsg2.IsBodyHtml = True

            Dim smtpMail As New SmtpClient
            smtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
            smtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
            smtpMail.Send(mailMsg2)
            smtpMail.Send(mailMsg)

            Return True
        Catch ex As Exception
            EventLog.WriteEntry("Application", "SendEmail: " & ex.ToString, EventLogEntryType.Error)
            Return False
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function BlockRoomsQuo(con As SqlConnection, ByVal QuotationID As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spBlockRoomsQuo"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@quotation_id", QuotationID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function ChangeVoidDateQuo(con As SqlConnection, ByVal QuotationID As String, ByVal NewDate As Date) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spChangeVoidDateQuo"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("quotation_id", QuotationID)
                .Parameters.AddWithValue("void_date", NewDate)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveDepositBankID(con As SqlConnection, ByVal sXML As String, ByVal sDeposit As String, sTransID As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Dim xmlDoc As New System.Xml.XmlDocument


        Try
            xmlDoc.LoadXml(sXML)

            With cmd
                .Connection = con
                .CommandType = CommandType.Text
            End With

            For i = 0 To xmlDoc.ChildNodes(0).ChildNodes.Count - 1
                With xmlDoc.ChildNodes(0).ChildNodes(i)
                    If sDeposit = String.Empty Then
                        cmd.CommandText = "UPDATE dbo.tblReservations SET transaction_bank = '" & sTransID & "' WHERE reservation_id = '" & .ChildNodes(0).InnerText & "'"
                    Else
                        cmd.CommandText = "UPDATE dbo.tblReservations SET deposit_number = '" & sDeposit & "' WHERE reservation_id = '" & .ChildNodes(0).InnerText & "'"
                    End If
                    cmd.ExecuteNonQuery()
                End With
            Next

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveGuest(con As SqlConnection, ByVal sMode As String, ByVal objGuest As clsGuestsInfo) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try

            With cmd
                .CommandText = "spGuests"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@guest_id", objGuest.ppGuestID)
                .Parameters.AddWithValue("@name", objGuest.ppName)
                .Parameters.AddWithValue("@lastname", objGuest.ppLastname)
                .Parameters.AddWithValue("@company_code", objGuest.ppCompany_Code)
                .Parameters.AddWithValue("@creation_date", objGuest.ppCreationDate)
                .Parameters.AddWithValue("@telephone_nr", objGuest.ppTelephone)
                .Parameters.AddWithValue("@country_iso3", objGuest.ppCountry_Iso3)
                .Parameters.AddWithValue("@email", objGuest.ppEmail)
            End With

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function SaveGuestsByReservation(con As SqlConnection, ByVal ReservationID As String, ByVal objGuest As clsGuestsInfo) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try

            With cmd
                .CommandText = "spGuestsByReservation"
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                .Parameters.AddWithValue("@ReservationID", ReservationID)
                .Parameters.AddWithValue("@name", objGuest.ppName)
                .Parameters.AddWithValue("@lastname", objGuest.ppLastname)
                .Parameters.AddWithValue("@company_code", objGuest.ppCompany_Code)
                .Parameters.AddWithValue("@creation_date", objGuest.ppCreationDate)
                .Parameters.AddWithValue("@telephone_nr", objGuest.ppTelephone)
                .Parameters.AddWithValue("@country_iso3", objGuest.ppCountry_Iso3)
                .Parameters.AddWithValue("@email", objGuest.ppEmail)
            End With

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveReservation(con As SqlConnection, ByVal objRes As clsReservationsInfo) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As String

        Try

            With cmd
                .CommandText = "spQuotations"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "INS")
                .Parameters.AddWithValue("guest_id", objRes.ppGuestID)
                .Parameters.AddWithValue("company_code", objRes.ppCompany_Code)
                .Parameters.AddWithValue("arrival", objRes.ppArrival)
                .Parameters.AddWithValue("departure", objRes.ppDeparture)
                .Parameters.AddWithValue("creation_date", objRes.ppCreation_Date)
                .Parameters.AddWithValue("rate_code", objRes.ppRate_Code)
                .Parameters.AddWithValue("rate_amount", objRes.ppRateAmount)
                .Parameters.AddWithValue("tax_amount", objRes.ppTaxAmount)
                .Parameters.AddWithValue("promo_id", objRes.ppPromoID)
                .Parameters.AddWithValue("addon_amount", objRes.ppAddonAmount)
                .Parameters.AddWithValue("promo_amount", objRes.ppPromoAmount)
                .Parameters.AddWithValue("rooms", objRes.ppRooms)
                .Parameters.AddWithValue("void", objRes.ppVoid)
            End With


            lResult = dataHelper.ExecuteScalar(cmd, con)


            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailabilityCalendar(con As SqlConnection, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal objRes As List(Of clsQryCalendar), ByVal UserID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            With cmd
                .CommandText = "spAvailabilityCal"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With


            For i = 0 To objRes.Count - 1
                If objRes(i).ppDay1 <> -10 Then
                    With objRes(i)
                        cmd.Parameters.Clear()
                        cmd.Parameters.AddWithValue("company_code", CompanyCode)
                        cmd.Parameters.AddWithValue("roomtype_code", RoomTypeCode)
                        cmd.Parameters.AddWithValue("date", .ppDate)
                        cmd.Parameters.AddWithValue("type", .ppType)
                        cmd.Parameters.AddWithValue("qty", .ppDay1)
                        cmd.Parameters.AddWithValue("userId", UserID)
                    End With
                    dataHelper.ExecuteNonQuery(cmd, con)
                End If
            Next

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailabilityCalendarService(con As SqlConnection, ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal objRes As List(Of clsQryCalendarService), ByVal UserID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            With cmd
                .CommandText = "spAvailabilityCalService"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With


            For i = 0 To objRes.Count - 1
                If objRes(i).ppDay1 <> -10 Then
                    With objRes(i)
                        cmd.Parameters.Clear()
                        cmd.Parameters.AddWithValue("serviceID", ServiceID)
                        cmd.Parameters.AddWithValue("scheduleID", ScheduleID)
                        cmd.Parameters.AddWithValue("date", .ppDate)
                        cmd.Parameters.AddWithValue("type", .ppType)
                        cmd.Parameters.AddWithValue("qty", .ppDay1)
                        cmd.Parameters.AddWithValue("userId", UserID)
                    End With
                    dataHelper.ExecuteNonQuery(cmd, con)
                End If
            Next

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveReservation(con As SqlConnection, ByVal objRes As List(Of clsReservationsRoomInfo)) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            With cmd
                .CommandText = "spQuotationRooms"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With

            For i = 0 To objRes.Count - 1
                With objRes(i)
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue("mode", "INS")
                    cmd.Parameters.AddWithValue("quotation_id", .ppReservationID)
                    cmd.Parameters.AddWithValue("roomtype_id", .ppRoomTypeID)
                    cmd.Parameters.AddWithValue("adults", .ppAdults)
                    cmd.Parameters.AddWithValue("children", .ppChildren)
                    cmd.Parameters.AddWithValue("rate_amount", .ppRateAmount)
                    cmd.Parameters.AddWithValue("tax_amount", .ppTaxAmount)
                    cmd.Parameters.AddWithValue("children_free", .ppChildrenFree)
                    cmd.Parameters.AddWithValue("date", .ppDate)
                End With
                dataHelper.ExecuteNonQuery(cmd, con)
            Next


            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveReservationAddons(con As SqlConnection, ByVal objAddons As List(Of AddonInfo), ByVal sReservation As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            With cmd
                .CommandText = "spQuotationAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With

            For i = 0 To objAddons.Count - 1
                With objAddons(i)
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue("addonID", .ppAddonID)
                    cmd.Parameters.AddWithValue("quotation_id", sReservation)
                    cmd.Parameters.AddWithValue("quantity", .ppQty)
                    cmd.Parameters.AddWithValue("amount", .ppAmount)
                End With
                dataHelper.ExecuteNonQuery(cmd, con)
            Next


            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveApprovalCode(con As SqlConnection, ByVal ReservationID As String, Code As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try


            With cmd
                .CommandText = "UPDATE tblCreditCardsTrans SET approb_code = '" & Code & "' WHERE reservation_id = '" & ReservationID & "'"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With

            dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function BlackOutDates(con As SqlConnection, ByVal sCompanyCode As String, ByVal sRoomtypeCode As String, ByVal dFrom As Date, ByVal dTo As Date) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response

        Try

            With cmd
                .CommandText = "spBlackOutDates"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("roomtype_code", sRoomtypeCode)
                .Parameters.AddWithValue("date_from", dFrom)
                .Parameters.AddWithValue("date_to", dTo)
            End With

            dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SendEmailPwdReminder(con As SqlConnection, ByVal Email As String) As String
        Dim objCmd As New SqlCommand
        Dim objDr As SqlDataReader
        Dim sPassword As String = ""
        Dim sName As String = ""
        Dim sResult As String = ""

        Dim strFrom = "bookingtrtinteractive.com <info@trtconsultores.com>"
        Dim mBody As String = ""

        Try

            With objCmd
                .Connection = con
                .CommandText = "spUsers"
                .CommandType = CommandType.StoredProcedure
            End With

            With objCmd
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@user_name", Email)
                objDr = .ExecuteReader
                objDr.Read()
                If objDr.HasRows = True Then
                    'sPassword = Decrypt(objDr.Item("password"), "Onli@2X4.")
                    sName = objDr.Item("first_name") & " " & objDr.Item("last_name")
                Else
                    sResult = "Error"
                    GoTo Salir
                End If
            End With

            objDr.Close()

            Dim mailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(Email))
            mailMsg.BodyEncoding = Encoding.Default
            mailMsg.Subject = "Password Reminder"

            mBody = "<h2>Dear " & sName & "</h2>"
            mBody &= "<p>Your password is: " & sPassword
            mBody &= "<p><b>Please remember to change your password after login</b> "
            mBody &= "<p>&nbsp; "
            mBody &= "<p>** This is an automated email, please do not reply ** "

            mailMsg.Body = mBody
            mailMsg.Priority = MailPriority.High
            mailMsg.IsBodyHtml = True

            Dim smtpMail As New SmtpClient
            smtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
            smtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
            smtpMail.Send(mailMsg)


            sResult = "Sent"
        Catch ex As Exception
            sResult = "Error"
            EventLog.WriteEntry("Application", "SendEmail: " & ex.ToString, EventLogEntryType.Error)
        End Try

Salir:
        Return sResult
    End Function



#End Region

End Class
