﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions
Imports BRules.Users

Public Class Queries

#Region "Public Methods"
    Public Function QueryReservations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservation)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryReservationList As New List(Of QryReservation)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryReservationList = QueryReservations(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return QryReservationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationConfirmations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryReservationConfirmationList = QueryReservationConfirmations(con, DateFrom, DateTo, CompanyCode, Range)
            End Using
            Return QryReservationConfirmationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationAccount(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryReservationConfirmationList = QueryReservationAccount(con, DateFrom, DateTo, CompanyCode, Range)
            End Using
            Return QryReservationConfirmationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryRoomsAvailability(ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Try
            Dim dataHelper As New DataHelper()
            Dim QryRoomsAvail As New QryRoomsAvailability

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryRoomsAvail = QueryRoomsAvailaiblity(con, CurrentDate, CompanyCode)
            End Using
            Return QryRoomsAvail
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryRoomsClosingGeneral(ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Try
            Dim dataHelper As New DataHelper()
            Dim QryRoomsAvail As New QryRoomsAvailability

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryRoomsAvail = QueryRoomsClosingGeneral(con, CurrentDate, CompanyCode)
            End Using
            Return QryRoomsAvail
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryRoomsClosingSales(ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Try
            Dim dataHelper As New DataHelper()
            Dim QryRoomsAvail As New QryRoomsAvailability

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryRoomsAvail = QueryRoomsClosingSales(con, CurrentDate, CompanyCode)
            End Using
            Return QryRoomsAvail
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationAccountOS(ByVal CompanyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryReservationConfirmationList = QueryReservationAccountOS(con, CompanyCode, DateFrom, DateTo)
            End Using
            Return QryReservationConfirmationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationRoomConfirmation(ByVal Reservation As String) As List(Of QryRoom)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryRoomList As New List(Of QryRoom)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryRoomList = QueryReservationRoomConfirmation(con, Reservation)
            End Using
            Return QryRoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationServiceItems(ByVal SaleID As String) As List(Of QrySaleItems)
        Try
            Dim dataHelper As New DataHelper()
            Dim QrySaleItemsList As New List(Of QrySaleItems)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QrySaleItemsList = QueryReservationServiceItems(con, SaleID)
            End Using
            Return QrySaleItemsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservationAddonConfirmations(ByVal Reservation As String) As List(Of QryAddon)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryAddonList As New List(Of QryAddon)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryAddonList = QueryReservationAddonConfirmations(con, Reservation)
            End Using
            Return QryAddonList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryAvailability(ByVal CompanyCode As String, RateCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryAvailabilityList As New List(Of QryAvailability)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryAvailabilityList = QueryAvailability(con, CompanyCode, RateCode, DateFrom, DateTo)
            End Using
            Return QryAvailabilityList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryCalendar(ByVal CompanyCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryCalendarList As New List(Of QryCalendar)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryCalendarList = QueryCalendar(con, CompanyCode, Month, Year)
            End Using
            Return QryCalendarList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function QueryCalendarService(ByVal ServiceID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryCalendarServiceList As New List(Of QryCalendarService)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryCalendarServiceList = QueryCalendarService(con, ServiceID, Month, Year)
            End Using
            Return QryCalendarServiceList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryCalendarRoom(ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryCalendarList As New List(Of QryCalendar)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryCalendarList = QueryCalendarRoom(con, CompanyCode, RoomTypeCode, Month, Year)
            End Using
            Return QryCalendarList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryCalendarServiceSchedule(ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryCalendarServiceList As New List(Of QryCalendarService)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryCalendarServiceList = QueryCalendarServiceSchedule(con, ServiceID, ScheduleID, Month, Year)
            End Using
            Return QryCalendarServiceList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function QueryReservationBankLog(ByVal Reservation As String) As List(Of BankLogInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim BankLogInfoList As New List(Of BankLogInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                BankLogInfoList = QueryReservationBankLog(con, Reservation)
            End Using
            Return BankLogInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryQuotations(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QryReservationConfirmationList = QueryQuotations(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return QryReservationConfirmationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryAvailabilityDay(ByVal CompanyCode As String, ByVal CurrentDate As Date) As List(Of DayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim DayAvailabilityList As New List(Of DayAvailability)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                DayAvailabilityList = QueryAvailabilityDay(con, CompanyCode, CurrentDate)
            End Using
            Return DayAvailabilityList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryCountries() As List(Of CountriesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CountriesInfoList As New List(Of CountriesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CountriesInfoList = QueryCountries(con)
            End Using
            Return CountriesInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QuerySales(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QrySale)
        Try
            Dim dataHelper As New DataHelper()
            Dim QrySaleList As New List(Of QrySale)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                QrySaleList = QuerySales(con, DateFrom, DateTo, CompanyCode, Range)
            End Using
            Return QrySaleList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function QueryPrices(ByVal CompanyCode As String, ByVal CurrentDate As Date, RoomType As String, ByVal PromoCode As String) As List(Of PricesDTO)
        Try
            Dim dataHelper As New DataHelper()
            Dim list As New List(Of PricesDTO)()

            If String.IsNullOrEmpty(PromoCode) Then
                PromoCode = Nothing
            End If

            Using con As SqlConnection = dataHelper.OpenConnection()
                list = QueryPrices(con, CompanyCode, CurrentDate, RoomType, PromoCode)
            End Using
            Return list
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


#End Region
#Region "Private Methods"

    Friend Function QueryReservations(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservation)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryReservationList As New List(Of QryReservation)()
        Dim sObjQryReservation As New QryReservation

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservations"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryReservation = New QryReservation

                With sObjQryReservation
                    .reservation = reader.Item("reservation")
                    .name = reader.Item("name")
                    .lastname = reader.Item("lastname")
                    .arrival = reader.Item("arrival")
                    .departure = reader.Item("departure")
                    .rate = reader.Item("rate")
                    .amount = reader.Item("amount")
                    .tax = reader.Item("tax")
                    .email = reader.Item("email")
                    .country = reader.Item("country")
                End With
                QryReservationList.Add(sObjQryReservation)
            End While

            reader.Close()

            Return QryReservationList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function QueryReservationConfirmations(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        Dim sObjQryReservationConfirmation As New QryReservationConfirmation
        Dim mCC As String
        Dim days As TimeSpan
        Dim user As New Users
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationConfirmation"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
                .Parameters.AddWithValue("@Range", Range)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryReservationConfirmation = New QryReservationConfirmation

                With sObjQryReservationConfirmation
                    .reservation = (reader.Item("reservation")).ToString.Trim
                    .name = reader.Item("name")
                    .lastname = reader.Item("lastname")
                    .arrival = reader.Item("arrival")
                    .departure = reader.Item("departure")
                    days = CDate(reader.Item("departure")).Subtract(reader.Item("arrival"))
                    .nights = days.TotalDays
                    .amount = reader.Item("amount")
                    .email = reader.Item("email")
                    If (reader.Item("creditCard")).ToString.Trim <> String.Empty Then

                        mCC = user.encriptaclave(reader.Item("creditCard"))
                        If mCC.Trim <> String.Empty Then
                            .creditCard = mCC.Trim
                        End If
                    Else
                        .creditCard = ""
                    End If
                    .expDate = reader.Item("expDate")
                    .approveCode = reader.Item("approveCode")
                    .secCode = reader.Item("secCode")
                    .cardType = reader.Item("cardType")
                    .remarks = (reader.Item("remarks")).ToString.Trim
                    .visitReason = (reader.Item("visit_reason")).ToString.Trim
                    .status = reader.Item("status")
                    .country = reader.Item("country_name")
                    .telephone = reader.Item("telephone_nr")
                    .created = reader.Item("creation_date")
                    .rateName = (reader.Item("rate_name")).ToString.Trim
                    .processor = reader.Item("processor")
                    .addonAmount = reader.Item("addons")
                End With
                QryReservationConfirmationList.Add(sObjQryReservationConfirmation)
            End While

            reader.Close()

            Return QryReservationConfirmationList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function QueryReservationAccount(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QryReservationConfirmation)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        Dim sObjQryReservationConfirmation As New QryReservationConfirmation
        Dim mCC As String
        Dim days As TimeSpan
        Dim user As New Users
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationAccount"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
                .Parameters.AddWithValue("@Range", Range)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryReservationConfirmation = New QryReservationConfirmation

                With sObjQryReservationConfirmation
                    .reservation = (reader.Item("reservation_id") & "").ToString.Trim
                    .name = reader.Item("name") & ""
                    .lastname = reader.Item("lastname") & ""

                    If IsDate(reader.Item("arrival")) Then
                        .arrival = CDate(reader.Item("arrival") & "").ToShortDateString
                    End If


                    If IsDate(reader.Item("departure")) Then
                        .departure = CDate(reader.Item("departure") & "").ToShortDateString
                    End If

                    If IsDate(reader.Item("departure")) Then
                        days = CDate(reader.Item("departure")).Subtract((reader.Item("arrival") & ""))
                    End If
                    .nights = days.TotalDays
                    .rate = reader.Item("rate_code") & ""
                    If reader.Item("status") = 2 Then
                        If IsNumeric(reader.Item("rate_amount")) Then
                            .amount = CDbl(reader.Item("rate_amount"))
                        End If
                        If IsNumeric(reader.Item("addon_amount")) Then
                            .addonAmount = CDbl(reader.Item("addon_amount"))
                        End If
                        If IsNumeric(reader.Item("promo_amount")) Then
                            .promoAmount = CDbl(reader.Item("promo_amount"))
                        End If
                        If IsNumeric(reader.Item("commissionOL")) Then
                            .commOL = CDbl(reader.Item("commissionOL"))
                        End If
                        If IsNumeric(reader.Item("commissionBank")) Then
                            .commBank = CDbl(reader.Item("commissionBank"))
                        End If

                        If IsNumeric(reader.Item("tax_amount")) Then
                            .taxAmount = CDbl(reader.Item("tax_amount"))
                        End If

                        .total = .amount - .commOL - .commBank
                    Else
                        .amount = 0
                        .addonAmount = 0
                        .promoAmount = 0
                        .commOL = 0
                        .commBank = 0
                        .taxAmount = 0
                        .total = 0
                        .commBank = 0
                        .commOL = 0
                    End If
                    .processor = reader.Item("Processor") & ""
                    mCC = user.encriptaclave(reader.Item("ccard_nr") & "")
                    If (reader.Item("ccard_nr")).ToString.Trim <> String.Empty Then
                        mCC = user.encriptaclave(reader.Item("ccard_nr") & "")
                        If mCC.Trim <> String.Empty Then .creditCard = "..." & Mid(mCC, Len(mCC) - 4)
                    Else
                        .creditCard = ""
                    End If
                    .expDate = reader.Item("ccard_exp") & ""
                    .approveCode = reader.Item("approb_code") & ""
                    .cardType = reader.Item("ccard_name") & ""
                    .statusText = reader.Item("statusText") & ""
                    .rateName = (reader.Item("rate_name")) & ""
                End With
                QryReservationConfirmationList.Add(sObjQryReservationConfirmation)
            End While

            reader.Close()

            Return QryReservationConfirmationList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function QueryReservationAccountOS(con As SqlConnection, ByVal CompanyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryReservationConfirmation)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        Dim sObjQryReservationConfirmation As New QryReservationConfirmation
        Dim mCC As String
        Dim days As TimeSpan
        Dim user As New Users
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationAccountOS"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryReservationConfirmation = New QryReservationConfirmation

                With sObjQryReservationConfirmation
                    .hotelName = reader.Item("company_name") & ""
                    .reservation = (reader.Item("reservation_id") & "").ToString.Trim
                    .arrival = reader.Item("arrival") & ""

                    If IsDate(reader.Item("departure")) Then
                        .departure = CDate(reader.Item("departure") & "").ToShortDateString
                    End If

                    If IsDate(reader.Item("departure")) Then
                        days = CDate(reader.Item("departure")).Subtract((reader.Item("arrival") & ""))
                    End If

                    .nights = days.TotalDays

                    If Val(reader.Item("status")) = 2 Then
                        If IsNumeric(reader.Item("rate_amount")) Then
                            .amount = CDbl(reader.Item("rate_amount"))
                        End If

                        If IsNumeric(reader.Item("commissionOL")) Then
                            .commOL = CDbl(reader.Item("commissionOL"))
                        End If
                        If IsNumeric(reader.Item("commissionBank")) Then
                            .commBank = CDbl(reader.Item("commissionBank"))
                        End If

                        '.taxAmount = reader.Item("tax_amount")
                        .total = .amount - .commOL - .commBank
                    Else
                        .amount = 0
                        '.addonAmount = 0
                        '.promoAmount = 0
                        .commOL = 0
                        .commBank = 0
                        .taxAmount = 0
                        .total = 0
                        .commBank = 0
                        .commOL = 0
                    End If
                    '.addonAmount = reader.Item("addon_amount")
                    '.promoAmount = reader.Item("promo_amount")
                    '.processor = reader.Item("Processor")
                    If (reader.Item("ccard_nr")).ToString.Trim <> String.Empty Then
                        mCC = user.encriptaclave(reader.Item("ccard_nr"))
                        If mCC.Trim <> String.Empty Then
                            .cardType = mCC
                            .creditCard = "..." & Mid(mCC, Len(mCC) - 4)
                        End If
                    Else
                        .creditCard = ""
                    End If

                    .transactionBank = reader.Item("transaction_bank") & ""
                    .depositNumber = reader.Item("deposit_number") & ""

                    .expDate = reader.Item("ccard_exp") & ""
                    .approveCode = reader.Item("approb_code") & ""
                    .cardType = reader.Item("ccard_name") & ""
                    .statusText = reader.Item("statusText") & ""
                    .rateName = (reader.Item("rate_name")) & ""

                End With
                QryReservationConfirmationList.Add(sObjQryReservationConfirmation)
            End While

            reader.Close()

            Return QryReservationConfirmationList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function QueryRoomsAvailaiblity(con As SqlConnection, ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim objRoomsAvail As New QryRoomsAvailability
        Dim loRoomTypes As New List(Of RoomsInfo)
        Dim objRoomTypeInfo As New RoomsInfo
        Dim loAvailability As New List(Of QryAvailability)
        Dim objAvailability As New QryAvailability
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryRoomsAvailability"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@CurrentDate", CurrentDate)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoomTypeInfo = New RoomsInfo

                With objRoomTypeInfo
                    .ppRoomTypeID = reader.Item("roomtype_id")
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppQuantity = reader.Item("quantity")
                    .ppDescription = reader.Item("description")
                End With
                loRoomTypes.Add(objRoomTypeInfo)
            End While

            objRoomsAvail.pploRoomTypes = loRoomTypes

            reader.NextResult()

            While reader.Read()
                objAvailability = New QryAvailability

                With objAvailability
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppDate = reader.Item("date")
                    .ppQuantity = reader.Item("rooms_qty")
                    .ppOccupaid = reader.Item("rooms_occup")
                    .ppBlocked = reader.Item("rooms_blocked")
                    .ppAvailable = reader.Item("rooms_avail")
                    .ppRoom = reader.Item("roomtype_name")
                End With
                loAvailability.Add(objAvailability)
            End While

            reader.Close()

            objRoomsAvail.pploRoomAvailability = loAvailability

            Return objRoomsAvail

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function QueryRoomsClosingSales(con As SqlConnection, ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim objRoomsAvail As New QryRoomsAvailability
        Dim loRoomTypes As New List(Of RoomsInfo)
        Dim objRoomTypeInfo As New RoomsInfo
        Dim loAvailability As New List(Of QryAvailability)
        Dim objAvailability As New QryAvailability
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryRoomsClosingSales"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@CurrentDate", CurrentDate)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoomTypeInfo = New RoomsInfo

                With objRoomTypeInfo
                    .ppRoomTypeID = reader.Item("roomtype_id")
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppQuantity = reader.Item("quantity")
                    .ppDescription = reader.Item("description")
                End With
                loRoomTypes.Add(objRoomTypeInfo)
            End While

            objRoomsAvail.pploRoomTypes = loRoomTypes

            reader.NextResult()

            While reader.Read()
                objAvailability = New QryAvailability

                With objAvailability
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppDate = reader.Item("date")
                    .ppQuantity = reader.Item("rooms_qty")
                    .ppOccupaid = reader.Item("rooms_occup")
                    .ppBlocked = reader.Item("rooms_blocked")
                    .ppAvailable = reader.Item("rooms_avail")
                    .ppRoom = reader.Item("roomtype_name")
                End With
                loAvailability.Add(objAvailability)
            End While

            reader.Close()

            objRoomsAvail.pploRoomAvailability = loAvailability

            Return objRoomsAvail

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function QueryRoomsClosingGeneral(con As SqlConnection, ByVal CurrentDate As Date, ByVal CompanyCode As String) As QryRoomsAvailability
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim objRoomsAvail As New QryRoomsAvailability
        Dim loRoomTypes As New List(Of RoomsInfo)
        Dim objRoomTypeInfo As New RoomsInfo
        Dim loAvailability As New List(Of QryAvailability)
        Dim objAvailability As New QryAvailability
        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryRoomsClosingGeneral"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@CurrentDate", CurrentDate)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoomTypeInfo = New RoomsInfo

                With objRoomTypeInfo
                    .ppRoomTypeID = reader.Item("roomtype_id")
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppQuantity = reader.Item("quantity")
                    .ppDescription = reader.Item("description")
                End With
                loRoomTypes.Add(objRoomTypeInfo)
            End While

            objRoomsAvail.pploRoomTypes = loRoomTypes

            reader.NextResult()

            While reader.Read()
                objAvailability = New QryAvailability

                With objAvailability
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppDate = reader.Item("date")
                    .ppQuantity = reader.Item("rooms_qty")
                    .ppOccupaid = reader.Item("rooms_occup")
                    .ppBlocked = reader.Item("rooms_blocked")
                    .ppAvailable = reader.Item("rooms_avail")
                    .ppRoom = reader.Item("roomtype_name")
                End With
                loAvailability.Add(objAvailability)
            End While

            reader.Close()

            objRoomsAvail.pploRoomAvailability = loAvailability

            Return objRoomsAvail

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function
    Friend Function QueryReservationRoomConfirmation(con As SqlConnection, ByVal Reservation As String) As List(Of QryRoom)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryRoomList As New List(Of QryRoom)()
        Dim sObjQryRoom As New QryRoom

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationRoomConfirmation"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Reservation", Reservation)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryRoom = New QryRoom

                With sObjQryRoom
                    .ppCode = (reader.Item("Code") & "").ToString.Trim
                    .ppRoom = reader.Item("Room") & ""
                    .ppAdults = Val(reader.Item("adults") & "")
                    .ppChildren = Val(reader.Item("children") & "")
                    .ppChildrenFree = Val(reader.Item("children_free") & "")
                    If IsDate(reader.Item("date")) Then
                        .ppDate = CDate(reader.Item("date")).ToShortDateString
                    End If
                    .ppAmount = 0
                End With
                QryRoomList.Add(sObjQryRoom)
            End While

            reader.Close()

            Return QryRoomList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QueryReservationServiceItems(con As SqlConnection, ByVal SaleID As String) As List(Of QrySaleItems)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QrySaleItemsList As New List(Of QrySaleItems)()
        Dim sObjQrySaleItems As New QrySaleItems

        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceItems"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@SaleID", SaleID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQrySaleItems = New QrySaleItems

                With sObjQrySaleItems
                    .ppSaleID = (reader.Item("sale_id")).ToString.Trim
                    .ppDescription = reader.Item("description")
                    .ppArrivalDate = reader.Item("arrival_date")
                    .ppAdultsQty = reader.Item("adults_qty")
                    .ppChildrenQty = reader.Item("children_qty")
                    .ppInfantsQty = reader.Item("infants_qty")
                    .ppStudentsQty = reader.Item("students_qty")
                    .ppAdultsAmt = reader.Item("adults_amount")
                    .ppChildrenAmt = reader.Item("children_amount")
                    .ppInfantsAmt = reader.Item("infants_amount")
                    .ppStudentsAmt = reader.Item("students_amount")
                    .ppHotel = reader.Item("Hotel")
                    .ppHotelPickup = reader.Item("HotelPickupHour")
                    .ppHour = reader.Item("Hour")
                End With
                QrySaleItemsList.Add(sObjQrySaleItems)
            End While

            reader.Close()

            Return QrySaleItemsList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QueryReservationAddonConfirmations(con As SqlConnection, ByVal Reservation As String) As List(Of QryAddon)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryAddonList As New List(Of QryAddon)()
        Dim sObjQryAddon As New QryAddon

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationAddonConfirmation"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Reservation", Reservation)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryAddon = New QryAddon

                With sObjQryAddon
                    .ppAddon = reader.Item("Addon") & ""
                    .ppQty = Val(reader.Item("quantity") & "")
                End With
                QryAddonList.Add(sObjQryAddon)
            End While

            reader.Close()

            Return QryAddonList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function QueryAvailability(con As SqlConnection, ByVal CompanyCode As String, RateCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of QryAvailability)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryAvailabilityList As New List(Of QryAvailability)()
        Dim sObjQryAvailability As New QryAvailability

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryAvailability"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Company_code", CompanyCode)
                .Parameters.AddWithValue("@RateCode", RateCode)
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryAvailability = New QryAvailability

                With sObjQryAvailability
                    .ppDate = reader.Item("date")
                    .ppRate = reader.Item("rate_code")
                    .ppRoom = reader.Item("description")
                    .ppAvailable = reader.Item("rooms_avail")
                    .ppQuantity = reader.Item("rooms_qty")
                    .ppBlocked = reader.Item("rooms_blocked")
                    .ppOccupaid = reader.Item("rooms_occup")
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                End With
                QryAvailabilityList.Add(sObjQryAvailability)
            End While

            reader.Close()

            Return QryAvailabilityList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function QueryCalendar(con As SqlConnection, ByVal CompanyCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryCalendarList As New List(Of QryCalendar)()
        Dim sObjQryCalendar As New QryCalendar

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityCalendar"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@month", Month)
                .Parameters.AddWithValue("@year", Year)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryCalendar = New QryCalendar
                With sObjQryCalendar
                    .ppRoom = reader.Item("description")
                    .ppRoomTypeCode = reader.Item("roomtype_code")
                    .ppDay1 = reader.Item("day1")
                    .ppDay2 = reader.Item("day2")
                    .ppDay3 = reader.Item("day3")
                    .ppDay4 = reader.Item("day4")
                    .ppDay5 = reader.Item("day5")
                    .ppDay6 = reader.Item("day6")
                    .ppDay7 = reader.Item("day7")
                    .ppDay8 = reader.Item("day8")
                    .ppDay9 = reader.Item("day9")
                    .ppDay10 = reader.Item("day10")
                    .ppDay11 = reader.Item("day11")
                    .ppDay12 = reader.Item("day12")
                    .ppDay13 = reader.Item("day13")
                    .ppDay14 = reader.Item("day14")
                    .ppDay15 = reader.Item("day15")
                    .ppDay16 = reader.Item("day16")
                    .ppDay17 = reader.Item("day17")
                    .ppDay18 = reader.Item("day18")
                    .ppDay19 = reader.Item("day19")
                    .ppDay20 = reader.Item("day20")
                    .ppDay21 = reader.Item("day21")
                    .ppDay22 = reader.Item("day22")
                    .ppDay23 = reader.Item("day23")
                    .ppDay24 = reader.Item("day24")
                    .ppDay25 = reader.Item("day25")
                    .ppDay26 = reader.Item("day26")
                    .ppDay27 = reader.Item("day27")
                    .ppDay28 = reader.Item("day28")
                    .ppDay29 = IIf(reader.Item("day29") Is DBNull.Value, 0, reader.Item("day29"))
                    .ppDay30 = IIf(reader.Item("day30") Is DBNull.Value, 0, reader.Item("day30"))
                    .ppDay31 = IIf(reader.Item("day31") Is DBNull.Value, 0, reader.Item("day31"))

                End With
                QryCalendarList.Add(sObjQryCalendar)
            End While

            reader.Close()

            Return QryCalendarList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function QueryCalendarService(con As SqlConnection, ByVal ServiceID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryCalendarServiceList As New List(Of QryCalendarService)()
        Dim sObjQryCalendarService As New QryCalendarService

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityServiceCalendar"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@serviceID", ServiceID)
                .Parameters.AddWithValue("@month", Month)
                .Parameters.AddWithValue("@year", Year)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryCalendarService = New QryCalendarService

                With sObjQryCalendarService
                    .ppSchedule = reader.Item("schedule")
                    .ppScheduleID = reader.Item("scheduleID")
                    .ppDay1 = reader.Item("day1")
                    .ppDay2 = reader.Item("day2")
                    .ppDay3 = reader.Item("day3")
                    .ppDay4 = reader.Item("day4")
                    .ppDay5 = reader.Item("day5")
                    .ppDay6 = reader.Item("day6")
                    .ppDay7 = reader.Item("day7")
                    .ppDay8 = reader.Item("day8")
                    .ppDay9 = reader.Item("day9")
                    .ppDay10 = reader.Item("day10")
                    .ppDay11 = reader.Item("day11")
                    .ppDay12 = reader.Item("day12")
                    .ppDay13 = reader.Item("day13")
                    .ppDay14 = reader.Item("day14")
                    .ppDay15 = reader.Item("day15")
                    .ppDay16 = reader.Item("day16")
                    .ppDay17 = reader.Item("day17")
                    .ppDay18 = reader.Item("day18")
                    .ppDay19 = reader.Item("day19")
                    .ppDay20 = reader.Item("day20")
                    .ppDay21 = reader.Item("day21")
                    .ppDay22 = reader.Item("day22")
                    .ppDay23 = reader.Item("day23")
                    .ppDay24 = reader.Item("day24")
                    .ppDay25 = reader.Item("day25")
                    .ppDay26 = reader.Item("day26")
                    .ppDay27 = reader.Item("day27")
                    .ppDay28 = reader.Item("day28")
                    .ppDay29 = IIf(reader.Item("day29") Is DBNull.Value, 0, reader.Item("day29"))
                    .ppDay30 = IIf(reader.Item("day30") Is DBNull.Value, 0, reader.Item("day30"))
                    .ppDay31 = IIf(reader.Item("day31") Is DBNull.Value, 0, reader.Item("day31"))
                End With
                QryCalendarServiceList.Add(sObjQryCalendarService)
            End While

            reader.Close()

            Return QryCalendarServiceList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function



    Friend Function QueryCalendarRoom(con As SqlConnection, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendar)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryCalendarList As New List(Of QryCalendar)()
        Dim sObjQryCalendar As New QryCalendar

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityCalendarRoom"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@roomtype_code", RoomTypeCode)
                .Parameters.AddWithValue("@month", Month)
                .Parameters.AddWithValue("@year", Year)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryCalendar = New QryCalendar

                With sObjQryCalendar
                    .ppRoom = reader.Item("description")
                    .ppDay1 = reader.Item("day1")
                    .ppDay2 = reader.Item("day2")
                    .ppDay3 = reader.Item("day3")
                    .ppDay4 = reader.Item("day4")
                    .ppDay5 = reader.Item("day5")
                    .ppDay6 = reader.Item("day6")
                    .ppDay7 = reader.Item("day7")
                    .ppDay8 = reader.Item("day8")
                    .ppDay9 = reader.Item("day9")
                    .ppDay10 = reader.Item("day10")
                    .ppDay11 = reader.Item("day11")
                    .ppDay12 = reader.Item("day12")
                    .ppDay13 = reader.Item("day13")
                    .ppDay14 = reader.Item("day14")
                    .ppDay15 = reader.Item("day15")
                    .ppDay16 = reader.Item("day16")
                    .ppDay17 = reader.Item("day17")
                    .ppDay18 = reader.Item("day18")
                    .ppDay19 = reader.Item("day19")
                    .ppDay20 = reader.Item("day20")
                    .ppDay21 = reader.Item("day21")
                    .ppDay22 = reader.Item("day22")
                    .ppDay23 = reader.Item("day23")
                    .ppDay24 = reader.Item("day24")
                    .ppDay25 = reader.Item("day25")
                    .ppDay26 = reader.Item("day26")
                    .ppDay27 = reader.Item("day27")
                    .ppDay28 = reader.Item("day28")
                    .ppDay29 = IIf(reader.Item("day29") Is DBNull.Value, 0, reader.Item("day29"))
                    .ppDay30 = IIf(reader.Item("day30") Is DBNull.Value, 0, reader.Item("day30"))
                    .ppDay31 = IIf(reader.Item("day31") Is DBNull.Value, 0, reader.Item("day31"))
                End With
                QryCalendarList.Add(sObjQryCalendar)
            End While

            reader.Close()

            Return QryCalendarList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function QueryCalendarServiceSchedule(con As SqlConnection, ByVal ServiceID As Integer, ByVal ScheduleID As Integer, ByVal Month As Integer, ByVal Year As Integer) As List(Of QryCalendarService)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryCalendarServiceList As New List(Of QryCalendarService)()
        Dim sObjQryCalendarService As New QryCalendarService

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityCalendarServiceSchedule"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@serviceID", ServiceID)
                .Parameters.AddWithValue("@scheduleID", ScheduleID)
                .Parameters.AddWithValue("@month", Month)
                .Parameters.AddWithValue("@year", Year)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryCalendarService = New QryCalendarService

                With sObjQryCalendarService
                    .ppSchedule = reader.Item("schedule")
                    .ppDay1 = reader.Item("day1")
                    .ppDay2 = reader.Item("day2")
                    .ppDay3 = reader.Item("day3")
                    .ppDay4 = reader.Item("day4")
                    .ppDay5 = reader.Item("day5")
                    .ppDay6 = reader.Item("day6")
                    .ppDay7 = reader.Item("day7")
                    .ppDay8 = reader.Item("day8")
                    .ppDay9 = reader.Item("day9")
                    .ppDay10 = reader.Item("day10")
                    .ppDay11 = reader.Item("day11")
                    .ppDay12 = reader.Item("day12")
                    .ppDay13 = reader.Item("day13")
                    .ppDay14 = reader.Item("day14")
                    .ppDay15 = reader.Item("day15")
                    .ppDay16 = reader.Item("day16")
                    .ppDay17 = reader.Item("day17")
                    .ppDay18 = reader.Item("day18")
                    .ppDay19 = reader.Item("day19")
                    .ppDay20 = reader.Item("day20")
                    .ppDay21 = reader.Item("day21")
                    .ppDay22 = reader.Item("day22")
                    .ppDay23 = reader.Item("day23")
                    .ppDay24 = reader.Item("day24")
                    .ppDay25 = reader.Item("day25")
                    .ppDay26 = reader.Item("day26")
                    .ppDay27 = reader.Item("day27")
                    .ppDay28 = reader.Item("day28")
                    .ppDay29 = IIf(reader.Item("day29") Is DBNull.Value, 0, reader.Item("day29"))
                    .ppDay30 = IIf(reader.Item("day30") Is DBNull.Value, 0, reader.Item("day30"))
                    .ppDay31 = IIf(reader.Item("day31") Is DBNull.Value, 0, reader.Item("day31"))
                End With
                QryCalendarServiceList.Add(sObjQryCalendarService)
            End While

            reader.Close()

            Return QryCalendarServiceList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Friend Function QueryReservationBankLog(con As SqlConnection, ByVal Reservation As String) As List(Of BankLogInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim BankLogInfoList As New List(Of BankLogInfo)()
        Dim sObjBankLogInfo As New BankLogInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryReservationBankLog"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Reservation", Reservation)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjBankLogInfo = New BankLogInfo

                With sObjBankLogInfo
                    .ppResponse = reader.Item("ResponseText") & ""
                    .ppTransactionID = reader.Item("authCode") & ""
                    If IsNumeric(reader.Item("Amount")) Then
                        .ppAmount = CDbl(reader.Item("Amount"))
                    End If
                End With
                BankLogInfoList.Add(sObjBankLogInfo)
            End While

            reader.Close()

            Return BankLogInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QueryQuotations(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of QryReservationConfirmation)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QryReservationConfirmationList As New List(Of QryReservationConfirmation)()
        Dim sObjQryReservationConfirmation As New QryReservationConfirmation
        Dim days As TimeSpan

        Try
            With cmd
                .Connection = con
                .CommandText = "spQueryQuotations"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQryReservationConfirmation = New QryReservationConfirmation

                With sObjQryReservationConfirmation
                    .reservation = (reader.Item("quotation")).ToString.Trim
                    .name = reader.Item("name")
                    .lastname = reader.Item("lastname")
                    .arrival = reader.Item("arrival")
                    .departure = reader.Item("departure")
                    days = CDate(reader.Item("departure")).Subtract(reader.Item("arrival"))
                    .nights = days.TotalDays
                    .amount = reader.Item("amount")
                    .addonAmount = reader.Item("addonAmount")
                    .promoAmount = reader.Item("promoAmount")
                    .taxAmount = reader.Item("taxAmount")
                    .rateName = (reader.Item("ratename")).ToString.Trim
                    .telephone = reader.Item("telephone")
                    .email = reader.Item("email")
                    .statusText = reader.Item("status")
                    .created = reader.Item("created")
                    .void = reader.Item("void")
                    .reference = reader.Item("reservation_id")
                End With
                QryReservationConfirmationList.Add(sObjQryReservationConfirmation)
            End While

            reader.Close()

            Return QryReservationConfirmationList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QueryAvailabilityDay(con As SqlConnection, ByVal CompanyCode As String, ByVal CurrentDate As Date) As List(Of DayAvailability)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim DayAvailabilityList As New List(Of DayAvailability)()
        Dim sObjDayAvailability As New DayAvailability

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityDay_v1"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@CurrentDate", CurrentDate)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjDayAvailability = New DayAvailability

                With sObjDayAvailability
                    If IsDate(reader.Item("Day")) Then
                        .ppDay = reader.Item("Day")
                    End If
                    .ppQty = Val(reader.Item("Qty") & "")
                End With
                DayAvailabilityList.Add(sObjDayAvailability)
            End While

            reader.Close()

            Return DayAvailabilityList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function QueryCountries(con As SqlConnection) As List(Of CountriesInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim CountriesInfoList As New List(Of CountriesInfo)()
        Dim sObjCountriesInfo As New CountriesInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spCountries"
                .CommandType = CommandType.StoredProcedure
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjCountriesInfo = New CountriesInfo

                With sObjCountriesInfo
                    .ppCountryIso3 = reader.Item("country_iso3")
                    .ppName = reader.Item("country_name")
                End With
                CountriesInfoList.Add(sObjCountriesInfo)
            End While

            reader.Close()

            Return CountriesInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QuerySales(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal CompanyCode As String, Range As Int16) As List(Of QrySale)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim QrySaleList As New List(Of QrySale)()
        Dim sObjQrySale As New QrySale
        Dim mCC As String = ""
        Dim user As New Users

        Try
            With cmd
                .Connection = con
                .CommandText = "spQuerySales"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
                .Parameters.AddWithValue("@Range", Range)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjQrySale = New QrySale

                With sObjQrySale
                    .reservation = (reader.Item("sale_id")).ToString.Trim
                    .name = reader.Item("name")
                    .lastname = reader.Item("lastname")
                    '.arrival = reader.Item("arrival")
                    .services = 0
                    .amount = reader.Item("amount")
                    .email = reader.Item("email")
                    If (reader.Item("ccard_nr")).ToString.Trim <> String.Empty Then
                        mCC = user.encriptaclave(reader.Item("ccard_nr"))
                    Else
                        mCC = ""
                    End If
                    .creditCard = mCC
                    .expDate = reader.Item("ccard_exp")
                    .approveCode = reader.Item("approb_code")
                    .cardType = reader.Item("ccard_name")
                    .remarks = (reader.Item("remarks")).ToString.Trim
                    .status = reader.Item("status")
                    .country = reader.Item("country_name")
                    .telephone = reader.Item("telephone_nr")
                    .created = reader.Item("creation_date")
                    .processor = reader.Item("processor")
                End With
                QrySaleList.Add(sObjQrySale)
            End While

            reader.Close()

            Return QrySaleList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function QueryPrices(con As SqlConnection, ByVal CompanyCode As String, ByVal CurrentDate As Date, RoomType As String, ByVal PromoCode As String) As List(Of PricesDTO)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim PricesList As New List(Of PricesDTO)()
        Dim price As New PricesDTO

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityPrices"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@RoomType", RoomType)
                .Parameters.AddWithValue("@CurrentDate", CurrentDate)
                .Parameters.AddWithValue("@PromoCode", PromoCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                price = New PricesDTO

                With price
                    If IsDate(reader.Item("date")) Then
                        .ppDate = reader.Item("date")
                    End If
                    If IsNumeric(reader.Item("adult1")) Then
                        .ppPrices = CDbl(reader.Item("adult1"))
                    End If
                    .strdate = .ppDate.ToShortDateString

                    If IsNumeric(reader.Item("discount")) Then
                        .Discount = CDbl(reader.Item("discount"))
                    End If

                    If Not IsDBNull(reader.Item("promoamount")) Then
                        If IsNumeric(reader.Item("promoamount")) Then
                            .ppPromoAmount = CDbl(reader.Item("promoamount"))
                        End If
                    End If

                    If Not IsDBNull(reader.Item("promotype")) Then
                        .ppPromoType = reader.Item("promotype")
                    End If


                End With
                PricesList.Add(price)
            End While

            reader.Close()

            Return PricesList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

#End Region
End Class
