﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Reservations

#Region "Public Methods"


    Public Function SaveReservationSession(ByRef reservation As ReservationSession) As Response

        Dim dataHelper As New DataHelper()
        Dim Response As New Response
        Dim Session As New Session
        Dim clsGuess As New Processes
        Dim GuessId As Long
        Dim SessionID As Guid
        Dim ReservationId As String = ""
        Dim RoomId As Long = 0

        Try

            SessionID = Guid.NewGuid()
            reservation.ReservationsInfo.ppSessionID = SessionID.ToString

            With reservation

                Using Scope As TransactionScope = New TransactionScope()
                    Using con As SqlConnection = dataHelper.OpenConnection()

                        Response = Session.InsertSessionRate(con, SessionID, .ReservationsInfo.ppRate_Code, .ReservationsInfo.ppArrival,
                                                         .ReservationsInfo.ppDeparture, .RoomList.Count, .ReservationsInfo.Room1Qty, .ReservationsInfo.Room2Qty, .ReservationsInfo.Room3Qty, .ReservationsInfo.Room4Qty, .ReservationsInfo.ppPromoID,
                                                             .ReservationsInfo.ppResellerID, .ReservationsInfo.ppCompany_Code, "en-US")

                        If Response.Status = "0" Then
                            GuessId = clsGuess.SaveGuest(con, "INS", reservation.GuestsInfo)

                            If GuessId = 0 Then
                                Throw New Exception("Failed saving guess object")
                            End If

                            .ReservationsInfo.ppGuestID = GuessId
                            .ReservationsInfo.ppSessionID = SessionID.ToString

                            ReservationId = SaveReservation(con, "INS", .ReservationsInfo.Processor, .ReservationsInfo)

                            If ReservationId = "" Then
                                Throw New Exception("Failed saving Reservation object")
                            End If

                            reservation.ReservationsInfo.ppReservationID = ReservationId

                            For Each room In .RoomList
                                room.ppReservationID = ReservationId
                                RoomId = SaveReservationRoom(con, "INS", room)
                            Next

                            For Each addon In .AddonList
                                Response = SaveAddons(con, addon.ppAddonID, ReservationId, 1, addon.ppAmount)
                            Next

                        End If

                        Scope.Complete()
                        con.Close()

                        Response.Status = "0"
                        Response.Message = "Reservation saved ok"

                    End Using

                End Using


            End With

            Return Response


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceReservationSession(ByRef reservation As ReservationSessionService) As Response

        Dim dataHelper As New DataHelper()
        Dim Response As New Response
        Dim Session As New Session
        Dim clsGuess As New Processes
        Dim clsSales As New Sales
        Dim GuessId As Long
        'Dim SessionID As Guid
        Dim ReservationId As String = String.Empty
        Dim RoomId As Long = 0
        Dim sSaleID As String = String.Empty
        Try

            'SessionID = Guid.NewGuid(reservation.ReservationsInfo.ppSessionID)
            'reservation.ReservationsInfo.ppSessionID = SessionID.ToString
            Dim sessionId As New Guid(reservation.SaleInfo.ppSessionID)
            With reservation

                Using Scope As TransactionScope = New TransactionScope()
                    Using con As SqlConnection = dataHelper.OpenConnection()

                        '-- Save Session
                        Session.SaveCompanySessionService(con, sessionId, reservation.SaleInfo.ppCompany_Code, "en-US")

                        If Response.Status = "0" Then

                            '-- Save Session per service
                            For Each item In reservation.ServicesList
                                Response = Session.InsertSessionServiceSC(con, sessionId.ToString, item.ppServiceID, item.ppArrivalDate, item.ppScheduleID,
                                  item.ppHotelPickUpID, item.ppHotelPickupHourID, item.ppQtyAdults, item.ppQtyChildren, item.ppQtyInfants, item.ppQtyStudents,
                                  item.ppPriceAdults, item.ppPriceChildren, item.ppPriceInfants, item.ppPriceStudent)
                            Next

                            '-- Save Guess Info
                            GuessId = clsGuess.SaveGuest(con, "INS", reservation.GuestsInfo)

                            If GuessId = 0 Then
                                Throw New Exception("Failed saving guess object")
                            End If

                            .SaleInfo.ppGuestID = GuessId
                            '.SaleInfo.ppSessionID = sessionId.ToString

                            sSaleID = clsSales.SaveSale(con, "INS", reservation.SaleInfo.Processor, reservation.SaleInfo)

                            If sSaleID = "" Then
                                Throw New Exception("Failed saving Reservation/Sale object")
                            End If

                            reservation.SaleInfo.ppSaleID = sSaleID
                        End If

                        Scope.Complete()
                        con.Close()

                        Response.Status = "0"
                        Response.Message = "Reservation saved ok"

                    End Using

                End Using


            End With

            Return Response


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveVirtualReceiptReservation(ByRef reservation As ReservationSessionService, ByVal ReceiptCode As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim Response As New Response
        Dim Session As New Session
        Dim clsGuess As New Processes
        Dim clsVR As New VirtualReceipts
        Dim GuessId As Long
        'Dim SessionID As Guid
        Dim ReservationId As String = String.Empty
        Dim RoomId As Long = 0
        Dim UpdateVRResult As String = String.Empty
        Try

            Dim sessionId As New Guid()
            With reservation

                Using Scope As TransactionScope = New TransactionScope()
                    Using con As SqlConnection = dataHelper.OpenConnection()

                        If Response.Status = "0" Then

                            '-- Save Guess Info
                            GuessId = clsGuess.SaveGuest(con, "INS", reservation.GuestsInfo)

                            If GuessId = 0 Then
                                Throw New Exception("Failed saving guess object")
                            End If

                            .SaleInfo.ppGuestID = GuessId

                            UpdateVRResult = clsVR.SaveVirtualReceipt(con, ReceiptCode, reservation.SaleInfo.ppCompany_Code, Nothing, Nothing, Nothing, Nothing, Nothing,
                                    reservation.SaleInfo.ppAmount, True, Nothing, GuessId, Nothing, "PAY")

                            'sSaleID = clsSales.SaveSale(con, "INS", reservation.SaleInfo.Processor, reservation.SaleInfo)

                            If UpdateVRResult = "" Then
                                Throw New Exception("Failed updating/paying virtual receipt object")
                            End If

                            reservation.SaleInfo.ppSaleID = ReceiptCode
                        End If

                        Scope.Complete()
                        con.Close()

                        Response.Status = "0"
                        Response.Message = "Reservation saved ok"

                    End Using

                End Using


            End With

            Return Response


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateReservationSession(reservation As ReservationSession) As Response

        Dim dataHelper As New DataHelper()
        Dim Response As New Response
        Dim Session As New Session
        Dim clsGuess As New Processes
        Dim GuessId As Long
        Dim SessionID As Guid
        Dim RoomId As Long = 0

        Try

            SessionID = New Guid(reservation.ReservationsInfo.ppSessionID)
            'reservation.ReservationsInfo.ppSessionID = SessionID.ToString

            With reservation

                Using Scope As TransactionScope = New TransactionScope()
                    Using con As SqlConnection = dataHelper.OpenConnection()

                        Response = Session.UpdateSessionRate(con, SessionID, .ReservationsInfo.ppRate_Code, .ReservationsInfo.ppArrival,
                                                         .ReservationsInfo.ppDeparture, .RoomList.Count, .ReservationsInfo.Room1Qty, .ReservationsInfo.Room2Qty, .ReservationsInfo.Room3Qty, .ReservationsInfo.Room4Qty, .ReservationsInfo.ppPromoID,
                                                             .ReservationsInfo.ppResellerID, "UP1")

                        If Response.Status = "0" Then

                            GuessId = clsGuess.SaveGuestsByReservation(con, reservation.ReservationsInfo.ppReservationID, reservation.GuestsInfo)

                            If GuessId = 0 Then
                                Throw New Exception("Failed saving guess object")
                            End If

                            .ReservationsInfo.ppGuestID = GuessId
                            .ReservationsInfo.ppSessionID = SessionID.ToString

                            UpdateReservation(con, .ReservationsInfo.Processor, .ReservationsInfo)

                            DeleteAddons(con, reservation.ReservationsInfo.ppReservationID)

                            DeleteReservationRoom(con, reservation.ReservationsInfo.ppReservationID)

                            For Each room In .RoomList
                                room.ppReservationID = reservation.ReservationsInfo.ppReservationID
                                RoomId = SaveReservationRoom(con, "INS", room)
                            Next

                            For Each addon In .AddonList
                                Response = SaveAddons(con, addon.ppAddonID, reservation.ReservationsInfo.ppReservationID, 1, addon.ppAmount)
                            Next

                        End If

                        Scope.Complete()
                        con.Close()

                        Response.Status = "0"
                        Response.Message = "Reservation saved ok"

                    End Using

                End Using


            End With

            Return Response


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetFreeNights(ByVal sCompanyId As String, ByVal RateCode As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim FreeNights As Integer = 0

            Using con As SqlConnection = dataHelper.OpenConnection()
                FreeNights = GetFreeNights(con, sCompanyId, RateCode)
            End Using
            Return FreeNights
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetReservationRoomsQuo(ByVal sQuotationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim objCards As New List(Of clsReservationsRoomInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                objCards = GetReservationRoomsQuo(con, sQuotationID, Culture)
            End Using
            Return objCards
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveReservation(ByVal sMode As String, ByVal sProcessor As String, reservation As clsReservationsInfo) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Long

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = SaveReservation(con, sMode, sProcessor, reservation)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteReservationRoom(ByVal sReservationNumber As String) As Response

        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteReservationRoom(con, sReservationNumber)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveReservationRoom(ByVal sMode As String, ReservationRoom As clsReservationsRoomInfo) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Long

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = SaveReservationRoom(con, sMode, ReservationRoom)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailability(ByVal ResNumber As String, ByVal KeyId As Long) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Long

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = SaveAvailability(con, ResNumber, KeyId)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailabilityQuo(ByVal ResNumber As String) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Long

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = SaveAvailabilityQuo(con, ResNumber)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function CheckStatus(ByVal OrderId As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim Existe As Integer = 0

            Using con As SqlConnection = dataHelper.OpenConnection()
                Existe = CheckStatus(con, OrderId)
            End Using

            Return Existe

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16, ByVal KeyID As Long) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Long

        Try


            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = UpdateStatus(con, ResNumber, Status, KeyID)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailabilityQuery(ByVal CompanyCode As String, ByVal IP As String, ByVal Arrival As Date,
                                     ByVal Departure As Date, ByVal Adults As Int16, ByVal Children As Int16,
                                     ByVal Rooms As Int16, ByVal Nights As Int16,
                                     ByVal PromoCode As String, ByVal MessageCode As String, ByVal sGuid As System.Guid) As Response

        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try


            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailabilityQuery(con, CompanyCode, IP, Arrival, Departure, Adults, Children, Rooms, Nights, PromoCode, MessageCode, sGuid)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCardTypes(ByVal Bank As String) As List(Of clsCardTypeInfo)
        Try
            Dim dataHelper As New DataHelper()

            Dim objCreditCard As New clsCardTypeInfo
            Dim CreditCardList As New List(Of clsCardTypeInfo)()


            Using con As SqlConnection = dataHelper.OpenConnection()
                CreditCardList = GetCardTypes(con, Bank)
            End Using


            Return CreditCardList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCardInformation(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCardInformation(con, sMode, objCard)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCardInformationServ(ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try


            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCardInformationServ(con, sMode, objCard)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCardInformationVirtualReceipt(ByVal sMode As String, ByVal objCard As clsCCardInfo, ByVal ReceiptCode As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try


            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCardInformationVirtualReceipt(con, sMode, objCard, ReceiptCode)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAddons(addonID As Long, reservation As String, ByVal quantity As Int16, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim response As New Response()
        Try


            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAddons(con, addonID, reservation, quantity, Amount)
            End Using

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAddonsQty(ByVal companyCode As String) As Integer

        Dim dataHelper As New DataHelper()
        Dim lResult As Integer

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = GetAddonsQty(con, companyCode)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCombinations(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Integer

        Dim dataHelper As New DataHelper()
        Dim lResult As Integer

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = GetCombinations(con, companyCode, adults, children)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveBankLog(ByVal ReservationID As String, ByVal Response As String,
                                ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                                ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim resp As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                resp = SaveBankLog(con, ReservationID, Response,
                                 AuthCode, ResponseCode, ErrorCode,
                                 ResponseText, TransactionID, Amount)
            End Using

            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveBankLogQuo(ByVal QuotationID As String, ByVal Response As String,
                            ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                            ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim resp As New Response()

        Try
            Using con As SqlConnection = dataHelper.OpenConnection()
                resp = SaveBankLog(con, QuotationID, Response,
                             AuthCode, ResponseCode, ErrorCode,
                             ResponseText, TransactionID, Amount)
            End Using

            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveBankPayPalLog(ByVal ReservationID As String, ByVal TransactionID As String,
                            ByVal PaymentStatus As String, ByVal ResponseText As String, ByVal PayerID As String,
                            ByVal CorrelationID As String) As Response
        Dim dataHelper As New DataHelper()
        Dim resp As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                resp = SaveBankPayPalLog(con, ReservationID, TransactionID,
                             PaymentStatus, ResponseText, PayerID,
                             CorrelationID)
            End Using

            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveBNLog(ByVal AuthorizationResult As Long, ByVal AuthorizationCode As String, ByVal ErrorCode As String,
                                ByVal ErrorMessage As String, ByVal OrderId As String) As Response
        Dim dataHelper As New DataHelper()
        Dim resp As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                resp = SaveBNLog(con, AuthorizationResult, AuthorizationCode,
                             ErrorCode, ErrorMessage, OrderId)
            End Using

            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetReservation(ByVal sReservationID As String) As List(Of clsReservationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ReservList As New List(Of clsReservationInfo)()


            Using con As SqlConnection = dataHelper.OpenConnection()
                ReservList = GetReservation(con, sReservationID)
            End Using

            Return ReservList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCredomaticOrderId(ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Long

        Dim dataHelper As New DataHelper()
        Dim lResult As Integer

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = GetCredomaticOrderId(con, companyCode, adults, children)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetQuotationToPay(ByVal sReservationID As String) As List(Of clsReservationPay)
        Try
            Dim dataHelper As New DataHelper()
            Dim ReservList As New List(Of clsReservationPay)()


            Using con As SqlConnection = dataHelper.OpenConnection()
                ReservList = GetQuotationToPay(con, sReservationID)
            End Using

            Return ReservList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetReservationRooms(ByVal sReservationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim list As New List(Of clsReservationsRoomInfo)


            Using con As SqlConnection = dataHelper.OpenConnection()
                list = GetReservationRooms(con, sReservationID, Culture)
            End Using

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetReservationsNotSynced(ByVal integrationID As Int16, ByVal companyCode As String) As List(Of clsReservationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsReservationInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetReservationsNotSynced(con, integrationID, companyCode)
            End Using

            Return List

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateDiscount(ByVal sCompanyId As String,
                                    ByVal Rate_code As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date) As Double
        Dim dataHelper As New DataHelper()
        Dim lResult As Double

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = GetRateDiscount(con, sCompanyId, Rate_code, ArrivalDate, DepartureDate)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRateConditions(ByVal company_code As String, ByVal rate_code As String, ByVal culture As String) As String
        Dim dataHelper As New DataHelper()
        Dim lResult As String

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                lResult = GetRateConditions(con, company_code, rate_code, culture)
            End Using

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetDaysAvailability(ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date) As List(Of clsDayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim list As New List(Of clsDayAvailability)


            Using con As SqlConnection = dataHelper.OpenConnection()
                list = GetDaysAvailability(con, company_code, arrival, departure)
            End Using

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRateException(ByVal Reservations As String) As Response
        Dim dataHelper As New DataHelper()
        Dim response As New Response()

        Try
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRateException(con, Reservations)
            End Using
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveQuotationToReservation(ByVal objRes As clsReservationsInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim resp As New Response()

        Try

            Using con As SqlConnection = dataHelper.OpenConnection()
                resp = SaveQuotationToReservation(con, objRes)
            End Using

            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function QueryReservations(ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of clsQryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim list As New List(Of clsQryReservationConfirmation)


            Using con As SqlConnection = dataHelper.OpenConnection()
                list = QueryReservations(con, DateFrom, DateTo)
            End Using

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function GetFreeNights(con As SqlConnection,
                               ByVal company_code As String, ByVal sCompanyCode As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim Existe As Integer = 0

            With cmd
                .CommandText = "GetFreeNights"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", company_code)
                .Parameters.AddWithValue("@rate_code", sCompanyCode)
            End With


            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While objDr.Read()
                Existe = objDr.Item("Existe") & ""
            End While

            objDr.Close()

            Return Existe

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationRoomsQuo(con As SqlConnection, ByVal sQuotationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objCards As New List(Of clsReservationsRoomInfo)
            Dim objCard As New clsReservationsRoomInfo

            With cmd
                .CommandText = "spGetReservationRoomsQuo"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("quotation_id", sQuotationID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While objDr.Read()

                objCard = New clsReservationsRoomInfo
                With objCard
                    .ppDescription = objDr.Item("description") & ""
                    .ppAdults = Val(objDr.Item("adults") & "")
                    .ppChildren = Val(objDr.Item("children") & "")
                    If IsNumeric(objDr.Item("rate_amount")) Then
                        .ppRateAmount = CDbl(objDr.Item("rate_amount"))
                    End If
                    .ppChildrenFree = Val(objDr.Item("children_free") & "")
                    If IsDate(objDr.Item("date")) Then
                        .ppDate = CDate(objDr.Item("date"))
                    End If
                End With
                objCards.Add(objCard)

            End While

            objDr.Close()

            Return objCards

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveReservation(con As SqlConnection, ByVal sMode As String, ByVal sProcessor As String, reservation As clsReservationsInfo) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As String = ""

        Try

            With reservation
                cmd.CommandText = "spReservations"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Parameters.AddWithValue("Mode", sMode)
                cmd.Parameters.AddWithValue("reservation_id", .ppReservationID)
                cmd.Parameters.AddWithValue("guest_id", .ppGuestID)
                cmd.Parameters.AddWithValue("company_code", .ppCompany_Code)
                cmd.Parameters.AddWithValue("arrival", .ppArrival)
                cmd.Parameters.AddWithValue("departure", .ppDeparture)
                cmd.Parameters.AddWithValue("creation_date", .ppCreation_Date)
                cmd.Parameters.AddWithValue("cancellation_date", .ppCancellation_Date)
                cmd.Parameters.AddWithValue("rate_code", .ppRate_Code)
                cmd.Parameters.AddWithValue("rate_amount", .ppRateAmount)
                cmd.Parameters.AddWithValue("tax_amount", .ppTaxAmount)
                cmd.Parameters.AddWithValue("ccard_transid", .ppCcard_TransID)
                cmd.Parameters.AddWithValue("remarks", .ppRemarks)
                cmd.Parameters.AddWithValue("visit_reason", .ppVisit_Reason)
                cmd.Parameters.AddWithValue("promo_id", .ppPromoID)
                cmd.Parameters.AddWithValue("addon_amount", .ppAddonAmount)
                cmd.Parameters.AddWithValue("promo_amount", .ppPromoAmount)
                cmd.Parameters.AddWithValue("disc_amount", .ppDiscount)
                cmd.Parameters.AddWithValue("processor", sProcessor)
                cmd.Parameters.AddWithValue("rooms", .ppRooms)
                cmd.Parameters.AddWithValue("origin", .ppOrigin)
                cmd.Parameters.AddWithValue("resellerID", .ppResellerID)
                cmd.Parameters.AddWithValue("sessionID", .ppSessionID)
            End With

            lResult = dataHelper.ExecuteScalar(cmd, con)

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function UpdateReservation(con As SqlConnection, ByVal sProcessor As String, reservation As clsReservationsInfo) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As String = ""

        Try

            With reservation
                cmd.CommandText = "spUpdateReservations"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Parameters.AddWithValue("reservation_id", .ppReservationID)
                cmd.Parameters.AddWithValue("guest_id", .ppGuestID)
                cmd.Parameters.AddWithValue("company_code", .ppCompany_Code)
                cmd.Parameters.AddWithValue("arrival", .ppArrival)
                cmd.Parameters.AddWithValue("departure", .ppDeparture)
                cmd.Parameters.AddWithValue("rate_code", .ppRate_Code)
                cmd.Parameters.AddWithValue("rate_amount", .ppRateAmount)
                cmd.Parameters.AddWithValue("tax_amount", .ppTaxAmount)
                cmd.Parameters.AddWithValue("ccard_transid", .ppCcard_TransID)
                cmd.Parameters.AddWithValue("remarks", .ppRemarks)
                cmd.Parameters.AddWithValue("visit_reason", .ppVisit_Reason)
                cmd.Parameters.AddWithValue("promo_id", .ppPromoID)
                cmd.Parameters.AddWithValue("addon_amount", .ppAddonAmount)
                cmd.Parameters.AddWithValue("promo_amount", .ppPromoAmount)
                cmd.Parameters.AddWithValue("disc_amount", .ppDiscount)
                cmd.Parameters.AddWithValue("processor", sProcessor)
                cmd.Parameters.AddWithValue("rooms", .ppRooms)
                cmd.Parameters.AddWithValue("origin", .ppOrigin)
                cmd.Parameters.AddWithValue("resellerID", .ppResellerID)
                cmd.Parameters.AddWithValue("sessionID", .ppSessionID)
            End With

            lResult = dataHelper.ExecuteScalar(cmd, con)

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteReservationRoom(con As SqlConnection, ByVal sReservationNumber As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spReservationsRooms"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "DEL")
                .Parameters.AddWithValue("reservation_id", sReservationNumber)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveReservationRoom(con As SqlConnection, ByVal sMode As String, ReservationRoom As clsReservationsRoomInfo) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try

            With ReservationRoom
                cmd.CommandText = "spReservationsRooms"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Parameters.AddWithValue("Mode", sMode)
                cmd.Parameters.AddWithValue("reservation_id", .ppReservationID)
                cmd.Parameters.AddWithValue("roomtype_id", .ppRoomTypeID)
                cmd.Parameters.AddWithValue("adults", .ppAdults)
                cmd.Parameters.AddWithValue("children", .ppChildren)
                cmd.Parameters.AddWithValue("rate_amount", .ppRateAmount)
                cmd.Parameters.AddWithValue("tax_amount", .ppTaxAmount)
                cmd.Parameters.AddWithValue("children_free", .ppChildrenFree)
                cmd.Parameters.AddWithValue("date", .ppDate)
            End With

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailability(con As SqlConnection, ByVal ResNumber As String, ByVal KeyId As Long) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try


            cmd.CommandText = "spUpdateAvailabilityRooms"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("reservation_id", ResNumber)
            cmd.Parameters.AddWithValue("keyID", KeyId)


            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailabilityQuo(con As SqlConnection, ByVal ResNumber As String) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try


            cmd.CommandText = "spUpdateAvailabilityRoomsQuo"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("reservation_id", ResNumber)

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function CheckStatus(con As SqlConnection, ByVal OrderId As String) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim Existe As Integer = 0

            With cmd
                .CommandText = "spCheckStatus"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@OrderId", OrderId)
            End With


            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While objDr.Read()
                Existe = objDr.Item("Existe") & ""
            End While

            objDr.Close()

            Return Existe

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function UpdateStatus(con As SqlConnection, ByVal ResNumber As String, ByVal Status As Int16, ByVal KeyID As Long) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Long

        Try


            cmd.CommandText = "spUpdateStatus"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("reservation_id", ResNumber)
            cmd.Parameters.AddWithValue("status", Status)
            cmd.Parameters.AddWithValue("keyId", KeyID)

            lResult = CLng(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailabilityQuery(con As SqlConnection, ByVal CompanyCode As String, ByVal IP As String, ByVal Arrival As Date,
                                     ByVal Departure As Date, ByVal Adults As Int16, ByVal Children As Int16,
                                     ByVal Rooms As Int16, ByVal Nights As Int16,
                                     ByVal PromoCode As String, ByVal MessageCode As String, ByVal sGuid As System.Guid) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try


            cmd.CommandText = "spAvailabilityQuery"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("Mode", "INS")
            cmd.Parameters.AddWithValue("companyCode", CompanyCode)
            cmd.Parameters.AddWithValue("Session", sGuid)
            cmd.Parameters.AddWithValue("IP", IP)
            cmd.Parameters.AddWithValue("Arrival", Arrival)
            cmd.Parameters.AddWithValue("Departure", Departure)
            cmd.Parameters.AddWithValue("Adults", Adults)
            cmd.Parameters.AddWithValue("Children", Children)
            cmd.Parameters.AddWithValue("Rooms", Rooms)
            cmd.Parameters.AddWithValue("Nights", Nights)
            cmd.Parameters.AddWithValue("Promocode", PromoCode)
            cmd.Parameters.AddWithValue("MessageCode", MessageCode)

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return Response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCardTypes(con As SqlConnection, ByVal Bank As String) As List(Of clsCardTypeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objCreditCard As New clsCardTypeInfo
            Dim CreditCardList As New List(Of clsCardTypeInfo)()


            With cmd
                .CommandText = "spCreditCards"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SEL")
                .Parameters.AddWithValue("type", Bank)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objCreditCard = New clsCardTypeInfo
                With objCreditCard
                    .ppCode = reader.Item("ccard_code") & ""
                    .ppName = reader.Item("ccard_name") & ""
                End With
                CreditCardList.Add(objCreditCard)
            End While

            reader.Close()

            Return CreditCardList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveCardInformation(con As SqlConnection, ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try

            With objCard
                sqlCmd.CommandText = "spCreditCardsTrans"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Connection = con
                sqlCmd.Parameters.AddWithValue("Mode", sMode)
                sqlCmd.Parameters.AddWithValue("ccard_code", .ppCardCode)
                sqlCmd.Parameters.AddWithValue("reservation_id", .ppReservationID)
                sqlCmd.Parameters.AddWithValue("ccard_nr", .ppCardNumber)
                sqlCmd.Parameters.AddWithValue("ccard_exp", .ppCardExp)
                sqlCmd.Parameters.AddWithValue("auth_nr", .ppAuthNr)
                sqlCmd.Parameters.AddWithValue("approb_code", .ppApprobCode)
                sqlCmd.Parameters.AddWithValue("name_card", .ppNameCard)
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveCardInformationServ(con As SqlConnection, ByVal sMode As String, ByVal objCard As clsCCardInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try

            With objCard
                sqlCmd.CommandText = "spCreditCardsTransServ"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Connection = con
                sqlCmd.Parameters.AddWithValue("Mode", sMode)
                sqlCmd.Parameters.AddWithValue("ccard_code", .ppCardCode)
                sqlCmd.Parameters.AddWithValue("reservation_id", .ppReservationID)
                sqlCmd.Parameters.AddWithValue("ccard_nr", .ppCardNumber)
                sqlCmd.Parameters.AddWithValue("ccard_exp", .ppCardExp)
                sqlCmd.Parameters.AddWithValue("auth_nr", .ppAuthNr)
                sqlCmd.Parameters.AddWithValue("approb_code", .ppApprobCode)
                sqlCmd.Parameters.AddWithValue("name_card", .ppNameCard)
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveCardInformationVirtualReceipt(con As SqlConnection, ByVal sMode As String, ByVal objCard As clsCCardInfo, ByVal ReceiptCode As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try

            With objCard
                sqlCmd.CommandText = "spCreditCardsTransVirtualReceipt"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Connection = con
                sqlCmd.Parameters.AddWithValue("Mode", sMode)
                sqlCmd.Parameters.AddWithValue("receipt_code", ReceiptCode)
                sqlCmd.Parameters.AddWithValue("ccard_code", .ppCardCode)
                sqlCmd.Parameters.AddWithValue("ccard_nr", .ppCardNumber)
                sqlCmd.Parameters.AddWithValue("ccard_exp", .ppCardExp)
                sqlCmd.Parameters.AddWithValue("auth_nr", .ppAuthNr)
                sqlCmd.Parameters.AddWithValue("approb_code", .ppApprobCode)
                sqlCmd.Parameters.AddWithValue("name_card", .ppNameCard)
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAddons(con As SqlConnection, addonID As Long, reservation As String, ByVal quantity As Int16, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try


            sqlCmd.CommandText = "spReservationsAddons"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("addonID", addonID)
            sqlCmd.Parameters.AddWithValue("reservation_id", reservation)
            sqlCmd.Parameters.AddWithValue("quantity", quantity)
            sqlCmd.Parameters.AddWithValue("amount", Amount)


            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAddons(con As SqlConnection, reservationId As String) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try


            sqlCmd.CommandText = "spDeleteReservationsAddons"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("reservation_id", reservationId)


            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddonsQty(con As SqlConnection, ByVal companyCode As String) As Integer

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Integer

        Try


            cmd.CommandText = "spAddonsQty"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("companyCode", companyCode)


            lResult = CInt(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCombinations(con As SqlConnection, ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Integer

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Integer

        Try

            cmd.CommandText = "spGetCombinations"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("company_code", companyCode)
            cmd.Parameters.AddWithValue("adults", adults)
            cmd.Parameters.AddWithValue("children", children)

            lResult = CInt(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveBankLog(con As SqlConnection, ByVal ReservationID As String, ByVal Response As String,
                                ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                                ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim resp As New Response()

        Try
            sqlCmd.CommandText = "spBankTransactions"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("Mode", "INS")
            sqlCmd.Parameters.AddWithValue("reservationID", ReservationID)
            sqlCmd.Parameters.AddWithValue("response", Response)
            sqlCmd.Parameters.AddWithValue("responseCode", ResponseCode)
            sqlCmd.Parameters.AddWithValue("authCode", AuthCode)
            sqlCmd.Parameters.AddWithValue("errorCode", ErrorCode)
            sqlCmd.Parameters.AddWithValue("responseText", ResponseText)
            sqlCmd.Parameters.AddWithValue("transactionID", TransactionID)
            sqlCmd.Parameters.AddWithValue("Amount", Amount)


            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            resp.Status = "0"
            resp.Message = "Command executes OK"
            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveBankLogQuo(con As SqlConnection, ByVal QuotationID As String, ByVal Response As String,
                            ByVal AuthCode As String, ByVal ResponseCode As String, ByVal ErrorCode As String,
                            ByVal ResponseText As String, ByVal TransactionID As String, ByVal Amount As Double) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim resp As New Response()

        Try
            sqlCmd.CommandText = "spBankTransactionsQuo"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("Mode", "INS")
            sqlCmd.Parameters.AddWithValue("quotationID", QuotationID)
            sqlCmd.Parameters.AddWithValue("response", Response)
            sqlCmd.Parameters.AddWithValue("responseCode", ResponseCode)
            sqlCmd.Parameters.AddWithValue("authCode", AuthCode)
            sqlCmd.Parameters.AddWithValue("errorCode", ErrorCode)
            sqlCmd.Parameters.AddWithValue("responseText", ResponseText)
            sqlCmd.Parameters.AddWithValue("transactionID", TransactionID)
            sqlCmd.Parameters.AddWithValue("Amount", Amount)

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            resp.Status = "0"
            resp.Message = "Command executes OK"
            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveBankPayPalLog(con As SqlConnection, ByVal ReservationID As String, ByVal TransactionID As String,
                            ByVal PaymentStatus As String, ByVal ResponseText As String, ByVal PayerID As String, ByVal CorrelationID As String) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim resp As New Response()

        Try
            sqlCmd.CommandText = "spBankTransactionsPayPal"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("Mode", "INS")
            sqlCmd.Parameters.AddWithValue("reservationID", ReservationID)
            sqlCmd.Parameters.AddWithValue("transactionID", TransactionID)
            sqlCmd.Parameters.AddWithValue("paymentStatus", PaymentStatus)
            sqlCmd.Parameters.AddWithValue("responseText", ResponseText)
            sqlCmd.Parameters.AddWithValue("payerID", PayerID)
            sqlCmd.Parameters.AddWithValue("correlationID", CorrelationID)

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            resp.Status = "0"
            resp.Message = "Command executes OK"
            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveBNLog(con As SqlConnection, ByVal AuthorizationResult As Long, ByVal AuthorizationCode As String, ByVal ErrorCode As String,
                                ByVal ErrorMessage As String, ByVal OrderId As String) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim resp As New Response()

        Try
            sqlCmd.CommandText = "spBNLog"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("Mode", "INS")
            sqlCmd.Parameters.AddWithValue("AuthorizationResult", AuthorizationResult)
            sqlCmd.Parameters.AddWithValue("AuthorizationCode", AuthorizationCode)
            sqlCmd.Parameters.AddWithValue("ErrorCode", ErrorCode)
            sqlCmd.Parameters.AddWithValue("ErrorMessage", ErrorMessage)
            sqlCmd.Parameters.AddWithValue("OrderId", OrderId)

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            resp.Status = "0"
            resp.Message = "Command executes OK"
            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservation(con As SqlConnection, ByVal sReservationID As String) As List(Of clsReservationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objReserv As New clsReservationInfo
            Dim ReservList As New List(Of clsReservationInfo)()


            With cmd
                .CommandText = "spReservations"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "RES")
                .Parameters.AddWithValue("reservation_id", sReservationID)
            End With

            Dim sqlDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While sqlDr.Read()
                objReserv = New clsReservationInfo
                With objReserv
                    .ppName = sqlDr.Item("name") & ""
                    .ppLastName = sqlDr.Item("lastname") & ""

                    If IsDate(sqlDr.Item("arrival")) Then
                        .ppArrival = sqlDr.Item("arrival")
                    End If
                    If IsDate(sqlDr.Item("departure")) Then
                        .ppDeparture = sqlDr.Item("departure")
                    End If

                    .ppEmail = sqlDr.Item("email") & ""
                    .ppTelephone = sqlDr.Item("telephone_nr") & ""
                    .ppCardType = sqlDr.Item("ccard_code") & ""
                    If sqlDr.Item("ccard_nr") <> "" Then .ppCardNumber = sqlDr.Item("ccard_nr") & "" 'Decrypt(sqlDr.Item("ccard_nr"), "X72@p91a")
                    .ppCardExp = sqlDr.Item("ccard_exp") & ""

                    If IsNumeric(sqlDr.Item("rate_amount")) Then
                        .ppRateAmount = sqlDr.Item("rate_amount")
                    End If

                    .ppRate = sqlDr.Item("rate_name") & ""
                    .ppCountry = sqlDr.Item("country_name") & ""
                    .ppRooms = sqlDr.Item("rooms") & ""

                    If IsNumeric(sqlDr.Item("tax_amount")) Then
                        .ppTaxAmount = sqlDr.Item("tax_amount")
                    End If

                    If IsNumeric(sqlDr.Item("addon_amount")) Then
                        .ppAddonAmount = sqlDr.Item("addon_amount")
                    End If

                    If IsNumeric(sqlDr.Item("promo_amount")) Then
                        .ppPromoAmount = sqlDr.Item("promo_amount")
                    End If

                    If IsNumeric(sqlDr.Item("disc_amount")) Then
                        .ppDiscAmount = sqlDr.Item("disc_amount")
                    End If

                    .ppVisitReason = sqlDr.Item("visit_reason") & ""
                    .ppRemarks = sqlDr.Item("remarks") & ""
                    .ppCountryISO = sqlDr.Item("country_iso3") & ""
                    .ppRateTerms = sqlDr.Item("rate_conditions") & ""

                    If IsDate(sqlDr.Item("creation_date")) Then
                        .ppCreation = sqlDr.Item("creation_date")
                    End If
                End With
                ReservList.Add(objReserv)
            End While

            sqlDr.Close()

            Return ReservList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCredomaticOrderId(con As SqlConnection, ByVal companyCode As String, ByVal adults As Integer, ByVal children As Integer) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Integer

        Try

            cmd.CommandText = "spConsecutives"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("ID", 1)

            lResult = CInt(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetQuotationToPay(con As SqlConnection, ByVal sReservationID As String) As List(Of clsReservationPay)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objReserv As New clsReservationPay
            Dim ReservList As New List(Of clsReservationPay)()


            With cmd
                .CommandText = "spGetQuotationToPay"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("quotation_id", sReservationID)
            End With

            Dim sqlDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While sqlDr.Read()
                objReserv = New clsReservationPay
                With objReserv
                    .ppGuestID = sqlDr.Item("guest_id") & ""

                    If IsNumeric(sqlDr.Item("rate_amount")) Then
                        .ppAmount = sqlDr.Item("rate_amount")
                    End If

                    .ppName = sqlDr.Item("name") & ""
                    .ppLastName = sqlDr.Item("lastname") & ""
                    .ppEmail = sqlDr.Item("email") & ""

                    If IsDate(sqlDr.Item("arrival")) Then
                        .ppArrival = sqlDr.Item("arrival")
                    End If
                    If IsDate(sqlDr.Item("departure")) Then
                        .ppDeparture = sqlDr.Item("departure")
                    End If

                    If IsNumeric(sqlDr.Item("addon_amount")) Then
                        .ppAddonAmount = sqlDr.Item("addon_amount")
                    End If
                    .ppCountry = sqlDr.Item("country_name") & ""
                    .ppTelephone = sqlDr.Item("telephone_nr") & ""
                End With
                ReservList.Add(objReserv)
            End While

            sqlDr.Close()

            Return ReservList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationRooms(con As SqlConnection, ByVal sReservationID As String, ByVal Culture As String) As List(Of clsReservationsRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objCards As New List(Of clsReservationsRoomInfo)
            Dim objCard As New clsReservationsRoomInfo

            With cmd
                .CommandText = "spGetReservationRoomsQuo"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("reservation_id", sReservationID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While objDr.Read()

                objCard = New clsReservationsRoomInfo
                With objCard
                    .ppDescription = objDr.Item("description") & ""
                    .ppAdults = Val(objDr.Item("adults") & "")
                    .ppChildren = Val(objDr.Item("children") & "")
                    If IsNumeric(objDr.Item("rate_amount")) Then
                        .ppRateAmount = CDbl(objDr.Item("rate_amount"))
                    End If
                    .ppChildrenFree = Val(objDr.Item("children_free") & "")
                    If IsDate(objDr.Item("date")) Then
                        .ppDate = CDate(objDr.Item("date"))
                    End If
                    .ppRoomTypeID = Val(objDr.Item("roomtype_id") & "")

                End With
                objCards.Add(objCard)

            End While

            objDr.Close()

            Return objCards

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationsNotSynced(con As SqlConnection, ByVal integrationID As Int16, ByVal companyCode As String) As List(Of clsReservationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objCards As New List(Of clsReservationInfo)
            Dim objCard As New clsReservationInfo

            With cmd
                .CommandText = "spSyncReservationsNotSynced"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                If integrationID > 0 Then
                    .Parameters.AddWithValue("integration", integrationID)
                Else
                    .Parameters.AddWithValue("companyCode", companyCode)
                End If
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While objDr.Read()

                objCard = New clsReservationInfo
                With objCard
                    .ppReservationID = objDr.Item("reservation_id") & ""
                    .ppIntegrationIP = objDr.Item("integrationIP") & ""
                    .ppIntegrationPort = Val(objDr.Item("integrationPort") & "")
                End With
                objCards.Add(objCard)

            End While

            objDr.Close()

            Return objCards

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateDiscount(con As SqlConnection, ByVal sCompanyId As String,
                                    ByVal Rate_code As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date) As Double
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As Double

        Try

            cmd.CommandText = "spGetRateDiscount"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("company_code", sCompanyId)
            cmd.Parameters.AddWithValue("rate_code", Rate_code)
            cmd.Parameters.AddWithValue("arrival", ArrivalDate.Date)
            cmd.Parameters.AddWithValue("departure", DepartureDate.Date)

            lResult = CDbl(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRateConditions(con As SqlConnection, ByVal company_code As String, ByVal rate_code As String, ByVal culture As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim lResult As String

        Try

            cmd.CommandText = "spRateConditions"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("company_code", company_code)
            cmd.Parameters.AddWithValue("rate_code", rate_code)
            cmd.Parameters.AddWithValue("culture", culture)

            lResult = Str(dataHelper.ExecuteScalar(cmd, con))

            Return lResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetDaysAvailability(con As SqlConnection, ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date) As List(Of clsDayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim list As New List(Of clsDayAvailability)
            Dim objAv As New clsDayAvailability

            With cmd
                .CommandText = "spGetAvailabilityDays"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("arrival", arrival)
                .Parameters.AddWithValue("departure", departure)
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While objDr.Read()

                objAv = New clsDayAvailability
                With objAv
                    If IsDate(objDr.Item("date")) Then
                        .ppDay = objDr.Item("date")
                    End If
                    .ppQty = Val(objDr.Item("Qty") & "")
                End With
                list.Add(objAv)

            End While

            objDr.Close()

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRateException(con As SqlConnection, ByVal Reservations As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Dim str As StringBuilder

        Try

            str = New StringBuilder("UPDATE dbo.reservation SET synced = 1 WHERE reservation_id IN (" & Reservations & ")")

            Call dataHelper.ExecuteNonQuery(str, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveQuotationToReservation(con As SqlConnection, ByVal objRes As clsReservationsInfo) As Response
        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim resp As New Response()

        Try
            sqlCmd.CommandText = "spQuotationToReservation"
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Connection = con
            sqlCmd.Parameters.AddWithValue("quotation_id", objRes.ppReservationID)
            sqlCmd.Parameters.AddWithValue("remarks", objRes.ppRemarks)
            sqlCmd.Parameters.AddWithValue("visit_reason", objRes.ppVisit_Reason)

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            resp.Status = "0"
            resp.Message = "Command executes OK"
            Return resp

        Catch EX1 As Exception
            resp.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function QueryReservations(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of clsQryReservationConfirmation)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim list As New List(Of clsQryReservationConfirmation)
            Dim objRes As New clsQryReservationConfirmation
            Dim mCC As String
            Dim days As TimeSpan

            With cmd
                .CommandText = "spQueryReservationConfirmationAll"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@DateFrom", DateFrom)
                .Parameters.AddWithValue("@DateTo", DateTo)
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While objDr.Read()

                objRes = New clsQryReservationConfirmation
                With objRes
                    .reservation = objDr.Item("reservation") & ""
                    .name = objDr.Item("name") & ""
                    .lastname = objDr.Item("lastname") & ""
                    If IsDate(objDr.Item("arrival")) Then
                        .arrival = objDr.Item("arrival")
                    End If
                    If IsDate(objDr.Item("departure")) Then
                        .departure = objDr.Item("departure")
                    End If
                    days = CDate(objDr.Item("departure")).Subtract(objDr.Item("arrival"))
                    .nights = days.TotalDays
                    .amount = objDr.Item("amount")
                    If (objDr.Item("creditCard") & "") <> String.Empty Then
                        'mCC = Decrypt(objDr.Item("creditCard"), "X72@p91a")
                        mCC = objDr.Item("creditCard") & ""
                        .creditCard = mCC & "  "
                    Else
                        .creditCard = ""
                    End If
                    .expDate = objDr.Item("expDate") & ""
                    .approveCode = objDr.Item("approveCode") & ""
                    .secCode = objDr.Item("secCode") & ""
                    .cardType = objDr.Item("cardType") & ""
                    .status = objDr.Item("status") & ""
                    .country = objDr.Item("company_name") & ""
                    If IsDate(objDr.Item("addons")) Then
                        .created = objDr.Item("creation_date")
                    End If
                    .rateName = objDr.Item("rate_name") & ""
                    .processor = objDr.Item("processor") & ""
                    If IsNumeric(objDr.Item("addons")) Then
                        .addonAmount = objDr.Item("addons")
                    End If
                End With
                list.Add(objRes)

            End While

            objDr.Close()

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
