﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Public Class Direcciones

#Region "Public Methods"
    Public Function GetStates(ByVal sCountryIso3 As String) As List(Of StatesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim StateList As New List(Of StatesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                StateList = GetStates(con, sCountryIso3)
            End Using
            Return StateList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCountries() As List(Of CountriesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CountryList As New List(Of CountriesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CountryList = GetCountries(con)
            End Using
            Return CountryList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

#Region "Private Methods"

    Friend Function GetStates(ByVal con As SqlConnection, ByVal sCountryIso3 As String) As List(Of StatesInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim StateList As New List(Of StatesInfo)()
        Dim sObjStates As New StatesInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spStates"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "SEL")
                .Parameters.AddWithValue("country_iso3", sCountryIso3)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                Dim sObjState As New StatesInfo
                With sObjState
                    .ppStateCode = reader.Item("state_code")
                    .ppName = reader.Item("state_name")
                End With
                StateList.Add(sObjState)
            End While

            reader.Close()
            Return StateList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCountries(ByVal con As SqlConnection) As List(Of CountriesInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim CountryList As New List(Of CountriesInfo)()
        Dim sObjcountries As New CountriesInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetCountries"
                .CommandType = CommandType.StoredProcedure
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                Dim sObjState As New CountriesInfo
                With sObjState
                    .ppCountryIso3 = reader.Item("country_iso3")
                    .ppName = reader.Item("country_name_eng")
                End With
                CountryList.Add(sObjState)
            End While

            reader.Close()
            Return CountryList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

End Class
