﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class AgencyTypes

#Region "Public Methods"

    Public Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of CultureInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetCulture(con, iCultureID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAgenciesTypes(ByVal AgencyTypeCode As Int16) As List(Of AgencyTypeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of AgencyTypeInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetAgenciesTypes(con, AgencyTypeCode)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAgenciesTypes(con As SqlConnection, ByVal AgencyTypeCode As Int16) As List(Of AgencyTypeInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim InfoList As New List(Of AgencyTypeInfo)()
        Dim objInfo As New AgencyTypeInfo
        Dim dtExpiry As New Date
        Dim dtCreated As New Date
        Try

            With cmd
                .CommandText = "spAgencyType"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@agency_type_code", AgencyTypeCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objInfo = New AgencyTypeInfo
                With objInfo
                    .ppAgencyTypeCode = reader.Item("agency_type_code")
                    .ppTypeName = reader.Item("type_name")
                End With
                InfoList.Add(objInfo)

            End While

            reader.Close()

            Return InfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function GetCulture(con As SqlConnection, ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CultureInfoList As New List(Of CultureInfo)()
            Dim objCultureInfo As New CultureInfo

            With cmd
                .CommandText = "spCulture"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@cultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objCultureInfo = New CultureInfo

                With objCultureInfo
                    .ppCultureID = Val(reader.Item("CultureID") & "")
                    .ppCultureName = reader.Item("CultureName") & ""
                    .ppDisplayName = reader.Item("DisplayName") & ""
                End With
                CultureInfoList.Add(objCultureInfo)

            End While

            reader.Close()

            Return CultureInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

End Class
