﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Session
#Region "Public Methods"
    Public Function SaveCompanySession(ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveCompanySession(con, SessionID, CompanyCode, Language)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateSessionRate(con As SqlConnection, ByVal SessionID As System.Guid, ByVal rateCode As String,
                                         ByVal arrivalDate As Date, ByVal departureDate As Date, ByVal rooms As Integer,
                                         ByVal room1Qty As String, ByVal room2Qty As String,
                                         ByVal room3Qty As String, ByVal room4Qty As String,
                                         ByVal promoID As Integer, ByVal resellerID As String) As Response
        Try

            Dim response As New Response

            response = UpdateSessionRate(con, SessionID, rateCode, arrivalDate, departureDate, rooms, room1Qty, room2Qty, room3Qty, room4Qty, promoID, resellerID, "UP1")


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function UpdateSessionService(ByVal SessionID As System.Guid,
                                     ByVal arrivalDate As Date, ByVal services As Integer,
                                     ByVal adults As Integer, ByVal children As Integer,
                                     ByVal infants As Integer, ByVal students As Integer,
                                     ByVal promoID As Integer, ByVal resellerID As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    sResult = UpdateSessionService(con, SessionID, arrivalDate, services, adults, children, infants, students, promoID, resellerID)
                End Using
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Public Function UpdateSessionRooms(ByVal SessionID As String, ByVal RateCode As String, ByVal Room1Code As String, ByVal Room2Code As String, ByVal Room3Code As String, ByVal Room4Code As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = UpdateSessionRooms(con, SessionID, RateCode, Room1Code, Room2Code, Room3Code, Room4Code)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdatePaypalToken(ByVal SessionID As String, ByVal PayPalToken As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = UpdatePaypalToken(con, SessionID, PayPalToken)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function UpdateAddons(ByVal SessionID As String, ByVal Addons As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = UpdateAddons(con, SessionID, Addons)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateStatus(ByVal company_Code As String, ByVal Active As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = UpdateStatus(con, company_Code, Active)
                End Using
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function CheckSessionID(ByVal SessionID As String) As Boolean
        Try
            Dim dataHelper As New DataHelper()
            Dim response As Boolean

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = CheckSessionID(con, SessionID)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function CheckSessionServiceID(ByVal SessionID As String) As Boolean
        Try
            Dim dataHelper As New DataHelper()
            Dim response As Boolean

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = CheckSessionServiceID(con, SessionID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanySessionByReservationID(ByVal ReservationID As String) As List(Of SessionInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of SessionInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetCompanySessionByReservationID(con, ReservationID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanySessionByPPToken(ByVal PayPalToken As String) As List(Of SessionInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of SessionInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetCompanySessionByPPToken(con, PayPalToken)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanySession(ByVal SessionID As String) As List(Of SessionInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of SessionInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetCompanySession(con, SessionID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanySessionService(ByVal Mode As String, ByVal SessionID As String, ByVal SaleID As String) As List(Of SessionServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of SessionServiceInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetCompanySessionService(con, Mode, SessionID, SaleID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanySessionService(ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveCompanySessionService(con, SessionID, CompanyCode, Language)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function InsertSessionServiceSC(ByVal SessionID As String, ByVal serviceID As Integer,
                             ByVal arrivalDate As Date, ByVal ScheduleID As Integer,
                             ByVal hotelPickupID As Integer, ByVal hotelPickupHourID As Integer,
                             ByVal adults As Integer, ByVal children As Integer,
                             ByVal infants As Integer, ByVal students As Integer,
                             ByVal priceAdults As Double, ByVal priceChildren As Double,
                             ByVal priceInfants As Double, ByVal priceStudents As Double) As Response

        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = InsertSessionServiceSC(con, SessionID, serviceID, arrivalDate, ScheduleID,
                              hotelPickupID, hotelPickupHourID, adults, children, infants, students,
                              priceAdults, priceChildren, priceInfants, priceStudents)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Public Function GetSessionServiceSC(ByVal SessionID As String, ByVal Culture As String) As List(Of ServiceSCInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As List(Of ServiceSCInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetSessionServiceSC(con, SessionID, Culture)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRowSC(ByVal RowID As Long, ByVal SessionID As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = DeleteRowSC(con, RowID, SessionID)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region
#Region "Private Methods"

    Friend Function SaveCompanySession(con As SqlConnection, ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try

            With cmd
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "INS")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("companyCode", CompanyCode)
                .Parameters.AddWithValue("lang", Language)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function UpdateSessionRate(con As SqlConnection, ByVal SessionID As System.Guid, ByVal rateCode As String,
                                         ByVal arrivalDate As Date, ByVal departureDate As Date, ByVal rooms As Integer,
                                         ByVal room1Qty As String, ByVal room2Qty As String,
                                         ByVal room3Qty As String, ByVal room4Qty As String,
                                         ByVal promoID As Integer, ByVal resellerID As String, Mode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", Mode)
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("rateCode", rateCode)
                .Parameters.AddWithValue("arrivalDate", arrivalDate)
                .Parameters.AddWithValue("departureDate", departureDate)
                .Parameters.AddWithValue("rooms", rooms)
                .Parameters.AddWithValue("room1Qty", room1Qty)
                .Parameters.AddWithValue("room2Qty", room2Qty)
                .Parameters.AddWithValue("room3Qty", room3Qty)
                .Parameters.AddWithValue("room4Qty", room4Qty)
                .Parameters.AddWithValue("promoID", promoID)
                .Parameters.AddWithValue("resellerID", resellerID)

            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try
    End Function


    Friend Function InsertSessionRate(con As SqlConnection, ByVal SessionID As System.Guid, ByVal rateCode As String,
                                         ByVal arrivalDate As Date, ByVal departureDate As Date, ByVal rooms As Integer,
                                         ByVal room1Qty As String, ByVal room2Qty As String,
                                         ByVal room3Qty As String, ByVal room4Qty As String,
                                         ByVal promoID As Integer, ByVal resellerID As String, companyCode As String, lang As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "Insert_SessionData"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("rateCode", rateCode)
                .Parameters.AddWithValue("arrivalDate", arrivalDate)
                .Parameters.AddWithValue("departureDate", departureDate)
                .Parameters.AddWithValue("rooms", rooms)
                .Parameters.AddWithValue("room1Qty", room1Qty)
                .Parameters.AddWithValue("room2Qty", room2Qty)
                .Parameters.AddWithValue("room3Qty", room3Qty)
                .Parameters.AddWithValue("room4Qty", room4Qty)
                .Parameters.AddWithValue("promoID", promoID)
                .Parameters.AddWithValue("resellerID", resellerID)
                .Parameters.AddWithValue("companyCode", companyCode)
                .Parameters.AddWithValue("lang", lang)


            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try
    End Function


    Friend Function UpdateSessionService(con As SqlConnection, ByVal SessionID As System.Guid,
                                     ByVal arrivalDate As Date, ByVal services As Integer,
                                     ByVal adults As Integer, ByVal children As Integer,
                                     ByVal infants As Integer, ByVal students As Integer,
                                     ByVal promoID As Integer, ByVal resellerID As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spSessionDataService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "UP1")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("arrivalDate", arrivalDate)
                .Parameters.AddWithValue("services", services)
                .Parameters.AddWithValue("adults", adults)
                .Parameters.AddWithValue("children", children)
                .Parameters.AddWithValue("infants", infants)
                .Parameters.AddWithValue("students", students)
                .Parameters.AddWithValue("promoID", promoID)
                .Parameters.AddWithValue("resellerID", resellerID)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try

    End Function

    Friend Function UpdateSessionRooms(con As SqlConnection, ByVal SessionID As String, ByVal RateCode As String, ByVal Room1Code As String, ByVal Room2Code As String, ByVal Room3Code As String, ByVal Room4Code As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spSessionDataRooms"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "UPD")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("rateCode", RateCode)
                .Parameters.AddWithValue("room1Code", Room1Code)
                .Parameters.AddWithValue("room2Code", Room2Code)
                .Parameters.AddWithValue("room3Code", Room3Code)
                .Parameters.AddWithValue("room4Code", Room4Code)

            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try
    End Function

    Friend Function UpdatePaypalToken(ByVal con As SqlConnection, ByVal SessionID As String, ByVal PayPalToken As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "UPP")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("PayPalToken", PayPalToken)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try
    End Function

    Friend Function UpdateAddons(ByVal con As SqlConnection, ByVal SessionID As String, ByVal Addons As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "UPA")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("addons", Addons)

            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try

    End Function

    Friend Function UpdateStatus(ByVal con As SqlConnection, ByVal company_Code As String, ByVal Active As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .CommandText = "spUpdateCompanyState"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", company_Code)
                .Parameters.AddWithValue("@active", Active)
            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try
    End Function


    Friend Function CheckSessionID(ByVal con As SqlConnection, ByVal SessionID As String) As Boolean
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Boolean
        Dim ihaysession As Integer
        Try

            With cmd
                .Connection = con
                .CommandText = "spCheckSessionID"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@SessionID", SessionID)
            End With


            ihaysession = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            If ihaysession > 0 Then
                response = True
            End If

            Return response

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function CheckSessionServiceID(ByVal con As SqlConnection, ByVal SessionID As String) As Boolean
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Boolean
        Dim ihaysession As Integer
        Try

            With cmd
                .Connection = con
                .CommandText = "spCheckSessionServiceID"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@SessionID", SessionID)
            End With


            ihaysession = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            If ihaysession > 0 Then
                response = True
            End If

            Return response

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanySessionByReservationID(ByVal con As SqlConnection, ByVal ReservationID As String) As List(Of SessionInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim SessionInfoList As New List(Of SessionInfo)()
        Dim sObjSessionInfo As New SessionInfo

        Try
            With cmd
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SE3")
                .Parameters.AddWithValue("reservationID", ReservationID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSessionInfo = New SessionInfo

                With sObjSessionInfo

                    With sObjSessionInfo
                        .ppSessionID = reader.Item("sessionID").ToString
                        .ppArrivalDate = reader.Item("arrivalDate")
                        .ppDepartureDate = reader.Item("departureDate")
                        .ppCompanyCode = reader.Item("companyCode")
                        .ppRateCode = reader.Item("rateCode")
                        .ppResellerID = reader.Item("resellerID")
                        .ppRooms = reader.Item("rooms")
                        .ppReservationID = reader.Item("reservationID")
                        .ppPayPalToken = reader.Item("PayPalToken")
                        .ppAddons = reader.Item("addons")
                        .ppLanguage = reader.Item("language")
                        .ppKeyID = reader.Item("KeyID")
                    End With

                    SessionInfoList.Add(sObjSessionInfo)
                End With

            End While

            reader.Close()

            Return SessionInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanySessionByPPToken(ByVal con As SqlConnection, ByVal PayPalToken As String) As List(Of SessionInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim SessionInfoList As New List(Of SessionInfo)()
        Dim sObjSessionInfo As New SessionInfo

        Try
            With cmd
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SE2")
                .Parameters.AddWithValue("PayPalToken", PayPalToken)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSessionInfo = New SessionInfo

                With sObjSessionInfo

                    With sObjSessionInfo
                        .ppSessionID = reader.Item("sessionID").ToString
                        .ppArrivalDate = reader.Item("arrivalDate")
                        .ppDepartureDate = reader.Item("departureDate")
                        .ppCompanyCode = reader.Item("companyCode")
                        .ppRateCode = reader.Item("rateCode")
                        .ppResellerID = reader.Item("resellerID")
                        .ppRooms = reader.Item("rooms")
                        .ppReservationID = reader.Item("reservationID")
                        .ppPayPalToken = reader.Item("PayPalToken")
                        .ppAddons = reader.Item("addons")
                        .ppLanguage = reader.Item("language")
                    End With

                    SessionInfoList.Add(sObjSessionInfo)
                End With

            End While

            reader.Close()

            Return SessionInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanySession(ByVal con As SqlConnection, ByVal SessionID As String) As List(Of SessionInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim SessionInfoList As New List(Of SessionInfo)()
        Dim sObjSessionInfo As New SessionInfo

        Try
            With cmd
                .CommandText = "spSessionData"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SE1")
                .Parameters.AddWithValue("SessionID", SessionID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSessionInfo = New SessionInfo

                With sObjSessionInfo

                    With sObjSessionInfo
                        .ppArrivalDate = reader.Item("arrivalDate")
                        .ppDepartureDate = reader.Item("departureDate")
                        .ppCompanyCode = reader.Item("companyCode")
                        .ppPromoID = reader.Item("promoID")
                        .ppRateCode = reader.Item("rateCode")
                        .ppResellerID = reader.Item("resellerID")
                        .ppRoom1Code = reader.Item("room1Code")
                        .ppRoom1Qty = reader.Item("room1Qty")
                        .ppRoom2Code = reader.Item("room2Code")
                        .ppRoom2Qty = reader.Item("room2Qty")
                        .ppRoom3Code = reader.Item("room3Code")
                        .ppRoom3Qty = reader.Item("room3Qty")
                        .ppRoom4Code = reader.Item("room4Code")
                        .ppRoom4Qty = reader.Item("room4Qty")
                        .ppRooms = reader.Item("rooms")
                        .ppReservationID = reader.Item("reservationID")
                        .ppPayPalToken = reader.Item("PayPalToken")
                        .ppAddons = reader.Item("addons")
                        .ppLanguage = reader.Item("language")
                    End With

                    SessionInfoList.Add(sObjSessionInfo)
                End With

            End While

            reader.Close()

            Return SessionInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanySessionService(ByVal con As SqlConnection, ByVal Mode As String, ByVal SessionID As String, ByVal SaleID As String) As List(Of SessionServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim SessionServiceInfoList As New List(Of SessionServiceInfo)()
        Dim sObjSessionServiceInfo As New SessionServiceInfo

        Try
            With cmd
                .CommandText = "spSessionDataService"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", Mode)
                If SessionID <> "" Then .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("saleID", SaleID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSessionServiceInfo = New SessionServiceInfo

                With sObjSessionServiceInfo

                    With sObjSessionServiceInfo
                        .ppCompanyCode = reader.Item("companyCode")
                        '.ppPromoID = reader.Item("promoID")
                        '.ppResellerID = reader.Item("resellerID")
                        .ppSaleID = reader.Item("saleID")
                        '.ppPayPalToken = reader.Item("PayPalToken")
                        .ppLanguage = reader.Item("language")
                        If Mode = "SE3" Then
                            .ppKeyID = reader.Item("KeyID")
                            .ppSessionID = reader.Item("SessionID").ToString()
                        End If
                    End With

                    SessionServiceInfoList.Add(sObjSessionServiceInfo)
                End With

            End While

            reader.Close()

            Return SessionServiceInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveCompanySessionService(ByVal con As SqlConnection, ByVal SessionID As System.Guid, ByVal CompanyCode As String, ByVal Language As String) As Response


        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spSessionDataService"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "INS")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("companyCode", CompanyCode)
                .Parameters.AddWithValue("lang", Language)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function InsertSessionServiceSC(ByVal con As SqlConnection, ByVal SessionID As String, ByVal serviceID As Integer,
                             ByVal arrivalDate As Date, ByVal ScheduleID As Integer,
                             ByVal hotelPickupID As Integer, ByVal hotelPickupHourID As Integer,
                             ByVal adults As Integer, ByVal children As Integer,
                             ByVal infants As Integer, ByVal students As Integer,
                             ByVal priceAdults As Double, ByVal priceChildren As Double,
                             ByVal priceInfants As Double, ByVal priceStudents As Double) As Response


        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spSessionDataServiceSC"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "INS")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("ServiceID", serviceID)
                .Parameters.AddWithValue("ScheduleID", ScheduleID)
                .Parameters.AddWithValue("ArrivalDate", arrivalDate)
                .Parameters.AddWithValue("HotelPickupID", hotelPickupID)
                .Parameters.AddWithValue("HotelPickupHourID", hotelPickupHourID)
                .Parameters.AddWithValue("QtyAdults", adults)
                .Parameters.AddWithValue("QtyChildren", children)
                .Parameters.AddWithValue("QtyInfants", infants)
                .Parameters.AddWithValue("QtyStudents", students)
                .Parameters.AddWithValue("PriceAdults", priceAdults)
                .Parameters.AddWithValue("PriceChildren", priceChildren)
                .Parameters.AddWithValue("PriceInfants", priceInfants)
                .Parameters.AddWithValue("PriceStudents", priceStudents)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetSessionServiceSC(ByVal con As SqlConnection, ByVal SessionID As String, ByVal Culture As String) As List(Of ServiceSCInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim ServiceSCInfoList As New List(Of ServiceSCInfo)()
            Dim sObjServiceSCInfo As New ServiceSCInfo

            With cmd
                .CommandText = "spSessionDataServiceSC"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SE2")
                .Parameters.AddWithValue("SessionID", SessionID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                sObjServiceSCInfo = New ServiceSCInfo
                With sObjServiceSCInfo
                    .ppRowID = reader.Item("RowID")
                    .ppServiceID = reader.Item("ServiceID")
                    .ppDescription = reader.Item("Description")
                    .ppArrivalDate = reader.Item("ArrivalDate")
                    .ppCheckIn = reader.Item("CheckIn")
                    .ppQtyAdults = reader.Item("QtyAdults")
                    .ppQtyChildren = reader.Item("QtyChildren")
                    .ppQtyInfants = reader.Item("QtyInfants")
                    .ppQtyStudents = reader.Item("QtyStudents")
                    .ppAdults = reader.Item("PriceAdults")
                    .ppChildren = reader.Item("PriceChildren")
                    .ppInfants = reader.Item("PriceInfants")
                    .ppStudents = reader.Item("PriceStudents")
                    .ppHotel = reader.Item("Hotel")
                    .ppHotelPickup = reader.Item("HotelPickup")
                    .ppTotal = reader.Item("Total")
                End With
                ServiceSCInfoList.Add(sObjServiceSCInfo)
            End While

            Return ServiceSCInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRowSC(ByVal con As SqlConnection, ByVal RowID As Long, ByVal SessionID As String) As Response


        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spSessionDataServiceSC"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("Mode", "DEL")
                .Parameters.AddWithValue("RowID", RowID)
                .Parameters.AddWithValue("SessionID", SessionID)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

End Class
