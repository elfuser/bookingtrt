﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Service
#Region "Public Methods"
    Public Function GetServiceSchedAvailability(ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal QtyPeople As Int16, ByVal PickupHour As String) As List(Of ServiceAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceAvailabilityList As New List(Of ServiceAvailability)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceAvailabilityList = GetServiceSchedAvailability(con, ServiceID, ArrivalDate, QtyPeople, PickupHour)
            End Using
            Return ServiceAvailabilityList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceSchedInventory(ByVal ScheduleID As Integer) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Dim result As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = GetServiceSchedInventory(con, ScheduleID)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetDayAvailability(ByVal serviceID As Integer, ByVal startDate As Date, ByVal endDate As Date) As List(Of DayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim DayAvailabilityList As New List(Of DayAvailability)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                DayAvailabilityList = GetDayAvailability(con, serviceID, startDate, endDate)
            End Using
            Return DayAvailabilityList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceByID(ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal Culture As String) As List(Of ServiceInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServiceByID(con, ServiceID, ArrivalDate, Culture)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceByID(ByVal ServiceID As Integer, ByVal Culture As String) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServiceByID(con, ServiceID, Culture)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceConditions(ByVal ServiceID As Integer, ByVal Culture As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = GetServiceConditions(con, ServiceID, Culture)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceWhatToBring(ByVal ServiceID As Integer, ByVal Culture As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = GetServiceWhatToBring(con, ServiceID, Culture)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = UpdateStatus(con, ResNumber, Status)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function LoadWaiver(ByVal CompanyCode As String, ByVal CultureID As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = LoadWaiver(con, CompanyCode, CultureID)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function LoadTerms(ByVal CompanyCode As String, ByVal CultureID As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = LoadTerms(con, CompanyCode, CultureID)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServicePickup(ByVal CompanyCode As String) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServicePickup(con, CompanyCode)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetServicePickupHours(ByVal PlaceID As Integer) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServicePickupHours(con, PlaceID)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetServiceAvailability(ByVal sCompanyCode As String, ByVal CategoryID As Integer, ByVal Culture As String, ByVal ArrivalDate As Date) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServiceAvailability(con, sCompanyCode, CategoryID, Culture, ArrivalDate)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServices(ByVal iServiceID As Integer, ByVal sCompanyCode As String, ByVal Culture As String, ByVal Mode As String) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServices(con, iServiceID, sCompanyCode, Culture, Mode)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServicesList(ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServicesList(con, sCompanyCode, sCulture)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceLocale(ByVal iServiceID As Integer, ByVal iCultureID As Integer) As List(Of ServiceLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInfoList As New List(Of ServiceLocaleInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInfoList = GetServiceLocale(con, iServiceID, iCultureID)
            End Using
            Return ServiceInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceCondInfo(ByVal iServiceID As Integer, ByVal iCultureID As Integer, ByVal sType As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetServiceCondInfo(con, iServiceID, iCultureID, sType)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceLocale(ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response


            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveServiceLocale(con, ServiceID, CultureID, Description, LongDescription, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveService(ByVal CompanyCode As String, ByVal ServiceID As Integer, ByVal Description As String, ByVal Details As String,
                                ByVal CategoryID As Integer, ByVal sMode As String, ByVal Code As String,
                                ByVal ListOrder As Integer, ByVal VideoURL As String, ByVal Culture As String, ByVal Difficulty As Integer,
                                ByVal Inventory As Boolean, ByVal Active As Boolean) As String

        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveService(con, CompanyCode, ServiceID, Description, Details,
                                 CategoryID, sMode, Code,
                                 ListOrder, VideoURL, Culture, Difficulty,
                                 Inventory, Active)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Public Function SavePickupService(ByVal sMode As String,
                                  ByVal CompanyCode As String, ByVal PlaceID As Integer,
                                  ByVal Name As String, ByVal Active As Boolean) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SavePickupService(con, sMode,
                                   CompanyCode, PlaceID,
                                   Name, Active)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceConditions(ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Conditions As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveServiceConditions(con, ServiceID, CultureID, Conditions)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServicePicture(ByVal ServiceID As Long, ByVal numPhoto As Integer, ByVal Picture As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveServicePicture(con, ServiceID, numPhoto, Picture)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceBringInfo(ByVal ServiceID As Integer, ByVal Culture As Integer, ByVal BringInfo As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveServiceBringInfo(con, ServiceID, Culture, BringInfo)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteService(ByVal ServiceID As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteService(con, ServiceID)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCategories(ByVal Culture As String) As List(Of CategoryInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CategoryInfoList As New List(Of CategoryInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CategoryInfoList = GetCategories(con, Culture)
            End Using
            Return CategoryInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceRates(ByVal iRateID As Integer, ByVal iServiceID As Integer) As List(Of ServiceRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceRateInfoList As New List(Of ServiceRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceRateInfoList = GetServiceRates(con, iRateID, iServiceID)
            End Using
            Return ServiceRateInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServiceInventory(ByVal iServiceID As Integer) As List(Of ServiceInventoryInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceInventoryInfoList As New List(Of ServiceInventoryInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceInventoryInfoList = GetServiceInventory(con, iServiceID)
            End Using
            Return ServiceInventoryInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceRate(ByVal sMode As String, ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal OldStartDate As Date, ByVal OldEndDate As Date, ByVal Adults As Double, ByVal Children As Double,
                                    ByVal Infant As Double, ByVal Students As Double, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer, ByVal MaxInfant As Integer,
                                    ByVal MaxStudent As Integer, ByVal Active As Boolean) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveServiceRate(con, sMode, RateID, ServiceID, Description,
                                     StartDate, EndDate, OldStartDate, OldEndDate, Adults, Children,
                                     Infant, Students, MaxAdult, MaxChildren, MaxInfant,
                                     MaxStudent, Active)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveServiceInventory(ByVal sMode As String, ByVal ScheduleID As Integer, ByVal ServiceID As Integer, ByVal Hour As String,
                                    ByVal Qty As Integer) As Response

        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = SaveServiceInventory(con, sMode, ScheduleID, ServiceID, Hour, Qty)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Public Function DeleteServiceRate(ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteServiceRate(con, RateID, ServiceID, StartDate, EndDate)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteServiceInventory(ByVal ServiceID As Integer, ByVal ScheduleID As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    response = DeleteServiceInventory(con, ServiceID, ScheduleID)
                End Using
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServicesPagingCount(CompanyCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetServicesPagingCount(con, CompanyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServicesPaging(CompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of ServiceInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim ServiceList As New List(Of ServiceInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                ServiceList = GetServicesPaging(con, CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return ServiceList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function GetServiceAvailability(ByVal con As SqlConnection, ByVal sCompanyCode As String, ByVal CategoryID As Integer, ByVal Culture As String, ByVal ArrivalDate As Date) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetAvailabilityService_v2"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("culture", Culture)
                .Parameters.AddWithValue("categoryID", CategoryID)
                .Parameters.AddWithValue("date", ArrivalDate)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppServiceID = reader.Item("ServiceID")
                    .ppVideoURL = reader.Item("VideoURL")
                    .ppDifficulty = reader.Item("Difficulty")
                    .ppDescription = reader.Item("Description")
                    .ppDetails = reader.Item("Details")
                    .ppAdults = reader.Item("Adults")
                    .ppChildren = reader.Item("Children")
                    .ppInfants = reader.Item("Infant")
                    .ppPicture1 = reader.Item("Picture1")
                    '.ppQConditions = reader.Item("Conditions")
                    '.ppQBringInfo = reader.Item("BringInfo")
                    .ppControlInventory = reader.Item("ControlInventory")
                    .ppRateId = reader.Item("RateID")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServiceSchedAvailability(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal QtyPeople As Int16, ByVal PickupHour As String) As List(Of ServiceAvailability)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceAvailabilityList As New List(Of ServiceAvailability)()
        Dim sObjServiceAvailability As New ServiceAvailability

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetServiceSchedAvailableAndInventory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("ServiceID", ServiceID)
                .Parameters.AddWithValue("Date", ArrivalDate)
                .Parameters.AddWithValue("QtyPeople", QtyPeople)
                .Parameters.AddWithValue("PickupHour", PickupHour)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceAvailability = New ServiceAvailability
                With sObjServiceAvailability
                    .ppScheduleID = reader.Item("ScheduleID")
                    .ppHour = reader.Item("Hour")
                    .ppQty = reader.Item("Qty")
                    .ppAvailable = reader.Item("Available")
                End With
                ServiceAvailabilityList.Add(sObjServiceAvailability)
            End While

            reader.Close()
            Return ServiceAvailabilityList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetServiceSchedInventory(ByVal con As SqlConnection, ByVal ScheduleID As Integer) As Integer
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim iResult As Integer

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetServiceSchedInventory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("ScheduleID", ScheduleID)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                reader.Read()
                iResult = reader.Item("Qty")
            End While
            reader.Close()

            Return iResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetDayAvailability(ByVal con As SqlConnection, ByVal serviceID As Integer, ByVal startDate As Date, ByVal endDate As Date) As List(Of DayAvailability)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim DayAvailabilityList As New List(Of DayAvailability)()
        Dim sObjDayAvailability As New DayAvailability

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetServiceAvailabilityDay"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("serviceID", serviceID)
                .Parameters.AddWithValue("startDate", startDate)
                .Parameters.AddWithValue("endDate", endDate)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjDayAvailability = New DayAvailability
                With sObjDayAvailability
                    .ppDay = reader.Item("Day")
                    .ppQty = reader.Item("Qty")
                End With
                DayAvailabilityList.Add(sObjDayAvailability)
            End While

            reader.Close()
            Return DayAvailabilityList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetServiceByID(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal Culture As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "SE1")
                .Parameters.AddWithValue("serviceID", ServiceID)
                .Parameters.AddWithValue("date", ArrivalDate)
                .Parameters.AddWithValue("culture", Culture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppDescription = reader.Item("Description")
                    .ppAdults = reader.Item("Adults")
                    .ppChildren = reader.Item("Children")
                    .ppInfants = reader.Item("Infant")
                    .ppControlInventory = reader.Item("ControlInventory")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetServiceByID(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal Culture As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "SE3")
                .Parameters.AddWithValue("serviceID", ServiceID)
                .Parameters.AddWithValue("culture", Culture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppRateId = reader.Item("RateId")
                    .ppServiceID = reader.Item("ServiceID")
                    .ppDescription = reader.Item("Description")
                    .ppDetails = reader.Item("Details")
                    .ppBringInfo = reader.Item("BringInfo")
                    .ppConditions = reader.Item("Conditions")
                    .ppPicture1 = reader.Item("Picture1")
                    .ppPicture2 = reader.Item("Picture2")
                    .ppPicture3 = reader.Item("Picture3")
                    .ppAdults = reader.Item("Adults")
                    .ppChildren = reader.Item("Children")
                    .ppInfants = reader.Item("Infant")
                    .ppStudent = reader.Item("Student")
                    .ppMaxadult = reader.Item("MaxAdult")
                    .ppMaxchildren = reader.Item("MaxChildren")
                    .ppMaxinfant = reader.Item("MaxInfant")
                    .ppMaxstudent = reader.Item("MaxStudent")
                    .ppRatestartdate = reader.Item("StartDate")
                    .ppRateenddate = reader.Item("EndDate")
                    .ppControlInventory = reader.Item("ControlInventory")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServiceasdsad(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal ArrivalDate As Date, ByVal Culture As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "SE1")
                .Parameters.AddWithValue("serviceID", ServiceID)
                .Parameters.AddWithValue("date", ArrivalDate)
                .Parameters.AddWithValue("culture", Culture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppDescription = reader.Item("Description")
                    .ppAdults = reader.Item("Adults")
                    .ppChildren = reader.Item("Children")
                    .ppInfants = reader.Item("Infant")
                    .ppControlInventory = reader.Item("ControlInventory")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetServiceConditions(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal Culture As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "SE3")
                .Parameters.AddWithValue("ServiceID", ServiceID)
                .Parameters.AddWithValue("Culture_id", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("Conditions")
                reader.Close()
            Else
                sResult = "There are no conditions for this service"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServiceWhatToBring(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal Culture As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "SE3")
                .Parameters.AddWithValue("ServiceID", ServiceID)
                .Parameters.AddWithValue("Culture_id", Culture)
            End With


            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("BringInfo")
                reader.Close()
            Else
                sResult = "There is no bring information for this service"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function UpdateStatus(ByVal con As SqlConnection, ByVal ResNumber As String, ByVal Status As Int16) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spUpdateServiceStatus"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("sale_id", ResNumber)
                .Parameters.AddWithValue("status", Status)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function LoadWaiver(ByVal con As SqlConnection, ByVal CompanyCode As String, ByVal CultureID As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Try
            With cmd
                .Connection = con
                .CommandText = "spCompanyTerm"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "SEL")
                .Parameters.AddWithValue("company_code", CompanyCode)
                .Parameters.AddWithValue("culture", CultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                reader.Read()
                sResult = reader.Item("waiver")
                reader.Close()
            Else
                sResult = "There is no waiver"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function LoadTerms(ByVal con As SqlConnection, ByVal CompanyCode As String, ByVal CultureID As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Try
            With cmd
                .Connection = con
                .CommandText = "spCompanyTerm"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", "SEL")
                .Parameters.AddWithValue("company_code", CompanyCode)
                .Parameters.AddWithValue("culture", CultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                reader.Read()
                sResult = reader.Item("terms")
                reader.Close()
            Else
                sResult = "There are no terms and conditions"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetServicePickup(ByVal con As SqlConnection, ByVal CompanyCode As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .CommandText = "spGetServicePickup"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("CompanyCode", CompanyCode)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo

                With sObjServiceInfo
                    .ppServiceID = reader.Item("PlaceID")
                    .ppDescription = reader.Item("Name")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()

            Return ServiceInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetServicePickupHours(ByVal con As SqlConnection, ByVal PlaceID As Integer) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .CommandText = "spGetServicePickupHours"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("PlaceID", PlaceID)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo

                With sObjServiceInfo
                    .ppServiceID = reader.Item("ScheduleID")
                    .ppDescription = reader.Item("Hour")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()

            Return ServiceInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetServices(ByVal con As SqlConnection, ByVal iServiceID As Integer, ByVal sCompanyCode As String, ByVal Culture As String, ByVal Mode As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", Mode)
                .Parameters.AddWithValue("@serviceID", iServiceID)
                .Parameters.AddWithValue("@CompanyCode", sCompanyCode)
                .Parameters.AddWithValue("@Culture", Culture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    If Mode = "CBO" Or Mode = "SE4" Then
                        .ppServiceID = reader.Item("ServiceID")
                        .ppDescription = reader.Item("Description")
                    Else
                        .ppServiceID = reader.Item("ServiceID")
                        .ppCompanyCode = reader.Item("CompanyCode")
                        .ppDescription = reader.Item("Description")
                        .ppDetails = reader.Item("Details")
                        .ppCode = reader.Item("Code")
                        .ppListOrder = reader.Item("ListOrder")
                        .ppVideoURL = reader.Item("VideoURL")
                        .ppDifficulty = reader.Item("Difficulty")
                        .ppPicture1 = reader.Item("Picture1")
                        .ppPicture2 = reader.Item("Picture2")
                        .ppPicture3 = reader.Item("Picture3")
                        .ppActive = reader.Item("Active")
                        .ppCategoryID = reader.Item("CategoryID")
                        .ppControlInventory = reader.Item("ControlInventory")
                    End If
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetServicesList(ByVal con As SqlConnection, ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SE2")
                .Parameters.AddWithValue("@CompanyCode", sCompanyCode)
                .Parameters.AddWithValue("@Culture", sCulture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppServiceID = reader.Item("ServiceID")
                    .ppDescription = reader.Item("Description")
                    .ppCode = reader.Item("Code")
                    .ppListOrder = reader.Item("ListOrder")
                    .ppActive = reader.Item("Active")
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServiceLocale(ByVal con As SqlConnection, ByVal iServiceID As Integer, ByVal iCultureID As Integer) As List(Of ServiceLocaleInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceLocaleInfoList As New List(Of ServiceLocaleInfo)()
        Dim sObjServiceLocaleInfo As New ServiceLocaleInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceLocale"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@ServiceID", iServiceID)
                .Parameters.AddWithValue("@CultureID", iCultureID)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceLocaleInfo = New ServiceLocaleInfo
                With sObjServiceLocaleInfo
                    .ppServiceID = reader.Item("ServiceID")
                    .ppCultureID = reader.Item("CultureID")
                    .ppDescription = reader.Item("Description")
                    .ppLongDescription = reader.Item("Details")
                End With
                ServiceLocaleInfoList.Add(sObjServiceLocaleInfo)
            End While

            reader.Close()
            Return ServiceLocaleInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function GetServiceCondInfo(ByVal con As SqlConnection, ByVal iServiceID As Integer, ByVal iCultureID As Integer, ByVal sType As String) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SE3")
                .Parameters.AddWithValue("@ServiceID", iServiceID)
                .Parameters.AddWithValue("@Culture_id", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                If sType = "Conditions" Then
                    sResult = reader.Item("Conditions")
                Else
                    sResult = reader.Item("BringInfo")
                End If
            Else
                sResult = "There are no conditions for this service"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function SaveServiceLocale(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response
        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceLocale"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@CultureID", CultureID)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@Details", LongDescription)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function SaveService(ByVal con As SqlConnection, ByVal CompanyCode As String, ByVal ServiceID As Integer, ByVal Description As String, ByVal Details As String,
                                ByVal CategoryID As Integer, ByVal sMode As String, ByVal Code As String,
                                ByVal ListOrder As Integer, ByVal VideoURL As String, ByVal Culture As String, ByVal Difficulty As Integer,
                                ByVal Inventory As Boolean, ByVal Active As Boolean) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Dim iRow As Integer


        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@CompanyCode", CompanyCode)
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@CategoryID", CategoryID)
                .Parameters.AddWithValue("@Code", Code)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@Details", Details)
                .Parameters.AddWithValue("@ListOrder", ListOrder)
                .Parameters.AddWithValue("@VideoURL", VideoURL)
                .Parameters.AddWithValue("@Difficulty", Difficulty + 1)
                .Parameters.AddWithValue("@Active", Active)
                .Parameters.AddWithValue("@Culture", Culture)
                .Parameters.AddWithValue("@Inventory", Inventory)

            End With


            If sMode = "INS" Then
                iRow = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
                sResult = "ID " & iRow.ToString
            Else
                dataHelper.ExecuteNonQuery(cmd, con)
                sResult = "UP"
            End If


            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function SavePickupService(ByVal con As SqlConnection, ByVal sMode As String,
                                  ByVal CompanyCode As String, ByVal PlaceID As Integer,
                                  ByVal Name As String, ByVal Active As Boolean) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String
        Dim iRow As Integer


        Try
            With cmd
                .Connection = con
                .CommandText = "spServicePickup"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("Mode", sMode)
                .Parameters.AddWithValue("PlaceID", PlaceID)
                .Parameters.AddWithValue("CompanyCode", CompanyCode)
                .Parameters.AddWithValue("Name", Name)
                .Parameters.AddWithValue("Active", Active)

            End With
            If sMode = "INS" Then
                iRow = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
                sResult = "ID " & iRow.ToString
            Else
                dataHelper.ExecuteNonQuery(cmd, con)
                sResult = "UP"
            End If


            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveServiceConditions(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal CultureID As Integer, ByVal Conditions As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "CON")
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@Culture_id", CultureID)
                .Parameters.AddWithValue("@Conditions", Conditions)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveServicePicture(con As SqlConnection, ByVal ServiceID As Long, ByVal numPhoto As Integer, ByVal Picture As Byte()) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spServicePhoto"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@serviceID", ServiceID)
                .Parameters.AddWithValue("@photoNumber", numPhoto)
                .Parameters.AddWithValue("@photo", Picture)
                .Parameters.AddWithValue("@mode", "INS")

            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveServiceBringInfo(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal Culture As Integer, ByVal BringInfo As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "BI")
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@Culture_id", Culture)
                .Parameters.AddWithValue("@BringInfo", BringInfo)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function DeleteService(ByVal con As SqlConnection, ByVal ServiceID As Integer) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = String.Empty

        Dim iCant As Integer

        Try
            With cmd
                .Connection = con
                .CommandText = "spService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Mode", "DEL")
                .Parameters.AddWithValue("@ServiceID", ServiceID)

            End With
            iCant = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            If iCant > 0 Then
                sResult = "Error: The service can not be deleted, it has sales associated"
            End If

            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCategories(ByVal con As SqlConnection, ByVal Culture As String) As List(Of CategoryInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim CategoryInfoList As New List(Of CategoryInfo)()
        Dim sObjCategoryInfo As New CategoryInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spCategory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@Culture", Culture)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjCategoryInfo = New CategoryInfo
                With sObjCategoryInfo

                    .ppCategoryID = reader.Item("CategoryID")
                    .ppDescription = reader.Item("Description")

                End With
                CategoryInfoList.Add(sObjCategoryInfo)
            End While

            reader.Close()
            Return CategoryInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServiceRates(ByVal con As SqlConnection, ByVal iRateID As Integer, ByVal iServiceID As Integer) As List(Of ServiceRateInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceRateInfoList As New List(Of ServiceRateInfo)()
        Dim sObjServiceRateInfo As New ServiceRateInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceRate"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@RateID", iRateID)
                .Parameters.AddWithValue("@ServiceID", iServiceID)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceRateInfo = New ServiceRateInfo
                With sObjServiceRateInfo
                    .ppRateID = Val(reader.Item("RateID") & "")
                    .ppDescription = reader.Item("Description") & ""

                    If IsDate(reader.Item("StartDate")) Then
                        .ppStartDate = CDate(reader.Item("StartDate"))
                        .UnFormatFrom = .ppStartDate.ToShortDateString()
                    End If

                    If IsDate(reader.Item("EndDate")) Then
                        .ppEndDate = reader.Item("EndDate")
                        .UnFormatTo = .ppEndDate.ToShortDateString()
                    End If

                    .ppAdults = CDbl(reader.Item("Adults") & "")
                    .ppChildren = CDbl(reader.Item("Children") & "")
                    .ppInfants = CDbl(reader.Item("Infant") & "")
                    .ppStudents = CDbl(reader.Item("Student") & "")
                    .ppMaxAdults = Val(reader.Item("MaxAdult") & "")
                    .ppMaxChildren = Val(reader.Item("MaxChildren") & "")
                    .ppMaxInfants = Val(reader.Item("MaxInfant") & "")
                    .ppMaxStudents = Val(reader.Item("MaxStudent") & "")
                    .ppActive = reader.Item("Active")
                End With
                ServiceRateInfoList.Add(sObjServiceRateInfo)
            End While

            reader.Close()
            Return ServiceRateInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function GetServiceInventory(ByVal con As SqlConnection, ByVal iServiceID As Integer) As List(Of ServiceInventoryInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInventoryInfoList As New List(Of ServiceInventoryInfo)()
        Dim sObjServiceInventoryInfo As New ServiceInventoryInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetServiceInventory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@ServiceID", iServiceID)
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInventoryInfo = New ServiceInventoryInfo
                With sObjServiceInventoryInfo
                    .ppScheduleID = reader.Item("ScheduleID")
                    '.ppServiceID = reader.Item("ServiceID")
                    .ppHour = reader.Item("Hour")
                    .ppQty = reader.Item("Qty")
                End With
                ServiceInventoryInfoList.Add(sObjServiceInventoryInfo)
            End While

            reader.Close()
            Return ServiceInventoryInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function SaveServiceRate(ByVal con As SqlConnection, ByVal sMode As String, ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal OldStartDate As Date, ByVal OldEndDate As Date, ByVal Adults As Double, ByVal Children As Double,
                                    ByVal Infant As Double, ByVal Students As Double, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer, ByVal MaxInfant As Integer,
                                    ByVal MaxStudent As Integer, ByVal Active As Boolean) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spServiceRate"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@RateID", RateID)
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@StartDate", StartDate)
                .Parameters.AddWithValue("@EndDate", EndDate)
                .Parameters.AddWithValue("@OldStartDate", OldStartDate)
                .Parameters.AddWithValue("@OldEndDate", OldEndDate)
                .Parameters.AddWithValue("@Adults", Adults)
                .Parameters.AddWithValue("@Children", Children)
                .Parameters.AddWithValue("@Infant", Infant)
                .Parameters.AddWithValue("@Student", Students)
                .Parameters.AddWithValue("@MaxAdult", MaxAdult)
                .Parameters.AddWithValue("@MaxChildren", MaxChildren)
                .Parameters.AddWithValue("@MaxInfant", MaxInfant)
                .Parameters.AddWithValue("@MaxStudent", MaxStudent)
                .Parameters.AddWithValue("@Active", Active)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveServiceInventory(ByVal con As SqlConnection, ByVal sMode As String, ByVal ScheduleID As Integer, ByVal ServiceID As Integer, ByVal Hour As String,
                                    ByVal Qty As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spServiceInventory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                If sMode = "UPD" Then .Parameters.AddWithValue("@ScheduleID", ScheduleID)
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@Hour", Hour)
                .Parameters.AddWithValue("@Qty", Qty)
            End With


            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteServiceRate(ByVal con As SqlConnection, ByVal RateID As Integer, ByVal ServiceID As Integer, ByVal StartDate As Date, ByVal EndDate As Date) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = String.Empty
        Dim iCant As Integer

        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceRate"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@RateID", RateID)
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@StartDate", StartDate)
                .Parameters.AddWithValue("@EndDate", EndDate)
            End With

            iCant = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))

            If iCant > 0 Then
                sResult = "Error: The rate can not be deleted"
            End If

            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteServiceInventory(ByVal con As SqlConnection, ByVal ServiceID As Integer, ByVal ScheduleID As Integer) As String
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = String.Empty
        Dim iCant As Integer

        Try
            With cmd
                .Connection = con
                .CommandText = "spServiceInventory"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@ServiceID", ServiceID)
                .Parameters.AddWithValue("@ScheduleID", ScheduleID)
            End With

            iCant = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))

            If iCant > 0 Then
                sResult = "Error: The schedule can not be deleted"
            End If

            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetServicesPaging(ByVal con As SqlConnection, ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of ServiceInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim ServiceInfoList As New List(Of ServiceInfo)()
        Dim sObjServiceInfo As New ServiceInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetServicesPaging"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@CompanyCode", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With
            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjServiceInfo = New ServiceInfo
                With sObjServiceInfo
                    .ppServiceID = Val(reader.Item("ServiceID") & "")
                    .ppDescription = reader.Item("Description") & ""
                    .ppCode = reader.Item("Code") & ""
                    .ppListOrder = Val(reader.Item("ListOrder") & "")
                    .ppActive = reader.Item("Active")
                    .ppDetails = reader.Item("Details") & ""
                    .ppVideoURL = reader.Item("VideoURL") & ""
                    .ppDifficulty = Val(reader.Item("Difficulty"))
                    .ppCategoryID = Val(reader.Item("CategoryID"))
                    .ppControlInventory = reader.Item("ControlInventory")
                    .ppBringInfo = reader.Item("BringInfo") & ""
                    .ppConditions = reader.Item("Conditions") & ""
                End With
                ServiceInfoList.Add(sObjServiceInfo)
            End While

            reader.Close()
            Return ServiceInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function

    Friend Function GetServicesPagingCount(con As SqlConnection, ByVal sCompanyCode As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetServicesPagingCount"
                .Parameters.AddWithValue("@CompanyCode", sCompanyCode)
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
