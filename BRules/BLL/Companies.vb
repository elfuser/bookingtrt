﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions
Public Class Companies

#Region "Public Methods"

    Public Function SaveCompany(ByVal CompanyCode As String, ByVal CompanyName As String, ByVal LegalName As String, ByVal LegalID As String, ByVal Address As String, ByVal Tel1 As String, ByVal Tel2 As String,
                                ByVal Tel3 As String, ByVal Fax As String, ByVal ContactName As String, ByVal ContactEmail As String, ByVal Web As String, ByVal Email As String,
                                ByVal CallTollFree As String, ByVal sMode As String, ByVal BankName As String, ByVal BankAcct As String,
                                ByVal CfoName As String, ByVal CfoEmail As String, ByVal ResName As String, ByVal ResEmail As String, ByVal ResEmailAlt As String,
                                ByVal FacebookURL As String, ByVal TwitterText As String, ByVal Hotel As Boolean, ByVal Services As Boolean,
                                ByVal GoogleID As String, ByVal GoogleConvID As String, ByVal GoogleLabel As String, ByVal Active As Boolean, ByVal UserID As Integer, ByVal Migrar As Boolean,
                                prov As String, canton As String, distrito As String, TechContact As String, TechEmail As String,
                                Pinterest As String, Instagram As String, Blog As String, GooglePlus As String,
                                ThingsToDo As String, GetThere As String, latitude As String, longitude As String) As Response

        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCompany(con, CompanyCode, CompanyName, LegalName, LegalID, Address, Tel1, Tel2,
                                 Tel3, Fax, ContactName, ContactEmail, Web, Email,
                                 CallTollFree, sMode, BankName, BankAcct,
                                 CfoName, CfoEmail, ResName, ResEmail, ResEmailAlt,
                                 FacebookURL, TwitterText, Hotel, Services,
                                 GoogleID, GoogleConvID, GoogleLabel, Active, UserID, Migrar, prov, canton, distrito,
                                 TechContact, TechEmail,
                                 Pinterest, Instagram, Blog, GooglePlus, ThingsToDo, GetThere, latitude, longitude)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanyHotel(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, _
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCompanyHotel(con, CompanyCode, sMode, TaxPerc, MaxAdults, MaxChildren,
                                     MsgAdults, MsgChildren, ChildrenEnabled, MsgChildrenFree, ChildrenNote, ConfirmPrefix)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanyService(ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal RentPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal MaxStudents As Integer, _
                                    ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal MsgStudents As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCompanyService(con, CompanyCode, sMode, TaxPerc, RentPerc, MaxAdults, MaxChildren, MaxStudents,
                                     MsgAdults, MsgChildren, MsgStudents, ChildrenEnabled, MsgChildrenFree, ChildrenNote, ConfirmPrefix)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanyTerms(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sTerms As String, ByVal iCultureID As Integer) As Integer
        Try
            Dim dataHelper As New DataHelper()
            Using Scope As TransactionScope = New TransactionScope()
                Using con As SqlConnection = dataHelper.OpenConnection()
                    iTermID = SaveCompanyTerms(con, CompanyCode, sMode, iTermID, sTerms, iCultureID)
                End Using
            End Using

            Return iTermID
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanyWaiver(ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sWaiver As String, ByVal iCultureID As Integer) As Integer
        Try

            Dim dataHelper As New DataHelper()
            Dim iID As Integer = 0


            Using Scope As TransactionScope = New TransactionScope()

                Using con As SqlConnection = dataHelper.OpenConnection()

                    iID = SaveCompanyWaiver(con, CompanyCode, sMode, iTermID, sWaiver, iCultureID)

                End Using

            End Using
            Return iID
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveCompanyProcessor(ByVal CompanyCode As String, ByVal sMode As String, ByVal Commission As Double, ByVal ProcessorID As String, _
                                         ByVal KeyID As String, ByVal KeyHash As String, ByVal Type As String, ByVal URL As String, ByVal Username As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCompanyProcessor(con, CompanyCode, sMode, Commission, ProcessorID,
                                          KeyID, KeyHash, Type, URL, Username)
            End Using


            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompany(ByVal sCompany_Code As String, ByVal sMode As String, ByVal sType As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CompanyList As New List(Of CompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CompanyList = GetCompany(con, sCompany_Code, sMode, sType)
            End Using
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyHotel(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CompanyList As New List(Of CompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CompanyList = GetCompanyHotel(con, sCompany_Code, sMode)
            End Using
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyService(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CompanyList As New List(Of CompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CompanyList = GetCompanyService(con, sCompany_Code, sMode)
            End Using
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyProcessor(ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CompanyList As New List(Of CompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CompanyList = GetCompanyProcessor(con, sCompany_Code, sMode)
            End Using
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteCompany(ByVal CompanyCode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteCompany(con, CompanyCode)
            End Using

            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyTerm(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sresult As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                sresult = GetCompanyTerm(con, sCompany_Code, sMode, iCulture)
            End Using
            Return sresult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyWaiver(ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = String.Empty

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyWaiver(con, sCompany_Code, sMode, iCulture)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyPrefix(ByVal sPrefix As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyPrefix(con, sPrefix)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyServicePrefix(ByVal sPrefix As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyServicePrefix(con, sPrefix)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetBankBNKeys(ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetBankBNKeys(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanyEmails(ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyEmails(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanyData(ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyData(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    Public Function GetCompanyGoogleInfo(ByVal sCompanyCode As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As List(Of CompanyInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyGoogleInfo(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyTermsEmails(ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As List(Of CompanyInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetCompanyTermsEmails(con, sCompanyCode, sCulture)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPayPalSubject(ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetPayPalSubject(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetBankCredomaticKeys(ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As String = ""

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetBankCredomaticKeys(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetFacilities(ByVal sCompanyCode As String) As List(Of clsFeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim sResult As List(Of clsFeaturesInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                sResult = GetFacilities(con, sCompanyCode)
            End Using
            Return sResult
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyPagingCount(PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetCompanyPagingCount(con, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCompanyPaging(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CompanyList As New List(Of CompanyInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CompanyList = GetCompanyPaging(con, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


#End Region

#Region "Private Methods"

    Friend Function SaveCompany(con As SqlConnection, ByVal CompanyCode As String, ByVal CompanyName As String, ByVal LegalName As String, ByVal LegalID As String, ByVal Address As String, ByVal Tel1 As String, ByVal Tel2 As String,
                                ByVal Tel3 As String, ByVal Fax As String, ByVal ContactName As String, ByVal ContactEmail As String, ByVal Web As String, ByVal Email As String,
                                ByVal CallTollFree As String, ByVal sMode As String, ByVal BankName As String, ByVal BankAcct As String,
                                ByVal CfoName As String, ByVal CfoEmail As String, ByVal ResName As String, ByVal ResEmail As String, ByVal ResEmailAlt As String,
                                ByVal FacebookURL As String, ByVal TwitterText As String, ByVal Hotel As Boolean, ByVal Services As Boolean,
                                ByVal GoogleID As String, ByVal GoogleConvID As String, ByVal GoogleLabel As String, ByVal Active As Boolean, ByVal UserID As Integer, ByVal Migrar As Boolean,
                                prov As String, canton As String, distrito As String, TechContact As String, TechEmail As String,
                                Pinterest As String, Instagram As String, Blog As String, GooglePlus As String,
                                ThingsToDo As String, GetThere As String, latitude As String, longitude As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@company_name", CompanyName)
                .Parameters.AddWithValue("@legal_name", LegalName)
                .Parameters.AddWithValue("@legail_id", LegalID)
                .Parameters.AddWithValue("@address", Address)
                .Parameters.AddWithValue("@telephone_nr1", Tel1)
                .Parameters.AddWithValue("@telephone_nr2", Tel2)
                .Parameters.AddWithValue("@telephone_nr3", Tel3)
                .Parameters.AddWithValue("@fax_nr", Fax)
                .Parameters.AddWithValue("@contact_name", ContactName)
                .Parameters.AddWithValue("@contact_email", ContactEmail)
                .Parameters.AddWithValue("@web", Web)
                .Parameters.AddWithValue("@email", Email)
                .Parameters.AddWithValue("@call_toll_free", CallTollFree)
                .Parameters.AddWithValue("@bank_account", BankAcct)
                .Parameters.AddWithValue("@bank_name", BankName)
                .Parameters.AddWithValue("@cfo_name", CfoName)
                .Parameters.AddWithValue("@cfo_email", CfoEmail)
                .Parameters.AddWithValue("@res_name", ResName)
                .Parameters.AddWithValue("@res_email", ResEmail)
                .Parameters.AddWithValue("@res_email_alt", ResEmailAlt)
                .Parameters.AddWithValue("@facebookURL", FacebookURL)
                .Parameters.AddWithValue("@twitterText", TwitterText)
                .Parameters.AddWithValue("@hotel", Hotel)
                .Parameters.AddWithValue("@services", Services)
                .Parameters.AddWithValue("@googleID", GoogleID)
                .Parameters.AddWithValue("@googleConvID", GoogleConvID)
                .Parameters.AddWithValue("@googleLabel", GoogleLabel)
                .Parameters.AddWithValue("@active", Active)
                .Parameters.AddWithValue("@userID", UserID)
                .Parameters.AddWithValue("@migrar", Migrar)
                .Parameters.AddWithValue("@provincia", prov)
                .Parameters.AddWithValue("@canton", canton)
                .Parameters.AddWithValue("@distrito", distrito)
                .Parameters.AddWithValue("@TechContact", TechContact)
                .Parameters.AddWithValue("@TechEmail", TechEmail)
                .Parameters.AddWithValue("@PinterestURL", Pinterest)
                .Parameters.AddWithValue("@InstagramURL", Instagram)
                .Parameters.AddWithValue("@BlogURL", Blog)
                .Parameters.AddWithValue("@GooglePlusURL", GooglePlus)
                .Parameters.AddWithValue("@ThingsToDo", ThingsToDo)
                .Parameters.AddWithValue("@GetThere", GetThere)
                .Parameters.AddWithValue("@latitude", latitude)
                .Parameters.AddWithValue("@longitude", longitude)

            End With
            Call dataHelper.ExecuteNonQuery(cmd, con)
            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch ex As SqlException
            response.Status = "-1"
            Throw New Exception(ex.Message)
        End Try


    End Function

    Friend Function SaveCompanyHotel(con As SqlConnection, ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, _
                                ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()

        Try
            With cmd
                .Connection = con
                .CommandText = "spCompanyHotel"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@tax_perc", TaxPerc)
                .Parameters.AddWithValue("@max_adults", MaxAdults)
                .Parameters.AddWithValue("@max_children", MaxChildren)
                .Parameters.AddWithValue("@msg_adults", MsgAdults)
                .Parameters.AddWithValue("@msg_children", MsgChildren)
                .Parameters.AddWithValue("@children_free", ChildrenEnabled)
                .Parameters.AddWithValue("@msg_children_free", MsgChildrenFree)
                .Parameters.AddWithValue("@children_note", ChildrenNote)
                .Parameters.AddWithValue("@confirm_prefix", ConfirmPrefix)
            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function SaveCompanyService(con As SqlConnection, ByVal CompanyCode As String, ByVal sMode As String, ByVal TaxPerc As Double, ByVal RentPerc As Double, ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal MaxStudents As Integer, _
                                ByVal MsgAdults As String, ByVal MsgChildren As String, ByVal MsgStudents As String, ByVal ChildrenEnabled As Boolean, ByVal MsgChildrenFree As String, ByVal ChildrenNote As String, ByVal ConfirmPrefix As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spCompanyService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@tax_perc", TaxPerc)
                .Parameters.AddWithValue("@rent_perc", RentPerc)
                .Parameters.AddWithValue("@max_adults", MaxAdults)
                .Parameters.AddWithValue("@max_children", MaxChildren)
                .Parameters.AddWithValue("@max_student", MaxStudents)
                .Parameters.AddWithValue("@msg_adults", MsgAdults)
                .Parameters.AddWithValue("@msg_children", MsgChildren)
                .Parameters.AddWithValue("@msg_student", MsgStudents)
                .Parameters.AddWithValue("@children_free", ChildrenEnabled)
                .Parameters.AddWithValue("@msg_children_free", MsgChildrenFree)
                .Parameters.AddWithValue("@children_note", ChildrenNote)
                .Parameters.AddWithValue("@confirm_prefix", ConfirmPrefix)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function SaveCompanyTerms(con As SqlConnection, ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sTerms As String, ByVal iCultureID As Integer) As Integer

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spCompanyTerm"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@termID", iTermID)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@cultureID", iCultureID)
                .Parameters.AddWithValue("@terms", sTerms)
            End With


            If iTermID = 0 Then
                iTermID = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            Else
                dataHelper.ExecuteNonQuery(cmd, con)
            End If


            Return iTermID

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function SaveCompanyWaiver(con As SqlConnection, ByVal CompanyCode As String, ByVal sMode As String, ByVal iTermID As Integer, ByVal sWaiver As String, ByVal iCultureID As Integer) As Integer

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim iID As Integer
        Try

            With cmd
                .Connection = con
                .CommandText = "spCompanyWaiver"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@termID", iTermID)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@cultureID", iCultureID)
                .Parameters.AddWithValue("@waiver", sWaiver)
            End With

            iID = Convert.ToInt32(dataHelper.ExecuteScalar(cmd, con))
            Return iID
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    


    Friend Function SaveCompanyProcessor(con As SqlConnection, ByVal CompanyCode As String, ByVal sMode As String, ByVal Commission As Double, ByVal ProcessorID As String, _
                                         ByVal KeyID As String, ByVal KeyHash As String, ByVal Type As String, ByVal URL As String, ByVal Username As String) As Response


        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spCompanyProcessor"
                .CommandType = CommandType.StoredProcedure

                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@commission", Commission)
                .Parameters.AddWithValue("@processorID", ProcessorID)
                .Parameters.AddWithValue("@keyID", KeyID)
                .Parameters.AddWithValue("@keyHash", KeyHash)
                .Parameters.AddWithValue("@type", Type)
                .Parameters.AddWithValue("@URL", URL)
                .Parameters.AddWithValue("@userName", Username)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try

    End Function


    Friend Function GetCompany(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String, ByVal sType As String) As List(Of CompanyInfo)

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim CompanyList As New List(Of CompanyInfo)()
        Dim sObjCompany As New CompanyInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
                .Parameters.AddWithValue("@company_type", sType)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjCompany = New CompanyInfo

                With sObjCompany
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppCompanyCode = .ppCompanyCode.Trim
                    .ppCompanyName = reader.Item("company_name") & ""
                    .ppResEmail = reader.Item("res_email") & ""
                    .ppResEmailAlt = reader.Item("res_email_alt") & ""
                    .ppLegalName = reader.Item("legal_name") & ""
                    .ppAddress = reader.Item("address") & ""
                    .ppCallTollFree = reader.Item("call_toll_free") & ""
                    .ppCompanyName = reader.Item("company_name") & ""
                    .ppContactEmail = reader.Item("contact_email") & ""
                    .ppContactName = reader.Item("contact_name") & ""
                    '.ppCreationDate = reader.Item("creation_date")
                    .ppEmail = reader.Item("email") & ""
                    .ppFax = reader.Item("fax_nr") & ""
                    .ppLegalID = reader.Item("legal_id") & ""
                    .ppTel1 = reader.Item("telephone_nr1") & ""
                    .ppTel2 = reader.Item("telephone_nr2") & ""
                    .ppTel3 = reader.Item("telephone_nr3") & ""
                    .ppWeb = reader.Item("web") & ""

                    '.ppCredomatic = reader.Item("credomatic")
                    '.ppBancoNacional = reader.Item("bancoNacional")
                    '.ppPayPal = reader.Item("PayPal")

                    'If IsNumeric(reader.Item("tax_perc")) Then
                    '    .ppTaxPerc = CDbl(reader.Item("tax_perc"))
                    'End If

                    .ppActive = reader.Item("active")
                    .ppCfoName = reader.Item("cfo_name") & ""
                    .ppCfoEmail = reader.Item("cfo_email") & ""
                    .ppBankName = reader.Item("bank_name") & ""
                    .ppBankAccount = reader.Item("bank_account") & ""
                    .ppResName = reader.Item("res_name") & ""
                    .ppResEmail = reader.Item("res_email") & ""
                    .ppResEmailAlt = reader.Item("res_email_alt") & ""
                    .ppFacebookURL = reader.Item("facebookURL") & ""
                    .ppTwitterText = reader.Item("twitterText") & ""
                    .ppWeb = reader.Item("web") & ""
                    .ppHotel = reader.Item("hotel") & ""
                    .ppServices = reader.Item("services") & ""
                    .ppGoogleID = reader.Item("google_id") & ""
                    .ppGoogleConvID = reader.Item("google_conv_id") & ""
                    .ppGoogleConvLabel = reader.Item("google_conv_label") & ""
                    .ppMigrar = reader.Item("migrar")
                    .ppPinterest = reader.Item("PinterestURL") & ""
                    .ppInstagram = reader.Item("InstagramURL") & ""
                    .ppBlog = reader.Item("BlogURL") & ""
                    .ppGooglePlus = reader.Item("GooglePlusURL") & ""
                    .ppThingsToDo = reader.Item("ThingsToDo") & ""
                    .ppGetThere = reader.Item("GetThere") & ""
                    .ppLatitude = reader.Item("Latitude") & ""
                    .ppLongitude = reader.Item("Longitude") & ""
                    '.ppMaxAdults = Val(reader.Item("max_adults"))
                    '.ppMaxChildren = Val(reader.Item("max_children"))

                    If .ppCredomatic = True And .ppBancoNacional = True And .ppPayPal = True Then
                        .Processor = "All"
                    ElseIf .ppCredomatic = True And .ppBancoNacional = False And .ppPayPal = False Then 'Only Credomatic
                        .Processor = "Credomatic"
                    ElseIf .ppCredomatic = False And .ppBancoNacional = True And .ppPayPal = False Then 'Only BN
                        .Processor = "BN"
                    ElseIf .ppCredomatic = True And .ppBancoNacional = True And .ppPayPal = False Then 'Credomatic + BN
                        .Processor = "CredomaticBN"
                    ElseIf .ppCredomatic = True And .ppBancoNacional = False And .ppPayPal = True Then 'Credomatic + Paypal
                        .Processor = "CredomaticPayPal"
                    ElseIf .ppCredomatic = False And .ppBancoNacional = True And .ppPayPal = True Then 'BN + Paypal
                        .Processor = "BNPayPal"
                    ElseIf .ppCredomatic = False And .ppBancoNacional = False And .ppPayPal = True Then 'Only PayPal
                        .Processor = "PayPal"
                    Else
                        .Processor = "NoProcessor"
                    End If

                End With
                CompanyList.Add(sObjCompany)
            End While

            reader.Close()

            Return CompanyList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetCompanyHotel(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim sObjRate As New RatesInfo

            With cmd
                .Connection = con
                .CommandText = "spCompanyHotel"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                Dim sObjCompany As New CompanyInfo
                With sObjCompany
                    .ppMaxChildren = Val(reader.Item("max_children"))
                    .ppTaxPerc = CDbl(reader.Item("tax_perc"))
                    .ppConfirmPrefix = reader.Item("confirm_prefix") & ""
                    .ppMaxAdults = Val(reader.Item("max_adults"))
                    .ppReservationNr = Val(reader.Item("reservation_nr"))
                    .ppMsgAdults = reader.Item("msg_adults") & ""
                    .ppMsgChildren = reader.Item("msg_children") & ""
                    .ppMsgChildrenFree = reader.Item("msg_children_free") & ""
                    .ppChildrenFree = CBool(reader.Item("children_free"))
                    .ppChildrenNote = reader.Item("children_note") & ""
                End With
                CompanyList.Add(sObjCompany)

            End While

            reader.Close()
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
    
    Friend Function GetCompanyService(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim sObjCompany As New CompanyInfo

            With cmd
                .Connection = con
                .CommandText = "spCompanyService"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                sObjCompany = New CompanyInfo

                With sObjCompany
                    .ppMaxAdults = Val(reader.Item("max_adults"))

                    If IsNumeric(reader.Item("tax_perc")) Then
                        .ppTaxPerc = CDbl(reader.Item("tax_perc"))
                    End If
                    If IsNumeric(reader.Item("rent_perc")) Then
                        .ppRentPorc = CDbl(reader.Item("rent_perc"))
                    End If
                    .ppChildrenFree = CBool(reader.Item("children_free"))
                    .ppConfirmPrefix = reader.Item("confirm_prefix") & ""
                    .ppMaxChildren = Val(reader.Item("max_children"))
                    .ppMaxStudent = Val(reader.Item("max_students"))
                    .ppReservationNr = Val(reader.Item("sale_nr"))
                    .ppMsgAdults = reader.Item("msg_adults") & ""
                    .ppMsgChildren = reader.Item("msg_children") & ""
                    .ppMsgStudent = reader.Item("msg_students") & ""
                    .ppMsgChildrenFree = reader.Item("msg_children_free") & ""
                    If IsNumeric(reader.Item("children_free")) Then
                        .ppChildrenFree = CDbl(reader.Item("children_free"))
                    End If
                    .ppChildrenNote = reader.Item("children_note") & ""
                End With
                CompanyList.Add(sObjCompany)

            End While

            reader.Close()

            Return CompanyList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function
  

    Friend Function GetCompanyProcessor(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String) As List(Of CompanyInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim sObjCompany As New CompanyInfo

            With cmd
                .Connection = con
                .CommandText = "spCompanyProcessor"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                sObjCompany = New CompanyInfo
                With sObjCompany
                    .ppCommission = reader.Item("credomaticPorcCom")
                    .ppProcessorID = reader.Item("processorID")
                    .ppKeyID = reader.Item("KeyID")
                    .ppKeyHash = reader.Item("KeyHash")
                    .ppType = reader.Item("Type")
                    .ppURL = reader.Item("URL")
                    .ppUserName = reader.Item("UserName")
                    .ppCredomatic = reader.Item("credomatic")
                    .ppBancoNacional = reader.Item("bancoNacional")
                    .ppPayPal = reader.Item("PayPal")
                End With
                CompanyList.Add(sObjCompany)
            End While

            Return CompanyList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function
    

    Friend Function DeleteCompany(con As SqlConnection, ByVal CompanyCode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@company_code", CompanyCode)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function
    

    Friend Function GetCompanyTerm(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = String.Empty

            With cmd
                .Connection = con
                .CommandText = "spCompanyTerm"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
                .Parameters.AddWithValue("@cultureID", iCulture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)
            If reader.Read Then
                sResult = reader.Item("term_id").ToString & "~" & reader.Item("terms")
            End If
            reader.Close()

            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    
    Friend Function GetCompanyWaiver(con As SqlConnection, ByVal sCompany_Code As String, ByVal sMode As String, ByVal iCulture As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spCompanyWaiver"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", sCompany_Code)
                .Parameters.AddWithValue("@cultureID", iCulture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("term_id").ToString & "~" & reader.Item("waiver")
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try

    End Function
    
    Friend Function GetCompanyPrefix(con As SqlConnection, ByVal sPrefix As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spCompanyPrefix"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@prefix", sPrefix)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("prefix").ToString
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetCompanyServicePrefix(con As SqlConnection, ByVal sPrefix As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spCompanyServicePrefix"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@prefix", sPrefix)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("prefix").ToString
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetCompanyEmails(ByVal con As SqlConnection, ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure

                .Parameters.AddWithValue("mode", "EMA")
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("res_email") & "|" & reader.Item("res_email_alt")
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanyData(ByVal con As SqlConnection, ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "EXT")
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("Existe") & ""
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanyGoogleInfo(ByVal con As SqlConnection, ByVal sCompanyCode As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim sObjCompany As New CompanyInfo
            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "GOG")
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                sObjCompany = New CompanyInfo
                With sObjCompany
                    .ppGoogleConvID = reader.Item("google_conv_id")
                    .ppGoogleConvLabel = reader.Item("google_conv_label")
                    .ppGoogleID = reader.Item("google_id")
                End With
                CompanyList.Add(sObjCompany)

            End While

            reader.Close()
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanyTermsEmails(ByVal con As SqlConnection, ByVal sCompanyCode As String, ByVal sCulture As String) As List(Of CompanyInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim sObjCompany As New CompanyInfo

            With cmd
                .Connection = con
                .CommandText = "spCompanies"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "SE1")
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("culture", sCulture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                sObjCompany = New CompanyInfo
                With sObjCompany
                    .ppResEmail = reader.Item("res_email")
                    .ppResEmailAlt = reader.Item("res_email_alt")
                    .ppTerms = reader.Item("terms")
                    .ppWaiver = reader.Item("waiver")
                    .ppCompanyName = reader.Item("company_name")
                End With
                CompanyList.Add(sObjCompany)

            End While

            reader.Close()
            Return CompanyList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetPayPalSubject(ByVal con As SqlConnection, ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spGetPayPalSubject"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("PayPalSubject").ToString
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetBankCredomaticKeys(ByVal con As SqlConnection, ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spGetBankCredomaticKeys"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("KeyID") & "|" & reader.Item("KeyHash") & "|" & reader.Item("type") & "|" & reader.Item("UserName") & "|" & reader.Item("processorID") & "|" & reader.Item("URL") & "|" & reader.Item("CredomaticProcID") & "|" & reader.Item("processorIDAmex")
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetBankBNKeys(ByVal con As SqlConnection, ByVal sCompanyCode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""
            With cmd
                .Connection = con
                .CommandText = "spGetBankBNKeys"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If reader.Read Then
                sResult = reader.Item("IDACQUIRER") & "|" & reader.Item("IDCOMMERCE")
            End If

            reader.Close()
            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetFacilities(ByVal con As SqlConnection, ByVal sCompanyCode As String) As List(Of clsFeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim FacilitysList As New List(Of clsFeaturesInfo)()
            Dim objFacility As New clsFeaturesInfo

            With cmd
                .Connection = con
                .CommandText = "spCompanyFacilities"
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("mode", "HOM")
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()
                objFacility = New clsFeaturesInfo
                With objFacility
                    .ppDescription = reader.Item("description") & ""
                End With
                FacilitysList.Add(objFacility)

            End While

            reader.Close()

            Return FacilitysList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanyPagingCount(con As SqlConnection, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetCompaniesPagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCompanyPaging(con As SqlConnection, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of CompanyInfo)

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim CompanyList As New List(Of CompanyInfo)()
        Dim sObjCompany As New CompanyInfo

        Try
            With cmd
                .Connection = con
                .CommandText = "spGetCompaniesPaging"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjCompany = New CompanyInfo

                With sObjCompany
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppCompanyName = reader.Item("company_name") & ""
                    .ppResEmail = reader.Item("res_email") & ""
                    .ppResEmailAlt = reader.Item("res_email_alt") & ""
                    .ppLegalName = reader.Item("legal_name") & ""
                    .ppAddress = reader.Item("address") & ""
                    .ppCallTollFree = reader.Item("call_toll_free") & ""
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppCompanyName = reader.Item("company_name") & ""
                    .ppContactEmail = reader.Item("contact_email") & ""
                    .ppContactName = reader.Item("contact_name") & ""
                    '.ppCreationDate = reader.Item("creation_date")
                    .ppEmail = reader.Item("email") & ""
                    .ppFax = reader.Item("fax_nr") & ""
                    .ppLegalID = reader.Item("legal_id") & ""
                    .ppTel1 = reader.Item("telephone_nr1") & ""
                    .ppTel2 = reader.Item("telephone_nr2") & ""
                    .ppTel3 = reader.Item("telephone_nr3") & ""
                    .ppWeb = reader.Item("web") & ""

                    '.ppCredomatic = reader.Item("credomatic")
                    '.ppBancoNacional = reader.Item("bancoNacional")
                    '.ppPayPal = reader.Item("PayPal")

                    'If IsNumeric(reader.Item("tax_perc")) Then
                    '    .ppTaxPerc = CDbl(reader.Item("tax_perc"))
                    'End If

                    .ppActive = reader.Item("active")
                    .ppCfoName = reader.Item("cfo_name") & ""
                    .ppCfoEmail = reader.Item("cfo_email") & ""
                    .ppBankName = reader.Item("bank_name") & ""
                    .ppBankAccount = reader.Item("bank_account") & ""
                    .ppResName = reader.Item("res_name") & ""
                    .ppResEmail = reader.Item("res_email") & ""
                    .ppResEmailAlt = reader.Item("res_email_alt") & ""
                    .ppFacebookURL = reader.Item("facebookURL") & ""
                    .ppTwitterText = reader.Item("twitterText") & ""
                    .ppWeb = reader.Item("web") & ""
                    .ppHotel = reader.Item("hotel") & ""
                    .ppServices = reader.Item("services") & ""
                    .ppGoogleID = reader.Item("google_id") & ""
                    .ppGoogleConvID = reader.Item("google_conv_id") & ""
                    .ppGoogleConvLabel = reader.Item("google_conv_label") & ""
                    .ppMigrar = reader.Item("migrar")

                    .provincia = reader.Item("provincia") & ""
                    .canton = reader.Item("canton") & ""
                    .distrito = reader.Item("distrito") & ""

                    .TechContact = reader.Item("TechContact") & ""
                    .TechEmail = reader.Item("TechEmail") & ""
                    .ppPinterest = reader.Item("PinterestURL") & ""
                    .ppInstagram = reader.Item("InstagramURL") & ""
                    .ppBlog = reader.Item("BlogURL") & ""
                    .ppGooglePlus = reader.Item("GooglePlusURL") & ""
                    .ppThingsToDo = reader.Item("ThingsToDo") & ""
                    .ppGetThere = reader.Item("GetThere") & ""
                    .ppLatitude = reader.Item("Latitude") & ""
                    .ppLongitude = reader.Item("Longitude") & ""
                    '.ppMaxAdults = Val(reader.Item("max_adults"))
                    '.ppMaxChildren = Val(reader.Item("max_children"))

                    'If .ppCredomatic = True And .ppBancoNacional = True And .ppPayPal = True Then
                    '    .Processor = "All"
                    'ElseIf .ppCredomatic = True And .ppBancoNacional = False And .ppPayPal = False Then 'Only Credomatic
                    '    .Processor = "Credomatic"
                    'ElseIf .ppCredomatic = False And .ppBancoNacional = True And .ppPayPal = False Then 'Only BN
                    '    .Processor = "BN"
                    'ElseIf .ppCredomatic = True And .ppBancoNacional = True And .ppPayPal = False Then 'Credomatic + BN
                    '    .Processor = "CredomaticBN"
                    'ElseIf .ppCredomatic = True And .ppBancoNacional = False And .ppPayPal = True Then 'Credomatic + Paypal
                    '    .Processor = "CredomaticPayPal"
                    'ElseIf .ppCredomatic = False And .ppBancoNacional = True And .ppPayPal = True Then 'BN + Paypal
                    '    .Processor = "BNPayPal"
                    'ElseIf .ppCredomatic = False And .ppBancoNacional = False And .ppPayPal = True Then 'Only PayPal
                    '    .Processor = "PayPal"
                    'Else
                    '    .Processor = "NoProcessor"
                    'End If

                End With
                CompanyList.Add(sObjCompany)
            End While

            reader.Close()

            Return CompanyList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
