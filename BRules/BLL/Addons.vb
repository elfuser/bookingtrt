﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Addons

#Region "Public Methods"

    Public Function GetAddonEnabled(ByVal sCompanyCode As String) As List(Of AddonInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetAddonEnabled(con, sCompanyCode)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetReservationAddons(ByVal ReservationID As String, ByVal Culture As String) As List(Of AddonInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetReservationAddons(con, ReservationID, Culture)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetReservationAddonsQuo(ByVal QuotationID As String, ByVal Culture As String) As List(Of AddonInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetReservationAddonsQuo(con, QuotationID, Culture)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of CultureInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetCulture(con, iCultureID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAddons(ByVal iAddonID As Long, ByVal sCompanyCode As String) As List(Of AddonInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetAddons(con, iAddonID, sCompanyCode)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetAddonRates(ByVal iAddonID As Long, ByVal sCompanyCode As String) As List(Of AddonRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetAddonRates(con, iAddonID, sCompanyCode)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetAddonLocale(ByVal iAddonID As Long, ByVal iCultureID As Integer) As List(Of AddonLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of AddonLocaleInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetAddonLocale(con, iAddonID, iCultureID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveAddon(ByVal AddonID As Long, ByVal Description As String, ByVal CompanyCode As String, ByVal Amount As Double, Enabled As Boolean, ByVal RowOrder As Integer, ByVal sMode As String,
                              ByVal LongDes As String, ByVal Departure As String, ByVal Duration As String, ByVal AddonType As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Amount1 As Double, ByVal Amount2 As Double, ByVal Amount3 As Double, ByVal Amount4 As Double,
                              ByVal Amount5 As Double, ByVal Amount6 As Double, ByVal Amount7 As Double, ByVal Amount8 As Double, ByVal Amount9 As Double, ByVal Amount10 As Double, ByVal Tax As Boolean) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String = ""
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAddon(con, AddonID, Description, CompanyCode, Amount, Enabled, RowOrder, sMode,
                              LongDes, Departure, Duration, AddonType,
                              DateFrom, DateTo, Amount1, Amount2, Amount3, Amount4,
                              Amount5, Amount6, Amount7, Amount8, Amount9, Amount10, Tax)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAddonLocale(con, AddonID, CultureID, Description, LongDescription, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteAddon(ByVal AddonID As Long) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAddon(con, AddonID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteAddonLocale(ByVal AddonID As Long, ByVal CultureID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAddonLocale(con, AddonID, CultureID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteAddonRate(ByVal RateID As Long) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteAddonRate(con, RateID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveAddonPicture(ByVal AddonID As Long, ByVal numPhoto As Integer, ByVal Picture As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAddonPicture(con, AddonID, numPhoto, Picture)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAddonRate(ByVal sMode As String, ByVal RateID As Integer, ByVal AddonID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal Type As String, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer,
                                    ByVal MaxStudent As Integer, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer, ByVal Price As Double,
                                    ByVal Adults As Double, ByVal Children As Double, ByVal Infant As Double, ByVal Students As Double, ByVal Active As Boolean) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAddonRate(con, sMode, RateID, AddonID, Description,
                                    StartDate, EndDate, Type, MaxAdult, MaxChildren,
                                    MaxStudent, MaxOccupance, MinOccupance, Price,
                                    Adults, Children, Infant, Students, Active)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function




    Public Function GetAddonQtys(ByVal companyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of AddonInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim AddOnsList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                AddOnsList = GetAddonQtys(con, companyCode, DateFrom, DateTo)
            End Using
            Return AddOnsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetAddons(ByVal companyCode As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, culture As String, Category As String) As List(Of AddonInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim AddOnsList As New List(Of AddonInfo)()

            Using con As SqlConnection = DataHelper.OpenConnection()
                AddOnsList = GetAddons(con, companyCode, DateFrom, DateTo, culture, Category)
            End Using

            Return AddOnsList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetAddonPaging(ByVal sCompanyCode As String, culture As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AddonInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim AddOnsList As New List(Of AddonInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                AddOnsList = GetAddonPaging(con, sCompanyCode, culture, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return AddOnsList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetAddonPagingCount(ByVal sCompanyCode As String, culture As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetAddonPagingCount(con, sCompanyCode, culture, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

#Region "Private Methods"

    Friend Function GetAddonPaging(con As SqlConnection, ByVal sCompanyCode As String, culture As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spGetAddonsPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("culture", culture)
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo
                With objAddonInfo
                    .ppAddonID = reader.Item("addonID")
                    .ppDescription = reader.Item("description") & ""
                    .ppAmount = reader.Item("amount")
                    .ppEnabled = reader.Item("enabled")
                    .ppRowOrder = reader.Item("roworder")
                    .ppLongDescription = reader.Item("longDescription") & ""
                    .ppDuration = reader.Item("duration") & ""
                    .ppDeparture = reader.Item("departure") & ""
                    .ppAddonType = reader.Item("addonType").ToString().Trim

                    .Picture1 = reader.Item("picture1") & ""
                    .Picture2 = reader.Item("picture2") & ""

                    If IsDate(reader.Item("valid_from")) Then
                        .ppFrom = reader.Item("valid_from")
                    End If

                    If IsDate(reader.Item("valid_to")) Then
                        .ppTo = reader.Item("valid_to")
                    End If


                    If IsNumeric(reader.Item("amount1")) Then
                        .ppAmount1 = CDbl(reader.Item("amount1"))
                    End If


                    If IsNumeric(reader.Item("amount2")) Then
                        .ppAmount2 = CDbl(reader.Item("amount2"))
                    End If


                    If IsNumeric(reader.Item("amount3")) Then
                        .ppAmount3 = CDbl(reader.Item("amount3"))
                    End If


                    If IsNumeric(reader.Item("amount4")) Then
                        .ppAmount4 = CDbl(reader.Item("amount4"))
                    End If


                    If IsNumeric(reader.Item("amount5")) Then
                        .ppAmount5 = CDbl(reader.Item("amount5"))
                    End If


                    If IsNumeric(reader.Item("amount6")) Then
                        .ppAmount6 = CDbl(reader.Item("amount6"))
                    End If


                    If IsNumeric(reader.Item("amount7")) Then
                        .ppAmount7 = CDbl(reader.Item("amount7"))
                    End If


                    If IsNumeric(reader.Item("amount8")) Then
                        .ppAmount8 = CDbl(reader.Item("amount8"))
                    End If

                    If IsNumeric(reader.Item("amount9")) Then
                        .ppAmount9 = CDbl(reader.Item("amount9"))
                    End If


                    If IsNumeric(reader.Item("amount10")) Then
                        .ppAmount10 = CDbl(reader.Item("amount10"))
                    End If

                    .CultureID = reader.Item("CultureID")
                    .ppTax = reader.Item("tax")
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddonPagingCount(con As SqlConnection, ByVal sCompanyCode As String, culture As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetAddonsPagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("culture", culture)
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetAddonEnabled(con As SqlConnection, ByVal sCompanyCode As String) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "RES")
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo

                With objAddonInfo
                    .ppAddonID = reader.Item("addonID")
                    .ppAmount = reader.Item("amount")
                    .ppQty = 0
                    .ppCompanyCode = reader.Item("company_code") & ""
                    If IsDate(reader.Item("created")) Then
                        .ppCreated = reader.Item("created")
                    End If
                    .ppDescription = reader.Item("description") & ""
                    .ppEnabled = reader.Item("enabled")
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationAddons(con As SqlConnection, ByVal ReservationID As String, ByVal Culture As String) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spGetReservationAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("reservation_id", ReservationID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo

                With objAddonInfo
                    .ppQty = reader.Item("quantity")
                    .ppAmount = reader.Item("amount")
                    .ppDescription = reader.Item("description") & ""
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationAddonsQuo(con As SqlConnection, ByVal QuotationID As String, ByVal Culture As String) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spGetReservationAddonsQuo"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("quotation_id", QuotationID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo

                With objAddonInfo
                    .ppQty = reader.Item("quantity")
                    .ppAmount = reader.Item("amount")
                    .ppDescription = reader.Item("description") & ""
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCulture(con As SqlConnection, ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CultureInfoList As New List(Of CultureInfo)()
            Dim objCultureInfo As New CultureInfo

            With cmd
                .CommandText = "spCulture"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@cultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objCultureInfo = New CultureInfo

                With objCultureInfo
                    .ppCultureID = Val(reader.Item("CultureID") & "")
                    .ppCultureName = reader.Item("CultureName") & ""
                    .ppDisplayName = reader.Item("DisplayName") & ""
                End With
                CultureInfoList.Add(objCultureInfo)

            End While

            reader.Close()

            Return CultureInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddons(con As SqlConnection, ByVal iAddonID As Long, ByVal sCompanyCode As String) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@addonID", iAddonID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo
                With objAddonInfo
                    .ppAddonID = reader.Item("addonID")
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    .ppAmount = reader.Item("amount")
                    .ppCreated = reader.Item("created")
                    .ppEnabled = reader.Item("enabled")
                    .ppRowOrder = reader.Item("roworder")
                    .ppLongDescription = reader.Item("longDescription")
                    .ppDuration = reader.Item("duration")
                    .ppDeparture = reader.Item("departure")
                    .ppAddonType = reader.Item("addonType")
                    .ppFrom = reader.Item("valid_from")
                    .ppTo = reader.Item("valid_to")
                    .ppAmount1 = reader.Item("amount1")
                    .ppAmount2 = reader.Item("amount2")
                    .ppAmount3 = reader.Item("amount3")
                    .ppAmount4 = reader.Item("amount4")
                    .ppAmount5 = reader.Item("amount5")
                    .ppAmount6 = reader.Item("amount6")
                    .ppAmount7 = reader.Item("amount7")
                    .ppAmount8 = reader.Item("amount8")
                    .ppAmount9 = reader.Item("amount9")
                    .ppAmount10 = reader.Item("amount10")
                    .ppTax = reader.Item("tax")
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddonRates(con As SqlConnection, ByVal iRateID As Integer, ByVal iAddonID As Integer) As List(Of AddonRateInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonRateInfo)()
            Dim objAddonInfo As New AddonRateInfo

            With cmd
                .CommandText = "spAddonRate"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@RateID", iRateID)
                .Parameters.AddWithValue("@AddonID", iAddonID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonRateInfo
                With objAddonInfo
                    .ppRateID = reader.Item("RateID")
                    .ppDescription = reader.Item("Description")
                    .ppStartDate = reader.Item("StartDate")
                    .ppEndDate = reader.Item("EndDate")
                    .ppAdults = reader.Item("PriceAdult")
                    .ppChildren = reader.Item("PriceChildren")
                    .ppInfants = reader.Item("PriceInfant")
                    .ppStudents = reader.Item("PriceStudent")
                    .ppMaxAdults = reader.Item("MaxAdult")
                    .ppMaxChildren = reader.Item("MaxChildren")
                    .ppMaxStudents = reader.Item("MaxStudent")
                    .ppType = reader.Item("Type")
                    .ppPrice = reader.Item("Price")
                    .ppActive = reader.Item("Active")
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddonLocale(con As SqlConnection, ByVal iAddonID As Long, ByVal iCultureID As Integer) As List(Of AddonLocaleInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonLocaleInfo)()
            Dim objAddonInfo As New AddonLocaleInfo

            With cmd
                .CommandText = "spAddonsLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@AddonID", iAddonID)
                .Parameters.AddWithValue("@CultureID", iCultureID)
            End With

            Dim objDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While objDr.Read()

                objAddonInfo = New AddonLocaleInfo
                With objAddonInfo
                    .ppAddonID = objDr.Item("addonID")
                    .ppCultureID = objDr.Item("cultureID")
                    .ppDescription = objDr.Item("description")
                    .ppLongDescription = objDr.Item("longDescription")
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            objDr.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAddon(con As SqlConnection, ByVal AddonID As Long, ByVal Description As String, ByVal CompanyCode As String, ByVal Amount As Double, Enabled As Boolean, ByVal RowOrder As Integer, ByVal sMode As String,
                              ByVal LongDes As String, ByVal Departure As String, ByVal Duration As String, ByVal AddonType As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Amount1 As Double, ByVal Amount2 As Double, ByVal Amount3 As Double, ByVal Amount4 As Double,
                              ByVal Amount5 As Double, ByVal Amount6 As Double, ByVal Amount7 As Double, ByVal Amount8 As Double, ByVal Amount9 As Double, ByVal Amount10 As Double, ByVal Tax As Boolean) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = ""
        Dim iRow As Integer

        Try

            With cmd
                .CommandText = "spAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@Mode", sMode)
                .Parameters.AddWithValue("@addonID", AddonID)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@amount", Amount)
                .Parameters.AddWithValue("@enabled", Enabled)
                .Parameters.AddWithValue("@roworder", RowOrder)
                .Parameters.AddWithValue("@longDescription", LongDes)
                .Parameters.AddWithValue("@departure", Departure)
                .Parameters.AddWithValue("@duration", Duration)
                .Parameters.AddWithValue("@addonType", AddonType)
                .Parameters.AddWithValue("@from", DateFrom)
                .Parameters.AddWithValue("@to", DateTo)
                .Parameters.AddWithValue("@amount1", Amount1)
                .Parameters.AddWithValue("@amount2", Amount2)
                .Parameters.AddWithValue("@amount3", Amount3)
                .Parameters.AddWithValue("@amount4", Amount4)
                .Parameters.AddWithValue("@amount5", Amount5)
                .Parameters.AddWithValue("@amount6", Amount6)
                .Parameters.AddWithValue("@amount7", Amount7)
                .Parameters.AddWithValue("@amount8", Amount8)
                .Parameters.AddWithValue("@amount9", Amount9)
                .Parameters.AddWithValue("@amount10", Amount10)
                .Parameters.AddWithValue("@tax", Tax)

                If sMode = "INS" Then
                    iRow = CInt(.ExecuteScalar())
                    sResult = "ID " & iRow.ToString
                Else
                    .ExecuteNonQuery()
                    sResult = "UP"
                End If

            End With


            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAddonLocale(con As SqlConnection, ByVal AddonID As Long, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAddonsLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@AddonID", AddonID)
                .Parameters.AddWithValue("@CultureID", CultureID)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@LongDescription", LongDescription)

            End With



            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAddon(con As SqlConnection, ByVal AddonID As Long) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAddons"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@addonID", AddonID)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAddonLocale(con As SqlConnection, ByVal AddonID As Long, ByVal CultureID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAddonsLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@AddonID", AddonID)
                .Parameters.AddWithValue("@CultureID", CultureID)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteAddonRate(con As SqlConnection, ByVal RateID As Long) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAddonRate"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@RateID", RateID)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)


            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAddonPicture(con As SqlConnection, ByVal AddonID As Long, ByVal numPhoto As Integer, ByVal Picture As Byte()) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spSaveAddOnPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@AddOn_id", AddonID)
                .Parameters.AddWithValue("@numPhoto", numPhoto)
                .Parameters.AddWithValue("@Image", Picture)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)


            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAddonRate(con As SqlConnection, ByVal sMode As String, ByVal RateID As Integer, ByVal AddonID As Integer, ByVal Description As String,
                                    ByVal StartDate As Date, ByVal EndDate As Date, ByVal Type As String, ByVal MaxAdult As Integer, ByVal MaxChildren As Integer,
                                    ByVal MaxStudent As Integer, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer, ByVal Price As Double,
                                    ByVal Adults As Double, ByVal Children As Double, ByVal Infant As Double, ByVal Students As Double, ByVal Active As Boolean) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spAddonRate"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@RateID", RateID)
                .Parameters.AddWithValue("@AddonID", AddonID)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@StartDate", StartDate)
                .Parameters.AddWithValue("@EndDate", EndDate)
                .Parameters.AddWithValue("@Type", Type)
                .Parameters.AddWithValue("@MaxAdult", MaxAdult)
                .Parameters.AddWithValue("@MaxChildren", MaxChildren)
                .Parameters.AddWithValue("@MaxStudent", MaxStudent)
                .Parameters.AddWithValue("@MaxOccupance", MaxOccupance)
                .Parameters.AddWithValue("@MinOccupance", MinOccupance)
                .Parameters.AddWithValue("@Price", Price)
                .Parameters.AddWithValue("@PriceAdult", Adults)
                .Parameters.AddWithValue("@PriceChildren", Children)
                .Parameters.AddWithValue("@PriceInfant", Infant)
                .Parameters.AddWithValue("@PriceStudent", Students)
                .Parameters.AddWithValue("@Active", Active)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)


            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetAddonQtys(con As SqlConnection, ByVal companyCode As String, ByVal DateFrom As Date, ByVal DateTo As Date) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spGetAddonsCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", companyCode)
                .Parameters.AddWithValue("@from", DateFrom)
                .Parameters.AddWithValue("@to", DateTo)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo
                With objAddonInfo
                    .ppAddonType = reader.Item("addonType")
                    .ppQty = Val(reader.Item("qty") & "")

                    Select Case .ppAddonType
                        Case "S"
                            .ppDescription = "SPA"
                        Case "T"
                            .ppDescription = "Tours"
                        Case "X"
                            .ppDescription = "Transport"
                        Case "O"
                            .ppDescription = "Other"
                        Case "M"
                            .ppDescription = "Meals"
                        Case "N"
                            .ppDescription = "Extra Night Special"
                    End Select

                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetAddons(con As SqlConnection, ByVal companyCode As String,
                              ByVal DateFrom As Date, ByVal DateTo As Date, culture As String, Category As String) As List(Of AddonInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim AddonInfoList As New List(Of AddonInfo)()
            Dim objAddonInfo As New AddonInfo

            With cmd
                .CommandText = "spGetAddonList"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", companyCode)
                .Parameters.AddWithValue("@from", DateFrom)
                .Parameters.AddWithValue("@to", DateTo)
                .Parameters.AddWithValue("@culture", culture)
                .Parameters.AddWithValue("@categoryID", Category)


            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objAddonInfo = New AddonInfo
                With objAddonInfo
                    .ppAddonType = reader.Item("addonType") & ""
                    If IsNumeric(reader.Item("amount")) Then
                        .ppAmount = CDbl(reader.Item("amount"))
                    End If

                    .ppDescription = reader.Item("description") & ""
                    .ppLongDescription = reader.Item("longDescription") & ""
                    .ppAddonID = Val(reader.Item("addOnId") & "")
                    .ImageUlr = "/Handlers/getAddonPhoto.ashx?AddonID=" + .ppAddonID.ToString
                End With
                AddonInfoList.Add(objAddonInfo)

            End While

            reader.Close()

            Return AddonInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
