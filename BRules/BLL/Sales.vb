﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String

Public Class Sales

#Region "Public Methods"

    Public Function SaveSale(ByVal sMode As String, ByVal sProcessor As String, ByVal objRes As clsSaleInfo) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveSale(con, sMode, sProcessor, objRes)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveSaleItem(ByVal sMode As String, ByVal objRes As clsSaleItemInfo) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveSaleItem(con, sMode, objRes)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetSale(ByVal SaleID As String) As List(Of clsSaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New List(Of clsSaleInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetSale(con, SaleID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetSaleItems(ByVal SaleID As String, ByVal Culture As String) As List(Of clsSaleItemInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New List(Of clsSaleItemInfo)

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = GetSaleItems(SaleID, Culture)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailability(ByVal ResNumber As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailability(con, ResNumber)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function UpdateStatus(ByVal ResNumber As String, ByVal Status As Int16) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = UpdateStatus(con, ResNumber, Status)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function SaveSale(con As SqlConnection, ByVal sMode As String, ByVal sProcessor As String, ByVal objRes As clsSaleInfo) As String

        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Dim lResult As String = String.Empty
        Try


            With sqlCmd
                .CommandText = "spSale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                With objRes
                    sqlCmd.Parameters.AddWithValue("Mode", sMode)
                    sqlCmd.Parameters.AddWithValue("sale_id", .ppSaleID)
                    sqlCmd.Parameters.AddWithValue("guest_id", .ppGuestID)
                    sqlCmd.Parameters.AddWithValue("company_code", .ppCompany_Code)
                    sqlCmd.Parameters.AddWithValue("rate_id", .ppRateID)
                    sqlCmd.Parameters.AddWithValue("cancellation_date", .ppCancellation_Date)
                    sqlCmd.Parameters.AddWithValue("remarks", .ppRemarks)
                    sqlCmd.Parameters.AddWithValue("promo_id", .ppPromoID)
                    sqlCmd.Parameters.AddWithValue("amount", .ppAmount)
                    sqlCmd.Parameters.AddWithValue("tax_amount", .ppTaxAmount)
                    sqlCmd.Parameters.AddWithValue("promo_amount", .ppPromoAmount)
                    sqlCmd.Parameters.AddWithValue("disc_amount", .ppDiscount)
                    sqlCmd.Parameters.AddWithValue("processor", sProcessor)
                    sqlCmd.Parameters.AddWithValue("origin", .ppOrigin)
                    sqlCmd.Parameters.AddWithValue("sessionID", .ppSessionID)
                End With
            End With

            lResult = dataHelper.ExecuteScalar(sqlCmd, con)

            Return lResult

            'Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            'response.Status = "0"
            'response.Message = "Command executes OK"
            'Return response

        Catch EX1 As Exception
            'response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveSaleItem(con As SqlConnection, ByVal sMode As String, ByVal objRes As clsSaleItemInfo) As Response

        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try


            With sqlCmd
                .CommandText = "spSaleItem"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                With objRes
                    sqlCmd.Parameters.AddWithValue("Mode", sMode)
                    sqlCmd.Parameters.AddWithValue("sale_id", .ppSaleID)
                    sqlCmd.Parameters.AddWithValue("ServiceID", .ppServiceID)
                    sqlCmd.Parameters.AddWithValue("ScheduleID", .ppScheduleID)
                    sqlCmd.Parameters.AddWithValue("adults_qty", .ppAdultQty)
                    sqlCmd.Parameters.AddWithValue("children_qty", .ppChildrenQty)
                    sqlCmd.Parameters.AddWithValue("students_qty", .ppStudentsQty)
                    sqlCmd.Parameters.AddWithValue("infants_qty", .ppInfantsQty)
                    sqlCmd.Parameters.AddWithValue("adults_amount", .ppAdultAmount)
                    sqlCmd.Parameters.AddWithValue("children_amount", .ppChildrenAmount)
                    sqlCmd.Parameters.AddWithValue("students_amount", .ppStudentsAmount)
                    sqlCmd.Parameters.AddWithValue("infants_amount", .ppInfantsAmount)
                    sqlCmd.Parameters.AddWithValue("discount", .ppDiscount)
                End With
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetSale(con As SqlConnection,
                                   ByVal SaleID As String) As List(Of clsSaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sales As New List(Of clsSaleInfo)()
            Dim sObjComb As New clsSaleInfo

            With cmd
                .CommandText = "spSale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SEL")
                .Parameters.AddWithValue("sale_id", SaleID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                sObjComb = New clsSaleInfo
                With sObjComb
                    .ppSaleID = reader.Item("sale_id") & ""
                    .ppRemarks = reader.Item("remarks") & ""
                    If IsNumeric(reader.Item("amount")) Then
                        .ppAmount = reader.Item("amount")
                    End If
                    If IsNumeric(reader.Item("tax_amount")) Then
                        .ppTaxAmount = reader.Item("tax_amount")
                    End If
                    If IsNumeric(reader.Item("promo_amount")) Then
                        .ppPromoAmount = reader.Item("promo_amount")
                    End If
                    If IsNumeric(reader.Item("disc_amount")) Then
                        .ppDiscount = reader.Item("disc_amount")
                    End If
                    .ppName = reader.Item("name") & ""
                    .ppLastName = reader.Item("lastname") & ""
                    .ppTelephone = reader.Item("telephone_nr") & ""
                    .ppEmail = reader.Item("email") & ""
                    .ppCountry = reader.Item("country_name") & ""
                End With
                sales.Add(sObjComb)
            End While

            reader.Close()

            Return sales

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetSaleItems(con As SqlConnection,
                                   ByVal SaleID As String, ByVal Culture As String) As List(Of clsSaleItemInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sales As New List(Of clsSaleItemInfo)()
            Dim sObjComb As New clsSaleItemInfo

            With cmd
                .CommandText = "spSaleItem"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("mode", "SEL")
                .Parameters.AddWithValue("sale_id", SaleID)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim sqlDr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While sqlDr.Read()

                sObjComb = New clsSaleItemInfo
                With sObjComb
                    .ppDescription = sqlDr.Item("Description") & ""
                    .ppAdultQty = Val(sqlDr.Item("adults_qty") & "")
                    .ppChildrenQty = Val(sqlDr.Item("children_qty") & "")
                    .ppInfantsQty = Val(sqlDr.Item("infants_qty") & "")

                    If IsNumeric(sqlDr.Item("adults_amount")) Then
                        .ppAdultAmount = sqlDr.Item("adults_amount")
                    End If

                    If IsNumeric(sqlDr.Item("children_amount")) Then
                        .ppChildrenAmount = sqlDr.Item("children_amount")
                    End If

                    If IsNumeric(sqlDr.Item("infants_amount")) Then
                        .ppInfantsAmount = sqlDr.Item("infants_amount")
                    End If

                    If IsNumeric(sqlDr.Item("discount")) Then
                        .ppDiscount = sqlDr.Item("discount")
                    End If
                    .ppSchedule = sqlDr.Item("schedule") & ""

                    If IsNumeric(sqlDr.Item("arrival_date")) Then
                        .ppArrival = sqlDr.Item("arrival_date")
                    End If

                    .ppConditions = sqlDr.Item("Conditions") & ""
                    .ppBringInfo = sqlDr.Item("BringInfo") & ""
                End With
                sales.Add(sObjComb)
            End While

            sqlDr.Close()

            Return sales

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailability(con As SqlConnection, ByVal ResNumber As String) As Response

        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try


            With sqlCmd
                .CommandText = "spUpdateAvailabilityServices"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("sale_id", ResNumber)
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function UpdateStatus(con As SqlConnection, ByVal ResNumber As String, ByVal Status As Int16) As Response

        Dim dataHelper As New DataHelper()
        Dim sqlCmd As New SqlCommand()
        Dim response As New Response()
        Try


            With sqlCmd
                .CommandText = "spUpdateServiceStatus"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("sale_id", ResNumber)
                .Parameters.AddWithValue("status", Status)
            End With

            Call dataHelper.ExecuteNonQuery(sqlCmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region
End Class
