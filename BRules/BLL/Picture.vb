﻿

Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class Picture

#Region "Public Methods"

    Public Function SaveToDatabaseRoomPhoto(roomTypeID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveToDatabaseRoomPhoto(con, roomTypeID, photoNumber, pictureStream)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveToDatabaseCompanyPhoto(companyCode As String, photoNumber As Integer, pictureStream As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveToDatabaseCompanyPhoto(con, companyCode, photoNumber, pictureStream)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveToDatabaseServicePhoto(serviceID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveToDatabaseServicePhoto(con, serviceID, photoNumber, pictureStream)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DownloadRoomPhoto(RoomTypeID As Integer, photoNumber As Integer) As PictureFile
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New PictureFile

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DownloadRoomPhoto(con, RoomTypeID, photoNumber)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DownloadCompanyPhoto(companyCode As String, photoNumber As Integer) As PictureFile
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New PictureFile

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DownloadCompanyPhoto(con, companyCode, photoNumber)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DownloadServicePhoto(ServiceID As Integer, photoNumber As Integer) As PictureFile
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New PictureFile

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DownloadServicePhoto(con, ServiceID, photoNumber)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


#End Region

#Region "Private Methods"

    Friend Function SaveToDatabaseRoomPhoto(con As SqlConnection, roomTypeID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spRoomPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                Dim param0 As SqlParameter = .Parameters.Add("@mode", SqlDbType.Char)
                param0.Value = "INS"
                Dim param1 As SqlParameter = .Parameters.Add("@roomtype_id", SqlDbType.Int)
                param1.Value = roomTypeID
                Dim param2 As SqlParameter = .Parameters.Add("@photoNumber", SqlDbType.Int)
                param2.Value = photoNumber
                Dim param3 As SqlParameter = .Parameters.Add("@photo", SqlDbType.VarBinary)
                param3.Value = pictureStream
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveToDatabaseCompanyPhoto(con As SqlConnection, companyCode As String, photoNumber As Integer, pictureStream As Byte()) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spCompanyPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                Dim param0 As SqlParameter = .Parameters.Add("@mode", SqlDbType.Char)
                param0.Value = "INS"
                Dim param1 As SqlParameter = .Parameters.Add("@company_code", SqlDbType.Char)
                param1.Value = companyCode
                Dim param2 As SqlParameter = .Parameters.Add("@photoNumber", SqlDbType.Int)
                param2.Value = photoNumber
                Dim param3 As SqlParameter = .Parameters.Add("@photo", SqlDbType.VarBinary)
                param3.Value = pictureStream
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveToDatabaseServicePhoto(con As SqlConnection, serviceID As Integer, photoNumber As Integer, pictureStream As Byte()) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spServicePhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                Dim param0 As SqlParameter = .Parameters.Add("@mode", SqlDbType.Char)
                param0.Value = "INS"
                Dim param1 As SqlParameter = .Parameters.Add("@serviceID", SqlDbType.Int)
                param1.Value = serviceID
                Dim param2 As SqlParameter = .Parameters.Add("@photoNumber", SqlDbType.Int)
                param2.Value = photoNumber
                Dim param3 As SqlParameter = .Parameters.Add("@photo", SqlDbType.VarBinary)
                param3.Value = pictureStream
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DownloadRoomPhoto(con As SqlConnection, roomTypeID As Integer, photoNumber As Integer) As PictureFile
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim empPic As Byte() = Nothing
        Dim seq As Long = 0

        Try

            With cmd
                .CommandText = "spRoomPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@roomtype_id", roomTypeID)
                .Parameters.AddWithValue("@photoNumber", photoNumber)
            End With

            Dim dr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If dr.Read() Then
                seq = dr.GetBytes(0, 0, Nothing, 0, Integer.MaxValue) - 1
                empPic = New Byte(seq) {}
                dr.GetBytes(0, 0, empPic, 0, Convert.ToInt32(seq))
            End If

            Return New PictureFile() With {.PictureStream = empPic, .RoomTypeID = roomTypeID, .PictureNumber = photoNumber}

        Catch EX1 As Exception
            Return Nothing
        End Try
    End Function

    Friend Function DownloadCompanyPhoto(con As SqlConnection, companyCode As String, photoNumber As Integer) As PictureFile
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim empPic As Byte() = Nothing
        Dim seq As Long = 0

        Try

            With cmd
                .CommandText = "spCompanyPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@company_code", companyCode)
                .Parameters.AddWithValue("@photoNumber", photoNumber)
            End With

            Dim dr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If dr.Read() Then
                seq = dr.GetBytes(0, 0, Nothing, 0, Integer.MaxValue) - 1
                empPic = New Byte(seq) {}
                dr.GetBytes(0, 0, empPic, 0, Convert.ToInt32(seq))
            End If

            Return New PictureFile() With {.PictureStream = empPic, .CompanyCode = companyCode, .PictureNumber = photoNumber}

        Catch EX1 As Exception
            Return Nothing
        End Try
    End Function

    Friend Function DownloadServicePhoto(con As SqlConnection, ServiceID As Integer, photoNumber As Integer) As PictureFile
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim empPic As Byte() = Nothing
        Dim seq As Long = 0

        Try

            With cmd
                .CommandText = "spCompanyPhoto"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@serviceID", ServiceID)
                .Parameters.AddWithValue("@photoNumber", photoNumber)
            End With

            Dim dr As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            If dr.Read() Then
                seq = dr.GetBytes(0, 0, Nothing, 0, Integer.MaxValue) - 1
                empPic = New Byte(seq) {}
                dr.GetBytes(0, 0, empPic, 0, Convert.ToInt32(seq))
            End If

            Return New PictureFile() With {.PictureStream = empPic, .ServiceID = ServiceID, .PictureNumber = photoNumber}

        Catch EX1 As Exception
            Return Nothing
        End Try
    End Function

#End Region

End Class
