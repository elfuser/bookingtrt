﻿

Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions


Public Class PromotionalCodeException


#Region "Public Methods"

    Public Function GetPromoCodesExceptions(ByVal PromoID As Integer) As List(Of PromotionalCodeExceptionDTO)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of PromotionalCodeExceptionDTO)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodesExceptions(con, PromoID)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveExceptionPromoCode(objExc As PromotionalCodeExceptionDTO) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveExceptionPromoCode(con, objExc)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteExceptionPromoCode(ByVal PromoExc_id As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteExceptionPromoCode(con, PromoExc_id)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function InsertExceptionWeekends(ByVal PromoID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = InsertExceptionWeekends(con, PromoID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteExceptionWeekends(ByVal PromoID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteExceptionWeekends(con, PromoID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region


#Region "Private Methods"

    Friend Function GetPromoCodesExceptions(con As SqlConnection, ByVal PromoID As Integer) As List(Of PromotionalCodeExceptionDTO)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim ExcList As New List(Of PromotionalCodeExceptionDTO)()

            With cmd
                .CommandText = "sp_PromotionalCodeException"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@promo_id", PromoID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objExc As New PromotionalCodeExceptionDTO
                With objExc
                    .PromoExc_id = Val(reader.Item("promoExc_id") & "")
                    .Promo_id = Val(reader.Item("promo_id") & "")
                    If IsDate(reader.Item("end_date")) Then
                        .End_date = CDate(reader.Item("end_date"))
                        .UnFormatDate = .End_date.ToShortDateString()
                    End If
                End With


                ExcList.Add(objExc)

            End While

            reader.Close()

            Return ExcList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function SaveExceptionPromoCode(con As SqlConnection, objExc As PromotionalCodeExceptionDTO) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "sp_PromotionalCodeException"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "INS")
                .Parameters.AddWithValue("@promo_id", objExc.Promo_id)
                .Parameters.AddWithValue("@promoExc_id", objExc.PromoExc_id)
                .Parameters.AddWithValue("@end_date", objExc.End_date)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function DeleteExceptionPromoCode(con As SqlConnection, PromoExc_id As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "sp_PromotionalCodeException"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@promoExc_id", PromoExc_id)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function InsertExceptionWeekends(con As SqlConnection, PromoID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "sp_PromotionalCodeException"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "INW")
                .Parameters.AddWithValue("@promo_id", PromoID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function DeleteExceptionWeekends(con As SqlConnection, PromoID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "sp_PromotionalCodeException"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEW")
                .Parameters.AddWithValue("@promo_id", PromoID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region


End Class
