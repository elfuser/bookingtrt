﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String

Public Class Dashboard
    Private sObjQuery As Object

#Region "Public Methods"

    Public Function DashSalesToday(ByVal DateFrom As Date, ByVal DateTo As Date,
                                   ByVal CompanyCode As String) As List(Of clsDashSalesToday)
        Try
            Dim dataHelper As New DataHelper()
            Dim SalesList As New List(Of clsDashSalesToday)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                SalesList = DashSalesToday(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DashSalesByDay(ByVal DateFrom As Date,
                                    ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashSalesByDay)
        Try
            Dim dataHelper As New DataHelper()
            Dim SalesList As New List(Of clsDashSalesByDay)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                SalesList = DashSalesByDay(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DashSalesByMonth(iYear As Integer, ByVal CompanyCode As String) As List(Of clsDashSalesByMonth)
        Try
            Dim dataHelper As New DataHelper()
            Dim SalesList As New List(Of clsDashSalesByMonth)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                SalesList = DashSalesByMonth(con, iYear, CompanyCode)
            End Using
            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DashSalesRoomsQty(ByVal DateFrom As Date,
                                ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashRoomsQty)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomList As New List(Of clsDashRoomsQty)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomList = DashSalesRoomsQty(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return RoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DashSalesReservationsByDay(ByVal DateFrom As Date, ByVal DateTo As Date,
                                               ByVal CompanyCode As String) As List(Of clsDashSalesReservationsByDay)
        Try
            Dim dataHelper As New DataHelper()
            Dim SalesList As New List(Of clsDashSalesReservationsByDay)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                SalesList = DashSalesReservationsByDay(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DashSalesReservationsByCountry(ByVal DateFrom As Date,
                                                   ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashCountryQty)
        Try
            Dim dataHelper As New DataHelper()
            Dim SalesList As New List(Of clsDashCountryQty)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                SalesList = DashSalesReservationsByCountry(con, DateFrom, DateTo, CompanyCode)
            End Using
            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function DashSalesToday(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date,
                                   ByVal CompanyCode As String) As List(Of clsDashSalesToday)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim SalesList As New List(Of clsDashSalesToday)()
            Dim sObjSale As New clsDashSalesToday

            With cmd
                .CommandText = "spDashSalesToday"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@From", DateFrom)
                .Parameters.AddWithValue("@To", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashSalesToday
                With sObjSale
                    .guest = reader.Item("Guest") & ""
                    If IsNumeric(reader.Item("Amount")) Then
                        .amount = reader.Item("Amount")
                    End If
                End With
                SalesList.Add(sObjSale)

            End While
            reader.Close()

            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DashSalesByDay(con As SqlConnection, ByVal DateFrom As Date,
                                    ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashSalesByDay)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim SalesList As New List(Of clsDashSalesByDay)()
            Dim sObjSale As New clsDashSalesByDay

            With cmd
                .CommandText = "spDashSalesByDay"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@From", DateFrom)
                .Parameters.AddWithValue("@To", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashSalesByDay
                With sObjSale
                    If IsDate(reader.Item("Date")) Then
                        .ddate = CDate(reader.Item("Date"))
                    End If
                    If IsNumeric(reader.Item("Amount")) Then
                        .amount = reader.Item("Amount")
                    End If
                    .qty = Val(reader.Item("Quantity") & "")
                End With
                SalesList.Add(sObjSale)

            End While
            reader.Close()

            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DashSalesByMonth(con As SqlConnection, iYear As Integer, ByVal CompanyCode As String) As List(Of clsDashSalesByMonth)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim SalesList As New List(Of clsDashSalesByMonth)()
            Dim sObjSale As New clsDashSalesByMonth

            With cmd
                .CommandText = "spDashSalesByMonth"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@Year", iYear)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashSalesByMonth
                With sObjSale
                    Select Case Val(reader.Item("Month") & "")
                        Case 1
                            .dday = "Jan"
                        Case 2
                            .dday = "Feb"
                        Case 3
                            .dday = "Mar"
                        Case 4
                            .dday = "Apr"
                        Case 5
                            .dday = "May"
                        Case 6
                            .dday = "Jun"
                        Case 7
                            .dday = "Jul"
                        Case 8
                            .dday = "Aug"
                        Case 9
                            .dday = "Sep"
                        Case 10
                            .dday = "Oct"
                        Case 11
                            .dday = "Nov"
                        Case 12
                            .dday = "Dec"
                    End Select
                    If IsNumeric(reader.Item("Amount")) Then
                        .amount = reader.Item("Amount")
                    End If
                End With
                SalesList.Add(sObjSale)

            End While
            reader.Close()

            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DashSalesRoomsQty(con As SqlConnection, ByVal DateFrom As Date,
                                ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashRoomsQty)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RoomsList As New List(Of clsDashRoomsQty)()
            Dim sObjSale As New clsDashRoomsQty

            With cmd
                .CommandText = "spDashRoomsQty"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@From", DateFrom)
                .Parameters.AddWithValue("@To", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashRoomsQty
                With sObjSale
                    .qty = Val(reader.Item("quantity") & "")
                    .room = reader.Item("room") & ""
                End With
                RoomsList.Add(sObjSale)

            End While
            reader.Close()

            Return RoomsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DashSalesReservationsByDay(con As SqlConnection, ByVal DateFrom As Date, ByVal DateTo As Date,
                                               ByVal CompanyCode As String) As List(Of clsDashSalesReservationsByDay)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RoomsList As New List(Of clsDashSalesReservationsByDay)()
            Dim sObjSale As New clsDashSalesReservationsByDay

            With cmd
                .CommandText = "spDashReservationsByDay"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@From", DateFrom)
                .Parameters.AddWithValue("@To", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashSalesReservationsByDay
                With sObjSale
                    If IsDate(reader.Item("Date")) Then
                        .ddate = CDate(reader.Item("Date"))
                    End If
                    .qty = Val(reader.Item("room") & "")
                End With
                RoomsList.Add(sObjSale)

            End While
            reader.Close()

            Return RoomsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DashSalesReservationsByCountry(con As SqlConnection, ByVal DateFrom As Date,
                                                   ByVal DateTo As Date, ByVal CompanyCode As String) As List(Of clsDashCountryQty)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim SalesList As New List(Of clsDashCountryQty)()
            Dim sObjSale As New clsDashCountryQty

            With cmd
                .CommandText = "spDashReservationsByDay"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@From", DateFrom)
                .Parameters.AddWithValue("@To", DateTo)
                .Parameters.AddWithValue("@Company_code", CompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sObjSale = New clsDashCountryQty
                With sObjSale
                    .country = reader.Item("country") & ""
                    .qty = Val(reader.Item("room") & "")
                End With
                SalesList.Add(sObjSale)

            End While
            reader.Close()

            Return SalesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
