﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String

Public Class Rooms

#Region "Public Methods"

    Public Function GetRoomsAvailable(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer) As List(Of RoomsAvailable)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsAvailableList As New List(Of RoomsAvailable)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsAvailableList = GetRoomsAvailable(con, sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children, "en-US")
            End Using
            Return RoomsAvailableList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomsAvailableDetail(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, ByVal RoomTypeCode As String) As List(Of RoomsAvailable)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsAvailableList As New List(Of RoomsAvailable)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsAvailableList = GetRoomsAvailableDetail(con, sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children, RoomTypeCode, "en-US")
            End Using
            Return RoomsAvailableList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCombinations(ByVal companyCode As String, ByVal adults1 As Integer, ByVal children1 As Integer,
                                    ByVal adults2 As Integer, ByVal children2 As Integer,
                                    ByVal adults3 As Integer, ByVal children3 As Integer,
                                    ByVal adults4 As Integer, ByVal children4 As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim Result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                Result = GetCombinations(con, companyCode, adults1, children1, adults2, children2, adults3, children3, adults4, children4)
            End Using
            Return Result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetCombination(ByVal RowID As Long, ByVal sCompanyCode As String) As List(Of CombinationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim CombinationList As New List(Of CombinationInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                CombinationList = GetCombination(con, RowID, sCompanyCode)
            End Using
            Return CombinationList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveCombinations(ByVal sMode As String, ByVal RowID As Long, ByVal CompanyCode As String, ByVal Adults As Integer, ByVal Children As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveCombinations(con, sMode, RowID, CompanyCode, Adults, Children)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRoomType(ByVal RoomTypeID As Integer, ByVal RoomTypeCode As String, ByVal CompanyCode As String, ByVal Description As String, ByVal Quantity As Integer,
                                 ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal RoomDetail As String, ByVal sMode As String, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer) As Long
        Try
            Dim dataHelper As New DataHelper()
            Dim response As Long

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRoomType(con, RoomTypeID, RoomTypeCode, CompanyCode, Description, Quantity, MaxAdults, MaxChildren, RoomDetail, sMode, MaxOccupance, MinOccupance)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRoomLocale(ByVal RoomTypeID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRoomLocale(con, RoomTypeID, CultureID, Description, LongDescription, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomType(ByVal iRoomID As Integer, ByVal sCompanyCode As String) As List(Of RoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsInfoList As New List(Of RoomsInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsInfoList = GetRoomType(con, iRoomID, sCompanyCode)
            End Using
            Return RoomsInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomLocale(ByVal iRoomTypeID As Integer, ByVal iCultureID As Integer) As List(Of RoomLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsInfoList As New List(Of RoomLocaleInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsInfoList = GetRoomLocale(con, iRoomTypeID, iCultureID)
            End Using
            Return RoomsInfoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRoomType(ByVal RoomTypeID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRoomType(con, RoomTypeID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveRoomBlocked(sMode As String, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                 ByVal Quantity As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveRoomBlocked(con, sMode, CompanyCode, RoomTypeCode, StartDate, EndDate, Quantity)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String) As List(Of RoomBlocked)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsList As New List(Of RoomBlocked)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsList = GetRoomBlocked(con, sRoomTypeCode, sCompanyCode)
            End Using
            Return RoomsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteRoomBlocked(ByVal sRoomTypeCode As String, ByVal sCompanyCode As String, StartDate As Date, EndDate As Date, Qty As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteRoomBlocked(con, sRoomTypeCode, sCompanyCode, StartDate, EndDate, Qty)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveAvailabilityRoom(RoomTypeCode As String, CompanyCode As String, dDate As Date, Qty As Integer, QtyBlocked As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveAvailabilityRoom(con, RoomTypeCode, CompanyCode, dDate, Qty, QtyBlocked)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetReservationRooms(ByVal ReservationID As String) As List(Of ReservationsRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomsList As New List(Of ReservationsRoomInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomsList = GetReservationRooms(con, ReservationID)
            End Using
            Return RoomsList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveFeature(ByVal FeatureID As Integer, ByVal Description As String, ByVal CompanyCode As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveFeature(con, FeatureID, Description, CompanyCode, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetFeature(ByVal iFeatureID As Integer, ByVal sCompanyCode As String) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim FeatureList As New List(Of FeaturesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                FeatureList = GetFeature(con, iFeatureID, sCompanyCode)
            End Using
            Return FeatureList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveFeatureToRoom(ByVal iRoomTypeID As Integer, ByVal iFeatureID As Int16, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveFeatureToRoom(con, iRoomTypeID, iFeatureID, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveFeatureToRoom(roomFeatures As List(Of RoomFeature)) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                For Each feature In roomFeatures
                    If feature.FeatureID > 0 Then
                        response = SaveFeatureToRoom(con, feature.RoomTypeID, feature.FeatureID, "INS")
                    End If
                Next

            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeleteFeaturesRoom(RoomTypeId As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteFeaturesRoom(con, RoomTypeId)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetFeaturesFromRoom(iRoomTypeID As Integer) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim FeatureList As New List(Of FeaturesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                FeatureList = GetFeaturesFromRoom(con, iRoomTypeID)
            End Using
            Return FeatureList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function




    Public Function GetFeaturesFromRoom_v1(iRoomTypeID As Integer, company_code As String) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim FeatureList As New List(Of FeaturesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                FeatureList = GetFeaturesFromRoom_v1(con, iRoomTypeID, company_code)
            End Using
            Return FeatureList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetFeatureLocale(ByVal iFeatureID As Int16, ByVal iCultureID As Integer) As List(Of FeatureLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim FeatureList As New List(Of FeatureLocaleInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                FeatureList = GetFeatureLocale(con, iFeatureID, iCultureID)
            End Using
            Return FeatureList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SaveFeatureLocale(ByVal FeatureID As Long, ByVal CultureID As Integer, ByVal Description As String, CompanyCode As String, ByVal sMode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response

            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveFeatureLocale(con, FeatureID, CultureID, Description, CompanyCode, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetRoomsAvailableDetailSum(ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, RoomTypeCode As String) As List(Of RoomsAvailable)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of RoomsAvailable)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetRoomsAvailableDetailSum(con, sCompanyId, RateCode, ArrivalDate, DepartureDate, Adults, Children, RoomTypeCode)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetDayAvailabilityPerRoom(ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date, ByVal roomCode As String) As List(Of clsDayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsDayAvailability)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetDayAvailabilityPerRoom(con, company_code, arrival, departure, roomCode)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetFeatures(ByVal scompany_code As String, ByVal roomtype_code As String,
                                ByVal Culture As String) As List(Of clsRoomFeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsRoomFeaturesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetFeatures(con, scompany_code, roomtype_code, Culture)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function




    Public Function GetRoomTypes(ByVal roomtype_code As String, ByVal company_code As String, ByVal culture As String) As List(Of clsRoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsRoomsInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetRoomTypes(con, roomtype_code, company_code, culture)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomPerson(ByVal company_code As String, ByVal roomtype_code As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim result As String

            Using con As SqlConnection = dataHelper.OpenConnection()
                result = GetRoomPerson(con, company_code, roomtype_code)
            End Using
            Return result
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function CheckRoomAvailability(ByVal company_code As String, ByVal StartDate As Date,
                                          ByVal EndDate As Date, ByVal roomtype_code1 As String, ByVal roomtype_code2 As String,
                                          ByVal roomtype_code3 As String, ByVal roomtype_code4 As String) As List(Of clsCheckRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsCheckRoomInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = CheckRoomAvailability(con, company_code, StartDate, EndDate, roomtype_code1, roomtype_code2, roomtype_code3, roomtype_code4)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRoomTypes_V1(ByVal roomtype_code As String, ByVal company_code As String, ByVal culture As String) As List(Of clsRoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsRoomsInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetRoomTypes_V1(con, roomtype_code, company_code, culture)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetRoomsPaging(ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RoomList As New List(Of RoomsInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RoomList = GetRoomsPaging(con, sCompanyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return RoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetFeaturesPaging(ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsFeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of clsFeaturesInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetFeaturesPaging(con, sCompanyCode, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetRoomsPagingCount(ByVal sCompanyCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetRoomsPagingCount(con, sCompanyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Public Function GetFeaturesPagingCount(ByVal sCompanyCode As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetFeaturePagingCount(con, sCompanyCode, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Friend Function GetRoomsAvailable(con As SqlConnection, ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, ByVal Culture As String) As List(Of RoomsAvailable)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim iDays As Integer
            Dim RoomList As New List(Of RoomsAvailable)()

            With cmd
                .CommandText = "spGetAvailability"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("rate_code", RateCode)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", Adults)
                .Parameters.AddWithValue("children", Children)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            iDays = DepartureDate.Subtract(ArrivalDate.Date).TotalDays

            If iDays = 0 Then iDays = 1

            While reader.Read()

                Dim objRoom As New RoomsAvailable
                With objRoom
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    .ppRoomsAvailable = Val(reader.Item("rooms_avail") & "")
                    If IsNumeric(reader.Item("total")) And IsNumeric(reader.Item("FreeNights")) Then
                        .ppAdults = reader.Item("total") - (reader.Item("total") / iDays * reader.Item("FreeNights"))
                    End If
                    If IsNumeric(reader.Item("total")) Then
                        .ppUnitPrice = reader.Item("total") / iDays
                    End If
                    .ppRoomTypeID = Val(reader.Item("roomtype_id") & "")
                    .ppRoomDetails = reader.Item("room_detail") & ""
                    .ppMaxOccupance = Val(reader.Item("max_occupance") & "")
                    .ppMinOccupance = Val(reader.Item("min_occupance") & "")
                    If IsNumeric(reader.Item("discount")) Then
                        .ppDiscount = reader.Item("discount")
                    End If
                End With

                RoomList.Add(objRoom)

                objRoom = Nothing

            End While

            reader.Close()

            Return RoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetAvailabilityRooms(con As SqlConnection, ByVal sCompanyId As String, ByVal ArrivalDate As Date,
                                         ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer,
                                         ByVal Culture As String) As List(Of RoomsAvailable)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RoomList As New List(Of RoomsAvailable)()

            With cmd
                .CommandText = "spGetAvailabilityRooms"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", Adults)
                .Parameters.AddWithValue("children", Children)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim objRoom As New RoomsAvailable
                With objRoom
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    .ppRoomsAvailable = Val(reader.Item("rooms_avail") & "")
                    If IsNumeric(reader.Item("min_rate")) Then
                        .ppUnitPrice = reader.Item("min_rate")
                    End If
                    .ppRoomDetails = reader.Item("room_detail") & ""
                    .ppMaxOccupance = Val(reader.Item("max_occupance") & "")
                    .ppCantAdults = Val(reader.Item("max_adults"))
                    .ppCantChildren = Val(reader.Item("max_children"))

                End With

                RoomList.Add(objRoom)

                objRoom = Nothing

            End While

            reader.Close()

            Return RoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRoomsAvailableDetail(con As SqlConnection, ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, ByVal RoomTypeCode As String, ByVal Culture As String) As List(Of RoomsAvailable)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim RoomList As New List(Of RoomsAvailable)()

            With cmd
                .CommandText = "spGetAvailabilityDetail"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("rate_code", RateCode)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", Adults)
                .Parameters.AddWithValue("children", Children)
                .Parameters.AddWithValue("roomtype_code", RoomTypeCode)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim objRoom As New RoomsAvailable
                With objRoom
                    .ppDescription = reader.Item("description") & ""
                    If IsDate(reader.Item("date")) Then
                        .ppDate = reader.Item("date")
                    End If
                    If IsNumeric(reader.Item("amount")) Then
                        .ppAdults = reader.Item("amount")
                    End If
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                    .ppRoomTypeID = Val(reader.Item("roomtype_id") & "")
                End With
                RoomList.Add(objRoom)
                objRoom = Nothing

            End While

            reader.Close()

            Return RoomList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCombinations(con As SqlConnection, ByVal companyCode As String, ByVal adults1 As Integer, ByVal children1 As Integer,
                                    ByVal adults2 As Integer, ByVal children2 As Integer,
                                    ByVal adults3 As Integer, ByVal children3 As Integer,
                                    ByVal adults4 As Integer, ByVal children4 As Integer) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim strResult As New StringBuilder()

            With cmd
                .CommandText = "spGetCombinationsRoom"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", companyCode)
                .Parameters.AddWithValue("adults1", adults1)
                .Parameters.AddWithValue("children1", children1)
                .Parameters.AddWithValue("adults2", adults2)
                .Parameters.AddWithValue("children2", children2)
                .Parameters.AddWithValue("adults3", adults3)
                .Parameters.AddWithValue("children3", children3)
                .Parameters.AddWithValue("adults4", adults4)
                .Parameters.AddWithValue("children4", children4)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                strResult.Append(reader.Item("Comb1").ToString)
                strResult.Append(",")
                strResult.Append(reader.Item("Comb2").ToString)
                strResult.Append(",")
                strResult.Append(reader.Item("Comb3").ToString)
                strResult.Append(",")
                strResult.Append(reader.Item("Comb4").ToString)

            End While

            reader.Close()

            Return strResult.ToString
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCombination(con As SqlConnection,
                                   ByVal RowID As Long, ByVal sCompanyCode As String) As List(Of CombinationInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim Combinations As New List(Of CombinationInfo)()

            With cmd
                .CommandText = "spCombination"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@RowID", RowID)
                .Parameters.AddWithValue("@companyCode", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjComb As New CombinationInfo
                With sObjComb
                    .ppRowID = Val(reader.Item("RowID") & "")
                    .ppAdults = Val(reader.Item("adults") & "")
                    .ppChildren = Val(reader.Item("children") & "")
                    .ppCompanyCode = sCompanyCode
                End With
                Combinations.Add(sObjComb)
                sObjComb = Nothing

            End While

            reader.Close()

            Return Combinations

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveCombinations(con As SqlConnection, ByVal sMode As String, ByVal RowID As Long, ByVal CompanyCode As String, ByVal Adults As Integer, ByVal Children As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spCombination"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@RowID", RowID)
                .Parameters.AddWithValue("@companyCode", CompanyCode)
                .Parameters.AddWithValue("@adults", Adults)
                .Parameters.AddWithValue("@children", Children)
            End With

            Call DataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return Response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRoomType(con As SqlConnection, ByVal RoomTypeID As Integer, ByVal RoomTypeCode As String, ByVal CompanyCode As String, ByVal Description As String, ByVal Quantity As Integer,
                                 ByVal MaxAdults As Integer, ByVal MaxChildren As Integer, ByVal RoomDetail As String, ByVal sMode As String, ByVal MaxOccupance As Integer, ByVal MinOccupance As Integer) As Long

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim retorno As Object

        Try


            With cmd
                .CommandText = "spRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
                .Parameters.AddWithValue("@roomtype_code", RoomTypeCode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@quantity", Quantity)
                .Parameters.AddWithValue("@max_adults", MaxAdults)
                .Parameters.AddWithValue("@max_children", MaxChildren)
                .Parameters.AddWithValue("@max_occupance", MaxOccupance)
                .Parameters.AddWithValue("@min_occupance", MinOccupance)
                .Parameters.AddWithValue("@room_detail", RoomDetail)
            End With

            retorno = dataHelper.ExecuteScalar(cmd, con)


            Return CInt(retorno)

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRoomLocale(con As SqlConnection, ByVal RoomTypeID As Integer, ByVal CultureID As Integer, ByVal Description As String, ByVal LongDescription As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spRoomTypeLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
                .Parameters.AddWithValue("@CultureID", CultureID)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@room_detail", LongDescription)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRoomType(con As SqlConnection, ByVal iRoomID As Integer, ByVal sCompanyCode As String) As List(Of RoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim Rooms As New List(Of RoomsInfo)()

            With cmd
                .CommandText = "spRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@roomtype_id", iRoomID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjRoom As New RoomsInfo
                With sObjRoom
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("max_adults")) Then
                        .ppMax_Adults = reader.Item("max_adults")
                    End If

                    If IsNumeric(reader.Item("max_children")) Then
                        .ppMax_Children = reader.Item("max_children")
                    End If

                    If IsNumeric(reader.Item("quantity")) Then
                        .ppQuantity = reader.Item("quantity")
                    End If

                    .ppRoom_Detail = reader.Item("room_detail") & ""
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""

                    If IsNumeric(reader.Item("roomtype_id")) Then
                        .ppRoomTypeID = reader.Item("roomtype_id")
                    End If


                    If IsNumeric(reader.Item("max_occupance")) Then
                        .ppMaxOccupance = reader.Item("max_occupance")
                    End If


                    If IsNumeric(reader.Item("min_occupance")) Then
                        .ppMinOccupance = reader.Item("min_occupance")
                    End If

                End With
                Rooms.Add(sObjRoom)
                sObjRoom = Nothing

            End While

            reader.Close()

            Return Rooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetRoomsPaging(con As SqlConnection, ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of RoomsInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim Rooms As New List(Of RoomsInfo)()
            Dim sObjRoom As New RoomsInfo
            Dim dt As Date

            With cmd
                .CommandText = "spGetRoomsPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                sObjRoom = New RoomsInfo
                With sObjRoom
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("max_adults")) Then
                        .ppMax_Adults = reader.Item("max_adults")
                    End If

                    If IsNumeric(reader.Item("max_children")) Then
                        .ppMax_Children = reader.Item("max_children")
                    End If

                    If IsNumeric(reader.Item("quantity")) Then
                        .ppQuantity = reader.Item("quantity")
                    End If

                    .ppRoom_Detail = reader.Item("room_detail") & ""
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""

                    If IsNumeric(reader.Item("roomtype_id")) Then
                        .ppRoomTypeID = reader.Item("roomtype_id")
                    End If


                    If IsNumeric(reader.Item("max_occupance")) Then
                        .ppMaxOccupance = reader.Item("max_occupance")
                    End If


                    If IsNumeric(reader.Item("min_occupance")) Then
                        .ppMinOccupance = reader.Item("min_occupance")
                    End If

                End With
                Rooms.Add(sObjRoom)

            End While

            reader.Close()

            Return Rooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeaturesPaging(con As SqlConnection, ByVal sCompanyCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsFeaturesInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim features As New List(Of clsFeaturesInfo)()
            Dim ObjFeature As New clsFeaturesInfo
            Dim dt As Date

            With cmd
                .CommandText = "spGetFeaturesLocalePaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                ObjFeature = New clsFeaturesInfo
                With ObjFeature
                    .ppDescription = reader.Item("description") & ""
                    If IsNumeric(reader.Item("feature_id")) Then
                        .ppFeatureID = Val(reader.Item("feature_id"))
                    End If

                    If IsNumeric(reader.Item("CultureID")) Then
                        .CultureId = Val(reader.Item("CultureID"))
                    End If


                End With
                features.Add(ObjFeature)

            End While

            reader.Close()

            Return features

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRoomsPagingCount(con As SqlConnection, ByVal sCompanyCode As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetRoomsPagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeaturePagingCount(con As SqlConnection, ByVal sCompanyCode As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spFeaturesLocalePagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetRoomLocale(con As SqlConnection, ByVal iRoomTypeID As Integer, ByVal iCultureID As Integer) As List(Of RoomLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sObjRooms As New List(Of RoomLocaleInfo)()

            With cmd
                .CommandText = "spRoomTypeLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@roomtype_id", iRoomTypeID)
                .Parameters.AddWithValue("@CultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim objRoom As New RoomLocaleInfo
                With objRoom
                    .ppRoomTypeID = Val(reader.Item("roomtype_id") & "")
                    .ppCultureID = Val(reader.Item("cultureID") & "")
                    .ppDescription = reader.Item("description") & ""
                    .ppLongDescription = reader.Item("room_detail") & ""
                End With
                sObjRooms.Add(objRoom)
                objRoom = Nothing

            End While

            reader.Close()

            Return sObjRooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRoomType(con As SqlConnection, ByVal RoomTypeID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@roomtype_id", RoomTypeID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveRoomBlocked(con As SqlConnection, sMode As String, ByVal CompanyCode As String, ByVal RoomTypeCode As String, ByVal StartDate As Date, ByVal EndDate As Date,
                                 ByVal Quantity As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spRoomBlocked"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@roomtype_code", RoomTypeCode)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@quantity", Quantity)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRoomBlocked(con As SqlConnection, ByVal sRoomTypeCode As String, ByVal sCompanyCode As String) As List(Of RoomBlocked)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sObjRooms As New List(Of RoomBlocked)()

            With cmd
                .CommandText = "spRoomBlocked"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@roomtype_code", sRoomTypeCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjRoom As New RoomBlocked
                With sObjRoom
                    .ppCompanyCode = reader.Item("company_code") & ""
                    If IsDate(reader.Item("start_date")) Then
                        .ppStartDate = reader.Item("start_date")
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        .ppEndDate = reader.Item("end_date")
                    End If
                    .ppQuantity = Val(reader.Item("quantity") & "")
                    .ppRoomName = reader.Item("room_name") & ""
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                End With
                sObjRooms.Add(sObjRoom)
                sObjRoom = Nothing

            End While

            reader.Close()

            Return sObjRooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteRoomBlocked(con As SqlConnection, ByVal sRoomTypeCode As String, ByVal sCompanyCode As String, StartDate As Date, EndDate As Date, Qty As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spRoomBlocked"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@roomtype_code", sRoomTypeCode)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@quantity", Qty)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveAvailabilityRoom(con As SqlConnection, RoomTypeCode As String, CompanyCode As String, dDate As Date, Qty As Integer, QtyBlocked As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spAvailabilityRoom"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@roomtype_code", RoomTypeCode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@date", dDate)
                .Parameters.AddWithValue("@quantity", Qty)
                .Parameters.AddWithValue("@blocked", QtyBlocked)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetReservationRooms(con As SqlConnection, ByVal ReservationID As String) As List(Of ReservationsRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sObjRooms As New List(Of ReservationsRoomInfo)()

            With cmd
                .CommandText = "spReservationsRooms"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "RES")
                .Parameters.AddWithValue("@reservation_id", ReservationID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjRes As New ReservationsRoomInfo
                With sObjRes
                    .ppAdults = Val(reader.Item("adults") & "")
                    .ppChildren = Val(reader.Item("children") & "")
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                End With
                sObjRooms.Add(sObjRes)
                sObjRes = Nothing

            End While

            reader.Close()

            Return sObjRooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveFeature(con As SqlConnection, ByVal FeatureID As Integer, ByVal Description As String, ByVal CompanyCode As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spFeatures"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@feature_id", FeatureID)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@company_code", CompanyCode)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetFeature(con As SqlConnection, ByVal iFeatureID As Integer, ByVal sCompanyCode As String) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim FeatureList As New List(Of FeaturesInfo)()

            With cmd
                .CommandText = "spFeatures"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@feature_id", iFeatureID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjFeature As New FeaturesInfo
                With sObjFeature
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    .ppFeatureID = Val(reader.Item("feature_id") & "")
                End With
                FeatureList.Add(sObjFeature)
                sObjFeature = Nothing

            End While

            reader.Close()

            Return FeatureList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveFeatureToRoom(con As SqlConnection, ByVal iRoomTypeID As Integer, ByVal iFeatureID As Int16, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spFeaturesRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@roomtype_id", iRoomTypeID)
                .Parameters.AddWithValue("@feature_id", iFeatureID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function DeleteFeaturesRoom(con As SqlConnection, ByVal iRoomTypeID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spFeaturesRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DRP")
                .Parameters.AddWithValue("@roomtype_id", iRoomTypeID)
                .Parameters.AddWithValue("@feature_id", 0)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeaturesFromRoom(con As SqlConnection, ByVal iRoomTypeID As Integer) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim FeatureList As New List(Of FeaturesInfo)()

            With cmd
                .CommandText = "spFeaturesRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@roomtype_id", iRoomTypeID)
                .Parameters.AddWithValue("@feature_id", 0)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjFeature As New FeaturesInfo
                With sObjFeature
                    .ppDescription = reader.Item("description") & ""
                    .ppFeatureID = Val(reader.Item("feature_id") & "")
                End With
                FeatureList.Add(sObjFeature)
                sObjFeature = Nothing

            End While

            reader.Close()

            Return FeatureList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeaturesFromRoom_v1(con As SqlConnection, ByVal iRoomTypeID As Integer, company_code As String) As List(Of FeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim FeatureList As New List(Of FeaturesInfo)()

            With cmd
                .CommandText = "GetFeaturesByRoom"
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                .Parameters.AddWithValue("@roomtype_id", iRoomTypeID)
                .Parameters.AddWithValue("@company_code", company_code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim sObjFeature As New FeaturesInfo
                With sObjFeature
                    .ppDescription = reader.Item("description") & ""
                    .ppFeatureID = Val(reader.Item("feature_id") & "")
                    .pChecked = reader.Item("checked")
                End With
                FeatureList.Add(sObjFeature)
                sObjFeature = Nothing

            End While

            reader.Close()

            Return FeatureList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeatureLocale(con As SqlConnection, ByVal iFeatureID As Int16, ByVal iCultureID As Integer) As List(Of FeatureLocaleInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim FeatureList As New List(Of FeatureLocaleInfo)()

            With cmd
                .CommandText = "spFeatureLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@feature_id", iFeatureID)
                .Parameters.AddWithValue("@CultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim objFeature As New FeatureLocaleInfo
                With objFeature
                    .ppFeatureID = Val(reader.Item("feature_id") & "")
                    .ppCultureID = Val(reader.Item("cultureID") & "")
                    .ppDescription = reader.Item("description") & ""
                End With
                FeatureList.Add(objFeature)
                objFeature = Nothing

            End While

            reader.Close()

            Return FeatureList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveFeatureLocale(con As SqlConnection, ByVal FeatureID As Long, ByVal CultureID As Integer, ByVal Description As String, CompanyCode As String, ByVal sMode As String) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try


            With cmd
                .CommandText = "spFeatureLocale"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@feature_id", FeatureID)
                .Parameters.AddWithValue("@CultureID", CultureID)
                .Parameters.AddWithValue("@Description", Description)
                .Parameters.AddWithValue("@company_code", CompanyCode)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRoomsAvailableDetailSum(con As SqlConnection, ByVal sCompanyId As String, ByVal RateCode As String, ByVal ArrivalDate As Date, ByVal DepartureDate As Date, ByVal Adults As Integer, ByVal Children As Integer, RoomTypeCode As String) As List(Of RoomsAvailable)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objRoom As New RoomsAvailable
            Dim RoomList As New List(Of RoomsAvailable)()


            With cmd
                .CommandText = "spGetAvailabilityDetailSum"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyId)
                .Parameters.AddWithValue("rate_code", RateCode)
                .Parameters.AddWithValue("arrival", ArrivalDate.Date)
                .Parameters.AddWithValue("departure", DepartureDate.Date)
                .Parameters.AddWithValue("adults", Adults)
                .Parameters.AddWithValue("children", Children)
                .Parameters.AddWithValue("roomtype_code", RoomTypeCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoom = New RoomsAvailable
                With objRoom
                    .ppDescription = reader.Item("description")
                    .ppDate = reader.Item("date")
                    .ppAdults = reader.Item("amount")
                End With
                RoomList.Add(objRoom)
            End While

            reader.Close()

            Return RoomList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetDayAvailabilityPerRoom(con As SqlConnection, ByVal company_code As String, ByVal arrival As Date, ByVal departure As Date, ByVal roomCode As String) As List(Of clsDayAvailability)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objRoom As New clsDayAvailability
            Dim RoomList As New List(Of clsDayAvailability)()


            With cmd
                .CommandText = "spGetAvailabilityDayPerRoom"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("arrival", arrival)
                .Parameters.AddWithValue("departure", departure)
                .Parameters.AddWithValue("roomtype_code", roomCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoom = New clsDayAvailability
                With objRoom
                    If IsDate(reader.Item("Day")) Then
                        .ppDay = reader.Item("Day")
                    End If
                    .ppQty = Val(reader.Item("Qty") & "")
                End With
                RoomList.Add(objRoom)
            End While

            reader.Close()

            Return RoomList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetFeatures(con As SqlConnection, ByVal scompany_code As String, ByVal roomtype_code As String,
                                ByVal Culture As String) As List(Of clsRoomFeaturesInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objFeature As New clsRoomFeaturesInfo
            Dim list As New List(Of clsRoomFeaturesInfo)()


            With cmd
                .CommandText = "spGetRoomFeatures"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", scompany_code)
                .Parameters.AddWithValue("roomtype_code", roomtype_code)
                .Parameters.AddWithValue("culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objFeature = New clsRoomFeaturesInfo
                With objFeature
                    .ppDescription = reader.Item("description") & ""
                End With
                list.Add(objFeature)
            End While

            reader.Close()

            Return list

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetRoomTypes(con As SqlConnection, ByVal roomtype_code As String, ByVal company_code As String,
                                 ByVal culture As String) As List(Of clsRoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objRoom As New clsRoomsInfo
            Dim objRooms As New List(Of clsRoomsInfo)


            With cmd
                .CommandText = "spGetRoomTypes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("roomtype_code", roomtype_code)
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("culture", culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoom = New clsRoomsInfo
                With objRoom
                    .ppDescription = reader.Item("description") & ""
                    .ppRoom_Detail = reader.Item("room_detail") & ""
                End With
                objRooms.Add(objRoom)
            End While

            reader.Close()

            Return objRooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetRoomPerson(con As SqlConnection, ByVal company_code As String, ByVal roomtype_code As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim sResult As String = ""


            With cmd
                .CommandText = "spGetRoomPerson"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("roomtype_code", roomtype_code)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                sResult = reader.Item("adults") & "&" & reader.Item("children")
            End While

            reader.Close()

            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function CheckRoomAvailability(con As SqlConnection, ByVal company_code As String, ByVal StartDate As Date,
                                          ByVal EndDate As Date, ByVal roomtype_code1 As String, ByVal roomtype_code2 As String,
                                          ByVal roomtype_code3 As String, ByVal roomtype_code4 As String) As List(Of clsCheckRoomInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objDays As New List(Of clsCheckRoomInfo)
            Dim objDay As New clsCheckRoomInfo

            With cmd
                .CommandText = "spCheckRoomAvailability"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("CompanyCode", company_code)
                .Parameters.AddWithValue("StartDate", StartDate)
                .Parameters.AddWithValue("EndDate", EndDate)
                .Parameters.AddWithValue("RoomtypeCode1", roomtype_code1)
                .Parameters.AddWithValue("RoomtypeCode2", roomtype_code2)
                .Parameters.AddWithValue("RoomtypeCode3", roomtype_code3)
                .Parameters.AddWithValue("RoomtypeCode4", roomtype_code4)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objDay = New clsCheckRoomInfo
                With objDay
                    If IsDate(reader.Item("date")) Then
                        .ppDate = reader.Item("date")
                    End If
                    .ppDescription = reader.Item("description") & ""
                End With
                objDays.Add(objDay)
            End While

            reader.Close()

            Return objDays

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetRoomTypes_V1(con As SqlConnection, ByVal roomtype_code As String, ByVal company_code As String,
                                 ByVal culture As String) As List(Of clsRoomsInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objRoom As New clsRoomsInfo
            Dim objRooms As New List(Of clsRoomsInfo)


            With cmd
                .CommandText = "spGetRoomTypes_V1"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("roomtype_code", roomtype_code)
                .Parameters.AddWithValue("company_code", company_code)
                .Parameters.AddWithValue("culture", culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()
                objRoom = New clsRoomsInfo
                With objRoom
                    .ppRoomTypeID = Val(reader.Item("roomtype_id") & "")
                    .ppRoomTypeCode = reader.Item("roomtype_code") & ""
                    .ppDescription = reader.Item("description") & ""
                    .ppRoom_Detail = reader.Item("room_detail") & ""
                    .ppMaxOccupance = Val(reader.Item("max_occupance") & "")
                    .ppMax_Children = Val(reader.Item("max_children") & "")
                    .ppMax_Adults = Val(reader.Item("max_adults") & "")
                    If IsNumeric(reader.Item("Price")) Then
                        .Price = CDbl(reader.Item("Price"))
                    End If

                    .ImageUlr = "/Handlers/getPhoto.ashx?roomTypeID=" + .ppRoomTypeID.ToString + "&photoNumber=1"
                    .RateCode = reader.Item("rate_code")

                End With
                objRooms.Add(objRoom)
            End While

            reader.Close()

            Return objRooms

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
