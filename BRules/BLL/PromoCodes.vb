﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions


Public Class PromoCodes

#Region "Public Methods"

    Public Function GetPromoCodesPaging(ByVal sCompanyCode As String, ByVal PromoType As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsPromoCodeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodesPaging(con, sCompanyCode, PromoType, OrderBy, IsAccending, PageNumber, PageSize)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetPromotionalCodesPagingCount(ByVal sCompanyCode As String, ByVal PromoType As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetPromotionalCodesPagingCount(con, sCompanyCode, PromoType, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPromoCode(ByVal sCompanyCode As String, ByVal sPromoCode As String, ByVal sPromoType As String) As List(Of clsPromoCodeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCode(con, sCompanyCode, sPromoCode, sPromoType)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPromoCodes(ByVal iPromoID As Integer, ByVal sCompanyCode As String) As List(Of clsPromoCodeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodes(con, iPromoID, sCompanyCode)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPromoCodesxRate(ByVal sPromoCode As String) As List(Of clsPromoCodeRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodesxRate(con, sPromoCode)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetServicePromoCodesxRate(ByVal sPromoCode As String, ByVal Culture As String) As List(Of clsPromoCodeRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetServicePromoCodesxRate(con, sPromoCode, Culture)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPromoCodeAmount(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal sRateCode As String) As List(Of clsPromoCodeRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodeAmount(con, iPromoID, sPromoCode, sCompanyCode, sRateCode)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetPromoCodeAmountService(iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal iRateID As Integer) As List(Of clsPromoCodeRateInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim PromoList As New List(Of clsPromoCodeRateInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                PromoList = GetPromoCodeAmountService(con, iPromoID, sPromoCode, sCompanyCode, iRateID)
            End Using
            Return PromoList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SavePromocode(ByVal PromoID As Integer, ByVal PromoCode As String, ByVal Description As String, ByVal Type As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                  ByVal EndDate As Date, ByVal Active As Boolean, ByVal PromoType As String, Mode As String) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SavePromocode(con, PromoID, PromoCode, Description, Type, CompanyCode, StartDate, EndDate, Active, PromoType, Mode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function DeletePromoCode(ByVal PromoID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeletePromoCode(con, PromoID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SavePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SavePromoCodexRate(con, PromoID, RateID, Amount, "INS")
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function SavePromoCodexRateService(ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double, ByVal ppServiceID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SavePromoCodexRateService(con, PromoID, RateID, Amount, ppServiceID, "INS")
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeletePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeletePromoCodexRate(con, PromoID, RateID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteServicePromoCodexRate(ByVal PromoID As Integer, ByVal RateID As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteServicePromoCodexRate(con, PromoID, RateID)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region


#Region "Private Methods"

    Friend Function GetPromoCode(con As SqlConnection, ByVal sCompanyCode As String, ByVal sPromoCode As String, ByVal sPromoType As String) As List(Of clsPromoCodeInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeInfo)()
            Dim objRate As New clsPromoCodeInfo
            Dim dt As Date

            With cmd
                .CommandText = "spGetPromotionalCode"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("promo_code", sPromoCode)
                .Parameters.AddWithValue("promo_type", sPromoType)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                Dim objPC As New clsPromoCodeInfo
                With objPC
                    .ppDescription = reader.Item("description") & ""
                    .ppType = reader.Item("type") & ""
                    If IsDate(reader.Item("start_date")) Then
                        DateTime.TryParse(reader.Item("start_date"), dt)
                        .ppStartDate = dt
                        .UnFormatFrom = dt.ToShortDateString()
                    Else
                        .UnFormatFrom = ""
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        DateTime.TryParse(reader.Item("end_date"), dt)
                        .ppEndDate = dt
                        .UnFormatTo = dt.ToShortDateString()
                    Else
                        .UnFormatTo = ""
                    End If
                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppPromoCode = IIf(IsDBNull(reader.Item("promo_code")), String.Empty, reader.Item("promo_code").ToString.Trim)

                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetPromoCodes(con As SqlConnection, ByVal iPromoID As Integer, ByVal sCompanyCode As String) As List(Of clsPromoCodeInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeInfo)()
            Dim objRate As New clsPromoCodeInfo

            With cmd
                .CommandText = "spPromotionalCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@promo_id", iPromoID)
                .Parameters.AddWithValue("@company_code", sCompanyCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeInfo
                With objPC
                    .ppDescription = reader.Item("description") & ""
                    .ppType = reader.Item("type") & ""
                    If IsDate(reader.Item("start_date")) Then
                        .ppStartDate = reader.Item("start_date")
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        .ppEndDate = reader.Item("end_date")
                    End If
                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppPromoCode = reader.Item("promo_code")
                    .ppActive = reader.Item("active")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Friend Function GetPromoCodesPaging(con As SqlConnection, ByVal sCompanyCode As String, ByVal PromoType As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of clsPromoCodeInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeInfo)()
            Dim objRate As New clsPromoCodeInfo
            Dim dt As Date

            With cmd
                .CommandText = "spGetPromotionalCodesPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@company_code", sCompanyCode)
                .Parameters.AddWithValue("@promo_type", PromoType)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeInfo
                With objPC
                    .ppDescription = reader.Item("description") & ""
                    .ppType = reader.Item("type") & ""

                    If IsDate(reader.Item("start_date")) Then
                        DateTime.TryParse(reader.Item("start_date"), dt)
                        .ppStartDate = dt
                        .UnFormatFrom = dt.ToShortDateString()
                    Else
                        .UnFormatFrom = ""
                    End If

                    If IsDate(reader.Item("end_date")) Then
                        DateTime.TryParse(reader.Item("end_date"), dt)
                        .ppEndDate = dt
                        .UnFormatTo = dt.ToShortDateString()
                    Else
                        .UnFormatTo = ""
                    End If

                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppCompanyCode = reader.Item("company_code") & ""
                    .ppPromoCode = reader.Item("promo_code").ToString().Trim
                    .ppActive = reader.Item("active")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetPromotionalCodesPagingCount(con As SqlConnection, ByVal sCompanyCode As String, ByVal PromoType As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetPromotionalCodesPagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("promo_type", PromoType)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function



    Friend Function GetPromoCodesxRate(con As SqlConnection, ByVal iPromoID As Integer) As List(Of clsPromoCodeRateInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeRateInfo)()
            Dim objRate As New clsPromoCodeRateInfo

            With cmd
                .CommandText = "spRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@promo_id", iPromoID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeRateInfo
                With objPC
                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppDescription = reader.Item("description") & ""
                    .ppRateID = Val(reader.Item("rate_id") & "")
                    .ppAmount = reader.Item("amount")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetServicePromoCodesxRate(con As SqlConnection, ByVal iPromoID As Integer, ByVal Culture As String) As List(Of clsPromoCodeRateInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeRateInfo)()
            Dim objRate As New clsPromoCodeRateInfo

            With cmd
                .CommandText = "spServiceRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@promo_id", iPromoID)
                .Parameters.AddWithValue("@Culture", Culture)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeRateInfo
                With objPC
                    .ppPromoID = Val(reader.Item("promo_id") & "")
                    .ppDescription = reader.Item("description") & ""
                    .ppRateID = Val(reader.Item("rate_id") & "")
                    .ppAmount = reader.Item("amount")
                    .ppServiceID = reader.Item("ServiceID")
                    .ppServiceDesc = reader.Item("service_desc")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetPromoCodeAmount(con As SqlConnection, iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal sRateCode As String) As List(Of clsPromoCodeRateInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeRateInfo)()
            Dim objRate As New clsPromoCodeRateInfo

            With cmd
                .CommandText = "spGetPromotionalCodeAmount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("promo_id", iPromoID)
                .Parameters.AddWithValue("promo_code", sPromoCode)
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("rate_code", sRateCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeRateInfo
                With objPC
                    .ppRateID = Val(reader.Item("rate_id") & "")
                    .ppAmount = reader.Item("amount")
                    .ppType = reader.Item("type")
                    .ppPromoID = reader.Item("promo_id")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetPromoCodeAmountService(con As SqlConnection, iPromoID As Integer, ByVal sPromoCode As String, ByVal sCompanyCode As String, ByVal iRateID As Integer) As List(Of clsPromoCodeRateInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim objPCs As New List(Of clsPromoCodeRateInfo)()
            Dim objRate As New clsPromoCodeRateInfo

            With cmd
                .CommandText = "spGetPromotionalCodeAmountService"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("promo_id", iPromoID)
                .Parameters.AddWithValue("promo_code", sPromoCode)
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.AddWithValue("rate_id", iRateID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                Dim objPC As New clsPromoCodeRateInfo
                With objPC
                    .ppRateID = Val(reader.Item("rate_id") & "")
                    .ppAmount = reader.Item("amount")
                    .ppType = reader.Item("type")
                    .ppPromoID = reader.Item("promo_id")
                End With
                objPCs.Add(objPC)

            End While

            reader.Close()

            Return objPCs

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SavePromocode(con As SqlConnection, ByVal PromoID As Integer, ByVal PromoCode As String, ByVal Description As String, ByVal Type As String, ByVal CompanyCode As String, ByVal StartDate As Date,
                                  ByVal EndDate As Date, ByVal Active As Boolean, ByVal PromoType As String, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spPromotionalCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@promo_id", PromoID)
                .Parameters.AddWithValue("@promo_code", PromoCode)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@type", Type)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@start_date", StartDate)
                .Parameters.AddWithValue("@end_date", EndDate)
                .Parameters.AddWithValue("@active", Active)
                .Parameters.AddWithValue("@promo_type", PromoType)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeletePromoCode(con As SqlConnection, PromoID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spPromotionalCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@promo_id", PromoID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SavePromoCodexRate(con As SqlConnection, ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@promo_id", PromoID)
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@amount", Amount)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SavePromoCodexRateService(con As SqlConnection, ByVal PromoID As Integer, ByVal RateID As Integer, ByVal Amount As Double, ByVal ppServiceID As Integer, ByVal sMode As String) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Dim statusresult As String = "0"
        Try

            With cmd
                .CommandText = "spServiceRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", sMode)
                .Parameters.AddWithValue("@promo_id", PromoID)
                .Parameters.AddWithValue("@rate_id", RateID)
                .Parameters.AddWithValue("@service_id", ppServiceID)
                .Parameters.AddWithValue("@amount", Amount)
            End With

            If sMode = "INS" Then
                Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)
                While reader.Read()
                    statusresult = reader.Item("return_code")
                End While

                reader.Close()
            Else
                Call dataHelper.ExecuteNonQuery(cmd, con)
            End If

            response.Status = statusresult
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeletePromoCodexRate(con As SqlConnection, ByVal PromoID As Integer, ByVal RateID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@promo_id", PromoID)
                .Parameters.AddWithValue("@rate_id", RateID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteServicePromoCodexRate(con As SqlConnection, ByVal PromoID As Integer, ByVal RateID As Integer) As Response
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd
                .CommandText = "spServiceRatePromoCodes"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@promo_id", PromoID)
                .Parameters.AddWithValue("@rate_id", RateID)
            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"
            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
