﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.String
Imports System.Transactions

Public Class VirtualReceipts

#Region "Public Methods"

    Public Function GetCulture(ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim RatesList As New List(Of CultureInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                RatesList = GetCulture(con, iCultureID)
            End Using
            Return RatesList
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function GetVirtualReceipt(ByVal ReceiptCode As Integer) As List(Of VirtualReceiptInfo)
        Try
            Dim dataHelper As New DataHelper()
            Dim List As New List(Of VirtualReceiptInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                List = GetVirtualReceipt(con, ReceiptCode)
            End Using
            Return List
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetVirtualReceipt(con As SqlConnection, ByVal ReceiptCode As Integer) As List(Of VirtualReceiptInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim InfoList As New List(Of VirtualReceiptInfo)()
        Dim objInfo As New VirtualReceiptInfo
        Dim dtExpiry As New Date
        Dim dtCreated As New Date
        Try

            With cmd
                .CommandText = "spVirtualReceipts"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@receipt_code", ReceiptCode)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objInfo = New VirtualReceiptInfo
                With objInfo
                    .ReceiptCode = reader.Item("receipt_code")
                    .CompanyCode = reader.Item("company_code")
                    .ClientName = reader.Item("client_name")
                    .Country = reader.Item("country")
                    .Phone = IIf(IsDBNull(reader.Item("phone")), String.Empty, reader.Item("phone"))
                    .Email = reader.Item("email")
                    .Description = IIf(IsDBNull(reader.Item("description")), String.Empty, reader.Item("description"))
                    .Amount = reader.Item("amount")
                    .ReceiptPaid = IIf(IsDBNull(reader.Item("receipt_paid")), 0, reader.Item("receipt_paid"))

                    If IsDate(reader.Item("expiry")) Then
                        DateTime.TryParse(reader.Item("expiry"), dtExpiry)
                        .Expiry = dtExpiry
                        .FormatExpiry = dtExpiry.ToString("MM/dd/yyyy hh:mm tt")
                    Else
                        .FormatExpiry = ""
                    End If

                    If IsDate(reader.Item("created")) Then
                        DateTime.TryParse(reader.Item("created"), dtCreated)
                        .Created = dtCreated
                        .FormatCreated = dtCreated.ToShortDateString()
                    Else
                        .FormatCreated = ""
                    End If

                End With
                InfoList.Add(objInfo)

            End While

            reader.Close()

            Return InfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function SaveVirtualReceipt(ByVal ReceiptCode As Integer, ByVal CompanyCode As String, ByVal ClientName As String, ByVal Country As String, ByVal Phone As String,
                                       ByVal Email As String, ByVal Description As String, ByVal Amount As Double, ByVal ReceiptPaid As Boolean,
                                       ByVal Expiry As Date, ByVal sMode As String) As String
        Try
            Dim dataHelper As New DataHelper()
            Dim response As String = ""
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = SaveVirtualReceipt(con, ReceiptCode, CompanyCode, ClientName, Country, Phone, Email, Description,
                              Amount, ReceiptPaid, Expiry, Nothing, Nothing, sMode)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Public Function DeleteVirtualReceipt(ByVal VirtualReceiptId As Integer) As Response
        Try
            Dim dataHelper As New DataHelper()
            Dim response As New Response
            Using con As SqlConnection = dataHelper.OpenConnection()
                response = DeleteVirtualReceipt(con, VirtualReceiptId)
            End Using
            Return response
        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetVirtualReceiptPaging(ByVal sCompanyCode As String, culture As String, OrderBy As String,
                                   IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of VirtualReceiptInfo)

        Try
            Dim dataHelper As New DataHelper()
            Dim VirtualReceiptsList As New List(Of VirtualReceiptInfo)()

            Using con As SqlConnection = dataHelper.OpenConnection()
                VirtualReceiptsList = GetVirtualReceiptPaging(con, sCompanyCode, culture, OrderBy, IsAccending, PageNumber, PageSize)
            End Using

            Return VirtualReceiptsList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function


    Public Function GetVirtualReceiptsPagingCount(ByVal sCompanyCode As String, culture As String, PageSize As Integer) As Integer

        Try
            Dim dataHelper As New DataHelper()
            Dim count As Integer

            Using con As SqlConnection = dataHelper.OpenConnection()
                count = GetVirtualReceiptsPagingCount(con, sCompanyCode, culture, PageSize)
            End Using

            Return count

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function
#End Region

#Region "Private Methods"

    Friend Function GetVirtualReceiptPaging(con As SqlConnection, ByVal sCompanyCode As String, culture As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As List(Of VirtualReceiptInfo)
        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim VirtualReceiptInfoList As New List(Of VirtualReceiptInfo)()
        Dim objVirtualReceiptInfo As VirtualReceiptInfo
        Dim dtExpiry As New Date
        Dim dtCreated As New Date
        Try
            With cmd
                .CommandText = "spGetVirtualReceiptsPaging"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("culture", culture)
                .Parameters.AddWithValue("company_code", sCompanyCode)
                .Parameters.Add("OrderBy", SqlDbType.VarChar).Value = OrderBy
                .Parameters.Add("IsAccending", SqlDbType.Bit).Value = IsAccending
                .Parameters.Add("PageNumber", SqlDbType.Int).Value = PageNumber
                .Parameters.Add("PageSize", SqlDbType.Int).Value = PageSize
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)

            While reader.Read()

                objVirtualReceiptInfo = New VirtualReceiptInfo
                With objVirtualReceiptInfo
                    .ReceiptCode = reader.Item("receipt_code")
                    .CompanyCode = reader.Item("company_code")
                    .ClientName = reader.Item("client_name")
                    .Country = reader.Item("country")
                    .Phone = IIf(IsDBNull(reader.Item("phone")), String.Empty, reader.Item("phone"))
                    .Email = reader.Item("email")
                    .Description = IIf(IsDBNull(reader.Item("description")), String.Empty, reader.Item("description"))
                    .Amount = reader.Item("amount")
                    .ReceiptPaid = IIf(IsDBNull(reader.Item("receipt_paid")), 0, reader.Item("receipt_paid"))

                    If IsDate(reader.Item("expiry")) Then
                        DateTime.TryParse(reader.Item("expiry"), dtExpiry)
                        .Expiry = dtExpiry
                        .FormatExpiry = dtExpiry.ToString("MM/dd/yyyy hh:mm tt")
                    Else
                        .FormatExpiry = ""
                    End If

                    If IsDate(reader.Item("created")) Then
                        DateTime.TryParse(reader.Item("created"), dtCreated)
                        .Created = dtCreated
                        .FormatCreated = dtCreated.ToShortDateString()
                    Else
                        .FormatCreated = ""
                    End If


                End With
                VirtualReceiptInfoList.Add(objVirtualReceiptInfo)

            End While

            reader.Close()

            Return VirtualReceiptInfoList


        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetVirtualReceiptsPagingCount(con As SqlConnection, ByVal sCompanyCode As String, culture As String, PageSize As Integer) As Integer
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()


            With cmd
                .CommandText = "spGetVirtualReceiptsPagingCount"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("culture", culture)
                .Parameters.AddWithValue("company_code", sCompanyCode)
            End With

            Dim count As Object = dataHelper.ExecuteScalar(cmd, con)

            If PageSize = 0 Then PageSize = 1

            If Not (count Is Nothing) Then
                Dim TotalCount As Integer = DirectCast(count, Integer)
                Dim TotalPageCount As Integer = Math.Ceiling(TotalCount / PageSize)
                Return TotalPageCount
            End If

            Return 1

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function GetCulture(con As SqlConnection, ByVal iCultureID As Integer) As List(Of CultureInfo)
        Try


            Dim dataHelper As New DataHelper()
            Dim cmd As New SqlCommand()
            Dim CultureInfoList As New List(Of CultureInfo)()
            Dim objCultureInfo As New CultureInfo

            With cmd
                .CommandText = "spCulture"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "SEL")
                .Parameters.AddWithValue("@cultureID", iCultureID)
            End With

            Dim reader As SqlDataReader = dataHelper.ExecuteReader(cmd, con)


            While reader.Read()

                objCultureInfo = New CultureInfo

                With objCultureInfo
                    .ppCultureID = Val(reader.Item("CultureID") & "")
                    .ppCultureName = reader.Item("CultureName") & ""
                    .ppDisplayName = reader.Item("DisplayName") & ""
                End With
                CultureInfoList.Add(objCultureInfo)

            End While

            reader.Close()

            Return CultureInfoList

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function SaveVirtualReceipt(con As SqlConnection, ByVal ReceiptCode As Integer, ByVal CompanyCode As String, ByVal ClientName As String, ByVal Country As String, ByVal Phone As String,
                                       ByVal Email As String, ByVal Description As String, ByVal Amount As Double, ByVal ReceiptPaid As Boolean,
                                       ByVal Expiry As Date, ByVal GuestId As Int64, ByVal CardTransId As Integer, ByVal sMode As String) As String

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim sResult As String = ""
        Dim iRow As Integer

        Try

            With cmd
                .CommandText = "spVirtualReceipts"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@Mode", sMode)
                .Parameters.AddWithValue("@receipt_code", ReceiptCode)
                .Parameters.AddWithValue("@company_code", CompanyCode)
                .Parameters.AddWithValue("@client_name", ClientName)
                .Parameters.AddWithValue("@country", Country)
                .Parameters.AddWithValue("@phone", Phone)
                .Parameters.AddWithValue("@email", Email)
                .Parameters.AddWithValue("@description", Description)
                .Parameters.AddWithValue("@amount", Amount)
                .Parameters.AddWithValue("@receipt_paid", ReceiptPaid)

                If sMode <> "PAY" Then
                    .Parameters.AddWithValue("@expiry", Expiry)
                Else
                    .Parameters.AddWithValue("@guest_id", GuestId)
                    .Parameters.AddWithValue("@ccard_transid", CardTransId)
                End If

                If sMode = "INS" Then
                    iRow = CInt(.ExecuteScalar())
                    If iRow = -1 Then
                        Throw New Exception("Error saving virtual receipt.")
                    End If
                    sResult = iRow.ToString
                Else
                    .ExecuteNonQuery()
                    sResult = "UP"
                End If

            End With


            Return sResult

        Catch EX1 As Exception
            Throw New Exception(EX1.Message)
        End Try
    End Function

    Friend Function DeleteVirtualReceipt(con As SqlConnection, ByVal VirtualReceiptID As Integer) As Response

        Dim dataHelper As New DataHelper()
        Dim cmd As New SqlCommand()
        Dim response As New Response()
        Try

            With cmd

                .CommandText = "spVirtualReceipts"
                .CommandType = CommandType.StoredProcedure
                .Connection = con
                .Parameters.AddWithValue("@mode", "DEL")
                .Parameters.AddWithValue("@receipt_code", VirtualReceiptID)

            End With

            Call dataHelper.ExecuteNonQuery(cmd, con)

            response.Status = "0"
            response.Message = "Command executes OK"

            Return response

        Catch EX1 As Exception
            response.Status = "-1"
            response.Status = EX1.Message
            Throw New Exception(EX1.Message)
        End Try
    End Function

#End Region

End Class
