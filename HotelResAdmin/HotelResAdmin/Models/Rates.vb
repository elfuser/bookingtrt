﻿Public Class RatesHeader
    Public Property RateID As Integer
    Public Property RateCode As String
    Public Property Description As String
    Public Property CompanyCode As String
    Public Property startDate As Date
    Public Property endDate As Date
    Public Property minLos As Integer
    Public Property maxLos As Integer
    Public Property enabled As Boolean
    Public Property weekend As Boolean
    Public Property type As String
    Public Property rateDetail As String
    Public Property rowOrder As Integer
    Public Property freeNights As Integer
    Public Property conditions As String
End Class



Public Class RatesDiscount
    Public Property rowID As Integer
    Public Property type As Integer
    Public Property start_date As Date
    Public Property end_date As Date
    Public Property disc1 As Double
    Public Property disc2 As Double
    Public Property disc3 As Double
    Public Property disc4 As Double
    Public Property disc5 As Double
    Public Property disc6 As Double
    Public Property disc7 As Double
    Public Property disc8 As Double
    Public Property discount As Double
    Public Property rate_Code As String
    Public Property typeDesc As String
    Public Property dateFrom As Date
    Public Property dateTo As Date
End Class