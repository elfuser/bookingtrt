﻿

Imports System.Runtime.Serialization



Public Class AddonModel

    Public Property ppAddonID As Long
    Public Property ppCompanyCode As String
    Public Property ppDescription As String
    Public Property ppAmountP As Double
    Public Property ppAmount1 As Double
    Public Property ppAmount2 As Double
    Public Property ppAmount3 As Double
    Public Property ppAmount4 As Double
    Public Property ppAmount5 As Double
    Public Property ppAmount6 As Double
    Public Property ppAmount7 As Double
    Public Property ppAmount8 As Double
    Public Property ppAmount9 As Double
    Public Property ppAmount10 As Double
    Public Property ppAmount As Double
    Public Property ppEnabled As Boolean
    Public Property ppTax As Boolean
    Public Property ppQty As Integer
    Public Property ppRowOrder As Integer
    Public Property ppFrom As Date
    Public Property ppTo As Date
    Public Property ppLongDescription As String
    Public Property ppDuration As String
    Public Property ppDeparture As String
    Public Property ppAddonType As String
    Public Property CultureID As Integer

    Public Property UnFormatFrom As Date
    Public Property UnFormatTo As Date


End Class
