﻿

Imports System.Runtime.Serialization

Public Class VirtualReceiptModel

    Public Property ReceiptCode As Integer
    Public Property CompanyCode As String
    Public Property ClientName As String
    Public Property Country As String
    Public Property Phone As String
    Public Property Email As String
    Public Property Description As String
    Public Property Amount As Double
    Public Property ReceiptPaid As Boolean
    Public Property Expiry As Date
    Public Property FormatExpiry As Date
    Public Property CompanyName As String


End Class
