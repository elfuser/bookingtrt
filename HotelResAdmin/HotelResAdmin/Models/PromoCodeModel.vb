﻿Public Class PromoCodeModel
    Public Property ppPromoID As Long
    Public Property ppPromoCode As String
    Public Property ppActive As Boolean
    Public Property ppCompanyCode As String
    Public Property ppDescription As String
    Public Property ppEndDate As String
    Public Property ppStartDate As String
    Public Property ppType As String
    Public Property UnFormatFrom As Date
    Public Property UnFormatTo As Date
    Public Property ppPromoType As String
End Class


Public Class RatePromoCodeModel
    Public Property ppPromoID As Long
    Public Property ppRateID As Long
    Public Property ppAmount As Double
    Public Property ppPromoType As String
    Public Property ppServiceID As Integer
End Class

