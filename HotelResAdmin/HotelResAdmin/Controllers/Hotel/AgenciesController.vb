﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class AgenciesController
        Inherits Controller

        ' GET: Agencies
        <Authorize>
        Function Index() As ActionResult
            Return View("Agencies")
        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetAgencies(ByVal AgencyCode As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim objList As New List(Of AgencyInfo)
            Dim Json As String = ""

            Try

                'objList = cls.GetAgencies(AgencyCode, Utils.CompanyCode)
                objList = cls.GetAgenciesPaging(AgencyCode, Utils.CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(objList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetAgencieByCompany() As String
            Dim cls As New BRules.Agencies
            Dim objList As New List(Of AgencyInfo)
            Dim Json As String = ""

            Try

                objList = cls.GetAgencies(0, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(objList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAgenciesCount(AgencyCode As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim count As Integer

            Try

                count = cls.GetAgenciesCount(AgencyCode, Utils.CompanyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDetail(AgencyCode As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim List As New List(Of AgencyContractInfo)
            Dim Json As String = ""

            Try

                List = cls.GetAgencyContractsPaging(AgencyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAgencyContractData(AgencyContractCode As Integer) As String
            Dim cls As New BRules.Agencies
            Dim List As New List(Of AgencyContractInfo)
            Dim Json As String = ""

            Try

                List = cls.GetAgencyContractData(AgencyContractCode)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDetailCount(AgencyCode As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim count As Integer

            Try

                count = cls.GetAgencyContractsCount(AgencyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAgenciesTypes(ByVal AgencyTypeCode As Int16) As String
            Dim BL As New BRules.AgencyTypes()
            Dim list As New List(Of AgencyTypeInfo)
            Dim Json As String = ""

            Try
                list = BL.GetAgenciesTypes(AgencyTypeCode)

                Json = Utils.GetJsonFromObject(list)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveHeader(Agency As AgencyInfo) As String
            Dim cls As New BRules.Agencies
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try

                If Agency.ppAgencyCode = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With Agency
                    resp = cls.SaveAgency(.ppAgencyCode, Utils.CompanyCode, .ppAgencyName, .ppAgencyTypeCode, .ppCountry,
                              .ppProvince, .ppCanton, .ppDistrict, .ppAddress, .ppPhone, .ppContactName, .ppContactOccupation, .ppContactEmail,
                              .ppContactOfficePhone, .ppContactMobilePhone, .ppNotificationEmailReservations, .ppActive, .ppAvailableCredit, .NumTarjeta, .FecVencimiento, .CodSeguridad, .TipoTarjeta, sMode)
                End With



            Catch ex As Exception
                Return "-1"
            End Try
            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteHeader(AgencyCode As Integer) As String
            Dim clsRates As New BRules.Agencies
            Dim resp As New Response
            Try

                resp = clsRates.DeleteAgency(AgencyCode)

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveDetail(AgencyContract As AgencyContractInfo) As String
            Dim cls As New BRules.Agencies
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If AgencyContract.ppAgencyContractCode = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With AgencyContract
                    'resp = cls.SaveAgencyContract(.ppAgencyContractCode, .ppAgencyCode, .ppStart_date, .ppEnd_date, .ppNotes, sMode)
                    resp = cls.SaveAgencyContract(.ppAgencyContractCode, .ppAgencyCode, .UnFormatFrom, .UnFormatTo, .ppNotes, sMode)
                End With

            Catch ex As Exception
                Return "-1"
            End Try
            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteAgencyContractDetail(AgencyContract As AgencyContractInfo) As String
            Dim cls As New BRules.Agencies
            Dim resp As New Response

            Try

                With AgencyContract
                    resp = cls.DeleteAgencyContract(.ppAgencyContractCode)
                End With

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function


#Region "PromoCodes"
        <Authorize>
        <HttpGet()>
        Public Function GetActivePromoCodes(ByVal sPromoCode As String) As String
            Dim PromoCode As New BRules.PromoCodes
            Dim PromoList As New List(Of clsPromoCodeInfo)
            Dim Json As String = ""
            Dim objPromo As clsPromoCodeInfo
            Try
                If sPromoCode = String.Empty Then
                    sPromoCode = Nothing
                End If

                PromoList = PromoCode.GetPromoCode(Utils.CompanyCode, sPromoCode, "H")
                If PromoList IsNot Nothing Then
                    If PromoList.Count > 0 And sPromoCode = String.Empty Then
                        objPromo = New clsPromoCodeInfo
                        objPromo.ppPromoID = -1
                        objPromo.ppPromoCode = "Select a promotional code..."
                        PromoList.Insert(0, objPromo)
                    End If
                End If

                Json = Utils.GetJsonFromObject(PromoList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function RelatePromoCode(PromoAgencyInfo As clsPromoCodeInfo) As String
            Dim cls As New BRules.Agencies
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try

                With PromoAgencyInfo
                    resp = cls.RelatePromoCodeAgency(.ppAgencyCode, .ppPromoID)
                End With

            Catch ex As Exception
                Return "-1"
            End Try
            Return "0"

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDetailPromo(AgencyCode As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim List As New List(Of clsPromoCodeInfo)
            Dim Json As String = ""

            Try

                List = cls.GetAgencyPromosPaging(AgencyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAgencyPromoCount(AgencyCode As Integer, PageSize As Integer) As String
            Dim cls As New BRules.Agencies
            Dim count As Integer

            Try

                count = cls.GetAgencyPromoCount(AgencyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteAgencyPromoDetail(PromoAgencyInfo As clsPromoCodeInfo) As String
            Dim cls As New BRules.Agencies
            Dim resp As New Response

            Try

                With PromoAgencyInfo
                    resp = cls.DeleteAgencyPromoDetail(.ppAgencyCode, .ppPromoID)
                End With

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function
#End Region

    End Class
End Namespace