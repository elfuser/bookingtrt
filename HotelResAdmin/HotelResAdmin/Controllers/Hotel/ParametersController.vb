﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class ParametersController
        Inherits Controller

        Private TermsID As Integer

        ' GET: Parameters
        <Authorize>
        Function Index() As ActionResult
            Return View("Parameters")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCompany() As String
            Dim Company As New BRules.Companies
            Dim CompanyList As New List(Of CompanyInfo)()
            Dim Json As String = ""
            Dim path As String = ""

            Try

                CompanyList = Company.GetCompanyHotel(Utils.CompanyCode, "SEL")

                If Not CompanyList Is Nothing AndAlso CompanyList.Count > 0 Then

                    path = ConfigurationManager.AppSettings("CompanyUrl").ToString()

                    CompanyList(0).SpanishUrl = path & Utils.CompanyCode
                    CompanyList(0).EnglishUrl = path & Utils.CompanyCode & "&lang=es-CR"
                    Json = Utils.GetJsonFromObject(CompanyList(0))
                End If

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function Save(company As CompanyInfo) As String
            Dim clsCompany As New BRules.Companies
            Dim resp As New Response


            Try


                With company
                    resp = clsCompany.SaveCompanyHotel(Utils.CompanyCode, "UPD", .ppTaxPerc, .ppMaxAdults, .ppMaxChildren,
                                     .ppMsgAdults, .ppMsgChildren, .ppChildrenFree, .ppMsgChildrenFree, .ppChildrenNote, .ppConfirmPrefix)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function



        <Authorize>
        <HttpGet()>
        Public Function GetTermsAndConditions(CultureId As String) As String
            Dim objTerm As New BRules.Companies
            Dim terms As String = ""
            Dim Company As New CompanyInfo()

            Try


                terms = objTerm.GetCompanyTerm(Utils.CompanyCode, "SEL", CultureId)


                Dim sResult() As String = terms.Split("~")

                TermsID = CInt(sResult(0))
                    terms = sResult(1)


                    Company.ppTerms = terms
                    Company.TermsId = TermsID

                    terms = Utils.GetJsonFromObject(Company)



            Catch ex As Exception
                Throw ex
            End Try

            Return terms

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveTerms(company As CompanyInfo) As String
            Dim clsCompany As New BRules.Companies
            Dim resp As New Response
            Dim iTermID As Integer

            Try


                iTermID = clsCompany.SaveCompanyTerms(Utils.CompanyCode, "INS", company.TermsId, company.ppTerms, company.CultureId)

                TermsID = iTermID

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function

    End Class
End Namespace