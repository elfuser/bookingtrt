﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class PromoCodesController
        Inherits Controller

        ' GET: PromoCodes
        <Authorize>
        Function Index(ByVal promotype As String) As ActionResult
            ViewBag.promotype = promotype
            Return View("PromoCodes")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetPromoCodes(ByVal PromoType As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim PromoCode As New BRules.PromoCodes
            Dim PromoList As New List(Of clsPromoCodeInfo)
            Dim Json As String = ""

            Try
                If PromoType IsNot Nothing Then
                    If PromoType = String.Empty Then
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                PromoList = PromoCode.GetPromoCodesPaging(Utils.CompanyCode, PromoType, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(PromoList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetPromoCodesPagingCount(ByVal PromoType As String, PageSize As Integer) As String
            Dim PromoCode As New BRules.PromoCodes
            Dim count As Integer
            Dim Json As String = ""

            Try
                If PromoType IsNot Nothing Then
                    If PromoType = String.Empty Then
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                count = PromoCode.GetPromotionalCodesPagingCount(Utils.CompanyCode, PromoType, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRateTypes() As String
            Dim Rates As New BRules.Rates
            Dim RateList As New List(Of RatesInfo)
            Dim Json As String = ""

            Try

                RateList = Rates.GetRateHeader(0, Utils.CompanyCode, False)


                Json = Utils.GetJsonFromObject(RateList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetPromoCodesxRate(Promoid As String, ByVal PromoType As String, ByVal Culture As String) As String
            Dim PromoCode As New BRules.PromoCodes
            Dim RateList As New List(Of clsPromoCodeRateInfo)
            Dim Json As String = ""

            Try
                If PromoType IsNot Nothing Then
                    If PromoType = String.Empty Then
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                If PromoType = "H" Then
                    RateList = PromoCode.GetPromoCodesxRate(Promoid)
                Else
                    RateList = PromoCode.GetServicePromoCodesxRate(Promoid, Culture)
                End If



                Json = Utils.GetJsonFromObject(RateList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function Delete(PromoID As String) As String
            Dim PromoCode As New BRules.PromoCodes
            Dim resp As New BRules.Response

            Try

                resp = PromoCode.DeletePromoCode(PromoID)

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SavePromoCode(PromoCode As PromoCodeModel) As String
            Dim Promo As New BRules.PromoCodes
            Dim resp As New BRules.Response
            Dim PromoType As String = String.Empty
            Try

                If PromoCode.ppPromoType IsNot Nothing Then
                    If PromoCode.ppPromoType <> String.Empty Then
                        PromoType = PromoCode.ppPromoType
                    Else
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                If PromoCode.ppPromoID = 0 Then
                    resp = Promo.SavePromocode(0, PromoCode.ppPromoCode, PromoCode.ppDescription, PromoCode.ppType, Utils.CompanyCode, PromoCode.UnFormatFrom,
                                  PromoCode.UnFormatTo, PromoCode.ppActive, PromoType, "INS")
                Else
                    resp = Promo.SavePromocode(PromoCode.ppPromoID, PromoCode.ppPromoCode, PromoCode.ppDescription, PromoCode.ppType, Utils.CompanyCode, PromoCode.UnFormatFrom,
                                  PromoCode.UnFormatTo, PromoCode.ppActive, PromoType, "UPD")

                End If

                Return resp.Status
            Catch ex As Exception
                If ex.Message.Contains("CK_tblPromotionalCodes_enddate") Then
                    Return "-2"
                End If
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SavePromoCodexRate(Rate As RatePromoCodeModel) As String
            Dim Promo As New BRules.PromoCodes
            Dim resp As New BRules.Response
            Dim PromoType As String = String.Empty

            Try
                If Rate.ppPromoType IsNot Nothing Then
                    If Rate.ppPromoType <> String.Empty Then
                        PromoType = Rate.ppPromoType
                    Else
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                If PromoType = "H" Then
                    resp = Promo.SavePromoCodexRate(Rate.ppPromoID, Rate.ppRateID, Rate.ppAmount)
                Else
                    resp = Promo.SavePromoCodexRateService(Rate.ppPromoID, Rate.ppRateID, Rate.ppAmount, Rate.ppServiceID)
                End If

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeletePromoCodexRate(ppPromoID As String, ppRateID As String, ByVal PromoType As String) As String
            Dim Promo As New BRules.PromoCodes
            Dim resp As New BRules.Response

            Try
                If PromoType IsNot Nothing Then
                    If PromoType = String.Empty Then
                        PromoType = "H"
                    End If
                Else
                    PromoType = "H"
                End If

                If PromoType = "H" Then
                    resp = Promo.DeletePromoCodexRate(ppPromoID, ppRateID)
                Else
                    resp = Promo.DeleteServicePromoCodexRate(ppPromoID, ppRateID)
                End If



                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

#Region "Services"
        <Authorize>
        <HttpGet()>
        Public Function GetServices(ByVal Culture As String, ByVal Mode As String) As String
            Dim Services As New BRules.Service

            Dim ServiceList As New List(Of ServiceInfo)
            Dim objService As New ServiceInfo
            Dim Json As String = ""

            Try

                ServiceList = Services.GetServices(0, Utils.CompanyCode, Culture, Mode)
                If ServiceList IsNot Nothing Then
                    If ServiceList.Count > 0 Then
                        objService = New ServiceInfo
                        objService.ppServiceID = -1
                        objService.ppDescription = "Select a service ..."
                        ServiceList.Insert(0, objService)
                        objService = Nothing
                    End If
                End If

                Json = Utils.GetJsonFromObject(ServiceList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetExceptionPromoCodes(PromoID As Integer) As String
            Dim PromoCode As New BRules.PromotionalCodeException
            Dim List As New List(Of PromotionalCodeExceptionDTO)
            Dim Json As String = ""

            Try

                List = PromoCode.GetPromoCodesExceptions(PromoID)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SavePromoException(PromoID As Integer, DateExc As Date) As String
            Dim PromoCode As New BRules.PromotionalCodeException
            Dim resp As New BRules.Response
            Dim Exception As New PromotionalCodeExceptionDTO

            Try

                Exception.End_date = DateExc
                Exception.Promo_id = PromoID

                resp = PromoCode.SaveExceptionPromoCode(Exception)

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function DeletePromoException(PromoExcID As Integer) As String
            Dim PromoCode As New BRules.PromotionalCodeException
            Dim resp As New BRules.Response
            Try


                resp = PromoCode.DeleteExceptionPromoCode(PromoExcID)

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function InsertExceptionWeekends(PromoID As Integer) As String
            Dim PromoCode As New BRules.PromotionalCodeException
            Dim resp As New BRules.Response
            Try

                resp = PromoCode.InsertExceptionWeekends(PromoID)

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteExceptionWeekends(PromoID As Integer) As String
            Dim PromoCode As New BRules.PromotionalCodeException
            Dim resp As New BRules.Response
            Try

                resp = PromoCode.DeleteExceptionWeekends(PromoID)

                Return resp.Status
            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function
#End Region



    End Class
End Namespace