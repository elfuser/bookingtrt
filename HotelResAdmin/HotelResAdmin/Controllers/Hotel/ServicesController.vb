﻿Imports System.Web.Mvc
Imports System.Data.SqlClient
Imports System.String
Imports BRules


Namespace Controllers.Hotel
    Public Class ServicesController
        Inherits Controller

        <Authorize>
        Function Index() As ActionResult
            Return View("Services")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetServicesPaging(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim Services As New BRules.Service

            Dim ServiceList As New List(Of ServiceInfo)
            Dim Json As String = ""

            Try

                ServiceList = Services.GetServicesPaging(Utils.CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(ServiceList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <HttpGet()>
        Public Function GetServiceRates(ServiceId As Integer) As String
            Dim Services As New BRules.Service

            Dim RatesList As New List(Of ServiceRateInfo)
            Dim Json As String = ""

            Try

                RatesList = Services.GetServiceRates(0, ServiceId)

                Json = Utils.GetJsonFromObject(RatesList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetServicesPagingCount(PageSize As Integer) As String
            Dim Services As New BRules.Service
            Dim count As Integer
            Dim Json As String = ""

            Try

                count = Services.GetServicesPagingCount(Utils.CompanyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function


        <Authorize>
        <HttpPost()>
        Public Function DeleteService(ByVal ServiceId As Integer) As String

            Dim user As New BRules.Service
            Dim response As String

            Try

                response = user.DeleteService(ServiceId)


            Catch ex As Exception
                Throw ex
            End Try

            Return response


        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveService(Service As ServiceInfo) As String
            Dim Services As New BRules.Service
            Dim resp As String
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If Service.ppServiceID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With Service
                    resp = Services.SaveService(Utils.CompanyCode, .ppServiceID, .ppDescription, .ppDetails, .ppCategoryID,
                             sMode, .ppCode, .ppListOrder, .ppVideoURL, "en-US", .ppDifficulty, .ppControlInventory, .ppActive)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCategories() As String
            Dim Services As New BRules.Service

            Dim CategoryList As New List(Of CategoryInfo)
            Dim Json As String = ""

            Try

                CategoryList = Services.GetCategories("en-US")

                Json = Utils.GetJsonFromObject(CategoryList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetServiceConditions(ServiceId As Integer, Culture As String) As String
            Dim Services As New BRules.Service
            Dim Json As String = ""

            Try

                Json = Services.GetServiceConditions(ServiceId, Culture)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpPost()>
        Public Function SaveServiceConditions(ServiceId As Integer, CultureId As Integer, Conditions As String) As String
            Dim Services As New BRules.Service
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = Services.SaveServiceConditions(ServiceId, CultureId, Conditions)

                Json = Utils.GetJsonFromObject(resp)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetServiceBringInfo(ServiceId As Integer, CultureId As Integer) As String
            Dim Services As New BRules.Service
            Dim Json As String = ""

            Try

                Json = Services.GetServiceCondInfo(ServiceId, CultureId, "BringInfo")

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveServiceBringInfo(ServiceId As Integer, CultureId As Integer, BingInfo As String) As String
            Dim Services As New BRules.Service
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = Services.SaveServiceBringInfo(ServiceId, CultureId, BingInfo)

                Json = Utils.GetJsonFromObject(resp)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpPost()>
        Public Function SaveServiceRate(ServiceRate As ServiceRateInfo) As String
            Dim Services As New BRules.Service
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If ServiceRate.ppRateID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                Dim OldStartDate As Date = CDate("2000-01-01")
                Dim OldEndDate As Date = CDate("2000-01-01")

                With ServiceRate
                    resp = Services.SaveServiceRate(sMode, .ppRateID, .ppServiceID, .ppDescription,
                                     .UnFormatFrom, .UnFormatTo, OldStartDate, OldEndDate, .ppAdults, .ppChildren,
                                     .ppInfants, .ppStudents, .ppMaxAdults, .ppMaxChildren, .ppMaxInfants,
                                     .ppMaxStudents, .ppActive)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function DeleteServiceRate(ServiceRate As ServiceRateInfo) As String

            Dim user As New BRules.Service
            Dim response As String

            Try

                response = user.DeleteServiceRate(ServiceRate.ppRateID, ServiceRate.ppServiceID, ServiceRate.UnFormatFrom, ServiceRate.UnFormatTo)


            Catch ex As Exception
                Throw ex
            End Try

            Return response


        End Function


    End Class
End Namespace