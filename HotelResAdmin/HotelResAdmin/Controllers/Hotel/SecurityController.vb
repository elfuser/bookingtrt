﻿Imports System.Web.Mvc
Imports System.Security.Principal
Imports System.DirectoryServices.AccountManagement
Imports BRules

Namespace Controllers.Hotel
    Public Class SecurityController
        Inherits Controller

        Private UserID As Integer
        Private _ReturnUrl As String


        Function Users() As ActionResult
            Return View("Users")
        End Function

        ' GET: Security
        Function Index() As ActionResult
            Return View()
        End Function

        '<AllowAnonymous>
        Function Login(Optional ReturnUrl As String = "") As ActionResult
            TempData("ReturnUrl") = ReturnUrl
            TempData("PwdTryCount") = 5
            Return View()
        End Function

        <AllowAnonymous>
        <HttpPost()>
        Function DoLogin(userName As String, Password As String) As String

            Dim Json As String = ""
            Dim objReturn As New Response
            Dim ReturnUrl As String = TempData("ReturnUrl") & ""
            Dim LoginReturn As New LoginResponseModel

            objReturn = ValidateUser(userName, Password)

            LoginReturn.Status = objReturn.Status
            LoginReturn.Message = objReturn.Message

            If objReturn.Status = "0" Then

                FormsAuthentication.SetAuthCookie(userName, False)

                If (Url.IsLocalUrl(ReturnUrl)) Then
                    LoginReturn.ReturnUrl = ReturnUrl
                End If

            End If



            Json = Utils.GetJsonFromObject(LoginReturn)
            Return Json

        End Function

        <AllowAnonymous>
        <HttpPost()>
        Function DoLoginWindows() As String

            Dim usrWin As WindowsIdentity
            usrWin = WindowsIdentity.GetCurrent()

            Dim userSid As String = usrWin.Name


            Dim Json As String = ""
            Dim objReturn As New Response
            Dim ReturnUrl As String = TempData("ReturnUrl") & ""
            Dim LoginReturn As New LoginResponseModel

            objReturn = ValidateUserWindows(userSid)

            LoginReturn.Status = objReturn.Status
            LoginReturn.Message = objReturn.Message

            If objReturn.Status = "0" Then

                FormsAuthentication.SetAuthCookie(userSid, False)

                If (Url.IsLocalUrl(ReturnUrl)) Then
                    LoginReturn.ReturnUrl = ReturnUrl
                End If

            End If


            Json = Utils.GetJsonFromObject(LoginReturn)
            Return Json

        End Function

        <AllowAnonymous>
        <HttpGet()>
        Function GetLoginWindows() As String

            Dim usrWin As WindowsIdentity
            usrWin = WindowsIdentity.GetCurrent()

            Dim userSid As String = usrWin.Name


            Dim Json As String = ""
            Json = userSid
            Return Json

        End Function

        <AllowAnonymous>
        <HttpPost()>
        Function SignOut() As String

            Dim Json As String = ""
            Dim objReturn As New Response

            Utils.CompanyCode = Nothing 'Clean company code session variable

            FormsAuthentication.SignOut()
            Utils.UserObject = Nothing


            objReturn.Status = "0"

            Json = Utils.GetJsonFromObject(objReturn)
            Return Json

        End Function

        <AllowAnonymous>
        <HttpGet()>
        Function ChangePassword(userName As String, Password As String, NewPassword As String) As String
            Dim user As New BRules.Users
            Dim objReturn As New BRules.Response
            Dim Json As String
            Dim objUserResponse As New Response

            Try


                objUserResponse = ValidateUser(userName, Password)

                If objUserResponse.Status = "-1" Then
                    objReturn.Message = "Not valid credentials"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                If Not ValidatePwdPattern(NewPassword) Then
                    objReturn.Message = "Password is not valid: minimum 8 characters, at least 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character (Use @#$%^&+= only). Example: Temp@2014"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                If Not ValidLast5Passwords(UserID, NewPassword) Then
                    objReturn.Message = "The new password has been used in the last 5 changes, please change again"
                    objReturn.Status = "-1"
                    Json = Utils.GetJsonFromObject(objReturn)
                    Return Json
                End If

                NewPassword = user.encriptaclave(NewPassword)

                user.SaveLastPassword(UserID, Now, NewPassword)

            Catch ex As Exception
                objReturn.Message = "change password process failed"
                objReturn.Status = "-1"
                Json = Utils.GetJsonFromObject(objReturn)
                Return Json
            End Try


            objReturn.Message = "Login OK"
            objReturn.Status = "0"
            Json = Utils.GetJsonFromObject(objReturn)
            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetProfiles() As String
            Dim user As New BRules.Users
            Dim Profiles As New List(Of ProfileInfo)
            Dim Json As String = ""
            Dim Profile As New ProfileInfo

            Try

                Profiles = user.GetProfile(0, Utils.CompanyCode)

                For Each Profile In Profiles
                    Call GetProfilePermissions(Profile)
                Next


                Json = Utils.GetJsonFromObject(Profiles)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveProfile(Profile As ProfileInfo) As String
            Dim user As New BRules.Users
            Dim sMode As String = ""
            Dim ProfileId As Integer = 0
            Dim listPermissions As New List(Of ProfileInfo)

            Try

                If Profile.ppProfileID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                listPermissions = SetProfilePermissions(Profile)

                ProfileId = user.SaveProfile(Profile.ppProfileID, Profile.ppProfileName, Utils.CompanyCode, sMode)

                user.SaveProfilePermissions(listPermissions, 0, ProfileId, "INS")

            Catch ex As Exception
                Throw ex
            End Try

            Return ProfileId.ToString()

        End Function




        <Authorize>
        <HttpPost()>
        Public Function DeleteProfile(ProfileId As Integer) As String
            Dim user As New BRules.Users
            Dim result As Object

            Try

                result = user.DeleteProfile(ProfileId)

            Catch ex As Exception
                Throw ex
            End Try

            Return result.ToString

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetUsers(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsUsers As New BRules.Users
            Dim UserList As New List(Of UserInfo)
            Dim Json As String = ""

            Try

                UserList = clsUsers.GetUsersPaging(Utils.CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(UserList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetUsersPagingCount(PageSize As Integer) As String
            Dim clsUsers As New BRules.Users
            Dim count As Integer

            Try

                count = clsUsers.GetUsersPagingCount(Utils.CompanyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetUsersCompany() As String
            Dim clsUsers As New BRules.Users
            Dim UserList As New List(Of UserInfo)
            Dim Json As String = ""

            Try

                UserList = clsUsers.GetUser(0, Utils.CompanyCode, "CBO")

                Json = Utils.GetJsonFromObject(UserList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetUsersLogPagingCount(UserID As Integer, FromDt As Date, ToDt As Date, PageSize As Integer) As String
            Dim clsUsers As New BRules.Users
            Dim count As Integer

            Try

                count = clsUsers.GetUsersLogPagingCount(UserID, FromDt, ToDt, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetUserCompany(ByVal UserID As Integer) As String

            Dim user As New BRules.Users
            Dim UserCompanyInfoList As New List(Of UserCompanyInfo)()
            Dim HotelList As New List(Of UserCompanyInfo)
            Dim Json As String = ""
            Dim SetCompanyCodeInCombo As Boolean = False
            Try

                If UserID = -1 Then
                    UserID = Utils.UserObject.UserId
                    SetCompanyCodeInCombo = True
                End If

                UserCompanyInfoList = user.GetUserCompany(UserID)

                If (SetCompanyCodeInCombo And (Utils.CompanyCode = "" Or Utils.CompanyCode Is Nothing)) Then
                    Utils.CompanyCode = UserCompanyInfoList(0).ppCompanyCode
                End If

                Json = Utils.GetJsonFromObject(UserCompanyInfoList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveUser(User As UserInfo) As String
            Dim sMode As String = ""
            Dim clsUsers As New BRules.Users

            Try

                If User.ppUserID = 0 Then
                    sMode = "INS"
                    If User.ppisDomainUser = False Then


                        If Not ValidatePwdPattern(User.ppPassword) Then
                            Return "-1"
                        End If
                    End If
                Else
                    sMode = "UPD"
                End If


                clsUsers.SaveUser(Utils.CompanyCode, User.ppUserID, User.ppUserName, User.ppPassword, User.ppFirstName, User.ppLastName, User.ppProfileID, sMode, User.ppActive, User.SecondSurname, User.isAgencyUser, User.AgencyCode, User.ppisDomainUser)

            Catch ex As Exception
                Throw ex
            End Try

            Return ""

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveUserCompany(ByVal UserID As Integer, CompanyCode As String) As String

            Dim user As New BRules.Users
            Dim response As New Response

            Try

                response = user.SaveUserCompany(CompanyCode, UserID)


            Catch ex As Exception
                Throw ex
            End Try

            Return response.Status


        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteUserCompany(ByVal UserID As Integer, CompanyCode As String) As String

            Dim user As New BRules.Users
            Dim response As New Response

            Try

                response = user.DeleteUserCompany(CompanyCode, UserID)


            Catch ex As Exception
                Throw ex
            End Try

            Return response.Status


        End Function



        <Authorize>
        <HttpPost()>
        Public Function DeleteUser(ByVal UserID As Integer) As String

            Dim user As New BRules.Users
            Dim response As New Response

            Try

                response = user.DeleteUser(UserID)


            Catch ex As Exception
                Throw ex
            End Try

            Return response.Status


        End Function


        <Authorize>
        <HttpPost()>
        Public Function BlockUser(User As UserInfo) As String

            Dim ClsUser As New BRules.Users
            Dim response As String
            Dim resp As New Response

            Try

                If User.ppActive Then
                    resp = ClsUser.BlockUser(User.ppUserID, False)
                    SaveUserLog("The user " & User.ppUserID & " has been blocked", User.ppUserID)
                    response = "The user has been blocked"
                Else
                    resp = ClsUser.BlockUser(User.ppUserID, True)
                    SaveUserLog("The user " & User.ppUserID & " has been unblocked", User.ppUserID)
                    response = "The user has been unblocked"
                End If

            Catch ex As Exception
                Throw ex
            End Try

            Return response


        End Function




        <Authorize>
        <HttpPost()>
        Public Function ResetPasword(ByVal UserID As Integer, password As String) As String

            Dim user As New BRules.Users
            Dim response As New Response

            Try


                If Not ValidatePwdPattern(password) Then
                    Return "-1"
                End If

                response = user.SaveLastPassword(UserID, Now, password)

                SaveUserLog("The user " & UserID & " has changed the password", UserID)

            Catch ex As Exception
                Throw ex
            End Try

            Return response.Status


        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetUserLog(UserID As Integer, FromDt As Date, ToDt As Date, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsUsers As New BRules.Users
            Dim UserLogList As New List(Of UserLogInfo)
            Dim Json As String = ""

            Try

                UserLogList = clsUsers.GetUserLogPagin(UserID, FromDt, ToDt, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(UserLogList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetLoggedUser() As String
            Dim usr As String
            Dim Json As String = ""

            Try

                usr = "User: " + Utils.UserObject.UserName_Email ' +" - " + Utils.UserObject.UserName
                Json = usr

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetUserProfilePermissions(ByVal ProfileId As Integer) As String
            Dim user As New BRules.Users
            Dim Profiles As New List(Of ProfileInfo)
            Dim Json As String = ""
            Dim objProfile As New ProfileInfo
            Try

                Profiles = user.GetProfilePermissions(0, Utils.UserObject.ProfileId)

                For i = 0 To Profiles.Count - 1
                    With Profiles(i)
                        Select Case .ppControlTag.Trim
                            Case "1"
                                objProfile.Dashboard = .ppAllow
                            Case "2"
                                objProfile.Company = .ppAllow
                            Case "3"
                                objProfile.Rooms = .ppAllow
                            Case "4"
                                objProfile.Rates = .ppAllow
                            Case "5"
                                objProfile.Addons = .ppAllow
                            Case "6.1"
                                objProfile.Services = .ppAllow
                            Case "7.1"
                                objProfile.Reservations = .ppAllow
                            Case "7.2"
                                objProfile.Avaibility = .ppAllow
                            Case "7.3"
                                objProfile.Comissions = .ppAllow
                            Case "8.1"
                                objProfile.ReportsReserv = .ppAllow
                            Case "8.2"
                                objProfile.ReportSales = .ppAllow
                            Case "8.3"
                                objProfile.ReportGuest = .ppAllow
                            Case "9.1"
                                objProfile.Profiles = .ppAllow
                            Case "9.2"
                                objProfile.EventLog = .ppAllow
                        End Select
                    End With
                Next

                Json = Utils.GetJsonFromObject(objProfile)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        Private Function GetProfilePermissions(Profile As ProfileInfo) As String
            Dim user As New BRules.Users
            Dim Profiles As New List(Of ProfileInfo)
            Dim Json As String = ""

            Try


                Profiles = user.GetProfilePermissions(0, Profile.ppProfileID)

                For i = 0 To Profiles.Count - 1
                    With Profiles(i)
                        Select Case .ppControlTag.Trim
                            Case "1"
                                Profile.Dashboard = .ppAllow
                            Case "2"
                                Profile.Company = .ppAllow
                            Case "3"
                                Profile.Rooms = .ppAllow
                            Case "4"
                                Profile.Rates = .ppAllow
                            Case "5"
                                Profile.Addons = .ppAllow
                            Case "6.1"
                                Profile.Services = .ppAllow
                            Case "7.1"
                                Profile.Reservations = .ppAllow
                            Case "7.2"
                                Profile.Avaibility = .ppAllow
                            Case "7.3"
                                Profile.Comissions = .ppAllow
                            Case "8.1"
                                Profile.ReportsReserv = .ppAllow
                            Case "8.2"
                                Profile.ReportSales = .ppAllow
                            Case "8.3"
                                Profile.ReportGuest = .ppAllow
                            Case "9.1"
                                Profile.Profiles = .ppAllow
                            Case "9.2"
                                Profile.EventLog = .ppAllow
                        End Select
                    End With
                Next

                Json = Utils.GetJsonFromObject(Profile)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        Private Function SetProfilePermissions(Profile As ProfileInfo) As List(Of ProfileInfo)
            Dim user As New BRules.Users
            Dim AddProfile As New ProfileInfo
            Dim ProfileList As New List(Of ProfileInfo)

            Try

                AddProfile.ppControlTag = "1"
                AddProfile.ppAllow = Profile.Dashboard
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "2"
                AddProfile.ppAllow = Profile.Company
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "3"
                AddProfile.ppAllow = Profile.Rooms
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "4"
                AddProfile.ppAllow = Profile.Rates
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "5"
                AddProfile.ppAllow = Profile.Addons
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "6.1"
                AddProfile.ppAllow = Profile.Services
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "7.1"
                AddProfile.ppAllow = Profile.Reservations
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "7.2"
                AddProfile.ppAllow = Profile.Avaibility
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "7.3"
                AddProfile.ppAllow = Profile.Comissions
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "8.1"
                AddProfile.ppAllow = Profile.ReportsReserv
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "8.2"
                AddProfile.ppAllow = Profile.ReportSales
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "8.3"
                AddProfile.ppAllow = Profile.ReportGuest
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "9.1"
                AddProfile.ppAllow = Profile.Profiles
                ProfileList.Add(AddProfile)

                AddProfile = New ProfileInfo
                AddProfile.ppControlTag = "9.2"
                AddProfile.ppAllow = Profile.EventLog
                ProfileList.Add(AddProfile)

            Catch ex As Exception
                Throw ex
            End Try

            Return ProfileList

        End Function



        Private Function ValidateUser(userName As String, Password As String) As Response

            Dim user As New BRules.Users
            Dim sResult As String = ""
            Dim dtPwdVoid As String
            Dim strResult() As String
            Dim pwd As String
            Dim SuperUser As Boolean
            Dim Active As Boolean
            Dim ProfileID As Integer
            Dim ResturnUserName As String
            Dim mPwdTimes As Integer
            Dim objReturn As New BRules.Response
            Dim Permissions As New List(Of ProfileInfo)
            Dim objUser As New BRules.ResponseUser
            Dim isDomainUser As Boolean

            Try

                'Password = user.encriptaclave(Password)

                mPwdTimes = CInt(TempData("PwdTryCount"))

                sResult = user.LoginUser(userName)

                If sResult = String.Empty Then
                    objReturn.Message = "Not valid credentials"
                    objReturn.Status = "-1"
                    Return objReturn
                End If


                strResult = Split(sResult, "~")
                UserID = CInt(strResult(0))
                ResturnUserName = strResult(1)
                ProfileID = CInt(strResult(2))
                Active = CBool(strResult(3))
                SuperUser = CBool(strResult(4))
                pwd = strResult(5)
                dtPwdVoid = strResult(6)
                isDomainUser = CBool(strResult(9))

                If Active = False Then
                    SaveUserLog("Login user: blocked", UserID)
                    objReturn.Message = "Your user account is Blocked, please contact support"
                    objReturn.Status = "-1"
                    Return objReturn
                End If

                If isDomainUser = True Then
                    SaveUserLog("Login user: Windows Only", UserID)
                    objReturn.Message = "Your user account has been set for Windows Authentication. Use the Windows button"
                    objReturn.Status = "-1"
                    Return objReturn

                End If

                objUser.UserName = ResturnUserName
                objUser.UserName_Email = userName
                objUser.UserId = UserID
                objUser.ProfileId = ProfileID
                Utils.UserObject = objUser


                'wfallas delete this line-------------
                '' Password = pwd
                '-------------------------------------

                Password = Password.ToLower()

                If Password <> pwd Then
                    If mPwdTimes = 1 Then
                        user.BlockUser(UserID, False)
                        SaveUserLog("User has been blocked", UserID)
                        objReturn.Message = "Your account has been blocked, please contact support"
                        objReturn.Status = "-1"
                        Return objReturn
                    Else
                        mPwdTimes -= 1
                        TempData("PwdTryCount") = mPwdTimes
                        SaveUserLog("Login user: incorrect password entered", UserID)
                        objReturn.Message = "The password is invalid, please try again (you have " & mPwdTimes.ToString & " time(s) left before the account is blocked)"
                        objReturn.Status = "-1"
                        Return objReturn
                    End If
                End If


                If dtPwdVoid = "1" Then
                    objReturn.Message = "You have to change your password"
                    objReturn.Status = "1"
                    Return objReturn
                End If


                SaveUserLog("User logged in", UserID)

                Permissions = user.GetProfilePermissions(0, ProfileID)

            Catch ex As Exception
                objReturn.Message = "Login process failed"
                objReturn.Status = "-1"
                Return objReturn
            End Try

            objReturn.Message = "Login OK"
            objReturn.Status = "0"
            Return objReturn
        End Function
        Private Function ValidateUserWindows(userName As String) As Response

            Dim user As New BRules.Users
            Dim sResult As String = ""
            Dim dtPwdVoid As String
            Dim strResult() As String
            Dim SuperUser As Boolean
            Dim Active As Boolean
            Dim ProfileID As Integer
            Dim ResturnUserName As String
            Dim objReturn As New BRules.Response
            Dim Permissions As New List(Of ProfileInfo)
            Dim objUser As New BRules.ResponseUser
            Dim isDomainUser As Boolean

            Try

                'Password = user.encriptaclave(Password)


                sResult = user.LoginUser(userName)

                If sResult = String.Empty Then
                    objReturn.Message = "Not valid credentials"
                    objReturn.Status = "-1"
                    Return objReturn
                End If


                strResult = Split(sResult, "~")
                UserID = CInt(strResult(0))
                ResturnUserName = strResult(1)
                ProfileID = CInt(strResult(2))
                Active = CBool(strResult(3))
                SuperUser = CBool(strResult(4))
                dtPwdVoid = strResult(6)
                isDomainUser = CBool(strResult(9))


                If Active = False Then
                    SaveUserLog("Login user: blocked", UserID)
                    objReturn.Message = "Your user account is Blocked, please contact support"
                    objReturn.Status = "-1"
                    Return objReturn
                End If
                If isDomainUser Then

                    objUser.UserName = ResturnUserName
                    objUser.UserName_Email = userName
                    objUser.UserId = UserID
                    objUser.ProfileId = ProfileID
                    Utils.UserObject = objUser

                    SaveUserLog("User logged in", UserID)

                    Permissions = user.GetProfilePermissions(0, ProfileID)
                End If
            Catch ex As Exception
                objReturn.Message = "Login process failed"
                objReturn.Status = "-1"
                Return objReturn
            End Try

            objReturn.Message = "Login OK"
            objReturn.Status = "0"
            Return objReturn
        End Function
        Private Function ValidLast5Passwords(userid As Integer, NewPassword As String) As Boolean
            Dim user As New BRules.Users
            Dim sResult As String = ""
            Dim strResult() As String

            sResult = user.Last5Passwords(userid)

            strResult = Split(sResult, "~")

            For Each pwd In strResult
                If pwd = NewPassword Then
                    Return False
                End If
            Next

            Return True
        End Function

        Private Function ValidatePwdPattern(NewPassword As String) As Boolean
            Dim bReturn As Boolean = True
            Dim pattern As String = "^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"
            If Not Regex.IsMatch(NewPassword, pattern) Then
                bReturn = False
            End If
            Return bReturn
        End Function

        Private Sub SaveUserLog(ByVal sEvent As String, ByVal iRefID As Long)
            Dim cslUsers As New BRules.Users
            Try
                cslUsers.SaveUserLog(Utils.UserObject.UserId, iRefID, sEvent)
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace