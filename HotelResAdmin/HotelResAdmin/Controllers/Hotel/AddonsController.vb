﻿Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization.Json
Imports BRules

Namespace Controllers.Hotel
    Public Class AddonsController
        Inherits Controller

        ' GET: Addons
        <Authorize>
        Function Index() As ActionResult
            Return View("Addons")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAddonsPaging(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim addons As New BRules.Addons
            Dim AddOnsList As New List(Of AddonInfo)
            Dim Json As String = ""

            Try

                AddOnsList = addons.GetAddonPaging(Utils.CompanyCode, Utils.Culture, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(AddOnsList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Delete(AddonId As Long) As String
            Dim addons As New BRules.Addons
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = addons.DeleteAddon(AddonId)

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveAddon(Addon As AddonModel) As String
            Dim addons As New BRules.Addons
            Dim resp As String
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If Addon.ppAddonID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With Addon
                    resp = addons.SaveAddon(.ppAddonID, .ppDescription, Utils.CompanyCode, .ppAmount, .ppEnabled, .ppRowOrder, sMode,
                              .ppLongDescription, .ppDeparture, .ppDuration, .ppAddonType,
                              .UnFormatFrom, .UnFormatTo, .ppAmount1, .ppAmount2, .ppAmount3, .ppAmount4,
                              .ppAmount5, .ppAmount6, .ppAmount7, .ppAmount8, .ppAmount9, .ppAmount10, .ppTax)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAddonsPagingCount(PageSize As Integer) As String
            Dim addons As New BRules.Addons
            Dim count As Integer
            Dim Json As String = ""

            Try

                count = addons.GetAddonPagingCount(Utils.CompanyCode, Utils.Culture, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetCultures() As String
            Dim addons As New BRules.Addons
            Dim cultures As New List(Of CultureInfo)
            Dim Json As String = ""

            Try

                cultures = addons.GetCulture(0)

                Json = Utils.GetJsonFromObject(cultures)


            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetAddonsTypes() As String
            Dim AddOnsTypeList As New List(Of AddOnTypeModel)
            Dim AddOnsType As New AddOnTypeModel
            Dim Json As String = ""

            Try

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "S"
                AddOnsType.description = "SPA"
                AddOnsTypeList.Add(AddOnsType)

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "T"
                AddOnsType.description = "Tours"
                AddOnsTypeList.Add(AddOnsType)

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "X"
                AddOnsType.description = "Transport"
                AddOnsTypeList.Add(AddOnsType)

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "O"
                AddOnsType.description = "Other"
                AddOnsTypeList.Add(AddOnsType)

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "M"
                AddOnsType.description = "Meals"
                AddOnsTypeList.Add(AddOnsType)

                AddOnsType = New AddOnTypeModel
                AddOnsType.code = "N"
                AddOnsType.description = "Extra Night Special"
                AddOnsTypeList.Add(AddOnsType)

                Json = Utils.GetJsonFromObject(AddOnsTypeList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



    End Class
End Namespace