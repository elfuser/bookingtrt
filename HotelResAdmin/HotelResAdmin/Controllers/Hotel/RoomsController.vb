﻿Imports System.Web.Mvc
Imports BRules


Namespace Controllers.Hotel
    Public Class RoomsController
        Inherits Controller

        ' GET: Rooms
        <Authorize>
        Function Index() As ActionResult
            Return View("Rooms")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRooms(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsRoom As New BRules.Rooms
            Dim RoomList As New List(Of RoomsInfo)
            Dim Json As String = ""

            Try

                RoomList = clsRoom.GetRoomsPaging(Utils.CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(RoomList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRoomsPagingCount(PageSize As Integer) As String
            Dim clsRoom As New BRules.Rooms
            Dim count As Integer

            Try

                count = clsRoom.GetRoomsPagingCount(Utils.CompanyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Save(Room As RoomsInfo) As String
            Dim clsRoom As New BRules.Rooms
            Dim resp As New Long
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If Room.ppRoomTypeID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With Room
                    resp = clsRoom.SaveRoomType(.ppRoomTypeID, .ppRoomTypeCode, Utils.CompanyCode, .ppDescription, .ppQuantity,
                              .ppMax_Adults, .ppMax_Children, .ppRoom_Detail, sMode,
                              .ppMaxOccupance, .ppMinOccupance)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Delete(RoomTypeId As Long) As String
            Dim clsRoom As New BRules.Rooms
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = clsRoom.DeleteRoomType(RoomTypeId)

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetFeatures(RoomTypeId As Integer) As String
            Dim clsRoom As New BRules.Rooms
            Dim FeatureList As New List(Of FeaturesInfo)
            Dim Json As String = ""

            Try

                FeatureList = clsRoom.GetFeaturesFromRoom_v1(RoomTypeId, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(FeatureList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveFeatures(FeatureList As List(Of RoomFeature)) As String
            Dim clsRoom As New BRules.Rooms
            Dim resp As New Response
            Dim Json As String = ""

            Try

                If Not FeatureList Is Nothing Then
                    resp = clsRoom.DeleteFeaturesRoom(FeatureList(0).RoomTypeID)

                End If


                If FeatureList.Count > 0 Then
                    resp = clsRoom.SaveFeatureToRoom(FeatureList)
                End If

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function

    End Class

End Namespace