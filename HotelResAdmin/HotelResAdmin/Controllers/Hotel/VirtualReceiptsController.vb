﻿Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization.Json
Imports BRules
Imports System.Net.Mail
Imports System.Net

Namespace Controllers.Hotel
    Public Class VirtualReceiptsController
        Inherits Controller

        ' GET: VirtualReceipts
        <Authorize>
        Function Index() As ActionResult
            Return View("VirtualReceipts")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetVirtualReceiptsPaging(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim VirtualReceipts As New BRules.VirtualReceipts
            Dim VirtualReceiptsList As New List(Of VirtualReceiptInfo)
            Dim Json As String = ""

            Try

                VirtualReceiptsList = VirtualReceipts.GetVirtualReceiptPaging(Utils.CompanyCode, Utils.Culture, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(VirtualReceiptsList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Delete(VirtualReceiptId As Integer) As String
            Dim VirtualReceipts As New BRules.VirtualReceipts
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = VirtualReceipts.DeleteVirtualReceipt(VirtualReceiptId)

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveVirtualReceipt(VirtualReceipt As VirtualReceiptModel) As String
            Dim VirtualReceipts As New BRules.VirtualReceipts
            Dim resp As String
            Dim Json As String = ""
            Dim sMode As String = ""
            Dim bSendLink As Boolean = False

            Try

                If VirtualReceipt.ReceiptCode = 0 Then
                    sMode = "INS"

                    With VirtualReceipt
                        resp = VirtualReceipts.SaveVirtualReceipt(.ReceiptCode, Utils.CompanyCode, .ClientName, .Country, .Phone, .Email, .Description,
                              .Amount, .ReceiptPaid, .FormatExpiry, sMode)
                    End With

                    If resp <> String.Empty Then
                        If IsNumeric(resp) Then
                            VirtualReceipt.ReceiptCode = CInt(resp)
                            bSendLink = SendLink(VirtualReceipt)
                            If Not bSendLink Then
                                'Delete receipt created
                                With VirtualReceipt
                                    resp = VirtualReceipts.SaveVirtualReceipt(CInt(resp), Utils.CompanyCode, .ClientName, .Country, .Phone, .Email, .Description,
                                    .Amount, .ReceiptPaid, .FormatExpiry, "DEL")
                                End With

                                Return "-2"
                            End If
                        End If
                    End If

                Else
                    sMode = "UPD"

                    With VirtualReceipt
                        resp = VirtualReceipts.SaveVirtualReceipt(.ReceiptCode, Utils.CompanyCode, .ClientName, .Country, .Phone, .Email, .Description,
                              .Amount, .ReceiptPaid, .FormatExpiry, sMode)
                    End With
                End If

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function GenerateReceiptLink(VirtualReceipt As VirtualReceiptModel) As Boolean
            Dim bSendLink As Boolean = False

            Try
                If VirtualReceipt.ReceiptCode <> 0 Then
                    bSendLink = SendLink(VirtualReceipt)
                End If

                Return bSendLink

            Catch ex As Exception
                'Throw ex
                Return False
            End Try

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetVirtualReceiptsPagingCount(PageSize As Integer) As String
            Dim VirtualReceipts As New BRules.VirtualReceipts
            Dim count As Integer
            Dim Json As String = ""

            Try

                count = VirtualReceipts.GetVirtualReceiptsPagingCount(Utils.CompanyCode, Utils.Culture, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCountries() As String
            Dim CountriesList As New List(Of CountriesInfo)
            Dim Json As String = ""

            Dim objDir As New Direcciones

            Try
                CountriesList = objDir.GetCountries()
                Json = Utils.GetJsonFromObject(CountriesList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCultures() As String
            Dim VirtualReceipts As New BRules.VirtualReceipts
            Dim cultures As New List(Of CultureInfo)
            Dim Json As String = ""

            Try

                cultures = VirtualReceipts.GetCulture(0)

                Json = Utils.GetJsonFromObject(cultures)


            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

#Region "Email"
        Private Function SendLink(ByVal VirtualReceipt As VirtualReceiptModel) As Boolean
            Dim strFrom As String = "noreply@bookingtrtinteractive.com"
            Dim strTo As String = VirtualReceipt.Email.Trim
            Dim mBody As StringBuilder = New StringBuilder("")
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            Dim Link As String = String.Empty
            Try
                Dim encriptedAddress As String = Utils.cryptURL("VirtualReceiptCheckout/VirtualReceiptCheckout?", "receiptcode=" & VirtualReceipt.ReceiptCode & "&companycode=" & Utils.CompanyCode)

                'Link = Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & encriptedAddress
                Link = ConfigurationManager.AppSettings("BookingWebDomain") & encriptedAddress

                MailMsg.BodyEncoding = Encoding.Default
                MailMsg.Subject = "Virtual Receipt / Confirm payment"

                mBody.Append("<h2>Virtual Receipt / Confirm payment</h2>")
                mBody.Append("<br />")
                mBody.Append("<h3>" & VirtualReceipt.CompanyName & "</h3>")
                mBody.Append("<ul>")
                mBody.Append("<li><b> Client Name:</b> " & VirtualReceipt.ClientName & "</li>")
                mBody.Append("<li><b> Description:</b> " & VirtualReceipt.Description & "</li>")
                mBody.Append("<li><b> Amount: $</b> " & VirtualReceipt.Amount & "</li>")
                mBody.Append("<li><b> Expiry:</b> " & VirtualReceipt.FormatExpiry & "</li>")
                mBody.Append("</ul>")
                mBody.Append("<br />")
                mBody.Append("<a href='" & Link & "' target='_blank'>Click here to complete your payment...</a>")

                MailMsg.Body = mBody.ToString
                MailMsg.Priority = MailPriority.Normal
                MailMsg.IsBodyHtml = True

                Dim SmtpMail As New SmtpClient
                SmtpMail.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SMTPUser"), ConfigurationManager.AppSettings("SMTPPassword"))
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
                SmtpMail.Host = ConfigurationManager.AppSettings("SMTPHost")
                SmtpMail.Port = 8889
                'SmtpMail.Port = 25                
                SmtpMail.Send(MailMsg)

                Return True

            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

    End Class
End Namespace