﻿Imports System.Web.Script.Serialization
Imports BRules

Namespace Controllers.Hotel
    Public Class CompaniesController
        Inherits Controller

        ' GET: Hotels
        <Authorize>
        Function Index() As ActionResult
            Return View("Companies")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCompaniesPaging(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim Companies As New BRules.Companies

            Dim HotelsList As New List(Of CompanyInfo)
            Dim Json As String = ""

            Try

                HotelsList = Companies.GetCompanyPaging(OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(HotelsList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Delete(HotelId As String) As String
            Dim Hotels As New BRules.Companies

            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = Hotels.DeleteCompany(HotelId)

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function AddHotel(Hotel As CompanyInfo) As String
            Dim Hotels As New BRules.Companies
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "INS"

            Try


                Dim result As String = Hotels.GetCompanyData(Hotel.ppCompanyCode)

                If result = "1" Then
                    Return "1"
                End If

                With Hotel
                    resp = Hotels.SaveCompany(.ppCompanyCode.Trim, .ppCompanyName.Trim, .ppLegalName, .ppLegalID, .ppAddress, .ppTel1, .ppTel2,
                              .ppTel3, .ppFax, .ppContactName, .ppContactEmail, .ppWeb, .ppEmail, .ppCallTollFree, sMode, .ppBankName,
                              .ppBankAccount, .ppCfoName, .ppCfoEmail, .ppResName, .ppResEmail, .ppResEmailAlt, .ppFacebookURL, .ppTwitterText,
                              .ppHotel, .ppServices, .ppGoogleID, .ppGoogleConvID, .ppGoogleConvLabel, .ppActive, Utils.UserObject.UserId,
                              .ppMigrar, .provincia, .canton, .distrito, .TechContact, .TechEmail, .ppPinterest, .ppInstagram, .ppBlog, .ppGooglePlus,
                              .ppThingsToDo, .ppGetThere, .ppLatitude, .ppLongitude)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function

        <Authorize>
        <HttpPost()>
        Public Function UpdateHotel(Hotel As CompanyInfo) As String
            Dim Hotels As New BRules.Companies
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "UPD"

            Try


                Dim result As String = Hotels.GetCompanyData(Hotel.ppCompanyCode)

                If result = "0" Then
                    Return "-1"
                End If

                With Hotel
                    resp = Hotels.SaveCompany(.ppCompanyCode.Trim, .ppCompanyName.Trim, .ppLegalName, .ppLegalID, .ppAddress, .ppTel1, .ppTel2,
                              .ppTel3, .ppFax, .ppContactName, .ppContactEmail, .ppWeb, .ppEmail, .ppCallTollFree, sMode, .ppBankName,
                              .ppBankAccount, .ppCfoName, .ppCfoEmail, .ppResName, .ppResEmail, .ppResEmailAlt, .ppFacebookURL, .ppTwitterText,
                              .ppHotel, .ppServices, .ppGoogleID, .ppGoogleConvID, .ppGoogleConvLabel, .ppActive,
                               Utils.UserObject.UserId, .ppMigrar, .provincia, .canton, .distrito, .TechContact, .TechEmail,
                              .ppPinterest, .ppInstagram, .ppBlog, .ppGooglePlus,
                              .ppThingsToDo, .ppGetThere, .ppLatitude, .ppLongitude)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-2"
            End Try

            Return resp.Status

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCompaniesPagingCount(PageSize As Integer) As String
            Dim Hotels As New BRules.Companies
            Dim count As Integer
            Dim Json As String = ""

            Try

                count = Hotels.GetCompanyPagingCount(PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function



        <Authorize>
        <HttpGet()>
        Public Function GetCompanyProcessor(CompanyId As String) As String
            Dim Companies As New BRules.Companies

            Dim HotelsList As New List(Of CompanyInfo)
            Dim Json As String = ""

            Try

                HotelsList = Companies.GetCompanyProcessor(CompanyId, "SEL")

                If HotelsList.Count > 0 Then
                    Json = Utils.GetJsonFromObject(HotelsList(0))
                End If


            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveHotelProcessor(Hotel As CompanyInfo) As String
            Dim Hotels As New BRules.Companies
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "INS"

            Try



                With Hotel
                    resp = Hotels.SaveCompanyProcessor(.ppCompanyCode, sMode, .ppCommission, .ppProcessorID, .ppKeyID, .ppKeyHash, .ppType, .ppURL, .ppUserName)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetCompanyHotel(CompanyId As String) As String
            Dim Companies As New BRules.Companies
            Dim HotelsList As New List(Of CompanyInfo)
            Dim Json As String = ""

            Try

                HotelsList = Companies.GetCompanyHotel(CompanyId, "SEL")

                If HotelsList.Count > 0 Then
                    Json = Utils.GetJsonFromObject(HotelsList(0))
                End If


            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetCompanyPrefix(Prefix As String) As String
            Dim Companies As New BRules.Companies
            Dim result As String = ""

            Try

                result = Companies.GetCompanyPrefix(Prefix)

            Catch ex As Exception
                Throw ex
            End Try

            Return result

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveHotelParameters(Hotel As CompanyInfo) As String
            Dim Hotels As New BRules.Companies
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "INS"

            Try

                With Hotel
                    resp = Hotels.SaveCompanyHotel(.ppCompanyCode, sMode, .ppTaxPerc, .ppMaxAdults, .ppMaxChildren, .ppMsgAdults, .ppMsgChildren, .ppChildrenFree, .ppMsgChildren, .ppChildrenNote, .ppConfirmPrefix)
                End With


            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetCombinations(CompanyID As String) As String
            Dim Rooms As New BRules.Rooms

            Dim CombinationList As New List(Of CombinationInfo)
            Dim Json As String = ""

            Try

                CombinationList = Rooms.GetCombination(0, CompanyID)

                Json = Utils.GetJsonFromObject(CombinationList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpGet()>
        Public Function GetCompanyServices(CompanyId As String) As String
            Dim Companies As New BRules.Companies
            Dim HotelsList As New List(Of CompanyInfo)
            Dim Json As String = ""

            Try

                HotelsList = Companies.GetCompanyService(CompanyId, "SEL")

                If HotelsList.Count > 0 Then
                    Json = Utils.GetJsonFromObject(HotelsList(0))
                End If


            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpPost()>
        Public Function SaveCompanyServices(Hotel As CompanyInfo) As String
            Dim Hotels As New BRules.Companies
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "INS"

            Try

                With Hotel
                    resp = Hotels.SaveCompanyService(.ppCompanyCode, sMode, .ppTaxPerc, .ppRentPorc,
                                                     .ppMaxAdults, .ppMaxChildren, .ppMaxStudent, .ppMsgAdults, .ppMsgChildren,
                                                     .ppMsgStudent, .ppChildrenFree, .ppMsgChildrenFree, .ppChildrenNote, .ppConfirmPrefix)
                End With


            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetCompanyServicePrefix(Prefix As String) As String
            Dim Companies As New BRules.Companies
            Dim result As String = ""

            Try

                result = Companies.GetCompanyServicePrefix(Prefix)

            Catch ex As Exception
                Throw ex
            End Try

            Return result

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetCompanies() As String
            Dim Companies As New BRules.Companies

            Dim HotelsList As New List(Of CompanyInfo)
            Dim Json As String = ""

            Try

                HotelsList = Companies.GetCompany("", "SEL", "")

                If Utils.CompanyCode = "" Or Utils.CompanyCode Is Nothing Then
                    Utils.CompanyCode = HotelsList(0).ppCompanyCode
                End If


                Json = Utils.GetJsonFromObject(HotelsList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SetSelectedCompany(CompanyId As String) As String


            Try

                Utils.CompanyCode = CompanyId

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetSelectedCompany() As String

            Return Utils.CompanyCode

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveCombination(combination As CombinationInfo) As String
            Dim Room As New BRules.Rooms
            Dim resp As New BRules.Response
            Dim Json As String = ""
            Dim sMode As String = "INS"

            Try

                If combination.ppRowID > 0 Then
                    sMode = "UPD"
                End If


                With combination
                    resp = Room.SaveCombinations(sMode, .ppRowID, .ppCompanyCode, .ppAdults, .ppChildren)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteCombination(combination As CombinationInfo) As String
            Dim Room As New BRules.Rooms
            Dim resp As New BRules.Response
            Dim Json As String = ""

            Try

                With combination
                    resp = Room.SaveCombinations("DEL", .ppRowID, .ppCompanyCode, 0, 0)
                End With


            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function



    End Class
End Namespace