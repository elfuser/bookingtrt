﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class RatesController
        Inherits Controller

        ' GET: Rates
        <Authorize>
        Function Index() As ActionResult
            Return View("Rates")
        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetRates(SelectAll As Boolean) As String
            Dim clsRates As New BRules.Rates
            Dim RateList As New List(Of RatesInfo)
            Dim Json As String = ""

            Try

                RateList = clsRates.GetRateHeader(0, Utils.CompanyCode, SelectAll)

                Json = Utils.GetJsonFromObject(RateList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDiscounts(RateCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim DiscountList As New List(Of RateDiscountInfo)
            Dim Json As String = ""

            Try

                DiscountList = clsRates.GetRatesDiscountPaging(Utils.CompanyCode, RateCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(DiscountList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDiscountsCount(RateCode As String, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim count As Integer

            Try

                count = clsRates.GetRatesDiscountCount(Utils.CompanyCode, RateCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDetail(RatingId As Integer, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim DatesList As New List(Of RateDetailInfo)
            Dim Json As String = ""

            Try

                DatesList = clsRates.GetRatesDatesPaging(RatingId, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(DatesList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRatesException(RateCode As String, OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim ExcList As New List(Of RateDetailInfo)
            Dim Json As String = ""

            Try

                ExcList = clsRates.GetRatesExceptionPaging(Utils.CompanyCode, RateCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(ExcList)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDetailCount(RatingId As Integer, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim count As Integer

            Try

                count = clsRates.GetRatesDetailCount(RatingId, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetExceptionCount(RateCode As String, PageSize As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim count As Integer

            Try

                count = clsRates.spRateExceptionCount(Utils.CompanyCode, RateCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRoomTypes() As String
            Dim RoomsBL As New BRules.Rooms()
            Dim list As New List(Of RoomsInfo)
            Dim Json As String = ""

            Try


                list = RoomsBL.GetRoomType(0, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(list)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveHeader(RateHeader As RatesInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If RateHeader.ppRateId = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With RateHeader
                    resp = clsRates.SaveRateHeader(.ppRateId, .ppRateCode, .ppDescription, Utils.CompanyCode, .UnFormatFrom,
                              .UnFormatTo, .ppMinLos, .ppMaxLos, .ppEnabled, .ppWeekend, .ppType, .ppRateDetail, .ppRowOrder,
                              .ppFreeNights, .ppConditions, sMode)
                End With

            Catch ex As Exception
                If ex.Message.Contains("CK_tblRateHeader_enddate") Then
                    Return "-2"
                ElseIf ex.Message.Contains("CK_tblRateHeader_maxlos") Then
                    Return "-3"
                Else
                    Return "-1"
                End If
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteHeader(RateID As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response

            Try

                resp = clsRates.DeleteRateHeader(RateID)

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveDetail(RateDetail As RateDetailInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If RateDetail.ppRateDet_id = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If
                Dim a As Integer

                With RateDetail

                    a = .ppRoomtype_id


                    resp = clsRates.SaveRateDetails(.ppRateDet_id, .ppRate_id, .ppRoomtype_id, .UnFormatFrom, .UnFormatTo,
                             .ppAdult1, .ppAdult2, .ppAdult3, .ppAdult4, .ppAdult5, .ppAdult6, .ppAdult7, .ppAdult8,
                              .ppAdult9, .ppAdult10, .ppChild, .ppWAdult1, .ppWAdult2, .ppWAdult3, .ppWAdult4, .ppWAdult5, .ppWAdult6,
                              .ppWAdult7, .ppWAdult8, .ppWAdult9, .ppWAdult10, .ppWChild, Utils.CompanyCode, sMode, .ppRoomtype_id)

                End With



            Catch ex As Exception
                If ex.Message.Contains("CK_tblRateDetail_enddate") Then
                    Return "-2"
                Else
                    Return "-1"
                End If
            End Try
            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteRateDetail(RateDetail As RateDetailInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response

            Try

                With RateDetail
                    resp = clsRates.DeleteRateDetails(.ppRateDet_id, Utils.CompanyCode, .ppRoomtype_id, .FormatStartDate, .FormatEndDate)
                End With

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function DeleteRateDiscount(rowID As Integer) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response

            Try

                resp = clsRates.DeleteRateDiscount(rowID)

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function SaveRateDiscount(RateDiscount As RateDiscountInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If RateDiscount.ppRowID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With RateDiscount

                    If .ppDiscount > 0 Then
                        .ppDisc1 = 0
                        .ppDisc2 = 0
                        .ppDisc3 = 0
                        .ppDisc4 = 0
                        .ppDisc5 = 0
                        .ppDisc6 = 0
                        .ppDisc7 = 0
                        .ppDisc8 = 0
                        .ppType = 1
                    Else
                        .ppDiscount = 0
                        .ppType = 2
                    End If

                    resp = clsRates.SaveRateDiscount(.ppRowID, Utils.CompanyCode, .ppRateCode, .UnFormatFrom, .UnFormatTo,
                              .ppDisc1, .ppDisc2, .ppDisc3, .ppDisc4, .ppDisc5, .ppDisc6, .ppDisc7, .ppDisc8,
                              .ppDiscount, .ppType, sMode)

                End With



            Catch ex As Exception
                Return "-1"
            End Try
            Return "0"

        End Function


        <Authorize>
        <HttpPost()>
        Public Function SaveRateException(RateDetail As RateDetailInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If RateDetail.ppRowID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With RateDetail
                    resp = clsRates.SaveRateException(Utils.CompanyCode, .ppRateCode, .ppRoomCode, .UnFormatFrom, .UnFormatTo,
                             .ppStart_dateAnt, .ppEnd_dateAnt, .ppAdult1, .ppAdult2, .ppAdult3, .ppAdult4, .ppAdult5, .ppAdult6, .ppAdult7, .ppAdult8,
                              .ppAdult9, .ppAdult10, .ppChild, sMode)
                End With



            Catch ex As Exception
                Return "-1"
            End Try
            Return "0"

        End Function

        <Authorize>
        <HttpPost()>
        Public Function DeleteRateExcDates(RateDetail As RateDetailInfo) As String
            Dim clsRates As New BRules.Rates
            Dim resp As New Response

            Try

                With RateDetail
                    resp = clsRates.DeleteRateExcDates(Utils.CompanyCode, .ppRateCode, .ppRowID, .ppRoomCode, .FormatStartDate, .FormatEndDate)
                End With

            Catch ex As Exception
                Return "-1"
            End Try

            Return "0"

        End Function
    End Class
End Namespace