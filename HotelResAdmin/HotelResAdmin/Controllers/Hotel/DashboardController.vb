﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class DashboardController
        Inherits Controller

        ' GET: Dashboard
        <Authorize>
        Function Index() As ActionResult
            If Utils.UserObject Is Nothing Then
                Return RedirectToAction("Login", "Security")
            End If

            Return View("SalesDashboard")
        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetSalesByMonth() As String
            Dim clsDashBoard As New BRules.Dashboard
            Dim List As New List(Of clsDashSalesByMonth)
            Dim Json As String = ""

            Try

                List = clsDashBoard.DashSalesByMonth(Now.Year(), Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpGet()>
        Public Function DashSalesRooms() As String
            Dim clsDashBoard As New BRules.Dashboard
            Dim List As New List(Of clsDashRoomsQty)
            Dim Json As String = ""

            Try

                Dim dt1 As Date = New DateTime(Now.Year, Now.Month - 1, 1)
                Dim dt2 As Date = Now

                List = clsDashBoard.DashSalesRoomsQty(dt1, dt2, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

    End Class
End Namespace