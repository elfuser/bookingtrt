﻿Imports System.Web.Mvc
Imports BRules


Namespace Controllers.Hotel
    Public Class FeatureController
        Inherits Controller

        ' GET: Feature
        <Authorize>
        Function Index() As ActionResult
            Return View("Feature")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetFeatures(OrderBy As String, IsAccending As Boolean, PageNumber As Integer, PageSize As Integer) As String
            Dim clsfeature As New BRules.Rooms
            Dim list As New List(Of clsFeaturesInfo)
            Dim Json As String = ""

            Try

                list = clsfeature.GetFeaturesPaging(Utils.CompanyCode, OrderBy, IsAccending, PageNumber, PageSize)

                Json = Utils.GetJsonFromObject(list)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetPagingCount(PageSize As Integer) As String
            Dim clsfeature As New BRules.Rooms
            Dim count As Integer

            Try

                count = clsfeature.GetFeaturesPagingCount(Utils.CompanyCode, PageSize)

            Catch ex As Exception
                Throw ex
            End Try

            Return count.ToString

        End Function

        <Authorize>
        <HttpPost()>
        Public Function Save(feature As clsFeaturesInfo) As String
            Dim clsfeature As New BRules.Rooms
            Dim resp As New Response
            Dim Json As String = ""
            Dim sMode As String = ""

            Try


                If feature.ppFeatureID = 0 Then
                    sMode = "INS"
                Else
                    sMode = "UPD"
                End If

                With feature
                    resp = clsfeature.SaveFeatureLocale(.ppFeatureID, .CultureId, .ppDescription, Utils.CompanyCode, sMode)
                End With



            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return resp.Status

        End Function


        <Authorize>
        <HttpPost()>
        Public Function Delete(FeatureID As Long, CultureId As Integer) As String
            Dim clsfeature As New BRules.Rooms
            Dim resp As New Response
            Dim Json As String = ""

            Try

                resp = clsfeature.SaveFeatureLocale(FeatureID, CultureId, "", Utils.CompanyCode, "DEL")

            Catch ex As Exception
                'Throw ex
                Return "-1"
            End Try

            Return "0"

        End Function




    End Class
End Namespace