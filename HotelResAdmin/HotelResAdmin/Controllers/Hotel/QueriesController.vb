﻿Imports System.Web.Mvc
Imports BRules

Namespace Controllers.Hotel
    Public Class QueriesController
        Inherits Controller

        <Authorize>
        Function Reservations() As ActionResult
            Return View("Reservations")
        End Function


        <Authorize>
        Function Commisions() As ActionResult
            Return View("Commisions")
        End Function

        <Authorize>
        Function ClosingGeneral() As ActionResult
            Return View("ClosingGeneral")
        End Function

        <Authorize>
        Function ClosingSales() As ActionResult
            Return View("ClosingSales")
        End Function

        <Authorize>
        Function Availability() As ActionResult
            Return View("AvailabilityQry")
        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetDefaultDate() As String

            Dim Json As String = ""

            Try
                'Json = Utils.GetJsonFromObject(Today.ToShortDateString)
                Json = Today.ToShortDateString

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function GetReservations(DateFrom As String, Dateto As String, Range As Integer) As String
            Dim clsQueries As New BRules.Queries
            Dim List As New List(Of QryReservationConfirmation)
            Dim Json As String = ""
            Dim DtFrom As Date = Now.AddDays(-1)
            Dim DtTo As Date = Now

            Try

                If IsDate(DateFrom) Then
                    DtFrom = CDate(DateFrom)
                End If

                If IsDate(Dateto) Then
                    DtTo = CDate(Dateto)
                End If

                List = clsQueries.QueryReservationAccountOS(Utils.CompanyCode, DtFrom, DtTo)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function




        <Authorize>
        <HttpGet()>
        Public Function GetCommissions(DateFrom As String, Dateto As String, Range As Integer) As String
            Dim clsQueries As New BRules.Queries
            Dim List As New List(Of QryReservationConfirmation)
            Dim Json As String = ""
            Dim DtFrom As Date = Now.AddDays(-1)
            Dim DtTo As Date = Now

            Try

                If IsDate(DateFrom) Then
                    DtFrom = CDate(DateFrom)
                End If

                If IsDate(Dateto) Then
                    DtTo = CDate(Dateto)
                End If

                List = clsQueries.QueryReservationAccount(DtFrom, DtTo, Utils.CompanyCode, Range)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRoomsAvailabillity(ByVal CurrentDate As String) As String
            Dim clsQueries As New BRules.Queries
            Dim obj As New QryRoomsAvailability
            Dim Json As String = ""
            Dim DtCurrentDate As Date = Now

            Try

                If IsDate(CurrentDate) Then
                    DtCurrentDate = CDate(CurrentDate)
                End If

                obj = clsQueries.QueryRoomsAvailability(DtCurrentDate, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(obj)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function




        <Authorize>
        <HttpGet()>
        Public Function ReservationRoomConfirmation(ReservationID As String) As String
            Dim clsQueries As New BRules.Queries
            Dim List As New List(Of QryRoom)
            Dim Json As String = ""

            Try

                List = clsQueries.QueryReservationRoomConfirmation(ReservationID)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function


        <Authorize>
        <HttpGet()>
        Public Function ReservationAddonConfirmations(ReservationID As String) As String
            Dim clsQueries As New BRules.Queries
            Dim List As New List(Of QryAddon)
            Dim Json As String = ""

            Try

                List = clsQueries.QueryReservationAddonConfirmations(ReservationID)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function



        <Authorize>
        <HttpGet()>
        Public Function ReservationBankLog(ReservationID As String) As String
            Dim clsQueries As New BRules.Queries
            Dim List As New List(Of BankLogInfo)
            Dim Json As String = ""

            Try

                List = clsQueries.QueryReservationBankLog(ReservationID)

                Json = Utils.GetJsonFromObject(List)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRoomsClosingSales(ByVal CurrentDate As String) As String
            Dim clsQueries As New BRules.Queries
            Dim obj As New QryRoomsAvailability
            Dim Json As String = ""
            Dim DtCurrentDate As Date = Now

            Try

                If IsDate(CurrentDate) Then
                    DtCurrentDate = CDate(CurrentDate)
                End If

                obj = clsQueries.QueryRoomsClosingSales(DtCurrentDate, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(obj)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

        <Authorize>
        <HttpGet()>
        Public Function GetRoomsClosingGeneral(ByVal CurrentDate As String) As String
            Dim clsQueries As New BRules.Queries
            Dim obj As New QryRoomsAvailability
            Dim Json As String = ""
            Dim DtCurrentDate As Date = Now

            Try

                If IsDate(CurrentDate) Then
                    DtCurrentDate = CDate(CurrentDate)
                End If

                obj = clsQueries.QueryRoomsClosingGeneral(DtCurrentDate, Utils.CompanyCode)

                Json = Utils.GetJsonFromObject(obj)

            Catch ex As Exception
                Throw ex
            End Try

            Return Json

        End Function

    End Class


End Namespace