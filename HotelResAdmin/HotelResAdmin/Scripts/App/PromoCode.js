﻿function PromoCodeVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.RatesList = ko.observableArray();
    self.ServiceList = ko.observableArray();
    self.RateServiceList = ko.observableArray();
    self.PromoCodeList = ko.observableArray();
    self.PromoCodeRateList= ko.observableArray();

    self.selectedRateType = ko.observable(new RatesModel());
    self.SelectPromoCode = ko.observable(new PromoCodeModel());
    self.SelectedService = ko.observable(new ServicesModel());

    self.RatePromo = ko.observable(new PromoCodeRateModel());


    self.PromoException = ko.observable(new PromoCodeExceptionModel());
    self.PromoExceptionList = ko.observableArray();
    self.selectedExcDate = ko.observable();

    self.isRefresh = ko.observable(false);

    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();
    self.RateAmount = ko.observable();

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable("a.promo_id");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);
    self.isRate = ko.observable(false);
    self.isServiceRate = ko.observable(false);
    self.getDisabledState = ko.observable(null);

    

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.GetServices = function () {

        self.ServiceList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetServices?Culture=en-US&Mode=SE4",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var ServiceDTO = new ServicesModel();
                            ServiceDTO.MapEntity(value, ServiceDTO);
                            self.ServiceList.push(ServiceDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetRateTypes = function () {

        self.RatesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetRateTypes",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var RateDTO = new RatesModel();
                            RateDTO.MapEntity(value, RateDTO);
                            self.RatesList.push(RateDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetPromoCodesxRate = function (PromoId) {

  
        self.PromoCodeRateList.removeAll();

        $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetPromoCodesxRate?Promoid=" + PromoId + "&PromoType=" + $('#hdf_promo_type').val() + "&Culture=en-US",
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var PromoRateDTO = new PromoCodeRateModel();
                            PromoRateDTO.MapEntity(value, PromoRateDTO);
                            self.PromoCodeRateList.push(PromoRateDTO);
                    })
                }
              }

                
        },
        error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
        }
        });
}


    self.GetPromoCodes = function () {

        loading(true, 'Please wait ...');

        self.PromoCodeList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetPromoCodes?PromoType=" + $('#hdf_promo_type').val() + "&OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var PromoCodeDTO = new PromoCodeModel();
                            PromoCodeDTO.MapEntity(value, PromoCodeDTO);
                            self.PromoCodeList.push(PromoCodeDTO);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetPromoCodesPagingCount?PromoType=" + $('#hdf_promo_type').val() + "&PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }


    self.GetPromoCodesException= function (PromoId) {


        self.PromoExceptionList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/PromoCodes/GetExceptionPromoCodes?PromoID=" + PromoId,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var PromoExceptionDTO = new PromoCodeExceptionModel();
                            PromoExceptionDTO.MapEntity(value, PromoExceptionDTO);
                            self.PromoExceptionList.push(PromoExceptionDTO);
                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }



    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetPromoCodes();
        self.GetTotalPageCount();
    }

    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetPromoCodes();

    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetPromoCodes();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetPromoCodes();

        }
    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetPromoCodes();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetPromoCodes();

        }
    }

    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable("a.promo_id");
        self.isAccending = ko.observable(true);
        self.GetPromoCodes();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }

    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.SelectPromoCode(new PromoCodeModel());
        self.SelectPromoCode().ppType("P");
        $("#ExceptionsDiv").hide();
    }

    self.ValidateData = function () {

        var valid = true;
        var validfromdate = false;
        var validtodate = false;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectPromoCode().ppDescription() == null || self.SelectPromoCode().ppDescription() == undefined || self.SelectPromoCode().ppDescription() == "") {
            ValidateMsgText = "Promo description field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectPromoCode().ppPromoCode() == null || self.SelectPromoCode().ppPromoCode() == undefined || self.SelectPromoCode().ppPromoCode() == "") {
            ValidateMsgText = "Promo code field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectPromoCode().UnFormatFrom() == null || self.SelectPromoCode().UnFormatFrom() == undefined
            || self.SelectPromoCode().UnFormatFrom() == "" || self.SelectPromoCode().UnFormatFrom() == "NaN-NaN-NaN") {
            ValidateMsgText = "valid  from date field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            validfromdate = true;
        }


        if (self.SelectPromoCode().UnFormatTo() == null || self.SelectPromoCode().UnFormatTo() == undefined
            || self.SelectPromoCode().UnFormatTo() == "" || self.SelectPromoCode().UnFormatTo() == "NaN-NaN-NaN") {
            ValidateMsgText = "valid end date field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            validtodate = true;
        }

        if (validfromdate && validtodate) {
            dfrom = new Date(self.SelectPromoCode().UnFormatFrom().replace('-', '/'));
            dto = new Date(self.SelectPromoCode().UnFormatTo().replace('-', '/'));
            if (dfrom > dto) {
                ValidateMsgText = "Start Date is greater than End Date\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        return valid;
    }

    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "FromDate") {
                        self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "ExceptionDate") {
                        self.selectedExcDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };

    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "ToDate") {
                        self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }


    self.CloseClick = function () {
        self.isRate(false);
        self.isServiceRate(false);        
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        $('#cmdButtons').show();
        $("#ExceptionsDiv").hide();
    }

    self.SaveClick = function () {


        var valid = true;

        try {

            self.PrepareDtoForInsert();

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newPromoCode = JSON.stringify(ko.toJS(self.SelectPromoCode()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/PromoCodes/SavePromoCode",
                    async: false,
                    crossDomain: true,
                    data: newPromoCode,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.GetPromoCodes();
                            self.isSucess(true);
                            $("#PromoCodeMsg").show();
                            $("#PromoCodeMsg").html("<h5>" + "Promo Code saved successfully" + "</h5>").fadeIn(0);
                            $("#PromoCodeMsg").html("<h5>" + "Promo Code saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else if (response === "-2") {
                            ValidateMsgText = "Start Date is equal or greater than End Date";
                            self.ShowEditErrorList(ValidateMsgText);                          
                        } else {
                            ValidateMsgText = "An error has ocurred saving the Promo Code";
                            self.ShowEditErrorList(ValidateMsgText);
                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#PromoCodeMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
        }

    }

    self.EditPromoCode = function (data) {
        try {
            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectPromoCode(data);

            self.selectedFromDate(formatDate(self.SelectPromoCode().ppStartDate(), '/'));
            self.selectedToDate(formatDate(self.SelectPromoCode().ppEndDate(), '/'));

            self.GetPromoCodesException(self.SelectPromoCode().ppPromoID());

            $("#ExceptionsDiv").show();


            loading(false);
        }
        catch (err) {
            loading(false);
        }
    }
     
    self.SaveRate = function (data) {

        try {

                loading(true, 'Please wait ...');

                var RateType = '';
                valid = true;

                self.ErrorList.removeAll();
                self.isShowErrorEdit(false);

                if (self.RateAmount() == null || self.RateAmount() == undefined || self.RateAmount() == "") {
                    ValidateMsgText = "Rate amount field is required\n";
                    self.ShowEditErrorList(ValidateMsgText);
                    valid = false;
                }

                //if ($('#hdf_promo_type').val() == null || $('#hdf_promo_type').val() == '' || $('#hdf_promo_type').val() == 'H') {
                
                //}

                if (self.selectedRateType() != null && self.selectedRateType().ppRateId() != null && self.selectedRateType().ppRateId() != undefined) {
                    RateType = self.selectedRateType().ppRateId();
                }

                self.RatePromo().ppRateID(RateType);
                self.RatePromo().ppAmount(self.RateAmount());
                self.RatePromo().ppPromoID(self.SelectPromoCode().ppPromoID());
                self.RatePromo().ppPromoType($('#hdf_promo_type').val());
                self.RatePromo().ppServiceID($('#ddlService').val());


                if (valid === true) {

                    var newRate = JSON.stringify(ko.toJS(self.RatePromo()));

                    $.ajax({
                        type: "POST",
                        url: $("#txtTrackingURLBase").val() + "/PromoCodes/SavePromoCodexRate",
                        async: false,
                        crossDomain: true,
                        data: newRate,
                        contentType: 'application/json',
                        success: function (response) {
                            if (response === "0") {
                                self.GetPromoCodesxRate(self.SelectPromoCode().ppPromoID());
                                self.RateAmount("");
                            }
                            else if (response === "-2") {
                                ValidateMsgText = "There is already a record for the rate and promo code.";
                                self.ShowEditErrorList(ValidateMsgText);
                            }
                            else {
                                ValidateMsgText = "An error has ocurred saving the Promo Code";
                                self.ShowEditErrorList(ValidateMsgText);
                            }

                            loading(false);

                        },
                        error: function (errorResponse) {
                            loading(false);
                            var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                            self.ShowEditErrorList(errorMsg);
                        }
                    });
                }


                loading(false);
        }
        catch (err) {
            loading(false);
        }

    }

    self.servicesChanged = function (obj, event) {

        try {

            loading(true, 'Please wait ...');

            if (event.originalEvent) { //user changed
                var ServiceId = event.currentTarget.value;
                if ((ServiceId != -1)) {
                    try {                       
                        self.GetServiceRates(ServiceId);

                        if ((self.RatesList() !== null) && (self.RatesList() !== undefined) && (self.RatesList().length > 0)) {
                            self.getDisabledState(undefined);
                        } else {
                            self.getDisabledState('disabled');
                        }
                        
                    }
                    catch (ex) {
                        loading(false);
                    }

                } else {
                    self.RatesList.removeAll();
                    self.getDisabledState('disabled');
                }
            } else { // program changed

            }            

            loading(false);
        }
        catch (err) {
            loading(false);
        }

    }


    self.RatesClick = function (data) {

        try {

            $('#cmdButtons').hide();
            self.isRefresh(false);
            self.ErrorList.removeAll();
            self.RateAmount("");
            self.isRate(true);            
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);            

            loading(true, 'Please wait ...');
            self.SelectPromoCode(data);            

            self.GetPromoCodesxRate(self.SelectPromoCode().ppPromoID());

            if ($('#hdf_promo_type').val() != null && $('#hdf_promo_type').val() != '') {                
                self.isServiceRate(true);
                self.getDisabledState('disabled');
                ServicesDropDownChangeAction();
            } else {                
                self.isServiceRate(false);
                self.getDisabledState(undefined);
            }

            loading(false);
        }
        catch (err) {
            loading(false);
        }


    }


    self.DeleteClick = function (data) {


        try {

            if (self.SelectPromoCode() != null && self.SelectPromoCode() != undefined && self.SelectPromoCode().ppPromoID() != undefined && self.SelectPromoCode().ppPromoID() != null) {

                self.isShowErrorPanel(false);
                $("#DeletePromoCode").html($("<h5>" + "Are you sure you want to delete the Promo Code?" + "</h5>"));



                $("#DeletePromoCode").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/PromoCodes/Delete?PromoID=" + self.SelectPromoCode().ppPromoID(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeletePromoCode").dialog("close");


                                    if (response === "0") {
                                        self.GetPromoCodes();
                                        self.isSucess(true);

                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "Promo Code deleted successfully" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "Promo Code deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Promo Code" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Promo Code" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#PromoCodeMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeletePromoCode").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
        }

    }



    self.DeletePromoCodexRate = function (data) {


        try {

            if (data != null && data != undefined && data.ppPromoID() != undefined && data.ppPromoID() != null && data.ppRateID() != null && data.ppRateID() != undefined) {

                self.isShowErrorPanel(false);
                $("#DeletePromoCode").html($("<h5>" + "Are you sure you want to delete the Rate?" + "</h5>"));



                $("#DeletePromoCode").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/PromoCodes/DeletePromoCodexRate?ppPromoID=" + data.ppPromoID() + "&ppRateID=" + data.ppRateID() + "&PromoType=" + $('#hdf_promo_type').val(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeletePromoCode").dialog("close");


                                    if (response === "0") {
                                        self.GetPromoCodesxRate(self.SelectPromoCode().ppPromoID());
                                 
                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "Rate deleted successfully" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "Rate deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Rate" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Promo Code" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#PromoCodeMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeletePromoCode").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
        }

    }

    self.GetServiceRates = function (ServiceId) {

        self.RatesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetServiceRates?ServiceId=" + ServiceId,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var rate = new RatesServiceModel();
                            rate.MapEntity(value, rate);
                            //self.RateServiceList.push(rate);

                            var RateDTO = new RatesModel();
                            RateDTO.MapEntity(value, RateDTO);
                            RateDTO.ppRateId(rate.ppRateID());
                            self.RatesList.push(RateDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.InsertExceptionWeekends = function () {

        try {


                loading(true, 'Please wait ...');

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/PromoCodes/InsertExceptionWeekends?PromoID=" + self.SelectPromoCode().ppPromoID(),
                    async: false,
                    crossDomain: true,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.GetPromoCodesException(self.SelectPromoCode().ppPromoID());
                            self.isSucess(true);
                            $("#PromoCodeMsg").show();
                            $("#PromoCodeMsg").html("<h5>" + "Promo Exception saved successfully" + "</h5>").fadeIn(0);
                            $("#PromoCodeMsg").html("<h5>" + "Promo Exception saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Promo Code Exception";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#PromoCodeMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }

        catch (err) {
            loading(false);
        }

    }

    self.DeleteExceptionWeekends = function () {

        try {


            loading(true, 'Please wait ...');

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/PromoCodes/DeleteExceptionWeekends?PromoID=" + self.SelectPromoCode().ppPromoID(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {

                    loading(false);
                    if (response === "0") {
                        self.GetPromoCodesException(self.SelectPromoCode().ppPromoID());
                        self.isSucess(true);
                        $("#PromoCodeMsg").show();
                        $("#PromoCodeMsg").html("<h5>" + "Promo Exception deleted successfully" + "</h5>").fadeIn(0);
                        $("#PromoCodeMsg").html("<h5>" + "Promo Exception deleted successfully" + "</h5>").fadeOut(8000);
                    }
                    else {

                        ValidateMsgText = "An error has ocurred deleting the Promo Code Exception";
                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#PromoCodeMsg").dialog("close");
                    //$("#LocationMsg").hidden();
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });
        }

        catch (err) {
            loading(false);
        }

    }

    self.DeleteExceptionClick = function (data) {


        try {

            if (data != null && data != undefined && data.PromoExc_id() != undefined && data.PromoExc_id() != null) {

                self.isShowErrorPanel(false);
                $("#DeletePromoCode").html($("<h5>" + "Are you sure you want to delete the Promo Code Exception?" + "</h5>"));



                $("#DeletePromoCode").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/PromoCodes/DeletePromoException?PromoExcID=" + data.PromoExc_id(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeletePromoCode").dialog("close");


                                    if (response === "0") {
                                        self.GetPromoCodesException(self.SelectPromoCode().ppPromoID());
                                        self.isSucess(true);

                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "Promo Code Exception deleted successfully" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "Promo Code  Exception deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#PromoCodeMsg").show();
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Promo Code Exception" + "</h5>").fadeIn(0);
                                        $("#PromoCodeMsg").html("<h5>" + "An error has ocurred deleting the Promo Code Exception" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#PromoCodeMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeletePromoCode").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
        }

    }

    self.SaveException = function () {

        try {

            loading(true, 'Please wait ...');


            valid = true;

            self.ErrorList.removeAll();
            self.isShowErrorEdit(false);

            if (self.selectedExcDate() == null || self.selectedExcDate() == undefined || self.selectedExcDate() == "") {
                ValidateMsgText = "Date Exception field is required\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }


            var DateTo = GetUniversalDate(self.selectedExcDate());

            self.PromoException().UnFormatDate(DateTo);
            var promo_id = self.SelectPromoCode().ppPromoID();


            if (valid === true) {

                var newException = JSON.stringify(ko.toJS(self.PromoException()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/PromoCodes/SavePromoException?PromoID=" + promo_id + "&DateExc=" + DateTo,
                    async: false,
                    crossDomain: true,
                    data: newException,
                    contentType: 'application/json',
                    success: function (response) {
                        if (response === "0") {
                            self.GetPromoCodesException(self.SelectPromoCode().ppPromoID());
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Rate Exception";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                        loading(false);

                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorList(errorMsg);
                    }
                });
            }


            loading(false);
        }
        catch (err) {
            loading(false);
        }

    }


    self.PrepareDtoForInsert = function () {


        var DateFrom = GetUniversalDate(self.selectedFromDate());
        var DateTo = GetUniversalDate(self.selectedToDate());

        self.SelectPromoCode().UnFormatFrom(DateFrom);
        self.SelectPromoCode().UnFormatTo(DateTo);
        
        //if ($('#hdf_promo_type').val() == null) {

        //}

        self.SelectPromoCode().ppPromoType($('#hdf_promo_type').val());


    }

    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetPromoCodes();
        self.GetTotalPageCount();

        if ($('#hdf_promo_type').val() == null || $('#hdf_promo_type').val() == '' || $('#hdf_promo_type').val() == 'H'){
            self.GetRateTypes();
        } 

        self.GetServices();


    }



    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               

               try {
                   loading(true, 'Please wait ...');

                   //var element = document.getElementById("AddOnSection");
                   PromoCodeObj = new PromoCodeVm();

                   ko.applyBindings(PromoCodeObj);

                   PromoCodeObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   if ($('#hdf_promo_type').val() != null && $('#hdf_promo_type').val() != '') {                       
                       $('.RatesButtonColumn .btn').html('Services and Rates');
                       //$('.ratefields').prop('disabled', 'disabled');
                       ServicesDropDownChangeAction();
                   } else {                       
                       $('.RatesButtonColumn .btn').html('Rates');
                       //$('.ratefields').prop('disabled', false);
                   }


                   loading(false);

                   $("#AddOnSection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//

function ServicesDropDownChangeAction() {


    $("#ddlService").change(function () {
        
    });
}

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


