﻿function VirtualReceiptsVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();
    self.getLogo = ko.observable();
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);
    self.ShowTransportRates = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.VirtualReceiptsList = ko.observableArray();
    self.CultureList = ko.observableArray();

    self.SelectVirtualReceipt = ko.observable(new VirtualReceiptsModel());
    self.isRefresh = ko.observable(false);

    self.selectedCulture = ko.observable(new CultureModel());
    self.selectedCountry = ko.observable(new CountriesModel());



    self.selectedExpiryDate = ko.observable();
    self.selectedToDate = ko.observable();

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable("a.receipt_code");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    //self.Picture1 = ko.observable("");
    //self.Picture2 = ko.observable("");

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.GetCountries = function () {

        self.CountryList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/GetCountries",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CountryDTO = new CountriesModel();
                            CountryDTO.MapEntity(value, CountryDTO);
                            self.CountryList.push(CountryDTO);

                        })
                    }
                }

    
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetCultureTypes = function () {

        self.CultureList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetVirtualReceipts = function (Category) {

        loading(true, 'Please wait ...');

        self.VirtualReceiptsList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/GetVirtualReceiptsPaging?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var VirtualReceiptDTO = new VirtualReceiptsModel();
                            VirtualReceiptDTO.MapEntity(value, VirtualReceiptDTO);
                            self.VirtualReceiptsList.push(VirtualReceiptDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() +  "/VirtualReceipts/GetVirtualReceiptsPagingCount?PageSize=" + self.selectedPageSize(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if (response != null) {

                        var PageCount = response;
                        self.totalPageCount(PageCount);
                        self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                    }

                },
                error: function (errorResponse) {
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });
        
    }


    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetVirtualReceipts();
        self.GetTotalPageCount();
    }


    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetVirtualReceipts();

    }


    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetVirtualReceipts();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetVirtualReceipts();
   
        }
    }


    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetVirtualReceipts();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetVirtualReceipts();

        }
    }


    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable("a.ReceiptCode");
        self.isAccending = ko.observable(true);
        self.GetVirtualReceipts();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }


    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        
        self.SelectVirtualReceipt(new VirtualReceiptsModel());

        //-- Load countries in dropdownlist
        //self.LoadCountriesInDropDownList();        
        //$('#ddlCountryId').selectpicker('refresh');

        //self.SetAddonType("S");
        self.selectedCountry("CRI");
        //$('#dtpExpiry').datetimepicker();
        $('#ExpiryDate').val('');

        $('#btnSave').html("Generate Link and Save");
        $('#btnGenerateLink').hide();

        //$("#PhotoDiv").hide();
        //$("#PhotoTab").hide();


    }

    //-- Load countries in dropdown
    self.LoadCountriesInDropDownList = function () {
        var dll_countries = $("#ddlCountryId");

        if (self.CountryList() != null) {
            if (self.CountryList().length > 0) {
                $.each(self.CountryList(), function () {
                    dll_countries.append($("<option />").val(this.ppCountryIso3()).text(this.ppName()));
                });
            } else {
                dll_countries.append($("<option />").val(-1).text("---"));
            }
        } else {
            dll_countries.append($("<option />").val(-1).text("---"));
        }
    }


    self.ValidateData = function () {

        var valid = true;
       
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectVirtualReceipt().ClientName() == null || self.SelectVirtualReceipt().ClientName() == undefined || self.SelectVirtualReceipt().ClientName() == "") {
            ValidateMsgText = "Client Name field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectVirtualReceipt().Country() == null || self.SelectVirtualReceipt().Country() == undefined || self.SelectVirtualReceipt().Country() == "") {
            ValidateMsgText = "Country is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectVirtualReceipt().Email() == null || self.SelectVirtualReceipt().Email() == undefined || self.SelectVirtualReceipt().Email() == "") {
            ValidateMsgText = "Email field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            if (!validateEmail(self.SelectVirtualReceipt().Email()))
            {
                ValidateMsgText = "Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectVirtualReceipt().Description() == null || self.SelectVirtualReceipt().Description() == undefined || self.SelectVirtualReceipt().Description() == "") {
            ValidateMsgText = "Description field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectVirtualReceipt().Amount() == null || self.SelectVirtualReceipt().Amount() == undefined || self.SelectVirtualReceipt().Amount() == "") {
            ValidateMsgText = "Amount field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }        
        
        if (self.SelectVirtualReceipt().FormatExpiry() == null || self.SelectVirtualReceipt().FormatExpiry() == undefined || self.SelectVirtualReceipt().FormatExpiry() == "") {
            ValidateMsgText = "Expiry field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        


        return valid;
    }

 
    //ko.bindingHandlers.ExpiryDatePicker = {
    //    init: function (element, valueAccessor) {
    //        $(element).datepicker({
    //            onSelect: function (dateText, inst) {
    //                var date = new Date(dateText);
    //                if (inst.id == "ExpiryDate") {
    //                    self.selectedExpiryDate(date.ConvertDateToMMDDYYFormat());
    //                }
    //            },
    //            dateFormat: 'mm/dd/yy'
    //        });
    //    }
    //};



    ko.bindingHandlers.datetimepicker = {
        init: function (element, valueAccessor, allBindings) {
            var options = {
                format: 'MM/DD/YYYY hh:mm A',
                defaultDate: ko.unwrap(valueAccessor())
            };

            ko.utils.extend(options, allBindings.dateTimePickerOptions);

            $(element).datetimepicker(options).on("dp.change", function (evntObj) {
                var observable = valueAccessor();
                if (evntObj.timeStamp !== undefined) {
                    var picker = $(this).data("DateTimePicker");
                    var d = picker.date();
                    observable(d.format(options.format));
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            $(element).datetimepicker('date', value || '');
        }
    };



    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);


    }

    //self.CloseRates = function () {

    //    $("#TransportRates").hide(200);
    //    self.ShowTransportRates(false);

    //}
    


    self.SaveClick = function () {


        var valid = true;

        try
        {

          

            self.PrepareDtoForInsert();


            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newVirtualReceipt = JSON.stringify(ko.toJS(self.SelectVirtualReceipt()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() +  "/VirtualReceipts/SaveVirtualReceipt",
                    async: false,
                    crossDomain: true,
                    data: newVirtualReceipt,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);                        

                        if (response === "0") {
                            self.CloseClick();
                            self.GetVirtualReceipts();
                            self.isSucess(true);
                            $("#VirtualReceiptMsg").show();
                            $("#VirtualReceiptMsg").html("<h5>" + "Virtual Receipt saved successfully" + "</h5>").fadeIn(0);
                            $("#VirtualReceiptMsg").html("<h5>" + "Virtual Receipt saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {
                            if (response === "-2") {
                                ValidateMsgText = "An error has ocurred generating link and saving the Virtual Receipt";
                            } else {
                                ValidateMsgText = "An error has ocurred saving the Virtual Receipt";
                            }
                            
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#VirtualReceiptMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }

    self.GenerateLinkClick = function () {
        var valid = true;

        try {
            self.PrepareDtoForInsert();

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newVirtualReceipt = JSON.stringify(ko.toJS(self.SelectVirtualReceipt()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/GenerateReceiptLink",
                    async: false,
                    crossDomain: true,
                    data: newVirtualReceipt,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);

                        if (response == "True") {
                            self.CloseClick();
                            self.GetVirtualReceipts();
                            self.isSucess(true);
                            $("#VirtualReceiptMsg").show();
                            $("#VirtualReceiptMsg").html("<h5>" + "Link generated successfully. An email has been sent to the registered email address." + "</h5>").fadeIn(0);
                            $("#VirtualReceiptMsg").html("<h5>" + "Link generated successfully. An email has been sent to the registered email address." + "</h5>").fadeOut(14000);
                        }
                        else {                            
                            ValidateMsgText = "An error has ocurred generating link for the Virtual Receipt";                            
                            self.ShowEditErrorList(ValidateMsgText);
                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#VirtualReceiptMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }


    self.EditVirtualReceipt = function (data) {


        try {
            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectVirtualReceipt(data);           

            self.VirtualReceiptClick();

            //$('#dtpExpiry').datetimepicker();
            $('#ExpiryDate').val('');
            $('#btnSave').html("Save");
            $('#btnGenerateLink').show();

            //-- Load countries in dropdownlist
            //self.LoadCountriesInDropDownList();
            //$('#ddlCountryId').selectpicker('refresh');

            self.selectedExpiryDate(formatDateTime(self.SelectVirtualReceipt().Expiry(), '/'));
            //self.selectedToDate(formatDate(self.SelectVirtualReceipt().ppTo(), '/'));




            var filteredCulture;
            var CultureID;

            //CultureID = $.trim(self.SelectVirtualReceipt().CultureID());
            //if (CultureID != null && CultureID != undefined && CultureID != '') {
            //    filteredCulture = Enumerable.From(self.CultureList()).Where(function (x) { return x.ppCultureID().toString().toUpperCase().match("^" + CultureID.toUpperCase()) }).FirstOrDefault()
            //    if (filteredCulture != null && filteredCulture != undefined) { self.selectedCulture(filteredCulture); }
            //    else { self.selectedCulture(new CultureModel()); }

            //} else { self.selectedCulture(new CultureModel()); }


            var filteredType;
            var Country;

            Country = $.trim(self.SelectVirtualReceipt().Country());
            if (Country != null && Country != undefined && Country != '') {
                filteredType = Enumerable.From(self.CountryList()).Where(function (x) { return x.ppCountryIso3().toString().toUpperCase().match("^" + Country.toUpperCase()) }).FirstOrDefault()
                if (filteredType != null && filteredType != undefined) {
                    self.selectedCountry(filteredType);
                    //$("#ddlCountryId").val(filteredType.ppCountryIso3());
                }
                else { self.selectedCountry(new CountriesModel()); }

            } else { self.selectedCountry(new CountriesModel()); }


            //self.SetAddonType(AddOnType);

            //if ((data.ppAddonID() !== "") &&  (data.ppAddonID() !== "0") &&  (data.ppAddonID() !== undefined))
            //{
            //    self.Picture1("../../Handlers/AddOnHandler.ashx?PictureNum=1&AddonID=" + data.ppAddonID());
            //    self.Picture2("../../Handlers/AddOnHandler.ashx?PictureNum=2&AddonID=" + data.ppAddonID());
            //    $("#PhotoTab").show();
            //    $("#PhotoDiv").hide();
            //}
            //else { 
            //    $("#PhotoDiv").hide();
            //    $("#PhotoTab").hide();
            //}

            //$("#AddonId").val(data.ppAddonID());

            loading(false);           
        }
        catch (err) {
            loading(false);
        }


    }

    self.ReceiptCountryChanged = function () {

        //self.SetAddonType(self.selectedAddOnType().code());
    }

    self.DeleteClick = function (data) {


        try
        {

            if (self.SelectVirtualReceipt() != null && self.SelectVirtualReceipt() != undefined && self.SelectVirtualReceipt().ReceiptCode() != undefined && self.SelectVirtualReceipt().ReceiptCode() != null) {

            self.isShowErrorPanel(false);
            $("#DeleteVirtualReceipt").html($("<h5>" + "Are you sure you want to delete the Virtual Receipt?" + "</h5>"));



            $("#DeleteVirtualReceipt").dialog({
                resizable: false,
                modal: false,
                closeOnEscape: false,
                position: { at: "center  " },
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                },
                buttons: {
                    Yes: function () {
                        loading(true, 'Please wait ...');
                        $.ajax({
                            type: "POST",
                            url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/Delete?VirtualReceiptId=" + self.SelectVirtualReceipt().ReceiptCode(),
                            async: false,
                            crossDomain: true,
                            contentType: 'application/json',
                            success: function (response) {

                                loading(false);
                                $("#DeleteVirtualReceipt").dialog("close");


                                if (response === "0") {
                                    self.GetVirtualReceipts();
                                    self.isSucess(true);

                                    $("#VirtualReceiptMsg").show();
                                    $("#VirtualReceiptMsg").html("<h5>" + "Virtual Receipt deleted successfully" + "</h5>").fadeIn(0);
                                    $("#VirtualReceiptMsg").html("<h5>" + "Virtual Receipt deleted successfully" + "</h5>").fadeOut(3000);

                                }
                                else {
                                    $("#VirtualReceiptMsg").show();
                                    $("#VirtualReceiptMsg").html("<h5>" + "An error has ocurred deleting the Virtual Receipt" + "</h5>").fadeIn(0);
                                    $("#VirtualReceiptMsg").html("<h5>" + "An error has ocurred deleting the Virtual Receipt" + "</h5>").fadeOut(3000);
                                    
                                }

                            },
                            error: function (errorResponse) {
                                loading(false);
                                $("#VirtualReceiptMsg").dialog("close");
                                //$("#LocationMsg").hidden();
                                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                self.ShowErrorList(errorMsg);
                            }
                        });
                    },
                    No: function () {
                        loading(false);
                        $("#DeleteVirtualReceipt").dialog("close");

                    }
                }
            });

        }


        }
        catch (err) {
            loading(false);
        }

    }


    self.PrepareDtoForInsert = function () {

        if (self.selectedCountry() != null && self.selectedCountry().ppCountryIso3() != null && self.selectedCountry().ppCountryIso3() != undefined) {
            self.SelectVirtualReceipt().Country(self.selectedCountry().ppCountryIso3());
        }
 
        var DateExpiry = GetUniversalDateTime(self.selectedExpiryDate());        

        self.SelectVirtualReceipt().FormatExpiry(DateExpiry);        

        self.SelectVirtualReceipt().CompanyName($("#ddl_Companies").find(":selected").text()); //Store company name based on the companies dropdownlist        
    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }




    self.VirtualReceiptClick = function () {

        $("#VirtualReceiptEntryDiv").show();
        //$("#PhotoDiv").hide();
        $("#VirtualReceiptTab").addClass("activePageMenu");
        //$("#PhotoTab").removeClass("activePageMenu");

    }   



    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetCountries(); //--RS
        self.GetVirtualReceipts();
        self.GetTotalPageCount();
        self.GetCultureTypes();      
 
    }




    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//
//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               
               
               try {
                   loading(true, 'Please wait ...');

                   //setTimeout(function () {

                   VirtualReceiptObj = new VirtualReceiptsVm();

                   ko.applyBindings(VirtualReceiptObj);

                   VirtualReceiptObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });                   

                   //$('#ddlCountryId').selectpicker('refresh');

                   

                   loading(false);

                   $("#VirtualReceiptSection").show(); //Show main div

                   $('#dtpExpiry').datetimepicker();

                   //$("input[name='file[]']").removeAttr("multiple");
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//



function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }            
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


