﻿function CompaniesVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();
    self.getLogo = ko.observable();
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.CompaniesList = ko.observableArray();
    self.CombinationsList = ko.observableArray();
    self.SelectCompany = ko.observable(new CompanyModel());

    self.CompanyHotel = ko.observable(new CompanyModel());
    self.CompanyService = ko.observable(new CompanyModel());

    self.isRefresh = ko.observable(false);

    self.CompanyProcessor = ko.observable(new CompanyModel());

    self.SelectedCombination = ko.observable(new CombinationsModel());

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable(" a.company_code ");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.PrefixMsg = ko.observable();


    self.Picture1 = ko.observable("");
    self.Picture2 = ko.observable("");
    self.Picture3 = ko.observable("");
    self.Picture4 = ko.observable("");
    self.Picture5 = ko.observable("");

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }

    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.GetCompanies = function (Category) {

        loading(true, 'Please wait ...');

        self.CompaniesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCompaniesPaging?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CompanyDTO = new CompanyModel();
                            CompanyDTO.MapEntity(value, CompanyDTO);
                            self.CompaniesList.push(CompanyDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCompaniesPagingCount?PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetCompanies();
        self.GetTotalPageCount();
    }

    self.GetSelectedPage = function () {



        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }
  
        self.GetCompanies();

    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetCompanies();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetCompanies();

        }
    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetCompanies();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetCompanies();

        }
    }

    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable(" a.company_code ");
        self.isAccending = ko.observable(true);
        self.GetCompanies();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }

    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);

        self.SelectCompany(new CompanyModel());


        $("#GeneralDiv").show();
        $("#ProcessorDiv").hide();
        $("#HotelDiv").hide();
        $("#ServicesDiv").hide();

        //--RS
        $("#PhotoDiv").hide();
        $("#PhotoTab").hide();

        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#PhotoTab").removeClass("activePageMenu");                
        //

        $("#ppCompanyCode").prop('disabled', false);
        $("#btnSaveHotelInfo").html('Add');


    }

    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectCompany().ppCompanyName() == null || self.SelectCompany().ppCompanyName() == undefined || self.SelectCompany().ppCompanyName() == "") {
            ValidateMsgText = "The Name cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectCompany().ppAddress() == null || self.SelectCompany().ppAddress() == undefined || self.SelectCompany().ppAddress() == "") {
            ValidateMsgText = "The Address cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectCompany().ppTel1() == null || self.SelectCompany().ppTel1() == undefined || self.SelectCompany().ppTel1() == "") {
            ValidateMsgText = "The Telephone cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectCompany().ppContactName() == null || self.SelectCompany().ppContactName() == undefined || self.SelectCompany().ppContactName() == "") {
            ValidateMsgText = "The Contact Name cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectCompany().TechEmail() != null && self.SelectCompany().TechEmail() != undefined && self.SelectCompany().TechEmail() != "") {
            if (!validateEmail(self.SelectCompany().TechEmail())) {
                ValidateMsgText = "Technical Contact Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectCompany().ppContactEmail() != null && self.SelectCompany().ppContactEmail() != undefined && self.SelectCompany().ppContactEmail() != "") {
            if (!validateEmail(self.SelectCompany().ppContactEmail())) {
                ValidateMsgText = "Main Contact Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectCompany().ppCfoEmail() != null && self.SelectCompany().ppCfoEmail() != undefined && self.SelectCompany().ppCfoEmail() != "") {
            if (!validateEmail(self.SelectCompany().ppCfoEmail())) {
                ValidateMsgText = "Financial Contact Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectCompany().ppResEmail() != null && self.SelectCompany().ppResEmail() != undefined && self.SelectCompany().ppResEmail() != "") {
            if (!validateEmail(self.SelectCompany().ppResEmail())) {
                ValidateMsgText = "Reservation Contact Email 1 is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectCompany().ppResEmailAlt() != null && self.SelectCompany().ppResEmailAlt() != undefined && self.SelectCompany().ppResEmailAlt() != "") {
            if (!validateEmail(self.SelectCompany().ppResEmailAlt())) {
                ValidateMsgText = "Reservation Contact Email 2 is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectCompany().ppEmail() != null && self.SelectCompany().ppEmail() != undefined && self.SelectCompany().ppEmail() != "") {
            if (!validateEmail(self.SelectCompany().ppEmail())) {
                ValidateMsgText = "Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }
     

        return valid;
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);


    }

    self.CloseRates = function () {

        $("#TransportRates").hide(200);
        self.ShowTransportRates(false);

    }

    self.SeeMap = function () {

        var userLng = parseFloat($('#ppLongitude').val());
        var userLat = parseFloat($('#ppLatitude').val());
        var myLatlng = new google.maps.LatLng(userLat, userLng);

        var mapOptions = {
            //center: { lat: userLat, lng: userLng },
            center:  myLatlng ,
            zoom: 12
        };
        $("#map-canvas").show();        
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title: "Hello World!"
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    }

    self.CloseMap = function () {

        $("#map-canvas").hide();
       
    }

    self.SaveClick = function () {
        var valid = true;
        var URL = $("#txtTrackingURLBase").val() + "/Companies/AddHotel"
        var action = "Add"
        if ($.trim($("#btnSaveHotelInfo").text()) != 'Add') {
            URL = $("#txtTrackingURLBase").val() + "/Companies/UpdateHotel"
            action = "Update"
        }

        try {

            //self.PrepareDtoForInsert();
            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newCompany = JSON.stringify(ko.toJS(self.SelectCompany()));               

                $.ajax({
                    type: "POST",
                    url: URL,
                    async: false,
                    crossDomain: true,
                    data: newCompany,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.RefreshClick();
                            //self.GetCompanies();
                            ReloadCompanies();
                            self.isSucess(true);
                            $("#CompanyMsg").show();
                            $("#CompanyMsg").html("<h5>" + "Company saved successfully" + "</h5>").fadeIn(0);
                            $("#CompanyMsg").html("<h5>" + "Company saved successfully" + "</h5>").fadeOut(8000);
                        }

                        else {

                            if (response === "1") {

                                ValidateMsgText = "The company code: " + self.SelectCompany().ppCompanyCode() + " already exists in database ";
                                self.ShowEditErrorList(ValidateMsgText);
                            } else if (response === "-1" && action == "Update") {
                                ValidateMsgText = "The company code: " + self.SelectCompany().ppCompanyCode() + " does not exist in database ";
                                self.ShowEditErrorList(ValidateMsgText);
                            }
                            else {
                                if (action == "Add") {
                                    ValidateMsgText = "Error adding company";                                    
                                } else {
                                    ValidateMsgText = "Error updating company";                                    
                                }

                                self.ShowEditErrorList(ValidateMsgText);
                                
                            }

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#CompanyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }

    }

    self.EditCompany = function (data) {


        try {
            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectCompany(data);

            self.GetCompanyProcesor();
          
            //self.GeneralClick();


            $("#GeneralDiv").show();
            $("#ProcessorDiv").show();            
     

            if (self.SelectCompany().ppHotel() === true)
            {
                self.GetCompanyHotel();
                $("#HotelDiv").show();
            }
            else
            {

                $("#HotelDiv").hide();
                
            }


            if (self.SelectCompany().ppServices() === true) {
                self.GetCompanyServices();
                $("#ServicesDiv").show();
            }
            else {
                $("#ServicesDiv").hide();

            }
                                              
            self.GetCombinations();

            self.GeneralInfoClick();

            if (self.SelectCompany().ppCompanyCode() != null && self.SelectCompany().ppCompanyCode() !== "" && self.SelectCompany().ppCompanyCode() !== undefined) {                
                self.Picture2($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=2&CompanyCode=" + self.SelectCompany().ppCompanyCode());
                self.Picture3($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=3&CompanyCode=" + self.SelectCompany().ppCompanyCode());
                self.Picture4($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=1&CompanyCode=" + self.SelectCompany().ppCompanyCode());
                self.Picture1($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=4&CompanyCode=" + self.SelectCompany().ppCompanyCode()); //logo
                self.Picture5($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=5&CompanyCode=" + self.SelectCompany().ppCompanyCode());
                $("#PhotoTab").show();
                $("#PhotoDiv").hide();
            } else {
                $("#PhotoDiv").hide();
                $("#PhotoTab").hide();
            }
            
            
            $("#ppCompanyCode").prop('disabled', true);
            $("#btnSaveHotelInfo").html('Save');
            

            loading(false);
        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }

    self.DeleteClick = function (data) {


        try {

            if (self.SelectCompany() != null && self.SelectCompany() != undefined && self.SelectCompany().ppCompanyCode() != undefined && self.SelectCompany().ppCompanyCode() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteCompany").html($("<h5>" + "Are you sure you want to delete the Company?" + "</h5>"));



                $("#DeleteCompany").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Companies/Delete?HotelId=" + self.SelectCompany().ppCompanyCode(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteCompany").dialog("close");


                                    if (response === "0") {
                                        self.GetCompanies();
                                        self.isSucess(true);

                                        $("#CompanyMsg").show();
                                        $("#CompanyMsg").html("<h5>" + "Company deleted successfully" + "</h5>").fadeIn(0);
                                        $("#CompanyMsg").html("<h5>" + "Company deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#CompanyMsg").show();
                                        $("#CompanyMsg").html("<h5>" + "An error has ocurred deleting the Company" + "</h5>").fadeIn(0);
                                        $("#CompanyMsg").html("<h5>" + "An error has ocurred deleting the Company" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#CompanyMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteCompany").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorList(err);
        }

    }


    self.PrepareDtoForInsert = function () {


        if (self.selectedCulture() != null && self.selectedCulture().ppCultureID() != null && self.selectedCulture().ppCultureID() != undefined) {
            self.SelectCompany().CultureID(self.selectedCulture().ppCultureID());
        }

        if (self.selectedCompanyType() != null && self.selectedCompanyType().code() != null && self.selectedCompanyType().code() != undefined) {
            self.SelectCompany().ppCompanyType(self.selectedCompanyType().code());
        }


        var DateFrom = GetUniversalDate(self.selectedFromDate());
        var DateTo = GetUniversalDate(self.selectedToDate());

        self.SelectCompany().UnFormatFrom(DateFrom);
        self.SelectCompany().UnFormatTo(DateTo);


    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }

    ///processor //////////////////////////////////////////////////////
    self.GetCompanyProcesor= function () {

        self.CompanyProcessor(new CompanyModel());

        if (self.SelectCompany() != null && self.SelectCompany() != undefined && self.SelectCompany().ppCompanyCode() != undefined && self.SelectCompany().ppCompanyCode() != null) 
        {
                loading(true, 'Please wait ...');

                $.ajax({
                    type: "GET",
                    url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanyProcessor?CompanyId=" + self.SelectCompany().ppCompanyCode(),
                    async: false,
                    crossDomain: true,
                    contentType: 'application/json',
                    success: function (response) {
                        if ((response !== null) && (response !== '')) {

                            var ds = jQuery.parseJSON(response);

                            if (ds !== null) {

                                var CompanyDTO = new CompanyModel();
                                CompanyDTO.MapEntity(ds, CompanyDTO);
                                self.CompanyProcessor(CompanyDTO);
                            }
                        }

                        loading(false);



                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });

         }
    }

    self.SaveProcesorClick = function () {


        var valid = true;

        try {


            if (self.SelectCompany().ppCompanyCode() == null || self.SelectCompany().ppCompanyCode() == undefined || self.SelectCompany().ppCompanyCode() == "") {
                ValidateMsgText = "The company code cannot be empty, please correct\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (valid === true) {

                loading(true, 'Please wait ...');

                self.CompanyProcessor().ppCompanyCode(self.SelectCompany().ppCompanyCode());

                var newCompany = JSON.stringify(ko.toJS(self.CompanyProcessor()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Companies/SaveHotelProcessor",
                    async: false,
                    crossDomain: true,
                    data: newCompany,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.isSucess(true);
                            $("#CompanyMsg").show();
                            $("#CompanyMsg").html("<h5>" + "Company processor saved successfully" + "</h5>").fadeIn(0);
                            $("#CompanyMsg").html("<h5>" + "Company processor saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Company";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#CompanyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {

            loading(false);
            self.ShowErrorList(err);
        }

    }
    ///processor //////////////////////////////////////////////////////




    //hotel /////////////////////////////////////////////////////////////////
    self.GetCompanyHotel = function () {
       
        self.CompanyHotel(new CompanyModel());

        if (self.SelectCompany() != null && self.SelectCompany() != undefined && self.SelectCompany().ppCompanyCode() != undefined && self.SelectCompany().ppCompanyCode() != null) {
            loading(true, 'Please wait ...');

            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanyHotel?CompanyId=" + self.SelectCompany().ppCompanyCode(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if ((response !== null) && (response !== '')) {

                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {
                            var CompanyDTO = new CompanyModel();
                            CompanyDTO.MapEntity(ds, CompanyDTO);
                            self.CompanyHotel(CompanyDTO);
                            self.CompanyHotel().ppHotel(!self.SelectCompany().ppHotel());
                        }
                    }

                    loading(false);



                },
                error: function (errorResponse) {
                    loading(false);
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });

        }
    }

    self.SaveHotelClick = function () {


        var valid = true;

        try {


            if (self.SelectCompany().ppCompanyCode() == null || self.SelectCompany().ppCompanyCode() == undefined || self.SelectCompany().ppCompanyCode() == "") {
                ValidateMsgText = "The company code cannot be empty, please correct\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (valid === true) {

                loading(true, 'Please wait ...');

                self.CompanyHotel().ppCompanyCode(self.SelectCompany().ppCompanyCode());

                var newHotel = JSON.stringify(ko.toJS(self.CompanyHotel()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Companies/SaveHotelParameters",
                    async: false,
                    crossDomain: true,
                    data: newHotel,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.isSucess(true);
                            $("#CompanyMsg").show();
                            $("#CompanyMsg").html("<h5>" + "Company hotel saved successfully" + "</h5>").fadeIn(0);
                            $("#CompanyMsg").html("<h5>" + "Company hotel saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the hotel";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#CompanyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorList(err);
        }

    }

    self.CheckCompanyPrefix = function () {

    
        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanyPrefix?Prefix=" + self.CompanyHotel().ppConfirmPrefix(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    if (response !== "0") {
                        self.PrefixMsg("El Prefijo de Confirmación del Hotel ya existe, favor cambiar");
                    }
                    else { self.PrefixMsg("El Prefijo de Confirmación del Hotel es Válido"); }

                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }
    //hotel /////////////////////////////////////////////////////////////////

    //Combinations /////////////////////////////////////////////////////////////////
    self.GetCombinations = function () {

        loading(true, 'Please wait ...');

        self.CombinationsList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCombinations?CompanyID=" + self.SelectCompany().ppCompanyCode(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CombinationDTO = new CombinationsModel();
                            CombinationDTO.MapEntity(value, CombinationDTO);
                            self.CombinationsList.push(CombinationDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.EditCombination = function (data) {


        try {

            self.SelectedCombination(data);

        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }
    
    self.SaveCombinationClick = function () {

        try {

            loading(true, 'Please wait ...');

                 self.SelectedCombination().ppCompanyCode(self.SelectCompany().ppCompanyCode());

                var newCombination = JSON.stringify(ko.toJS(self.SelectedCombination()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Companies/SaveCombination",
                    async: false,
                    crossDomain: true,
                    data: newCombination,
                    contentType: 'application/json',
                    success: function (response) {
                        self.SelectedCombination(new CombinationsModel());
                        loading(false);
                        if (response === "0") {
                            self.GetCombinations();
                            $("#CompanyMsg").show();
                            $("#CompanyMsg").html("<h5>" + "combination saved successfully" + "</h5>").fadeIn(0);
                            $("#CompanyMsg").html("<h5>" + "combination saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the combination";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#CompanyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }

        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }


    self.DeleteCombinationClick = function (data) {

        try {

            loading(true, 'Please wait ...');

            self.SelectedCombination(data);

            self.SelectedCombination().ppCompanyCode(self.SelectCompany().ppCompanyCode());


            var newCombination = JSON.stringify(ko.toJS(self.SelectedCombination()));


            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Companies/DeleteCombination",
                async: false,
                crossDomain: true,
                data: newCombination,
                contentType: 'application/json',
                success: function (response) {

                    loading(false);
                    if (response === "0") {
                        self.GetCombinations();
                        self.SelectedCombination(new CombinationsModel());
                        $("#CompanyMsg").show();
                        $("#CompanyMsg").html("<h5>" + "combination deleted successfully" + "</h5>").fadeIn(0);
                        $("#CompanyMsg").html("<h5>" + "combination deleted successfully" + "</h5>").fadeOut(8000);
                    }
                    else {

                        ValidateMsgText = "An error has ocurred deleting the combination";
                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#CompanyMsg").dialog("close");
                    //$("#LocationMsg").hidden();
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });
        }

        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }
    //Combinations /////////////////////////////////////////////////////////////////


    //Services /////////////////////////////////////////////////////////////////
    self.GetCompanyServices = function () {

        self.CompanyService(new CompanyModel());

        if (self.SelectCompany() != null && self.SelectCompany() != undefined && self.SelectCompany().ppCompanyCode() != undefined && self.SelectCompany().ppCompanyCode() != null) {
            loading(true, 'Please wait ...');

            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanyServices?CompanyId=" + self.SelectCompany().ppCompanyCode(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if ((response !== null) && (response !== '')) {

                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {
                            var CompanyDTO = new CompanyModel();
                            CompanyDTO.MapEntity(ds, CompanyDTO);
                            self.CompanyService(CompanyDTO);
                            self.CompanyService().ppServices(!self.SelectCompany().ppServices());
                        }
                    }

                    loading(false);



                },
                error: function (errorResponse) {
                    loading(false);
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });

        }
    }

    self.SaveServiceClick = function () {


        var valid = true;

        try {


            if (self.SelectCompany().ppCompanyCode() == null || self.SelectCompany().ppCompanyCode() == undefined || self.SelectCompany().ppCompanyCode() == "") {
                ValidateMsgText = "The company code cannot be empty, please correct\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (valid === true) {

                loading(true, 'Please wait ...');

                self.CompanyService().ppCompanyCode(self.SelectCompany().ppCompanyCode());

                var newSrv = JSON.stringify(ko.toJS(self.CompanyService()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Companies/SaveCompanyServices",
                    async: false,
                    crossDomain: true,
                    data: newSrv,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.isSucess(true);
                            $("#CompanyMsg").show();
                            $("#CompanyMsg").html("<h5>" + "Service hotel saved successfully" + "</h5>").fadeIn(0);
                            $("#CompanyMsg").html("<h5>" + "Service hotel saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Service";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#CompanyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorList(err);
        }

    }

    self.CheckServicePrefix = function () {


        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanyServicePrefix?Prefix=" + self.CompanyService().ppConfirmPrefix(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    if (response !== "0") {
                        self.PrefixMsg("El Prefijo de Confirmación del servicio ya existe, favor cambiar");
                    }
                    else { self.PrefixMsg("El Prefijo de Confirmación del  servicio es Válido"); }

                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }
    //Services  /////////////////////////////////////////////////////////////////

    //self.GeneralClick = function () {
    //    $("#GeneralDiv").show();
    //    $("#ProcessorDiv").hide();
    //    $("#ServicesDiv").hide();
    //    $("#HotelDiv").hide();
    //    $("#GeneralTab").addClass("activePageMenu");
    //    $("#ProcessorTab").removeClass("activePageMenu");
    //    $("#HotelTab").removeClass("activePageMenu");
    //    $("#ServiceTab").removeClass("activePageMenu");
    //}

    //self.ProcessorClick = function () {
    //    $("#GeneralDiv").hide();
    //    $("#ProcessorDiv").show();
    //    $("#ServicesDiv").hide();
    //    $("#HotelDiv").hide();
    //    $("#GeneralTab").removeClass("activePageMenu");
    //    $("#ProcessorTab").addClass("activePageMenu");
    //    $("#HotelTab").removeClass("activePageMenu");
    //    $("#ServiceTab").removeClass("activePageMenu");
    //}


    //self.HotelClick = function () {
    //    $("#GeneralDiv").hide();
    //    $("#ProcessorDiv").hide();
    //    $("#ServicesDiv").hide();
    //    $("#HotelDiv").show();
    //    $("#GeneralTab").removeClass("activePageMenu");
    //    $("#ProcessorTab").removeClass("activePageMenu");
    //    $("#ServiceTab").removeClass("activePageMenu");
    //    $("#HotelTab").addClass("activePageMenu");
    //}


    //self.ServiceClick = function () {

        
    //    $("#GeneralDiv").hide();
    //    $("#ProcessorDiv").hide();
    //    $("#HotelDiv").hide();
    //    $("#ServicesDiv").show();
    //    $("#GeneralTab").removeClass("activePageMenu");
    //    $("#ProcessorTab").removeClass("activePageMenu");
    //    $("#HotelTab").removeClass("activePageMenu");
    //    $("#ServiceTab").addClass("activePageMenu");
    //}

    self.GeneralInfoClick = function () {

        $("#GeneralDiv").show();        
        $("#ProcessorDiv").show();


        if (self.SelectCompany().ppHotel() === true) {
            self.GetCompanyHotel();
            $("#HotelDiv").show();
        }
        else {

            $("#HotelDiv").hide();

        }

        if (self.SelectCompany().ppServices() === true) {
            self.GetCompanyServices();
            $("#ServicesDiv").show();
        }
        else {
            $("#ServicesDiv").hide();

        }


        $("#PhotosDiv").hide();
        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#PhotoTab").removeClass("activePageMenu");

    }

    self.LogoPhotosClick = function () {

        $("#GeneralDiv").hide();
        $("#ProcessorDiv").hide();
        $("#HotelDiv").hide();
        $("#ServicesDiv").hide();


        $("#PhotosDiv").show();
        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#PhotoTab").addClass("activePageMenu");

    }



    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetCompanies();
        self.GetTotalPageCount();

    }




    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               

               try {
                   loading(true, 'Please wait ...');

                   CompanyObj = new CompaniesVm();

                   ko.applyBindings(CompanyObj);

                   CompanyObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });
                  
                   //-- Photos
                   $('#txtUploadFile1').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile1").get(0).files;                           

                           var CompanyCode = CompanyObj.SelectCompany().ppCompanyCode();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=4&CompanyCode=" + CompanyCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=4&CompanyCode=" + CompanyCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile1").val('');
                               $("#Picture1").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?" + d.getTime() + "&PictureNum=4&CompanyCode=" + CompanyCode);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile2').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile2").get(0).files;
                           
                           var CompanyCode = CompanyObj.SelectCompany().ppCompanyCode();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=2&CompanyCode=" + CompanyCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=2&CompanyCode=" + CompanyCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile2").val('');
                               $("#Picture2").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?" + d.getTime() + "&PictureNum=2&CompanyCode=" + CompanyCode);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile3').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile3").get(0).files;
                           
                           var CompanyCode = CompanyObj.SelectCompany().ppCompanyCode();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=3&CompanyCode=" + CompanyCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=3&CompanyCode=" + CompanyCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile3").val('');
                               $("#Picture3").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?" + d.getTime() + "&PictureNum=3&CompanyCode=" + CompanyCode);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile4').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile4").get(0).files;
                           
                           var CompanyCode = CompanyObj.SelectCompany().ppCompanyCode();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=1&CompanyCode=" + CompanyCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=1&CompanyCode=" + CompanyCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile4").val('');
                               $("#Picture4").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?" + d.getTime() + "&PictureNum=1&CompanyCode=" + CompanyCode);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });

                   $('#txtUploadFile5').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile5").get(0).files;
                           
                           var CompanyCode = CompanyObj.SelectCompany().ppCompanyCode();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=5&CompanyCode=" + CompanyCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?PictureNum=5&CompanyCode=" + CompanyCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile5").val('');
                               $("#Picture5").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/CompanyPhotoHandler.ashx?" + d.getTime() + "&PictureNum=5&CompanyCode=" + CompanyCode);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });
                   //

                   loading(false);

                   $("#Companiesection").show(); //Show main div

               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//



function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


