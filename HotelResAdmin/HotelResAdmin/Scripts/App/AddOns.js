﻿function AddOnsVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.CompanyCode = ko.observable();
    self.getLogo = ko.observable();
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);
    self.ShowTransportRates = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.AddOnTypeList = ko.observableArray();
    self.AddOnsList = ko.observableArray();
    self.CultureList = ko.observableArray();

    self.SelectAddon = ko.observable(new AddOnsModel());
    self.isRefresh = ko.observable(false);

    self.selectedCulture = ko.observable(new CultureModel());
    self.selectedAddOnType = ko.observable(new AddOnsTypeModel());



    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable("a.addOnId");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.Picture1 = ko.observable("");
    self.Picture2 = ko.observable("");

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.GetAddOnTypes = function () {

        self.AddOnTypeList.removeAll();

        $.ajax({
            type: "GET",
            url:  $("#txtTrackingURLBase").val() + "/Addons/GetAddonsTypes",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var AddOnDTO = new AddOnsTypeModel();
                            AddOnDTO.MapEntity(value, AddOnDTO);
                            self.AddOnTypeList.push(AddOnDTO);

                        })
                    }
                }

    
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetCultureTypes = function () {

        self.CultureList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Addons/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetAddOns = function (Category) {

        loading(true, 'Please wait ...');

        self.AddOnsList.removeAll();

        $.ajax({
            type: "GET",
            url:  $("#txtTrackingURLBase").val() + "/Addons/GetAddonsPaging?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var AddOnDTO = new AddOnsModel();
                            AddOnDTO.MapEntity(value, AddOnDTO);
                            self.AddOnsList.push(AddOnDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/Addons/GetAddonsPagingCount?PageSize=" + self.selectedPageSize(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if (response != null) {

                        var PageCount = response;
                        self.totalPageCount(PageCount);
                        self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                    }

                },
                error: function (errorResponse) {
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowErrorList(errorMsg);
                }
            });
        
    }


    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetAddOns();
        self.GetTotalPageCount();
    }


    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetAddOns();

    }


    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetAddOns();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetAddOns();
   
        }
    }


    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetAddOns();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetAddOns();

        }
    }


    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable("a.addOnId");
        self.isAccending = ko.observable(true);
        self.GetAddOns();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }


    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        
        self.SelectAddon(new AddOnsModel());

        //self.SetAddonType("S");
        self.selectedAddOnType("S");

        $("#PhotoDiv").hide();
        $("#PhotoTab").hide();


    }


    self.ValidateData = function () {

        var valid = true;
        var validfromdate = false;
        var validtodate = false;
       
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectAddon().ppDescription() == null || self.SelectAddon().ppDescription() == undefined || self.SelectAddon().ppDescription() == "") {
            ValidateMsgText = "Addon name field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectAddon().ppAmount() == null || self.SelectAddon().ppAmount() == undefined || self.SelectAddon().ppAmount() == "") {
            ValidateMsgText = "Addon price field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectAddon().ppAddonType() == null || self.SelectAddon().ppAddonType() == undefined || self.SelectAddon().ppAddonType() == "") {
            ValidateMsgText = "Addon type  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectAddon().CultureID() == null || self.SelectAddon().CultureID() == undefined || self.SelectAddon().CultureID() == "") {
            ValidateMsgText = "Language type  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectAddon().UnFormatFrom() == null || self.SelectAddon().UnFormatFrom() == undefined
            || self.SelectAddon().UnFormatFrom() == "" || self.SelectAddon().UnFormatFrom() == "NaN-NaN-NaN") {
            ValidateMsgText = "valid from field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;            
        } else {
            validfromdate = true;
        }

        
        if (self.SelectAddon().UnFormatTo() == null || self.SelectAddon().UnFormatTo() == undefined
            || self.SelectAddon().UnFormatTo() == "" || self.SelectAddon().UnFormatTo() == "NaN-NaN-NaN") {
            ValidateMsgText = "valid to field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            validtodate = true;
        }

        if (validfromdate && validtodate) {
            dfrom = new Date(self.SelectAddon().UnFormatFrom().replace('-', '/'));
            dto = new Date(self.SelectAddon().UnFormatTo().replace('-', '/'));
            if (dfrom > dto) {
                ValidateMsgText = "From date is greater than To date\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }


        return valid;
    }

 
    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "FromDate") {
                        self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "ToDate") {
                        self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);


    }

    self.CloseRates = function () {

        $("#TransportRates").hide(200);
        self.ShowTransportRates(false);

    }
    


    self.SaveClick = function () {


        var valid = true;

        try
        {

          

            self.PrepareDtoForInsert();


            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newAddon = JSON.stringify(ko.toJS(self.SelectAddon()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Addons/SaveAddon",
                    async: false,
                    crossDomain: true,
                    data: newAddon,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.GetAddOns();
                            self.isSucess(true);
                            $("#AddonMsg").show();
                            $("#AddonMsg").html("<h5>" + "Addon saved successfully" + "</h5>").fadeIn(0);
                            $("#AddonMsg").html("<h5>" + "Addon saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText =  "An error has ocurred saving the Addon" ;
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#AddonMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }


    self.EditAddon = function (data) {


        try {
            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectAddon(data);

            self.AddOnClick();

            self.selectedFromDate(formatDate(self.SelectAddon().ppFrom(), '/'));
            self.selectedToDate(formatDate(self.SelectAddon().ppTo(), '/'));




            var filteredCulture;
            var CultureID;

            CultureID = $.trim(self.SelectAddon().CultureID());
            if (CultureID != null && CultureID != undefined && CultureID != '') {
                filteredCulture = Enumerable.From(self.CultureList()).Where(function (x) { return x.ppCultureID().toString().toUpperCase().match("^" + CultureID.toUpperCase()) }).FirstOrDefault()
                if (filteredCulture != null && filteredCulture != undefined) { self.selectedCulture(filteredCulture); }
                else { self.selectedCulture(new CultureModel()); }

            } else { self.selectedCulture(new CultureModel()); }


            var filteredType;
            var AddOnType;

            AddOnType = $.trim(self.SelectAddon().ppAddonType());
            if (AddOnType != null && AddOnType != undefined && AddOnType != '') {
                filteredType = Enumerable.From(self.AddOnTypeList()).Where(function (x) { return x.code().toString().toUpperCase().match("^" + AddOnType.toUpperCase()) }).FirstOrDefault()
                if (filteredType != null && filteredType != undefined) { self.selectedAddOnType(filteredType); }
                else { self.selectedAddOnType(new AddOnsTypeModel()); }

            } else { self.selectedAddOnType(new AddOnsTypeModel()); }


            self.SetAddonType(AddOnType);

            if ((data.ppAddonID() !== "") &&  (data.ppAddonID() !== "0") &&  (data.ppAddonID() !== undefined))
            {
                self.Picture1($("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=1&AddonID=" + data.ppAddonID());
                self.Picture2($("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=2&AddonID=" + data.ppAddonID());
                $("#PhotoTab").show();
                $("#PhotoDiv").hide();
            }
            else { 
                $("#PhotoDiv").hide();
                $("#PhotoTab").hide();
            }

            $("#AddonId").val(data.ppAddonID());

            loading(false);
        }
        catch (err) {
            loading(false);
        }


    }

    self.AddOnTypeChanged = function () {

        self.SetAddonType(self.selectedAddOnType().code());
    }

    self.SetAddonType = function (AddOnType) {


        $("#TransportRates").hide(200);
        self.ShowTransportRates(false);

        if (AddOnType === "M" || AddOnType === "N") {
            
            self.SelectAddon().DepartureEnable(false);
            self.SelectAddon().DurationEnable(false);
            self.SelectAddon().ppDuration("");
            self.SelectAddon().ppDeparture("");
        }

        if (AddOnType === "T") {
            self.SelectAddon().DepartureEnable(true);
            self.SelectAddon().DurationEnable(true);
        }

        if (AddOnType === "S") {
            self.SelectAddon().DepartureEnable(false);
            self.SelectAddon().DurationEnable(true);
            self.SelectAddon().ppDuration("");
            self.SelectAddon().ppDeparture("");
        }


        if (AddOnType === "X") {
            self.SelectAddon().DepartureEnable(false);
            self.SelectAddon().DurationEnable(true);
            self.SelectAddon().ppDeparture("");
            self.ShowTransportRates(true);
            
        }
        if (AddOnType === "O") {
            self.SelectAddon().DepartureEnable(false);
            self.SelectAddon().DurationEnable(true);
            self.SelectAddon().ppDuration("");
            self.SelectAddon().ppDeparture("");
        }

    }


    self.DeleteClick = function (data) {


        try
        {

        if (self.SelectAddon() != null && self.SelectAddon() != undefined && self.SelectAddon().ppAddonID() != undefined && self.SelectAddon().ppAddonID() != null) {

            self.isShowErrorPanel(false);
            $("#DeleteAddOn").html($("<h5>" + "Are you sure you want to delete the Addon?" + "</h5>"));



            $("#DeleteAddOn").dialog({
                resizable: false,
                modal: false,
                closeOnEscape: false,
                position: { at: "center  " },
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                },
                buttons: {
                    Yes: function () {
                        loading(true, 'Please wait ...');
                        $.ajax({
                            type: "POST",
                            url: $("#txtTrackingURLBase").val() + "/Addons/Delete?AddonId=" + self.SelectAddon().ppAddonID(),
                            async: false,
                            crossDomain: true,
                            contentType: 'application/json',
                            success: function (response) {

                                loading(false);
                                $("#DeleteAddOn").dialog("close");


                                if (response === "0") {
                                    self.GetAddOns();
                                    self.isSucess(true);

                                    $("#AddonMsg").show();
                                    $("#AddonMsg").html("<h5>" + "Addon deleted successfully" + "</h5>").fadeIn(0);
                                    $("#AddonMsg").html("<h5>" + "Addon deleted successfully" + "</h5>").fadeOut(3000);

                                }
                                else {
                                    $("#AddonMsg").show();
                                    $("#AddonMsg").html("<h5>" + "An error has ocurred deleting the Addon" + "</h5>").fadeIn(0);
                                    $("#AddonMsg").html("<h5>" + "An error has ocurred deleting the Addon" + "</h5>").fadeOut(3000);
                                    
                                }

                            },
                            error: function (errorResponse) {
                                loading(false);
                                $("#AddonMsg").dialog("close");
                                //$("#LocationMsg").hidden();
                                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                self.ShowErrorList(errorMsg);
                            }
                        });
                    },
                    No: function () {
                        loading(false);
                        $("#DeleteAddOn").dialog("close");

                    }
                }
            });

        }


        }
        catch (err) {
            loading(false);
        }

    }


    self.PrepareDtoForInsert = function () {


        if (self.selectedCulture() != null &&  self.selectedCulture().ppCultureID() != null && self.selectedCulture().ppCultureID() != undefined) {
            self.SelectAddon().CultureID(self.selectedCulture().ppCultureID());
        }

        if (self.selectedAddOnType() != null && self.selectedAddOnType().code() != null && self.selectedAddOnType().code() != undefined) {
            self.SelectAddon().ppAddonType(self.selectedAddOnType().code());
        }

 
        var DateFrom = GetUniversalDate(self.selectedFromDate());
        var DateTo = GetUniversalDate(self.selectedToDate());

        self.SelectAddon().UnFormatFrom(DateFrom);
        self.SelectAddon().UnFormatTo(DateTo);

        
    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }




    self.AddOnClick = function () {

        $("#AddOnEntryDiv").show();
        $("#PhotoDiv").hide();
        $("#AddonTab").addClass("activePageMenu");
        $("#PhotoTab").removeClass("activePageMenu");

    }


    self.PhotosClick = function () {

        $("#AddOnEntryDiv").hide();
        $("#PhotoDiv").show();
        $("#AddonTab").removeClass("activePageMenu");
        $("#PhotoTab").addClass("activePageMenu");

    }




    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetAddOnTypes();
        self.GetAddOns();
        self.GetTotalPageCount();
        self.GetCultureTypes();


        //$("#fileUploaderPicture1").uploadFile({
        //    url: $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx",
        //    maxNumberOfFiles: 1,
        //    dynamicFormData: function () {

        //        var data = {
        //            AddonID: self.SelectAddon().ppAddonID(),
        //            PictureNum : 1
        //        }
        //        return data;
        //    },
        //    onSuccess: function (files, data, xhr) {
        //        if (xhr != null && xhr.responseText.search("Error") != -1) {
        //            var errorMsg = xhr.responseText;
        //            self.ShowErrorList(errorMsg);
        //        }
        //        else
        //        {
                    

        //            self.Picture1($("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=1&AddonID=" + self.SelectAddon().ppAddonID());

        //        }

        //    },
        //    onError: function (files, status, errMsg) {
        //        var errorMsg = ($.parseJSON(errMsg));
        //        self.ShowErrorList(errorMsg);
        //    }

        //});

        //$("#fileUploaderPicture2").uploadFile({
        //    url: $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx",
        //    maxNumberOfFiles: 1,
        //    showPreview: true,
        //    dynamicFormData: function () {

        //        var data = {
        //            AddonID: self.SelectAddon().ppAddonID(),
        //            PictureNum: 2
        //        }
        //        return data;
        //    },
        //    onSuccess: function (files, data, xhr, pd) {
        //        if (xhr != null && xhr.responseText.search("Error") != -1) {
        //            var errorMsg = xhr.responseText;
        //            self.ShowErrorList(errorMsg);
        //        }
        //        else {
                    
        //            pd.preview.html("<img src='uploads/" + data[0] + "' />");

        //        }

        //    },

        //    onError: function (files, status, errMsg) {
        //        var errorMsg = ($.parseJSON(errMsg));
        //        self.ShowErrorList(errorMsg);
        //    }

        //});



 
    }




    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//

//$(document).ready(function () {

    


//});

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               
               
               try {
                   loading(true, 'Please wait ...');

                   //setTimeout(function () {

                   addonObj = new AddOnsVm();

                   ko.applyBindings(addonObj);

                   addonObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   //--
                   $('#txtUploadFile').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile").get(0).files;

                           var addonId = $("#AddonId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=2&AddonID=" + addonId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=2&AddonID=" + addonId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile").val('');
                               $("#Picture2").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?" + d.getTime() + "&PictureNum=2&AddonID=" + addonId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });

                   $('#txtUploadFile1').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile1").get(0).files;

                           var addonId = $("#AddonId").val();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=1&AddonID=" + addonId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?PictureNum=1&AddonID=" + addonId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();

                               $("#Picture1").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/AddOnHandler.ashx?" + d.getTime() + "&PictureNum=1&AddonID=" + addonId);
                               $("#txtUploadFile1").val('');
                               loading(false);
                           });


                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }

                       loading(false);
                   });
                   //--

                   //}, 400);

                   loading(false);

                   $("#AddOnSection").show(); //Show main div

                   $("input[name='file[]']").removeAttr("multiple");
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//



function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }            
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


