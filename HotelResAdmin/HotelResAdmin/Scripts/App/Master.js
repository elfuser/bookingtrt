﻿
var SelectedCompany;


function ReloadCompanies() {
    GetCompanies();
}


function GetCompanies() {

    loading(true, 'Please wait ...');
    var ddl_Companies = $("#ddl_Companies");

    $.ajax({
        type: "GET",
        //url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanies?OrderBy",
        url: $("#txtTrackingURLBase").val() + "/Security/GetUserCompany?UserID=-1",
        async: false,
        crossDomain: true,
        contentType: 'application/json',
        success: function (response) {
            if ((response !== null) && (response !== '')) {

                var ds = jQuery.parseJSON(response);

                if (ds !== null) {
                    $.each(ds, function (key, value) {
                        ddl_Companies.append($("<option />").val($.trim(value.ppCompanyCode)).text($.trim(value.ppCompanyName)));
                    })
                } else {
                    ddl_Companies.append($("<option />").val(-1).text("---"));
                }
            }

            loading(false);
        },
        error: function (errorResponse) {
            loading(false);
        }
    });
}


function GetSelectedCompany() {


    $.ajax({
        type: "GET",
        url: $("#txtTrackingURLBase").val() + "/Companies/GetSelectedCompany",
        async: false,
        crossDomain: true,
        contentType: 'application/json',
        success: function (response) {
            if ((response !== null) && (response !== '')) {
                SelectedCompany = response;

                $("#ddl_Companies").val(SelectedCompany);
                $("#ddl_Companies").change();

            }

        },
        error: function (errorResponse) {

        }
    });
}


 function  SetSelectedCompany(CompanyId) {

    $.ajax({
        type: "POST",
        url: $("#txtTrackingURLBase").val() + "/Companies/SetSelectedCompany?CompanyId=" + CompanyId,
        async: false,
        crossDomain: true,
        contentType: 'application/json',
        success: function (response) {
            location.reload();
        },
        error: function (errorResponse) {
            loading(false);
            alert(selected);
        }
    });



}

 function GetAndValidateUserProfile() {

     $.ajax({
         type: "GET",
         url: $("#txtTrackingURLBase").val() + "/Security/GetUserProfilePermissions?ProfileId=0",
         async: false,
         crossDomain: true,
         contentType: 'application/json',
         success: function (response) {
             if ((response !== null) && (response !== '')) {

                 var obj = jQuery.parseJSON(response);

                 if (obj !== null) {                     
                    //var ProfileDTO = new ProfileModel();
                    //ProfileDTO.MapEntity(value, ProfileDTO);
                    ValidateUserPermissions(obj);
                 }
             }
         },
         error: function (errorResponse) {
             loading(false);
         }
     });
 }

 function GetLoggedUser() {
     $("#lblUser").text('');
     $.ajax({
         type: "GET",
         url: $("#txtTrackingURLBase").val() + "/Security/GetLoggedUser",
         async: false,
         crossDomain: true,
         contentType: 'application/json',
         success: function (response) {
             if ((response !== null) && (response !== '')) {

                 var obj = response;

                 if (obj !== null) {
                     $("#lblUser").text(obj);
                 }
             }
         },
         error: function (errorResponse) {
             loading(false);
         }
     });
 }


 function ValidateUserPermissions(objProfile)
 {
     if (objProfile != null) {
         if (!objProfile.Dashboard) {
             $("#DivDashboard").parent().hide();
         }

         if (!objProfile.Company) {
             $('#DivAdmin li:contains("Companies")').hide();
         }

         if (!objProfile.Rates) {
             $('#DivHotel li:contains("Rates")').hide();
         }

         if (!objProfile.Rooms) {
             $('#DivHotel li:contains("Rooms")').hide();
         }

         if (!objProfile.Addons) {
             $('#DivHotel li:contains("Addons")').hide();
         }

         if (!objProfile.Services) {
             $("#DivServices").parent().hide();
         }

         if (!objProfile.Reservations) {
             $('#DivQueries li:contains("Reservations")').hide();
         }

         if (!objProfile.Avaibility) {
             $('#DivQueries li:contains("Availability")').hide();
             $('#DivQueries li:contains("Closing Alerts by Sales")').hide();
             $('#DivQueries li:contains("Closing Alerts General")').hide();
         }

         if (!objProfile.Comissions) {
             $('#DivQueries li:contains("Comissions")').hide();
         }

         if (!objProfile.Profiles) {
             $("#DivSecurity").parent().hide();
         }

         if (!objProfile.EventLog) {
             $("#EventLogTab").hide();
         }        
     }
 }

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {


               try {
                   loading(true, 'Please wait ...');

                   //MasterObj = new Mastervm();

                   //Validate user permissions
                   GetAndValidateUserProfile();

                   GetCompanies();
                   
                   
                   $('#ddl_Companies').selectpicker('refresh');

                   GetSelectedCompany();

                   $("#logout").click(function () {
                       loading(true, 'Please wait ...');                      

                       $.ajax({
                           type: "POST",
                           url: $("#txtTrackingURLBase").val() + "/Security/SignOut",
                           async: false,
                           crossDomain: true,
                           //data: obj,
                           contentType: 'application/json; charset=utf-8',
                           success: function (response) {
                               loading(false);
                               if (response != null) {
                                   var ResponseDTO = JSON.parse(response);
                               }

                               if (ResponseDTO.Status === "0") {                                   
                                   window.location = $("#txtTrackingURLBase").val() + "/Security/login";
                               }

                           },
                           error: function (errorResponse) {                              
                               var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);                               
                               bReturn = false;
                           }
                       });
                   })

                   GetLoggedUser();

                   loading(false);
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }


               $('.selectpicker').on('change', function () {
                   var company = $(this).find("option:selected").val();
                   SetSelectedCompany(company);
               });
           }
);
//*************************************//

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}





