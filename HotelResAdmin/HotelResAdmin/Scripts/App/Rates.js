﻿function RatesVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.DiscountType = ko.observable("general");

    self.isShowErrorEditDetail = ko.observable(false);
    self.isShowErrorEditDiscount = ko.observable(false);
    self.isShowErrorEditExcep = ko.observable(false);


    self.ErrorList = ko.observableArray();
    self.RatesList = ko.observableArray();
    self.RoomTypeList = ko.observableArray();

    self.ErrorListDetail = ko.observableArray();
    self.ErrorListDiscount = ko.observableArray();
    self.ErrorListExcep = ko.observableArray();
    

    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();


    self.selectedFromDateDetail = ko.observable();
    self.selectedToDateDetail = ko.observable();

    self.selectedFromDateDiscount = ko.observable();
    self.selectedToDateDiscount = ko.observable();


    self.selectedFromDateExcep = ko.observable();
    self.selectedToDateExcep = ko.observable();


    self.SelectedRate = ko.observable(new RatesHeaderModel());
    self.SelectedDeatilRate = ko.observable(new RatesDetailModel());
    self.SelectedDiscount = ko.observable(new RatesDiscountModel());
    self.selectedRoomType = ko.observable(new RoomsModel());
    self.SelectedExcRate = ko.observable(new RatesDetailModel());

    self.isRefresh = ko.observable(false);

    self.isEditingDetail = ko.observable(false);
    self.ShowGridDetailDetail = ko.observable(true);




    self.isEditingDisc = ko.observable(false);
    self.ShowGridDetailDisc = ko.observable(true);


    self.isEditingExcep = ko.observable(false);
    self.ShowGridDetailExcep = ko.observable(true);


    //DETAILS GRID ///////////////////////////////////////////////////////////
    self.RatesDetailList = ko.observableArray();
    self.totalPageCountDetail = ko.observable();
    self.pageNoLabelDetail = ko.observable();
    self.pageNumberDetail = ko.observable(1);
    self.selectedPageSizeDetail = ko.observable(10);
    self.pageSizeOptionsDetail = ko.observableArray();
    self.pageSizeOptionsDetail(['5', '10', '20']);
    self.orderByDetail = ko.observable("ratedet_id");
    self.isAccendingDetail = ko.observable(true);
    self.isUpDetail = ko.observable(false);
    self.isDownDetail = ko.observable(false);
    //DETAILS GRID ///////////////////////////////////////////////////////////

    //DISCOUNT GRID ///////////////////////////////////////////////////////////
    self.RatesDiscountList = ko.observableArray();
    self.totalPageCountDiscount = ko.observable();
    self.pageNoLabelDiscount = ko.observable();
    self.pageNumberDiscount = ko.observable(1);
    self.selectedPageSizeDiscount = ko.observable(10);
    self.pageSizeOptionsDiscount = ko.observableArray();
    self.pageSizeOptionsDiscount(['5', '10', '20']);
    self.orderByDiscount = ko.observable("RowID");
    self.isAccendingDiscount = ko.observable(true);
    self.isUpDiscount = ko.observable(false);
    self.isDownDiscount = ko.observable(false);
    //DISCOUNT GRID ///////////////////////////////////////////////////////////

    //EXCEPTION GRID ///////////////////////////////////////////////////////////
    self.RatesExcList = ko.observableArray();
    self.totalPageCountExc = ko.observable();
    self.pageNoLabelExc = ko.observable();
    self.pageNumberExc = ko.observable(1);
    self.selectedPageSizeExc = ko.observable(10);
    self.pageSizeOptionsExc = ko.observableArray();
    self.pageSizeOptionsExc(['5', '10', '20']);
    self.orderByExc = ko.observable("a.RowID");
    self.isAccendingExc = ko.observable(true);
    self.isUpExc = ko.observable(false);
    self.isDownExc = ko.observable(false);
    //EXCEPTION GRID ///////////////////////////////////////////////////////////


    self.isEditing = ko.observable(false);
    self.SelectAll = ko.observable(false);

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowErrorListDetail = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }


    self.ShowEditErrorListDetail = function (data) {
        self.ErrorListDetail.push({ ErrorText: data });
        self.isShowErrorEditDetail(true);
    }

    self.ShowEditErrorListDiscount = function (data) {
        self.ErrorListDiscount.push({ ErrorText: data });
        self.isShowErrorEditDiscount(true);
    }



    self.ShowEditErrorListExcep = function (data) {
        self.ErrorListExcep.push({ ErrorText: data });
        self.isShowErrorEditExcep(true);
    }


    self.ClearErrors = function () {
        self.ErrorList.removeAll();
        self.ErrorListDetail.removeAll();
        self.ErrorListDiscount.removeAll();
        self.ErrorListExcep.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorEditDetail(false);
        self.isShowErrorEditDiscount(false);
        self.isShowErrorEditExcep(false);

    }



    self.GetRoomTypes = function () {

        self.RoomTypeList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetRoomTypes",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var room = new RoomsModel();
                            room.MapEntity(value, room);
                            self.RoomTypeList.push(room);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }


    self.GetRatesHeader = function () {

        loading(true, 'Please wait ...');

        self.RatesList.removeAll();

        $.ajax({
            type: "GET",
            //url: $("#txtTrackingURLBase").val() + "/Rates/GetRates?SelectAll=" + self.SelectAll(),
            url: $("#txtTrackingURLBase").val() + "/Rates/GetRates?SelectAll=True" ,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var RateDTO = new RatesHeaderModel();
                            RateDTO.MapEntity(value, RateDTO);
                            self.RatesList.push(RateDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.GetRatesHeader();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }


    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.isEditingExcep(true);
        self.ShowGridDetailExcep(false);

        self.isEditingDetail (false);
        self.ShowGridDetailDetail(true);
        self.isEditingDisc (false);
        self.ShowGridDetailDisc(true);

        self.ClearErrors();

        self.SelectedRate(new RatesHeaderModel());

        self.GeneralInfoClick();
        $("#TabsDiv").hide();

   


    }



    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "FromDate") {
                        self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "FromDateDetail") {
                        self.selectedFromDateDetail(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "FromDateDiscount") {
                        self.selectedFromDateDiscount(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "FromDateExcep") {
                        self.selectedFromDateExcep(date.ConvertDateToMMDDYYFormat());
                    }

                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "ToDate") {
                        self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "ToDateDetail") {
                        self.selectedToDateDetail(date.ConvertDateToMMDDYYFormat());
                    }


                    if (inst.id == "ToDateDiscount") {
                        self.selectedToDateDiscount(date.ConvertDateToMMDDYYFormat());
                    }

                    if (inst.id == "ToDateExcep") {
                        self.selectedToDateExcep(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    self.ValidateData = function () {

        var valid = true;
        var validfromdate = false;
        var validtodate = false;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectedRate().ppRateCode() == null || self.SelectedRate().ppRateCode() == undefined || self.SelectedRate().ppRateCode() == "") {
            ValidateMsgText = "The Rate Code cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }



        if (self.SelectedRate().ppDescription() == null || self.SelectedRate().ppDescription() == undefined || self.SelectedRate().ppDescription() == "") {
            ValidateMsgText = "The Rate Description cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectedRate().ppMinLos() == null || self.SelectedRate().ppMinLos() == undefined || self.SelectedRate().ppMinLos() == "") {
            ValidateMsgText = "The Minimum Stay cannot be zero or empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectedRate().ppMaxLos() == null || self.SelectedRate().ppMaxLos() == undefined || self.SelectedRate().ppMaxLos() == "") {
            ValidateMsgText = "The Maximum Stay can't be zero, empty or less than Minimum Stay, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectedRate().ppRateDetail() == null || self.SelectedRate().ppRateDetail() == undefined || self.SelectedRate().ppRateDetail() == "") {
            ValidateMsgText = "The Rate Details cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;

        }


        if (self.SelectedRate().UnFormatFrom() == null || self.SelectedRate().UnFormatFrom() == undefined
            || self.SelectedRate().UnFormatFrom() == "" || self.SelectedRate().UnFormatFrom() == "NaN-NaN-NaN") {
            ValidateMsgText = "Start Date field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            validfromdate = true;
        }


        if (self.SelectedRate().UnFormatTo() == null || self.SelectedRate().UnFormatTo() == undefined
            || self.SelectedRate().UnFormatTo() == "" || self.SelectedRate().UnFormatTo() == "NaN-NaN-NaN") {
            ValidateMsgText = "End Date field  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            validtodate = true;
        }

        if (validfromdate && validtodate) {
            var dfrom = new Date(self.SelectedRate().UnFormatFrom().replace('-', '/'));
            var dto = new Date(self.SelectedRate().UnFormatTo().replace('-', '/'));
            if (dfrom > dto) {
                ValidateMsgText = "Start Date is greater than End Date\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }        

        return valid;
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.ClearErrors();

    }

    self.SaveClick = function () {

        var valid = true;

        try {



            var DateFrom = GetUniversalDate(self.selectedFromDate());
            var DateTo = GetUniversalDate(self.selectedToDate());

            self.SelectedRate().UnFormatFrom(DateFrom);
            self.SelectedRate().UnFormatTo(DateTo);

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newRate = JSON.stringify(ko.toJS(self.SelectedRate()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Rates/SaveHeader",
                    async: false,
                    crossDomain: true,
                    data: newRate,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.GetRatesHeader();
                            self.isSucess(true);
                            $("#RateMsg").show();
                            $("#RateMsg").html("<h5>" + "Rate saved successfully" + "</h5>").fadeIn(0);
                            $("#RateMsg").html("<h5>" + "Rate saved successfully" + "</h5>").fadeOut(8000);
                        }else if (response === "-2") {
                            ValidateMsgText = "Start Date is equal or greater than End Date";
                            self.ShowEditErrorList(ValidateMsgText);                          
                        } else if (response === "-3") {
                            ValidateMsgText = "Min Stay is greater than Max Stay";
                            self.ShowEditErrorList(ValidateMsgText);
                        } else {
                            ValidateMsgText = "An error has ocurred saving the Rate";
                            self.ShowEditErrorList(ValidateMsgText);
                        }                       

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#RateMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);
        }

    }

    self.DeleteClick = function (data) {


        try {

            if (self.SelectedRate() != null && self.SelectedRate() != undefined && self.SelectedRate().ppRateId() != undefined && self.SelectedRate().ppRateId() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteRates").html($("<h5>" + "Are you sure you want to delete the Rate?" + "</h5>"));



                $("#DeleteRates").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Rates/DeleteHeader?RateID=" + self.SelectedRate().ppRateId(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteRates").dialog("close");


                                    if (response === "0") {
                                        self.GetRatesHeader();
                                        self.isSucess(true);

                                        $("#RateMsg").show();
                                        $("#RateMsg").html("<h5>" + "Rate deleted successfully" + "</h5>").fadeIn(0);
                                        $("#RateMsg").html("<h5>" + "Rate deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#RateMsg").show();
                                        $("#RateMsg").html("<h5>" + "An error has ocurred deleting the Rate" + "</h5>").fadeIn(0);
                                        $("#RateMsg").html("<h5>" + "An error has ocurred deleting the Rate" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#RateMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteRates").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
            self.ShowErrorList(errorMsg);

        }

    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }

    self.GeneralInfoClick = function () {

        $("#GeneralInfoDiv").show();
        $("#DatesDiv").hide();
        $("#DiscountsDiv").hide();
        $("#ExcepDiv").hide();

        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#DatesTab").removeClass("activePageMenu");
        $("#DiscountsTab").removeClass("activePageMenu");
        $("#ExcepTab").removeClass("activePageMenu");

    }

    self.DatesClick = function () {

        $("#GeneralInfoDiv").hide();
        $("#DatesDiv").show();
        $("#DiscountsDiv").hide();
        $("#ExcepDiv").hide();

        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#DatesTab").addClass("activePageMenu");
        $("#DiscountsTab").removeClass("activePageMenu");
        $("#ExcepTab").removeClass("activePageMenu");

    }

    self.DiscountsClick = function () {

        $("#GeneralInfoDiv").hide();
        $("#DatesDiv").hide();
        $("#DiscountsDiv").show();
        $("#ExcepDiv").hide();

        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#DatesTab").removeClass("activePageMenu");
        $("#DiscountsTab").addClass("activePageMenu");
        $("#ExcepTab").removeClass("activePageMenu");

    }

    self.ExcepClick = function () {

        $("#GeneralInfoDiv").hide();
        $("#DatesDiv").hide();
        $("#DiscountsDiv").hide();
        $("#ExcepDiv").show();

        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#DatesTab").removeClass("activePageMenu");
        $("#DiscountsTab").removeClass("activePageMenu");
        $("#ExcepTab").addClass("activePageMenu");

    }

    self.EditRate = function (data) {


        try {

            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectedRate(data);


            $("#TabsDiv").show();
            self.GeneralInfoClick();

            self.selectedFromDate(formatDate(self.SelectedRate().ppStartDate(), '/'));
            self.selectedToDate(formatDate(self.SelectedRate().ppEndDate(), '/'));

            self.GeneralInfoClick();
            self.GetDetails();
            self.GetTotalPageCountDetail();

            self.GetDiscounts();
            self.GetTotalPageCountDiscount();


            self.GetExceptions();
            self.GetTotalPageCountExc();


            self.isEditingDetail(false);
            self.ShowGridDetailDetail(true);
            self.isEditingDisc(false);
            self.ShowGridDetailDisc(true);


            loading(false);
        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);

        }


    }


    //DETAILS GRID ///////////////////////////////////////////////////////////
    self.GetDetails = function () {

        loading(true, 'Please wait ...');
        self.RatesDetailList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetDetail?RatingId=" + self.SelectedRate().ppRateId() + "&OrderBy=" + self.orderByDetail() + '&IsAccending='
                + self.isAccendingDetail() + '&PageNumber=' + self.pageNumberDetail() + '&PageSize=' + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var DetailModel = new RatesDetailModel();
                            DetailModel.MapEntity(value, DetailModel);
                            self.RatesDetailList.push(DetailModel);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }

         
        });
    }

    self.GetTotalPageCountDetail = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetDetailCount?RatingId=" + self.SelectedRate().ppRateId() + "&PageSize=" + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCountDetail(PageCount);
                    self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeDetail = function () {

        self.pageNumberDetail(1);
        self.GetDetails();
        self.GetTotalPageCountDetail();
    }

    self.GetSelectedPageDetail = function () {

        if (self.pageNumberDetail() <= 0) {
            self.pageNumberDetail(1);
        }
        else {
            if (self.totalPageCountDetail() >= self.pageNumberDetail()) {
                var PageNo = parseInt(self.pageNumberDetail());
                self.pageNumberDetail(PageNo);
            }
            else {
                self.pageNumberDetail(self.totalPageCountDetail());
            }
        }

        self.GetDetails();

    }

    self.SetOrderByDetail = function (data) {
        if (data != self.orderByDetail()) {
            self.orderByDetail(data);
            self.isAccendingDetail(true)
            self.isDownDetail(false);
            self.isUpDetail(true);
            self.GetDetails();

        }
        else {
            if (self.isAccendingDetail() == true) {
                self.isAccendingDetail(false);
                self.isUpDetail(false);
                self.isDownDetail(true);

            }
            else {
                self.isAccendingDetail(true);
                self.isDownDetail(false);
                self.isisUpDetailUp(true);
            }

            self.GetDetails();

        }
    }

    self.ForwardPageClickDetail = function () {

        if (self.totalPageCountDetail() > self.pageNumberDetail()) {
            var PageNo = parseInt(self.pageNumberDetail());
            self.pageNumberDetail(PageNo + 1);
            self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());
            self.GetDetails();
        }
    }

    self.BackwardpageClickDetail = function () {
        if (self.pageNumberDetail() > 1) {
            var PageNo = parseInt(self.pageNumberDetail());
            self.pageNumberDetail(PageNo - 1);
            self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());

            self.GetDetails();

        }
    }

    self.RefreshClickDetail = function () {
        self.orderByDetail = ko.observable("ratedet_id");
        self.GetDetails();
        self.GetTotalPageCountDetail();
    }

    self.AddClickDetail = function () {
        self.SelectedDeatilRate(new RatesDetailModel());
        self.isEditingDetail(true);
        self.ShowGridDetailDetail(false);
    }

    self.DeleteClickDetail = function () {

        var valid = true;

        try {


            if (self.SelectedDeatilRate() != null && self.SelectedDeatilRate() != undefined ) {

                self.isShowErrorPanel(false);
                $("#DeleteRates").html($("<h5>" + "Are you sure you want to delete the Rate Date?" + "</h5>"));


                var RateDetail = JSON.stringify(ko.toJS(self.SelectedDeatilRate()));


                $("#DeleteRates").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Rates/DeleteRateDetail",
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                data: RateDetail,
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteRates").dialog("close");


                                    if (response === "0") {
                                        self.GetDetails();
                                    }
  

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteRates").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = err.message;
            self.ShowErrorList(errorMsg);

        }
    }

    self.RateEntryDetail = function (data) {


        self.SelectedDeatilRate(data);

        self.selectedFromDateDetail(formatDate(self.SelectedDeatilRate().ppStart_date(), '/'));
        self.selectedToDateDetail(formatDate(self.SelectedDeatilRate().ppEnd_date(), '/'));


        var filteredType;
        var TypeID;

        TypeID = $.trim(self.SelectedDeatilRate().ppRoomCode());
        if (TypeID != null && TypeID != undefined && TypeID != '') {
            filteredType = Enumerable.From(self.RoomTypeList()).Where(function (x) { return x.ppRoomTypeCode().toString().toUpperCase().match("^" + TypeID.toUpperCase()) }).FirstOrDefault()
            if (filteredType != null && filteredType != undefined) { self.selectedRoomType(filteredType); }
            else { self.selectedRoomType(new RoomsModel()); }

        } else { self.selectedRoomType(new RoomsModel()); }



        self.isEditingDetail(true);
        self.ShowGridDetailDetail(false);


   

    }

    self.CloseClickDetail = function (data) {


        self.isEditingDetail(false);
        self.ShowGridDetailDetail(true);
        self.ClearErrors();


    }

    self.ValidateDetail = function () {

        var valid = true;

        self.ErrorListDetail.removeAll();
        self.isShowErrorEditDetail(false);

        if (self.selectedFromDateDetail() == null || self.selectedFromDateDetail() == undefined || self.selectedFromDateDetail() == "") {
            self.ShowEditErrorListDetail("Start Date field is required");
            valid=false;
        }

        if (self.selectedToDateDetail() == null || self.selectedToDateDetail() == undefined || self.selectedToDateDetail() == "") {
            self.ShowEditErrorListDetail("End Date field is required");
            valid = false;
        }

        if (valid === false) { return;}


        var DateFrom = GetUniversalDate(self.selectedFromDateDetail());
        var DateTo = GetUniversalDate(self.selectedToDateDetail());

        self.SelectedDeatilRate().UnFormatFrom(DateFrom);
        self.SelectedDeatilRate().UnFormatTo(DateTo);
       
        var dfrom = new Date(self.SelectedDeatilRate().UnFormatFrom().replace('-', '/'));
        var dto = new Date(self.SelectedDeatilRate().UnFormatTo().replace('-', '/'));
        if (dfrom > dto) {
            ValidateMsgText = "Start Date is greater than End Date\n";
            self.ShowEditErrorListDetail(ValidateMsgText);
            valid = false;
        }              


        if (self.selectedRoomType() === null || self.selectedRoomType().ppRoomTypeID() === null || self.selectedRoomType().ppRoomTypeID() === undefined) {
            self.ShowEditErrorListDetail("Select a valid room type");
            valid = false;
            return;
        }

        if (self.SelectedRate() === null || self.SelectedRate().ppRateId() === null || self.SelectedRate().ppRateId() === undefined) {
            self.ShowEditErrorListDetail("Rate header not found");
            valid = false;
            return;
        }

        if (self.selectedRoomType() != null && self.selectedRoomType().ppRoomTypeID() != null && self.selectedRoomType().ppRoomTypeID() != undefined) {
            self.SelectedDeatilRate().ppRoomtype_id(self.selectedRoomType().ppRoomTypeID());
        }


        self.SelectedDeatilRate().ppRate_id(self.SelectedRate().ppRateId());

        return valid;
    }

    self.SaveClickDetail = function () {

        var valid = true;

        try {

            valid = self.ValidateDetail();


            if (valid === true) {



                loading(true, 'Please wait ...');

                var newRateDetail = JSON.stringify(ko.toJS(self.SelectedDeatilRate()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Rates/SaveDetail",
                    async: false,
                    crossDomain: true,
                    data: newRateDetail,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClickDetail();
                            self.GetDetails();
                        } else if (response === "-2") {
                            ValidateMsgText = "Start Date is equal or greater than End Date";
                            self.ShowEditErrorListDetail(ValidateMsgText);
                        }
                        else {
                            ValidateMsgText = "An error has ocurred saving the rate detail";
                            self.ShowEditErrorListDetail(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorListDetail(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorListDetail(err.message);
        }

    }



    //DETAILS GRID ///////////////////////////////////////////////////////////


    //DISCOUNT GRID ///////////////////////////////////////////////////////////
    self.GetDiscounts = function () {

        loading(true, 'Please wait ...');
        self.RatesDiscountList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetDiscounts?RateCode=" + self.SelectedRate().ppRateCode() + "&OrderBy=" + self.orderByDiscount() + '&IsAccending='
                + self.isAccendingDiscount() + '&PageNumber=' + self.pageNumberDiscount() + '&PageSize=' + self.selectedPageSizeDiscount(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var DiscountModel = new RatesDiscountModel();
                            DiscountModel.MapEntity(value, DiscountModel);
                            self.RatesDiscountList.push(DiscountModel);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.GetTotalPageCountDiscount = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetDiscountsCount?RateCode=" + self.SelectedRate().ppRateCode() + "&PageSize=" + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCountDiscount(PageCount);
                    self.pageNoLabelDiscount(self.pageNumberDiscount() + " of " + self.totalPageCountDiscount());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeDiscount = function () {

        self.pageNumberDiscount(1);
        self.GetDiscounts();
        self.GetTotalPageCountDiscount();
    }

    self.GetSelectedPageDiscount = function () {

        if (self.pageNumberDiscount() <= 0) {
            self.pageNumberDiscount(1);
        }
        else {
            if (self.totalPageCountDiscount() >= self.pageNumberDiscount()) {
                var PageNo = parseInt(self.pageNumberDiscount());
                self.pageNumberDiscount(PageNo);
            }
            else {
                self.pageNumberDiscount(self.totalPageCountDiscount());
            }
        }

        self.GetDiscounts();

    }

    self.SetOrderByDiscount = function (data) {
        if (data != self.orderByDiscount()) {
            self.orderByDiscount(data);
            self.isAccendingDiscount(true)
            self.isDownDiscount(false);
            self.isUpDiscount(true);
            self.GetDiscounts();

        }
        else {
            if (self.isAccendingDiscount() == true) {
                self.isAccendingDiscount(false);
                self.isUpDiscount(false);
                self.isDownDiscount(true);

            }
            else {
                self.isAccendingDiscount(true);
                self.isDownDiscount(false);
                self.isisUpDiscountUp(true);
            }

            self.GetDiscounts();

        }
    }

    self.ForwardPageClickDiscount = function () {

        if (self.totalPageCountDiscount() > self.pageNumberDiscount()) {
            var PageNo = parseInt(self.pageNumberDiscount());
            self.pageNumberDiscount(PageNo + 1);
            self.pageNoLabelDiscount(self.pageNumberDiscount() + " of " + self.totalPageCountDiscount());
            self.GetDiscounts();
        }
    }

    self.BackwardpageClickDiscount = function () {
        if (self.pageNumberDiscount() > 1) {
            var PageNo = parseInt(self.pageNumberDiscount());
            self.pageNumberDiscount(PageNo - 1);
            self.pageNoLabelDiscount(self.pageNumberDiscount() + " of " + self.totalPageCountDiscount());

            self.GetDiscounts();

        }
    }

    self.RefreshClickDiscount = function () {
        self.orderByDiscount = ko.observable("RowID");
        self.GetDiscounts();
        self.GetTotalPageCountDiscount();
    }

    self.AddClickDiscount = function () {
        self.SelectedDiscount(new RatesDiscountModel());
        self.DiscountType("general");
        self.isEditingDisc(true);
        self.ShowGridDetailDisc(false);
        self.selectedFromDateDiscount("");
        self.selectedToDateDiscount("");
    }

    self.EditRateDiscount = function (data) {


        self.SelectedDiscount(data);

        self.selectedFromDateDiscount(formatDate(self.SelectedDiscount().ppStartDate(), '/'));
        self.selectedToDateDiscount(formatDate(self.SelectedDiscount().ppEndDate(), '/'));

        self.isEditingDisc(true );
        self.ShowGridDetailDisc(false);


        if (self.SelectedDiscount().ppDiscount() > "0") {
            self.DiscountType("general");
        }
        else
        {
            self.DiscountType("night");
        }
    }

    self.DeleteClickDiscount = function () {

        var valid = true;

        try {


            if (self.SelectedDiscount() != null && self.SelectedDiscount() != undefined) {

                self.isShowErrorPanel(false);
      
                var RowId = self.SelectedDiscount().ppRowID();

                $("#DeleteRates").html($("<h5>" + "Are you sure you want to delete the discount?" + "</h5>"));
                $("#DeleteRates").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Rates/DeleteRateDiscount?rowID=" + RowId,
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteRates").dialog("close");


                                    if (response === "0") {
                                        self.GetDiscounts();
                                    }


                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowEditErrorListDiscount(err.message);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteRates").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = err.message;
            self.ShowErrorList(errorMsg);

        }
    }

    self.CloseClickDiscount = function (data) {


        self.isEditingDisc(false);
        self.ShowGridDetailDisc(true);
        self.ClearErrors();


    }

    self.ValidateDiscount = function () {

        var valid = true;

        self.ErrorListDiscount.removeAll();

        if (self.selectedFromDateDiscount() == null || self.selectedFromDateDiscount() == undefined || self.selectedFromDateDiscount() == "") {

            self.ShowEditErrorListDiscount("Start Date field is required");
            valid = false;
        }


        if (self.selectedToDateDiscount() == null || self.selectedToDateDiscount() == undefined || self.selectedToDateDiscount() == "") {
            self.ShowEditErrorListDiscount("End Date field is required");
            valid = false;
        }

        if (valid === false) { return; }


        var DateFrom = GetUniversalDate(self.selectedFromDateDiscount());
        var DateTo = GetUniversalDate(self.selectedToDateDiscount());

        self.SelectedDiscount().UnFormatFrom(DateFrom);
        self.SelectedDiscount().UnFormatTo(DateTo);

        var dfrom = new Date(self.SelectedDiscount().UnFormatFrom().replace('-', '/'));
        var dto = new Date(self.SelectedDiscount().UnFormatTo().replace('-', '/'));
        if (dfrom > dto) {
            ValidateMsgText = "Start Date is greater than End Date\n";
            self.ShowEditErrorListDiscount(ValidateMsgText);
            valid = false;
        }       

        if (self.SelectedRate() === null || self.SelectedRate().ppRateCode() === null || self.SelectedRate().ppRateCode() === undefined) {
            self.ShowEditErrorListDiscount("Rate header not found");
            valid = false;
            return;
        }


        self.SelectedDiscount().ppRateCode(self.SelectedRate().ppRateCode());

        return valid;
    }

    self.SaveClickDiscount = function () {

        var valid = true;

        try {

            valid = self.ValidateDiscount();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newRateDiscount = JSON.stringify(ko.toJS(self.SelectedDiscount()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Rates/SaveRateDiscount",
                    async: false,
                    crossDomain: true,
                    data: newRateDiscount,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.CloseClickDiscount();
                            self.GetDiscounts();
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the rate detail";
                            self.ShowEditErrorListDiscount(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorListDiscount(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorListDiscount(err.message);
        }

    }





    //DISCOUNT GRID ///////////////////////////////////////////////////////////

    //EXCEPTION GRID ///////////////////////////////////////////////////////////
    self.GetExceptions = function () {

        loading(true, 'Please wait ...');
        self.RatesExcList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetRatesException?RateCode=" + self.SelectedRate().ppRateCode() + "&OrderBy=" + self.orderByExc() + '&IsAccending='
                + self.isAccendingExc() + '&PageNumber=' + self.pageNumberExc() + '&PageSize=' + self.selectedPageSizeExc(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var ExcModel = new RatesDetailModel();
                            ExcModel.MapEntity(value, ExcModel);
                            self.RatesExcList.push(ExcModel);


                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.GetTotalPageCountExc = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rates/GetExceptionCount?RateCode=" + self.SelectedRate().ppRateCode() + "&PageSize=" + self.selectedPageSizeExc(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCountExc(PageCount);
                    self.pageNoLabelExc(self.pageNumberExc() + " of " + self.totalPageCountExc());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeExc = function () {

        self.pageNumberExc(1);
        self.GetExceptions();
        self.GetTotalPageCountExc();
    }

    self.GetSelectedPageExc = function () {

        if (self.pageNumberExc() <= 0) {
            self.pageNumberExc(1);
        }
        else {
            if (self.totalPageCountExc() >= self.pageNumberExc()) {
                var PageNo = parseInt(self.pageNumberExc());
                self.pageNumberExc(PageNo);
            }
            else {
                self.pageNumberExc(self.totalPageCountExc());
            }
        }

        self.GetExceptions();

    }

    self.SetOrderByExc = function (data) {
        if (data != self.orderByExc()) {
            self.orderByExc(data);
            self.isAccendingExc(true)
            self.isDownExc(false);
            self.isUpExc(true);
            self.GetExceptions();

        }
        else {
            if (self.isAccendingExc() == true) {
                self.isAccendingExc(false);
                self.isUpExc(false);
                self.isDownExc(true);

            }
            else {
                self.isAccendingExc(true);
                self.isDownExc(false);
                self.isisUpExcUp(true);
            }

            self.GetExceptions();

        }
    }

    self.ForwardPageClickExc = function () {

        if (self.totalPageCountExc() > self.pageNumberExc()) {
            var PageNo = parseInt(self.pageNumberExc());
            self.pageNumberExc(PageNo + 1);
            self.pageNoLabelExc(self.pageNumberExc() + " of " + self.totalPageCountExc());
            self.GetExceptions();
        }
    }

    self.BackwardpageClickExc = function () {
        if (self.pageNumberExc() > 1) {
            var PageNo = parseInt(self.pageNumberExc());
            self.pageNumberExc(PageNo - 1);
            self.pageNoLabelExc(self.pageNumberExc() + " of " + self.totalPageCountExc());

            self.GetExceptions();

        }
    }

    self.RefreshClickExc = function () {
        self.orderByExc = ko.observable("a.RowID");
        self.GetExceptions();
        self.GetTotalPageCountExc();
    }

    self.AddClickExc = function () {
        self.SelectedExcRate(new RatesDetailModel());
        self.isEditingExcep(true);
        self.ShowGridDetailExcep(false);
        self.selectedFromDateExcep("");
        self.selectedToDateExcep("");
    }


    self.CloseClickExcep = function (data) {
        self.isEditingExcep(false);
        self.ShowGridDetailExcep(true);
        self.ClearErrors();
    }

    self.EditRateExc = function (data) {

        self.SelectedExcRate(data);

        self.selectedFromDateExcep(formatDate(self.SelectedExcRate().ppStart_date(), '/'));
        self.selectedToDateExcep(formatDate(self.SelectedExcRate().ppEnd_date(), '/'));


        self.SelectedExcRate().ppStart_dateAnt(self.SelectedRate().ppStrStartDate());
        self.SelectedExcRate().ppEnd_dateAnt(self.SelectedRate().ppStrEndDate());

        self.isEditingExcep(true);
        self.ShowGridDetailExcep(false);

    }


    self.ValidateExcep= function () {

        var valid = true;

        self.ErrorListExcep.removeAll();

        if (self.selectedFromDateExcep() == null || self.selectedFromDateExcep() == undefined || self.selectedFromDateExcep() == "") {
            self.ShowEditErrorListExcep("Start Date field is required");
            valid = false;
        }


        if (self.selectedToDateExcep() == null || self.selectedToDateExcep() == undefined || self.selectedToDateExcep() == "") {
            self.ShowEditErrorListExcep("End Date field is required");
            valid = false;
        }

        if (valid === false) { return; }


        var DateFrom = GetUniversalDate(self.selectedFromDateExcep());
        var DateTo = GetUniversalDate(self.selectedToDateExcep());

        self.SelectedExcRate().UnFormatFrom(DateFrom);
        self.SelectedExcRate().UnFormatTo(DateTo);

        var dfrom = new Date(self.SelectedExcRate().UnFormatFrom().replace('-', '/'));
        var dto = new Date(self.SelectedExcRate().UnFormatTo().replace('-', '/'));
        if (dfrom > dto) {
            ValidateMsgText = "Start Date is greater than End Date\n";
            self.ShowEditErrorListExcep(ValidateMsgText);
            valid = false;
        }        

        if (self.selectedRoomType() === null || self.selectedRoomType().ppRoomTypeID() === null || self.selectedRoomType().ppRoomTypeID() === undefined) {
            self.ShowEditErrorListExcep("Select a valid room type");
            valid = false;
            return;
        }

        if (self.SelectedRate() === null || self.SelectedRate().ppRateCode() === null || self.SelectedRate().ppRateCode() === undefined) {
            self.ShowEditErrorListExcep("Rate header not found");
            valid = false;
            return;
        }

        if (self.selectedRoomType() != null && self.selectedRoomType().ppRoomTypeCode() != null && self.selectedRoomType().ppRoomTypeCode() != undefined) {
            self.SelectedExcRate().ppRoomCode(self.selectedRoomType().ppRoomTypeCode());
        }


        self.SelectedExcRate().ppRateCode(self.SelectedRate().ppRateCode());

        return valid;
    }

    
    self.SaveClickExcep = function () {
        var valid = true;

        try {

            valid = self.ValidateExcep();


            if (valid === true) {

                loading(true, 'Please wait ...');

                var newRateExcep = JSON.stringify(ko.toJS(self.SelectedExcRate()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Rates/SaveRateException",
                    async: false,
                    crossDomain: true,
                    data: newRateExcep,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.CloseClickExcep();
                            self.GetExceptions();
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the exception rate";
                            self.ShowEditErrorListExcep(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorListExcep(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorListExcep(err.message);
        }

    }


    self.DeleteClickExc = function () {


        var valid = true;

        try {

 
                self.isShowErrorPanel(false);

                var RateExcep = JSON.stringify(ko.toJS(self.SelectedExcRate()));

                $("#DeleteRates").html($("<h5>" + "Are you sure you want to delete the rate exception?" + "</h5>"));
                $("#DeleteRates").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Rates/DeleteRateExcDates",
                                async: false,
                                crossDomain: true,
                                data: RateExcep,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteRates").dialog("close");


                                    if (response === "0") {
                                        self.GetExceptions();
                                    }


                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteRates").dialog("close");

                        }
                    }
                });

        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);

        }
    }
    //EXCEPTION GRID ///////////////////////////////////////////////////////////


    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetRatesHeader();
        self.GetRoomTypes();


        this.DiscTypeButton = ko.observable("general").extend({ notify: 'always' });

        this.DiscTypeButton.subscribe(function (checkedVal) {
            alert(checkedVal);
        }, this);

    }



    ko.bindingHandlers["bsChecked"] = {
        init: function (element, valueAccessor) {
            ko.utils.registerEventHandler(element, "change", function (event) {
                var check = $(event.target);
                var type = check.val();

                if (type=="night")
                {
                    self.SelectedDiscount().ppDiscount("0");
                }
                else{
                    self.SelectedDiscount().ppDisc1("0");
                    self.SelectedDiscount().ppDisc2("0");
                    self.SelectedDiscount().ppDisc3("0");
                    self.SelectedDiscount().ppDisc4("0");
                    self.SelectedDiscount().ppDisc5("0");
                    self.SelectedDiscount().ppDisc6("0");
                    self.SelectedDiscount().ppDisc7("0");
                    self.SelectedDiscount().ppDisc8("0");
                }

            });
        }

    };


    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTOR END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               

               try {
                   loading(true, 'Please wait ...');


                   RatesObj = new RatesVm();

                   ko.applyBindings(RatesObj);

                   RatesObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   loading(false);

                   $("#RateSection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#ErrorDiv").html(strMessage);
    
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


