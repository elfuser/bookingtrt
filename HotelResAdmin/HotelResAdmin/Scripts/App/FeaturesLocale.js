﻿function FeaturesLocaleVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.FeaturesList = ko.observableArray();
    self.CultureList = ko.observableArray();

    self.FeatureList = ko.observableArray();
    self.SelectedFeatureList = ko.observableArray();

    self.SelectFeatures = ko.observable(new FeatureModel());
    self.isRefresh = ko.observable(false);

    self.selectedCulture = ko.observable(new CultureModel());

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable(" fl.feature_id  ");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }



    self.GetCultureTypes = function () {

        self.CultureList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Addons/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }



    self.GetFeatures = function (Category) {

        loading(true, 'Please wait ...');

        self.FeaturesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Feature/GetFeatures?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var FeaturesDTO = new FeatureModel();
                            FeaturesDTO.MapEntity(value, FeaturesDTO);
                            self.FeaturesList.push(FeaturesDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Feature/GetPagingCount?PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }


    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetFeatures();
        self.GetTotalPageCount();
    }

    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetFeatures();

    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetFeatures();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetFeatures();

        }
    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetFeatures();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetFeatures();

        }
    }

    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy ("fl.feature_id");
        self.isAccending (true);
        self.GetFeatures();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }

    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.SelectFeatures(new FeatureModel());

    }

    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectFeatures().ppDescription() == null || self.SelectFeatures().ppDescription() == undefined || self.SelectFeatures().ppDescription() == "") {
            ValidateMsgText = "Features description field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        return valid;
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);


    }

    self.SaveClick = function () {

        var valid = true;

        try {


            if (self.selectedCulture() != null && self.selectedCulture().ppCultureID() != null && self.selectedCulture().ppCultureID() != undefined) {
                self.SelectFeatures().CultureId(self.selectedCulture().ppCultureID());
            }

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newFeatures = JSON.stringify(ko.toJS(self.SelectFeatures()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Feature/Save",
                    async: false,
                    crossDomain: true,
                    data: newFeatures,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.CloseClick();
                            self.GetFeatures();
                            self.isSucess(true);
                            $("#FeatureMsg").show();
                            $("#FeatureMsg").html("<h5>" + "Features saved successfully" + "</h5>").fadeIn(0);
                            $("#FeatureMsg").html("<h5>" + "Features saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Features";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#FeatureMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            alert(err);
        }

    }


    self.EditFeatures = function (data) {


        try {




            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectFeatures(data);

  
            var filteredCulture;
            var CultureID;

            CultureID = $.trim(self.SelectFeatures().CultureId());
            if (CultureID != null && CultureID != undefined && CultureID != '') {
                filteredCulture = Enumerable.From(self.CultureList()).Where(function (x) { return x.ppCultureID().toString().toUpperCase().match("^" + CultureID.toUpperCase()) }).FirstOrDefault()
                if (filteredCulture != null && filteredCulture != undefined) { self.selectedCulture(filteredCulture); }
                else { self.selectedCulture(new CultureModel()); }

            } else { self.selectedCulture(new CultureModel()); }


            loading(false);
        }
        catch (err) {
            loading(false);
            alert(err);
        }


    }


    self.DeleteClick = function (data) {


        try {

            if (self.SelectFeatures() != null && self.SelectFeatures() != undefined && self.SelectFeatures().ppFeatureID() != undefined && self.SelectFeatures().ppFeatureID() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteFeature").html($("<h5>" + "Are you sure you want to delete the Feature?" + "</h5>"));



                $("#DeleteFeature").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Feature/Delete?FeatureID=" + self.SelectFeatures().ppFeatureID() + "&CultureId=" + self.SelectFeatures().CultureId(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteFeature").dialog("close");


                                    if (response === "0") {
                                        self.GetFeatures();
                                        self.isSucess(true);

                                        $("#FeatureMsg").show();
                                        $("#FeatureMsg").html("<h5>" + "Features deleted successfully" + "</h5>").fadeIn(0);
                                        $("#FeatureMsg").html("<h5>" + "Features deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#FeatureMsg").show();
                                        $("#FeatureMsg").html("<h5>" + "An error has ocurred deleting the Features" + "</h5>").fadeIn(0);
                                        $("#FeatureMsg").html("<h5>" + "An error has ocurred deleting the Features" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#FeatureMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteFeature").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
            alert(err);
        }

    }



    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }


    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetFeatures();
        self.GetTotalPageCount();
        self.GetCultureTypes();
    }



    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               

               try {
                   loading(true, 'Please wait ...');


                   //var element = document.getElementById("Featuresection");
                   FeaturesObj = new FeaturesLocaleVm();

                   ko.applyBindings(FeaturesObj);

                   FeaturesObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   loading(false);

                   $("#FeaturesSection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


