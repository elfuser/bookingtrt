﻿function Usersvm(mainVm) {

    var self = this;


    self.appData = mainVm;

    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isEditingUsers = ko.observable(false);
    self.ShowEditUser = ko.observable(false);
    self.isShowMsgProfile = ko.observable(false);
    self.ErrorList = ko.observableArray();

    self.isAgency = ko.observable(false);

    self.AgenciesList = ko.observableArray();

    self.selectedAgency = ko.observable(new AgenciesHeaderModel());

    self.selectedProfile = ko.observable(new ProfileModel());
    self.ProfileList = ko.observableArray();
    self.RootlVm = ko.observable();

    //users grid ////////////////////////////////////
    self.isShowMsgUser = ko.observable(false);
    self.SelectedUser = ko.observable(new UsersModel());
    self.SelectedProfileUser = ko.observable(new ProfileModel());

    self.selectedUserHotel = ko.observable(new CompanyModel());
    self.UserHotelList = ko.observableArray();

    self.UsersList = ko.observableArray();

    self.totalPageCountUsers = ko.observable();
    self.pageNoLabelUsers = ko.observable();
    self.pageNumberUsers = ko.observable(1);
    self.selectedPageSizeUsers = ko.observable(10);
    self.pageSizeOptionsUsers = ko.observableArray();
    self.pageSizeOptionsUsers(['5', '10', '20']);

    self.orderByUsers = ko.observable("user_name");
    self.isAccendingUsers = ko.observable(true);
    self.isUpUsers = ko.observable(false);
    self.isDownUsers = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.EditinExistingUser = ko.observable(false);

    self.UserAssingHotelList = ko.observableArray();
    //users grid ////////////////////////////////////

    self.isResetPassword = ko.observable(false);

    self.HotelList = ko.observableArray();


    //Log grid ////////////////////////////////////
    self.SelectedLogUser = ko.observable(new UsersModel());
    self.UserLogList = ko.observableArray();
    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();
    self.CompanyUserLogList = ko.observableArray();

    self.totalPageCountUsersLog = ko.observable();
    self.pageNoLabelUsersLog = ko.observable();
    self.pageNumberUsersLog = ko.observable(1);
    self.selectedPageSizeUsersLog = ko.observable(10);
    self.pageSizeOptionsUsersLog = ko.observableArray();
    self.pageSizeOptionsUsersLog(['5', '10', '20']);

    self.orderByUsersLog = ko.observable("LogID");
    self.isAccendingUsersLog = ko.observable(true);
    self.isUpUsersLog = ko.observable(false);
    self.isDownUsersLog = ko.observable(false);
    //Log grid ////////////////////////////////////

    self.RootlVm(mainVm);

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }


    //profiles ///////////////////////////////////////////////////////////
    self.GetProfileList = function () {

        self.ProfileList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +  "/Security/GetProfiles",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var ProfileDTO = new ProfileModel();
                            ProfileDTO.MapEntity(value, ProfileDTO);
                            self.ProfileList.push(ProfileDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.SaveProfileClick = function () {

        var valid = true;

        try {


            self.ErrorList.removeAll();
            self.isShowErrorPanel(false);


            if (self.selectedProfile() === null || self.selectedProfile().ppProfileName() === null
                || self.selectedProfile().ppProfileName() === undefined || self.selectedProfile().ppProfileName() === '') {
                self.ShowErrorList("Please enter the profile Name");
                return;
            }


            loading(true, 'Please wait ...');

            var newProfile = JSON.stringify(ko.toJS(self.selectedProfile()));

            $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Security/SaveProfile",
                    async: false,
                    crossDomain: true,
                    data: newProfile,
                    contentType: 'application/json',
                    success: function (response) {
                        if (response !== "-1") {
                            self.GetProfileList();
                            self.selectedProfile(new ProfileModel());
                            loading(false);
                            self.isShowMsgProfile(true);
                            $("#ProfileMsg").show();
                            $("#ProfileMsg").html("<h5>" + "Profile saved successfully" + "</h5>").fadeIn(0);
                            $("#ProfileMsg").html("<h5>" + "Profile saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            loading(false);
                            ValidateMsgText = "An error has ocurred saving the Profile";
                            self.ShowErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ProfileMsg").dialog("close");
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });


        }
        catch (err) {
            alert(ex);
            loading(false);
        }

    }

    self.DeleteProfileClick = function (data) {


        try {

            self.selectedProfile(data);

            if (self.selectedProfile() != null && self.selectedProfile() != undefined && self.selectedProfile().ppProfileID() != undefined && self.selectedProfile().ppProfileID() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteProfile").html($("<h5>" + "Are you sure you want to delete the Profile?" + "</h5>"));



                $("#DeleteProfile").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Security/DeleteProfile?ProfileId=" + self.selectedProfile().ppProfileID(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    self.GetProfileList();
                                    self.selectedProfile(new ProfileModel());
                                    loading(false);
                                    $("#DeleteProfile").dialog("close");


                                    if (response === "0") {
                                        self.GetProfileList();
                                        $("#ProfileMsg").show();
                                        $("#ProfileMsg").html("<h5>" + "Room deleted successfully" + "</h5>").fadeIn(0);
                                        $("#ProfileMsg").html("<h5>" + "Room deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#ProfileMsg").show();
                                        $("#ProfileMsg").html("<h5>" + "An error has ocurred deleting the Profile" + "</h5>").fadeIn(0);
                                        $("#ProfileMsg").html("<h5>" + "An error has ocurred deleting the Profile" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#DeleteProfile").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteProfile").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            alert(ex);
            loading(false);
        }

    }

    self.CancelClick = function () {

        try
        {
            loading(true, 'Please wait ...');
            self.GetProfileList();
            self.selectedProfile(new ProfileModel());
            self.ErrorList.removeAll();
            self.isShowErrorPanel(false);
            $("#ErrorListDiv").hide();

            loading(false);
        }
        catch (ex) {
            alert(ex);
            loading(false);
        }

    }

    self.InitModel = function () {
        self.isShowErrorPanel(false);
        self.GetProfileList();
        self.GetUsers();
        self.GettotalPageCountUsers();
        self.GetCompanies();
        self.GetUsersCompany();
        self.GetAgenciesHeader();
    }
    //profiles ///////////////////////////////////////////////////////////


    ///users/////////////////////////////////////////////////////////////

    self.GetCompanies = function () {

        loading(true, 'Please wait ...');
        

        self.UserHotelList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Companies/GetCompanies?OrderBy",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CompanyDTO = new CompanyModel();
                            CompanyDTO.MapEntity(value, CompanyDTO);
                            self.UserHotelList.push(CompanyDTO);
                        })
                    }

                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
            }
        });
    }

    self.GetUsers = function () {

        loading(true, 'Please wait ...');

        self.UsersList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/GetUsers?OrderBy=" + self.orderByUsers() + '&IsAccending='
                + self.isAccendingUsers() + '&PageNumber=' + self.pageNumberUsers() + '&PageSize=' + self.selectedPageSizeUsers(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var UserDTO = new UsersModel();
                            UserDTO.MapEntity(value, UserDTO);
                            self.UsersList.push(UserDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GettotalPageCountUsers = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/GetUsersPagingCount?PageSize=" + self.selectedPageSizeUsers(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCountUsers(PageCount);
                    self.pageNoLabelUsers(self.pageNumberUsers() + " of " + self.totalPageCountUsers());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }



    self.GetAgenciesHeader = function () {

        loading(true, 'Please wait ...');

        self.AgenciesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgencieByCompany",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var objDTO = new AgenciesHeaderModel();
                            objDTO.MapEntity(value, objDTO);
                            self.AgenciesList.push(objDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetSelectedPageSizeUsers = function () {

        self.pageNumberUsers(1);
        self.GetUsers();
        self.GettotalPageCountUsers();
    }

    self.GetSelectedPageUsers = function () {

        if (self.pageNumberUsers() <= 0) {
            self.pageNumberUsers(1);
        }
        else {
            if (self.totalPageCountUsers() >= self.pageNumberUsers()) {
                var PageNo = parseInt(self.pageNumberUsers());
                self.pageNumberUsers(PageNo);
            }
            else {
                self.pageNumberUsers(self.totalPageCountUsers());
            }
        }

        self.GetUsers();

    }

    self.ForwardPageClickUsers = function () {

        if (self.totalPageCountUsers() > self.pageNumberUsers()) {
            var PageNo = parseInt(self.pageNumberUsers());
            self.pageNumberUsers(PageNo + 1);
            self.pageNoLabelUsers(self.pageNumberUsers() + " of " + self.totalPageCountUsers());
            self.GetUsers();
        }
    }

    self.BackwardpageClickUsers = function () {
        if (self.pageNumberUsers() > 1) {
            var PageNo = parseInt(self.pageNumberUsers());
            self.pageNumberUsers(PageNo - 1);
            self.pageNoLabelUsers(self.pageNumberUsers() + " of " + self.totalPageCountUsers());

            self.GetUsers();

        }
    }

    self.SetOrderByUsers = function (data) {
        if (data != self.orderByUsers()) {
            self.orderByUsers(data);
            self.isAccendingUsers(true)
            self.isDownUsers(false);
            self.isUpUsers(true);
            self.GetUsers();

        }
        else {
            if (self.isAccendingUsers() == true) {
                self.isAccendingUsers(false);
                self.isUpUsers(false);
                self.isDownUsers(true);

            }
            else {
                self.isAccendingUsers(true);
                self.isDownUsers(false);
                self.isUpUsers(true);
            }

            self.GetUsers();

        }
    }

    self.RefreshUsersClick = function () {
        self.isEditingUsers(false);
        self.pageNumberUsers(1);
        self.orderByUsers = ko.observable("user_name");
        self.isAccendingUsers = ko.observable(true);
        self.GetUsers();
        self.GettotalPageCountUsers();
        self.ErrorList.removeAll();
    }

    self.EditUser = function (data) {
        try {

            self.ErrorList.removeAll();
            self.isShowErrorEdit(false);
            self.isEditingUsers(true);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.SelectedUser(data);
            self.isResetPassword(false);

            self.ShowEditUser(true);
            self.EditinExistingUser(true);

            var filteredProfile;
            var ProfileID;

            ProfileID = $.trim(self.SelectedUser().ppProfileID());
            if (ProfileID != null && ProfileID != undefined && ProfileID != '') {
                filteredProfile = Enumerable.From(self.ProfileList()).Where(function (x) { return x.ppProfileID().toString().toUpperCase().match("^" + ProfileID.toUpperCase()) }).FirstOrDefault()
                if (filteredProfile != null && filteredProfile != undefined) { self.SelectedProfileUser(filteredProfile); }
                else { self.SelectedProfileUser(new ProfileModel()); }

            } else { self.SelectedProfileUser(new ProfileModel()); }



            var AgencyID = $.trim(self.SelectedUser().AgencyCode());
            if (AgencyID != null && AgencyID != undefined && AgencyID != '') {
                filteredAgency = Enumerable.From(self.AgenciesList()).Where(function (x) { return x.ppAgencyCode().toString().toUpperCase().match("^" + AgencyID.toUpperCase()) }).FirstOrDefault()
                if (filteredAgency != null && filteredAgency != undefined) { self.selectedAgency(filteredAgency); }
                else { self.selectedAgency(new AgenciesHeaderModel()); }

            } else { self.selectedAgency(new AgenciesHeaderModel()); }



            self.GetUserCompanies(self.SelectedUser().ppUserID());


            if (self.SelectedUser().isAgencyUser() === true)
            { self.isAgency(true); } else { self.isAgency(false); }
           

            loading(false);
        }
        catch (err) {
            loading(false);
            alert(err);
        }


    }

    self.AgencyChange = function (data) {

        if (self.SelectedUser().isAgencyUser() === true)
        { self.isAgency(true); } else { self.isAgency(false); }

    }

    self.ResetPassword = function (data) {

        loading(true, 'Please wait ...');

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.SelectedUser(data);
        self.isEditingUsers(true);
        self.isShowErrorPanel(false);
        self.EditinExistingUser(true);
        self.isResetPassword(true);
        self.ShowEditUser(false);
        self.SelectedUser().ppPassword("");
        self.SelectedUser().ppPasswordConfirmation("");

        loading(false);
    }

    self.SavePasswordClick = function () {

        try
        {
                
            self.ErrorList.removeAll();
            self.isShowErrorEdit(false);
            
            if (self.SelectedUser().ppisDomainUser() == false || self.SelectedUser().ppisDomainUser() === '') {
                if (self.SelectedUser() === null || self.SelectedUser().ppPassword() === null
                    || self.SelectedUser().ppPassword() === undefined || self.SelectedUser().ppPassword() === '') {
                    self.ShowEditErrorList("Please enter the password");
                    return;
                }


                if (self.SelectedUser().ppPassword() !== self.SelectedUser().ppPasswordConfirmation()) {
                    self.ShowEditErrorList("Your new and confirmation passwords do not match, please check");
                    return;
                }
            }
            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/ResetPasword?UserID=" + self.SelectedUser().ppUserID() + "&password=" + self.SelectedUser().ppPassword(),
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if (response === "0") {
                        loading(false);
                        self.isShowMsgProfile(true);
                        $("#ProfileMsg").show();
                        $("#ProfileMsg").html("<h5>Password changed ok</h5>").fadeIn(0);
                        $("#ProfileMsg").html("<h5>Password changed ok</h5>").fadeOut(8000);
                        self.isEditingUsers(false);
                        self.EditinExistingUser(false);
                    }
                    else {
                        var ValidateMsgText = "An error has ocurred saving the user information";
                        loading(false);
                        if (response === "-1") { ValidateMsgText = "Password is not valid: minimum 8 characters, at least 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character (@#$%^&+=)"; }

                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });


        
        }
        catch (err) {
            alert(err);
            loading(false);
        }
    }

    self.GetUserCompanies = function (UserId) {

        loading(true, 'Please wait ...');


        self.UserAssingHotelList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/GetUserCompany?UserID=" + UserId,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CompanyDTO = new CompanyModel();
                            CompanyDTO.MapEntity(value, CompanyDTO);
                            self.UserAssingHotelList.push(CompanyDTO);
                        })
                    }

                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
            }
        });
    }

    self.DeleteAssignHotelClick = function (data) {

        try {

            if (self.SelectedUser() === null || self.SelectedUser().ppUserID() === null
                || self.SelectedUser().ppUserID() === undefined || self.SelectedUser().ppUserID() === '') {
                self.ShowEditErrorList("Please select the user");
                return;
            }

            //if (self.selectedUserHotel() === null || self.selectedUserHotel().ppCompanyCode() === null
            //    || self.selectedUserHotel().ppCompanyCode() === undefined || self.selectedUserHotel().ppCompanyCode() === '') {
            //    self.ShowEditErrorList("Please select the hotel");
            //    return;
            //}
            //var HotelId = self.selectedUserHotel().ppCompanyCode();

            var HotelId = $.trim(data.ppCompanyCode());

            if (HotelId == null || HotelId == '') {
                self.ShowEditErrorList("Please select the hotel");
                return;
            }            
            
                       
            var UserId = self.SelectedUser().ppUserID();

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/DeleteUserCompany?UserID=" + UserId + "&CompanyCode=" + HotelId,
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if (response === "0") {
                        self.GetUserCompanies(UserId);
                    }
                    else {
                        var ValidateMsgText = "An error has ocurred deleting the user information";
                        loading(false);
       
                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });



        }
        catch (err) {
            alert(err);
            loading(false);
        }



    }

    self.SaveUserHotelClick = function () {

        try
        {
            if (self.SelectedUser() === null || self.SelectedUser().ppUserID() === null
                || self.SelectedUser().ppUserID() === undefined || self.SelectedUser().ppUserID() === '') {
                self.ShowEditErrorList("Please select the user");
                return;
            }

            if (self.selectedUserHotel() === null || self.selectedUserHotel().ppCompanyCode() === null
                || self.selectedUserHotel().ppCompanyCode() === undefined || self.selectedUserHotel().ppCompanyCode() === '') {
                self.ShowEditErrorList("Please select the hotel");
                return;
            }

            var UserId = self.SelectedUser().ppUserID();
            var HotelId = self.selectedUserHotel().ppCompanyCode();



            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/SaveUserCompany?UserID=" + UserId + "&CompanyCode=" + HotelId,
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if (response === "0") {
                        self.GetUserCompanies(UserId);
                    }
                    else {
                        var ValidateMsgText = "An error has ocurred saving the user information";
                        loading(false);

                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });



        }
        catch (err) {
            alert(err);
            loading(false);
        }


    }

    self.AddUsersClick = function () {

        self.isEditingUsers(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.SelectedUser(new UsersModel());
        self.isResetPassword(false); 
        self.EditinExistingUser(false);
        self.ShowEditUser(true);

    }

    self.SaveUsersClick = function () {
        var valid = true;

        try {

            self.ErrorList.removeAll();
            self.isShowErrorEdit(false);

            if (self.SelectedUser() === null || self.SelectedUser().ppFirstName() === null
                || self.SelectedUser().ppFirstName() === undefined || self.SelectedUser().ppFirstName() === '') {
                self.ShowEditErrorList("Please enter the First Name");
                return;
            }

            if (self.SelectedUser() === null || self.SelectedUser().ppLastName() === null
                || self.SelectedUser().ppLastName() === undefined || self.SelectedUser().ppLastName() === '') {
                self.ShowEditErrorList("Please enter the Last Name");
                return;
            }



            if (self.SelectedUser() === null || self.SelectedUser().ppUserName() === null
                || self.SelectedUser().ppUserName() === undefined || self.SelectedUser().ppUserName() === '') {
                self.ShowEditErrorList("Please enter the User Name");
                return;
            }
            
            if (self.SelectedUser().ppisDomainUser() == false || self.SelectedUser().ppisDomainUser() === '') {
                if (self.SelectedUser() === null || self.SelectedUser().ppPassword() === null
                    || self.SelectedUser().ppPassword() === undefined || self.SelectedUser().ppPassword() === '') {
                    self.ShowEditErrorList("Please enter the Password");
                    return;
                }


                if (self.SelectedUser().ppPassword() !== self.SelectedUser().ppPasswordConfirmation()) {
                    self.ShowEditErrorList("Your new and confirmation passwords do not match, please check");
                    return;
                }
            }

            if (self.SelectedProfileUser() === null || self.SelectedProfileUser().ppProfileID() === null
                || self.SelectedProfileUser().ppProfileID() === undefined || self.SelectedProfileUser().ppProfileID() === '') {
                self.ShowEditErrorList("Please select a valid profile");
                return;
            }
            
            self.SelectedUser().ppProfileID(self.SelectedProfileUser().ppProfileID());

            if (self.selectedAgency() != null) {
                self.SelectedUser().AgencyCode(self.selectedAgency().ppAgencyCode());
            }
                    
            loading(true, 'Please wait ...');

            var newUser = JSON.stringify(ko.toJS(self.SelectedUser()));

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/SaveUser",
                async: false,
                crossDomain: true,
                data: newUser,
                contentType: 'application/json',
                success: function (response) {
                    if (response === "") {
                        self.GetUsers();
                        self.SelectedUser(new UsersModel());
                        loading(false);
                        self.isShowMsgUser(true);
                        $("#UserMsg").show();
                        $("#UserMsg").html("<h5>" + "Profile saved successfully" + "</h5>").fadeIn(0);
                        $("#UserMsg").html("<h5>" + "Profile saved successfully" + "</h5>").fadeOut(8000);
                        self.isEditingUsers(false);
                        self.EditinExistingUser(false);
                    }
                    else {
                        var ValidateMsgText = "An error has ocurred saving the user information";
                        loading(false);
                        if (response === "-1") { ValidateMsgText = "Password is not valid: minimum 8 characters, at least 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character (@#$%^&+=)"; } 

                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });



        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }

    self.CloseUserClick = function (data) {
        try {

            self.ErrorList.removeAll();
            self.isShowErrorEdit(false);
            self.isEditingUsers(false);
            self.isResetPassword(false);
            self.isShowErrorPanel(false);

        }
        catch (err) {
            alert(err);
        }

    }

    self.DeleteUsersClick = function (data) {

        try {
            if (self.SelectedUser() === null || self.SelectedUser().ppUserID() === null
                || self.SelectedUser().ppUserID() === undefined || self.SelectedUser().ppUserID() === '') {
                self.ShowEditErrorList("Please select the user");
                return;
            }

            loading(true, 'Please wait ...');

            var UserId = self.SelectedUser().ppUserID();

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/DeleteUser?UserID=" + UserId,
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    loading(false);
                    if (response === "0") {
                        self.GetUsers();
                        self.SelectedUser(new UsersModel());
                    }
                    else {
                        var ValidateMsgText = "An error has ocurred deleting the user information";
                        loading(false);

                        self.ShowEditErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });

            loading(false);

        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }

    self.BlockUser = function (data) {
        try {

            self.ErrorList.removeAll();
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            

            var newUser = new UsersModel();

            newUser.ppUserID(data.ppUserID());
            newUser.ppActive(data.ppActive());


            var User = JSON.stringify(ko.toJS(newUser));

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/BlockUser",
                async: false,
                crossDomain: true,
                data: User,
                contentType: 'application/json',
                success: function (response) {
                    if (response !== "") {
                        self.GetUsers();
                        self.SelectedUser(new UsersModel());
                        loading(false);
                        self.isShowMsgProfile(true);
                        $("#ProfileMsg").show();
                        $("#ProfileMsg").html("<h5>" + response + "</h5>").fadeIn(0);
                        $("#ProfileMsg").html("<h5>" + response + "</h5>").fadeOut(8000);

                    }
                    else {
                        
                        loading(false);
                        var ValidateMsgText = "An error has ocurred ";
                        self.ShowErrorList(ValidateMsgText);

                    }

                },
                error: function (errorResponse) {
                    loading(false);
                    $("#UserMsg").dialog("close");
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.ShowEditErrorList(errorMsg);
                }
            });



            loading(false);
        }
        catch (err) {
            loading(false);
            alert(err);
        }


    }
    

    //users/////////////////////////////////////////////////////////////

    ///Event log grid ////////////////////////////////////////////////////

    self.GetUsersCompany = function () {

        loading(true, 'Please wait ...');

        self.CompanyUserLogList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/GetUsersCompany",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var UserDTO = new UsersModel();
                            UserDTO.MapEntity(value, UserDTO);
                            self.CompanyUserLogList.push(UserDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetUserLog = function () {

        try {


            self.ErrorList.removeAll();
            self.isShowErrorPanel(false);
            self.UserLogList.removeAll();


            if (self.SelectedLogUser() === null || self.SelectedLogUser().ppUserID() === null
                || self.SelectedLogUser().ppUserID() === undefined || self.SelectedLogUser().ppUserID() === '') {
                self.ShowErrorList("Please select the user");
                return;
            }



            if (self.selectedFromDate() === null || self.selectedFromDate() === undefined || self.selectedFromDate() === '') {
                self.ShowErrorList("Please select the from date");
                return;
            }

            if (self.selectedToDate() === null || self.selectedToDate() === undefined || self.selectedToDate() === '') {
                self.ShowErrorList("Please select the to date");
                return;
            }

            var dfrom = self.selectedFromDate();
            var dto = self.selectedToDate();
            if (dfrom > dto) {                
                self.ShowErrorList("Start Date is greater than End Date");
                return;
            }

            var DateFrom = GetUniversalDate(self.selectedFromDate());
            var DateTo = GetUniversalDate(self.selectedToDate());

            loading(true, 'Please wait ...');

            var UserID;


            UserID = self.SelectedLogUser().ppUserID();


            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/Security/GetUserLog?OrderBy=" + self.orderByUsersLog() + '&IsAccending='
                    + self.isAccendingUsersLog() + '&PageNumber=' + self.pageNumberUsersLog() + '&PageSize=' + self.selectedPageSizeUsersLog() +
                    "&UserID=" + UserID + "&FromDt=" + DateFrom + "&ToDt=" + DateTo,
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if ((response !== null) && (response !== '')) {

                        var ds = jQuery.parseJSON(response);

                        if (ds !== null) {
                            $.each(ds, function (key, value) {
                                var UserLodDTO = new UserLogModel();
                                UserLodDTO.MapEntity(value, UserLodDTO);
                                self.UserLogList.push(UserLodDTO);
                            })
                        }

                        self.GettotalPageCountUsersLog(UserID,DateFrom,DateTo);
                    }

                    loading(false);
                },
                error: function (errorResponse) {
                    loading(false);
                }
            });

            loading(false);
        }
        catch (err) {
            loading(false);
            alert(err);
        }
    }

    self.GettotalPageCountUsersLog = function (UserID,DateFrom,DateTo) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/GetUsersLogPagingCount?PageSize=" + self.selectedPageSizeUsersLog() + "&UserID=" + UserID + "&FromDt=" + DateFrom + "&ToDt=" + DateTo,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCountUsersLog(PageCount);
                    self.pageNoLabelUsersLog(self.pageNumberUsersLog() + " of " + self.totalPageCountUsersLog());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeUsersLog = function () {

        self.pageNumberUsersLog(1);
        self.GetUserLog();
        self.GettotalPageCountUsersLog();
    }

    self.GetSelectedPageUsersLog = function () {

        if (self.pageNumberUsersLog() <= 0) {
            self.pageNumberUsersLog(1);
        }
        else {
            if (self.totalPageCountUsersLog() >= self.pageNumberUsersLog()) {
                var PageNo = parseInt(self.pageNumberUsersLog());
                self.pageNumberUsersLog(PageNo);
            }
            else {
                self.pageNumberUsersLog(self.totalPageCountUsersLog());
            }
        }

        self.GetUserLog();

    }

    self.ForwardPageClickUsersLog = function () {

        if (self.totalPageCountUsersLog() > self.pageNumberUsersLog()) {
            var PageNo = parseInt(self.pageNumberUsersLog());
            self.pageNumberUsersLog(PageNo + 1);
            self.pageNoLabelUsersLog(self.pageNumberUsersLog() + " of " + self.totalPageCountUsersLog());
            self.GetUserLog();
        }
    }

    self.BackwardpageClickUsersLog = function () {
        if (self.pageNumberUsersLog() > 1) {
            var PageNo = parseInt(self.pageNumberUsersLog());
            self.pageNumberUsersLog(PageNo - 1);
            self.pageNoLabelUsersLog(self.pageNumberUsersLog() + " of " + self.totalPageCountUsersLog());

            self.GetUserLog();

        }
    }

    self.SetOrderByUsersLog = function (data) {
        if (data != self.orderByUsersLog()) {
            self.orderByUsersLog(data);
            self.isAccendingUsersLog(true)
            self.isDownUsersLog(false);
            self.isUpUsersLog(true);
            self.GetUserLog();

        }
        else {
            if (self.isAccendingUsersLog() == true) {
                self.isAccendingUsersLog(false);
                self.isUpUsersLog(false);
                self.isDownUsersLog(true);

            }
            else {
                self.isAccendingUsersLog(true);
                self.isDownUsersLog(false);
                self.isUpUsersLog(true);
            }

            self.GetUserLog();

        }
    }

    //////////////////////////////////////////////////////////////////////////////////////

    self.ProfilesClick = function () {
        $("#ProfilesDiv").show();
        $("#UsersDiv").hide();
        $("#EventLogTDiv").hide();
        $("#ProfilesTab").addClass("activePageMenu");
        $("#UsersTab").removeClass("activePageMenu");
        $("#EventLogTab").removeClass("activePageMenu");
    }

    self.UsersClick = function () {
        $("#ProfilesDiv").hide();
        $("#UsersDiv").show();
        $("#EventLogTDiv").hide();
        $("#ProfilesTab").removeClass("activePageMenu");
        $("#UsersTab").addClass("activePageMenu");
        $("#EventLogTab").removeClass("activePageMenu");
    }

    self.EventLogClick = function () {
        $("#ProfilesDiv").hide();
        $("#UsersDiv").hide();
        $("#EventLogTDiv").show();
        $("#ProfilesTab").removeClass("activePageMenu");
        $("#UsersTab").removeClass("activePageMenu");
        $("#EventLogTab").addClass("activePageMenu");
    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }


    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "FromDate") {
                        self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };

    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "ToDate") {
                        self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };


}



//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               $("#dialog").dialog({
                   autoOpen: false,
                   modal: true,
                   buttons: {
                       Ok: function () {
                           $(this).dialog("close");
                       }
                   }
               });

               try {
                   loading(true, 'Please wait ...');


                   //var element = document.getElementById("Roomection");
                   UsersObj = new Usersvm();

                   ko.applyBindings(UsersObj);

                   UsersObj.InitModel();

                   loading(false);
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


