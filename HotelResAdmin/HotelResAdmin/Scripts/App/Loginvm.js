﻿function loginvm() {

    var self = this;
    self.userName = ko.observable();
    self.password = ko.observable();
    self.isuserNameMissing = ko.observable();
    self.isPasswordMissing = ko.observable();
    self.rememberme = ko.observable();
    self.errorMessage = ko.observable();

    self.isLogin = ko.observable(false);
    self.ChangeloginPage = ko.observable(false);
    self.loginPage = ko.observable(true);
    self.userName($.cookie("userNamecookie"));
    self.password($.cookie("passwordcookie"));
    self.UserToken = ko.observable();

    self.paneldata = true;

    self.isProcessRunning = ko.observable(false);
    self.loginInfo;
    self.IsPageLoaded = false;

    self.IsvmApplied = false;
    self.HideUserName = ko.observable();
    self.HidePassWord = ko.observable();

    self.NewPassword = ko.observable()
    self.ConfirmPassword = ko.observable()


    self.isCurrentPasswordMissing = ko.observable(false);
    self.isNewPasswordMissing = ko.observable(false);
    self.isConfirmPasswordMissing = ko.observable(false);


    ko.bindingHandlers.loginwithEnterKey = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var allBindings = allBindingsAccessor();
            $(element).keypress(function (event) {
                var keyCode = (event.which ? event.which : event.keyCode);
                if (keyCode === 13) {
                    allBindings.loginwithEnterKey.call(viewModel);
                    return false;
                }
                return true;
            });
        }
    };

    ko.bindingHandlers.stopBinding = {
        init: function () {
            return { controlsDescendantBindings: self.paneldata };
        }
    };

    self.Login = function () {
        self.doLogin();
    }

    self.doLogin = function () {


        self.loginPage(true);

        if (self.userName() === "" || self.userName() === undefined || self.userName() === null) {
            self.isuserNameMissing(true);
            self.isLogin(false);
        }
        else {
            self.isuserNameMissing(false);
        }

        if (self.password() === "" || self.password() === null || self.password() === undefined) {
            self.isPasswordMissing(true);
            self.isLogin(false);
        }
        else {
            self.isPasswordMissing(false);
        }

        if (self.isPasswordMissing() === false && self.isuserNameMissing() === false) {

            if (self.userName()) {

                self.userName(self.userName().replace("&", "%26"));
            }

            loading(true, 'Please wait ...');

            var params = {
                userName: self.userName(),
                Password: self.password()
            };

            var obj = JSON.stringify(params);

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/DoLogin?userName=" + self.userName() + "&Password=" + self.password(),
                async: false,
                crossDomain: true,
                data: obj,
                contentType: 'application/json',
                success: function (response) {
                    loading(false);
                    if (self.rememberme() == true) {
                        $.cookie("userNamecookie", self.userName(), { expires: 10000 });
                        $.cookie("passwordcookie", self.password(), { expires: 10000 });
                    }
                    if (response != null) {
                        //var ResponseDTO = new ResponseModel();
                        //ResponseDTO.MapEntity(response, ResponseDTO);

                        var ResponseDTO = JSON.parse(response);
                    }


                    if (ResponseDTO.Status === "-1") {
                        self.errorMessage(ResponseDTO.Message);
                    }


                    if (ResponseDTO.Status === "0") {
                        //self.errorMessage(ResponseDTO.Message);
                        //go to main page........
                        if (ResponseDTO.ReturnUrl !== null && ResponseDTO.ReturnUrl !== undefined && ResponseDTO.ReturnUrl !== '') {
                            //window.location = ResponseDTO.ReturnUrl;
                            //return;
                        }

                        window.location = $("#txtTrackingURLBase").val() + "/Dashboard/index";

                    }

                    if (ResponseDTO.Status === "1") {
                        self.errorMessage(ResponseDTO.Message);
                        //go to change password........
                        self.loginPage(false);
                        self.ChangeloginPage(true);
                    }



                    self.isLogin(false);
                },
                error: function (errorResponse) {
                    loading(false);
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.errorMessage(errorMsg);
                    self.isLogin(false);
                }
            });
        }
        
        loading(false);
    }

    self.doLoginWindows = function () {
        self.doLoginWindows();
    }

    self.doLoginWindows = function () {


        self.loginPage(true);

       

            if (self.userName()) {

                self.userName(self.userName().replace("&", "%26"));
            }

            loading(true, 'Please wait ...');

            var params = {
                userName: self.userName(),
                Password: self.password()
            };


            var obj = JSON.stringify(params);

            $.ajax({
                type: "GET",
                url: $("#txtTrackingURLBase").val() + "/Security/GetLoginWindows",
                async: false,
                crossDomain: true,
                contentType: 'application/json',
                success: function (response) {
                    if ((response !== null) && (response !== '')) {

                        var obj = response;

                        if (obj !== null) {
                            alert(obj);
                        }
                    }
                },
                error: function (errorResponse) {
                    loading(false);
                }
            });

            $.ajax({
                type: "POST",
                url: $("#txtTrackingURLBase").val() + "/Security/DoLoginWindows",
                async: false,
                crossDomain: true,
                data: obj,
                contentType: 'application/json',
                success: function (response) {
                    loading(false);
                    if (self.rememberme() == true) {
                        $.cookie("userNamecookie", self.userName(), { expires: 10000 });
                        $.cookie("passwordcookie", self.password(), { expires: 10000 });
                    }
                    if (response != null) {
                        //var ResponseDTO = new ResponseModel();
                        //ResponseDTO.MapEntity(response, ResponseDTO);

                        var ResponseDTO = JSON.parse(response);
                    }


                    if (ResponseDTO.Status === "-1") {
                        self.errorMessage(ResponseDTO.Message);
                    }


                    if (ResponseDTO.Status === "0") {
                        //self.errorMessage(ResponseDTO.Message);
                        //go to main page........
                        if (ResponseDTO.ReturnUrl !== null && ResponseDTO.ReturnUrl !== undefined && ResponseDTO.ReturnUrl !== '') {
                            //window.location = ResponseDTO.ReturnUrl;
                            //return;
                        }

                        window.location = $("#txtTrackingURLBase").val() + "/Dashboard/index";

                    }

                    if (ResponseDTO.Status === "1") {
                        self.errorMessage(ResponseDTO.Message);
                        //go to change password........
                        self.loginPage(false);
                        self.ChangeloginPage(true);
                    }



                    self.isLogin(false);
                },
                error: function (errorResponse) {
                    loading(false);
                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                    self.errorMessage(errorMsg);
                    self.isLogin(false);
                }
            });
        

        loading(false);
    }


    self.ChangePassword = function () {


        self.errorMessage("");

        if (self.password() === "" || self.password() === null || self.password() === undefined) {
            self.isPasswordMissing(true);
            return;
        }
        else {
            self.isPasswordMissing(false);
        }

        if (self.NewPassword() === "" || self.NewPassword() === null || self.NewPassword() === undefined) {
            self.isNewPasswordMissing(true);
            return;
        }
        else {
            self.isNewPasswordMissing(false);
        }

        if (self.ConfirmPassword() === "" || self.ConfirmPassword() === null || self.ConfirmPassword() === undefined) {
            self.isConfirmPasswordMissing(true);
            return;
        }
        else {
            self.isConfirmPasswordMissing(false);
        }


        if (self.ConfirmPassword() !==  self.NewPassword() ) {
            self.errorMessage("Your new and confirmation passwords do not match, please check");
            return;
        }


        loading(true, 'Please wait ...');


        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Security/ChangePassword?userName=" + self.userName() + "&Password=" + self.password() + '&NewPassword=' + self.NewPassword(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {

                loading(false);
 
                if (response != null) {
        
                    var ResponseDTO = JSON.parse(response);
                }


                if (ResponseDTO.Status === "-1") {
                    self.errorMessage(ResponseDTO.Message);
                }


                if (ResponseDTO.Status === "0")
                {
                    self.errorMessage(ResponseDTO.Message);
                    window.location = $("#txtTrackingURLBase").val() + "/Dashboard/index";
                }

    
      
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.errorMessage(errorMsg);
            }
        });

    loading(false);

    }

    self.Rememaberme = function () {
        if (self.userName() != null & self.password() != null) {
            self.rememberme(true);
        }
    }


    //LOGIN vm constructor.
    self.LoginVmConstructor = function () {

        self.Rememaberme();
        self.ChangeloginPage(false);
        self.loginPage(true);

        // checkAutoLogin 
        var hideUser = $('#HideUserName').val();
        var hidePassword = $('#HidePassword').val();
        if (hidePassword == null && hideUser == null || hidePassword == "" && hideUser == "") {
           // self.loginPage(true);
        }
        else {
            self.userName(hideUser);
            self.password(hidePassword);
            self.Login();
            self.LoginWindows();
        }
        // 


    }


}

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {


               try {
                   loading(true, 'Please wait ...');

                   LoginObj = new loginvm();

                   ko.applyBindings(LoginObj);

                   LoginObj.LoginVmConstructor();

                   loading(false);
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}





