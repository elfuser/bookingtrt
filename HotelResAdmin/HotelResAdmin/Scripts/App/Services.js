﻿function ServicesVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;

    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isShowErrorEditRate = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.ErrorRateList = ko.observableArray();
    self.ServiceList = ko.observableArray();
    self.RateServiceList = ko.observableArray();

    self.CategoryList = ko.observableArray();

    self.SelectedService = ko.observable(new ServicesModel());
    self.selectedCategory = ko.observable(new CategoryModel());
    self.selectedRate = ko.observable(new RatesServiceModel());



    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();

    self.isRefresh = ko.observable(false);

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable(" s.ServiceID ");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.CultureList = ko.observableArray();
    self.selectedCulture = ko.observable(new CultureModel());
    self.ServiceConditions = ko.observable("");
    self.ShowConditions = ko.observable(false);

    self.selectedBringInfoCulture = ko.observable(new CultureModel());
    self.ServicesBringInfo = ko.observable("");

    self.Picture1 = ko.observable("");
    self.Picture2 = ko.observable("");
    self.Picture3 = ko.observable("");

    self.selectedPromoCodeType = ko.observable(new PromoCodeModel());
    self.selectedPromoCode = ko.observable(new PromoCodeModel());
    self.selectedPromoId = ko.observable(0);

    self.RootlVm(mainVm);


    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }

    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.ShowEditRateErrorList = function (data) {
        self.ErrorRateList.push({ ErrorText: data });
        self.isShowErrorEditRate(true);
    }

    self.GetServices = function () {

        loading(true, 'Please wait ...');

        self.ServiceList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetServicesPaging?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var ServiceDTO = new ServicesModel();
                            ServiceDTO.MapEntity(value, ServiceDTO);
                            self.ServiceList.push(ServiceDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetServicesTotalPageCount = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetServicesPagingCount?PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetServices();
        self.GetServicesTotalPageCount();
    }

    self.GetSelectedPage = function () {



        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetServices();

    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetServices();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetServices();

        }
    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetServices();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetServices();

        }
    }

    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable(" s.ServiceID ");
        self.isAccending = ko.observable(true);
        self.GetServices();
        self.GetServicesTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorPanel(false);
        self.ShowConditions(false);
    }


    self.EditService = function (data) {


        try {

            self.Picture1("");
            self.Picture2("");
            self.Picture3("");
            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectedService(data);
            self.ShowConditions(true);

            self.ServiceConditions("");
            self.ServicesBringInfo ("");


            var filteredCategory;
            var CategoryID;

            CategoryID = $.trim(self.SelectedService().ppCategoryID());
            if (CategoryID != null && CategoryID != undefined && CategoryID != '') {
                filteredCategory = Enumerable.From(self.CategoryList()).Where(function (x) { return x.ppCategoryID().toString().toUpperCase().match("^" + CategoryID.toUpperCase()) }).FirstOrDefault()
                if (filteredCategory != null && filteredCategory != undefined) { self.selectedCategory(filteredCategory); }
                else { self.selectedCategory(new CategoryModel()); }

            } else { self.selectedCategory(new CategoryModel()); }


            self.GetServicesConditions(self.SelectedService().ppServiceID(), 1);
            self.GetServicesBringInfo(self.SelectedService().ppServiceID(), 1);

            self.GetServiceRates(self.SelectedService().ppServiceID());

            $("#HdfServiceId").val(self.SelectedService().ppServiceID());

            
            if ((data.ppServiceID() !== "") && (data.ppServiceID() !== "0") && (data.ppServiceID() !== undefined)) {
                self.Picture1($("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=1&ServiceId=" + data.ppServiceID());
                self.Picture2($("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=2&ServiceId=" + data.ppServiceID());
                self.Picture3($("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=3&ServiceId=" + data.ppServiceID());
            }

            loading(false);
        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }


    self.DeleteClick = function (data) {


        try {

            if (self.SelectedService() != null && self.SelectedService() != undefined && self.SelectedService().ppServiceID() != undefined && self.SelectedService().ppServiceID() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteServices").html($("<h5>" + "Are you sure you want to delete the service?" + "</h5>"));



                $("#DeleteServices").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Services/DeleteService?ServiceId=" + self.SelectedService().ppServiceID(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteServices").dialog("close");

                                     self.GetServices();
                                     self.isSucess(true);

                                    $("#ServiceMsg").show();
                                    $("#ServiceMsg").html("<h5>" + "service deleted successfully" + "</h5>").fadeIn(0);
                                    $("#ServiceMsg").html("<h5>" + "service deleted successfully" + "</h5>").fadeOut(3000);

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#ServiceMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteServices").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorList(err);
        }

    }

    self.EditRate= function (data) {
        try
        {
            self.selectedRate(data);



            self.selectedFromDate(formatDate(self.selectedRate().ppStartDate(), '/'));
            self.selectedToDate(formatDate(self.selectedRate().ppEndDate(), '/'));

        }
        catch (err) {
                loading(false);
                self.ShowErrorList(err);
        }


    }

    self.DeleteRateClick = function (data) {
        try {

                self.selectedRate(data);

                if (self.selectedRate() != null && self.selectedRate() != undefined && self.selectedRate().ppRateID() != undefined && self.selectedRate().ppRateID() != null) {

                    var newService = JSON.stringify(ko.toJS(self.selectedRate()));

                    self.isShowErrorPanel(false);

                    loading(true, 'Please wait ...');
                    $.ajax({
                        type: "POST",
                        url: $("#txtTrackingURLBase").val() + "/Services/DeleteServiceRate",
                        async: false,
                        crossDomain: true,
                        data: newService,
                        contentType: 'application/json',
                        success: function (response) {

                            loading(false);

                            self.GetServiceRates(self.SelectedService().ppServiceID());
                            self.isSucess(true);
                            self.isShowErrorEditRate(false);
                            self.selectedRate(new RatesServiceModel());
               

                            $("#ServiceMsg").show();
                            $("#ServiceMsg").html("<h5>" + "rate deleted successfully" + "</h5>").fadeIn(0);
                            $("#ServiceMsg").html("<h5>" + "rate deleted successfully" + "</h5>").fadeOut(3000);

                        },
                        error: function (errorResponse) {
                            loading(false);
                            $("#ServiceMsg").dialog("close");
                            //$("#LocationMsg").hidden();
                            var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                            self.ShowErrorList(errorMsg);
                        }
                    });



                }



        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }


    }

    

    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);

        self.SelectedService(new ServicesModel());
        self.ShowConditions(false);
        self.ServiceConditions("");
        self.ServicesBringInfo("");

        self.Picture1("");
        self.Picture2("");
        self.Picture3("");
        $("#HdfServiceId").val("");
    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);

        self.Picture1("");
        self.Picture2("");
        self.Picture3("");
        $("#HdfServiceId").val("");

    }

    self.SaveClick = function () {
        var valid = true;

        try {


            if (self.selectedCategory() != null && self.selectedCategory().ppCategoryID() != null && self.selectedCategory().ppCategoryID() != undefined) {
                self.SelectedService().ppCategoryID(self.selectedCategory().ppCategoryID());
            }

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newService = JSON.stringify(ko.toJS(self.SelectedService()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Services/SaveService",
                    async: false,
                    crossDomain: true,
                    data: newService,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.CloseClick();
                            self.ShowConditions(true);
                            self.GetServices();
                            self.isSucess(true);
                            $("#ServiceMsg").show();
                            $("#ServiceMsg").html("<h5>" + "Service saved successfully" + "</h5>").fadeIn(0);
                            $("#ServiceMsg").html("<h5>" + "Service saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Service";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ServiceMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            alert(err);
            loading(false);
        }

    }


    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectedService().ppCode() == null || self.SelectedService().ppCode() == undefined || self.SelectedService().ppCode() == "") {
            ValidateMsgText = "Code field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectedService().ppDescription() == null || self.SelectedService().ppDescription() == undefined || self.SelectedService().ppDescription() == "") {
            ValidateMsgText = "Please enter the description\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        return valid;
    }



    //categories/////////////////////////////////////


    self.GetCategories = function () {

        loading(true, 'Please wait ...');

        self.CategoryList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetCategories",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var categoryDTO = new CategoryModel();
                            categoryDTO.MapEntity(value, categoryDTO);
                            self.CategoryList.push(categoryDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    ///////////////////////////////////////////////////


    self.GetCultureTypes = function () {

        self.CultureList.removeAll();
        var count = 0;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Addons/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.CultureConditionsChanged = function () {

        var ServiceId;

        ServiceId = self.SelectedService().ppServiceID();

        if (ServiceId !== undefined)
        { self.GetServicesConditions(ServiceId, self.selectedCulture().ppCultureID()); }
    }

    self.CultureBringChanged = function () {

        var ServiceId;

        ServiceId = self.SelectedService().ppServiceID();

        if (ServiceId !== undefined)
        { self.GetServicesBringInfo(ServiceId, self.selectedBringInfoCulture().ppCultureID()); }
    }

    
    self.SaveConditions = function () {

        var valid = true;

        try {

            if (self.SelectedService().ppServiceID() == null || self.SelectedService().ppServiceID() == undefined || self.SelectedService().ppServiceID() == "") {
                ValidateMsgText = " Please select the service\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.ServiceConditions() == null || self.ServiceConditions() == undefined || self.ServiceConditions() == "") {
                ValidateMsgText = "Please enter the service conditions\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.selectedCulture().ppCultureID() == null || self.selectedCulture().ppCultureID() == undefined || self.selectedCulture().ppCultureID() == "") {
                ValidateMsgText = " Please select the languaje\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }


            if (valid === true) {

                loading(true, 'Please wait ...');


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Services/SaveServiceConditions?ServiceId=" + self.SelectedService().ppServiceID() +
                        "&CultureId=" + self.selectedCulture().ppCultureID() + "&Conditions=" + self.ServiceConditions(),
                    async: false,
                    crossDomain: true,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.isEditing(true);
                            self.isSucess(true);
                            $("#ServiceMsg").show();
                            $("#ServiceMsg").html("<h5>" + "Conditions saved successfully" + "</h5>").fadeIn(0);
                            $("#ServiceMsg").html("<h5>" + "Conditions saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Conditions";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ServiceMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }
    }


    self.GetServicesConditions = function (ServiceId,CultureID) {

        self.ServiceConditions("");
        var count = 0;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetServiceConditions?ServiceId=" + ServiceId + "&Culture=" + CultureID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    self.ServiceConditions(response);
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetServicesBringInfo = function (ServiceId, CultureID) {

        self.ServicesBringInfo("");
        var count = 0;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Services/GetServiceBringInfo?ServiceId=" + ServiceId + "&CultureId=" + CultureID,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    self.ServicesBringInfo(response);
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.SaveBringInfo = function () {

        var valid = true;

        try {

            if (self.SelectedService().ppServiceID() == null || self.SelectedService().ppServiceID() == undefined || self.SelectedService().ppServiceID() == "") {
                ValidateMsgText = " Please select the service\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.ServicesBringInfo() == null || self.ServicesBringInfo() == undefined || self.ServicesBringInfo() == "") {
                ValidateMsgText = "Please enter the service bring information\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.selectedBringInfoCulture().ppCultureID() == null || self.selectedBringInfoCulture().ppCultureID() == undefined || self.selectedBringInfoCulture().ppCultureID() == "") {
                ValidateMsgText = " Please select the languaje\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }


            if (valid === true) {

                loading(true, 'Please wait ...');


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Services/SaveServiceBringInfo?ServiceId=" + self.SelectedService().ppServiceID() +
                        "&CultureId=" + self.selectedBringInfoCulture().ppCultureID() + "&BingInfo=" + self.ServicesBringInfo(),
                    async: false,
                    crossDomain: true,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.isEditing(true);
                            self.isSucess(true);
                            $("#ServiceMsg").show();
                            $("#ServiceMsg").html("<h5>" + "Bing Info. saved successfully" + "</h5>").fadeIn(0);
                            $("#ServiceMsg").html("<h5>" + "Bing Info. saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Bing Info.";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ServiceMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }
    }

    self.GetServiceRates = function (ServiceId) {

        self.RateServiceList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() +"/Services/GetServiceRates?ServiceId=" + ServiceId,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {

                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var rate = new RatesServiceModel();
                            rate.MapEntity(value, rate);
                            self.RateServiceList.push(rate);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }



    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "FromDate") {
                        self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    }

                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    if (inst.id == "ToDate") {
                        self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    }
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };


    self.CancelRateClick = function () {
        self.selectedRate(new RatesServiceModel());
        self.isShowErrorEditRate(false);        
    }


    self.SaveRateClick = function () {


        var valid = true;
        var validfromdate = false;
        var validtodate = false;

        try {

            self.ErrorRateList.removeAll();
            self.isShowErrorEditRate(false);

            var DateFrom = GetUniversalDate(self.selectedFromDate());
            var DateTo = GetUniversalDate(self.selectedToDate());

            self.selectedRate().UnFormatFrom(DateFrom);
            self.selectedRate().UnFormatTo(DateTo);

            if (self.selectedRate().ppDescription() == null || self.selectedRate().ppDescription() == undefined || self.selectedRate().ppDescription() == "") {
                ValidateMsgText = " The Rate Description cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.selectedRate().ppAdults() == null || self.selectedRate().ppAdults() == undefined || self.selectedRate().ppAdults() == "") {
                ValidateMsgText = "The Adult's Price cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }


            if (self.selectedRate().ppChildren() == null || self.selectedRate().ppChildren() == undefined || self.selectedRate().ppChildren() == "") {
                ValidateMsgText = "The Children' Price cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }

            
            if (self.selectedRate().ppInfants() == null || self.selectedRate().ppInfants() == undefined || self.selectedRate().ppInfants() == "") {
                ValidateMsgText = "The Infants's Price cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }

                        
            if (self.selectedRate().ppMaxAdults() == null || self.selectedRate().ppMaxAdults() == undefined || self.selectedRate().ppMaxAdults() == "") {
                ValidateMsgText = "The Max Adults cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.selectedRate().ppMaxChildren() == null || self.selectedRate().ppMaxChildren() == undefined || self.selectedRate().ppMaxChildren() == "") {
                ValidateMsgText = "The Max Children cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }


            
            if (self.selectedRate().ppMaxInfants() == null || self.selectedRate().ppMaxInfants() == undefined || self.selectedRate().ppMaxInfants() == "") {
                ValidateMsgText = "The Max Infants cannot be empty, please correct\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            }

            if (self.selectedRate().UnFormatFrom() == null || self.selectedRate().UnFormatFrom() == undefined
                || self.selectedRate().UnFormatFrom() == "" || self.selectedRate().UnFormatFrom() == "NaN-NaN-NaN") {
                ValidateMsgText = "Start Date field is required\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            } else {
                validfromdate = true;
            }


            if (self.selectedRate().UnFormatTo() == null || self.selectedRate().UnFormatTo() == undefined
                || self.selectedRate().UnFormatTo() == "" || self.selectedRate().UnFormatTo() == "NaN-NaN-NaN") {
                ValidateMsgText = "End Date field is required\n";
                self.ShowEditRateErrorList(ValidateMsgText);
                valid = false;
            } else {
                validtodate = true;
            }

            if (validfromdate && validtodate) {
                dfrom = new Date(self.selectedRate().UnFormatFrom().replace('-', '/'));
                dto = new Date(self.selectedRate().UnFormatTo().replace('-', '/'));
                if (dfrom > dto) {
                    ValidateMsgText = "Start Date is greater than End Date\n";
                    self.ShowEditRateErrorList(ValidateMsgText);
                    valid = false;
                }
            }

            self.selectedRate().ppServiceID(self.SelectedService().ppServiceID());


            if (valid === true) {

                loading(true, 'Please wait ...');

                var newRate = JSON.stringify(ko.toJS(self.selectedRate()));


                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Services/SaveServiceRate",
                    async: false,
                    crossDomain: true,
                    data: newRate,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response === "0") {
                            self.GetServiceRates(self.SelectedService().ppServiceID());
                            self.isSucess(true);
                            self.isShowErrorEditRate(false);
                            self.selectedRate(new RatesServiceModel());
                            $("#ServiceMsg").show();
                            $("#ServiceMsg").html("<h5>" + "rate saved successfully" + "</h5>").fadeIn(0);
                            $("#ServiceMsg").html("<h5>" + "rate saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Service";
                            self.ShowEditRateErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ServiceMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditRateErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            alert(err);
            loading(false);
        }
  
    }


    //-- Promo Codes
    

    //

    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetServices();
        self.GetServicesTotalPageCount();
        self.GetCategories();
        self.GetCultureTypes();

    }

}

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {



               try {
                   loading(true, 'Please wait ...');

                   ServicesObj = new ServicesVm();

                   ko.applyBindings(ServicesObj);

                   ServicesObj.InitModel();

                   loading(false);

                   //--Upalod files

                   $('#txtUploadFile1').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile1").get(0).files;

                           var ServiceId = $("#HdfServiceId").val();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=1&ServiceId=" + ServiceId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=1&ServiceId=" + ServiceId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();

                               $("#Picture1").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?" + d.getTime() + "&PictureNum=1&ServiceId=" + ServiceId);
                               $("#txtUploadFile1").val('');
                               loading(false);
                           });


                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }

                       loading(false);
                   });

                   $('#txtUploadFile2').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile2").get(0).files;

                           var ServiceId = $("#HdfServiceId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=2&ServiceId=" + ServiceId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=2&ServiceId=" + ServiceId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile2").val('');
                               $("#Picture2").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?" + d.getTime() + "&PictureNum=2&ServiceId=" + ServiceId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });

                   $('#txtUploadFile3').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile3").get(0).files;

                           var ServiceId = $("#HdfServiceId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=3&ServiceId=" + ServiceId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?PictureNum=3&ServiceId=" + ServiceId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile3").val('');
                               $("#Picture3").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/ServiceHandler.ashx?" + d.getTime() + "&PictureNum=3&ServiceId=" + ServiceId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });

                   //--
                   $("input[name='file[]']").removeAttr("multiple");

               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//


//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}
