﻿function CommisionsQuery(mainVm) {

    var self = this;

    self.ErrorList = ko.observableArray();
    self.isShowErrorPanel = ko.observable(false);
    self.isDetail = ko.observable(false);
    self.ReservationID = ko.observable();
    self.Reservations = ko.observableArray();

    self.ReservationRooms = ko.observableArray();
    self.ReservationAddons = ko.observableArray();
    self.ReservationLog = ko.observableArray();

    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();
    self.rdDateType = ko.observable("1");

    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);

                    self.selectedFromDate(date.ConvertDateToMMDDYYFormat());

                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };

    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    self.selectedToDate(date.ConvertDateToMMDDYYFormat());

                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };

    self.GetDefaultDate = function () {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/GetDefaultDate",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    self.selectedFromDate(response);
                    self.selectedToDate(response);
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }




    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }

    self.ViewDetail = function (data) {

        try {

            self.ReservationID(data.reservation());

            self.ReservationRoomConfirmation();
            self.ReservationAddonConfirmations();
            self.ReservationBankLog();

            self.isDetail(true);

            $("#DetailDiv").dialog({
                resizable: true,
                width: 600,
                heigh: 400,
                modal: false,
                closeOnEscape: false,
                position: { at: "center" },
                buttons: {
                    OK: function () {
                        $("#DetailDiv").dialog("close");
                        self.isDetail(false);
                    }
                }
            });
        }
        catch (ex) {
            self.ShowErrorList(ex);
            loading(false);
        }
    }


    self.Search = function () {
       
        self.Reservations.removeAll();
        self.ErrorList.removeAll();
        self.isShowErrorPanel(false);

        if (self.selectedFromDate() == null || self.selectedFromDate() == undefined
             || self.selectedFromDate() == "" || self.selectedFromDate() == "NaN-NaN-NaN") {
            ValidateMsgText = "Start date field is required\n";
            self.ShowErrorList(ValidateMsgText);
            return;
        }

        if (self.selectedToDate() == null || self.selectedToDate() == undefined
            || self.selectedToDate() == "" || self.selectedToDate() == "NaN-NaN-NaN") {
            ValidateMsgText = "End date field is required\n";
            self.ShowErrorList(ValidateMsgText);
            return;
        }

        if (self.selectedFromDate() > self.selectedToDate()) {
            ValidateMsgText = "Start Date is greater than End Date\n";
            self.ShowErrorList(ValidateMsgText);
            return;
        }

        loading(true, 'Please wait ...');

        var DateFrom = GetUniversalDate(self.selectedFromDate());
        var DateTo = GetUniversalDate(self.selectedToDate());

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/GetCommissions?DateFrom=" + DateFrom + "&Dateto=" + DateTo + '&Range=' + self.rdDateType(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var reserv = new ReservationsQryModel();
                            reserv.MapEntity(value, reserv);
                            self.Reservations.push(reserv);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }


    self.ReservationRoomConfirmation = function () {

        loading(true, 'Please wait ...');
        self.ReservationRooms.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/ReservationRoomConfirmation?ReservationID=" + self.ReservationID(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var room = new QueryRoomModel();
                            room.MapEntity(value, room);
                            self.ReservationRooms.push(room);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.ReservationAddonConfirmations = function () {

        loading(true, 'Please wait ...');
        self.ReservationAddons.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/ReservationAddonConfirmations?ReservationID=" + self.ReservationID(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var addon = new QryAddonModel();
                            addon.MapEntity(value, addon);
                            self.ReservationAddons.push(addon);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.ReservationBankLog = function () {

        loading(true, 'Please wait ...');
        self.ReservationLog.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/ReservationBankLog?ReservationID=" + self.ReservationID(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var log = new BankLogInfoModel();
                            log.MapEntity(value, log);
                            self.ReservationLog.push(log);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }


    self.InitModel = function () {

        self.GetDefaultDate();
    }
}


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               try {
                   loading(true, 'Please wait ...');


                   CommisionsObj = new CommisionsQuery();

                   ko.applyBindings(CommisionsObj);

                   CommisionsObj.InitModel();

                   loading(false);

                   $("#CommissionsQueryDiv").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);



$(document).ready(function () {
    $("#btnExport").click(function (e) {
        e.preventDefault();

        //getting data from our table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('CommissionTable');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
        a.click();
    });
});
//*************************************//

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}
