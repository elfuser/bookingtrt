﻿function RoomVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.RoomList = ko.observableArray();
    self.CultureList = ko.observableArray();

    self.FeatureList = ko.observableArray();
    self.SelectedFeatureList = ko.observableArray();

    self.SelectRoom = ko.observable(new RoomsModel());
    self.isRefresh = ko.observable(false);

    self.selectedCulture = ko.observable(new CultureModel());

    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1);
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);

    self.orderBy = ko.observable("R.roomtype_id");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.Picture1 = ko.observable("");
    self.Picture2 = ko.observable("");
    self.Picture3 = ko.observable("");
    self.Picture4 = ko.observable("");

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }



    self.GetCultureTypes = function () {

        self.CultureList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Addons/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetFeatureList = function (RoomTypeId) {

        self.FeatureList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rooms/GetFeatures?RoomTypeId=" + RoomTypeId,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var FeatureDTO = new FeatureModel();
                            FeatureDTO.MapEntity(value, FeatureDTO);
                            self.FeatureList.push(FeatureDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }



    self.GetRooms = function (Category) {

        loading(true, 'Please wait ...');

        self.RoomList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rooms/GetRooms?OrderBy=" + self.orderBy() + '&IsAccending='
                + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var RoomDTO = new RoomsModel();
                            RoomDTO.MapEntity(value, RoomDTO);
                            self.RoomList.push(RoomDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTotalPageCount = function (data) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Rooms/GetRoomsPagingCount?PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {

                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.SaveFeatures = function (RoomId) {



        self.SelectedFeatureList.removeAll();

        for (var i = 0; i < self.FeatureList().length; i++)
        {

            if (self.FeatureList()[i].pChecked() === true)
            {
                var FeatureRoomDTO = new FeatureRoomModel();
                FeatureRoomDTO.FeatureID(self.FeatureList()[i].ppFeatureID());
                FeatureRoomDTO.RoomTypeID(RoomId);
                self.SelectedFeatureList.push(FeatureRoomDTO);
            }
           

        }

        if (self.SelectedFeatureList().length === 0)
        {
            var FeatureRoomDTO = new FeatureRoomModel();
            FeatureRoomDTO.FeatureID(0);
            FeatureRoomDTO.RoomTypeID(RoomId);
            self.SelectedFeatureList.push(FeatureRoomDTO);
        }


        var Features = JSON.stringify(ko.toJS(self.SelectedFeatureList()));

        $.ajax({
            type: "POST",
            url: $("#txtTrackingURLBase").val() + "/Rooms/SaveFeatures",
            async: false,
            crossDomain: true,
            data: Features,
            contentType: 'application/json',
            success: function (response) {
                return true;
            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
                return true;
            }
        });
    }

    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetRooms();
        self.GetTotalPageCount();
    }

    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetRooms();

    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetRooms();
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetRooms();

        }
    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetRooms();

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetRooms();

        }
    }

    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.pageNumber(1);
        self.orderBy = ko.observable("R.roomtype_id");
        self.isAccending = ko.observable(true);
        self.GetRooms();
        self.GetTotalPageCount();
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }

    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);

        self.SelectRoom(new RoomsModel());
        self.GetFeatureList(0);


        $("#PhotoDiv").hide();
        $("#PhotoTab").hide();

        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#PhotoTab").removeClass("activePageMenu");

    }

    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        if (self.SelectRoom().ppDescription() == null || self.SelectRoom().ppDescription() == undefined || self.SelectRoom().ppDescription() == "") {
            ValidateMsgText = "Room description field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectRoom().ppRoomTypeCode() == null || self.SelectRoom().ppRoomTypeCode() == undefined || self.SelectRoom().ppRoomTypeCode() == "") {
            ValidateMsgText = "Room type  is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectRoom().ppMax_Adults() == null || self.SelectRoom().ppMax_Adults() == undefined || self.SelectRoom().ppMax_Adults() == "") {
            ValidateMsgText = "Max Adults field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectRoom().ppMax_Children() == null || self.SelectRoom().ppMax_Children() == undefined || self.SelectRoom().ppMax_Children() == "") {
            ValidateMsgText = "Max Children field is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        return valid;
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);


    }

    self.SaveClick = function () {

        var valid = true;

        try {


            if (self.selectedCulture() != null && self.selectedCulture().ppCultureID() != null && self.selectedCulture().ppCultureID() != undefined) {
                self.SelectRoom().CultureId(self.selectedCulture().ppCultureID());
            }

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newRoom = JSON.stringify(ko.toJS(self.SelectRoom()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Rooms/Save",
                    async: false,
                    crossDomain: true,
                    data: newRoom,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {

                            self.SaveFeatures(response);
                            self.CloseClick();
                            self.GetRooms();
                            self.isSucess(true);
                            $("#RoomMsg").show();
                            $("#RoomMsg").html("<h5>" + "Room saved successfully" + "</h5>").fadeIn(0);
                            $("#RoomMsg").html("<h5>" + "Room saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Room";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#RoomMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
        }

    }


    self.EditRoom = function (data) {


        try {




            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectRoom(data);

            self.GetFeatureList(data.ppRoomTypeID());

            var filteredCulture;
            var CultureID;

            CultureID = $.trim(self.SelectRoom().CultureId());
            if (CultureID != null && CultureID != undefined && CultureID != '') {
                filteredCulture = Enumerable.From(self.CultureList()).Where(function (x) { return x.ppCultureID().toString().toUpperCase().match("^" + CultureID.toUpperCase()) }).FirstOrDefault()
                if (filteredCulture != null && filteredCulture != undefined) { self.selectedCulture(filteredCulture); }
                else { self.selectedCulture(new CultureModel()); }

            } else { self.selectedCulture(new CultureModel()); }


            self.GeneralInfoClick();

            if ((data.ppRoomTypeID() !== "") && (data.ppRoomTypeID() !== "0") && (data.ppRoomTypeID() !== undefined)) {
                self.Picture1($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=1&RoomID=" + data.ppRoomTypeID());
                self.Picture2($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=2&RoomID=" + data.ppRoomTypeID());
                self.Picture3($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=3&RoomID=" + data.ppRoomTypeID());
                self.Picture4($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=4&RoomID=" + data.ppRoomTypeID());
                $("#PhotoTab").show();
                $("#PhotoDiv").hide();
            }
            else {
                $("#PhotoDiv").hide();
                $("#PhotoTab").hide();
            }



            $("#RoomId").val(data.ppRoomTypeID());

            


            loading(false);
        }
        catch (err) {
            loading(false);
            alert(err);
        }


    }


    self.DeleteClick = function (data) {


        try {

            if (self.SelectRoom() != null && self.SelectRoom() != undefined && self.SelectRoom().ppRoomTypeID() != undefined && self.SelectRoom().ppRoomTypeID() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteRoom").html($("<h5>" + "Are you sure you want to delete the Room?" + "</h5>"));



                $("#DeleteRoom").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Rooms/Delete?RoomTypeId=" + self.SelectRoom().ppRoomTypeID(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteRoom").dialog("close");


                                    if (response === "0") {
                                        self.GetRooms();
                                        self.isSucess(true);

                                        $("#RoomMsg").show();
                                        $("#RoomMsg").html("<h5>" + "Room deleted successfully" + "</h5>").fadeIn(0);
                                        $("#RoomMsg").html("<h5>" + "Room deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#RoomMsg").show();
                                        $("#RoomMsg").html("<h5>" + "An error has ocurred deleting the Room" + "</h5>").fadeIn(0);
                                        $("#RoomMsg").html("<h5>" + "An error has ocurred deleting the Room" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#RoomMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteRoom").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);
        }

    }



    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }



    self.GeneralInfoClick = function () {

        $("#GeneralInfoDiv").show();
        $("#PhotoDiv").hide();
        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#PhotoTab").removeClass("activePageMenu");

    }


    self.PhotosClick = function () {

        $("#GeneralInfoDiv").hide();
        $("#PhotoDiv").show();
        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#PhotoTab").addClass("activePageMenu");

    }



    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetRooms();
        self.GetCultureTypes();
        self.GetFeatureList(0);
    }



    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {              

               try {
                   loading(true, 'Please wait ...');


                   //var element = document.getElementById("Roomection");
                   RoomObj = new RoomVm();

                   ko.applyBindings(RoomObj);

                   RoomObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   $('#txtUploadFile1').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile1").get(0).files;

                           var roomId = $("#RoomId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=1&RoomID=" + roomId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=1&RoomID=" + roomId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile1").val('');
                               $("#Picture1").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?" + d.getTime() + "&PictureNum=1&RoomID=" + roomId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile2').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile2").get(0).files;

                           var roomId = $("#RoomId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=2&RoomID=" + roomId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=2&RoomID=" + roomId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile2").val('');
                               $("#Picture2").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?" + d.getTime() + "&PictureNum=2&RoomID=" + roomId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile3').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile3").get(0).files;

                           var roomId = $("#RoomId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=3&RoomID=" + roomId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=3&RoomID=" + roomId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile3").val('');
                               $("#Picture3").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?" + d.getTime() + "&PictureNum=3&RoomID=" + roomId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });


                   $('#txtUploadFile4').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadFile4").get(0).files;

                           var roomId = $("#RoomId").val();


                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=4&RoomID=" + roomId, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?PictureNum=4&RoomID=" + roomId,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();
                               $("#txtUploadFile4").val('');
                               $("#Picture4").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/RoomPhotoHandler.ashx?" + d.getTime() + "&PictureNum=4&RoomID=" + roomId);

                               loading(false);
                           });

                           loading(false);
                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }
                   });

                   loading(false);

                   $("#RoomSection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


