﻿function ParametersVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.isShowErrorPanel = ko.observable(false);
    self.isSucess = ko.observable(false);
    self.isEditing = ko.observable(false);

    self.ErrorList = ko.observableArray();
    self.CultureList = ko.observableArray();
    self.selectedCulture = ko.observable(new CultureModel());

    self.SelectedHotel = ko.observable(new CompanyModel());

    self.terms = ko.observable("");
 
    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
    
    }


    self.GetParameters = function () {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Parameters/GetCompany",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
             
                        var CompanyDTO = new CompanyModel();
                        CompanyDTO.MapEntity(ds, CompanyDTO);
                        self.SelectedHotel(CompanyDTO);
                  
                    }
                }

                loading(false);


            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetCultureTypes = function () {

        self.CultureList.removeAll();
        var count = 0;

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Addons/GetCultures",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var culture = new CultureModel();
                            culture.MapEntity(value, culture);
                            self.CultureList.push(culture);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetTermsAndConditions = function (culture) {
        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Parameters/GetTermsAndConditions?CultureId=" + culture,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {



                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {

                        var CompanyDTO = new CompanyModel();
                        CompanyDTO.MapEntity(ds, CompanyDTO);
                        self.SelectedHotel().TermsId(CompanyDTO.TermsId());
                        self.terms(CompanyDTO.ppTerms());
                        tinymce.execCommand('mceSetContent', false, CompanyDTO.ppTerms());
                    }

                }

            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.CultureTypeChanged = function () {

        self.GetTermsAndConditions(self.selectedCulture().ppCultureID());
    }

    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorPanel(false);

        if (self.SelectedHotel().ppMaxAdults() == null || self.SelectedHotel().ppMaxAdults() == undefined || self.SelectedHotel().ppMaxAdults() == "") {
            ValidateMsgText = " Max. Adults field is required\n";
            self.ShowErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectedHotel().ppMaxChildren() == null || self.SelectedHotel().ppMaxChildren() == undefined || self.SelectedHotel().ppMaxChildren() == "") {
            ValidateMsgText = "Max. Children field is required\n";
            self.ShowErrorList(ValidateMsgText);
            valid = false;
        }


        return valid;
    }


    self.SaveClick = function () {

        var valid = true;

        try {



            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var Hotel = JSON.stringify(ko.toJS(self.SelectedHotel()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Parameters/Save",
                    async: false,
                    crossDomain: true,
                    data: Hotel,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.isEditing(true);
                            self.isSucess(true);
                            $("#ParamMsg").show();
                            $("#ParamMsg").html("<h5>" + "Parameters saved successfully" + "</h5>").fadeIn(0);
                            $("#ParamMsg").html("<h5>" + "Parameters saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the parameters";
                            self.ShowErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#ParamMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }

    }


    self.SaveTermsClick = function () {

        var valid = true;

        try {


            loading(true, 'Please wait ...');

            var CultureId = "";
            var terms = tinymce.activeEditor.getContent();



            if (self.selectedCulture() != null && self.selectedCulture().ppCultureID() != null && self.selectedCulture().ppCultureID() != undefined) {
                CultureId = self.selectedCulture().ppCultureID();
            }


            self.SelectedHotel().CultureId(CultureId);
            self.SelectedHotel().ppTerms(terms);

            var Hotel = JSON.stringify(ko.toJS(self.SelectedHotel()));
      

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Parameters/SaveTerms",
                    async: false,
                    crossDomain: true,
                    data: Hotel,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            alertDialog("Terms and Conditions saved successfully");
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the terms and conditions";
                            self.ShowErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });

        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err);
        }

    }


    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }





    self.GeneralTabClick = function () {

        $("#ParamsDiv").show();
        $("#TermsDiv").hide();
        $("#GeneralTab").addClass("activePageMenu");
        $("#TermsTab").removeClass("activePageMenu");

    }


    self.TermsTabClick = function () {

        $("#ParamsDiv").hide();
        $("#TermsDiv").show();
        $("#GeneralTab").removeClass("activePageMenu");
        $("#TermsTab").addClass("activePageMenu");

    }



    self.InitModel = function () {

        self.isEditing(false);
        self.isShowErrorPanel(false);
        self.GetParameters();
        self.GetCultureTypes();


    }



    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTER END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

   

               try {
                   loading(true, 'Please wait ...');


                   //var element = document.getElementById("Roomection");
                   ParamObj = new ParametersVm();

                   ko.applyBindings(ParamObj);

                   ParamObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: false,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });


                   tinymce.init({
                       selector: '#txtterms',
                       height: 500,
                       plugins: [
                         'advlist autolink lists link image charmap print preview anchor',
                         'searchreplace visualblocks code fullscreen',
                         'insertdatetime media table contextmenu paste code'
                       ],
                       toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                       content_css: '//www.tinymce.com/css/codepen.min.css'
                   });

                   loading(false);

                   $(".ParameterTabs").show(); //Show parameter tabs
                   $("#ParamsDiv").show(); //Show main div
                   
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


