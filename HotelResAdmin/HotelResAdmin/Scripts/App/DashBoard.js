﻿function DashBoardVm(mainVm) {


    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;

    self.ErrorList = ko.observableArray();
    self.SalesByMonthList = ko.observableArray();
    self.SalesByRoomList = ko.observableArray();


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }

    self.GetSalesByRoom = function () {

        self.SalesByRoomList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Dashboard/DashSalesRooms",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var salesDTO = new SalesByRoomModel();
                            salesDTO.MapEntity(value, salesDTO);
                            self.SalesByRoomList.push(salesDTO);

                        })


                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }


    self.GetSalesByMonth = function () {

        self.SalesByMonthList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Dashboard/GetSalesByMonth",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var salesDTO = new SalesByMonthModel();
                            salesDTO.MapEntity(value, salesDTO);
                            self.SalesByMonthList.push(salesDTO);

                        })

             
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.BuildSalesGraph = function () {


        $.jqplot.config.enablePlugins = true;

        var s1 = [];
        var ticks = [];
        if (self.SalesByMonthList() != null && self.SalesByMonthList() != undefined) {
            $.each(self.SalesByMonthList(), function (key, value) {
                var total = value;

                ticks.push(value.dday());
                s1.push(value.amount());
            })


            if (s1.length == 0 || ticks.length == 0) { return; }

            plot1 = $.jqplot('DivTotalSales', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                //height: 250,
                //width: 350,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    pointLabels: { show: true },
                    rendererOptions: {
                        varyBarColor: true
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });

            plot1.replot({ resetAxes: true });
        }
        else {
            $('#DivTotalSales').html("");
        }


    }


    self.BuildRoomsGraph = function () {


        $.jqplot.config.enablePlugins = true;

        var s1 = [];
        var ticks = [];
        if (self.SalesByRoomList() != null && self.SalesByRoomList() != undefined) {
            $.each(self.SalesByRoomList(), function (key, value) {
                var total = value;

                ticks.push(value.room());
                s1.push(value.qty());
            })


            if (s1.length == 0 || ticks.length == 0) { return; }

            plot1 = $.jqplot('DivTotalRoomSales', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                //height: 250,
                //width: 350,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    pointLabels: { show: true },
                    rendererOptions: {
                        varyBarColor: true
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });

            plot1.replot({ resetAxes: true });
        }
        else {
            $('#DivTotalRoomSales').html("");
        }


    }

    self.InitModel = function () {
        self.GetSalesByMonth();
        self.BuildSalesGraph();
        self.GetSalesByRoom();
        self.BuildRoomsGraph();
    }
}




//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               try {
                   loading(true, 'Please wait ...');

                   //var element = document.getElementById("AddOnSection");
                   DashBoardObj = new DashBoardVm();

                   ko.applyBindings(DashBoardObj);

                   DashBoardObj.InitModel();

                   loading(false);

                   $("#DashboardSection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);
//*************************************//


function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}