﻿function AgenciesVm(mainVm) {

    var self = this;
    self.RootlVm = ko.observable();
    self.appData = mainVm;
    self.isShowErrorPanel = ko.observable(false);
    self.isShowErrorEdit = ko.observable(false);
    self.isSucess = ko.observable(false);   

    self.isShowErrorEditDetail = ko.observable(false);  

    self.ErrorList = ko.observableArray();
    self.AgenciesList = ko.observableArray();
    self.AgencyTypeList = ko.observableArray();
    self.CountryList = ko.observableArray();
    self.PromoCodeList = ko.observableArray();    

    self.ErrorListDetail = ko.observableArray();   
    

    self.selectedFromDate = ko.observable();
    self.selectedToDate = ko.observable();


    self.selectedFromDateDetail = ko.observable();
    self.selectedToDateDetail = ko.observable();    

    self.SelectedAgency = ko.observable(new AgenciesHeaderModel());
    self.SelectedAgencyContract = ko.observable(new AgencyContractModel());    
    self.selectedAgencyType = ko.observable(new AgencyTypesModel());
    self.selectedCountry = ko.observable(new CountriesModel());
    self.selectedPromoCodeType = ko.observable(new PromoCodeModel());
    self.selectedPromoCode = ko.observable(new PromoCodeModel());
    self.selectedPromoId = ko.observable(0);
 
    self.isRefresh = ko.observable(false);

    self.isEditingDetail = ko.observable(false);
    self.ShowGridDetailDetail = ko.observable(true);  

    //AGENCIES GRID ///////////////////////////////////////////////////////////    
    self.totalPageCount = ko.observable();
    self.pageNoLabel = ko.observable();
    self.pageNumber = ko.observable(1)
    self.selectedPageSize = ko.observable(10);
    self.pageSizeOptions = ko.observableArray();
    self.pageSizeOptions(['5', '10', '20']);
    self.orderBy = ko.observable("agency_code");
    self.isAccending = ko.observable(true);
    self.isUp = ko.observable(false);
    self.isDown = ko.observable(false);
    //AGENCIES GRID /////////////////////////////////////////////////////////// 

    //DETAILS GRID ///////////////////////////////////////////////////////////
    self.ContractDetailList = ko.observableArray();
    self.ContractDetailListTemp = ko.observableArray();
    self.totalPageCountDetail = ko.observable();
    self.pageNoLabelDetail = ko.observable();
    self.pageNumberDetail = ko.observable(1);
    self.selectedPageSizeDetail = ko.observable(10);
    self.pageSizeOptionsDetail = ko.observableArray();
    self.pageSizeOptionsDetail(['5', '10', '20']);
    self.orderByDetail = ko.observable("agency_contract_code");
    self.isAccendingDetail = ko.observable(true);
    self.isUpDetail = ko.observable(false);
    self.isDownDetail = ko.observable(false);
    //DETAILS GRID /////////////////////////////////////////////////////////// 

    //PROMOCODES GRID ///////////////////////////////////////////////////////////
    self.PromoDetailList = ko.observableArray();
    self.PromoDetailListTemp = ko.observableArray();
    self.totalPageCountDetailPromo = ko.observable();
    self.pageNoLabelDetailPromo = ko.observable();
    self.pageNumberDetailPromo = ko.observable(1);
    self.selectedPageSizeDetailPromo = ko.observable(10);
    self.pageSizeOptionsDetailPromo = ko.observableArray();
    self.pageSizeOptionsDetailPromo(['5', '10', '20']);
    self.orderByDetailPromo = ko.observable("pc.promo_code");
    self.isAccendingDetailPromo = ko.observable(true);
    self.isUpDetailPromo = ko.observable(false);
    self.isDownDetailPromo = ko.observable(false);
    self.ShowGridDetailDetailPromo = ko.observable(true);
    self.isEditingDetailPromo = ko.observable(false);
    //PROMOCODES GRID /////////////////////////////////////////////////////////// 


    self.isEditing = ko.observable(false);
    self.SelectAll = ko.observable(false);

    //*******************DEFAULT VALUES START*****************/
    self.RootlVm(mainVm);

    //*******************DEFAULT VALUES END*****************//

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }


    self.ShowErrorListDetail = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);

    }


    self.ShowEditErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorEdit(true);
    }


    self.ShowEditErrorListDetail = function (data) {
        self.ErrorListDetail.push({ ErrorText: data });
        self.isShowErrorEditDetail(true);
    } 


    self.ClearErrors = function () {
        self.ErrorList.removeAll();
        self.ErrorListDetail.removeAll();        
        self.isShowErrorEdit(false);
        self.isShowErrorEditDetail(false);     
    }



    self.GetAgenciesTypes = function (AgencyTypeCode) {

        self.AgencyTypeList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgenciesTypes?AgencyTypeCode=" + AgencyTypeCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var room = new AgencyTypesModel();
                            room.MapEntity(value, room);
                            self.AgencyTypeList.push(room);
                        })
                    }
                }
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }


    self.GetAgenciesHeader = function (AgencyCode) {

        loading(true, 'Please wait ...');

        self.AgenciesList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgencies?AgencyCode=" + AgencyCode + "&OrderBy=" + self.orderBy() + '&IsAccending='
                           + self.isAccending() + '&PageNumber=' + self.pageNumber() + '&PageSize=' + self.selectedPageSize(),
            //url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgencies?AgencyCode=" + AgencyCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var objDTO = new AgenciesHeaderModel();
                            objDTO.MapEntity(value, objDTO);
                            self.AgenciesList.push(objDTO);
                        })
                    }
                }

                loading(false);



            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetCountries = function () {

        self.CountryList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/VirtualReceipts/GetCountries",
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {


                if ((response !== null) && (response !== '')) {

                    var ds = jQuery.parseJSON(response);

                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var CountryDTO = new CountriesModel();
                            CountryDTO.MapEntity(value, CountryDTO);
                            self.CountryList.push(CountryDTO);

                        })
                    }
                }


            },
            error: function (errorResponse) {
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.GetPromoCodes = function (PromoCode) {

        self.PromoCodeList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetActivePromoCodes?sPromoCode=" + PromoCode,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        if (ds.length > 0) {
                            $("#PromoCodes").show();
                            $("#NoPromoCodesSection").hide();                            
                            $.each(ds, function (key, value) {
                                var PromoCodeDTO = new PromoCodeModel();
                                PromoCodeDTO.MapEntity(value, PromoCodeDTO);
                                self.PromoCodeList.push(PromoCodeDTO);
                            })                            
                        } else {
                            $("#PromoCodes").hide();
                            $("#NoPromoCodesSection").show();                            
                        }
                        
                    } else {
                        $("#PromoCodes").hide();
                        $("#NoPromoCodesSection").show();                        
                    }
                }
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }


    self.RefreshClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.GetAgenciesHeader(0);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
    }


    self.AddClick = function () {

        self.isRefresh(false);
        self.isEditing(true);
        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);       

        self.isEditingDetail (false);
        self.ShowGridDetailDetail(true);

        self.isEditingDetailPromo(false);
        self.ShowGridDetailDetailPromo(true);

        self.ClearErrors();

        self.SelectedAgency(new AgenciesHeaderModel());

        self.GeneralInfoClick();
        $("#TabsDiv").hide();

   


    }



    ko.bindingHandlers.FromDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    //if (inst.id == "FromDate") {
                    //    self.selectedFromDate(date.ConvertDateToMMDDYYFormat());
                    //}

                    if (inst.id == "FromDateDetail") {
                        self.selectedFromDateDetail(date.ConvertDateToMMDDYYFormat());
                    }                 

                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    ko.bindingHandlers.ToDatePicker = {
        init: function (element, valueAccessor) {
            $(element).datepicker({
                onSelect: function (dateText, inst) {
                    var date = new Date(dateText);
                    //if (inst.id == "ToDate") {
                    //    self.selectedToDate(date.ConvertDateToMMDDYYFormat());
                    //}

                    if (inst.id == "ToDateDetail") {
                        self.selectedToDateDetail(date.ConvertDateToMMDDYYFormat());
                    }
                  
                },
                dateFormat: 'mm/dd/yy'
            });
        }
    };



    self.ValidateData = function () {

        var valid = true;

        self.ErrorList.removeAll();
        self.isShowErrorEdit(false);

        //if (self.SelectedAgency().ppAgencyCode() == null || self.SelectedAgency().ppAgencyCode() == undefined || self.SelectedAgency().ppAgencyCode() == "") {
        //    ValidateMsgText = "The Rate Code cannot be empty, please correct\n";
        //    self.ShowEditErrorList(ValidateMsgText);
        //    valid = false;
        //}

        if (self.SelectedAgency().ppAgencyName() == null || self.SelectedAgency().ppAgencyName() == undefined || self.SelectedAgency().ppAgencyName() == "") {
            ValidateMsgText = "The Agency Name cannot be empty, please correct\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectedAgency().ppAgencyTypeCode() == null || self.SelectedAgency().ppAgencyTypeCode() == undefined || self.SelectedAgency().ppAgencyTypeCode() == "") {
            ValidateMsgText = "Agency Type is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }

        if (self.SelectedAgency().ppCountry() == null || self.SelectedAgency().ppCountry() == undefined || self.SelectedAgency().ppCountry() == "") {
            ValidateMsgText = "Country is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectedAgency().ppPhone() == null || self.SelectedAgency().ppPhone() == undefined || self.SelectedAgency().ppPhone() == "") {
            ValidateMsgText = "Agency phone is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;

        }


        if (self.SelectedAgency().ppContactName() == null || self.SelectedAgency().ppContactName() == undefined || self.SelectedAgency().ppContactName() == "") {
            ValidateMsgText = "Contact Name is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        }


        if (self.SelectedAgency().ppContactEmail() == null || self.SelectedAgency().ppContactEmail() == undefined || self.SelectedAgency().ppContactEmail() == "") {
            ValidateMsgText = "Contact Email is required\n";
            self.ShowEditErrorList(ValidateMsgText);
            valid = false;
        } else {
            if (!validateEmail(self.SelectedAgency().ppContactEmail())) {
                ValidateMsgText = "Contact Email is invalid\n";
                self.ShowEditErrorList(ValidateMsgText);
                valid = false;
            }
        }

        if (self.SelectedAgency().ppNotificationEmailReservations() != null && self.SelectedAgency().ppNotificationEmailReservations() != undefined && self.SelectedAgency().ppNotificationEmailReservations() != "") {
            var valuesarray = self.SelectedAgency().ppNotificationEmailReservations().split(",");            
            if (valuesarray.length > 0) {
                for (var i = 0, len = valuesarray.length; i < len; i++) {                  
                    if (!validateEmail($.trim(valuesarray[i]))) {
                        ValidateMsgText = "At least one email in [Notification Emails] is invalid\n";
                        self.ShowEditErrorList(ValidateMsgText);
                        valid = false;
                        break;
                    }
                }              
            }           
        } 

        //if (self.selectedAgencyType() != null && self.selectedAgencyType().ppAgencyTypeCode() != null && self.selectedAgencyType().ppAgencyTypeCode() != undefined) {
        //    self.SelectedAgency().ppAgencyTypeCode(self.selectedAgencyType().ppAgencyTypeCode());
        //}        

        return valid;
    }

    self.CloseClick = function () {
        self.isEditing(false);
        self.isRefresh(true);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.ClearErrors();

    }

    self.SaveClick = function () {

        var valid = true;

        try {



            //var DateFrom = GetUniversalDate(self.selectedFromDate());
            //var DateTo = GetUniversalDate(self.selectedToDate());

            //self.SelectedAgency().UnFormatFrom(DateFrom);
            //self.SelectedAgency().UnFormatTo(DateTo);

            if (self.selectedCountry() != null && self.selectedCountry().ppCountryIso3() != null && self.selectedCountry().ppCountryIso3() != undefined) {
                self.SelectedAgency().ppCountry(self.selectedCountry().ppCountryIso3());
            }

            if (self.selectedAgencyType() != null && self.selectedAgencyType().ppAgencyTypeCode() != null && self.selectedAgencyType().ppAgencyTypeCode() != undefined) {
                self.SelectedAgency().ppAgencyTypeCode(self.selectedAgencyType().ppAgencyTypeCode());
            }

            valid = self.ValidateData();

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newobj = JSON.stringify(ko.toJS(self.SelectedAgency()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Agencies/SaveHeader",
                    async: false,
                    crossDomain: true,
                    data: newobj,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.CloseClick();
                            self.GetAgenciesHeader(0);
                            self.isSucess(true);
                            $("#AgencyMsg").show();
                            $("#AgencyMsg").html("<h5>" + "Agency saved successfully" + "</h5>").fadeIn(0);
                            $("#AgencyMsg").html("<h5>" + "Agency saved successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the Agency";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#AgencyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);
        }

    }

    self.DeleteClick = function (data) {


        try {

            if (self.SelectedAgency() != null && self.SelectedAgency() != undefined && self.SelectedAgency().ppAgencyCode() != undefined && self.SelectedAgency().ppAgencyCode() != null) {

                self.isShowErrorPanel(false);
                $("#DeleteAgencies").html($("<h5>" + "Are you sure you want to delete the Agency?" + "</h5>"));



                $("#DeleteAgencies").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Agencies/DeleteHeader?AgencyCode=" + self.SelectedAgency().ppAgencyCode(),
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteAgencies").dialog("close");


                                    if (response === "0") {
                                        self.GetAgenciesHeader(0);
                                        self.isSucess(true);

                                        $("#AgencyMsg").show();
                                        $("#AgencyMsg").html("<h5>" + "Agency deleted successfully" + "</h5>").fadeIn(0);
                                        $("#AgencyMsg").html("<h5>" + "Agency deleted successfully" + "</h5>").fadeOut(3000);

                                    }
                                    else {
                                        $("#AgencyMsg").show();
                                        $("#AgencyMsg").html("<h5>" + "An error has ocurred deleting the Agency" + "</h5>").fadeIn(0);
                                        $("#AgencyMsg").html("<h5>" + "An error has ocurred deleting the Agency" + "</h5>").fadeOut(3000);

                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    $("#AgencyMsg").dialog("close");
                                    //$("#LocationMsg").hidden();
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteAgencies").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
            self.ShowErrorList(errorMsg);

        }

    }

    self.errorExpanderClick = function (controlID) {

        $('#' + controlID).toggleClass("errorExpanded");
        $("#" + controlID + "  span.fa-caret-down").toggle();
        $("#" + controlID + "  span.fa-caret-up").toggle();
    }

    self.GeneralInfoClick = function () {

        $("#GeneralInfoDiv").show();
        $("#ContractsDiv").hide();
        $("#PromosDiv").hide();

        $("#GeneralInfoTab").addClass("activePageMenu");
        $("#ContractsTab").removeClass("activePageMenu");
        $("#PromosTab").removeClass("activePageMenu");

    }

    self.ContractsClick = function () {        

        $("#GeneralInfoDiv").hide();
        $("#ContractsDiv").show();
        $("#PromosDiv").hide();

        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#ContractsTab").addClass("activePageMenu");
        $("#PromosTab").removeClass("activePageMenu");

    }

    self.PromosClick = function () {

        $("#GeneralInfoDiv").hide();
        $("#ContractsDiv").hide();
        $("#PromosDiv").show();

        $("#GeneralInfoTab").removeClass("activePageMenu");
        $("#ContractsTab").removeClass("activePageMenu");
        $("#PromosTab").addClass("activePageMenu");

    }

    self.RelatePromoCodeClick = function () {

        var valid = true;

        try {            

            //valid = self.ValidateData();

            self.selectedPromoCode().ppAgencyCode(self.SelectedAgency().ppAgencyCode());
            self.selectedPromoCode().ppPromoID(self.selectedPromoId());

            if (valid === true) {

                loading(true, 'Please wait ...');

                var newobj = JSON.stringify(ko.toJS(self.selectedPromoCode()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Agencies/RelatePromoCode",
                    async: false,
                    crossDomain: true,
                    data: newobj,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.GetDetailsPromo();

                            //-- Refresh promo codes dropdown
                            self.GetPromoCodes('');
                            self.ValidateAvailablePromoCodes();
                            $("#PromoInfoSection").hide();
                            PromoCodesDropDownChangeAction();
                            //--

                            self.isSucess(true);                            
                            $("#AgencyMsg").show();
                            $("#AgencyMsg").html("<h5>" + "Promotional code related successfully" + "</h5>").fadeIn(0);
                            $("#AgencyMsg").html("<h5>" + "Promotional code related successfully" + "</h5>").fadeOut(8000);
                        }
                        else {

                            ValidateMsgText = "An error has ocurred relating the promotional code";
                            self.ShowEditErrorList(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        $("#AgencyMsg").dialog("close");
                        //$("#LocationMsg").hidden();
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowErrorList(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);
        }

    }

    self.EditAgency = function (data) {

        
        try {

            self.isEditing(true);
            self.isShowErrorEdit(false);
            self.isShowErrorPanel(false);
            loading(true, 'Please wait ...');
            self.isRefresh(false);
            self.SelectedAgency(data);


            $("#TabsDiv").show();
            //self.GeneralInfoClick();

            //self.selectedFromDate(formatDate(self.SelectedAgency().ppStartDate(), '/'));
            //self.selectedToDate(formatDate(self.SelectedAgency().ppEndDate(), '/'));

            self.GeneralInfoClick();
            self.GetDetails();
            self.GetTotalPageCountDetail();

            self.isEditingDetail(false);
            self.ShowGridDetailDetail(true);

            self.GetPromoCodes('');
            self.GetDetailsPromo();
            self.GetTotalPageCountDetailPromo();

            self.isEditingDetailPromo(false);
            self.ShowGridDetailDetailPromo(true);
            
            $('#ddlPromoCode')
                .val(-1)
                .trigger('change');
            
            //-- Validate Promo codes in dropdownlist agains the promo codes in grid
            //self.PromoCodeList -- combo

            //self.PromoDetailList -- grid     

            self.ValidateAvailablePromoCodes();            

            loading(false);
        }
        catch (err) {
            loading(false);
            self.ShowErrorList(err.message);

        }


    }

    //Agency GRID Functions
    self.GetTotalPageCount = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgenciesCount?AgencyCode=0&PageSize=" + self.selectedPageSize(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCount(PageCount);
                    self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSize = function () {

        self.pageNumber(1);
        self.GetAgenciesHeader(0);
        self.GetTotalPageCount();
    }

    self.GetSelectedPage = function () {

        if (self.pageNumber() <= 0) {
            self.pageNumber(1);
        }
        else {
            if (self.totalPageCount() >= self.pageNumber()) {
                var PageNo = parseInt(self.pageNumber());
                self.pageNumber(PageNo);
            }
            else {
                self.pageNumber(self.totalPageCount());
            }
        }

        self.GetAgenciesHeader(0);

    }

    self.SetOrderBy = function (data) {
        if (data != self.orderBy()) {
            self.orderBy(data);
            self.isAccending(true)
            self.isDown(false);
            self.isUp(true);
            self.GetAgenciesHeader(0);

        }
        else {
            if (self.isAccending() == true) {
                self.isAccending(false);
                self.isUp(false);
                self.isDown(true);

            }
            else {
                self.isAccending(true);
                self.isDown(false);
                self.isUp(true);
            }

            self.GetAgenciesHeader(0);

        }
    }

    self.ForwardPageClick = function () {

        if (self.totalPageCount() > self.pageNumber()) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumbe(PageNo + 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());
            self.GetAgenciesHeader(0);
        }
    }

    self.BackwardpageClick = function () {
        if (self.pageNumber() > 1) {
            var PageNo = parseInt(self.pageNumber());
            self.pageNumber(PageNo - 1);
            self.pageNoLabel(self.pageNumber() + " of " + self.totalPageCount());

            self.GetAgenciesHeader(0);

        }
    }
    ///////////////////////

    //DETAILS GRID ///////////////////////////////////////////////////////////

    self.GetAgencyContractData = function () {

        loading(true, 'Please wait ...');
        self.ContractDetailListTemp.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgencyContractData?AgencyContractCode=" + self.SelectedAgencyContract().ppAgencyContractCode(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var DetailModel = new AgencyContractModel();
                            DetailModel.MapEntity(value, DetailModel);
                            self.ContractDetailListTemp.push(DetailModel);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.GetDetails = function () {

        loading(true, 'Please wait ...');
        self.ContractDetailList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetDetail?AgencyCode=" + self.SelectedAgency().ppAgencyCode() + "&OrderBy=" + self.orderByDetail() + '&IsAccending='
                + self.isAccendingDetail() + '&PageNumber=' + self.pageNumberDetail() + '&PageSize=' + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var DetailModel = new AgencyContractModel();
                            DetailModel.MapEntity(value, DetailModel);
                            self.ContractDetailList.push(DetailModel);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }

         
        });
    }

    self.GetTotalPageCountDetail = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetDetailCount?AgencyCode=" + self.SelectedAgency().ppAgencyCode() + "&PageSize=" + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCountDetail(PageCount);
                    self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeDetail = function () {

        self.pageNumberDetail(1);
        self.GetDetails();
        self.GetTotalPageCountDetail();
    }

    self.GetSelectedPageDetail = function () {

        if (self.pageNumberDetail() <= 0) {
            self.pageNumberDetail(1);
        }
        else {
            if (self.totalPageCountDetail() >= self.pageNumberDetail()) {
                var PageNo = parseInt(self.pageNumberDetail());
                self.pageNumberDetail(PageNo);
            }
            else {
                self.pageNumberDetail(self.totalPageCountDetail());
            }
        }

        self.GetDetails();

    }

    self.SetOrderByDetail = function (data) {
        if (data != self.orderByDetail()) {
            self.orderByDetail(data);
            self.isAccendingDetail(true)
            self.isDownDetail(false);
            self.isUpDetail(true);
            self.GetDetails();

        }
        else {
            if (self.isAccendingDetail() == true) {
                self.isAccendingDetail(false);
                self.isUpDetail(false);
                self.isDownDetail(true);

            }
            else {
                self.isAccendingDetail(true);
                self.isDownDetail(false);
                self.isUpDetail(true);
            }

            self.GetDetails();

        }
    }

    self.ForwardPageClickDetail = function () {

        if (self.totalPageCountDetail() > self.pageNumberDetail()) {
            var PageNo = parseInt(self.pageNumberDetail());
            self.pageNumberDetail(PageNo + 1);
            self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());
            self.GetDetails();
        }
    }

    self.BackwardpageClickDetail = function () {
        if (self.pageNumberDetail() > 1) {
            var PageNo = parseInt(self.pageNumberDetail());
            self.pageNumberDetail(PageNo - 1);
            self.pageNoLabelDetail(self.pageNumberDetail() + " of " + self.totalPageCountDetail());

            self.GetDetails();

        }
    }

    self.RefreshClickDetail = function () {
        self.orderByDetail = ko.observable("agency_contract_code");
        self.GetDetails();
        self.GetTotalPageCountDetail();
    }

    self.AddClickDetail = function () {
        self.SelectedAgencyContract(new AgencyContractModel());
        self.isEditingDetail(true);
        self.ShowGridDetailDetail(false);

        //Hide "add contract file section", because the contract has to be created first
        $("#ContractFileSection").hide();        
    }

    self.DeleteClickDetail = function () {

        var valid = true;

        try {


            if (self.SelectedAgencyContract() != null && self.SelectedAgencyContract() != undefined ) {

                self.isShowErrorPanel(false);
                $("#DeleteAgencies").html($("<h5>" + "Are you sure you want to delete the agency contract?" + "</h5>"));


                var Detail = JSON.stringify(ko.toJS(self.SelectedAgencyContract()));


                $("#DeleteAgencies").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Agencies/DeleteAgencyContractDetail",
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                data: Detail,
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteAgencies").dialog("close");


                                    if (response === "0") {
                                        self.GetDetails();
                                    }
  

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteAgencies").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = err.message;
            self.ShowErrorList(errorMsg);

        }
    }

    self.ContractEntryDetail = function (data) {


        self.SelectedAgencyContract(data);

        self.selectedFromDateDetail(formatDate(self.SelectedAgencyContract().ppStart_date(), '/'));
        self.selectedToDateDetail(formatDate(self.SelectedAgencyContract().ppEnd_date(), '/'));


        var filteredType;
        var TypeID;        

        self.isEditingDetail(true);
        self.ShowGridDetailDetail(false);

        if ((self.SelectedAgencyContract().ppAgencyContractCode() !== "") && (self.SelectedAgencyContract().ppAgencyContractCode() !== "0") && (self.SelectedAgencyContract().ppAgencyContractCode() !== undefined)) {
            self.GetAgencyContractData();
            var filename = '';
            if (self.ContractDetailListTemp() != null) {
                
                for (var i = 0, len = self.ContractDetailListTemp().length; i < len; i++) {
                    if (self.SelectedAgencyContract().ppAgencyContractCode() == self.ContractDetailListTemp()[i].ppAgencyContractCode()) {
                        filename = self.ContractDetailListTemp()[i].ppContractFileName();
                        break;
                    }
                }
            }

            $("#ContractFileSection").show();
            if (filename != null && filename != "") {
                $("#lbDownloadFile").show();
                if (filename != '') {
                    $("#lbDownloadFile").text('Download Contract File: ' + filename);
                } else {
                    $("#lbDownloadFile").text('Download Contract File uploaded...');
                }
                
            } else {
                $("#lbDownloadFile").hide();
            }
        }
        else {
            $("#ContractFileSection").hide();
        }

        $("#hfContractCode").val(self.SelectedAgencyContract().ppAgencyContractCode());
   

    }

    self.CloseClickDetail = function (data) {


        self.isEditingDetail(false);
        self.ShowGridDetailDetail(true);
        self.ClearErrors();


    }

    self.ValidateDetail = function () {

        var valid = true;

        self.ErrorListDetail.removeAll();

        if (self.selectedFromDateDetail() == null || self.selectedFromDateDetail() == undefined || self.selectedFromDateDetail() == "") {   
            self.ShowEditErrorListDetail("start date field is required");
            valid=false;
        }


        if (self.selectedToDateDetail() == null || self.selectedToDateDetail() == undefined || self.selectedToDateDetail() == "") {
            self.ShowEditErrorListDetail("end start field is required");
            valid = false;
        }

        if (valid === false) { return;}
        
        //var ExpiryDate = new Date(parseInt(JsonExpiryDate.substr(6)))
        //if (ExpiryDate != null) {
        //    var today = new Date();

        //    if (ExpiryDate <= today) {

        //    }
        //}

        var DateFrom = GetUniversalDate(self.selectedFromDateDetail());
        var DateTo = GetUniversalDate(self.selectedToDateDetail());

        self.SelectedAgencyContract().UnFormatFrom(DateFrom);
        self.SelectedAgencyContract().UnFormatTo(DateTo);

        if (self.SelectedAgencyContract().UnFormatFrom() > self.SelectedAgencyContract().UnFormatTo()) {
            self.ShowEditErrorListDetail("The Start Date cannot be greater than End Date, please correct");
            valid = false;
        }

        if (self.ContractDetailList() != null) {
            for (var i = 0, len = self.ContractDetailList().length; i < len; i++) {
                if (self.SelectedAgencyContract().ppAgencyContractCode() != self.ContractDetailList()[i].ppAgencyContractCode()) {
                    
                    var dtFrom = GetUniversalDate(formatDate(self.ContractDetailList()[i].ppStart_date(), '/'));
                    var dtTo = GetUniversalDate(formatDate(self.ContractDetailList()[i].ppEnd_date(), '/'));

                    if (((self.SelectedAgencyContract().UnFormatFrom() >= dtFrom) && (self.SelectedAgencyContract().UnFormatFrom() <= dtTo))
                    || ((self.SelectedAgencyContract().UnFormatTo() >= dtFrom) && (self.SelectedAgencyContract().UnFormatTo() <= dtTo)))
                    {
                        self.ShowEditErrorListDetail("There is already a contract including the dates selected, please correct");
                        valid = false;
                        break;
                    }                    
                }                
            }
        }

        //if (self.selectedAgencyType() === null || self.selectedAgencyType().ppAgencyTypeCode() === null || self.selectedAgencyType().ppAgencyTypeCode() === undefined) {
        //    self.ShowEditErrorListDetail("Select a valid agency type");
        //    valid = false;
        //    return;
        //}

        //if (self.SelectedAgency() === null || self.SelectedAgency().ppRateId() === null || self.SelectedAgency().ppRateId() === undefined) {
        //    self.ShowEditErrorListDetail("Rate header not found");
        //    valid = false;
        //    return;
        //}

        //if (self.selectedAgencyType() != null && self.selectedAgencyType().ppAgencyTypeCode() != null && self.selectedAgencyType().ppAgencyTypeCode() != undefined) {
        //    self.SelectedAgencyContract().ppAgencyTypeCode(self.selectedAgencyType().ppAgencyTypeCode());
        //}


        self.SelectedAgencyContract().ppAgencyCode(self.SelectedAgency().ppAgencyCode());

        return valid;
    }

    self.SaveClickDetail = function () {

        var valid = true;

        try {

            valid = self.ValidateDetail();


            if (valid === true) {



                loading(true, 'Please wait ...');

                var newDetail = JSON.stringify(ko.toJS(self.SelectedAgencyContract()));

                $.ajax({
                    type: "POST",
                    url: $("#txtTrackingURLBase").val() + "/Agencies/SaveDetail",
                    async: false,
                    crossDomain: true,
                    data: newDetail,
                    contentType: 'application/json',
                    success: function (response) {

                        loading(false);
                        if (response !== "-1") {
                            self.CloseClickDetail();
                            self.GetDetails();
                        }
                        else {

                            ValidateMsgText = "An error has ocurred saving the agency contract";
                            self.ShowEditErrorListDetail(ValidateMsgText);

                        }

                    },
                    error: function (errorResponse) {
                        loading(false);
                        var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                        self.ShowEditErrorListDetail(errorMsg);
                    }
                });
            }


        }
        catch (err) {
            loading(false);
            self.ShowEditErrorListDetail(err.message);
        }

    }



    //DETAILS GRID ///////////////////////////////////////////////////////////    


    //PROMOS GRID ////////////////////////////////////////////////////////////

    self.GetDetailsPromo = function () {

        loading(true, 'Please wait ...');
        self.PromoDetailList.removeAll();

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetDetailPromo?AgencyCode=" + self.SelectedAgency().ppAgencyCode() + "&OrderBy=" + self.orderByDetailPromo() + '&IsAccending='
                + self.isAccendingDetailPromo() + '&PageNumber=' + self.pageNumberDetailPromo() + '&PageSize=' + self.selectedPageSizeDetailPromo(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        $.each(ds, function (key, value) {
                            var DetailModelPromo = new PromoCodeModel();
                            DetailModelPromo.MapEntity(value, DetailModelPromo);
                            self.PromoDetailList.push(DetailModelPromo);
                        })
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }


        });
    }

    self.GetTotalPageCountDetailPromo = function (data) {

        loading(true, 'Please wait ...');

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Agencies/GetAgencyPromoCount?AgencyCode=" + self.SelectedAgency().ppAgencyCode() + "&PageSize=" + self.selectedPageSizeDetail(),
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if (response != null) {
                    var PageCount = response;
                    self.totalPageCountDetailPromo(PageCount);
                    self.pageNoLabelDetailPromo(self.pageNumberDetailPromo() + " of " + self.totalPageCountDetailPromo());
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });

    }

    self.GetSelectedPageSizeDetailPromo = function () {

        self.pageNumberDetailPromo(1);
        self.GetDetailsPromo();
        self.GetTotalPageCountDetailPromo();
    }

    self.GetSelectedPageDetailPromo = function () {

        if (self.pageNumberDetailPromo() <= 0) {
            self.pageNumberDetailPromo(1);
        }
        else {
            if (self.totalPageCountDetailPromo() >= self.pageNumberDetailPromo()) {
                var PageNo = parseInt(self.pageNumberDetailPromo());
                self.pageNumberDetailPromo(PageNo);
            }
            else {
                self.pageNumberDetailPromo(self.totalPageCountDetailPromo());
            }
        }

        self.GetDetailsPromo();

    }

    self.SetOrderByDetailPromo = function (data) {
        if (data != self.orderByDetailPromo()) {
            self.orderByDetailPromo(data);
            self.isAccendingDetailPromo(true)
            self.isDownDetailPromo(false);
            self.isUpDetailPromo(true);
            self.GetDetailsPromo();

        }
        else {
            if (self.isAccendingDetailPromo() == true) {
                self.isAccendingDetailPromo(false);
                self.isUpDetailPromo(false);
                self.isDownDetailPromo(true);

            }
            else {
                self.isAccendingDetailPromo(true);
                self.isDownDetailPromo(false);
                self.isUpDetailPromo(true);
            }

            self.GetDetailsPromo();

        }
    }

    self.ForwardPageClickDetailPromo = function () {

        if (self.totalPageCountDetailPromo() > self.pageNumberDetailPromo()) {
            var PageNo = parseInt(self.pageNumberDetailPromo());
            self.pageNumberDetailPromo(PageNo + 1);
            self.pageNoLabelDetailPromo(self.pageNumberDetailPromo() + " of " + self.totalPageCountDetailPromo());
            self.GetDetailsPromo();
        }
    }

    self.BackwardpageClickDetailPromo = function () {
        if (self.pageNumberDetailPromo() > 1) {
            var PageNo = parseInt(self.pageNumberDetailPromo());
            self.pageNumberDetailPromo(PageNo - 1);
            self.pageNoLabelDetailPromo(self.pageNumberDetailPromo() + " of " + self.totalPageCountDetailPromo());

            self.GetDetailsPromo();

        }
    }

    self.RefreshClickDetailPromo = function () {
        self.orderByDetailPromo = ko.observable("pc.promo_code");
        self.GetDetailsPromo();
        self.GetTotalPageCountDetailPromo();
    }

    //self.AddClickDetailPromo = function () {
    //    self.SelectedAgencyContract(new AgencyContractModel());
    //    self.isEditingDetail(true);
    //    self.ShowGridDetailDetail(false);

    //    //Hide "add contract file section", because the contract has to be created first
    //    $("#ContractFileSection").hide();
    //}

    self.DeleteClickDetailPromo = function () {

        var valid = true;

        self.selectedPromoCode().ppAgencyCode(self.SelectedAgency().ppAgencyCode());

        try {

            if (self.PromoDetailList() != null) {
                if (self.PromoDetailList().length == 0) {
                    return false;
                }
            }

            if (self.selectedPromoCode() != null && self.selectedPromoCode() != undefined) {

                self.isShowErrorPanel(false);
                $("#DeleteAgencies").html($("<h5>" + "Are you sure you want to delete the promotional code?" + "</h5>"));

                var Detail = JSON.stringify(ko.toJS(self.selectedPromoCode()));

                $("#DeleteAgencies").dialog({
                    resizable: false,
                    modal: false,
                    closeOnEscape: false,
                    position: { at: "center  " },
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    },
                    buttons: {
                        Yes: function () {
                            loading(true, 'Please wait ...');
                            $.ajax({
                                type: "POST",
                                url: $("#txtTrackingURLBase").val() + "/Agencies/DeleteAgencyPromoDetail",
                                async: false,
                                crossDomain: true,
                                contentType: 'application/json',
                                data: Detail,
                                success: function (response) {

                                    loading(false);
                                    $("#DeleteAgencies").dialog("close");

                                    if (response === "0") {
                                        self.GetDetailsPromo();

                                        //-- Refresh promo codes dropdown
                                        self.GetPromoCodes('');
                                        self.ValidateAvailablePromoCodes();
                                        //--                                        

                                        PromoCodesDropDownChangeAction();
                                    }

                                },
                                error: function (errorResponse) {
                                    loading(false);
                                    var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                                    self.ShowErrorList(errorMsg);
                                }
                            });
                        },
                        No: function () {
                            loading(false);
                            $("#DeleteAgencies").dialog("close");

                        }
                    }
                });

            }


        }
        catch (err) {
            loading(false);

            var errorMsg = err.message;
            self.ShowErrorList(errorMsg);

        }
    }    

    self.CloseClickDetailPromo = function (data) {


        self.isEditingDetailPromo(false);
        self.ShowGridDetailDetailPromo(true);
        self.ClearErrors();


    }

    //-- Load promo codes in dropdown
    self.LoadPromoCodesInDropDownList = function () {
        var ddl = $("#ddlPromoCode");
        ddl.empty();
        if (self.PromoCodeList() != null) {
            if (self.PromoCodeList().length > 0) {
                $.each(self.PromoCodeList(), function () {
                    ddl.append($("<option />").val(this.ppPromoID()).text(this.ppPromoCode()));
                });
            } else {
                ddl.append($("<option />").val(-1).text("---"));
            }
        } else {
            ddl.append($("<option />").val(-1).text("---"));
        }
    }
    
    self.ValidateAvailablePromoCodes = function () {
        for (var i = 0; i < self.PromoDetailList().length; i++) {
            for (var j = 0; j < self.PromoCodeList().length; j++) {
                if (self.PromoCodeList()[j].ppPromoID() == self.PromoDetailList()[i].ppPromoID()) {
                    self.PromoCodeList().splice(j, 1);
                }
            }
        }

        self.LoadPromoCodesInDropDownList();
    }

    //PROMOS GRID ///////////////////////////////////////////////////////////

    self.InitModel = function () {
        self.isRefresh(true);
        self.isEditing(false);
        self.isSucess(false);
        self.isShowErrorEdit(false);
        self.isShowErrorPanel(false);
        self.GetAgenciesTypes(0);
        self.GetCountries();
        self.GetPromoCodes('');
        self.GetAgenciesHeader(0);
            
    }
   


    //***************** CALL CONSTRUCTER**********************//

    //self.InitModel();

    //***************** CALL CONSTRUCTOR END**********************//
}
//*************************************//


//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {               

               try {
                   loading(true, 'Please wait ...');


                   AgenciesObj = new AgenciesVm();

                   ko.applyBindings(AgenciesObj);

                   AgenciesObj.InitModel();

                   $("#dialog").dialog({
                       autoOpen: false,
                       modal: true,
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });

                   //-- Load countries in dropdownlist
                   AgenciesObj.LoadPromoCodesInDropDownList();

                   $('#txtUploadContractFile').on('change', function (e) {

                       try {

                           loading(true, 'Please wait ...');

                           var data = new FormData();

                           var files = $("#txtUploadContractFile").get(0).files;

                           var ContractCode = $("#hfContractCode").val();

                           // Add the uploaded image content to the form data collection
                           if (files.length > 0) {
                               data.append($("#txtTrackingURLBase").val() + "/Handlers/AgencyContractHandler.ashx?PictureNum=1&ContractCode=" + ContractCode, files[0]);
                           }

                           // Make Ajax request with the contentType = false, and procesDate = false
                           var ajaxRequest = $.ajax({
                               type: "POST",
                               url: $("#txtTrackingURLBase").val() + "/Handlers/AgencyContractHandler.ashx?PictureNum=1&ContractCode=" + ContractCode,
                               contentType: false,
                               processData: false,
                               data: data,
                               async: false
                           });

                           ajaxRequest.done(function (xhr, textStatus) {
                               // Do other operation
                               d = new Date();

                               //$("#Picture1").attr("src", $("#txtTrackingURLBase").val() + "/Handlers/AgencyContractHandler.ashx?" + d.getTime() + "&PictureNum=1&ContractCode=" + ContractCode);
                               $("#lbDownloadFile").show();
                               $("#lbDownloadFile").text('Download Contract File uploaded...');
                               $("#txtUploadContractFile").val('');
                               loading(false);
                           });


                       }
                       catch (ex) {
                           alert(ex);
                           loading(false);
                       }

                       loading(false);
                   });
                  
                   $("#lbDownloadFile").click(function () {                       
                       var url = $("#txtTrackingURLBase").val() + "/Handlers/AgencyContractHandler.ashx?ContractCode=" + AgenciesObj.SelectedAgencyContract().ppAgencyContractCode();
                       window.location.href = url;
                   });

                   PromoCodesDropDownChangeAction();

                   loading(false);

                   $("#AgencySection").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }




           }
);
//*************************************//

function PromoCodesDropDownChangeAction() {
    $("#ddlPromoCode").change(function () {
        var promocode = $(this).val();
        if ((promocode != -1)) {
            try {
                var filtered;
                filtered = Enumerable.From(AgenciesObj.PromoCodeList()).Where(function (x) { return x.ppPromoID().toString().toUpperCase().match("^" + promocode.toString().toUpperCase()) }).ToArray();

                if ((filtered !== null) && (filtered !== undefined) && (filtered.length > 0)) {
                    PopulatePromoLabels(filtered[0]);
                }

                $("#PromoInfoSection").show(500);
            }
            catch (ex) {
                $("#PromoInfoSection").hide();
                loading(false);
            }

        } else {
            $("#PromoInfoSection").hide(300);
        }
    });
}

function PopulatePromoLabels(objPromo) {
    if (objPromo != null) {
        $("#lblPromoDescription").text(objPromo.ppDescription());

        var PromoType = '';
        switch(objPromo.ppType()) {
            case "P":
                PromoType = 'Percentage';
                break;          
            default:
                PromoType = 'Amount';
        }
        
        $("#lblPromoType").text(PromoType);
        $("#lblPromoStartDate").text(objPromo.UnFormatFrom());
        $("#lblPromoEndDate").text(objPromo.UnFormatTo());

        AgenciesObj.selectedPromoId(objPromo.ppPromoID());
    }
}

function alertDialog(strMessage) {
    $("#DialogAlertMessage").html(strMessage);
    $("#ErrorDiv").html(strMessage);
    
    $("#dialog").show();
    $("#dialog").dialog("open");
}

//******************* Loading dialog ****************//
function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}


