﻿function ClosingGQuery(mainVm) {

    var self = this;

    self.ErrorList = ko.observableArray();
    self.isShowErrorPanel = ko.observable(false);
    self.isDetail = ko.observable(false);
    self.ReservationID = ko.observable();
    self.RoomsAvailability = ko.observableArray();
    self.RoomTypesInfo = ko.observableArray();
    self.RoomsDetails = ko.observableArray();

    self.ShowErrorList = function (data) {
        self.ErrorList.push({ ErrorText: data });
        self.isShowErrorPanel(true);
        $("#ErrorListDiv").show();

    }

    self.LoadAvailability = function (mydate) {      

        $.ajax({
            type: "GET",
            url: $("#txtTrackingURLBase").val() + "/Queries/GetRoomsClosingGeneral?CurrentDate=" + mydate,
            async: false,
            crossDomain: true,
            contentType: 'application/json',
            success: function (response) {
                if ((response !== null) && (response !== '')) {
                    var ds = jQuery.parseJSON(response);
                    if (ds !== null) {
                        if (ds.pploRoomTypes != null) {
                            $.each(ds.pploRoomTypes, function (key, value) {
                                var obj = new RoomsModel();
                                obj.MapEntity(value, obj);
                               
                                    self.RoomTypesInfo.push(obj);
                               
                            })
                        }

                        if (ds.pploRoomAvailability != null) {
                            $.each(ds.pploRoomAvailability, function (key, value) {                              
                                var obj = new AvailabilityRoomModel();
                                obj.MapEntity(value, obj);
                               
                                    self.RoomsAvailability.push(obj);
                               
                            })
                        }
                        
                    }
                }

                loading(false);
            },
            error: function (errorResponse) {
                loading(false);
                var errorMsg = ($.parseJSON(errorResponse.responseText).ErrorMessage);
                self.ShowErrorList(errorMsg);
            }
        });
    }

    self.InitModel = function () {

        self.LoadAvailability();
    }
}

var AvailObj;

//*******************Document ready - Apply Bindings*****************//
$(document).ready(
           function () {

               try {
                   loading(true, 'Please wait ...');


                   AvailObj = new ClosingGQuery();

                   ko.applyBindings(AvailObj);

                   AvailObj.InitModel();

                   if ($("#dt_availability").hasClass("is-datepick")) {
                       $("#dt_availability").datepick('destroy');
                   }

                   $("#dt_availability").datepick({
                       monthsToShow: 2,
                       //rangeSelect: true,
                       //showOtherMonths: true,
                       changeMonth: true,
                       prevText: 'Previous Month',
                       nextText: 'Next Month',
                       onDate: showDayCellData,
                       //onChangeMonthYear: loadDataOnMonthYearChange,
                       onSelect: dateSelected,
                       //onShow: onShowCalendar,
                       //showTrigger: '#calImg',
                       pickerClass: 'PickerStyle',
                       minDate: 0, maxDate: +330                       
                   });


                   $("#btnExport").click(function (e) {
                       e.preventDefault();

                       //getting data from our table
                       var data_type = 'data:application/vnd.ms-excel';
                       var table_div = document.getElementById('dt_availability');
                       var table_html = table_div.outerHTML.replace(/ /g, '%20');

                       var a = document.createElement('a');
                       a.href = data_type + ', ' + table_html;
                       a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
                       a.click();
                   });


                   

                   loading(false);                   

                   $("#AvailabilityQueryDiv").show(); //Show main div
               }
               catch (ex) {
                   alert(ex);
                   loading(false);
               }

           }
);

//*************************************//

function dateSelected(dates) {
    AvailObj.RoomsDetails([]);

    if (dates != null) {        
        var TypeRoomsNotAvailable = 0;
        var dateformatted = formatDate(dates[0], '/', 'ddmmyyyy');
        var datetext = $.datepick.formatDate('DD, M d, yyyy', dates[0]);
        $("#lbDateOnDetail").text(datetext);


        if (AvailObj.RoomsAvailability != null) {
            var filteredList;
            //linq to find the price for the specific date
            filteredList = Enumerable.From(AvailObj.RoomsAvailability()).Where(function (x) { return formatDateJsonLocal(x.ppDate(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
            if (filteredList != null) {
                if (filteredList.length > 0) {
                    $.each(filteredList, function (key, item) {
                        var obj = new AvailabilityRoomModel();                        
                        obj.ppRoomTypeName(item.ppRoom());                        
                        obj.ppRoomsQty(item.ppQuantity());
                        obj.ppRoomsOccup(item.ppOccupaid());
                        obj.ppRoomsBlocked(item.ppBlocked());
                        obj.ppRoomsAvail(item.ppAvailable());
                        AvailObj.RoomsDetails.push(obj);
                       
                        if (item.ppAvailable() <= 0) {
                            TypeRoomsNotAvailable += 1;
                        }
                    })

                    //-- check if there is no type of room in date
                    var MissingRoomTypeAdded = false;
                    if (filteredList.length < AvailObj.RoomTypesInfo().length) {
                        $.each(AvailObj.RoomTypesInfo(), function (key, roomtypeitem) {
                            var bFound = false;
                            $.each(filteredList, function (key, roomavailitem) {
                                if (roomtypeitem.ppRoomTypeCode() == roomavailitem.ppRoomTypeCode()) {
                                    bFound = true;
                                    return false;
                                }
                            })

                            if (!bFound) {
                                var obj = new AvailabilityRoomModel();                                
                                obj.ppRoomTypeName(roomtypeitem.ppDescription());
                                obj.ppRoomsQty(roomtypeitem.ppQuantity());
                                obj.ppRoomsOccup(0);
                                obj.ppRoomsBlocked(0);
                                obj.ppRoomsAvail(roomtypeitem.ppQuantity());
                                AvailObj.RoomsDetails.push(obj);
                            }
                        })
                    }
                    //--                  
                } else {                    
                    $.each(AvailObj.RoomTypesInfo(), function (key, item) {
                        var obj = new AvailabilityRoomModel();                        
                        obj.ppRoomTypeName(item.ppDescription());
                        obj.ppRoomsQty(item.ppQuantity());
                        obj.ppRoomsOccup(0);
                        obj.ppRoomsBlocked(0);
                        obj.ppRoomsAvail(item.ppQuantity());
                        AvailObj.RoomsDetails.push(obj);
                    })                    
                }
            }
           
        }
       
        $("#detailDialog").dialog({
            resizable: true,
            width: 600,
            heigh: 400,
            modal: false,
            closeOnEscape: true,
            position: { at: "center" },
            buttons: {
                OK: function () {
                    $("#detailDialog").dialog("close");
                }
            }
        });

        $("#detailDialog").show();
    }
    
}

function showDayCellData(date) {    
    var RoomTypesTable = '';
    var dateformatted = formatDate(date, '/', 'ddmmyyyy');
    var TypeRoomsNotAvailable = 0;
    var TypeRoomsNotAvailablebySale = 0;
    var CellClassStyle = 'DateCellStyle';
    var tooltip = $.datepick.formatDate('DD, M d, yyyy', date);           

    var currentdate = new Date();
    var dateinCalendar = new Date(date);
    var bselectable = true;

    dateinCalendar.setHours(0, 0, 0, 0)
    currentdate.setHours(0, 0, 0, 0)

    if (dateinCalendar >= currentdate) {
        if (AvailObj.RoomsAvailability() != null) {
            var filteredList;
            //linq to find the price for the specific date
            filteredList = Enumerable.From(AvailObj.RoomsAvailability()).Where(function (x) { return formatDateJsonLocal(x.ppDate(), '/', 'ddmmyyyy').match("^" + dateformatted) }).ToArray();
            if (filteredList != null) {
                if (filteredList.length > 0) {
                    RoomTypesTable = '<hr class="hrdatecell" /><ul>';
                    $.each(filteredList, function (key, item) {
                        var TableRow = '<li>' + item.ppRoom() + ': ' + item.ppAvailable() + '</li>'
                        RoomTypesTable = RoomTypesTable + TableRow;
                        
                        if (item.ppAvailable() <= 0) {
                            TypeRoomsNotAvailable += 1;
                        }
                    })

                    //-- check if there is no type of room in date
                    var MissingRoomTypeAdded = false;
                    if (filteredList.length < AvailObj.RoomTypesInfo().length) {
                        $.each(AvailObj.RoomTypesInfo(), function (key, roomtypeitem) {
                            var bFound = false;
                            $.each(filteredList, function (key, roomavailitem) {
                                if (roomtypeitem.ppRoomTypeCode() == roomavailitem.ppRoomTypeCode()) {
                                    bFound = true;
                                    return false;
                                }
                            })

                            if (!bFound){
                                var TableRow = '<li>' + roomtypeitem.ppDescription() + ': ' + roomtypeitem.ppQuantity() + '</li>'
                                RoomTypesTable = RoomTypesTable + TableRow;
                                MissingRoomTypeAdded = true;
                            }
                        })
                    }
                    //--

                    RoomTypesTable = RoomTypesTable + '</ul>';

                    if ( TypeRoomsNotAvailable ) {
                        CellClassStyle = 'UnavailableDateCellStyle';
                        tooltip = 'No rooms available'
                        bselectable = true;
                    }

                } else {
                    RoomTypesTable = '<hr class="hrdatecell" /><ul>';
                    $.each(AvailObj.RoomTypesInfo(), function (key, item) {
                        var TableRow = '<li>' + item.ppDescription() + ': ' + item.ppQuantity() + '</li>'
                        RoomTypesTable = RoomTypesTable + TableRow;
                        if (typeof (item.ppQuantity()) === "undefined") {
                            TypeRoomsNotAvailable += 1;
                        } else {
                            if (item.ppQuantity() <= 0) {
                                TypeRoomsNotAvailable += 1;
                            }
                        }
                    })

                    RoomTypesTable = RoomTypesTable + '</ul>';

                    if ( TypeRoomsNotAvailable) {
                        CellClassStyle = 'UnavailableDateCellStyle';
                        tooltip = 'No rooms available'
                        bselectable = false;
                    }
                }
            }
        }
    }

    var textcontent = '<div class="datenumberstyle">' + date.getDate() + '</div>' + '<div>' + RoomTypesTable + '</div>';
    //var textcontent = '' + dayname + '' + '<div class="datenumberstyle">' + date.getDate() + '</div>';

    return { content: textcontent, dateClass: CellClassStyle, selectable: bselectable, title: tooltip };
}

function loading(show, pmessage) {
    var text_message = '';

    if (pmessage != null) {
        if (pmessage != '') {
            text_message = pmessage
        }
    }

    if (show == true) {
        $.blockUI({
            message: '<div class="k-label" style="padding: 10px;"><img src="' + $("#txtTrackingURLBase").val() + '/Images/bx_loader.gif" /> ' + text_message + '<div/>',
            overlayCSS: { backgroundColor: '#fff' },
            css: { border: '2px solid #aaa' }
        });
    }
    else {
        setTimeout(function () {
            $.unblockUI();
        }, 1000);
    }
}

function formatDateJsonLocal(date, separator_char, returned_format) {
    var dateString = date;
    var dx = new Date(parseInt(dateString.substr(6)));

    var day = dx.getDate();
    var month = dx.getMonth() + 1;
    var year = dx.getFullYear();

    if (day <= 9) {
        day = "0" + day;
    }

    if (month <= 9) {
        month = "0" + month;
    }

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}

function formatDate(date, separator_char, returned_format) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var returneddate;

    if (returned_format == 'ddmmyyyy') {
        returned_format = [day, month, year].join(separator_char)
    } else if (returned_format == 'yyyymmdd') {
        returned_format = [year, month, day].join(separator_char);
    } else if (returned_format == 'mmddyyyy') {
        returned_format = [month, day, year].join(separator_char);
    }

    return returned_format;
}