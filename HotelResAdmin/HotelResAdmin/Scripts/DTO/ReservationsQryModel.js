﻿
ReservationsQryModel = function () {
    var self = this;

    self.check = ko.observable();
    self.reference = ko.observable();
    self.rateName = ko.observable();
    self.processor = ko.observable();
    self.statusText = ko.observable();
    self.telephone = ko.observable();
    self.country = ko.observable();
    self.hotelName = ko.observable();
    self.transactionBank = ko.observable();
    self.depositNumber = ko.observable();
    self.status = ko.observable();
    self.nights = ko.observable();
    self.visitReason = ko.observable();
    self.remarks = ko.observable();
    self.reservation = ko.observable();
    self.name = ko.observable();
    self.lastname = ko.observable();
    self.arrival = ko.observable();
    self.departure = ko.observable();
    self.created = ko.observable();
    self.void = ko.observable();
    self.rate = ko.observable();
    self.amount = ko.observable();
    self.taxAmount = ko.observable();
    self.addonAmount = ko.observable();
    self.promoAmount = ko.observable();
    self.commOL = ko.observable();
    self.commBank = ko.observable();
    self.total = ko.observable();
    self.email = ko.observable();
    self.creditCard = ko.observable();
    self.expDate = ko.observable();
    self.secCode = ko.observable();
    self.approveCode = ko.observable();
    self.cardType = ko.observable();



    self.MapEntity = function (serviceEntity, ReservationsQryDto) {
        var compareKey = {
            'reservation': {
                key: function (data) {
                    return data.reservation;
                }
            }
        };


        ko.mapping.fromJS(serviceEntity, compareKey, ReservationsQryDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}