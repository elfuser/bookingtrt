﻿RatesDetailModel = function () {
    var self = this;
    self.ppRowID = ko.observable();
    self.ppRoomCode = ko.observable();
    self.ppRoomName = ko.observable();
    self.ppRateCode = ko.observable();
    self.ppRateDet_id = ko.observable();
    self.ppRate_id = ko.observable();
    self.ppRoomtype_id = ko.observable();
    self.ppStart_date = ko.observable();
    self.ppEnd_date = ko.observable();
    self.ppAdult1 = ko.observable();
    self.ppAdult2 = ko.observable();
    self.ppAdult3 = ko.observable();
    self.ppAdult4 = ko.observable();
    self.ppAdult5 = ko.observable();
    self.ppAdult6 = ko.observable();
    self.ppAdult7 = ko.observable();
    self.ppAdult8 = ko.observable();
    self.ppAdult9 = ko.observable();
    self.ppAdult10 = ko.observable();
    self.ppAdult11 = ko.observable();
    self.ppAdult12 = ko.observable();
    self.ppChild = ko.observable();
    self.ppWAdult1 = ko.observable();
    self.ppWAdult2 = ko.observable();
    self.ppWAdult3 = ko.observable();
    self.ppWAdult4 = ko.observable();
    self.ppWAdult5 = ko.observable();
    self.ppWAdult6 = ko.observable();
    self.ppWAdult7 = ko.observable();
    self.ppWAdult8 = ko.observable();
    self.ppWAdult9 = ko.observable();
    self.ppWAdult10 = ko.observable();
    self.ppWAdult11 = ko.observable();
    self.ppWAdult12 = ko.observable();
    self.ppWChild = ko.observable();
    self.ppStart_dateAnt = ko.observable();
    self.ppEnd_dateAnt = ko.observable();
    self.FormatEndDate = ko.observable();
    self.FormatStartDate = ko.observable();


    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();

    self.MapEntity = function (serviceEntity, RatesDetailDTO) {
        var compareKey = {
            'ppRateDet_id': {
                key: function (data) {
                    return data.ppRateDet_id;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RatesDetailDTO);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}