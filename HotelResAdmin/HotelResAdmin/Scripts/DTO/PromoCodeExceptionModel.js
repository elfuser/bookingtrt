﻿
PromoCodeExceptionModel = function () {
    var self = this;


    self.PromoExc_id = ko.observable();
    self.Promo_id = ko.observable();
    self.End_date = ko.observable();
    self.UnFormatDate = ko.observable();


    self.MapEntity = function (serviceEntity, PromoCodeExceptionDto) {
        var compareKey = {
            'PromoExc_id': {
                key: function (data) {
                    return data.PromoExc_id;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, PromoCodeExceptionDto);


    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}