﻿CombinationsModel = function () {
    var self = this;
    self.ppRowID = ko.observable();
    self.ppAdults = ko.observable();
    self.ppChildren = ko.observable();
    self.ppCompanyCode = ko.observable();

    self.MapEntity = function (serviceEntity, CombinationsDto) {
        var compareKey = {
            'ppRowID': {
                key: function (data) {
                    return data.ppRowID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CombinationsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}