﻿
RatesDiscountModel = function () {
    var self = this;

    self.ppTypeDesc = ko.observable();
    self.ppRowID = ko.observable();
    self.ppType = ko.observable();
    self.ppStartDate = ko.observable();
    self.ppEndDate = ko.observable();
    self.ppError = ko.observable();
    self.ppRateCode = ko.observable();
    self.ppDisc1 = ko.observable();
    self.ppDisc2 = ko.observable();
    self.ppDisc3 = ko.observable();
    self.ppDisc4 = ko.observable();
    self.ppDisc5 = ko.observable();
    self.ppDisc6 = ko.observable();
    self.ppDisc7 = ko.observable();
    self.ppDisc8 = ko.observable();
    self.ppDiscount = ko.observable();


    self.FormatEndDate = ko.observable();
    self.FormatStartDate = ko.observable();


    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();


    self.FormatFrom = ko.observable();
    self.FormatTo = ko.observable();


    self.MapEntity = function (serviceEntity, RatesDiscountDto) {
        var compareKey = {
            'rowID': {
                key: function (data) {
                    return data.rowID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RatesDiscountDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}