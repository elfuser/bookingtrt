﻿CultureModel = function () {
    var self = this;
    self.ppCultureID = ko.observable();
    self.ppCultureName = ko.observable();
    self.ppDisplayName = ko.observable();


    self.MapEntity = function (serviceEntity, CultureDto) {
        var compareKey = {
            'ppCultureID': {
                key: function (data) {
                    return data.ppCultureID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CultureDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}