﻿
QryAddonModel = function () {
    var self = this;
    self.ppAddon = ko.observable();
    self.ppQty = ko.observable();

    self.MapEntity = function (serviceEntity, QryAddonDto) {
        var compareKey = {
            'ppAddon': {
                key: function (data) {
                    return data.ppAddon;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, QryAddonDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}