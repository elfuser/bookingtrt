﻿AddOnsModel = function () {
    var self = this;
    self.ppAddonID = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppDescription = ko.observable();
    self.ppAmountP = ko.observable();
    self.ppAmount1 = ko.observable();
    self.ppAmount2 = ko.observable();
    self.ppAmount3 = ko.observable();
    self.ppAmount4 = ko.observable();
    self.ppAmount5 = ko.observable();
    self.ppAmount6 = ko.observable();
    self.ppAmount7 = ko.observable();
    self.ppAmount8 = ko.observable();
    self.ppAmount9 = ko.observable();
    self.ppAmount10 = ko.observable();
    self.ppAmount = ko.observable();
    self.ppEnabled = ko.observable();
    self.ppTax = ko.observable();
    self.ppQty = ko.observable();
    self.ppRowOrder = ko.observable();
    self.ppFrom = ko.observable();
    self.ppTo = ko.observable();
    self.ppCreated = ko.observable();
    self.ppError = ko.observable();
    self.ppLongDescription = ko.observable();
    self.ppDuration = ko.observable();
    self.ppDeparture = ko.observable();
    self.ppAddonType = ko.observable();
    self.Alignmentclass = ko.observable();
    self.DescPrice = ko.observable();
    self.ImageUlr = ko.observable();
    self.ImageUrlON = ko.observable();
    self.CultureID = ko.observable();
    self.DepartureEnable = ko.observable();
    self.DurationEnable = ko.observable();

    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();

    self.Picture1 = ko.observable();
    self.Picture2 = ko.observable();

    

    self.MapEntity = function (serviceEntity, AddOnsDto) {
        var compareKey = {
            'ppAddonID': {
                key: function (data) {
                    return data.ppAddonID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, AddOnsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}   