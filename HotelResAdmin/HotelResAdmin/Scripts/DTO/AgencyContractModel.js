﻿AgencyContractModel = function () {
    var self = this;
    self.ppAgencyContractCode = ko.observable();
    self.ppAgencyCode = ko.observable();
    self.ppStart_date = ko.observable();
    self.ppEnd_date = ko.observable();
    self.ppNotes = ko.observable();
    self.ppLastDateModified = ko.observable();
    self.FormatStartDate = ko.observable();
    self.FormatEndDate = ko.observable();
    self.FormatLastDateModified = ko.observable();    

    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();

    self.ppContractFileName = ko.observable();
    self.ppContractFileType = ko.observable();

    self.MapEntity = function (serviceEntity, objDTO) {
        var compareKey = {
            'ppAgencyContractCode': {
                key: function (data) {
                    return data.ppAgencyContractCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, objDTO);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}