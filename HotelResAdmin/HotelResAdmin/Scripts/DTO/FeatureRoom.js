﻿
FeatureRoomModel = function () {
    var self = this;
    self.RoomTypeID = ko.observable();
    self.FeatureID = ko.observable();
  
    self.MapEntity = function (serviceEntity, FeatureRoomDto) {
        var compareKey = {
            'FeatureID': {
                key: function (data) {
                    return data.FeatureID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, FeatureRoomDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}