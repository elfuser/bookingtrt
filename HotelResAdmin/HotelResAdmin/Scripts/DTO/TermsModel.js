﻿TermsModel = function () {
    var self = this;
    self.CultureId = ko.observable();
    self.Terms = ko.observable();


    self.MapEntity = function (serviceEntity, TermsDto) {
        var compareKey = {
            'CultureId': {
                key: function (data) {
                    return data.CultureId;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, TermsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}