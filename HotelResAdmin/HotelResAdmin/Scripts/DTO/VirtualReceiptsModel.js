﻿VirtualReceiptsModel = function () {
    var self = this;
    self.ReceiptCode = ko.observable();
    self.CompanyCode = ko.observable();
    self.ClientName = ko.observable();
    self.Country = ko.observable();
    self.Phone = ko.observable();
    self.Email = ko.observable();
    self.Description = ko.observable();
    self.Amount = ko.observable();
    self.ReceiptPaid = ko.observable();
    self.Expiry = ko.observable();
    self.Created = ko.observable();
    self.FormatExpiry = ko.observable();
    self.FormatCreated = ko.observable();
    self.CompanyName = ko.observable();
    

    self.MapEntity = function (serviceEntity, VirtualReceiptsDto) {
        var compareKey = {
            'ReceiptCode': {
                key: function (data) {
                    return data.ReceiptCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, VirtualReceiptsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}   