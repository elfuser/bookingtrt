﻿SalesByMonthModel = function () {
    var self = this;
    self.dday = ko.observable();
    self.amount = ko.observable();

    self.MapEntity = function (serviceEntity, SalesByMonthDto) {
        var compareKey = {
            'dday': {
                key: function (data) {
                    return data.dday;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, SalesByMonthDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}