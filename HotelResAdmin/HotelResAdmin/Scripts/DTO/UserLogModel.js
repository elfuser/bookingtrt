﻿

UserLogModel = function () {
    var self = this;
    self.ppUserID = ko.observable();
    self.ppLogID = ko.observable();
    self.ppRefID = ko.observable();
    self.ppEvent = ko.observable();
    self.ppEventDate = ko.observable();
    self.ppError = ko.observable();
    self.FormatDate = ko.observable();

    self.MapEntity = function (serviceEntity, UserLogDto) {
        var compareKey = {
            'ppLogID': {
                key: function (data) {
                    return data.ppLogID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, UserLogDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}
