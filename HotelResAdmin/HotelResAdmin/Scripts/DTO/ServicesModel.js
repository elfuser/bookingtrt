﻿ServicesModel = function () {
    var self = this;
    self.ppServiceID = ko.observable();
    self.ppDescription = ko.observable();
    self.ppCode = ko.observable();
    self.ppListOrder = ko.observable();
    self.ppActive = ko.observable();
    self.ppDetails = ko.observable();
    self.ppVideoURL = ko.observable();
    self.ppDifficulty = ko.observable();
    self.ppCategoryID = ko.observable();
    self.ppControlInventory = ko.observable();
    self.ppConditions = ko.observable();
    self.ppBringInfo = ko.observable();

    self.MapEntity = function (serviceEntity, ServicesDto) {
        var compareKey = {
            'ppServiceID': {
                key: function (data) {
                    return data.ppServiceID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ServicesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}