﻿AddOnsTypeModel = function () {
    var self = this;
    self.code = ko.observable();
    self.description = ko.observable();

    self.MapEntity = function (serviceEntity, AddOnsTypeDto) {
        var compareKey = {
            'code': {
                key: function (data) {
                    return data.code;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, AddOnsTypeDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}