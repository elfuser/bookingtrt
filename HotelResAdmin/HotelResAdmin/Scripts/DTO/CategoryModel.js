﻿CategoryModel = function () {
    var self = this;
    self.ppCategoryID = ko.observable();
    self.ppDescription = ko.observable();

    self.MapEntity = function (serviceEntity, CategoryDto) {
        var compareKey = {
            'ppCategoryID': {
                key: function (data) {
                    return data.ppCategoryID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, CategoryDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}