﻿LoginModel = function () {
    var self = this;
   
    self.SuperUs = ko.observable();
    self.Active = ko.observable();
    self.UserID = ko.observable();
    self.UserName = ko.observable();
    self.ProfileID = ko.observable();

    self.MapEntity = function (serviceEntity, LoginDto) {
        var compareKey = {
            'UserID': {
                key: function (data) {
                    return data.UserID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, LoginDto);


    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}