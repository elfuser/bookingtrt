﻿RatesServiceModel = function () {
    var self = this;

    self.ppActive = ko.observable();
    self.ppDiscount = ko.observable();
    self.ppStudents = ko.observable();
    self.ppInfants = ko.observable();
    self.ppChildren = ko.observable();
    self.ppAdults = ko.observable();
    self.ppEndDate = ko.observable();
    self.ppStartDate = ko.observable();
    self.ppDiscountType = ko.observable();
    self.ppDescription = ko.observable();
    self.ppError = ko.observable();
    self.ppMaxStudents = ko.observable();
    self.ppMaxInfants = ko.observable();
    self.ppMaxChildren = ko.observable();
    self.ppMaxAdults = ko.observable();
    self.ppServiceID = ko.observable();
    self.ppRateID = ko.observable();

    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();


    self.MapEntity = function (serviceEntity, RatesServiceDto) {
        var compareKey = {
            'ppRateID': {
                key: function (data) {
                    return data.ppRateID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RatesServiceDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}