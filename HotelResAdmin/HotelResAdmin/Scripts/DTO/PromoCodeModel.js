﻿PromoCodeModel = function () {
    var self = this;

    self.ppActive= ko.observable();
    self.ppEndDate= ko.observable();
    self.ppStartDate= ko.observable();
    self.ppError= ko.observable();
    self.ppCompanyCode= ko.observable();
    self.ppType= ko.observable();
    self.ppDescription= ko.observable();
    self.ppPromoCode= ko.observable();
    self.ppPromoID = ko.observable();
    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();
    self.ppPromoType = ko.observable();

    self.ppAgencyCode = ko.observable();

    self.MapEntity = function (serviceEntity, PromoCodeDto) {
        var compareKey = {
            'ppPromoID': {
                key: function (data) {
                    return data.ppPromoID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, PromoCodeDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}