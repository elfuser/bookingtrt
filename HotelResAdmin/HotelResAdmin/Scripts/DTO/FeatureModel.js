﻿
FeatureModel = function () {
    var self = this;
    self.ppFeatureID = ko.observable();
    self.ppDescription = ko.observable();
    self.pChecked = ko.observable();
    self.CultureId = ko.observable();


    self.MapEntity = function (serviceEntity, FeatureDto) {
        var compareKey = {
            'ppFeatureID': {
                key: function (data) {
                    return data.ppFeatureID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, FeatureDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}