﻿ProfileModel = function () {
    var self = this;

    self.ppProfileID = ko.observable();
    self.ppProfileName = ko.observable();


    self.Dashboard = ko.observable(false);
    self.Company = ko.observable(false);
    self.Rates = ko.observable(false);
    self.Rooms = ko.observable(false);
    self.Addons = ko.observable(false);
    self.Services = ko.observable(false);

    self.Reservations = ko.observable(false);
    self.Avaibility = ko.observable(false);
    self.Comissions = ko.observable(false);

    self.ReportsReserv = ko.observable(false);
    self.ReportSales = ko.observable(false);
    self.ReportGuest = ko.observable(false);

    self.Profiles = ko.observable(false);
    self.EventLog = ko.observable(false);


    self.MapEntity = function (serviceEntity, ProfileDto) {
        var compareKey = {
            'ppProfileID': {
                key: function (data) {
                    return data.ppProfileID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ProfileDto);


    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}