﻿
AgencyTypesModel = function () {
    var self = this;
    self.ppAgencyTypeCode = ko.observable();
    self.ppTypeName = ko.observable();    

    self.MapEntity = function (serviceEntity, TypesDto) {
        var compareKey = {
            'ppAgencyTypeCode': {
                key: function (data) {
                    return data.ppAgencyTypeCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, TypesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}