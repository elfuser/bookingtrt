﻿RatesHeaderModel = function () {
    var self = this;

    self.ppRateId = ko.observable();
    self.ppFreeNights = ko.observable();
    self.ppRowOrder = ko.observable();
    self.ppRateCode = ko.observable();
    self.ppConditions = ko.observable();
    self.ppDescription = ko.observable();
    self.ppType = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppStartDate = ko.observable();
    self.ppEndDate = ko.observable();
    self.ppMinLos = ko.observable();
    self.ppMaxLos = ko.observable();
    self.ppEnabled = ko.observable();
    self.ppWeekend = ko.observable();
    self.ppMaster = ko.observable();
    self.ppRateDetail = ko.observable();
    self.ppMinValue = ko.observable();
    self.ppDiscount = ko.observable();

    self.ppStrStartDate = ko.observable();
    self.ppStrEndDate = ko.observable();



    self.UnFormatFrom = ko.observable();
    self.UnFormatTo = ko.observable();


    self.MapEntity = function (serviceEntity, RatesHeaderDto) {
        var compareKey = {
            'ppRateId': {
                key: function (data) {
                    return data.ppRateId;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RatesHeaderDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}