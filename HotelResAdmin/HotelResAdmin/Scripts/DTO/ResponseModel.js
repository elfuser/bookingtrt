﻿ResponseModel = function () {
    var self = this;
    self.Status = ko.observable();
    self.Message = ko.observable();


    self.MapEntity = function (serviceEntity, ResponseDto) {
        var compareKey = {
            'Status': {
                key: function (data) {
                    return data.Status;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, ResponseDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}