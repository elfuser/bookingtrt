﻿PromoCodeRateModel = function () {
    var self = this;

    self.ppType = ko.observable();
    self.ppDescription = ko.observable();
    self.ppAmount = ko.observable();
    self.ppRateID = ko.observable();
    self.ppPromoID = ko.observable();
    self.ppPromoType = ko.observable();
    self.ppServiceID = ko.observable();
    self.ppServiceDesc = ko.observable();

    self.MapEntity = function (serviceEntity, PromoCodeRateDto) {
        var compareKey = {
            'RateId': {
                key: function (data) {
                    return data.RateId;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, PromoCodeRateDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}