﻿
CountriesModel = function () {
    var self = this;
    self.ppCountryIso3 = ko.observable();
    self.ppName = ko.observable();


    self.MapEntity = function (serviceEntity, CountriesDto) {
        var compareKey = {
            'ppCountryIso3': {
                key: function (data) {
                    return data.ppCountryIso3;
                }
            }
        };


        ko.mapping.fromJS(serviceEntity, compareKey, CountriesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}