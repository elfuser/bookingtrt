﻿BankLogInfoModel = function () {
    var self = this;

    self.ppResponse = ko.observable();
    self.ppTransactionID = ko.observable();
    self.ppAmount = ko.observable();

    self.MapEntity = function (serviceEntity, BankLogInfoDto) {
        var compareKey = {
            'ppTransactionID': {
                key: function (data) {
                    return data.ppTransactionID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, BankLogInfoDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}