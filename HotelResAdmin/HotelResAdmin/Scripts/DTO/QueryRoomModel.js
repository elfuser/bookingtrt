﻿
QueryRoomModel = function () {
    var self = this;

    self.ppCode= ko.observable();
    self.ppRoom= ko.observable();
    self.ppAdults= ko.observable();
    self.ppChildren= ko.observable();
    self.ppAmount= ko.observable();
    self.ppChildrenFree= ko.observable();
    self.ppDate = ko.observable();

    self.MapEntity = function (serviceEntity, QueryRoomDto) {
        var compareKey = {
            'ppCode': {
                key: function (data) {
                    return data.ppCode;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, QueryRoomDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}