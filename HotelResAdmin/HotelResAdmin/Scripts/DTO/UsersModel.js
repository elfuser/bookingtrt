﻿
UsersModel = function () {
    var self = this;

    self.ppProfile = ko.observable();
    self.ppCompany = ko.observable();
    self.ppUserID = ko.observable();
    self.ppCompanyCode = ko.observable();
    self.ppUserName = ko.observable();
    self.ppPassword = ko.observable();
    self.ppPasswordConfirmation = ko.observable();
    self.ppFirstName = ko.observable();
    self.ppLastName = ko.observable();
    self.ppProfileID = ko.observable();
    self.ppActive = ko.observable();
    self.ppLastLogin = ko.observable();
    self.ppError = ko.observable();
    self.SecondSurname = ko.observable();
    self.LastLoginFormat = ko.observable();
    self.isAgencyUser = ko.observable();
    self.AgencyCode = ko.observable();
    self.ppisDomainUser = ko.observable();

    self.MapEntity = function (serviceEntity, UserDto) {
        var compareKey = {
            'ppUserID': {
                key: function (data) {
                    return data.ppUserID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, UserDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}