﻿
RatesModel = function () {
    var self = this;
    self.ppRateId = ko.observable();
    self.ppDescription = ko.observable();


    self.MapEntity = function (serviceEntity, RatesDto) {
        var compareKey = {
            'ppRateId': {
                key: function (data) {
                    return data.ppRateId;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RatesDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}