﻿SalesByRoomModel = function () {
    var self = this;
    self.room = ko.observable();
    self.qty = ko.observable();

    self.MapEntity = function (serviceEntity, SalesByRoomDto) {
        var compareKey = {
            'room': {
                key: function (data) {
                    return data.room;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, SalesByRoomDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }
}