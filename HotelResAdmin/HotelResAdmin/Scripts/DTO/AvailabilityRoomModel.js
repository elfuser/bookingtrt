﻿
AvailabilityRoomModel = function () {
    var self = this;
    self.ppRowID = ko.observable();    
    self.ppCompanyCode = ko.observable();
    self.ppRoomTypeCode = ko.observable();
    self.ppDate = ko.observable();
    self.ppRoomsQty = ko.observable();
    self.ppRoomsOccup = ko.observable();
    self.ppRoomsBlocked = ko.observable();
    self.ppRoomsAvail = ko.observable();
    self.ppOnlineBlock = ko.observable();    
    self.ppRoomTypeName = ko.observable();    

    self.MapEntity = function (serviceEntity, RoomsDto) {
        var compareKey = {
            'ppRoomTypeID': {
                key: function (data) {
                    return data.ppRoomTypeID;
                }
            }
        };

        ko.mapping.fromJS(serviceEntity, compareKey, RoomsDto);
    };

    self.UnMapEntity = function (observableDto) {
        return ko.mapping.toJS(observableDto);
    }

}