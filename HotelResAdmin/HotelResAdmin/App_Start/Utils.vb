﻿
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Security.Cryptography
Imports BRules

Public Class Utils

    Public Shared Culture As String = "en-US"

    Public Shared Property CompanyCode As String
        Get
            Return HttpContext.Current.Session("CompanyCode") & ""
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session("CompanyCode") = value
        End Set
    End Property

    'Public Shared Property UserID As String
    '    Get
    '        If HttpContext.Current.Session("Session_UserID") & "" = "" Then
    '            Return "350"
    '        End If
    '        Return HttpContext.Current.Session("Session_UserID") & ""
    '    End Get
    '    Set(ByVal value As String)
    '        HttpContext.Current.Session("Session_UserID") = value
    '    End Set
    'End Property

    Public Shared Property UserObject As ResponseUser
        Get
            Return HttpContext.Current.Session("Session_User_Admin_Console")
        End Get
        Set(ByVal value As ResponseUser)
            HttpContext.Current.Session("Session_User_Admin_Console") = value
        End Set
    End Property



    Public Shared Function GetMD5(Str As String) As String
        Dim md5 As MD5 = MD5CryptoServiceProvider.Create
        Dim encoding As New ASCIIEncoding()
        Dim stream As Byte() = Nothing
        Dim sb As New StringBuilder()
        stream = md5.ComputeHash(encoding.GetBytes(Str))
        For i As Integer = 0 To stream.Length - 1
            sb.AppendFormat("{0:x2}", stream(i))
        Next
        Return sb.ToString()
    End Function

    ''' <summary>
    '''Encrypts a string
    ''' </summary>
    ''' <param name="strValues">String that wants to code</param>
    ''' <returns>String encripted</returns>
    ''' <remarks></remarks>
    Public Shared Function encrypt(ByVal strValues As String) As String
        Dim objEncrypt As New CryptoUtil()
        Return objEncrypt.Encrypt(strValues)
    End Function

    ''' <summary>
    '''Decrypts a string
    ''' </summary>
    ''' <param name="strValues">string that wants to be decoded</param>
    ''' <returns>String decrypted</returns>
    ''' <remarks></remarks>
    Public Shared Function decrypt(ByVal strValues As String) As String
        If strValues Is Nothing Then
            Return Nothing
        Else
            Dim objEncrypt As New CryptoUtil()
            Return objEncrypt.Decrypt(strValues)
        End If
    End Function

    Public Shared Function GetJsonFromObject(ByVal obj As Object) As String
        Try
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Serialize(obj)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function cryptURL(ByVal url As String, ByVal params As String) As String
        If params <> "" Then
            params = encrypt(params)
            Return url & "auth=" & params
        Else
            Return url
        End If
    End Function




End Class
