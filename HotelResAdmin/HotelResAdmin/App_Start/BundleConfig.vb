﻿Imports System.Web
Imports System.Web.Optimization

Public Class BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    Public Shared Sub RegisterBundles(ByVal bundles As BundleCollection)
        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                   "~/Scripts/jquery-1.11.1.min.js",
                   "~/Scripts/jquery-ui.min.js",
                   "~/Scripts/bootstrap.min.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/bootstrap-select.min.js",
                   "~/Scripts/bootstrap-switch.js"))

        bundles.Add(New ScriptBundle("~/bundles/master").Include(
                   "~/Scripts/DTO/CompanyModel.js",
                   "~/Scripts/DTO/ProfileModel.js",
                   "~/Scripts/App/Master.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryui").Include(
                      "~/Scripts/jquery.plugin.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.unobtrusive*",
                    "~/Scripts/jquery.validate*"))

        bundles.Add(New StyleBundle("~/Styles/css/fontawesomestyles").Include(
                    "~/Styles/css/font-awesome.min.css"))

        bundles.Add(New StyleBundle("~/Styles/CustomStyles").Include(
            "~/Styles/masterPage.css",
               "~/Styles/bootstrap.min.css",
             "~/Styles/jquery-ui.theme.min.css",
            "~/Styles/bootstrap-switch.css",
            "~/Styles/bootstrap-theme.min.css",
                "~/Styles/jquery-ui.css",
            "~/Styles/uploadfile.css",
                 "~/Styles/jquery.jqplot.min.css",
            "~/Content/TRTStyle.css",
                "~/Content/bootstrap-select.min.css",
             "~/Content/bootstrapValidator.min.css"))

        '-- Booking redesign styles
        bundles.Add(New StyleBundle("~/Styles/BookingRedesign").Include("~/Styles/BookingRedesign.css"))

        'bundles.Add(New StyleBundle("~/bundles/StylesFolder").IncludeDirectory("~/Styles", "*.css"))


        '-- CompaniesCss styles
        bundles.Add(New StyleBundle("~/Content/CompaniesCss").Include("~/Content/bootstrap-select.min.css"))

        bundles.Add(New ScriptBundle("~/bundles/Util").Include(
                   "~/Scripts/Utility.js"))

        bundles.Add(New ScriptBundle("~/bundles/KnockOut").Include(
                   "~/Scripts/knockout-3.4.0.js",
                   "~/Scripts/knockout_mapping_latest.js",
                   "~/Scripts/linq.min.js"))


        '--login js
        bundles.Add(New ScriptBundle("~/bundles/LoginScripts").Include(
                   "~/Scripts/App/Loginvm.js",
                   "~/Scripts/DTO/LoginModel.js",
                   "~/Scripts/DTO/ResponseModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.cookie.js",
                   "~/Scripts/jquery.blockui.js"))


        '--Users js
        bundles.Add(New ScriptBundle("~/bundles/Users").Include(
                   "~/Scripts/App/Users.js",
                   "~/Scripts/DTO/ProfileModel.js",
                   "~/Scripts/DTO/UsersModel.js",
                   "~/Scripts/DTO/UserLogModel.js",
                      "~/Scripts/DTO/AgenciesHeaderModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js"))


        '-- addons js
        bundles.Add(New ScriptBundle("~/bundles/Addons").Include(
                   "~/Scripts/App/AddOns.js",
                   "~/Scripts/DTO/AddOnsModel.js",
                    "~/Scripts/DTO/AddOnsTypeModel.js",
                   "~/Scripts/DTO/CultureModel.js",
                   "~/Scripts/Utility.js",
                     "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/jquery.blockui.js"))

        '--promo codes js
        bundles.Add(New ScriptBundle("~/bundles/PromoCodes").Include(
                   "~/Scripts/App/PromoCode.js",
                   "~/Scripts/DTO/PromoCodeModel.js",
                    "~/Scripts/DTO/PromoCodeRateModel.js",
                    "~/Scripts/DTO/PromoCodeExceptionModel.js",
                   "~/Scripts/DTO/RatesModel.js",
                   "~/Scripts/DTO/RateServiceModel.js",
                   "~/Scripts/DTO/ServicesModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js"))



        '--rooms js
        bundles.Add(New ScriptBundle("~/bundles/Rooms").Include(
                   "~/Scripts/App/Rooms.js",
                   "~/Scripts/DTO/RoomsModel.js",
                   "~/Scripts/Utility.js",
                  "~/Scripts/DTO/CultureModel.js",
                  "~/Scripts/DTO/FeatureModel.js",
                  "~/Scripts/DTO/FeatureRoom.js",
                  "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/jquery.blockui.js"))


        '--Features js
        bundles.Add(New ScriptBundle("~/bundles/Features").Include(
                   "~/Scripts/App/FeaturesLocale.js",
                   "~/Scripts/DTO/CultureModel.js",
                   "~/Scripts/Utility.js",
                    "~/Scripts/DTO/FeatureModel.js",
                   "~/Scripts/jquery.blockui.js"))


        '--Companies js
        bundles.Add(New ScriptBundle("~/bundles/Companies").Include(
                   "~/Scripts/App/Companies.js",
                   "~/Scripts/DTO/CompanyModel.js",
                   "~/Scripts/DTO/CombinationsModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/jquery.blockui.js"))


        '--Params js
        bundles.Add(New ScriptBundle("~/bundles/Parameters").Include(
                   "~/Scripts/App/Parameters.js",
                   "~/Scripts/DTO/CompanyModel.js",
                  "~/Scripts/DTO/CultureModel.js",
                  "~/Scripts/DTO/TermsModel.js",
                  "~/Scripts/Utility.js",
                  "~/Scripts/jquery.blockui.js"))

        '--reservation query js
        bundles.Add(New ScriptBundle("~/bundles/ReservationsQuery").Include(
                   "~/Scripts/App/ReservationsQuery.js",
                   "~/Scripts/DTO/ReservationsQryModel.js",
                    "~/Scripts/DTO/BankLogInfoModel.js",
                    "~/Scripts/DTO/QryAddonModel.js",
                    "~/Scripts/DTO/QueryRoomModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js"))


        '--rates js
        bundles.Add(New ScriptBundle("~/bundles/Rates").Include(
                   "~/Scripts/App/Rates.js",
                   "~/Scripts/Utility.js",
                             "~/Scripts/DTO/RoomsModel.js",
                    "~/Scripts/DTO/RatesHeader.js",
                    "~/Scripts/DTO/RatesDiscount.js",
                   "~/Scripts/DTO/RatesDetailModel.js",
                   "~/Scripts/jquery.blockui.js"))




        '--Services js
        bundles.Add(New ScriptBundle("~/bundles/Services").Include(
                   "~/Scripts/App/Services.js",
                   "~/Scripts/DTO/ServicesModel.js",
                   "~/Scripts/DTO/CultureModel.js",
                   "~/Scripts/DTO/CategoryModel.js",
                   "~/Scripts/DTO/RateServiceModel.js",
                   "~/Scripts/DTO/PromoCodeModel.js",
                   "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js"))


        '-- VirtualReceipts js
        bundles.Add(New ScriptBundle("~/bundles/VirtualReceipts").Include(
                   "~/Scripts/App/VirtualReceipt.js",
                   "~/Scripts/DTO/VirtualReceiptsModel.js",
                   "~/Scripts/DTO/CountriesModel.js",
                   "~/Scripts/DTO/CultureModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/moment.min.js",
                   "~/Scripts/bootstrap-datetimepicker.min.js",
                   "~/Scripts/bootstrap-filestyle.min.js"
                   ))

        '--Agencies js
        bundles.Add(New ScriptBundle("~/bundles/Agencies").Include(
                   "~/Scripts/App/Agencies.js",
                   "~/Scripts/Utility.js",
                    "~/Scripts/DTO/AgenciesHeaderModel.js",
                   "~/Scripts/DTO/AgencyContractModel.js",
                   "~/Scripts/DTO/AgencyTypesModel.js",
                   "~/Scripts/DTO/CountriesModel.js",
                   "~/Scripts/bootstrap-filestyle.min.js",
                   "~/Scripts/DTO/PromoCodeModel.js",
                   "~/Scripts/jquery.blockui.js"))

        '--comissions query js
        bundles.Add(New ScriptBundle("~/bundles/ComissionsQuery").Include(
                   "~/Scripts/App/CommisionsQuery.js",
                   "~/Scripts/DTO/ReservationsQryModel.js",
                    "~/Scripts/DTO/BankLogInfoModel.js",
                    "~/Scripts/DTO/QryAddonModel.js",
                    "~/Scripts/DTO/QueryRoomModel.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js"))

        '--availability query js
        bundles.Add(New ScriptBundle("~/bundles/AvailabilityQuery").Include(
                   "~/Scripts/DTO/ReservationsQryModel.js",
                   "~/Scripts/DTO/QueryRoomModel.js",
                   "~/Scripts/DTO/RoomsModel.js",
                   "~/Scripts/DTO/AvailabilityRoomModel.js",
                   "~/Scripts/linq.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/App/AvailabilityQuery.js"))
        '--closing general query js
        bundles.Add(New ScriptBundle("~/bundles/ClosingGQuery").Include(
                   "~/Scripts/DTO/ReservationsQryModel.js",
                   "~/Scripts/DTO/QueryRoomModel.js",
                   "~/Scripts/DTO/RoomsModel.js",
                   "~/Scripts/DTO/AvailabilityRoomModel.js",
                   "~/Scripts/linq.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/App/ClosingGQuery.js"))

        '--closing by sales query js
        bundles.Add(New ScriptBundle("~/bundles/ClosingSalesQuery").Include(
                   "~/Scripts/DTO/ReservationsQryModel.js",
                   "~/Scripts/DTO/QueryRoomModel.js",
                   "~/Scripts/DTO/RoomsModel.js",
                   "~/Scripts/DTO/AvailabilityRoomModel.js",
                   "~/Scripts/linq.min.js",
                   "~/Scripts/Utility.js",
                   "~/Scripts/jquery.blockui.js",
                   "~/Scripts/App/ClosingSalesQuery.js"))

        '-- datepicker js
        bundles.Add(New ScriptBundle("~/bundles/datepicker").Include(
                   "~/Scripts/jquery.plugin.min.js",
                   "~/Scripts/jquery.datepick.min.js"))


        '--dashboard js
        bundles.Add(New ScriptBundle("~/bundles/DashboardScripts").Include(
                   "~/Scripts/App/DashBoard.js",
                   "~/Scripts/DTO/SalesByMonthDTO.js",
                      "~/Scripts/DTO/SalesRoomModel.js",
                   "~/Scripts/jquery.jqplot.js",
                        "~/Scripts/jqplot.categoryAxisRenderer.js",
                                      "~/Scripts/jqplot.enhancedPieLegendRenderer.js",
                   "~/Scripts/jqplot.barRenderer.min.js",
                   "~/Scripts/jqplot.pointLabels.js",
                   "~/Scripts/jqplot.pieRenderer.js"))

        '-- VirtualReceipts styles
        bundles.Add(New StyleBundle("~/Content/VirtualReceiptsCss").Include("~/Content/bootstrap-datetimepicker.min.css"))

        '-- Agencies styles
        bundles.Add(New StyleBundle("~/Content/AgenciesCss").Include("~/Content/Agencies.css"))

        '-- rooms availability styles
        bundles.Add(New StyleBundle("~/Content/QueryRoomsAvailabilityCss").Include("~/Content/jquery.datepick.css",
                                                                              "~/Content/QueryRoomsAvailability.css"))

        'bundles.Add(New StyleBundle("~/bundles/StylesFolder").IncludeDirectory("~/Styles", "*.css"))

        bundles.Add(New StyleBundle("~/Scripts/tinymce/skins/styles").IncludeDirectory("~/Scripts/tinymce/skins/lightgray", "*.css"))

        ' Use the development version of Modernizr to develop with and learn from. Then, when you're
        ' ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"))

        'bundles.Add(New StyleBundle("~/Content/css").Include("~/Content/site.css"))

        bundles.Add(New StyleBundle("~/bundles/Login").Include(
                        "~/Styles/Loginpage.css",
               "~/Styles/bootstrap.min.css",
            "~/css/font-awesome.min.css",
             "~/Styles/jquery-ui.theme.min.css",
            "~/Styles/bootstrap-switch.css",
            "~/Styles/bootstrap-theme.min.css",
                "~/Styles/jquery-ui.css",
            "~/Content/TRTStyle.css",
             "~/Content/bootstrapValidator.min.css"))

        bundles.Add(New StyleBundle("~/Content/themes/base/css").Include(
                    "~/Content/themes/base/jquery.ui.core.css",
                    "~/Content/themes/base/jquery.ui.resizable.css",
                    "~/Content/themes/base/jquery.ui.selectable.css",
                    "~/Content/themes/base/jquery.ui.accordion.css",
                    "~/Content/themes/base/jquery.ui.autocomplete.css",
                    "~/Content/themes/base/jquery.ui.button.css",
                    "~/Content/themes/base/jquery.ui.dialog.css",
                    "~/Content/themes/base/jquery.ui.slider.css",
                    "~/Content/themes/base/jquery.ui.tabs.css",
                    "~/Content/themes/base/jquery.ui.datepicker.css",
                    "~/Content/themes/base/jquery.ui.progressbar.css",
                    "~/Content/themes/base/jquery.ui.theme.css"))
    End Sub
End Class