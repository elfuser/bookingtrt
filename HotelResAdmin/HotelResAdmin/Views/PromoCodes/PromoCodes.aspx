﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="PromoCodes" ContentPlaceHolderID="TitleContent" runat="server">
Promotional Codes
</asp:Content>


<asp:Content ID="PromoCodesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/promocodes", "")%>" type="hidden"/>
    <input id="hdf_promo_type"  value="<%: ViewBag.promotype%>" type="hidden"/>
     
     <div id="DeletePromoCode">

     </div>

    
<div id="AddOnSection" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none;">
    <!---------Toolbar------>
    <div class="row   commandbuttondiv" id="cmdButtons"  data-bind="css: { 'toolbarhide': isEditing() }">


            <div class="toolBox">

                 <div data-bind="click:RefreshClick">
                   <%-- <img src="../../images/refreshIcon.png" />--%>  
                    <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                    <span>Refresh</span>
                </div>
                <div data-bind="click:AddClick">
                    <%--<img src="../../images/NewDocument.png" />--%>
                    <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                    <span>Add</span>
                </div>
                <div data-bind="click:DeleteClick">
                    <%--<img src="../../images/deleteIcon.png" />--%>
                    <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                    <span>Delete </span>
                </div>
            </div>

        <div class="pagerPanel">
            <div>
                <span>Page size</span>
                <select data-bind="options:pageSizeOptions,value:selectedPageSize,event: { change:GetSelectedPageSize}"></select>
            </div>

            <div>
                <span>Page no.</span>
                <input type="text" name="page-number" value="" data-bind="value:pageNumber,valueUpdate:'input',PagingwithEnterKey:GetSelectedPage" />
                <button data-bind="click:GetSelectedPage">GO</button>
            </div>

            <div>
                <span class="fa fa-angle-left" data-bind="click:BackwardpageClick"></span>
                <span data-bind="text:pageNoLabel"></span>
                <span class="fa fa-angle-right" data-bind="click:ForwardPageClick"></span>
            </div>
        </div>



    </div>
    <!------>
    <!----Error Msg Div--->

      

    <div data-bind="visible:isEditing" style="display:none;">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


            <!----Success Msg Div--->
        <div class="row  col-md-12 rowNoMargin succesMsgDiv"  data-bind="css:{'entrypanelSuccessDiv':isEditing()}">                   
            <div class="col-md-7 rowNoMargin  alert alert-success" id="PromoCodeMsg" data-bind="css:{'toolbarhide':isEditing(),'divhiddenVisiblity':!isSucess()}">
            </div>
          
        </div>
        <!------->

    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important"   data-bind="visible:isRefresh">
        <table>

            <thead>
                <tr>
                    <th  style="width:5%" class="editButtonHeader">


                    </th>


                


                    <th   style="width:8%" data-bind="click: function () { SetOrderBy('a.promo_code') }">
                        <span>Code</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.promo_code' == orderBy(), 'divhidden': 'a.promo_code' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th> 

                    <th data-bind="click: function () { SetOrderBy('a.description') }">
                        <span>Description</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.description' == orderBy(), 'divhidden': 'a.description' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <th  style="width:5%" data-bind="click: function () { SetOrderBy('a.start_date') }">
                        <span>Start Date</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.start_date' == orderBy(), 'divhidden': 'a.start_date' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>


                    <th style="width:5%" data-bind="click: function () { SetOrderBy('a.end_date') }">
                        <span> End Date</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.end_date' == orderBy(), 'divhidden': 'a.end_date' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                     <th style="width:2%" data-bind="click: function () { SetOrderBy('a.active') }">
                        <span> Active</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.active' == orderBy(), 'divhidden': 'a.active' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>


                     <th  style="width:4%" class="editButtonHeader">


                    </th>


                </tr>
            </thead>

            <tbody data-bind="foreach: $root.PromoCodeList">
               <tr data-bind="click: function (data, event) { $parent.SelectPromoCode(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppPromoID() == $parent.SelectPromoCode().ppPromoID() }">
                   
                 <td class="actioncolumn">
                        <%--<img class="editButton" src="../../Images/Edit.png" >--%>

                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditPromoCode(data) }" />

    

                    </td>

                    <td data-bind="text: ppPromoCode"></td>
                   
                    <td data-bind="text: ppDescription"></td>

                    <td data-bind="text: UnFormatFrom"></td>

                    <td data-bind="text: UnFormatTo"></td>

                    <td >

                           <input type="checkbox" class="mediumCheckBox" data-bind="checked: ppActive()" />
                    </td>


                   <td class="RatesButtonColumn">
                       <button class="btn btn-primary"   data-bind="click: function (data, event) { $parent.RatesClick(data) }">
                        Rates
                            </button>
                   </td>

                </tr>

            </tbody>

        </table>




    </div>


    <!----------->

    <!--------Entry Panel---------->
     
    <!--------Entry Panel---------->
        <!-- #include file="PromoCodeEntry.html" -->
    <!-------->

    <!--------Rates Panel---------->
        <!-- #include file="PromoCodeRate.html" -->
    <!-------->
    <!-------->
</div>
<!---------------->

            

</asp:Content>


<asp:Content ID="AddOnsScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/PromoCodes") %>              
</asp:Content>

