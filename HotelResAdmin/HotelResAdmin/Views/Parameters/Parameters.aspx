﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Parameters" ContentPlaceHolderID="TitleContent" runat="server">
Parameters             
</asp:Content>

<asp:Content ID="ParametersContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/parameters", "")%>" type="hidden"/>

       <div style="width: 100%; background-color:white; display:none; " class="ParameterTabs" >
        <ul class="noMargin tabAppMenu">
            <li class="carrierLink activePageMenu" id="GeneralTab"  data-bind="click: GeneralTabClick"><a>Parameters</a>  </li>

            <li>|</li>
            <li  class="carrierLink " id="TermsTab"   data-bind="click: TermsTabClick"><a> Terms and Conditions</a>  </li>
        </ul>

    </div>

    <div id="ParamsDiv" style="display:none">


       <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
        <div class=" row clientErrorList alert alert-danger col-md-12" id="EntryDetailErrorBox" data-bind="click: function () { $root.errorExpanderClick('EntryDetailErrorBox') }">
            <div class=" errUpDownArrowDiv">
                <span class="fa fa-caret-down"></span>
                <span class="fa fa-caret-up"></span>
            </div>
            <div class="errorLabelfull">

                <ul data-bind="foreach: $root.ErrorList">
                    <li data-bind="text: ErrorText"></li>
                </ul>

            </div>
        </div>
    </div>


                            <!----Success Msg Div--->
        <div class="row  col-md-10 rowNoMargin succesMsgDiv"  data-bind="css: { 'entrypanelSuccessDiv': isEditing() }">                   
                    <div class="col-md-7 rowNoMargin  alert alert-success" id="ParamMsg" data-bind="css: { 'toolbarhide': isEditing(), 'divhiddenVisiblity': !isSucess() }">
                    </div>
          
                </div>
                <!------->

      <div class="col-md-12 col-xs-12 col-sm-12 noMarginright EntryPanelcss" data-bind="with: SelectedHotel()">

        
        
    <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Confirmation Prefix
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control " readonly id="ppConfirmPrefix" name="ppConfirmPrefix"   maxlength="10" data-bind="value: ppConfirmPrefix" />

        </div>

    </div>


       <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Reservation Consec.
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control " readonly id="ppReservationNr" name="ppReservationNr"   maxlength="10" data-bind="value: ppReservationNr" />

        </div>

    </div>


      <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Max. Adults
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control numericTextbox"  placeholder="Max."   onkeypress="return OnlyPositiveNumeric(event)" tabindex="2" maxlength="10" data-bind="value: ppMaxAdults" id="ppMaxAdults" name="ppMaxAdults" />
        </div>


    </div>

    <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
               Max. Children
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control numericTextbox"  placeholder="Max."  onkeypress="return OnlyPositiveNumeric(event)" tabindex="2" maxlength="10" data-bind="value: ppMaxChildren" id="ppMaxChildren" name="ppMaxChildren" />
        </div>


    </div>


        
      <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Adult Years
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control "  placeholder="Max."  tabindex="2" maxlength="20" data-bind="value: ppMsgAdults" id="ppMsgAdults" name="ppMsgAdults" />
        </div>


    </div>

    <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Children Years
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control "  placeholder="Max." tabindex="2" maxlength="20" data-bind="value: ppMsgChildren" id="ppMsgChildren" name="ppMsgChildren" />
        </div>


    </div>

    <div class="row rowNoMargin entryPanel">

                    <div class="col-md-2  smallMargintop">
                <label>
                     Enable children free
                </label>


            </div>

            <div class="col-md-4 noMargin smallMargintop">
                <input tabindex="1" type="checkbox" id="ppChildrenFree" name="ppChildrenFree"  class="form-control  mediumCheckBox" data-bind="checked: ppChildrenFree" />
            </div>
    </div>

           <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Message
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control "  tabindex="2" maxlength="20" data-bind="value: ppMsgChildrenFree" id="ppMsgChildrenFree" name="ppMsgChildrenFree" />
        </div>


    </div>


            <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Children Note
            </label>
        </div>
        <div class="col-md-8 noMargin smallMargintop">

            <textarea cols="20" class="form-control " id="ppRoom_Detail" name="ppChildrenNote" tabindex="3" maxlength="100" rows="2" data-bind="value: ppChildrenNote"></textarea>
        </div>

    </div>


              <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Tax %
            </label>
        </div>
        <div class="col-md-2 noMargin smallMargintop">
            <input type="text" class="form-control "  placeholder="Max."  tabindex="2" maxlength="10"   onkeypress="return OnlyPositiveNumeric(event)" data-bind="value: ppTaxPerc" id="ppTaxPerc" name="ppTaxPerc" />
        </div>


    </div>


                   <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                English Site Url
            </label>
        </div>
        <div class="col-md-8 noMargin smallMargintop">
            <input type="text" class="form-control "  readonly tabindex="2" maxlength="100" data-bind="value: EnglishUrl" id="EnglishUrl" name="EnglishUrl" />
        </div>


    </div>


                           <div class="row rowNoMargin entryPanel">

        <div class="col-sm-2 noMargin smallMargintop">
            <label>
                Spanish Site Url
            </label>
        </div>
        <div class="col-md-8 noMargin smallMargintop">
            <input type="text" class="form-control "  readonly tabindex="2" maxlength="100" data-bind="value: SpanishUrl" id="SpanishUrl" name="SpanishUrl" />
        </div>


    </div>


        
        <div class="col-md-12  noMargin mediumMargintop">
            <div class="btn btn-primary " role="button"  type="submit" data-bind="click: $parent.SaveClick"  >
                Save Changes
            </div>

        



        </div>
   </div>


    </div>

    
    <div id="TermsDiv" style="display:none">

        <div class="col-md-12 col-xs-12 col-sm-12 noMarginright EntryPanelcss">



            <div class="row rowNoMargin entryPanel">

                        <div class="col-sm-1 noMargin smallMargintop">
                <label>
                    Language
                </label>
            </div>
            <div class="col-md-4 noMargin smallMargintop">
                <select class="form-control " id="CultureID"  name="CultureID"  tabindex="0" data-bind="options: CultureList, optionsText: 'ppDisplayName', value: selectedCulture , event:{ change: CultureTypeChanged}""></select>

            </div>

         </div>

            
                   <div class="row rowNoMargin entryPanel">


        <div class="col-md-12 noMargin smallMargintop">
                       <textarea cols="20" class="form-control " id="txtterms" name="txtterms" tabindex="3"  rows="16"
                            data-bind="value: terms" >

                       </textarea>


   
        </div>


                       
        
        <div class="col-md-12  noMargin mediumMargintop">
            <div class="btn btn-primary " role="button"  type="submit" data-bind="click: SaveTermsClick"  >
                Save Terms
            </div>

        



        </div>

    </div>

        </div>
        
    </div>


        
    <div id="dialog" title="Alert" style="display:none;">  
     <div id="DialogAlertMessage" class="DialogAlertMessage"></div>  
    </div> 

</asp:Content>

<asp:Content ID="FeaturesScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/Parameters") %>   
       
    <script src="<%=Url.Content("~/Scripts/tinymce/jquery.tinymce.min.js")%>"></script>
    <script src="<%=Url.Content("~/Scripts/tinymce/tinymce.min.js")%>"></script>

    <%--<%: Scripts.Render("~/bundles/Scripts/tinymce") %> --%>      
    <%: Styles.Render("~/Scripts/tinymce/skins/styles") %>       
</asp:Content>
