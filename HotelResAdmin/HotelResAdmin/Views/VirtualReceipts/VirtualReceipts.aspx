﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="VirtualReceipts" ContentPlaceHolderID="TitleContent" runat="server">
Virtual Receipts

    
</asp:Content>


<asp:Content ID="VirtualReceiptsContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/virtualreceipts", "")%>" type="hidden"/>

    <!---------------------------->
 
     <div id="DeleteVirtualReceipt">

     </div>
   

<div id="VirtualReceiptSection" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none;">
    <!---------Toolbar------>
    <div class="row   commandbuttondiv"  data-bind="css: { 'toolbarhide': isEditing() }">


            <div class="toolBox">

                <div data-bind="click:RefreshClick">
                   <%-- <img src="../../images/refreshIcon.png" />--%>  
                    <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                    <span>Refresh</span>
                </div>
                <div data-bind="click:AddClick">
                    <%--<img src="../../images/NewDocument.png" />--%>
                    <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                    <span>Add</span>
                </div>
                <div data-bind="click:DeleteClick">
                    <%--<img src="../../images/deleteIcon.png" />--%>
                    <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                    <span>Delete </span>
                </div>
            </div>

        <div class="pagerPanel">
            <div>
                <span>Page size</span>
                <select data-bind="options:pageSizeOptions,value:selectedPageSize,event: { change:GetSelectedPageSize}"></select>
            </div>

            <div>
                <span>Page no.</span>
                <input type="text" name="page-number" value="" data-bind="value:pageNumber,valueUpdate:'input',PagingwithEnterKey:GetSelectedPage" />
                <button data-bind="click:GetSelectedPage">GO</button>
            </div>

            <div>
                <span class="fa fa-angle-left" data-bind="click:BackwardpageClick"></span>
                <span data-bind="text:pageNoLabel"></span>
                <span class="fa fa-angle-right" data-bind="click:ForwardPageClick"></span>
            </div>
        </div>



    </div>
    <!------>
    <!----Error Msg Div--->

      

    <div data-bind="visible:isEditing" style="display:none;">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


            <!----Success Msg Div--->
        <div class="row  col-md-12 rowNoMargin succesMsgDiv"  data-bind="css:{'entrypanelSuccessDiv':isEditing()}">                   
            <div class="col-md-7 rowNoMargin  alert alert-success" id="VirtualReceiptMsg" data-bind="css:{'toolbarhide':isEditing(),'divhiddenVisiblity':!isSucess()}">
            </div>
          
        </div>
        <!------->

    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important"   data-bind="visible:isRefresh">
        <table>

            <thead>
                <tr>
                    <th  style="width:5%" class="editButtonHeader">


                    </th>                

                    <th style="width:5%" data-bind="click: function () { SetOrderBy('a.receipt_code') }">
                        <span>Receipt Code</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.receipt_code' == orderBy(), 'divhidden': 'a.receipt_code' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th> 

                    <th style="width:20%" data-bind="click: function () { SetOrderBy('a.client_name') }">
                        <span>Client Name</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.client_name' == orderBy(), 'divhidden': 'a.client_name' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <%--<th  style="width:5%" data-bind="click: function () { SetOrderBy('a.Country') }">
                        <span>Country</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'Country' == orderBy(), 'divhidden': 'a.country' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>--%>

                   <%-- <th  style="width:5%" data-bind="click: function () { SetOrderBy('a.Phone') }">
                        <span>Phone</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'Phone' == orderBy(), 'divhidden': 'a.phone' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>--%>

                    <th  style="width:8%" data-bind="click: function () { SetOrderBy('a.email') }">
                        <span>Email</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.email' == orderBy(), 'divhidden': 'a.email' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <%--<th  style="width:5%" data-bind="click: function () { SetOrderBy('a.Description') }">
                        <span>Description</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'Description' == orderBy(), 'divhidden': 'a.Description' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>--%>

                    <th  style="width:5%" data-bind="click: function () { SetOrderBy('a.amount') }">
                        <span>Amount</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.amount' == orderBy(), 'divhidden': 'a.amount' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <th  style="width:8%" data-bind="click: function () { SetOrderBy('a.expiry') }">
                        <span>Expiry</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'a.expiry' == orderBy(), 'divhidden': 'a.expiry' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <th style="width:5%" data-bind="click: function () { SetOrderBy('receipt_paid') }">
                        <span> Receipt Paid</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'receipt_paid' == orderBy(), 'divhidden': 'receipt_paid' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                </tr>
            </thead>

            <tbody data-bind="foreach: $root.VirtualReceiptsList">
               <tr data-bind="click: function (data, event) { $parent.SelectVirtualReceipt(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ReceiptCode() == $parent.SelectVirtualReceipt().ReceiptCode() }">
                   
                 <td class="actioncolumn">
                        <%--<img class="editButton" src="../../Images/Edit.png"  data-bind="click: function (data, event) { $parent.EditVirtualReceipt(data) }" >--%>

                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditVirtualReceipt(data)}" />
    
                    </td>

                    <td data-bind="text: ReceiptCode"></td>

                    <td data-bind="text: ClientName"></td>

                   <%-- <td data-bind="text: Country"></td>

                    <td data-bind="text: Phone"></td>--%>

                    <td data-bind="text: Email"></td>
                    
                    <td data-bind="text: Amount"></td>
                    
                    <td data-bind="text: FormatExpiry"></td>                 

                    <td >

                           <input type="checkbox" class="mediumCheckBox" data-bind="checked: ReceiptPaid()" />
                    </td>



                </tr>

            </tbody>

        </table>




    </div>
    <!----------->
   
    <!--------Entry Panel---------->        
       <!-- #include file="VirtualReceiptEntry.html" -->
    <!-------->
</div>
<!---------------->

                



</asp:Content>

<asp:Content ID="AddOnsScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/VirtualReceipts") %>    
      <%: Styles.Render("~/Content/VirtualReceiptsCss") %>         
</asp:Content>

