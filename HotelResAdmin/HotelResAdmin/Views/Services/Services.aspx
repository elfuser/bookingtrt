﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Services
</asp:Content>
<asp:Content ID="ServicesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/services", "")%>" type="hidden"/>

         <div id="DeleteServices">

         </div>

        <!---------Toolbar------>
    <div class="row   commandbuttondiv"  data-bind="css: { 'toolbarhide': isEditing() }">


            <div class="toolBox">

                 <div data-bind="click:RefreshClick">
                   <%-- <img src="../../images/refreshIcon.png" />--%>  
                    <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                    <span>Refresh</span>
                </div>
                <div data-bind="click:AddClick">
                    <%--<img src="../../images/NewDocument.png" />--%>
                    <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                    <span>Add</span>
                </div>
                <div data-bind="click: DeleteClick">
                    <%--<img src="../../images/deleteIcon.png" />--%>
                    <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                    <span>Delete </span>
                </div>
            </div>

        <div class="pagerPanel">
            <div>
                <span>Page size</span>
                <select data-bind="options:pageSizeOptions,value:selectedPageSize,event: { change:GetSelectedPageSize}"></select>
            </div>

            <div>
                <span>Page no.</span>
                <input type="text" name="page-number" value="" data-bind="value:pageNumber,valueUpdate:'input',PagingwithEnterKey:GetSelectedPage" />
                <button data-bind="click:GetSelectedPage">GO</button>
            </div>

            <div>
                <span class="fa fa-angle-left" data-bind="click:BackwardpageClick"></span>
                <span data-bind="text:pageNoLabel"></span>
                <span class="fa fa-angle-right" data-bind="click:ForwardPageClick"></span>
            </div>
        </div>



    </div>
    <!------>
    <!----Error Msg Div--->

      

    <div data-bind="visible:isEditing" style="display:none;">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


            <!----Success Msg Div--->
     <div class="row  col-md-12 rowNoMargin succesMsgDiv"  data-bind="css:{'entrypanelSuccessDiv':isEditing()}">                   
            <div class="col-md-7 rowNoMargin  alert alert-success" id="ServiceMsg" data-bind="css:{'toolbarhide':isEditing(),'divhiddenVisiblity':!isSucess()}">
            </div>
          
        </div>
        <!------->

    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important"   data-bind="visible:isRefresh">
        <table>

            <thead>
                <tr>
                    <th  style="width:5%" class="editButtonHeader">


                    </th>


                
                    <th data-bind="click: function () { SetOrderBy('s.Code') }">
                        <span>Code</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 's.Code' == orderBy(), 'divhidden': 's.Code' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th> 

                    <th  style="width:70%" data-bind="click: function () { SetOrderBy('sl.Description ') }">
                        <span>Service</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'sl.Description ' == orderBy(), 'divhidden': 'sl.Description ' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th> 

                    <th   data-bind="click: function () { SetOrderBy('s.ListOrder') }">
                        <span>Order</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 's.ListOrder' == orderBy(), 'divhidden': 's.ListOrder' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>

                    <th  style="width:5%" data-bind="click: function () { SetOrderBy('s.Active') }">
                        <span>Active</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 's.Active' == orderBy(), 'divhidden': 's.Active' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>




                </tr>
            </thead>

            <tbody data-bind="foreach: $root.ServiceList">
               <tr data-bind="click: function (data, event) { $parent.SelectedService(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppServiceID() == $parent.SelectedService().ppServiceID() }">
             
                 <td class="actioncolumn">
                    

                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditService(data) }" />

    

                    </td>

                     <td data-bind="text: ppCode"></td>

                    <td data-bind="text: ppDescription"></td>

                    <td data-bind="text: ppListOrder"></td>


                    <td >

                           <input type="checkbox" class="mediumCheckBox" data-bind="checked: ppActive()" />
                    </td>

                </tr>

            </tbody>

        </table>




    </div>
    <!----------->

        <!--------Entry Panel---------->
        <!-- #include file="ServiceEntry.html" -->
    <!-------->

</asp:Content>

<asp:Content ID="ServicesScripts" ContentPlaceHolderID="ScriptsSection" runat="server">
        <%: Scripts.Render("~/bundles/Services") %>   
</asp:Content>
