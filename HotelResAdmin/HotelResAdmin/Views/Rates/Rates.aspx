﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Rates" ContentPlaceHolderID="TitleContent" runat="server">
Rates
</asp:Content>


<asp:Content ID="RatesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/rates", "")%>" type="hidden"/>

         
     <div id="DeleteRates">

     </div>

    
    
<div id="RateSection" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none;">
    <!---------Toolbar------>
    <div class="row   commandbuttondiv" id="cmdButtons"  data-bind="css: { 'toolbarhide': isEditing() }">


            <div class="toolBox">

                <div data-bind="click:RefreshClick">
                   <%-- <img src="../../images/refreshIcon.png" />--%>  
                    <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                    <span>Refresh</span>
                </div>
                <div data-bind="click:AddClick">
                    <%--<img src="../../images/NewDocument.png" />--%>
                    <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                    <span>Add</span>
                </div>
                <div data-bind="click:DeleteClick">
                    <%--<img src="../../images/deleteIcon.png" />--%>
                    <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                    <span>Delete </span>
                </div>
            </div>



    </div>
    <!------>
    <!----Error Msg Div--->

        <div data-bind="visible:isEditing" style="display:none;">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
        <div class="row clientErrorList alert alert-danger col-md-12" id="EntryDetailErrorBoxHeader" data-bind="click: function () { errorExpanderClick('EntryDetailErrorBoxHeader') }">
            <div class="errUpDownArrowDiv">
                <span class="fa fa-caret-down"></span>
                <span class="fa fa-caret-up"></span>
            </div>
            <div class="errorLabelfull">

                <ul data-bind="foreach: ErrorList">
                    <li data-bind="text: ErrorText"></li>
                </ul>

            </div>
        </div>
    </div>

              </div>

      

<%--    <div data-bind="visible:isEditing">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>--%>


    <!----Success Msg Div--->
    <div class="row  col-md-12 rowNoMargin succesMsgDiv"  data-bind="css:{'entrypanelSuccessDiv':isEditing()}">                   
            <div class="col-md-7 rowNoMargin  alert alert-success" id="RateMsg" data-bind="css:{'toolbarhide':isEditing(),'divhiddenVisiblity':!isSucess()}">
            </div>
          
        </div>
    <!------->

    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important"   data-bind="visible:isRefresh">
        <table>

            <thead>
                <tr>
                    <th  style="width:5%" class="editButtonHeader">


                    </th>


                    <th   style="width:8%" >
                        <span>Code</span>
 
                    </th> 

                    <th >
                        <span>Description</span>
                    </th>

                    <th  style="width:6%" >
                        <span>Start Date</span>

                    </th>


                    <th style="width:6%" >
                        <span> End Date</span>

                    </th>

                     <th style="width:4%" >
                        <span> Order</span>

                    </th>

                     <th style="width:4%" >
                        <span>Enabled</span>

                    </th>

                </tr>
            </thead>

            <tbody data-bind="foreach: $root.RatesList">
               <tr data-bind="click: function (data, event) { $parent.SelectedRate(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppRateId() == $parent.SelectedRate().ppRateId() }">
                   
                   <td class="actioncolumn">
                 
                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditRate(data) }" />


                    </td>

                    <td data-bind="text: ppRateCode"></td>
                   
                    <td data-bind="text: ppDescription"></td>

                    <td data-bind="text: ppStrStartDate"></td>

                    <td data-bind="text: ppStrEndDate"></td>

                     <td data-bind="text: ppRowOrder"></td>
                     <td>
                        <input type="checkbox" class="mediumCheckBox" data-bind="checked: ppEnabled()" />
                    </td>

                </tr>

            </tbody>

        </table>




    </div>

    
    <!--------Entry Panel---------->
        <!-- #include file="RateEntry.html" -->
    <!-------->

</div>
<!---------------->

        <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage">
              
          </div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                  <div id="ErrorDiv" class="DialogAlertMessage">
              
          </div> 
        </div>
      </div>
      
    </div>
  </div>   

  

</asp:Content>

<asp:Content ID="RatesScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/Rates") %>              
</asp:Content>
