﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Agencies" ContentPlaceHolderID="TitleContent" runat="server">
Agencies
</asp:Content>


<asp:Content ID="AgenciesContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/agencies", "")%>" type="hidden"/>

         
     <div id="DeleteAgencies">

     </div>

    
    
<div id="AgencySection" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none;">
    <!---------Toolbar------>
    <div class="row   commandbuttondiv" id="cmdButtons"  data-bind="css: { 'toolbarhide': isEditing() }">


            <div class="toolBox">

                <div data-bind="click:RefreshClick">
                   <%-- <img src="../../images/refreshIcon.png" />--%>  
                    <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                    <span>Refresh</span>
                </div>
                <div data-bind="click:AddClick">
                    <%--<img src="../../images/NewDocument.png" />--%>
                    <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                    <span>Add</span>
                </div>
                <div data-bind="click:DeleteClick">
                    <%--<img src="../../images/deleteIcon.png" />--%>
                    <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                    <span>Delete </span>
                </div>
            </div>
            <div class="pagerPanel">
                    <div>
                        <span>Page size</span>
                        <select data-bind="options:pageSizeOptions,value:selectedPageSize,event: { change:GetSelectedPageSize}"></select>
                    </div>

                    <div>
                        <span>Page no.</span>
                        <input type="text" name="page-number" value="" data-bind="value:pageNumber,valueUpdate:'input',PagingwithEnterKey:GetSelectedPage" />
                        <button data-bind="click:GetSelectedPage">GO</button>
                    </div>

                    <div>
                        <span class="fa fa-angle-left" data-bind="click:BackwardpageClick"></span>
                        <span data-bind="text:pageNoLabel"></span>
                        <span class="fa fa-angle-right" data-bind="click:ForwardPageClick"></span>
                    </div>
                </div>


    </div>
    <!------>
    <!----Error Msg Div--->

        <div data-bind="visible:isEditing" style="display:none;">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
        <div class="row clientErrorList alert alert-danger col-md-12" id="EntryDetailErrorBoxHeader" data-bind="click: function () { errorExpanderClick('EntryDetailErrorBoxHeader') }">
            <div class="errUpDownArrowDiv">
                <span class="fa fa-caret-down"></span>
                <span class="fa fa-caret-up"></span>
            </div>
            <div class="errorLabelfull">

                <ul data-bind="foreach: ErrorList">
                    <li data-bind="text: ErrorText"></li>
                </ul>

            </div>
        </div>
    </div>

              </div>

      

<%--    <div data-bind="visible:isEditing">
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>--%>


    <!----Success Msg Div--->
    <div class="row  col-md-12 rowNoMargin succesMsgDiv"  data-bind="css:{'entrypanelSuccessDiv':isEditing()}">                   
            <div class="col-md-7 rowNoMargin  alert alert-success" id="AgencyMsg" data-bind="css:{'toolbarhide':isEditing(),'divhiddenVisiblity':!isSucess()}">
            </div>
          
        </div>
    <!------->

    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important"   data-bind="visible:isRefresh">
        <table>

            <thead>
                <tr>
                    <th  style="width:5%" class="editButtonHeader">


                    </th>

                    <th style="width:8%" data-bind="click: function () { SetOrderBy('agency_code') }">
                        <span>Code</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'agency_code' == orderBy(), 'divhidden': 'agency_code' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>  
                    
                    <th style="width:25%" data-bind="click: function () { SetOrderBy('name') }">
                        <span>Name</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'name' == orderBy(), 'divhidden': 'name' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>                      

                    <th style="width:25%" data-bind="click: function () { SetOrderBy('contact_name') }">
                        <span>Main contact name</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'contact_name' == orderBy(), 'divhidden': 'contact_name' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>                  

                    <th style="width:6%" data-bind="click: function () { SetOrderBy('created') }">
                        <span>Created</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'created' == orderBy(), 'divhidden': 'created' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>                                         

                    <th style="width:4%" data-bind="click: function () { SetOrderBy('active') }">
                        <span>Active</span>
                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'active' == orderBy(), 'divhidden': 'active' != orderBy() }">
                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDown(), 'divhidden': !isDown() }">
                            </div>
                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUp(), 'divhidden': !isUp() }">
                            </div>
                        </div>
                    </th>   
                    
                </tr>
            </thead>

            <tbody data-bind="foreach: $root.AgenciesList">
               <tr data-bind="click: function (data, event) { $parent.SelectedAgency(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppAgencyCode() == $parent.SelectedAgency().ppAgencyCode() }">
                   
                   <td class="actioncolumn">
                 
                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditAgency(data) }" />

                    </td>

                    <td data-bind="text: ppAgencyCode"></td>
                   
                    <td data-bind="text: ppAgencyName"></td>

                    <td data-bind="text: ppContactName"></td>
                   
                    <td data-bind="text: FormatCreated"></td>
                     
                    <td>
                        <input type="checkbox" class="mediumCheckBox" data-bind="checked: ppActive()" />
                    </td>

                </tr>

            </tbody>

        </table>




    </div>

    
    <!--------Entry Panel---------->
        <!-- #include file="AgencyEntry.html" -->
    <!-------->

</div>
<!---------------->

        <!-- Modal -->
 <div class="modal fade" id="dialog_msg" role="dialog">
    <div class="modal-dialog modal-sm">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header ModalHeaderStyle">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <div id="DialogAlertMessage" class="DialogAlertMessage">
              
          </div>  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                  <div id="ErrorDiv" class="DialogAlertMessage">
              
          </div> 
        </div>
      </div>
      
    </div>
  </div>   

  

</asp:Content>

<asp:Content ID="AgenciesScripts" ContentPlaceHolderID="ScriptsSection" runat="server">                   
    <%: Scripts.Render("~/bundles/Agencies") %>    
    <%: Styles.Render("~/Content/AgenciesCss") %>                
</asp:Content>
