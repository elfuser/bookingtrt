﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="AvailabilityAdminDashboard" ContentPlaceHolderID="TitleContent" runat="server">
Closing Alerts
</asp:Content>


<asp:Content ID="AvailabilityContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/queries", "")%>" type="hidden"/>

    <div id="AvailabilityQueryDiv" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none; background-color:white">

             <h4>Closing Alerts</h4>

        
        <div >
            <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
                <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                    <div class="errorLabelfull">
                        <ul data-bind="foreach: ErrorList">
                            <li data-bind="text: ErrorText"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row rowNoMargin entryPanel">            
             <div class="col-sm-10 noMargin smallMargintop">                        
                <div class="btn btn-warning " role="button" id="btnExport">
                    Export to Excel
                </div>
            </div>
        </div>

        <br />
        
        <!-----Calendar------->    
        <div class="Calendar_TD">
            <div class="datepicker" id="dt_availability"></div>                   
        </div>    

    </div>


    <div id="detailDialog" style="display:none;">
        <h4>Room Detail - <label id="lbDateOnDetail"></label></h4>

        <!-----Grid------->
        <div class="dataGrid" style="margin-top:5px !important">
            <table>

                <thead>
                    <tr>                       

                        <th>
                            <span>Room</span>
                        </th>
                        <th>
                            <span>Quantity</span>
                        </th>
                        <th>
                            <span>Occupied</span>
                        </th>
                        <th>
                            <span>Blocked</span>
                        </th>
                        <th>
                            <span>Available</span>
                        </th>

                    </tr>
                </thead>

                <tbody data-bind="foreach: RoomsDetails">                
                    <tr>                                                  
                        <td data-bind="text: ppRoomTypeName"></td>

                        <td data-bind="text: ppRoomsQty"></td>

                        <td data-bind="text: ppRoomsOccup"></td>

                        <td data-bind="text: ppRoomsBlocked"></td>

                        <td data-bind="text: ppRoomsAvail"></td>
                                       
                    </tr>

                </tbody>

            </table>



        </div>
    </div>
    
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptsSection" runat="server">
        <%: Scripts.Render("~/bundles/datepicker") %>
        <%: Scripts.Render("~/bundles/ClosingGQuery") %>    
        <%: Styles.Render("~/Content/QueryRoomsAvailabilityCss") %>  
    
        <%--<script>
           $(document).ready(function () {
               var max = -1
               $("#dt_availability").find("table").find("td").each(function () {
                   var h = $(this).height();
                   max = h > max ? h : max;                       
               });

               $("#dt_availability").find("table").find("td").each(function () {
                   $(this).css("height", max);
                   //if ($(this).find("span").hasClass("UnavailableDateCellStyle")) {
                   //    $(this).css("background-color", 'lightcoral');
                   //}
               });
           })
        </script>  --%> 
</asp:Content>

