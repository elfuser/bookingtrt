﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reservation List Commissions
</asp:Content>
<asp:Content ID="CommissionsContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/queries", "")%>" type="hidden"/>
    <div id="CommissionsQueryDiv" class="col-md-12 col-xs-12 col-sm-12 noMarginright" style="display:none; background-color:white">

             <h4>Reservation List Commissions</h4>

        
    <div >
        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class=" row clientErrorList alert alert-danger col-md-12" id="ErrorListDiv">
                <div class="errorLabelfull">
                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

          <div class="row rowNoMargin entryPanel">

            <div class="col-sm-1 noMargin smallMargintop">
                <label>
                    Start
                </label>
            </div>
            <div class="col-md-2 noMargin smallMargintop">

                <input type="text" placeholder="To Date(mm/dd/yyyy)" required title="From Date is required" name="FromDateReserv"
                       class="form-control dateinput" id="FromDateReserv" data-bind="FromDatePicker:{}, value:selectedFromDate" />
            </div>

            <div class="col-sm-1 noMargin smallMargintop">
                <label>
                    End
                </label>
            </div>
            <div class="col-md-2 noMargin smallMargintop">
                <input type="text" placeholder="To Date(mm/dd/yyyy)" required title="To Date is required" name="ToDateReserv"
                       class="form-control dateinput" id="ToDateReserv" data-bind="ToDatePicker:{}, value:selectedToDate" />
            </div>




               <div class="col-sm-2 noMargin smallMargintop">

                   <div>
                        <div><input type="radio" name="DateType" class="smallCheckBox" value="1" data-bind="checked: rdDateType" /> Created Date</div>
                        <div><input type="radio" name="DateType"  class="smallCheckBox" value="2" data-bind="checked: rdDateType" /> Checkin Date</div>
                    </div>
                </div>

             <div class="col-sm-4 noMargin smallMargintop">
                            <div class="btn btn-primary " role="button" type="submit" data-bind="click: Search">
                                Search
                            </div>

                            <div class="btn btn-warning " role="button" id="btnExport" type="submit" >
                                Export to Excel
                            </div>
            </div>
        </div>

        <br />


        
    <!-----Grid------->
    <div class="dataGrid" style="margin-top:5px !important" id="CommissionTable"  >
        <table>

            <thead>
                <tr>
       
                     <th  style="width:1%" class="editButtonHeader">


                    </th>

                
                    <th >
                        <span>Reservation</span>
                    </th> 

                    <th  >
                        <span>Name</span>

                    </th> 

                    <th  >
                        <span>Lastname</span>
                    </th>

                    <th>
                        <span>Arrival</span>

                    </th>

                    <th>
                        <span>Departure</span>

                    </th>

                      <th><span>Nights</span> </th>
                      <th><span>Status</span> </th>
                    <th><span>Rate</span> </th>
                        <th><span>Amount</span> </th>
                     <th><span>Addons</span> </th>
                      <th><span>Promocode</span> </th>
                      <th><span> % OL</span> </th>
                    <th><span>   % Bank</span> </th>
                   <th><span>  Total </span> </th>
                       <th><span>  Processor </span> </th>
                       <th><span>   Card Number </span> </th>
                   <th><span>  Exp Date </span> </th>
                      <th><span> Approve Code </span> </th>
                      <th><span> Type </span> </th>
                    



                </tr>
            </thead>

            <tbody data-bind="foreach: Reservations">
               <tr >
             
                 <td class="actioncolumn">
                    

                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.ViewDetail(data) }" />

    

                    </td>

                     <td data-bind="text: reservation"></td>

                    <td data-bind="text: name"></td>

                    <td data-bind="text: lastname"></td>
                  
                    <td data-bind="text: arrival"></td>

                    <td data-bind="text: departure"></td>
                   
                       <td data-bind="text: nights"></td>
                   
                    <td data-bind="text: statusText"></td>
                   
                     <td data-bind="text: rateName"></td>
                   
                       <td data-bind="text: amount"></td>
                       <td data-bind="text: addonAmount"></td>
                      <td data-bind="text: promoAmount"></td>
                     <td data-bind="text: commOL"></td>
                       <td data-bind="text: commBank"></td>
                        <td data-bind="text: total"></td>
                       <td data-bind="text: processor"></td>
                       <td data-bind="text: creditCard"></td>
                       <td data-bind="text: expDate"></td>
                    <td data-bind="text: approveCode"></td>
                         <td data-bind="text: cardType"></td>
                   

                </tr>

            </tbody>

        </table>




    </div>
    <!----------->

                <div id="DetailDiv">
             <!-- #include file="ReservationDetail.html" -->
    </div>

    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptsSection" runat="server">
        <%: Scripts.Render("~/bundles/ComissionsQuery") %>        
</asp:Content>
