﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="SalesDashboard" ContentPlaceHolderID="TitleContent" runat="server">
Sales Dashboard
</asp:Content>


<asp:Content ID="SalesDashboardContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/dashboard", "")%>" type="hidden"/>


    <div id="DashboardSection" class="col-md-12 col-xs-12 col-sm-12 noMarginright">

        <table style="width: 99%">
            <tr>
                <td>
                    <div style="max-height: 500px; max-width: 400px; padding-left: 5%">
                        <h3>Sales by Month </h3>
                        <div id="DivTotalSales">
                        </div>
                    </div>
                </td>
                <td>

                    <div style="max-height: 500px; max-width: 400px; padding-left: 1%">
                        <h3>Sales by Room </h3>
                        <div id="DivTotalRoomSales">
                        </div>
                    </div>

                </td>
            </tr>
        </table>

    </div>

</asp:Content>


<asp:Content ID="AddOnsScripts" ContentPlaceHolderID="ScriptsSection" runat="server">               
    <%: Scripts.Render("~/bundles/DashboardScripts") %>              
</asp:Content>