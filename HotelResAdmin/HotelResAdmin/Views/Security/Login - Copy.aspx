﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>

       
     
      <%: Styles.Render("~/bundles/Login") %>
  

        <%: Scripts.Render("~/bundles/modernizr") %>
    
    

</head>
<body>
<form id="form1" runat="server">
  <div>
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/security", "")%>" type="hidden"/>
    <div class="row rowNoMargin col-md-12 col-xs-12 col-sm-12 loginMaindiv"  data-bind="visible:loginPage">
        <div class="col-md-12  col-xs-12 col-sm-12 centered" >
            <div class="col-md-4 col-xs-6 col-sm-6 HdrLogin centered">
               Login
            </div>
            <div class="col-md-4 col-xs-6 col-sm-6 centered logindiv">
                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel">
                    User Name
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6  rowNoMargin">
                    <input type="text" class="mediumtextboxes" data-bind="value:userName,valueUpdate: 'input', loginwithEnterKey:doLogin" name="UserName" />
                    <div data-bind="visible:isuserNameMissing" class="validatemsgdiv">
                        User Name is required
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                    Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: password,loginwithEnterKey:doLogin,valueUpdate: 'input'" name="Password" />
                    <div data-bind="visible:isPasswordMissing" class="validatemsgdiv">
                        Password is required
                    </div>
                </div>

                <div class="col-md-12  col-xs-12 col-sm-12 text-center mediumMargintop">
                    <input type="checkbox" data-bind="checked:rememberme" />
                    Remember me
                </div>
                <div class="text-center col-md-12  col-xs-12 col-sm-12 mediumMargintop">
                    <button data-bind="click: doLogin ,css:{'loginCursorHide':isLogin()}" type="submit">
                        Login
                    </button>

                </div>
                <div class="col-md-12  col-xs-12 col-sm-12  text-center">
                    <label data-bind="text:$root.errorMessage" class="validatemsgdiv">
                    </label>
                </div>
            </div>
        </div>
    </div>
   
       

        <div class="row rowNoMargin col-md-12 col-xs-12 col-sm-12 loginMaindiv"  data-bind="visible: ChangeloginPage">
            <div class="col-md-12  col-xs-12 col-sm-12 centered" >
                <div class="col-md-4 col-xs-6 col-sm-6 HdrLogin centered">
                    Change Password
                </div>

               <div class="col-md-4 col-xs-6 col-sm-6 centered logindiv">

                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   Current Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: password,  valueUpdate: 'input'" name="Password" />
                    <div data-bind="visible:isCurrentPasswordMissing" class="validatemsgdiv">
                      Current  Password is required
                    </div>
                </div>

                 <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   New Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: NewPassword,valueUpdate: 'input'" name="NewPassword" />
                    <div data-bind="visible:isNewPasswordMissing" class="validatemsgdiv">
                      New  Password is required
                    </div>
                </div>

                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   Confirm Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: ConfirmPassword, valueUpdate: 'input'" name="ConfirmPassword" />
                    <div data-bind="visible:isConfirmPasswordMissing" class="validatemsgdiv">
                      Confirm  Password is required
                    </div>
                </div>

                <div class="text-center col-md-12  col-xs-12 col-sm-12 mediumMargintop">
                    <button data-bind="click: ChangePassword " type="submit">
                        Change Password
                    </button>
                 
                </div>
                <div class="col-md-12  col-xs-12 col-sm-12  text-center">
                    <label data-bind="text:$root.errorMessage" class="validatemsgdiv">
                    </label>
                </div>

               </div>
            </div>
        </div>
        
   </div>
 </form>
</body>
</html>

        <%: Scripts.Render("~/bundles/jquery") %>
        <%: Scripts.Render("~/bundles/jqueryui") %>
          <%: Scripts.Render("~/bundles/KnockOut") %>
              <%: Scripts.Render("~/bundles/Util") %>

              <%: Scripts.Render("~/bundles/LoginScripts") %>
