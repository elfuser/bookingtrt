﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>

       
     
      <%: Styles.Render("~/bundles/Login") %>
  

        <%: Scripts.Render("~/bundles/modernizr") %>
    
    

</head>
<body>
<form id="form1" runat="server">
 <div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uVqHQj/7BWptTx26AIX5PWJLVy3zB4NfuCqOiIsifxy+Sfgpi1KnVXPSSVag2Y1lm9Wb81vcuWWTOidRMbTo+tTmnBTE81BULjhwT8x3PgA=">
</div>

  <div>
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/security", "")%>" type="hidden"/>
    <div class="row rowNoMargin col-md-12 col-xs-12 col-sm-12 loginMaindiv" data-bind="visible:loginPage">
        <div class="col-md-12  col-xs-12 col-sm-12 centered">
            <div class="col-md-4 col-xs-6 col-sm-6 centered logindiv">
<!-- Redesign Update -->  
               <!-- Add this div and h1-->
               <div class="login-left"> 
               	<h1 class="login-title">Sign In</h1>
               <!-- End add div -->
                	<label for="UserName">
                		<p>User Name:</p>
                		<input type="text" class="mediumtextboxes" data-bind="value:userName,valueUpdate: &#39;input&#39;, loginwithEnterKey:doLogin" name="UserName">
                		<div data-bind="visible:isuserNameMissing" class="validatemsgdiv" style="display: none;">
                        User Name is required
                    </div>
                	</label>
                	<label for="Password">
                		Password:
                		<input type="password" class="mediumtextboxes" data-bind="value: password,loginwithEnterKey:doLogin,valueUpdate: &#39;input&#39;" name="Password">
                    	<div data-bind="visible:isPasswordMissing" class="validatemsgdiv" style="display: none;">
                        	Password is required
                    	</div>
                	</label>
                   <label>
                   		<input type="checkbox" data-bind="checked:rememberme">
                    	Remember me
                   </label>
                   <br> 
                   <button data-bind="click: doLogin ,css:{'loginCursorHide':isLogin()}" type="submit">
                        Sign In
                    </button>
                    <label data-bind="text:$root.errorMessage" class="validatemsgdiv"></label>
                    
                <!-- End new code -->
                
                
                
                <!-- Close the div-->
                </div>
                <!-- End close div-->
                
                <!-- Add new div for credomatic logo-->
                <div class="login-right">
                	<img src="../../Images/credomatic.jpg">
                    <br> 
                    <br> 
                    <br> 
                    <br> 
                    <button data-bind="click: doLoginWindows ,css:{'loginCursorHide':isLogin()}" type="submit">
                        WINDOWS
                    </button
                </div>
            	<!-- End new div for credomatic logo-->
            </div>
        </div>
    </div>
   
       

        <div class="row rowNoMargin col-md-12 col-xs-12 col-sm-12 loginMaindiv" data-bind="visible: ChangeloginPage" style="display: none;">
            <div class="col-md-12  col-xs-12 col-sm-12 centered">
                <div class="col-md-4 col-xs-6 col-sm-6 HdrLogin centered">
                    Change Password
                </div>

               <div class="col-md-4 col-xs-6 col-sm-6 centered logindiv">

                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   Current Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: password,  valueUpdate: 'input'" name="Password" />
                    <div data-bind="visible:isCurrentPasswordMissing" class="validatemsgdiv" style="display: none;">
                      Current  Password is required
                    </div>
                </div>

                 <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   New Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                    <input type="password" class="mediumtextboxes" data-bind="value: NewPassword,valueUpdate: 'input'" name="NewPassword" />
                    <div data-bind="visible:isNewPasswordMissing" class="validatemsgdiv" style="display: none;">
                      New  Password is required
                    </div>
                </div>

                <div class="col-md-4 col-xs-6 col-sm-6 rowNoMargin loginLabel mediumMargintop">
                   Confirm Password
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6 rowNoMargin mediumMargintop">
                     <input type="password" class="mediumtextboxes" data-bind="value: ConfirmPassword, valueUpdate: 'input'" name="ConfirmPassword" />
                    <div data-bind="visible:isConfirmPasswordMissing" class="validatemsgdiv" style="display: none;">
                      Confirm  Password is required
                    </div>
                </div>

                <div class="text-center col-md-12  col-xs-12 col-sm-12 mediumMargintop">
                    <button data-bind="click: ChangePassword " type="submit">
                        Change Password
                    </button>
                 
                </div>
                <div class="col-md-12  col-xs-12 col-sm-12  text-center">
                    <label data-bind="text:$root.errorMessage" class="validatemsgdiv"></label>
                </div>

               </div>
            </div>
        </div>
        
   </div>
 
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C31BC190">
</div>
 </form>
</body>
</html>

        <%: Scripts.Render("~/bundles/jquery") %>
        <%: Scripts.Render("~/bundles/jqueryui") %>
          <%: Scripts.Render("~/bundles/KnockOut") %>
              <%: Scripts.Render("~/bundles/Util") %>

              <%: Scripts.Render("~/bundles/LoginScripts") %>
