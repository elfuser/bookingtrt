﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="UsersAdminDashboard" ContentPlaceHolderID="TitleContent" runat="server">
    Users
</asp:Content>


<asp:Content ID="UsersContent" ContentPlaceHolderID="MainContent" runat="server">
    <input id="txtTrackingURLBase"  value="<%: Url.Action("Index").ToLower().Replace("/security", "")%>" type="hidden"/>

    <div id="DeleteProfile">
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12 noMarginright EntryPanelcss">

        <div class="row rowNoMargin" data-bind="css: { 'divvisible': isShowErrorPanel(), 'divhidden': !isShowErrorPanel() }">
            <div class="row clientErrorList alert alert-danger col-md-12" id="EntryDetailErrorBox" data-bind="click: function () { errorExpanderClick('EntryDetailErrorBox') }">
                <div class="errUpDownArrowDiv">
                    <span class="fa fa-caret-down"></span>
                    <span class="fa fa-caret-up"></span>
                </div>
                <div class="errorLabelfull">

                    <ul data-bind="foreach: ErrorList">
                        <li data-bind="text: ErrorText"></li>
                    </ul>

                </div>
            </div>
        </div>


      <div class="row  col-md-12 rowNoMargin succesMsgDiv" >
        <div class="col-md-7 rowNoMargin  alert alert-success" id="ProfileMsg"   data-bind="css: { 'divvisible': isShowMsgProfile(), 'divhidden': !isShowMsgProfile() }">
        </div>

    </div>

        <div id="TabsDiv" style="width: 100%; background-color: white">
            <ul class="noMargin tabAppMenu">
                <li class="carrierLink activePageMenu" id="ProfilesTab" data-bind="click: ProfilesClick"><a>Profiles</a>  </li>

                <li>|</li>
                <li class="carrierLink " id="UsersTab" data-bind="click: UsersClick"><a>Users</a>  </li>

                <li>|</li>
                <li class="carrierLink " id="EventLogTab" data-bind="click: EventLogClick"><a>Event Log</a>  </li>


            </ul>
        </div>


        <div id="ProfilesDiv" class="col-md-12 col-xs-12 col-sm-12 noMarginright">

            <div class="row rowNoMargin entryPanel">

                <div class="col-md-10 noMargin smallMargintop">

                    <table class=" "  style="width: 100%; vertical-align: top">
                    <tr>
                        <td style="width: 50%; vertical-align: top">

                            <!------profiles--------->
                            <!-----Grid------->
                            <div class="dataGrid" style="margin-top: 5px !important">
                                <table>

                                    <thead>
                                        <tr>

                                            <th  style="width:5%" class="editButtonHeader">


                                              </th>

                                            <th style="width: 95%">
                                                <span>Profile</span>
                                            </th>


                                        </tr>
                                    </thead>

                                    <tbody data-bind="foreach: ProfileList">

                                                     
                         
                                        <tr data-bind="click: function (data, event) { $root.selectedProfile(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppProfileID() == $root.selectedProfile().ppProfileID() }">

                                                            <td class="actioncolumn">
                    

                                                 <i class="fa fa-trash-o " data-bind="click: function (data, event) { $root.DeleteProfileClick(data) }" />

    

                                            </td>


                                            <td data-bind="text: ppProfileName"></td>

                                        </tr>

                                    </tbody>

                                </table>




                            </div>
                            <!----------->
                        </td>
                        <td style="width: 50%; vertical-align: top">

                            <div  class="" data-bind="with: selectedProfile()">

                                <div class="row rowNoMargin entryPanel">

                                    <div class="col-sm-2 noMargin smallMargintop">
                                        <label>
                                            Profile
                                        </label>
                                    </div>
                                    <div class="col-md-6 noMargin">
                                        <input type="text" class="form-control " maxlength="30" id="ppProfileName" data-bind="value: ppProfileName" name="ppProfileName" />
                                    </div>




                                </div>
                          

                        
                                 
                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-10 noMargin smallMargintop">
                                                    <label style="font-size: medium; font-weight: bold;">
                                                        This profile is going to have access to:
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Dashboard
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkDasboard" name="chkDasboard" class="form-control  mediumCheckBox" data-bind="checked: Dashboard" />
                                                </div>

                                                                                   <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Company
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkCompany" name="chkCompany" class="form-control  mediumCheckBox" data-bind="checked: Company" />
                                                </div>

                                            </div>

         

                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Rates
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkRates" name="chkRates" class="form-control  mediumCheckBox" data-bind="checked: Rates" />
                                                </div>

                                                                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Rooms
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkRooms" name="chkRooms" class="form-control  mediumCheckBox" data-bind="checked: Rooms" />
                                                </div>
                                            </div>




                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Addons
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkAddons" name="chkAddons" class="form-control  mediumCheckBox" data-bind="checked: Addons" />
                                                </div>

                                                                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Services
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkServices" name="chkServices" class="form-control  mediumCheckBox" data-bind="checked: Services" />
                                                </div>
                                            </div>


                                 
                         
                                                   <br />
                          
                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-sm-5 noMargin smallMargintop">
                                                    <label style="font-size: medium; font-weight: bold;">
                                                        Queries
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Reservations
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkReservations" name="chkReservations" class="form-control  mediumCheckBox" data-bind="checked: Reservations" />
                                                </div>


                                                                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Availability
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkAvaibility" name="chkAvaibility" class="form-control  mediumCheckBox" data-bind="checked: Avaibility" />
                                                </div>
                                            </div>




                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Comissions
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkComissions" name="chkComissions" class="form-control  mediumCheckBox" data-bind="checked: Comissions" />
                                                </div>

                                            </div>

                                 <br />

                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-sm-5 noMargin smallMargintop">
                                                    <label style="font-size: medium; font-weight: bold;">
                                                        Reports
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Reservations
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkReportsReserv" name="chkReportsReserv" class="form-control  mediumCheckBox" data-bind="checked: ReportsReserv" />
                                                </div>

                                                
                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Sales
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkReportSalesv" name="chkReportSales" class="form-control  mediumCheckBox" data-bind="checked: ReportSales" />
                                                </div>
                                            </div>




                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Guest
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkReportGuest" name="chkReportGuest" class="form-control  mediumCheckBox" data-bind="checked: ReportGuest" />
                                                </div>

                                            </div>

       

                                <br />

                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-sm-5 noMargin smallMargintop">
                                                    <label style="font-size: medium; font-weight: bold;">
                                                        Security
                                                    </label>
                                                </div>
                                            </div>




                                            <div class="row rowNoMargin entryPanel">

                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        Profiles
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkProfiles" name="chkProfiles" class="form-control  mediumCheckBox" data-bind="checked: Profiles" />
                                                </div>

                                                                                                <div class="col-md-2  smallMargintop">
                                                    <label>
                                                        EventLog
                                                    </label>


                                                </div>

                                                <div class="col-sm-1 noMargin smallMargintop">
                                                    <input tabindex="1" type="checkbox" id="chkEventLog" name="chkEventLog" class="form-control  mediumCheckBox" data-bind="checked: EventLog" />
                                                </div>
                                            </div>






                            </div>


                        </td>
                    </tr>

                    
                    </table>

                </div>



                                            <br />

                    <div class="row rowNoMargin entryPanel">
                                     <div class="btn btn-primary " role="button"   type="submit" data-bind="click: $root.SaveProfileClick">
                                        Save
                                    </div>

                                    <div class="btn btn-danger" tabindex="21" role="button" data-bind="click: $root.CancelClick">
                                        Cancel
                                    </div>
                    </div>


            </div>
        </div>

        <div id="UsersDiv" style="background-color: white; display:none">

                    <div id="UserList" class="col-md-12 col-xs-12 col-sm-12 noMarginright"  data-bind="css: { 'divvisible': !isEditingUsers(), 'divhidden': isEditingUsers() }">
                        <br />
                     <div class="row rowNoMargin entryPanel">
                                  
                                    <!---------Toolbar------>
                            <div class="row   commandbuttondiv"  data-bind="css: { 'toolbarhide': isEditing() }">


                                    <div class="toolBox">

                                         <div data-bind="click:RefreshUsersClick">
                                           <%-- <img src="../../images/refreshIcon.png" />--%>  
                                            <i class="fa fa-refresh actionButton" aria-hidden="true"></i>                                      
                                            <span>Refresh</span>
                                        </div>
                                        <div data-bind="click:AddUsersClick">
                                            <%--<img src="../../images/NewDocument.png" />--%>
                                            <i class="fa fa-file-o actionButton" aria-hidden="true"></i>
                                            <span>Add</span>
                                        </div>
                                        <div data-bind="click:DeleteUsersClick">
                                            <%--<img src="../../images/deleteIcon.png" />--%>
                                            <i class="fa fa-trash-o actionButton" aria-hidden="true"></i>
                                            <span>Delete </span>
                                        </div>
                                    </div>


                                    <div class="pagerPanel">
                                    <div>
                                        <span>Page size</span>
                                        <select data-bind="options:pageSizeOptionsUsers,value:selectedPageSizeUsers,event: { change:GetSelectedPageSizeUsers}"></select>
                                    </div>

                                    <div>
                                        <span>Page no.</span>
                                        <input type="text" name="page-number" value="" data-bind="value: pageNumberUsers, valueUpdate: 'input', PagingwithEnterKey: GetSelectedPageUsers" />
                                        <button data-bind="click: GetSelectedPageUsers">GO</button>
                                    </div>

                                    <div>
                                        <span class="fa fa-angle-left" data-bind="click: BackwardpageClickUsers"></span>
                                        <span data-bind="text:pageNoLabelUsers"></span>
                                        <span class="fa fa-angle-right" data-bind="click: ForwardPageClickUsers"></span>
                                    </div>
                                </div>




                            </div>

                         

                            
                                    <!-----Grid------->
                                    <div class="dataGrid" style="margin-top:5px !important"  >
                                        <table>

                                            <thead>
                                                <tr>
                                                    <th  style="width:5%" class="editButtonHeader">


                                                    </th>


                


                                                    <th  data-bind="click: function () { SetOrderByUsers('first_name') }">
                                                        <span>First Name</span>
                                                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'first_name' == orderByUsers(), 'divhidden': 'first_name' != orderByUsers() }">
                                                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                            </div>
                                                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                            </div>
                                                        </div>
                                                    </th> 

 
                                                     <th  data-bind="click: function () { SetOrderByUsers('last_name') }">
                                                        <span>Last Name</span>
                                                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'last_name' == orderByUsers(), 'divhidden': 'last_name' != orderByUsers() }">
                                                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                            </div>
                                                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                            </div>
                                                        </div>
                                                    </th> 
                                       
                                        
                                                      <th  data-bind="click: function () { SetOrderByUsers('user_name') }">
                                                                <span>UserName</span>
                                                                <div class="sortingIcon" data-bind="css: { 'divvisible': 'user_name' == orderByUsers(), 'divhidden': 'user_name' != orderByUsers() }">
                                                                    <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                                    </div>
                                                                    <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                                    </div>
                                                                </div>
                                                            </th> 
                                      
                                                         <th  data-bind="click: function () { SetOrderByUsers('c.profile_name') }">
                                                                <span>Profile</span>
                                                                <div class="sortingIcon" data-bind="css: { 'divvisible': 'c.profile_name' == orderByUsers(), 'divhidden': 'c.profile_name' != orderByUsers() }">
                                                                    <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                                    </div>
                                                                    <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                                    </div>
                                                                </div>
                                                            </th> 
                                      

                                                            <th  data-bind="click: function () { SetOrderByUsers('U.last_login') }">
                                                                <span>Last Login</span>
                                                                <div class="sortingIcon" data-bind="css: { 'divvisible': 'U.last_login' == orderByUsers(), 'divhidden': 'U.last_login' != orderByUsers() }">
                                                                    <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                                    </div>
                                                                    <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                                    </div>
                                                                </div>
                                                            </th> 

                                                              <th  data-bind="click: function () { SetOrderByUsers('U.active') }">
                                                                <span>Active</span>
                                                                <div class="sortingIcon" data-bind="css: { 'divvisible': 'U.active' == orderByUsers(), 'divhidden': 'U.active' != orderByUsers() }">
                                                                    <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsers(), 'divhidden': !isDownUsers() }">
                                                                    </div>
                                                                    <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsers(), 'divhidden': !isUpUsers() }">
                                                                    </div>
                                                                </div>
                                                            </th> 

                                                    
                                                            <th  style="width:5%" class="editButtonHeader">


                                                            </th>

                                                              <th  style="width:5%" class="editButtonHeader">


                                                            </th>

                                                     </tr>


                                            </thead>

                        


                                            <tbody data-bind="foreach: $root.UsersList">
                                               <tr data-bind="click: function (data, event) { $parent.SelectedUser(data) }, css: { rowEven: ($index() % 2 === 0), rowOdd: ($index() % 2 === 1), 'selectedRow': ppUserID() == $parent.SelectedUser().ppUserID() }">
                   
                                                 <td class="actioncolumn">
                                                        <%--<img class="editButton" src="../../Images/Edit.png" >--%>

                                                         <i class="fa fa-pencil editButton" data-bind="click: function (data, event) { $parent.EditUser(data) }" />

    

                                                    </td>

                                                    <td data-bind="text: ppFirstName"></td>

                                                    <td data-bind="text: ppLastName"></td>

                                                       <td data-bind="text: ppUserName"></td>

                                                       <td data-bind="text: ppProfile"></td>

                                                      <td data-bind="text: LastLoginFormat"></td>

                                                      <td data-bind="text: ppActive"></td>

                                                      <td class="actioncolumn">
                                                           <div class="btn btn-primary " role="button"   type="submit"  data-bind="click: function (data, event) { $parent.BlockUser(data) }">
                                                                Block / Unblock
                                                            </div>
                                                        </td>

                                                        <td class="actioncolumn">
                                                           <div class="btn btn-primary " role="button"   type="submit"  data-bind="click: function (data, event) { $parent.ResetPassword(data) }">
                                                                Reset Pwd.
                                                            </div>
                                                        </td>
                                                </tr>

                                            </tbody>

                                        </table>

                                        <br />



                                    </div>


                                    <!----------->
                            <!------>

                      </div>
                    </div>


                <!--------Entry Panel---------->
                <!-- #include file="UsersEntry.html" -->
            <!-------->
            <br />
        </div>

        <div id="EventLogTDiv" style="background-color: white;display:none">

             <div class="row rowNoMargin entryPanel">
                 
                    <div class="col-sm-2 noMargin smallMargintop">
                        <label>
                            User
                        </label>
                    </div>
                    <div class="col-md-5 noMargin smallMargintop">
                           <select class="form-control " id="UserListID" name="UserListID" tabindex="0" data-bind="options: CompanyUserLogList, optionsText: 'ppFirstName', value: SelectedLogUser"></select>

                    </div>
            </div>

             <div class="row rowNoMargin entryPanel">

                    <div class="col-sm-2 noMargin smallMargintop">
                        <label>
                            Start Date
                        </label>
                    </div>
                    <div class="col-md-2 noMargin smallMargintop">

                        <input type="text" placeholder="To Date(mm/dd/yyyy)" required title="From Date is required" name="FromDate"
                               class="form-control dateinput" id="FromDate" data-bind="FromDatePicker:{}, value: selectedFromDate" />
                    </div>

                    <div class="col-sm-1 noMargin smallMargintop">
                        <label>
                            End Date
                        </label>
                    </div>
                    <div class="col-md-2 noMargin smallMargintop">
                        <input type="text" placeholder="To Date(mm/dd/yyyy)" required title="To Date is required" name="ToDate"
                               class="form-control dateinput" id="ToDate" data-bind="ToDatePicker:{}, value: selectedToDate" />
                     </div>

                       <div class="col-md-1 noMargin smallMargintop">
                    <div class="btn btn-primary " role="button"   type="submit"   data-bind="click: GetUserLog">
                        Search
                    </div>    
                 </div> 
             </div>

             <br />

                                 <div class="row rowNoMargin entryPanel">
                                  
                                    <!---------Toolbar------>
                            <div class="row   commandbuttondiv" style="width:80%"  >



                                    <div class="pagerPanel">
                                    <div>
                                        <span>Page size</span>
                                        <select data-bind="options:pageSizeOptionsUsersLog,value:selectedPageSizeUsersLog,event: { change:GetSelectedPageSizeUsersLog}"></select>
                                    </div>

                                    <div>
                                        <span>Page no.</span>
                                        <input type="text" name="page-number" value="" data-bind="value: pageNumberUsersLog, valueUpdate: 'input', PagingwithEnterKey: GetSelectedPageUsersLog" />
                                        <button data-bind="click: GetSelectedPageUsersLog">GO</button>
                                    </div>

                                    <div>
                                        <span class="fa fa-angle-left" data-bind="click: BackwardpageClickUsersLog"></span>
                                        <span data-bind="text:pageNoLabelUsersLog"></span>
                                        <span class="fa fa-angle-right" data-bind="click: ForwardPageClickUsersLog"></span>
                                    </div>
                                </div>




                            </div>

                         

                            
                                    <!-----Grid------->
                                    <div class="dataGrid" style="margin-top:5px !important; width:80%"  >
                                        <table>

                                            <thead>
                                                <tr>
                                                   


                                                    <th  data-bind="click: function () { SetOrderByUsersLog('ref_id') }">
                                                        <span>ref Id</span>
                                                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'ref_id' == orderByUsersLog(), 'divhidden': 'ref_id' != orderByUsersLog() }">
                                                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsersLog(), 'divhidden': !isDownUsersLog() }">
                                                            </div>
                                                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsersLog(), 'divhidden': !isUpUsersLog() }">
                                                            </div>
                                                        </div>
                                                    </th> 

 
                                                     <th  data-bind="click: function () { SetOrderByUsersLog('event') }">
                                                        <span>Event</span>
                                                        <div class="sortingIcon" data-bind="css: { 'divvisible': 'event' == orderByUsersLog(), 'divhidden': 'event' != orderByUsersLog() }">
                                                            <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsersLog(), 'divhidden': !isDownUsersLog() }">
                                                            </div>
                                                            <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsersLog(), 'divhidden': !isUpUsersLog() }">
                                                            </div>
                                                        </div>
                                                    </th> 
                                       
                                        
                                                    <th  data-bind="click: function () { SetOrderByUsersLog('event_date') }">
                                                                <span>Date</span>
                                                                <div class="sortingIcon" data-bind="css: { 'divvisible': 'event_date' == orderByUsersLog(), 'divhidden': 'event_date' != orderByUsersLog() }">
                                                                    <div class=" glyphicon glyphicon-chevron-down" data-bind="css: { 'divvisible': isDownUsersLog(), 'divhidden': !isDownUsersLog() }">
                                                                    </div>
                                                                    <div class="glyphicon glyphicon-chevron-up" data-bind="css: { 'divvisible': isUpUsersLog(), 'divhidden': !isUpUsersLog() }">
                                                                    </div>
                                                                </div>
                                                     </th> 
                                      
                                                       
                                                  </tr>


                                            </thead>

                        


                                            <tbody data-bind="foreach: $root.UserLogList">
                                               <tr >
                   
                

                                                    <td data-bind="text: ppRefID"></td>

                                                    <td data-bind="text: ppEvent"></td>

                                                    <td data-bind="text: FormatDate"></td>

  
                                                </tr>

                                            </tbody>

                                        </table>

                                        <br />



                                    </div>


                                    <!----------->
                            <!------>

                      </div>


        </div>


    </div>


</asp:Content>


<asp:Content ID="CompaniesScripts" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/Users") %>
</asp:Content>
