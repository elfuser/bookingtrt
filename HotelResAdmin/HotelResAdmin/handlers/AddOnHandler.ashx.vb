﻿Imports System.Web
Imports System.Web.Services
Imports BRules
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO

Public Class AddOnHandler
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim AddonID As String = ""
        Dim PictureNum As String = ""


        Try

            'AddonID = context.Request.Form("AddonID") & ""
            'PictureNum = context.Request.Form("PictureNum") & ""

            AddonID = (context.Request.QueryString("AddonID") & "")
            PictureNum = (context.Request.QueryString("PictureNum") & "")

            If AddonID <> "" And context.Request.Files.Count > 0 Then

                Dim postedfile = context.Request.Files(0)
                Dim Logo As Byte() = Nothing

                If postedfile IsNot Nothing Then

                    Dim clsAddon As New BRules.Addons
                    Dim response As Response


                    Dim extention As String = System.IO.Path.GetExtension(postedfile.FileName)
                    Dim Buffer As Byte() = New Byte(CInt(postedfile.InputStream.Length) - 1) {}
                    postedfile.InputStream.Read(Buffer, 0, CInt(postedfile.InputStream.Length))
                    Logo = Buffer


                    response = clsAddon.SaveAddonPicture(AddonID, PictureNum, Buffer)

                    context.Response.BinaryWrite(Buffer)

                End If


                Dim image As Image = GetAddonImage(AddonID, PictureNum)

                If image Is Nothing Then
                    context.Response.ContentType = "image/jpeg"
                    image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                    Dim ms As New MemoryStream()

                    image.Save(ms, ImageFormat.Jpeg)
                    ms.WriteTo(context.Response.OutputStream)
                Else
                    context.Response.ContentType = "image/jpeg"
                    image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
                End If


            End If

            If Not [String].IsNullOrEmpty(context.Request.QueryString("AddonID")) Then

                AddonID = Val(context.Request.QueryString("AddonID") & "")
                PictureNum = Val(context.Request.QueryString("PictureNum") & "")


                Dim image As Image = GetAddonImage(AddonID, PictureNum)

                If image Is Nothing Then
                    context.Response.ContentType = "image/jpeg"
                    image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                    Dim ms As New MemoryStream()

                    image.Save(ms, ImageFormat.Jpeg)
                    ms.WriteTo(context.Response.OutputStream)
                Else
                    context.Response.ContentType = "image/jpeg"
                    image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
                End If


            Else
                Dim Image As Image
                context.Response.ContentType = "image/jpeg"
                image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                Dim ms As New MemoryStream()

                image.Save(ms, ImageFormat.Jpeg)
                ms.WriteTo(context.Response.OutputStream)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function GetAddonImage(AddOnID As Long, PictureNum As Long) As Image
        Dim memoryStream As New MemoryStream()
        memoryStream = Nothing

        Try


            Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
            Using sqlConnection As New SqlConnection(connectionString)
                Using sqlCommand As New SqlCommand("spAddOnPhotobyNum", sqlConnection)
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.Parameters.AddWithValue("@AddOn_id", AddOnID)
                    sqlCommand.Parameters.AddWithValue("@photoNum", PictureNum)
                    sqlConnection.Open()
                    Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                    If sqlDataReader.HasRows Then
                        sqlDataReader.Read()
                        If Not sqlDataReader("photo") Is DBNull.Value Then
                            Dim btImage As Byte() = DirectCast(sqlDataReader("photo"), Byte())
                            memoryStream = New MemoryStream(btImage, False)
                        End If
                    End If
                End Using
                sqlConnection.Close()
            End Using

        Catch ex As Exception
            Return Nothing
        End Try

        If memoryStream Is Nothing Then
            Return Nothing
        Else
            Return Image.FromStream(memoryStream)
        End If
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class