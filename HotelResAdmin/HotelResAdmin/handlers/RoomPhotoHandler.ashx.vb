﻿Imports System.Web
Imports System.Web.Services
Imports BRules
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO

Public Class RoomPhotoHandler
    Implements System.Web.IHttpHandler


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim RoomID As String = ""
        Dim PictureNum As String = ""


        Try

            RoomID = (context.Request.QueryString("RoomID") & "")
            PictureNum = (context.Request.QueryString("PictureNum") & "")

            If RoomID <> "" And context.Request.Files.Count > 0 Then

                Dim postedfile = context.Request.Files(0)
                Dim Logo As Byte() = Nothing

                If postedfile IsNot Nothing Then

                    Dim response As New Response
                    Dim objPicture As New Picture

                    Dim extention As String = System.IO.Path.GetExtension(postedfile.FileName)
                    Dim Buffer As Byte() = New Byte(CInt(postedfile.InputStream.Length) - 1) {}
                    postedfile.InputStream.Read(Buffer, 0, CInt(postedfile.InputStream.Length))
                    Logo = Buffer

                    response = objPicture.SaveToDatabaseRoomPhoto(RoomID, PictureNum, Buffer)

                    context.Response.BinaryWrite(Buffer)

                End If


                Dim image As Image = GetRoomImage(RoomID, PictureNum)

                If image Is Nothing Then
                    context.Response.ContentType = "image/jpeg"
                    image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                    Dim ms As New MemoryStream()

                    image.Save(ms, ImageFormat.Jpeg)
                    ms.WriteTo(context.Response.OutputStream)
                Else
                    context.Response.ContentType = "image/jpeg"
                    image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
                End If


            End If

            If Not [String].IsNullOrEmpty(context.Request.QueryString("RoomID")) Then

                RoomID = Val(context.Request.QueryString("RoomID") & "")
                PictureNum = Val(context.Request.QueryString("PictureNum") & "")


                Dim image As Image = GetRoomImage(RoomID, PictureNum)

                If image Is Nothing Then
                    context.Response.ContentType = "image/jpeg"
                    image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                    Dim ms As New MemoryStream()

                    image.Save(ms, ImageFormat.Jpeg)
                    ms.WriteTo(context.Response.OutputStream)
                Else
                    context.Response.ContentType = "image/jpeg"
                    image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
                End If


            Else
                Dim Image As Image
                context.Response.ContentType = "image/jpeg"
                Image = New Bitmap(context.Server.MapPath("../images/no-image.jpg"))
                Dim ms As New MemoryStream()

                Image.Save(ms, ImageFormat.Jpeg)
                ms.WriteTo(context.Response.OutputStream)
            End If

        Catch ex As Exception

        End Try

    End Sub


    Private Function GetRoomImage(roomTypeID As Integer, photoNumber As Integer) As Image
        Dim memoryStream As New MemoryStream()
        memoryStream = Nothing

        Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
        Using sqlConnection As New SqlConnection(connectionString)
            Using sqlCommand As New SqlCommand("spRoomPhoto", sqlConnection)
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@mode", "SEL")
                sqlCommand.Parameters.AddWithValue("@roomtype_id", roomTypeID)
                sqlCommand.Parameters.AddWithValue("@photoNumber", photoNumber)

                sqlConnection.Open()
                Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                If sqlDataReader.HasRows Then
                    sqlDataReader.Read()
                    If Not sqlDataReader("photo") Is DBNull.Value Then
                        Dim btImage As Byte() = DirectCast(sqlDataReader("photo"), Byte())
                        memoryStream = New MemoryStream(btImage, False)
                    End If
                End If
            End Using
            sqlConnection.Close()
        End Using

        If memoryStream Is Nothing Then
            Return Nothing
        Else
            Return Image.FromStream(memoryStream)
        End If
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class