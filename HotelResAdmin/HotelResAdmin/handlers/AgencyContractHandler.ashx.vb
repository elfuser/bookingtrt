﻿Imports System.Web
Imports System.Web.Services
Imports BRules
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO

Public Class AgencyContractHandler
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim ContractCode As String = ""
        Dim PictureNum As String = ""


        Try

            ContractCode = (context.Request.QueryString("ContractCode") & "")

            If ContractCode <> "" And context.Request.Files.Count > 0 Then

                Dim postedfile = context.Request.Files(0)
                Dim Logo As Byte() = Nothing

                If postedfile IsNot Nothing Then

                    Dim clsAddon As New BRules.Agencies
                    Dim response As Response


                    Dim extention As String = System.IO.Path.GetExtension(postedfile.FileName)
                    Dim Buffer As Byte() = New Byte(CInt(postedfile.InputStream.Length) - 1) {}
                    postedfile.InputStream.Read(Buffer, 0, CInt(postedfile.InputStream.Length))
                    Logo = Buffer

                    response = clsAddon.SaveAgencyContactFile(ContractCode, postedfile.FileName, postedfile.ContentType, Buffer)
                    context.Response.BinaryWrite(Buffer)
                End If

            Else
                If Not [String].IsNullOrEmpty(context.Request.QueryString("ContractCode")) Then

                    ContractCode = Val(context.Request.QueryString("ContractCode") & "")

                    Dim FileName As String = String.Empty
                    Dim ContentType As String = String.Empty
                    Dim image As Byte() = GetAgencyContractFile(ContractCode, FileName, ContentType)

                    If image IsNot Nothing Then
                        context.Response.Clear()
                        context.Response.Buffer = True
                        context.Response.Charset = ""
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        context.Response.ContentType = ContentType '"application/pdf"
                        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName)
                        context.Response.BinaryWrite(image)
                        context.Response.Flush()
                        context.Response.End()
                    End If
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function GetAgencyContractFile(ByVal ContractCode As Integer, ByRef FileName As String, ByRef ContentType As String) As Byte()
        Dim memoryStream As New MemoryStream()
        memoryStream = Nothing
        Dim bt As Byte()
        Try


            Dim connectionString As String = ConfigurationManager.ConnectionStrings("CnnString").ConnectionString
            Using sqlConnection As New SqlConnection(connectionString)
                Using sqlCommand As New SqlCommand("spAgencyContract", sqlConnection)
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.Parameters.AddWithValue("@mode", "SEL")
                    sqlCommand.Parameters.AddWithValue("@agency_contract_code", ContractCode)
                    sqlConnection.Open()
                    Dim sqlDataReader As SqlDataReader = sqlCommand.ExecuteReader()

                    If sqlDataReader.HasRows Then
                        sqlDataReader.Read()
                        If Not sqlDataReader("contract_file_data") Is DBNull.Value Then
                            bt = DirectCast(sqlDataReader("contract_file_data"), Byte())
                            memoryStream = New MemoryStream(bt, False)
                        End If

                        If Not sqlDataReader("contract_file_name") Is DBNull.Value Then
                            FileName = sqlDataReader("contract_file_name")
                        End If

                        If Not sqlDataReader("contract_file_type") Is DBNull.Value Then
                            ContentType = sqlDataReader("contract_file_type")
                        End If
                    End If
                End Using
                sqlConnection.Close()
            End Using

        Catch ex As Exception
            Return Nothing
        End Try

        If bt IsNot Nothing Then
            Return bt
        Else
            Return Nothing
        End If
        'If memoryStream Is Nothing Then
        '    Return Nothing
        'Else
        '    Return Image.FromStream(memoryStream)
        'End If
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class